﻿using Dapper;
using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace PRM.Services.Common
{
    public class ExcelService : IExcelService
    {
        public IList<T> To<T>(string sheetName, byte[] buffer) where T : class, new()
        {

            DataTable DT = new DataTable();
            Stream stream = new MemoryStream(buffer);

            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            DataSet result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
            {
                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                {
                    UseHeaderRow = true
                }
            });

            excelReader.Close();


            return result.ToList<T>();
        }

        public IList<T> To<T>(string sheetName, string filePath) where T : class, new()
        {
            string Import_FileName = filePath;
            string fileExtension = Path.GetExtension(Import_FileName);


            DataTable DT = new DataTable();
            FileStream stream = File.Open(Import_FileName, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            DataSet result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
            {
                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                {
                    UseHeaderRow = true
                }
            });

            excelReader.Close();


            return result.ToList<T>();

        }





        private string[] GetExcelColumn<T>()
        {
            List<string> columns = new List<string>();
            var COlumns = typeof(T).GetProperties();
            foreach (var p in COlumns)
            {
                if (!p.GetCustomAttributes(typeof(NotMappedAttribute), false).Any() && !p.GetGetMethod().IsVirtual)
                {
                    if (p.GetCustomAttributes(typeof(ColumnAttribute), false).Any())
                    {
                        var attr = p.GetCustomAttributes(typeof(ColumnAttribute), false).FirstOrDefault() as ColumnAttribute;
                        columns.Add(string.Format("[{0}] AS {1} ", attr.Name, p.Name));
                    }
                    else
                    {
                        columns.Add(string.Format("[{0}]", p.Name));

                    }
                }


            }

            return columns.ToArray();
        }
    }



}
