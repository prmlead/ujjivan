﻿using PRM.Core.Domain.Messages;
using PRM.Core.Domain.RealTimePrices;
using PRM.Services.Messages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PRM.Services.RealTimePrices
{
    public class RealTimePriceWorkflowMessage : IRealTimePriceWorkflowMessage
    {

        public RealTimePriceWorkflowMessage(IEmailSender emailSender)
        {

            this._emailSender = emailSender;
        }
        private readonly IEmailSender _emailSender;


        protected virtual void SendNotification(string messageTemplate, string subject,
             EmailAccount emailAccount, int languageId, Dictionary<string, string> tokens,
             string toEmailAddress, string toName,
             string attachmentFilePath = null, string attachmentFileName = null,
             string replyToEmailAddress = null, string replyToName = null)
        {
            if (messageTemplate == null)
                throw new ArgumentNullException("messageTemplate");
            if (emailAccount == null)
                throw new ArgumentNullException("emailAccount");



            //Replace subject and body tokens
            var subjectReplaced = _emailSender.Replace(subject, tokens, false);
            var bodyReplaced = _emailSender.Replace(messageTemplate, tokens, true);


            _emailSender.SendEmail(emailAccount, subjectReplaced, bodyReplaced, emailAccount.Email, emailAccount.DisplayName, toEmailAddress, toName);


            //limit name length
        }

        public void SendMailOnProductPriceDrop(string toEmail,string name,IList<RealtimePriceDrop> RealTimePrice, DateTime? dropDate)
        {


            StringBuilder htmlStr = new StringBuilder("");
            htmlStr.Append("<p>Dear User</p> <p> The prices for the following has taken a dip on %dropDate%</p>");
            htmlStr.Append("<table width=\"100%\" cellpadding=\"0\" cellspacing =\"0\" bgcolor =\"#FFFFFF\" style=\"border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7\"> ");
            htmlStr.Append("<tr>");
            TableHeader(htmlStr, "Product");
            TableHeader(htmlStr, "Market");
            TableHeader(htmlStr, "Previous price");
            TableHeader(htmlStr, "Current price");
            TableHeader(htmlStr, "Unit");


            htmlStr.Append("</tr>");

            foreach(var price in RealTimePrice)
            {
                htmlStr.Append("<tr>");
                TableCell(htmlStr, price.Today.Product);
                TableCell(htmlStr, price.Today.Market);
                TableCell(htmlStr, price.Yesterday.Price.ToString());
                TableCell(htmlStr, price.Today.Price.ToString());
                TableCell(htmlStr, price.Today.Unit);

                htmlStr.Append("</tr>");
            }

            htmlStr.Append("</table>");

            var Tokens = new Dictionary<string, string>();
            Tokens.Add("dropDate", dropDate.Value.ToString("dd-MM-yyyy"));
            SendNotification(htmlStr.ToString(), "Price Drop Notification", GetDefaultEmailAccount(),0, Tokens, toEmail, name);
        }

        public void SendMessageOnDropLastPursched(string toEmail, string name, string productName, decimal diffPrice, decimal price, DateTime? dropDate)
        {
            var messageTemplate = @"<p>Dear User, <br/>
                             %productName% price has taken a dip of %diffPrice% compared to your last purchase of %price% On %dropDate%. <p>";

            var Tokens = new Dictionary<string, string>();
            Tokens.Add("productName", productName);
            Tokens.Add("diffPrice", diffPrice.ToString());
            Tokens.Add("price", price.ToString());

            Tokens.Add("dropDate", dropDate.Value.ToString("dd-MM-yyyy"));

            SendNotification(messageTemplate, "Product Price Drop", GetDefaultEmailAccount(), 0, Tokens, toEmail, name);
        }

        public void SendMessageOnPriceReachedThershlod(string toEmail, string name, IList<RealTimePrice> RealTimePrice, DateTime? dropDate, List<RealTimeSelectedProduct> RealtimeSelectProduct)
        {
            var messageTemplate = @"<p>Dear User, <br/>
                              %productName% price for today is %price%";

            var Tokens = new Dictionary<string, string>();

            StringBuilder htmlStr = new StringBuilder("");
            htmlStr.Append("<p>Dear User</p> <p>The prices for the following have reached the threshold value today</p>");
            htmlStr.Append("<table width=\"100%\" cellpadding=\"0\" cellspacing =\"0\" bgcolor =\"#FFFFFF\" style=\"border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7\"> ");
            htmlStr.Append("<tr>");
            TableHeader(htmlStr, "Product");
            TableHeader(htmlStr, "Market");
            TableHeader(htmlStr, "Threshold price");
            TableHeader(htmlStr, "Current price");
            TableHeader(htmlStr, "Unit");

            htmlStr.Append("</tr>");

            foreach (var price in RealTimePrice)
            {
                htmlStr.Append("<tr>");
                TableCell(htmlStr, price.Product);
                TableCell(htmlStr, price.Market);
                TableCell(htmlStr, RealtimeSelectProduct.Where(e=>e.Product == price.Product).FirstOrDefault().ThresholdValue.ToString());
                TableCell(htmlStr, price.Price.ToString());
                TableCell(htmlStr, price.Unit);

                htmlStr.Append("</tr>");
            }

            htmlStr.Append("</table>");
            Tokens.Add("dropDate", dropDate.Value.ToString("dd-MM-yyyy"));

            SendNotification(htmlStr.ToString(), "Threshold Price Alert", GetDefaultEmailAccount(), 0, Tokens, toEmail, name);
        }


        private  EmailAccount GetDefaultEmailAccount()
        {

            return new EmailAccount()
            {
                Host = ConfigurationManager.AppSettings["MAILHOST"],
                DisplayName = "PRM 360",
                Email = ConfigurationManager.AppSettings["FROMADDRESS"],
                Username  = ConfigurationManager.AppSettings["MAILHOST_USER"],
                Password = ConfigurationManager.AppSettings["MAILHOST_PWD"],
                EnableSsl = true,
                UseDefaultCredentials = false,
                Port = 25
            };
        }

        private void TableHeader(StringBuilder htmlStr, string name) {
            htmlStr.Append($"<td  align=\"left\" bgcolor=\"#252525\" style=\"font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 12px; color: #EEEEEE; padding:10px; padding-right:0;border-bottom:1px solid #d7d7d7\">{name}</td>");

        }

        private void TableCell(StringBuilder htmlStr, string value)
        {
            htmlStr.Append($"<td  align=\"left\" bgcolor=\"#fff\" style=\"font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 12px; color: #777; padding:10px; padding-right:0;border-bottom:1px solid #d7d7d7\">{value}</td>");

        }


    }
}
