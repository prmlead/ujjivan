﻿using PRM.Core.Domain.Acl;
using PRM.Core.Pagers;
using System.Collections.Generic;

namespace PRM.Services.Acl
{
    public interface IAclMappingService
    {

        void InsertAclMapping(AclMapping entity);

        void UpdateAclMapping(AclMapping entity);

        void DeleteAclMapping(AclMapping entity);

        AclMapping GetAclMappingById(int id);

        IList<AclMapping> GetAclMappingList(object filter);

        IPagedList<AclMapping> GetAclMappingList(Pager pager, int companyId);

        void InsertDepartmentMapping(int companyId,string entityName,int entityId,int[] deparmentIds);
        void InsertProjectMapping(int companyId, string entityName, int entityId, int[] prohects);

        void InsertUserMapping(int companyId, string entityName, int entityId, int[] users);


    }
}