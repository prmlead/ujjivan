﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Configuration;
using System.Data;
using PRMServices.SQLHelper;
using System.Data.SqlClient;
using PRMServices;
using CORE = PRM.Core.Common;
using OfficeOpenXml;
using System.Text;


namespace SAPFTPIntegration
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static char csvDelimiter = '|';
        private static string _archiveFolder = ConfigurationManager.AppSettings["ArchiveFolder"].ToString();
        private static string _errorFolder = ConfigurationManager.AppSettings["ErrorFolder"].ToString();
        private static string _stageFolder = ConfigurationManager.AppSettings["StageFolder"].ToString();
        private static string _processFolder = ConfigurationManager.AppSettings["ProcessFolder"].ToString();
        private static string SAPFTPPRFilePattern = "PRM_SAP_PR";
        private static string SAPFTPVEndorFilePattern = "VEND";
        private static string SAPFTPMATERIALFilePattern = "MAT";

        static void Main(string[] args)
        {
            string jobType = ConfigurationManager.AppSettings["JOB_TYPE"].ToString();
            logger.Info("JOB_TYPE:" + jobType);

            NetworkCredential credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserId"], ConfigurationManager.AppSettings["UserPwd"]);
            string url = ConfigurationManager.AppSettings["Host"];

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "PR_JOB")
            {
                DownloadFtpDirectory(url + "/PR_Data/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPPRFilePattern);
                ProcessPRFiles();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "VENDOR_JOB")
            {
                //DownloadFtpDirectory(url + "/INDUS/VEND_DATA/" , credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPVEndorFilePattern);
                ProcessVendorFiles();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "MATERIAL_JOB")
            {
                //DownloadFtpDirectory(url + "/INDUS/MAT_DATA/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPMATERIALFilePattern);
                ProcessMaterialsFiles();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "SAVE_PO_JOB")
            {
                // DownloadFtpDirectory(url + "/PO_Data/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPPOFilePattern);
                string poUrl = url + "/PO_DATA/";
                ProcesSavePOFiles();
            }
        }

        private static void ProcesSavePOFiles()//string url, NetworkCredential credentials
        {
            char[] charsToTrim = { ' ' };
            string delimetre = "|";
            string POSCHParams = ConfigurationManager.AppSettings["POSCHParams"];
            string CID = POSCHParams.Split(';')[0];
            string UID = POSCHParams.Split(';')[1];
            int COMP_ID = Convert.ToInt32(CID);
            int USER_ID = Convert.ToInt32(UID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_COMP_ID", COMP_ID);
            sd.Add("P_U_ID", USER_ID);
            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            try
            {
                var dataset = sqlHelper.SelectList("sap_GetPOScheduleList", sd);
                var dataColumns = dataset.Tables[0].Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                dataColumns = dataColumns.Skip(1).ToArray();
                string excelValues = string.Join(delimetre, dataColumns);
                int totalRows = dataset.Tables[0].Rows.Count;
                string fileExtension = ConfigurationManager.AppSettings["POFileextension"];
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("PO_DATA_" + DateTime.Now.Ticks);
                MemoryStream ms = new MemoryStream();
                if (totalRows > 0)
                {
                    List<string> ids = new List<string>();
                    foreach (DataRow row in dataset.Tables[0].Rows)
                    {
                        excelValues += Environment.NewLine;
                        excelValues += string.Join(delimetre, row.ItemArray.Skip(1));
                        //File.AppendAllText(_stageFolder + "PO_DATA_" + DateTime.Now.Ticks + fileExtension, excelValues);
                        ids.Add(row.ItemArray[0].ToString());
                    }
                    File.WriteAllText(_stageFolder + "\\" + "PO_DATA_" + DateTime.Now.Ticks + fileExtension, excelValues);
                    if (ids.Count > 0)
                    {
                        string updatePOQuery = string.Empty;
                        updatePOQuery += $@"update POScheduleDetails SET PO_SENT_STATUS = 1,PO_STATUS = 'SUBMITTED' where PO_SCHEDULE_ID in ({string.Join(",", ids)});";
                        sqlHelper.ExecuteNonQuery_IUD(updatePOQuery);
                    }
                }
            }
            catch (Exception ex) {
                logger.Error("Exception is>>"+ex.Message);
                logger.Error("Inner Exception is>>"+ ex.InnerException);
            }
        }

        //private static string MakeCsvFile(List<FTPPOSchedule> data)
        //{
        //    List<string> columns = "WERKS,MATNR,TXZ01,MATKL,LIFNR,NAME1,TELF2,SMTP_ADDR,EBELN,M_PO_NUM,EBELP,AEDAT,ERNAM,ZTERM,MWSKZ,TEXT1,FGDAT,EINDT,MTART,CITY1,BEZEI,STEUC,MENGE,MEINS,NETPR,NETWR,WAERS,M_FREIGHT,OBMNG,BNFPO,BANFN,MENGE,FRGDT,M_PRLINETEXT,LFDAT,M_ITEMTEXTPO,FRGKX,AFNAM,BSART,SRVPOS,KTEXT1,M_MISCCHARGES,M_PACKINGCHARGES,ANFNR,EKORG,EKGRP,BEDNR,KNTTP,AUFNR,SAKTO,EXTROW,TBTWR,MENGE,EPSTP,BUKRS,BRANCH".Split(',').ToList();
        //    MemoryStream ms = new MemoryStream();
        //    byte[] contents = null;
        //    var distinctPRMPONum = data.GroupBy(elem => elem.PRM_PO_NUMBER).Select(group => group.First());
        //    List<string> PRNumbers1 = new List<string>();
        //    string result = "";
        //    var LocalPath = Path.GetTempPath();
        //    //var fileName1 = Guid.NewGuid().ToString() + ".csv";
        //    //Path.Combine(path, fileName1);
        //    try
        //    {
        //        string PRString1 = "";
        //        foreach (FTPPOSchedule prmpo in distinctPRMPONum)
        //        {
        //            List<FTPPOSchedule> groupdata = new List<FTPPOSchedule>();
        //            groupdata = data.Where(item => item.PRM_PO_NUMBER == prmpo.PRM_PO_NUMBER).ToList();

        //            char Delimiter = '|';
        //            ExcelPackage ExcelPkg = new ExcelPackage();
        //            ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add(groupdata[0].BRANCH_LOCATION + "_PO_DATA");
        //            if (columns.Count > 0)
        //            {
        //                string csvDelimitedString = "";

        //                long tick = DateTime.UtcNow.Ticks;
        //                string fileExtension = ConfigurationManager.AppSettings["POFileextension"];
        //                string fileName = "PO_" + groupdata[0].PRM_PO_NUMBER + "_" + tick + fileExtension;
        //                string locationPath = "";
        //                if (!string.IsNullOrEmpty(groupdata[0].BRANCH_LOCATION))
        //                {
        //                    locationPath = ConfigurationManager.AppSettings["POData"];
        //                }
        //                //if (groupdata[0].VERTICAL_TYPE == "SUGARS")
        //                //{
        //                //    locationPath = ConfigurationManager.AppSettings["PODataSugars"];
        //                //}
        //                //if (groupdata[0].VERTICAL_TYPE == "TEXTILES")
        //                //{
        //                //    locationPath = ConfigurationManager.AppSettings["PODataTextiles"];
        //                //}
        //                if (!File.Exists(locationPath + fileName))
        //                {
        //                    for (int i = 0; i <= (columns.Count - 1); i++)
        //                    {
        //                        var row = columns[i];
        //                        if (!string.IsNullOrEmpty(row))
        //                        {
        //                            csvDelimitedString += row + Delimiter;
        //                        }
        //                    }
        //                    csvDelimitedString = csvDelimitedString.Substring(0, csvDelimitedString.Length - (",").Length);
        //                    csvDelimitedString = csvDelimitedString + Environment.NewLine;
        //                    //------to local folder-------//
        //                    //File.WriteAllText(locationPath + fileName, csvDelimitedString);
        //                    File.WriteAllText(LocalPath + fileName, csvDelimitedString);
        //                    //------to local folder-------//
        //                }

        //                string PRString = "";
        //                if (groupdata.Count > 0)
        //                {
        //                    List<string> PRNumbers = new List<string>();

        //                    foreach (FTPPOSchedule po in groupdata)
        //                    {
        //                        string dataString = "";
        //                        PRNumbers.Add(Convert.ToString(po.PO_SCHEDULE_ID));
        //                        dataString += (!string.IsNullOrEmpty(po.PLANT) ? po.PLANT : "") + Delimiter;
        //                        dataString += (po.IS_SERVICE_PRODUCT == 0 ? (!string.IsNullOrEmpty(po.MATERIAL) ? po.MATERIAL : "") : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.DESCRIPTION) ? po.DESCRIPTION : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.MATERIAL_GROUP) ? po.MATERIAL_GROUP : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.VENDOR_CODE) ? po.VENDOR_CODE : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.VENDOR_COMPANY) ? po.VENDOR_COMPANY : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.VENDOR_PHONE) ? po.VENDOR_PHONE : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.VENDOR_EMAIL) ? po.VENDOR_EMAIL : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.PO_NUMBER) ? po.PO_NUMBER : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.PRM_PO_NUMBER) ? po.PRM_PO_NUMBER : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.PO_LINE_ITEM) ? po.PO_LINE_ITEM : "") + Delimiter;
        //                        string poDate = Convert.ToString(po.PO_DATE);
        //                        dataString += (!string.IsNullOrEmpty(poDate) ? poDate : "") + Delimiter;
        //                        dataString += (po.FTPPOScheduleItems.Count > 0 ? (!string.IsNullOrEmpty(po.FTPPOScheduleItems[0].PR_CREATOR_NAME) ? po.FTPPOScheduleItems[0].PR_CREATOR_NAME : "") : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.PAYMENT_TERMS) ? po.PAYMENT_TERMS : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.TAX_CODE) ? po.TAX_CODE : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.TAX_CODE_DESC) ? po.TAX_CODE_DESC : "") + Delimiter;
        //                        string poReleaseDate = Convert.ToString(po.PO_RELEASE_DATE);
        //                        string deliveryDate = Convert.ToString(po.DELIVERY_DATE);
        //                        string materialDeliveryDate = po.FTPPOScheduleItems.Count > 0 ? Convert.ToString(po.FTPPOScheduleItems[0].MATERIAL_DELIVERY_DATE) : "";
        //                        materialDeliveryDate = materialDeliveryDate.Contains("1900") ? null : materialDeliveryDate;
        //                        dataString += (!string.IsNullOrEmpty(poReleaseDate) ? poReleaseDate : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(materialDeliveryDate) ? materialDeliveryDate : null) + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.MAT_TYPE) ? po.MAT_TYPE : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.CITY) ? po.CITY : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.REGION) ? po.REGION : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.HSN_CODE) ? po.HSN_CODE : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.ORDER_QTY)) ? Convert.ToString(po.ORDER_QTY) : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.UOM) ? po.UOM : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.NET_PRICE)) ? Convert.ToString(po.NET_PRICE) : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.VALUE)) ? Convert.ToString(po.VALUE) : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.CURRENCY)) ? Convert.ToString(po.CURRENCY) : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.FREIGHT)) ? Convert.ToString(po.FREIGHT) : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.PENDING_QTY)) ? Convert.ToString(po.PENDING_QTY) : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.PO_LINE_ITEM)) ? Convert.ToString(po.PO_LINE_ITEM) : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.PR_NUMBER)) ? Convert.ToString(po.PR_NUMBER) : "") + Delimiter;
        //                        dataString += (po.FTPPOScheduleItems.Count > 0 ? (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].REQUIRED_QUANTITY)) ? Convert.ToString(po.FTPPOScheduleItems[0].REQUIRED_QUANTITY) : "") : "") + Delimiter;
        //                        dataString += (po.FTPPOScheduleItems.Count > 0 ? (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].RELEASE_DATE)) ? Convert.ToString(po.FTPPOScheduleItems[0].RELEASE_DATE) : "") : "") + Delimiter;
        //                        dataString += "" + Delimiter; //(!string.IsNullOrEmpty(Convert.ToString(po.PR_ITEM_ID)) ? Convert.ToString(po.PR_ITEM_ID) : "") + Delimiter; // PR Line Text
        //                        dataString += (!string.IsNullOrEmpty(materialDeliveryDate) ? materialDeliveryDate : null) + Delimiter;
        //                        dataString += "" + Delimiter; // (!string.IsNullOrEmpty(po.DESCRIPTION) ? po.DESCRIPTION : "")
        //                        dataString += "" + Delimiter; // (!string.IsNullOrEmpty(Convert.ToString(po.PR_ITEM_ID)) ? Convert.ToString(po.PR_ITEM_ID) : "") + Delimiter; //Rel Ind
        //                        dataString += (po.FTPPOScheduleItems.Count > 0 ? (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].REQUISITIONER_NAME)) ? Convert.ToString(po.FTPPOScheduleItems[0].REQUISITIONER_NAME) : "") : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(po.DOC_TYPE) ? po.DOC_TYPE : "") + Delimiter;
        //                        dataString += (po.IS_SERVICE_PRODUCT == 1 ? (!string.IsNullOrEmpty(po.MATERIAL) ? po.MATERIAL : "") : "") + Delimiter;
        //                        dataString += (po.IS_SERVICE_PRODUCT == 1 ? (!string.IsNullOrEmpty(po.SERVICE_DESC) ? po.SERVICE_DESC : "") : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.MISC_CHARGES)) ? Convert.ToString(po.MISC_CHARGES) : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.PACKING_CHARGES)) ? Convert.ToString(po.PACKING_CHARGES) : "") + Delimiter;

        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.REQ_NUMBER)) ? Convert.ToString(po.REQ_NUMBER) : "") + Delimiter; // ANFNR
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.PURCHASE_ORGANISATION)) ? Convert.ToString(po.PURCHASE_ORGANISATION) : "") + Delimiter; // EKORG
        //                        dataString += (po.FTPPOScheduleItems.Count > 0 ? (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].PURCHASE_GROUP_CODE)) ? Convert.ToString(po.FTPPOScheduleItems[0].PURCHASE_GROUP_CODE) : "") : "") + Delimiter;//EKGRP
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.REQ_ID)) ? Convert.ToString(po.REQ_ID) : "") + Delimiter; // BEDNR
        //                        dataString += (po.FTPPOScheduleItems.Count > 0 ? (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].ACCOUNT_ASSIGNMENT_CATEGORY)) ? Convert.ToString(po.FTPPOScheduleItems[0].ACCOUNT_ASSIGNMENT_CATEGORY) : "") : "") + Delimiter;//KNTTP
        //                        dataString += (po.FTPPOScheduleItems.Count > 0 ? (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].ORDER_NO)) ? Convert.ToString(po.FTPPOScheduleItems[0].ORDER_NO) : "") : "") + Delimiter; // AUFNR
        //                        dataString += (po.FTPPOScheduleItems.Count > 0 ? (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].GL_ACCOUNT)) ? Convert.ToString(po.FTPPOScheduleItems[0].GL_ACCOUNT) : "") : "") + Delimiter; // SAKTO
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.SERVICE_LINE)) ? Convert.ToString(po.SERVICE_LINE) : "") + Delimiter;//EXTROW
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.GROSS_PRICE)) ? Convert.ToString(po.GROSS_PRICE) : "") + Delimiter; // TBTWR
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.ORDER_QTY)) ? Convert.ToString(po.ORDER_QTY) : "") + Delimiter; //MENGE
        //                        dataString += (po.FTPPOScheduleItems.Count > 0 ? (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].PR_ITEM_TYPE)) ? Convert.ToString(po.FTPPOScheduleItems[0].PR_ITEM_TYPE) : "") : "") + Delimiter; //EPSTP
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.COMPANY_CODE)) ? Convert.ToString(po.COMPANY_CODE) : "") + Delimiter;
        //                        dataString += (!string.IsNullOrEmpty(Convert.ToString(po.BRANCH_LOCATION)) ? Convert.ToString(po.BRANCH_LOCATION) : "") + Environment.NewLine;
        //                        //------to local folder-------//
        //                        //File.AppendAllText(locationPath + fileName, dataString);
        //                        File.AppendAllText(LocalPath + fileName, dataString);
        //                        //------to local folder-------//
        //                    }
        //                    PRNumbers1 = PRNumbers1.Concat(PRNumbers).ToList();
        //                    PRString = string.Join(",", PRNumbers);
        //                    //PRString = "";
        //                }

        //                //string readText = File.ReadAllText(locationPath + fileName);
        //                string readText = File.ReadAllText(LocalPath + fileName);
        //                //------to local folder-------//
        //                //File.WriteAllText(locationPath + fileName, readText);
        //                File.WriteAllText(LocalPath + fileName, readText);
        //                //------to local folder-------//

        //                //---------------direct file upload to ftp-------------------//

        //                NetworkCredential credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserId"], ConfigurationManager.AppSettings["UserPwd"]);
        //                string url1 = ConfigurationManager.AppSettings["Host"];
        //                var uploadPath = "/INDUS/PO_DATA/";
        //                FtpWebRequest listRequest = (FtpWebRequest)WebRequest.Create(url1 + uploadPath + fileName);
        //                listRequest.Proxy = null;
        //                listRequest.KeepAlive = true;
        //                listRequest.UseBinary = true;
        //                listRequest.Method = WebRequestMethods.Ftp.UploadFile;
        //                listRequest.Credentials = credentials;

        //                //StreamReader sourceStream = new StreamReader(fileName);
        //                byte[] fileContents = Encoding.UTF8.GetBytes(readText);
        //                //sourceStream.Close();
        //                listRequest.ContentLength = fileContents.Length;
        //                Stream requestStream = listRequest.GetRequestStream();
        //                requestStream.Write(fileContents, 0, fileContents.Length);
        //                requestStream.Close();
        //                File.Delete(Path.Combine(LocalPath, fileName));
        //                //---------------direct file upload to ftp-------------------//

        //            }
        //            PRString1 = string.Join(",", PRNumbers1);
        //            result = PRString1;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Info("SAVE_PO:" + ex);
        //        logger.Info("SAVE_PO_MESSAGE:" + ex.Message);
        //        result = "0";
        //    }
        //    return result;
        //}

        private static void ProcessPRFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPPRFilePattern))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.ToLower().Replace(".xls", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.ToLower().Replace(".xls", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.ToLower().Replace(".xls", ".csv");

                        try
                        {
                            List<string> columns = "PLANT,PLANT_CODE,PURCHASE_GROUP_CODE,PURCHASE_GROUP_NAME,REQUESITION_DATE,PR_RELEASE_DATE,PR_CHANGE_DATE,PR_NUMBER,REQUISITIONER_NAME,REQUISITIONER_EMAIL,ITEM_OF_REQUESITION,MATERIAL_GROUP_CODE,MATERIAL_GROUP_DESC,MATERIAL_CODE,MATERIAL_TYPE,MATERIAL_HSN_CODE,MATERIAL_DESCRIPTION,SHORT_TEXT,ITEM_TEXT,UOM,QTY_REQUIRED,MATERIAL_DELIVERY_DATE,LEAD_TIME,PR_TYPE,PR_TYPE_DESC,PR_NOTE,LOEKZ,ERDAT,OTHER_INFO1".Split(',').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"PR records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    string query = @"INSERT INTO [dbo].[SAP_PR_DETAILS] ([JOB_ID], [PLANT], [PLANT_CODE], [PURCHASE_GROUP_CODE], [PURCHASE_GROUP_NAME], [REQUESITION_DATE], [PR_RELEASE_DATE], 
                                    [PR_CHANGE_DATE], [PR_NUMBER], [REQUISITIONER_NAME], [REQUISITIONER_EMAIL], [ITEM_OF_REQUESITION], [MATERIAL_GROUP_CODE], [MATERIAL_GROUP_DESC], [MATERIAL_CODE], [MATERIAL_TYPE], 
                                    [MATERIAL_HSN_CODE], [MATERIAL_DESCRIPTION], [SHORT_TEXT], [ITEM_TEXT], [UOM], [QTY_REQUIRED], [MATERIAL_DELIVERY_DATE], [LEAD_TIME], [PR_TYPE], [PR_TYPE_DESC], [PR_NOTE], 
                                    [LOEKZ], [ERDAT], [DATE_CREATED], [OTHER_INFO1]) VALUES ";
                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;
                                        newRow["PLANT"] = GetDataRowVaue(row, "PLANT");
                                        newRow["PLANT_CODE"] = GetDataRowVaue(row, "PLANT_NAME");
                                        newRow["PURCHASE_GROUP_CODE"] = GetDataRowVaue(row, "PURCHASE_GROUP");
                                        newRow["PURCHASE_GROUP_NAME"] = GetDataRowVaue(row, "PURCHASE_GROUP_NAME");
                                        newRow["REQUESITION_DATE"] = GetDataRowVaue(row, "REQUESITION_DATE");
                                        newRow["PR_RELEASE_DATE"] = GetDataRowVaue(row, "PR_RELEASED_DATE");
                                        newRow["PR_CHANGE_DATE"] = GetDataRowVaue(row, "PR_CHANGE_DATE");
                                        newRow["PR_NUMBER"] = GetDataRowVaue(row, "PR_NUMBER");
                                        newRow["REQUISITIONER_NAME"] = GetDataRowVaue(row, "REQUISITIONER_NAME");
                                        newRow["REQUISITIONER_EMAIL"] = GetDataRowVaue(row, "REQUISITIONER_EMAIL");
                                        newRow["ITEM_OF_REQUESITION"] = GetDataRowVaue(row, "REQUISITION_ITEM");
                                        newRow["MATERIAL_GROUP_CODE"] = GetDataRowVaue(row, "MATERIAL_GROUP_CODE");
                                        newRow["MATERIAL_GROUP_DESC"] = GetDataRowVaue(row, "MATERIAL_GROUP");
                                        newRow["MATERIAL_CODE"] = GetDataRowVaue(row, "MATERIAL_CODE");
                                        newRow["MATERIAL_TYPE"] = GetDataRowVaue(row, "MATERIAL_TYPE");
                                        newRow["MATERIAL_HSN_CODE"] = GetDataRowVaue(row, "HSN_CODE");
                                        newRow["MATERIAL_DESCRIPTION"] = GetDataRowVaue(row, "MATERIAL_DESCRIPTION");
                                        newRow["SHORT_TEXT"] = GetDataRowVaue(row, "SHORT_TEXT");
                                        newRow["ITEM_TEXT"] = GetDataRowVaue(row, "ITEM_TEXT");
                                        newRow["UOM"] = GetDataRowVaue(row, "UOM");
                                        newRow["QTY_REQUIRED"] = GetDataRowVaue(row, "QTY_REQUIRED")?.Replace(",", "");
                                        newRow["MATERIAL_DELIVERY_DATE"] = GetDataRowVaue(row, "MATERIAL_DELIVERY_DATE");
                                        newRow["LEAD_TIME"] = GetDataRowVaue(row, "LEAD_TIME");
                                        newRow["PR_TYPE"] = GetDataRowVaue(row, "PR_TYPE");
                                        newRow["PR_TYPE_DESC"] = GetDataRowVaue(row, "PR_DESC");
                                        newRow["PR_NOTE"] = GetDataRowVaue(row, "DELIVERY_LOCATION");
                                        newRow["LOEKZ"] = GetDataRowVaue(row, "DELETION_IND");
                                        newRow["ERDAT"] = GetDataRowVaue(row, "PR_DATE");
                                        newRow["OTHER_INFO1"] = fileName;
                                        newRow["DATE_CREATED"] = DateTime.UtcNow;

                                        query += $@"('{guid}',";
                                        query += $@"'{GetDataRowVaue(row, "PLANT")}',";
                                        query += $@"'{GetDataRowVaue(row, "PLANT_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "PURCHASE_GROUP")}',";
                                        query += $@"'{GetDataRowVaue(row, "PURCHASE_GROUP_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "REQUESITION_DATE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_RELEASED_DATE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_CHANGE_DATE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_NUMBER")}',";
                                        query += $@"'{GetDataRowVaue(row, "REQUISITIONER_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "REQUISITIONER_EMAIL")}',";
                                        query += $@"'{GetDataRowVaue(row, "REQUISITION_ITEM")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_GROUP_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_GROUP")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_TYPE")}',";
                                        query += $@"'{GetDataRowVaue(row, "HSN_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_DESCRIPTION")}',";
                                        query += $@"'{GetDataRowVaue(row, "SHORT_TEXT")}',";
                                        query += $@"'{GetDataRowVaue(row, "ITEM_TEXT")}',";
                                        query += $@"'{GetDataRowVaue(row, "UOM")}',";
                                        query += $@"'{GetDataRowVaue(row, "QTY_REQUIRED").Replace(",", "")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_DELIVERY_DATE")}',";
                                        query += $@"'{GetDataRowVaue(row, "LEAD_TIME")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_TYPE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_DESC")}',";
                                        query += $@"'{GetDataRowVaue(row, "DELIVERY_LOCATION")}',";
                                        query += $@"'{GetDataRowVaue(row, "DELETION_IND")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_DATE")}',";
                                        query += $@"GETUTCDATE(),";
                                        query += $@"'{fileName}'),";

                                        tempDT.Rows.Add(newRow);
                                    }

                                    query = query.TrimEnd(',');
                                    //bizClass.ExecuteNonQuery_IUD(query);
                                    logger.Debug("END GetPRDetails QUERY: " + query);
                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_PR_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_pr_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }
        private static void ProcessVendorFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPVEndorFilePattern))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv");

                        try
                        {
                            List<string> columns = "NAMEV,NAME1,SMTP_ADDR,TELF1,NAME2,WAERS,LIFNR,MATNR,STCD3,VEN_ALT_FIR,VEN_ALT_LAS,SMTP_ADDR1,TEL_NUMBER,CITY1,ERDAT,UTIME,PAN_NUMBER,POST_CODE1,ZTERM,LAND1,LOCATION,CITY2,LOEVM,STREET,WERKS,STREET1,MFCD,MFCD_DIV,DELV_TERMS,EKORG,DRUG_LICENCE_NO,VEND_TYPE,VENDOR_CLASSIFICATION_FOR_GST,VEND_STATUS,OTHER_INFO1".Split(',').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"Vendor records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    string query = @"INSERT INTO [dbo].[SAP_VENDOR_DETAILS] ([JOB_ID], [NAMEV], [NAME1], [SMTP_ADDR], [TELF1], [NAME2], [WAERS], [LIFNR], [MATNR], [STCD3], 
                                    [VEN_ALT_FIR], [VEN_ALT_LAS], [SMTP_ADDR1], [TEL_NUMBER], [CITY1], [ERDAT], [UTIME], 
                                    PAN_NUMBER,POST_CODE1,ZTERM,LAND1,LOCATION,CITY2,LOEVM,STREET,WERKS,STREET1,MFCD,MFCD_DIV,DELV_TERMS,EKORG,DRUG_LICENCE_NO,VEND_TYPE,VENDOR_CLASSIFICATION_FOR_GST,VEND_STATUS,
                                    [OTHER_INFO1], [DATE_CREATED]) VALUES ";

                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;
                                        newRow["NAMEV"] = GetDataRowVaue(row, "VENDORFIRSTNAME");
                                        newRow["NAME1"] = GetDataRowVaue(row, "VENDORLASTNAME");
                                        newRow["SMTP_ADDR"] = GetDataRowVaue(row, "VENDOREMAIL");
                                        newRow["TELF1"] = GetDataRowVaue(row, "VENDORPHONENUMBER");
                                        newRow["NAME2"] = GetDataRowVaue(row, "LEGALENTITY");
                                        newRow["WAERS"] = GetDataRowVaue(row, "VENDORCURRENCY");
                                        newRow["LIFNR"] = GetDataRowVaue(row, "VENDORERPCODE");
                                        newRow["MATNR"] = GetDataRowVaue(row, "VENDORMATERIALCODE");
                                        newRow["STCD3"] = GetDataRowVaue(row, "VENDORGSTNUMBER");
                                        newRow["VEN_ALT_FIR"] = GetDataRowVaue(row, "VENDORALTERNATEPOCFIRSTNAME");
                                        newRow["VEN_ALT_LAS"] = GetDataRowVaue(row, "VENDORALTERNATECONTACTLASTNAME");
                                        newRow["SMTP_ADDR1"] = GetDataRowVaue(row, "VENDORALTERNATEEMAIL1");
                                        newRow["TEL_NUMBER"] = GetDataRowVaue(row, "VENDORALTERNATEPHONENUMBER");
                                        newRow["CITY1"] = GetDataRowVaue(row, "VENDORCITYNAME");
                                        newRow["ERDAT"] = GetDataRowVaue(row, "VENDORCREATIONDATEINERP");
                                        newRow["UTIME"] = GetDataRowVaue(row, "VENDORCREATIONDATEINERP");
                                        //newRow["TYPE_CODE"] = GetDataRowVaue(row, "TYPE_CODE");
                                        //newRow["TYPE_DESC"] = GetDataRowVaue(row, "TYPE_DESC");
                                        //newRow["BANK_ACCOUNT_NO"] = GetDataRowVaue(row, "BANK_ACCOUNT_NO");
                                        //newRow["IFSC_CODE"] = GetDataRowVaue(row, "IFSC_CODE");
                                        //newRow["BANK_NAME"] = GetDataRowVaue(row, "BANK_NAME");
                                        newRow["PAN_NUMBER"] = GetDataRowVaue(row, "VENDORPANNUMBER");
                                        newRow["POST_CODE1"] = GetDataRowVaue(row, "VENDORPOSTALCODE");
                                        newRow["ZTERM"] = GetDataRowVaue(row, "PAYMENTTERMS");
                                        newRow["LAND1"] = GetDataRowVaue(row, "VENDORCOUNTRYDETAILS");
                                        newRow["LOCATION"] = GetDataRowVaue(row, "VENDORSTATE");
                                        newRow["CITY2"] = GetDataRowVaue(row, "VENDORLINKEDTOCOUNTRY");
                                        newRow["LOEVM"] = GetDataRowVaue(row, "DELETEFLAGFORPURCHASINGORGANIZATION");
                                        newRow["STREET"] = GetDataRowVaue(row, "VENDORSTREET");
                                        newRow["WERKS"] = GetDataRowVaue(row, "PLANT");

                                        newRow["STREET1"] = GetDataRowVaue(row, "VENDORSTREET2");
                                        newRow["MFCD"] = GetDataRowVaue(row, "MANUFACTURERCODE");
                                        newRow["MFCD_DIV"] = GetDataRowVaue(row, "MANUFACTURERDIVISION");
                                        newRow["DELV_TERMS"] = GetDataRowVaue(row, "DELIVERYTERMS");
                                        newRow["EKORG"] = GetDataRowVaue(row, "VENDORPURCHASEORGANIZATIONDATA");
                                        newRow["DRUG_LICENCE_NO"] = GetDataRowVaue(row, "DRUGLICENCENO");
                                        newRow["VEND_TYPE"] = GetDataRowVaue(row, "VENDORTYPE");
                                        newRow["VENDOR_CLASSIFICATION_FOR_GST"] = GetDataRowVaue(row, "VENDORCLASSIFICATIONFORGST");
                                        newRow["VEND_STATUS"] = GetDataRowVaue(row, "STATUS");
                                        



                                        newRow["OTHER_INFO1"] = fileName;
                                        newRow["DATE_CREATED"] = DateTime.UtcNow;

                                        query += $@"('{guid}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORFIRSTNAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORLASTNAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOREMAIL")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORPHONENUMBER")}',";
                                        query += $@"'{GetDataRowVaue(row, "LEGALENTITY")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORCURRENCY")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORERPCODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORMATERIALCODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORGSTNUMBER")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORALTERNATEPOCFIRSTNAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORALTERNATECONTACTLASTNAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORALTERNATEEMAIL1")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORALTERNATEPHONENUMBER")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORCITYNAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORCREATIONDATEINERP")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORCREATIONDATEINERP")}',";
                                        //query += $@"'{GetDataRowVaue(row, "TYPE_CODE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "TYPE_DESC")}',";
                                        //query += $@"'{GetDataRowVaue(row, "BANK_ACCOUNT_NO")}',";
                                        //query += $@"'{GetDataRowVaue(row, "IFSC_CODE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "BANK_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORPANNUMBER")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORPOSTALCODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PAYMENTTERMS")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORCOUNTRYDETAILS")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORSTATE")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORLINKEDTOCOUNTRY")}',";
                                        query += $@"'{GetDataRowVaue(row, "DELETEFLAGFORPURCHASINGORGANIZATION")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORSTREET")}',";
                                        query += $@"'{GetDataRowVaue(row, "PLANT")}',";


                                        query += $@"'{GetDataRowVaue(row, "VENDORSTREET2")}',";
                                        query += $@"'{GetDataRowVaue(row, "MANUFACTURERCODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MANUFACTURERDIVISION")}',";
                                        query += $@"'{GetDataRowVaue(row, "DELIVERYTERMS")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORPURCHASEORGANIZATIONDATA")}',";
                                        query += $@"'{GetDataRowVaue(row, "DRUGLICENCENO")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORTYPE")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDORCLASSIFICATIONFORGST")}',";
                                        query += $@"'{GetDataRowVaue(row, "STATUS")}',";
                                        

                                        query += $@"'{fileName}',";
                                        query += $@"GETUTCDATE()),";

                                        tempDT.Rows.Add(newRow);
                                    }

                                    query = query.TrimEnd(',');
                                    //bizClass.ExecuteNonQuery_IUD(query);
                                    logger.Debug("END GetVENDORDetails QUERY: " + query);
                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_VENDOR_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_vendor_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }

        private static void ProcessMaterialsFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();

                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPMATERIALFilePattern))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv");

                        try
                        {
                            string query1 = @"SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'SAP_MATERIAL_DETAILS'";
                            DataSet ds = bizClass.ExecuteQuery(query1);
                            if (ds!=null && ds.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow row in ds.Tables[0].Rows) 
                                { 
                                
                                }
                            }
                            List<string> columns = "MATKL,WGBEZ,MATNR,MTART,STEUC,MAKTX,MEINS,MEINH,LVORM,GENERIC_CODE,GENERIC_DESCRIPTION,DOSAGE,IS_FORMULARY,SPECILIZATION,SPECILIZATION_DESCRIPTION,MFCD,MFCD_DIV,SPECIFICATION,TEMPERATURE,SCHEDULE,SCHEDULECOLOURCODE_COUNTRYCODE,IS_ASSET,BATCH_MANAGEMENT,RE_USABLE,RE_USABLE_TIMES,IMPORTED,RE_ORDERED,WRAPPERS_REQUIRED,HAZARDOUS,IS_BILLABLE,IS_LOOKALIKE,IS_SOUNDALIKE,IS_NARCOTIC,IS_HIGHRISK,IS_DRUG,IS_CRITICAL,IS_RESERVE_ANTI_BIOTIC,IS_NON_RETURN_ABLE_ITEM,EXPIRY_NONE_MONTH_DATE,EXPIRY_DAYS,COID_DRY,INTERNAL_EXTERNAL_BOTH,MAINTAIN_EXPIRY,BATCH_NONBATCH,SCARCE_DIFICULTY_EASY,SEASONAL_NON_SEASONAL,VITAL_ESSENTIAL_DESIRABLE,UOM_DESCRIPTION,PACK_CONVERSION_FACTOR,ISSUE_UOM,ISSUE_UOM_DESCRIPTION,MANUFACTURER_UOM,MANUFACTURER_PACK,MINIMUM_ORDER_QUANTITY,BRANCH,DEPARTMENT,NAPII_CODE,SALT_NAME,COMPANY_NAME,VENDOR_ITEM_NAME,MINIMUM_QTY,MAXIMUM_QTY,REORDER_LEVEL,REORDER_QTY,MATERIAL_CLASS_CHARACTERISTICS,PRODUCT_RATE,REGION".Split(',').ToList(); ;
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"Material records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(5000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));

                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    string query = @"INSERT INTO [dbo].[SAP_MATERIAL_DETAILS]([JOB_ID],[MATKL],[WGBEZ],[MATNR],[MTART],[STEUC],[MAKTX],[MEINS],[MEINH],[LVORM],[GENERIC_CODE],
                                        [GENERIC_DESCRIPTION],[DOSAGE],[IS_FORMULARY],[SPECILIZATION],[SPECILIZATION_DESCRIPTION],[MFCD],[MFCD_DIV],[SPECIFICATION],[TEMPERATURE],[SCHEDULE],[SCHEDULECOLOURCODE_COUNTRYCODE],
                                        [IS_ASSET],[BATCH_MANAGEMENT],[RE_USABLE],[RE_USABLE_TIMES],[IMPORTED],[RE_ORDERED],[WRAPPERS_REQUIRED],[HAZARDOUS],[IS_BILLABLE],[IS_LOOKALIKE],[IS_SOUNDALIKE],
                                        [IS_NARCOTIC],[IS_HIGHRISK],[IS_DRUG],[IS_CRITICAL],[IS_RESERVE_ANTI_BIOTIC],[IS_NON_RETURN_ABLE_ITEM],[EXPIRY_NONE_MONTH_DATE],[EXPIRY_DAYS],[COID_DRY],
                                        [INTERNAL_EXTERNAL_BOTH],[MAINTAIN_EXPIRY],[BATCH_NONBATCH],[SCARCE_DIFICULTY_EASY],[SEASONAL_NON_SEASONAL],[VITAL_ESSENTIAL_DESIRABLE],[UOM_DESCRIPTION],
                                        [PACK_CONVERSION_FACTOR],[ISSUE_UOM],[ISSUE_UOM_DESCRIPTION],[MANUFACTURER_UOM],[MANUFACTURER_PACK],[MINIMUM_ORDER_QUANTITY],[BRANCH],[DEPARTMENT],
                                        [NAPII_CODE],[SALT_NAME],[COMPANY_NAME],[VENDOR_ITEM_NAME],[MINIMUM_QTY],[MAXIMUM_QTY],[REORDER_LEVEL],[REORDER_QTY],[MATERIAL_CLASS_CHARACTERISTICS],[DATE_CREATED],[PRODUCT_RATE],[REGION]) VALUES ";

                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;
                                        newRow["MATKL"] = GetDataRowVaue(row, "MATERIALGROUP");
                                        newRow["WGBEZ"] = GetDataRowVaue(row, "MATERIALGROUPDESCRIPTION");
                                        newRow["MATNR"] = GetDataRowVaue(row, "ERPITEMCODE");
                                        newRow["MTART"] = GetDataRowVaue(row, "ITEMNAME");
                                        newRow["STEUC"] = GetDataRowVaue(row, "HSNCODE");
                                        newRow["MAKTX"] = GetDataRowVaue(row, "ITEMNAME");
                                        newRow["MEINS"] = GetDataRowVaue(row, "PURCHASEUOM");
                                        newRow["MEINH"] = GetDataRowVaue(row, "BASEUOM");
                                        newRow["LVORM"] = GetDataRowVaue(row, "DELETIONFLAG");
                                        newRow["GENERIC_CODE"] = GetDataRowVaue(row, "GENERICCODE");
                                        newRow["GENERIC_DESCRIPTION"] = GetDataRowVaue(row, "GENERICDESCRIPTION");
                                        newRow["DOSAGE"] = GetDataRowVaue(row, "DOSAGE");
                                        newRow["IS_FORMULARY"] = GetDataRowVaue(row, "ISFORMULARY");
                                        newRow["SPECILIZATION"] = GetDataRowVaue(row, "SPECILIZATION");
                                        newRow["SPECILIZATION_DESCRIPTION"] = GetDataRowVaue(row, "SPECILIZATION");
                                        newRow["MFCD"] = GetDataRowVaue(row, "MANUFACTURERCODE");
                                        newRow["MFCD_DIV"] = GetDataRowVaue(row, "MANUFACTURERDIVISION");
                                        newRow["SPECIFICATION"] = GetDataRowVaue(row, "SPECIFICATION");
                                        newRow["TEMPERATURE"] = GetDataRowVaue(row, "TEMPERATURE");
                                        newRow["SCHEDULE"] = GetDataRowVaue(row, "SCHEDULE");
                                        newRow["SCHEDULECOLOURCODE_COUNTRYCODE"] = GetDataRowVaue(row, "SCHEDULECOLOURCODE_COUNTRYCODE");
                                        newRow["IS_ASSET"] = GetDataRowVaue(row, "ISASSET");
                                        newRow["BATCH_MANAGEMENT"] = GetDataRowVaue(row, "BATCHMANAGEMENT");
                                        newRow["RE_USABLE"] = GetDataRowVaue(row, "REUSABLE");
                                        newRow["RE_USABLE_TIMES"] = GetDataRowVaue(row, "REUSABLETIMES");
                                        newRow["IMPORTED"] = GetDataRowVaue(row, "IMPORTED");
                                        newRow["RE_ORDERED"] = GetDataRowVaue(row, "REORDERED");
                                        newRow["WRAPPERS_REQUIRED"] = GetDataRowVaue(row, "WRAPPERSREQUIRED");
                                        newRow["HAZARDOUS"] = GetDataRowVaue(row, "HAZARDOUS");
                                        newRow["IS_BILLABLE"] = GetDataRowVaue(row, "ISBILLABLE");
                                        newRow["IS_LOOKALIKE"] = GetDataRowVaue(row, "ISLOOKALIKE");
                                        newRow["IS_SOUNDALIKE"] = GetDataRowVaue(row, "ISSOUNDALIKE");
                                        newRow["IS_NARCOTIC"] = GetDataRowVaue(row, "ISNARCOTIC");
                                        newRow["IS_HIGHRISK"] = GetDataRowVaue(row, "ISHIGHRISK");
                                        newRow["IS_DRUG"] = GetDataRowVaue(row, "ISDRUG");
                                        newRow["IS_CRITICAL"] = GetDataRowVaue(row, "ISCRITICAL");
                                        newRow["IS_RESERVE_ANTI_BIOTIC"] = GetDataRowVaue(row, "ISRESERVEANTIBIOTIC");
                                        newRow["IS_NON_RETURN_ABLE_ITEM"] = GetDataRowVaue(row, "ISNONRETURNABLEITEM");
                                        newRow["EXPIRY_NONE_MONTH_DATE"] = GetDataRowVaue(row, "EXPIRY_NONE_MONTH_DATE");
                                        newRow["EXPIRY_DAYS"] = GetDataRowVaue(row, "EXPIRYDAYS");
                                        newRow["COID_DRY"] = GetDataRowVaue(row, "COID_DRY");
                                        newRow["INTERNAL_EXTERNAL_BOTH"] = GetDataRowVaue(row, "INTERNAL_EXTERNAL_BOTH");
                                        newRow["MAINTAIN_EXPIRY"] = GetDataRowVaue(row, "MAINTAINEXPIRY");// int
                                        newRow["BATCH_NONBATCH"] = GetDataRowVaue(row, "BATCH_NONBATCH");// int
                                        newRow["SCARCE_DIFICULTY_EASY"] = GetDataRowVaue(row, "SCARCE_DIFICULTY_EASY");
                                        newRow["SEASONAL_NON_SEASONAL"] = GetDataRowVaue(row, "SEASONAL_NON_SEASONAL");
                                        newRow["VITAL_ESSENTIAL_DESIRABLE"] = GetDataRowVaue(row, "VITALESSENTIALDESIRABLE");
                                        newRow["UOM_DESCRIPTION"] = GetDataRowVaue(row, "PURCHASEUOMDESCRIPTION");
                                        newRow["PACK_CONVERSION_FACTOR"] = GetDataRowVaue(row, "PACKCONVERSIONFACTOR"); // float
                                        newRow["ISSUE_UOM"] = GetDataRowVaue(row, "ISSUEUOM");
                                        newRow["ISSUE_UOM_DESCRIPTION"] = GetDataRowVaue(row, "ISSUEUOMDESCRIPTION");
                                        newRow["MANUFACTURER_UOM"] = GetDataRowVaue(row, "MANUFACTURERUOM");
                                        newRow["MANUFACTURER_PACK"] = GetDataRowVaue(row, "MANUFACTURERPACK"); // float
                                        newRow["MINIMUM_ORDER_QUANTITY"] = GetDataRowVaue(row, "MINIMUMORDERQUANTITY"); // float
                                        newRow["BRANCH"] = GetDataRowVaue(row, "BRANCH"); // int
                                        newRow["DEPARTMENT"] = GetDataRowVaue(row, "DEPARTMENT"); 
                                        newRow["NAPII_CODE"] = GetDataRowVaue(row, "NAPIICODE"); 
                                        newRow["SALT_NAME"] = GetDataRowVaue(row, "SALTNAME"); 
                                        newRow["COMPANY_NAME"] = GetDataRowVaue(row, "COMPANYNAME"); 
                                        newRow["VENDOR_ITEM_NAME"] = GetDataRowVaue(row, "VENDORITEMNAME"); 
                                        newRow["MINIMUM_QTY"] = GetDataRowVaue(row, "MINIMUMQTY"); // float
                                        newRow["MAXIMUM_QTY"] = GetDataRowVaue(row, "MAXIMUMQTY"); // float
                                        newRow["REORDER_LEVEL"] = GetDataRowVaue(row, "REORDERLEVEL"); // float
                                        newRow["REORDER_QTY"] = GetDataRowVaue(row, "REORDERQTY"); // float
                                        newRow["MATERIAL_CLASS_CHARACTERISTICS"] = GetDataRowVaue(row, "MATERIALCLASSCHARACTERISTICS"); 
                                        newRow["PRODUCT_RATE"] = GetDataRowVaue(row, "FIXED_PRICE"); 
                                        newRow["REGION"] = GetDataRowVaue(row, "REGION"); 
                                        newRow["DATE_CREATED"] = DateTime.UtcNow;

                                        //query += $@"('{guid}',";
                                        //query += $@"'{GetDataRowVaue(row, "MATERIALGROUP")}',";
                                        //query += $@"'{GetDataRowVaue(row, "MATERIALGROUPDESCRIPTION")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ERPITEMCODE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ITEMNAME")}',";
                                        //query += $@"'{GetDataRowVaue(row, "HSNCODE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ITEMNAME")}',";
                                        //query += $@"'{GetDataRowVaue(row, "PURCHASEUOM")}',";
                                        //query += $@"'{GetDataRowVaue(row, "BASEUOM")}',";
                                        //query += $@"'{GetDataRowVaue(row, "DELETIONFLAG")}',";
                                        //query += $@"'{GetDataRowVaue(row, "GENERICCODE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "GENERICDESCRIPTION")}',";
                                        //query += $@"'{GetDataRowVaue(row, "DOSAGE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ISFORMULARY")}',";
                                        //query += $@"'{GetDataRowVaue(row, "SPECILIZATION")}',";
                                        //query += $@"'{GetDataRowVaue(row, "SPECILIZATIONDESCRIPTION")}',";
                                        //query += $@"'{GetDataRowVaue(row, "MANUFACTURERCODE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "MANUFACTURERDIVISION")}',";
                                        //query += $@"'{GetDataRowVaue(row, "SPECIFICATION")}',";
                                        //query += $@"'{GetDataRowVaue(row, "TEMPERATURE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "SCHEDULE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "SCHEDULECOLOURCODE_COUNTRYCODE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ISASSET")}',";
                                        //query += $@"'{GetDataRowVaue(row, "BATCHMANAGEMENT")}',";
                                        //query += $@"'{GetDataRowVaue(row, "REUSABLE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "REUSABLETIMES")}',";
                                        //query += $@"'{GetDataRowVaue(row, "IMPORTED")}',";
                                        //query += $@"'{GetDataRowVaue(row, "REORDERED")}',";
                                        //query += $@"'{GetDataRowVaue(row, "WRAPPERSREQUIRED")}',";
                                        //query += $@"'{GetDataRowVaue(row, "HAZARDOUS")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ISBILLABLE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ISLOOKALIKE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "IS_SOUNDALIKE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ISNARCOTIC")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ISHIGHRISK")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ISDRUG")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ISCRITICAL")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ISRESERVEANTIBIOTIC")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ISNONRETURNABLEITEM")}',";
                                        //query += $@"'{GetDataRowVaue(row, "EXPIRY_NONE_MONTH_DATE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "EXPIRYDAYS")}',";
                                        //query += $@"'{GetDataRowVaue(row, "COID_DRY")}',";
                                        //query += $@"'{GetDataRowVaue(row, "INTERNAL_EXTERNAL_BOTH")}',";
                                        //query += $@"'{GetDataRowVaue(row, "MAINTAINEXPIRY")}',";
                                        //query += $@"'{GetDataRowVaue(row, "BATCH_NONBATCH")}',";
                                        //query += $@"'{GetDataRowVaue(row, "SCARCE_DIFICULTY_EASY")}',";
                                        //query += $@"'{GetDataRowVaue(row, "SEASONAL_NON_SEASONAL")}',";
                                        //query += $@"'{GetDataRowVaue(row, "VITALESSENTIALDESIRABLE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "PURCHASEUOMDESCRIPTION")}',";
                                        //query += $@"'{GetDataRowVaue(row, "PACKCONVERSIONFACTOR")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ISSUEUOM")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ISSUEUOMDESCRIPTION")}',";
                                        //query += $@"'{GetDataRowVaue(row, "MANUFACTURERUOM")}',";
                                        //query += $@"'{GetDataRowVaue(row, "MANUFACTURERPACK")}',";
                                        //query += $@"'{GetDataRowVaue(row, "MINIMUMORDERQUANTITY")}',";
                                        //query += $@"'{GetDataRowVaue(row, "BRANCH")}',";
                                        //query += $@"'{GetDataRowVaue(row, "DEPARTMENT")}',";
                                        //query += $@"'{GetDataRowVaue(row, "NAPIICODE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "SALTNAME")}',";
                                        //query += $@"'{GetDataRowVaue(row, "COMPANYNAME")}',";
                                        //query += $@"'{GetDataRowVaue(row, "VENDORITEMNAME")}',";
                                        //query += $@"'{GetDataRowVaue(row, "MINIMUMQTY")}',";
                                        //query += $@"'{GetDataRowVaue(row, "MAXIMUMQTY")}',";
                                        //query += $@"'{GetDataRowVaue(row, "REORDERLEVEL")}',";
                                        //query += $@"'{GetDataRowVaue(row, "REORDERQTY")}',";
                                        //query += $@"'{GetDataRowVaue(row, "MATERIALCLASSCHARACTERISTICS")}',";
                                        //query += $@"'{GetDataRowVaue(row, "FIXED_PRICE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "REGION")}',";
                                        //query += $@"GETUTCDATE()),";

                                        tempDT.Rows.Add(newRow);
                                    }
                                    query = query.TrimEnd(',');
                                    logger.Debug("END GetMaterialDetails QUERY: " + query);
                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_MATERIAL_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }
                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                             bizClass.SelectList("erp_process_sap_material_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }

        private static void DownloadFtpDirectory(string url, NetworkCredential credentials, string localPath, string jobType)
        {
            FtpWebRequest listRequest = (FtpWebRequest)WebRequest.Create(url);
            listRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            listRequest.Credentials = credentials;

            List<string> lines = new List<string>();

            using (FtpWebResponse listResponse = (FtpWebResponse)listRequest.GetResponse())
            using (Stream listStream = listResponse.GetResponseStream())
            using (StreamReader listReader = new StreamReader(listStream))
            {
                while (!listReader.EndOfStream)
                {
                    lines.Add(listReader.ReadLine());
                }
            }

            foreach (string line in lines)
            {
                try
                {
                    string[] tokens = line.Split(new[] { ' ' }, 9, StringSplitOptions.RemoveEmptyEntries);
                    string name = tokens[3];

                    string localFilePath = Path.Combine(localPath, name);
                    string fileUrl = url + "/" + name;

                    if (tokens[2] == "<DIR>" && false)
                    {
                        if (!Directory.Exists(localFilePath))
                        {
                            Directory.CreateDirectory(localFilePath);
                        }

                        DownloadFtpDirectory(fileUrl + "/", credentials, localFilePath, jobType);
                    }
                    else if (tokens[2] != "<DIR>" && !string.IsNullOrWhiteSpace(name) && name.ToLower().Contains(".csv"))
                    {
                        logger.Info("FILEURL:" + fileUrl);
                        if (fileUrl.ToUpper().Contains(jobType))
                        {
                            FtpWebRequest downloadRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                            downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                            downloadRequest.Credentials = credentials;
                            using (FtpWebResponse downloadResponse = (FtpWebResponse)downloadRequest.GetResponse())
                            using (Stream sourceStream = downloadResponse.GetResponseStream())
                            using (Stream targetStream = File.Create(localFilePath))
                            {
                                byte[] buffer = new byte[10240];
                                int read;
                                while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    targetStream.Write(buffer, 0, read);
                                }
                            }

                            FtpWebRequest fileMoveRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                            fileMoveRequest.UseBinary = true;
                            fileMoveRequest.Method = WebRequestMethods.Ftp.Rename;
                            fileMoveRequest.Credentials = credentials;
                            fileMoveRequest.RenameTo = $"{ConfigurationManager.AppSettings["FTPArchiveFolder"]}{name}";
                            var response = (FtpWebResponse)fileMoveRequest.GetResponse();
                            bool Success = response.StatusCode == FtpStatusCode.CommandOK || response.StatusCode == FtpStatusCode.FileActionOK;
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error($"DOWNLOAD FTP FILE: {line} with ERROR:{ex.Message}, DETAILS: {ex.StackTrace}");
                }
            }
        }

        private static string GetDataRowVaue(DataRow row, string columnName)
        {
            if (row.Table.Columns.Contains(columnName))
            {
                return row[columnName] != DBNull.Value ? row[columnName].ToString() : string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }

        private static string RemoveFirstandLastChar(DataRow row, string columnName)
        {
            if (row.Table.Columns.Contains(columnName))
            {
                string resultString = row[columnName] != DBNull.Value ? row[columnName].ToString() : string.Empty;
                string unQuotedString = "";
                char[] charArr = { '\'', '\"', ',', '"', '-' };
                foreach (char C in charArr)
                {
                    string C1 = Convert.ToString(C);
                    if (resultString.StartsWith(C1))
                    {
                        char C2 = Convert.ToChar(C1);
                        unQuotedString = resultString.TrimStart(C2);

                    }
                    else if (resultString.EndsWith(C1))
                    {
                        char C2 = Convert.ToChar(C1);
                        unQuotedString = resultString.TrimEnd(C2);
                    }
                    else
                    {
                        unQuotedString = resultString;
                    }
                }
                resultString = unQuotedString;

                return resultString;
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
