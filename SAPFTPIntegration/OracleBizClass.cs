﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PRMServices.SQLHelper
{
    public class OracleBizClass : IDatabaseHelper
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        PRMOracleHelper objDAL = new PRMOracleHelper();
        //All Business Method here
        #region ALL Business method here
        public DataSet SelectList(String SP_NAME, SortedDictionary<object, object> sd, int timeout = 0)
        {
            try
            {
                return objDAL.SP_Dataset_return(SP_NAME, timeout, GetSdParameter(sd));
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw ex;
            }
        }

        public DataTable SelectQuery(String query)
        {
            try
            {
                return objDAL.DataTable_return(query);
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw ex;
            }
        }

        public DataSet ExecuteQuery(String query)
        {
            try
            {
                query = cleanMySqlSyntax(query);
                return objDAL.DataSet_return(query);
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw ex;
            }
        }


        // Insert /update and Delete by Query
        public int ExecuteNonQuery_IUD(String Query)
        {
            Query = cleanMySqlSyntax(Query);
            return objDAL.ExecuteNonQuery_IUD(Query);
        }

        public void BulkInsert(DataTable dt, string tableName, List<SqlBulkCopyColumnMapping> columnMappings)
        {
            try
            {
                objDAL.BulkInsert(dt, tableName);
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                throw ex;
            }
        }

        #endregion

        #region Methods Parameter

        /// <summary>
        /// This method Sorted-Dictionary key values to an array of OracleParameters
        /// </summary>
        OracleParameter[] GetSdParameter(SortedDictionary<object, object> sortedDictionary)
        {
            OracleParameter[] paramArray = new OracleParameter[] { };

            foreach (string key in sortedDictionary.Keys)
            {
                AddParameter(ref paramArray, new OracleParameter($"@{key}", sortedDictionary[key]));
            }

            return paramArray;
        }

        void AddParameter(ref OracleParameter[] paramArray, string parameterName, object parameterValue)
        {
            OracleParameter parameter = new OracleParameter(parameterName, parameterValue);

            AddParameter(ref paramArray, parameter);
        }

        void AddParameter(ref OracleParameter[] paramArray, string parameterName, object parameterValue, object parameterNull)
        {
            OracleParameter parameter = new OracleParameter();
            parameter.ParameterName = parameterName;

            if (parameterValue.ToString() == parameterNull.ToString())
                parameter.Value = DBNull.Value;
            else
                parameter.Value = parameterValue;

            AddParameter(ref paramArray, parameter);
        }

        void AddParameter(ref OracleParameter[] paramArray, string parameterName, SqlDbType dbType, object parameterValue)
        {
            OracleParameter parameter = new OracleParameter(parameterName, dbType);
            parameter.Value = parameterValue;

            AddParameter(ref paramArray, parameter);
        }

        void AddParameter(ref OracleParameter[] paramArray, string parameterName, SqlDbType dbType, ParameterDirection direction, object parameterValue)
        {
            OracleParameter parameter = new OracleParameter(parameterName, dbType);
            parameter.Value = parameterValue;
            parameter.Direction = direction;

            AddParameter(ref paramArray, parameter);
        }

        void AddParameter(ref OracleParameter[] paramArray, params OracleParameter[] newParameters)
        {
            OracleParameter[] newArray = Array.CreateInstance(typeof(OracleParameter), paramArray.Length + newParameters.Length) as OracleParameter[];
            paramArray.CopyTo(newArray, 0);
            newParameters.CopyTo(newArray, paramArray.Length);

            paramArray = newArray;
        }

        string cleanMySqlSyntax(string query)
        {
            if (!string.IsNullOrEmpty(query) && query.ToUpper().Contains("LAST_INSERT_ID()"))
            {
                query = query.Replace("LAST_INSERT_ID()", "SCOPE_IDENTITY()");
            }

            if (!string.IsNullOrEmpty(query) && query.ToLower().Contains("utc_timestamp"))
            {
                if (query.ToLower().Contains("utc_timestamp()"))
                {
                    query = query.Replace("utc_timestamp()", "GETUTCDATE()").Replace("UTC_TIMESTAMP()", "GETUTCDATE()");
                }
                else if (query.ToLower().Contains("utc_timestamp"))
                {
                    query = query.Replace("utc_timestamp", "GETUTCDATE()").Replace("UTC_TIMESTAMP", "GETUTCDATE()");
                }
            }

            if (!string.IsNullOrEmpty(query) && query.ToLower().Contains("now()"))
            {
                query = query.Replace("now()", "GETUTCDATE()").Replace("NOW()", "GETUTCDATE()");
            }

            if (!string.IsNullOrEmpty(query) && query.ToLower().Contains("call "))
            {
                query = query.Replace("CALL ", "EXEC ").Replace("call ", "EXEC ").Replace("(", " ").Replace(")", "");
            }

            if (!string.IsNullOrEmpty(query) && query.ToLower().Contains("getcompanyid")) {
                if (query.ToLower().Contains("getcompanyid"))
                {
                    query = query.Replace("getcompanyid", "dbo.getcompanyid");
                }
                //dbo.getcompanyid
            }

            if (!string.IsNullOrEmpty(query) && query.ToLower().Contains("ifnull"))
            {
                query = query.Replace("IFNULL", "ISNULL");
            }

            if (!string.IsNullOrEmpty(query) && query.ToLower().Contains(" || "))
            {
                query = query.Replace(" || ", " OR ");
            }

            if (!string.IsNullOrEmpty(query) && query.ToLower().Contains(" user "))
            {
                query = query.Replace(" User ", " [User] ");
            }

            return query;
        }

        #endregion
    }
}