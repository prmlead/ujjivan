﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using GRID = SendGrid;
using GRID_EMAIL = SendGrid.Helpers.Mail;

namespace SMTP_CONFIGURATION
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            string to = ConfigurationManager.AppSettings["TO"].ToString();
            string subject = ConfigurationManager.AppSettings["SUBJECT"].ToString();
            string body = ConfigurationManager.AppSettings["BODY"].ToString();
            string cc = ConfigurationManager.AppSettings["CC"].ToString();
            logger.Debug("checking for to Email>>>"+ to);
            Console.WriteLine("checking for to Email>>>" + to);
            SendEmail(to, subject, body, cc);
        }

        public static async Task SendEmail(string To, string Subject, string Body, string cc = "")
        {
            string clientType = ConfigurationManager.AppSettings["SMTP_CLIENT_TYPE"].ToString();
            if (!clientType.Equals("SENDGRID", StringComparison.InvariantCultureIgnoreCase))
            {
                await SendSMTPEmail(To, Subject, Body, cc);
            }
            else
            {
                await SendGridEmail(To, Subject, Body, cc);
            }
        }

        public static bool RemoteServerCertificateValidationCallback(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public static async Task SendSMTPEmail(string To, string Subject, string Body, string cc = "")
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(RemoteServerCertificateValidationCallback);
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
            SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["MAILHOST_PORT"]));
            MailMessage mail = new MailMessage();

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072; // .NET 4.0

                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["MAILHOST_SSL_ENABLED"].ToString());
                mail.From = new MailAddress(GetFromAddress(""), GetFromDisplayName(""));
                mail.BodyEncoding = System.Text.Encoding.ASCII;
                List<string> ToAddresses = To.Split(',').ToList<string>();
                foreach (string address in ToAddresses)
                {
                    if (!string.IsNullOrEmpty(address))
                    {
                        mail.To.Add(new MailAddress(address));
                    }
                }

                if (!string.IsNullOrEmpty(cc))
                {
                    List<string> ccAddresses = cc.Split(';').ToList<string>();
                    foreach (string address in ccAddresses)
                    {
                        if (!string.IsNullOrEmpty(address))
                        {
                            mail.CC.Add(new MailAddress(address));
                        }
                    }
                }
                logger.Debug("in send email>>>");
                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                mail.Body = Body;
                mail.Subject = Subject;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                smtpClient.Send(mail);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + (ex.InnerException.Message ?? ""));
                
            }
            finally
            {
                mail = null;
                smtpClient = null;
            }
        }

        public static string GetFromAddress(string FromAdd = null)
        {
            string from = ConfigurationManager.AppSettings["FROMADDRESS"].ToString();
            if (!string.IsNullOrEmpty(FromAdd))
            {
                from = FromAdd;
            }

            if (from.ToLower().Contains("@yahoo"))
            {
                from = ConfigurationManager.AppSettings["FROMADDRESS"].ToString();
            }

            return from;
        }

        public static string GetFromDisplayName(string DisplayName = null)
        {
            string from = ConfigurationManager.AppSettings["FROMDISPLAYNAME"].ToString();
            if (!string.IsNullOrEmpty(DisplayName))
            {
                from = DisplayName;
            }

            return from;
        }

        public static async Task SendGridEmail(string To, string Subject, string Body, string cc = "")
        {
            GRID.SendGridClient sendGridClient = new GRID.SendGridClient(ConfigurationManager.AppSettings["SEND_GRID_KEY"].ToString());
            GRID_EMAIL.SendGridMessage sendGridMessage = new GRID_EMAIL.SendGridMessage();
            List<SendGrid.Helpers.Mail.Attachment> sendGridAttachements = new List<SendGrid.Helpers.Mail.Attachment>();

            try
            {
                    SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString());
                    System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = credentials;
                    smtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["MAILHOST_SSL_ENABLED"].ToString());
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(GetFromAddress(""), GetFromDisplayName(""));
                    sendGridMessage.From = new GRID_EMAIL.EmailAddress(GetFromAddress(""), GetFromDisplayName(""));
                    mail.BodyEncoding = System.Text.Encoding.ASCII;
                    List<string> ToAddresses = To.Split(',').ToList<string>();
                    foreach (string address in ToAddresses)
                    {
                        if (!string.IsNullOrEmpty(address))
                        {
                            mail.To.Add(new MailAddress(address));
                        }
                    }

                    if (!string.IsNullOrEmpty(cc))
                    {
                        List<string> ccAddresses = cc.Split(';').ToList<string>();
                        foreach (string address in ccAddresses)
                        {
                            if (!string.IsNullOrEmpty(address))
                            {
                                mail.CC.Add(new MailAddress(address));
                            }
                        }
                    }

                    mail.Subject = Subject;
                    mail.IsBodyHtml = true;
                    mail.Body = Body;
                    sendGridMessage.Subject = Subject;
                    sendGridMessage.HtmlContent = Body;
                    sendGridMessage.AddTos(mail.To.Distinct().ToList().Select(m => new GRID_EMAIL.EmailAddress(m.Address)).ToList());
                    if (mail.CC != null && mail.CC.Count > 0)
                    {
                        sendGridMessage.AddCcs(mail.CC.Distinct().ToList().Select(m => new GRID_EMAIL.EmailAddress(m.Address)).ToList());
                    }

                    if (sendGridAttachements != null && sendGridAttachements.Count > 0)
                    {
                        sendGridMessage.Attachments = sendGridAttachements;
                    }

                    logger.Info("Send email Before");
                    var sendGridResponse = await sendGridClient.SendEmailAsync(sendGridMessage);
                    logger.Info(sendGridResponse.StatusCode);
                    logger.Info("Send email after");
                
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            finally
            {
                sendGridMessage = null;
                sendGridClient = null;
            }
        }
    }
}
