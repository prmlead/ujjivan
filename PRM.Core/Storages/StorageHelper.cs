﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Configuration;

namespace PRM.Core.Storages
{
   public class StorageHelper
    {
        public StorageHelper()
        {

        }

        public object ConfigurationionManager { get; private set; }

        public string BasePath()
        {
            var appDomain = System.AppDomain.CurrentDomain;
            return appDomain.RelativeSearchPath ?? appDomain.BaseDirectory;
        }

        public string MapPath(string virtualPath)
        {
            return HostingEnvironment.MapPath(virtualPath);
        }

        public string SaveFile(string virtualPath, byte[] stream, string fileName, string extention = "")
        {
           try
            {
                var allowedExtns = ConfigurationManager.AppSettings["SUPPORTED.FILE.EXT"].ToString().Split(',').ToList();
                var isValid = allowedExtns.Any(e => fileName.ToLower().EndsWith(e));
                File.WriteAllBytes(virtualPath, stream); // Requires System.IO
                return fileName;
            }
            catch(Exception ex)
            {
                return "";
            }
        }
    }
}
