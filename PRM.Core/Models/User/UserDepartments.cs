﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRM.Core.Models
{
    [DataContract]
    public class UserDepartments : Entity
    {        
        //DEPT_ID, DEPT_CODE, DEPT_DESC, COMP_ID, CREATED_BY, MODIFIED_BY, DATE_CREATED, DATE_MODIFIED 

        [DataMember(Name = "userDeptID")]
        public int UserDeptID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }
        
        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "companyDepartments")]
        public CompanyDepartments CompanyDepartments { get; set; }

        [DataMember(Name = "createdBy")]
        public int CreatedBy { get; set; }

        [DataMember(Name = "user")]
        public User User { get; set; }

    }
}