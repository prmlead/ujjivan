﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class User : Entity
    {
        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        string firstName = string.Empty;
        [DataMember(Name = "firstName")]
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    firstName = value;
                }
            }
        }

        string lastName = string.Empty;
        [DataMember(Name = "lastName")]
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    lastName = value;
                }
            }
        }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        string phoneNum = string.Empty;
        [DataMember(Name = "phoneNum")]
        public string PhoneNum
        {
            get
            {
                return phoneNum;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    phoneNum = value;
                }
            }
        }

        [DataMember(Name = "altEmail")]
        public string AltEmail { get; set; }

        [DataMember(Name = "altPhoneNum")]
        public string AltPhoneNum { get; set; }

        [DataMember(Name = "userInfo")]
        public UserInfo UserInfo { get; set; }
    }
}