﻿
using System;
using System.Runtime.Serialization;
namespace PRM.Core.Models
{
    [DataContract]
    public class POItems : ResponseAudit
    {
        [DataMember(Name = "reqID")]
        public int ReqID { get; set; }

        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

        string productIDorName = string.Empty;
        [DataMember(Name = "productIDorName")]
        public string ProductIDorName
        {
            get
            {
                if (!string.IsNullOrEmpty(productIDorName))
                { return productIDorName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productIDorName = value; }
            }
        }

        [DataMember(Name = "poID")]
        public int POID { get; set; }

        string purchaseID = string.Empty;
        [DataMember(Name = "purchaseID")]
        public string PurchaseID
        {
            get
            {
                if (!string.IsNullOrEmpty(purchaseID))
                { return purchaseID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { purchaseID = value; }
            }
        }

        string indentID = string.Empty;
        [DataMember(Name = "indentID")]
        public string IndentID
        {
            get
            {
                if (!string.IsNullOrEmpty(indentID))
                { return indentID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { indentID = value; }
            }
        }

        [DataMember(Name = "reqQuantity")]
        public decimal ReqQuantity { get; set; }

        [DataMember(Name = "poQuantity")]
        public decimal POQuantity { get; set; }

        [DataMember(Name = "vendorPOQuantity")]
        public decimal VendorPOQuantity { get; set; }

        DateTime expectedDeliveryDate = DateTime.MaxValue;
        [DataMember(Name = "expectedDeliveryDate")]
        public DateTime? ExpectedDeliveryDate
        {
            get
            {
                return expectedDeliveryDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    expectedDeliveryDate = value.Value;
                }
            }
        }

        string deliveryAddress = string.Empty;
        [DataMember(Name = "deliveryAddress")]
        public string DeliveryAddress
        {
            get
            {
                if (!string.IsNullOrEmpty(deliveryAddress))
                { return deliveryAddress; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { deliveryAddress = value; }
            }
        }

        [DataMember(Name = "vendorPrice")]
        public decimal VendorPrice { get; set; }

        [DataMember(Name = "poPrice")]
        public decimal POPrice { get; set; }

        string comments = string.Empty;
        [DataMember(Name = "comments")]
        public string Comments
        {
            get
            {
                if (!string.IsNullOrEmpty(comments))
                { return comments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { comments = value; }
            }
        }

        string status = string.Empty;
        [DataMember(Name = "status")]
        public string Status
        {
            get
            {
                if (!string.IsNullOrEmpty(status))
                { return status; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { status = value; }
            }
        }

        string poLink = string.Empty;
        [DataMember(Name = "poLink")]
        public string POLink
        {
            get
            {
                if (!string.IsNullOrEmpty(poLink))
                { return poLink; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { poLink = value; }
            }
        }

        [DataMember(Name = "cGst")]
        public decimal CGst { get; set; }

        [DataMember(Name = "sGst")]
        public decimal SGst { get; set; }

        [DataMember(Name = "iGst")]
        public decimal IGst { get; set; }

        [DataMember(Name = "vendorTotalPrice")]
        public decimal VendorTotalPrice { get; set; }

        [DataMember(Name = "poTotalPrice")]
        public decimal PoTotalPrice { get; set; }

        [DataMember(Name = "dispatchQuantity")]
        public decimal DispatchQuantity { get; set; }

        [DataMember(Name = "dTID")]
        public int DTID { get; set; }

        [DataMember(Name = "recivedQuantity")]
        public decimal RecivedQuantity { get; set; }

        [DataMember(Name = "returnQuantity")]
        public decimal ReturnQuantity { get; set; }



        [DataMember(Name = "sumDispatchQuantity")]
        public decimal SumDispatchQuantity { get; set; }

        [DataMember(Name = "sumRecivedQuantity")]
        public decimal SumRecivedQuantity { get; set; }

        [DataMember(Name = "sumReturnQuantity")]
        public decimal SumReturnQuantity { get; set; }
    }
}