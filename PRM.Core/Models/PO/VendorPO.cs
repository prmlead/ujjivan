﻿
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class VendorPO : ResponseAudit
    {
        [DataMember(Name = "vendor")]
        public UserDetails Vendor { get; set; }

        [DataMember(Name = "poFile")]
        public FileUpload POFile { get; set; }

        [DataMember(Name = "req")]
        public Requirement Req { get; set; }

        [DataMember(Name = "listPOItems")]
        public List<POItems> ListPOItems { get; set; }

        string poLink = string.Empty;
        [DataMember(Name = "poLink")]
        public string POLink
        {
            get
            {
                if (!string.IsNullOrEmpty(poLink))
                { return poLink; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { poLink = value; }
            }
        }

        [DataMember(Name = "common")]
        public bool Common { get; set; }

        
    }
}