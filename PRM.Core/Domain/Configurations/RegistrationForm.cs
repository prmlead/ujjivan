﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.Configurations
{
    [Table("config_registrationform")]
    public class RegistrationForm : BaseEntity
    {

        public override int Id { get; set; }

        public int Company_Id { get; set; }

        /// <summary>
        /// Field Name
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Display name
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Field Required
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// Field type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Field score
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// Is system field
        /// </summary>
        public bool SystemField { get; set; }


        /// <summary>
        /// Display order
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Allow attachement
        /// </summary>
        public bool AllowAttachment { get; set; }

        [Dapper.IgnoreUpdate]
        public DateTime? CreatedOnUtc { get; set; }

        public DateTime? UpdatedOnUtc { get; set; }

        [Dapper.IgnoreUpdate]
        public int CreatedBy { get; set; }

        public int UpdatedBy { get; set; }
        public bool Deleted { get; set; }
    }
}
