﻿using System;
using System.Collections.Generic;
using System.Linq;
using PRM.Core.Domain.RealTimePrices;
using PRM.Core.Models;
using PRM.Core.Pagers;
using PRM.Services.RealTimePrices;
using PRMServices.Models.RealTimePrice;
using PRMServices.Models.Common;
using PRM.Core.Storages;
using System.ServiceModel.Activation;
using PRM.Services.Common;
using PRMServices.Infrastructure.Mapper;
using Newtonsoft.Json;
using PRM.Core;
using System.Threading.Tasks;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PRMRealTimePriceService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PRMRealTimePriceService.svc or PRMRealTimePriceService.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMRealTimePriceService : IPRMRealTimePriceService
    {
        public PRMRealTimePriceService(IRealTimePriceService realTimePriceService,
            IExcelService excelService,
            IWorkContext workContext,
            IRealTimePriceWorkflowMessage workflowMessageService
            )
        {
            _realTimePriceService = realTimePriceService;
            _excelService = excelService;
            _workContext = workContext;
            _workflowMessageService = workflowMessageService;


        }

        private readonly IRealTimePriceService _realTimePriceService;
        private readonly IExcelService _excelService;
        private readonly IWorkContext _workContext;
        private readonly IRealTimePriceWorkflowMessage _workflowMessageService;
        public RealTimeFilterData GetCategories(string typeId)
        {
            var data = new RealTimeFilterData();

            data.Categories = _realTimePriceService.GetCategories(Convert.ToInt32(typeId));
            data.Markets = _realTimePriceService.GetMarkets(Convert.ToInt32(typeId));
            data.Products = _realTimePriceService.GetProducts(Convert.ToInt32(typeId));
            return data;
        }

        public dynamic GetPrices(Pager pager)
        {

           var lst = _realTimePriceService.GetPriceList(pager);


            return JsonConvert.SerializeObject(new PagedResponse() { Data = lst, HasNextPage = lst.HasNextPage, HasPreviousPage = lst.HasPreviousPage, PageIndex = lst.PageIndex, PageSize = lst.PageSize, TotalPages = lst.TotalPages,TotalCount = lst.TotalCount });
        }

        public Response UploadPrices(RealTimePriceUploadModel model)
        {
            var _storageHelper = new StorageHelper();
            var lst = _excelService.To<ExcelRealTimePrice>("Touelne three month data", model.File.FileStream).ToEntity();
            _realTimePriceService.InsertPrice(lst.Select(e=>{
                e.Currency = "INR";
                e.TypeId = model.TypeId == 0 ? 1: model.TypeId;
                return e;
            }).ToList());
            return new Response() { Message = "Uploaded successfully" };
        }

        public IList<object[]> GetReport(RealTimeReportPostModel model)
        {

            var list = _realTimePriceService.GetPriceReport(model.TypeId,model.Product, model.Market, model.FromDate , model.ToDate);

            var data = list.Select(e =>
            {
                IList<object> ls = new List<object>();
                ls.Add(e.PriceDate.Value);
                ls.Add(e.Price);
                return ls.ToArray();
            });


            return data.ToList();
        }

        public RealTimePriceSettingModel GetRealTimePriceSettings()
        {

            var settings = _realTimePriceService.GetRealTimeSettingByCompany(_workContext.CurrentCompany);

            var _settingsModel = new RealTimePriceSettingModel();

            var products = _realTimePriceService.GetProducts().Select(e =>
            {

                return new RealTimePriceAllowProductModel() { Active = false, Product = e, ThresholdValue = 0 };

            }).ToList();

            if (settings != null)
            {

                if (!string.IsNullOrEmpty(settings.AllowedProducts))
                {

                    var _allowrdProductList = JsonConvert.DeserializeObject<List<RealTimeSelectedProduct>>(settings.AllowedProducts);

                   foreach( var e in products)
                    {
                        if(_allowrdProductList.Any(a => a.Product == e.Product))
                        {
                            var prodtc = _allowrdProductList.Where(a => a.Product == e.Product).FirstOrDefault();
                            e.Active = true;
                            e.ThresholdValue = prodtc.ThresholdValue;
                        }
                    };

                }
            }
            _settingsModel.AllowedProducts = products.ToList();
            _settingsModel.AllowThresholdNotification = settings.AllowThresholdNotification;
            _settingsModel.AllowPriceDipNotification = settings.AllowPriceDipNotification;
            return _settingsModel;



        }

        public Response SaveRealTimePriceSettings(RealTimePriceSettingModel model)
        {
            var settings = _realTimePriceService.GetRealTimeSettingByCompany(_workContext.CurrentCompany);

            if(settings != null)
            {
                settings.AllowedProducts = JsonConvert.SerializeObject(model.AllowedProducts);

                settings.AllowPriceDipNotification = model.AllowPriceDipNotification;
                settings.AllowThresholdNotification = model.AllowThresholdNotification;
                _realTimePriceService.UpdateRealTimeSetting(settings);

            }
            else
            {
                var _setting = new RealTimePriceSetting();
                _setting.Company_Id = _workContext.CurrentCompany;
                _setting.AllowedProducts = JsonConvert.SerializeObject(model.AllowedProducts);
                _setting.AllowPriceDipNotification = model.AllowPriceDipNotification;
                _setting.AllowThresholdNotification = model.AllowThresholdNotification;
                _realTimePriceService.InsertRealTimeSetting(_setting);

            }

            return new Response() { Message = "Settings Saved" };
        }


        public Response SendNotifications()
        {


            var times = _realTimePriceService.GetPriceList(new { TypeId = 1 }).Take(10);

            var settings = _realTimePriceService.GetRealTimeSettingByCompany(_workContext.CurrentCompany);

            if(settings != null)
            {
                var _allowrdProductList = JsonConvert.DeserializeObject<List<RealTimeSelectedProduct>>(settings.AllowedProducts);


                if(_allowrdProductList != null && _allowrdProductList.Any())
                {
                    var list = _realTimePriceService.GetPriceDrop(_allowrdProductList.Select(e => e.Product).ToArray(), DateTime.Now, DateTime.Now.AddDays(-1));

                    var thersholdError = _realTimePriceService.GetPriceThershold(_allowrdProductList.Select(e => e.Product).ToArray(), DateTime.Now);

                    thersholdError = thersholdError.Where(e => _allowrdProductList.Any(pa => pa.Product == e.Product) && e.Price <= _allowrdProductList.Where(pa => pa.Product == e.Product).FirstOrDefault().ThresholdValue).ToList();

                    var users = _realTimePriceService.GetCompanyUser(_workContext.CurrentCompany);

                    foreach (var user in users)
                    {
                        var task = Task.Run(() =>
                        {
                            if (list != null && list.Any() && settings.AllowPriceDipNotification)
                                _workflowMessageService.SendMailOnProductPriceDrop(user.Email, user.Name, list, DateTime.Now);

                            if (thersholdError != null && thersholdError.Any() && settings.AllowThresholdNotification)
                                _workflowMessageService.SendMessageOnPriceReachedThershlod(user.Email, user.Name, thersholdError, DateTime.Now, _allowrdProductList);
                        });

                    }
                }



            }




            return new Response() { Message = "Notification send process started" };

        }
    }
}
