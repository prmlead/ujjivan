﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMFwdService
    {
        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrunningauctions?userid={userID}&sessionid={sessionID}&section={section}&limit={limit}")]
        List<RunningAuction> GetAuctions(int userID, string sessionID, string section, int limit);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcurrentauctions?userid={userID}&sessionid={sessionID}&section={section}&limit={limit}")]
        List<Requirement> GetRunningAuctions(int userID, string sessionID, string section, int limit);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementdata?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        Requirement GetRequirementData(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpodetails?reqid={reqID}&userid={userID}&vendorid={vendorID}&sessionid={sessionID}")]
        RequirementPO GetPODetails(int reqID, int userID, int vendorID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserinfo?userid={userID}&sessionid={sessionID}")]
        UserInfo GetUser(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsuperuserdetails?userid={userID}&sessionid={sessionID}")]
        UserInfo GetSuperUser(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpricecomparison?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        PriceComparison GetPriceComparison(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpricecomparisonprenegotiation?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        PriceComparison GetPriceComparisonPreNegotiation(int reqID, int userID, string sessionID);
        
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "generatepoforuser")]
        Response UpdatePOData(RequirementPO reqPO);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "selectvendor")]
        Response SelectVendor(int userID, int vendorID, int reqID, string reason, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "stopbids")]
        Response StopBids(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "endnegotiation")]
        Response EndNegotiation(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getattachment?attachid={attachmentID}&sessionid={sessionID}")]
        Response GetAttachment(string attachmentID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getattachmentbase64?attachid={attachmentID}&sessionid={sessionID}")]
        FileData GetAttachmentBase64(string attachmentID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "materialdispatch")]
        Response MaterialDispatch(ReqMaterialDetails reqMat);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmyauctions?userid={userID}&sessionid={sessionID}")]
        List<Requirement> GetMyAuctions(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsubuserdata?userid={userID}&sessionid={sessionID}")]
        List<UserInfo> GetSubUserData(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getactiveleads?userid={userID}&sessionid={sessionID}")]
        List<Requirement> GetActiveLeads(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "paymentdetails")]
        Response PaymentDetails(PaymentDetails paymentDets);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcomments?reqid={reqID}&userID={userID}&sessionid={sessionID}")]
        List<Comment> GetComments(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdetails?userid={userID}&sessionid={sessionID}")]
        UserDetails GetUserDetails(int userID, string sessionID);

        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "register")]
        Response Register(Register register);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcategories?userid={userID}")]
        List<CategoryObj> GetCategories(int userID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getusersubcategories?userid={userID}&sessionid={sessionID}")]
        List<CategoryObj> GetSubCategories(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getusercategories?userid={userID}&sessionid={sessionID}")]
        List<CategoryObj> GetUserCategories(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserssubcategories?userid={userID}&sessionid={sessionID}")]
        List<CategoryObj> GetUserSubCategories(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanytypes")]
        List<string> GetCompanyTypes();

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getusercredentials?userid={userID}&sessionid={sessionID}")]
        List<Credentials> GetUserCredentials(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdashboardstats?userid={userID}&sessionid={sessionID}")]
        DashboardStats GetDashboardStats(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdate")]
        DateTime GetDate();

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "issessionvalid?sessionid={sessionID}")]
        Response IsSessionValid(string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatestatus")]
        Response UpdateStatus(int reqid, int userid, string status, string type, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "generatepo")]
        Response GeneratePO(int reqid, int userid, byte[] POfile, string POfileName, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "updateauctionstart")]
        Response UpdateAuctionStart(int reqID, int userID, DateTime date, string sessionID, string negotiationDuretion, NegotiationSettings NegotiationSettings);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "checkuserifexists")]
        bool CheckUserIfExists(string phone, string idtype);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "isnegotiationrunning")]
        bool IsNegotationRunning(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "loginuser")]
        Response LoginUser(string username, string password);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "logoutuser")]
        Response LogoutUser(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatebidtime")]
        Response UpdateBidTime(int reqID, int userID, long newTicks, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "requirementsave")]
        Response RequirementSave(Requirement requirement, byte[] attachment);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "registeruser")]
        Response RegisterUser(UserInfo userInfo);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "addvendortoauction")]
        Response AddVendorToAuction(VendorDetails vendor, int reqID, string sessionID, int userID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "makeabid")]
        Response MakeABid(int reqID, int userID, double price, byte[] quotation, string quotationName, string sessionID,
            double tax, double freightcharges, string warranty, string payment, string duration, string validity,
            List<RequirementItems> quotationObject, int revised, double priceWithoutTax, string type, double discountAmount,
            List<RequirementTaxes> listRequirementTaxes, string otherProperties);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "verifyotp")]
        Response VerifyOTP(int OTP, int userID, string phone, int isMobile);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "verifyemailotp")]
        Response VerifyEmailOTP(int OTP, int userID, string email);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "addnewvendor")]
        UserInfo AddNewVendor(VendorInfo vendorInfo);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "addnewcustomer")]
        Response AddNewCustomer(VendorInfo customerInfo);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getvendors")]
        List<VendorDetails> GetVendors(string[] Categories, string sessionID, int uID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getvendorsbycatnsubcat")]
        List<VendorDetails> GetVendorsByCatNSubCat(int[] Categories, string sessionID, int count, int uID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savecomment")]
        Response SaveComment(Comment com);
        // TODO: Add your service operations here

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "forgotpassword")]
        Response ForgotPassword(string email, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "changepassword")]
        Response ChangePassword(int userID, string username, string oldPass, string newPass);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "resetpassword")]
        Response resetpassword(string email, string sessionid, string NewPass, string ConfNewPass);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateuserinfo")]
        Response UpdateUserInfo(UserDetails user);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateuserbasicinfo")]
        Response UpdateUserBasicInfo(UserDetails user);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateuserprofileinfo")]
        Response UpdateUserProfileInfo(UserDetails user);
        
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendotpforemail")]
        Response SendOTPForEmail(int userID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendcontactemail")]
        Response SendContactEmail(string Name, string Email, string Mobile, string Comment, string Industry);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendotp")]
        Response SendOTP(int userID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendotpformobile")]
        Response SendOTPforMobile(string phone);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatecredentials")]
        Response UpdateCredentials(int userID, List<CredentialUpload> files, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleterequirement")]
        Response DeleteRequirement(int reqID, int userID, string sessionID, string reason);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleteuser")]
        Response DeleteUser(int userID, int referringUserID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "activateuser")]
        Response ActivateUser(int userID, int referringUserID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "userratings")]
        Response UserRatings(int uID, int userID, float rating, string sessionID);

        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "verifydocuments")]
        Response VerifyDocuments(string phone, List<CredentialUpload> files, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DocumentVerification?phone={phone}")]
        Response DocumentVerification(string phone);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteLogin?phone={phone}")]
        Response DeleteLogin(string phone);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "TestGetOtp?phone={phone}")]
        Response TestGetOtp(string phone);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleteattachment")]
        Response DeleteAttachment(int userID, int reqID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateexpdelandpaydate")]
        Response UpadteExpDelAndPayDate(int reqid, DateTime date, string type, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "makeabidformobile")]
        Response MakeABidForMobile(int reqID, int userID, double price, string strQuotation, string quotationName, string sessionID, double tax);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "startNegotiation")]
        Response StartNegotiation(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "restartnegotiation")]
        Response RestartNegotiation(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deletevendorfromauction")]
        Response DeleteVendorFromAuction(int userID, int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "testverifyuser?phone={phone}")]
        Response testVerifyUser(string phone);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getkeyvaluepairs?parameter={parameter}")]
        KeyValuePair[] GetKeyValuePairs(string parameter);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getbidhistory?reqid={reqID}&userID={userID}&sessionid={sessionID}")]
        List<Comment> GetBidHistory(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "appversion?uid={uID}&appversioncode={appVersionCode}")]
        AppInfo AppVersion(int uID, double appVersionCode);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "isnegotiationended?reqid={reqID}&sessionid={sessionID}")]
        Response IsNegotiationEnded(int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "saveuserphone")]
        Response SaveUserPhone(int uid, string model, string os, string token, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "uploadquotation")]
        Response UploadQuotation(List<RequirementItems> quotationObject, int userID, int reqID, string sessionID, double price,
            double tax, double freightcharges, double vendorBidPrice, string warranty, string payment, string duration,
            string validity, int revised, string DesfileName, double discountAmount, List<RequirementTaxes> listRequirementTaxes,
            string otherProperties);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "uploadquotationsfromexcel")]
        Response UploadQuotationsFromExcel(int reqID, int userID, string sessionID, byte[] quotationAttachment);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendtelegrammsg")]
        TelegramMsg SendTelegramMsg(TelegramMsg telegrammsg);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendpushnotification")]
        Response SendPushNotification(PushNotifications push);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "revquotationupload")]
        Response RevQuotationUpload(int reqID, int userID, double price, byte[] quotation, string quotationName, string sessionID, double tax);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "revquotationuploadformobile")]
        Response RevQuotationUploadForMobile(int reqID, int userID, double price, string strQuotation, string quotationName, string sessionID, double tax);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementreminders")]
        List<RequirementReminders> GetRequirementReminders();

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getquotationreminders")]
        List<RequirementReminders> GetQuotationReminders();
            
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "quatationaprovel")]
        Response QuatationAprovel(int reqID, int customerID, int vendorID, bool value, string reason, string sessionID, string action);

        //[AuthInterceptor][OperationContract]
        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "assignvendorstocustomer?phone={phone}")]
        //Response AssignVendorsToCustomer(string phone);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyvendors?userid={userID}&sessionid={sessionID}")]
        List<UserDetails> GetCompanyVendors(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "itemwiseselectvendor")]
        Response ItemWiseSelectVendor(int userID, int vendorID, int reqID, int itemID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyleads?userid={userID}&searchstring={searchString}&searchtype={searchType}&sessionid={sessionID}")]
        List<Requirement> GetCompanyLeads(int userID, string searchString, string searchType, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savenegotiationsettings")]
        NegotiationSettings SaveNegotiationSettings(NegotiationSettings NegotiationSettings);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "vendorreminders")]
        Response VendorReminders(int reqID, int userID, string message, int[] vendorIDs, string[] vendorCompanyNames, string sessionID, string requestType);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorreminders?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        List<Reminders> GetVendorReminders(int reqID, int userID, string sessionID);

        //[AuthInterceptor][OperationContract]
        //[WebInvoke(Method = "POST",
        //BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //RequestFormat = WebMessageFormat.Json,
        //ResponseFormat = WebMessageFormat.Json,
        //UriTemplate = "generatenewquotation")]
        //Response GenerateNewQuotation(int reqID, int customerID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "activatecompanyvendor")]
        Response ActivateCompanyVendor(int customerID, int vendorID, int isValid, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreportsrequirement?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        Requirement GetReportsRequirement(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "importentity")]
        Response ImportEntity(ImportEntity entity);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savepricecap")]
        Response SavePriceCap(int reqID, string sessionID, int userID, string reqType, double priceCapValue, int isUnitPriceBidding);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuseraccess?userid={userID}&sessionid={sessionID}")]
        List<UserAccess> GetUserAccess(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveuseraccess")]
        Response SaveUserAccess(List<UserAccess> listUserAccess, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanydepartments?userid={userID}&sessionid={sessionID}")]
        List<CompanyDepartments> GetCompanyDepartments(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanydepartment")]
        Response SaveCompanyDepartment(CompanyDepartments companyDepartment);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "deletedepartment")]
        Response DeleteDepartment(int deptID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveuserdepartments")]
        Response SaveUserDepartments(List<UserDepartments> listUserDepartments, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdepartments?userid={userID}&sessionid={sessionID}")]
        List<UserDepartments> GetUserDepartments(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdepartmentusers?deptid={deptID}&userid={userID}&sessionid={sessionID}")]
        List<UserDepartments> GetDepartmentUsers(int deptID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savereqdepartments")]
        Response SaveReqDepartments(List<ReqDepartments> listReqDepartments, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdepartments?userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        List<ReqDepartments> GetReqDepartments(int userID, int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getisauthorized?userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        Response GetIsAuthorized(int userID, int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanyconfiguration")]
        Response SaveCompanyConfiguration(List<CompanyConfiguration> listCompanyConfiguration, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyconfiguration?compid={compID}&configkey={configKey}&sessionid={sessionID}")]
        List<CompanyConfiguration> GetCompanyConfiguration(int compID, string configKey, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "reqtechsupport")]
        Response ReqTechSupport(ReqShare reqShare);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saverunningitemprice")]
        Response SaveRunningItemPrice(List<RequirementItems> itemsList, int userID, int reqID, double price, double vendorBidPrice, double freightcharges);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "assignvendortocompany")]
        User AssignVendorToCompany(int userID, string vendorPhone, string vendorEmail, string category, string[] subCategory, string sessionID, string altPhoneNum, string altEmail);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savemulticontacts")]
        Response SaveMultiContacts(int userID, string phone, string email, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmaterialreceiveddata?reqid={reqID}&sessionid={sessionID}")]
        MaterialReceived GetMaterialReceivedData(int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "materialreceived")]
        Response MaterialReceived(MaterialReceived materialreceived);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanycategories")]
        Response SaveCompanyCategories(int compID, int id, string category, string subcategory, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanycategories?compid={compID}&sessionid={sessionID}")]
        List<CategoryObj> GetCompanyCategories(int compID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savealternatecontacts")]
        Response SaveAlternateContacts(UserDetails user);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getalternatecontacts?userid={userID}&compid={compID}&sessionid={sessionID}")]
        List<UserDetails> GetAlternateContacts(int userID, int compID, string sessionID);

    }
}
