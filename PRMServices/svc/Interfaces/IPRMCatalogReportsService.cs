﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMCatalogReportsService
    {

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getuserdepartments?u_id={U_ID}&is_super_user={IS_SUPER_USER}&sessionid={sessionid}")]
        List<cr_UserDepartments> GetUserDepartments(int U_ID, int IS_SUPER_USER, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "getdepartmentcategories")]
        List<cr_DepartmentCategories> GetDepartmentCategories(int U_ID, string DEPT_IDS, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "getcategorysubcategories")]
        List<cr_DepartmentCategories> GetCategorySubcategories(int U_ID, string CAT_IDS, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "getcategoryproducts")]
        List<cr_CategoryProducts> GetCategoryProducts(int U_ID, string CAT_IDS, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getproductanalysis?u_id={U_ID}&prod_id={PROD_ID}&sessionid={sessionid}&call_code={call_code}")]
        cr_CategoryProducts GetProductAnalysis(int U_ID, int PROD_ID, string sessionid, int call_code);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getvendorsandorders?u_id={U_ID}&prod_id={PROD_ID}&sessionid={sessionid}")]
        List<cr_VendorsAndOrders> GetVendorsAndOrders(int U_ID, int PROD_ID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getmostordersandvendors?u_id={U_ID}&prod_id={PROD_ID}&sessionid={sessionid}")]
        List<cr_VendorsAndOrders> GetMostOrdersAndVendors(int U_ID, int PROD_ID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getproductrequirements?u_id={U_ID}&prod_id={PROD_ID}&sessionid={sessionid}")]
        List<cr_VendorsAndOrders> GetProductRequirements(int U_ID, int PROD_ID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getproductpricehistory?u_id={U_ID}&prod_id={PROD_ID}&sessionid={sessionid}")]
        List<cr_VendorsAndOrders> GetProductPriceHistory(int U_ID, int PROD_ID, string sessionid);
    }
}
