﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;
//using PRM.Core.Models;
//using PRMServices.Models;
using PRMServices.Models.Catalog;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPRMRealTimePriceService" in both code and config file together.
    [ServiceContract]
    public interface IPRMOpsService
    {
        #region


        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetClients?sessionid={sessionID}&from={from}&to={to}")]
        List<OpsClients> GetClients(string sessionID, string from, string to);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetClientsRequirements?sessionid={sessionID}&domain={domain}&from={from}&to={to}&clientid={ClientID}&auctionType={auctionType}")]
        List<OpsRequirement> GetClientsRequirements(string sessionID, string domain, string from, string to, int ClientID, int auctionType);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetClientsVendors?reqid={REQ_ID}&sessionid={sessionID}&domain={domain}&clientid={ClientID}&auctionType={auctionType}")]
        List<OpsRequirementVendors> GetClientsVendors(int REQ_ID, string sessionID, string domain, int ClientID, int auctionType);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetPassword?type={type}&field={field}&key={key}&domain={domain}")]
        Response GetPassword(string type, string field, string key, string domain);



        //[AuthInterceptor][OperationContract]
        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVendorsOnline?reqid={REQ_ID}&sessionid={sessionID}&userid={userID}&domain={domain}")]
        //Response GetVendorsOnline(int REQ_ID, string sessionID, int userID , string domain );

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVendorsOnline?reqid={REQ_ID}&sessionid={sessionID}&userid={userID}&domain={domain}&auctionType={auctionType}")]
        List<OpsRequirementVendors> GetVendorsOnline(int REQ_ID, string sessionID, int userid, string domain, int auctionType);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetPendingQuotations?reqid={REQ_ID}&clientid={ClientID}&domain={domain}&auctionType={auctionType}&sessionid={sessionID}")]
        List<OpsRequirementVendors> GetPendingQuotations(int REQ_ID, int ClientID, string domain, int auctionType, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAllQuotationsVendors?reqid={REQ_ID}&clientid={ClientID}&domain={domain}&auctionType={auctionType}&sessionid={sessionID}")]
        List<OpsRequirementVendors> GetAllQuotationsVendors(int REQ_ID, int ClientID, string domain, int auctionType, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetReminders?reqid={REQ_ID}&domain={domain}&clientid={ClientID}&sessionid={sessionID}")]
        PRMServices GetReminders(int REQ_ID, string domain, int ClientID,  string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVendorDetails?vendorID={vendorID}&domain={domain}&sessionid={sessionid}")]
        OpsRequirementVendors GetVendorDetails(int vendorID, string domain, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getDomain?domain={domain}")]
        string GetDomain(string domain);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendotp")]
        Response SendOTP(int userID,string domain, string phone,int issmsverified,int isemailverified,string email);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
ResponseFormat = WebMessageFormat.Json,
BodyStyle = WebMessageBodyStyle.WrappedRequest,
UriTemplate = "changepassword")]
        Response ChangePassword(int userID, string username, string oldPass, string newPass, string domain);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "SendPassword")]
        Response SendPassword(int userID, string domain, string phone, string email);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetClientDetails?sessionID={sessionID}&domain={domain}")]
        OpsClients GetClientDetails(string sessionID, string domain);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetReportsTemplates?name={name}&domain={domain}&from={from}&to={to}&sessionID={sessionID}")]
        string GetReportsTemplates(string name, string domain, string from, string to, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVendorNeedTraining?sessionid={sessionID}&domain={domain}&from={from}&to={to}&clientid={ClientID}&auctionType={auctionType}")]
        List<OpsRequirementVendors> GetVendorNeedTraining(string sessionID, string domain, string from, string to, int ClientID, int auctionType);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetReports?domain={domain}&from={from}&to={to}&sessionID={sessionID}")]
        OpsReports GetReports(string domain, string from, string to, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
         ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest,
         UriTemplate = "SaveVendorTraining")]
        Response SaveVendorTraining(OpsVendorTraining vendTrain, string domain, int clientID, int isUpdate);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVendorTrainingHistory?vtID={vtID}&reqID={reqID}&vendorID={vendorID}&domain={domain}")]
        List<OpsVendorTraining> GetVendorTrainingHistory(int vtID, int reqID, int vendorID, string domain);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetALLVendorTrainingDetails?sessionID={sessionID}&domain={domain}&from={from}&to={to}")]
        List<OpsVendorTraining> GetALLVendorTrainingDetails(string sessionID, string domain, string from, string to);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetRegularRfqReport?domain={domain}&from={from}&to={to}&sessionID={sessionID}")]
        List<OpsRequirement> GetRegularRfqReport(string domain, string from, string to, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetLogisticsRfqReport?domain={domain}&from={from}&to={to}&sessionID={sessionID}")]
        List<OpsRequirement> GetLogisticsRfqReport(string domain, string from, string to, string sessionID);


        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetSubUserDataReport?domain={domain}&from={from}&to={to}&sessionID={sessionID}")]
        List<OpsUserDetails> GetSubUserDataReport(string domain, string from, string to, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVendorRegisteredReport?domain={domain}&from={from}&to={to}&sessionID={sessionID}")]
        List<OpsUserDetails> GetVendorRegisteredReport(string domain, string from, string to, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVendorRfqData?domain={domain}&from={from}&to={to}&email={email}&phone={phone}&sessionID={sessionID}")]
        List<OpsRequirement> GetVendorRfqData(string domain, string from, string to, string email, string phone, string sessionID);
    }
}
#endregion
