﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;


namespace PRMServices
{
    [ServiceContract]
    public interface IPRMCijIndentService
    {
        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanydeptdesigtypes?userId={userid}&type={type}&sessionid={sessionID}")]
        List<CompanyDeptDesigTypes> GetCompanyDeptDesigTypes(int userId, string type, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveworkcategory")]
        Response SaveWorkCategory(List<BudgetCodeEntity> listWorkCategories, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savebudgetcode")]
        Response SaveBudgetCode(List<BudgetCodeEntity> listBudgetCodes, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getworkcategories?userid={userID}&sessionid={sessionID}")]
        List<BudgetCodeEntity> GetWorkCategories(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getbudgetcodes?userid={userID}&sessionid={sessionID}")]
        List<BudgetCodeEntity> GetBudgetCodes(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getassignedindents?userid={userID}&sessionid={sessionID}")]
        List<Requirement> GetAssignedIndents(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "editcijindentdetails")]
        Response EditCijIndentDetails(int id, string type, int val, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getitemslist?moduleid={moduleID}&module={module}&sessionid={sessionID}")]
        List<IndentItems> GetItemsList(int moduleID, string module, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecij")]
        Response saveCIJ(stringCIJ stringcij, MPIndent indent);


        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcij?cijid={cijID}&sessionid={sessionID}")]
        stringCIJ GetCIJ(int cijID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getindentdetails?indentid={indentID}&sessionid={sessionID}")]
        MPIndent GetIndentDetails(int indentID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveindentdetails")]
        Response SaveIndentDetails(MPIndent mpindent);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "deletecij")]
        Response DeleteCIJ(stringCIJ stringcij);


        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcijlist?userid={userID}&sessionid={sessionID}")]
        List<stringCIJ> GetCIJList(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getindentlist?userid={userID}&sessionid={sessionID}")]
        List<MPIndent> GetIndentList(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savefileupload")]
        Response saveFileUpload(FileUpload fileupload);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcijitems?id={id}&sessionid={sessionID}")]
        List<IndentItems> GetCijItems(int id, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getseries?series={series}&seriestype={seriesType}&compid={compID}&deptid={deptID}&sessionid={sessionID}")]
        Models.Response GetSeries(string series, string seriesType, string sessionID, int compID, int deptID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "rejectindentitems")]
        Response RejectIndentItems(List<IndentItems> listItems, int indentID, int userID, string sessionID);

    }
}