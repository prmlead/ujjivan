﻿using PRM.Core.Domain.Logistics;
using PRM.Core.Models;
using PRMServices.Models.Logistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPRMLogisticQuotationService" in both code and config file together.
    [ServiceContract]
    public interface IPRMLogisticQuotationService
    {
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "logistic/quotation_comparison/{rid}")]
        IList<LogisticQuotationReport> GetLogisticQuotation(string rid);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       ResponseFormat = WebMessageFormat.Json,
       RequestFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "logistic/update_charges")]
        Response UpdateQuotationCharges(LogisticQuotationUpdateModel model);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
     ResponseFormat = WebMessageFormat.Json,
     RequestFormat = WebMessageFormat.Json,
     BodyStyle = WebMessageBodyStyle.Bare,
     UriTemplate = "logistic/update_charges_list")]
        Response UpdateQuotationChargesList(IList<LogisticQuotationUpdateModel> model);
    }
}
