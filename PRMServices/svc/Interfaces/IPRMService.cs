﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;
using PRMServices.Models.Vendor;
using CATALOG = PRMServices.Models.Catalog;
using System.Threading.Tasks;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMServices
    {
        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuseralerts?uid={uid}&sessionid={sessionid}")]
        List<EmailLogs> GetUserAlerts(int uid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrunningauctions?userid={userID}&sessionid={sessionID}&section={section}&limit={limit}")]
        List<RunningAuction> GetAuctions(int userID, string sessionID, string section, int limit);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcurrentauctions?userid={userID}&sessionid={sessionID}&section={section}&limit={limit}")]
        List<Requirement> GetRunningAuctions(int userID, string sessionID, string section, int limit);

        //#CB-0-2018-12-05
        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementdata?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        Requirement GetRequirementData(int reqID, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "requirementdatalite?reqid={reqID}&sessionid={sessionID}")]
        RequirementLite GetRequirementDataLite(int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreportrequirementdata?reqid={reqID}&userid={userID}&excludePriceCap={excludePriceCap}&sessionid={sessionID}")]
        Requirement GetReportRequirementData(int reqID, int userID, int excludePriceCap, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpodetails?reqid={reqID}&userid={userID}&vendorid={vendorID}&sessionid={sessionID}")]
        RequirementPO GetPODetails(int reqID, int userID, int vendorID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserinfo?userid={userID}&sessionid={sessionID}")]
        UserInfo GetUser(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsuperuserdetails?userid={userID}&sessionid={sessionID}")]
        UserInfo GetSuperUser(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementsreport?userid={userID}&sessionid={sessionID}")]
        List<ReportData> GetRequirementsReport(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpricecomparison?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        PriceComparison GetPriceComparison(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpricecomparisonprenegotiation?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        PriceComparison GetPriceComparisonPreNegotiation(int reqID, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getCatalogCategories?uid={uid}")]
        List<CATALOG.Category> GetCatalogCategories(int uid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "validatecurrencyrate?reqid={reqid}&sessionid={sessionid}")]
        Response ValidateCurrencyRate(int reqid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "refreshrequirementcurrency?reqid={reqid}&sessionid={sessionid}")]
        Response RefreshRequirementCurrency(int reqid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcurrencyfactors?userid={userID}&sessionid={sessionID}&compid={compID}")]
        List<CurrencyFactors> GetCurrencyFactors(int userID, string sessionID, int compID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcurrencyfactoraudit?currencycode={currencycode}&sessionid={sessionid}&compid={compid}")]
        List<CurrencyFactors> GetCurrencyFactorAudit(string currencycode, int compid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getlppbyproductids?productids={productids}&ignorereqid={ignorereqid}&compid={compid}&sessionid={sessionid}")]
        List<LPPEntity> GetLPPByProductIds(string productids, string ignorereqid, int compid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getlocations?compid={compid}&type={type}&sessionid={sessionid}")]
        List<LocationDetails> GetLocations(int compid, string type, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getregionhierarchy?compid={compid}&sessionid={sessionid}")]
        List<RegionHierarchy> GetRegionHierarchy(int compid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecurrencyfactor")]
        Response SaveCurrencyFactor(CurrencyFactors currencyFactor);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savelocation")]
        Response SaveLocation(LocationDetails locationDetails);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveregionhierarchy")]
        Response SaveRegionHierarchy(RegionHierarchy details);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "generatepoforuser")]
        Response UpdatePOData(RequirementPO reqPO);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "selectvendor")]
        Response SelectVendor(int userID, int vendorID, int reqID, string reason, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "stopbids")]
        Response StopBids(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "endnegotiation")]
        Response EndNegotiation(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getattachment?attachid={attachmentID}&sessionid={sessionID}")]
        Response GetAttachment(string attachmentID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getattachmentbase64?attachid={attachmentID}&sessionid={sessionID}")]
        FileData GetAttachmentBase64(string attachmentID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "materialdispatch")]
        Response MaterialDispatch(ReqMaterialDetails reqMat);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmyauctions?userid={userID}&fromDate={fromDate}&toDate={toDate}&onlyrfq={onlyrfq}&sessionid={sessionID}")]
        List<Requirement> GetMyAuctions(int userID,string fromDate,string toDate, string sessionID, bool onlyrfq = false);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsubuserdata?userid={userID}&sessionid={sessionID}")]
        List<UserInfo> GetSubUserData(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyuserdata?compId={compId}&sessionId={sessionId}")]
        List<UserInfo> GetCompanyUserData(int compId, string sessionId);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getactiveleads?userid={userID}&sessionid={sessionID}")]
        List<Requirement> GetActiveLeads(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "paymentdetails")]
        Response PaymentDetails(PaymentDetails paymentDets);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcomments?reqid={reqID}&userID={userID}&sessionid={sessionID}")]
        List<Comment> GetComments(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdetails?userid={userID}&sessionid={sessionID}")]
        UserDetails GetUserDetails(int userID, string sessionID);

        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "register")]
        Response Register(Register register, List<User> altUsers = null);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcategories?userid={userID}")]
        List<CategoryObj> GetCategories(int userID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getusersubcategories?userid={userID}&sessionid={sessionID}")]
        List<CategoryObj> GetSubCategories(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getusercategories?userid={userID}&sessionid={sessionID}")]
        List<CategoryObj> GetUserCategories(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserssubcategories?userid={userID}&sessionid={sessionID}")]
        List<CategoryObj> GetUserSubCategories(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanytypes")]
        List<string> GetCompanyTypes();

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getusercredentials?userid={userID}&sessionid={sessionID}")]
        List<Credentials> GetUserCredentials(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdashboardstats?userid={userID}&sessionid={sessionID}&fromdate={fromDate}&todate={toDate}")]
        DashboardStats GetDashboardStats(int userID, string sessionID, DateTime fromDate, DateTime toDate);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdashboardstats1?userid={userID}&sessionid={sessionID}&fromdate={fromDate}&todate={toDate}")]
        DashboardStats GetDashboardStats1(int userID, string sessionID, DateTime fromDate, DateTime toDate);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcustomerdashboard?userid={userID}&sessionid={sessionID}&fromdate={fromDate}&todate={toDate}")]
        CustomerDashboard GetCustomerDashbaord(int userID, string sessionID, DateTime fromDate, DateTime toDate);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdate")]
        DateTime GetDate();

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdatelogistics")]
        DateTime GetDateLogistics();

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getserverdatetime")]
        ServerDateTime GetServerDateTime();

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "issessionvalid?sessionid={sessionID}")]
        Response IsSessionValid(string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorstatistics?userid={userid}&sessionid={sessionID}")]
        VendorStatistics GetVendorStatistics(string sessionID, int userid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "companycurrencies?compid={compid}&sessionid={sessionID}")]
        List<Currencies> GetCompanyCurrencies(int compid, string sessionID);



        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatestatus")]
        Response UpdateStatus(int reqid, int userid, string status, string type, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getrevisedquotations")]
        Response GetRevisedQuotations(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "generatepo")]
        Response GeneratePO(int reqid, int userid, byte[] POfile, string POfileName, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "updateauctionstart")]
        Response UpdateAuctionStart(int reqID, int userID, DateTime date, string sessionID, string negotiationDuretion, NegotiationSettings NegotiationSettings);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "SaveReqNegSettings")]
        Response SaveReqNegSettings(int reqID, string sessionID, string negotiationDuretion, NegotiationSettings NegotiationSettings);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "checkuserifexists")]
        bool CheckUserIfExists(string phone, string idtype, bool onlyvendor, bool onlycustomer);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "isnegotiationrunning")]
        bool IsNegotationRunning(int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "loginuser")]
        Response LoginUser(string username, string password, string selecteduid, string selectedcompid, string source, string userLoginOtp1);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "loginuserbyhash")]
        Response LoginUserByHash(string userhashtoken, string source);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "loginjwtsso")]
        Response LoginJWTSSO(string ssotoken, string source);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "loginadsso")]
        Response LoginADSSO(string ssotoken, string source);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "logoutuser")]
        Response LogoutUser(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatebidtime")]
        Response UpdateBidTime(int reqID, int userID, long newTicks, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "requirementsave")]
        Response RequirementSave(Requirement requirement, byte[] attachment);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "registeruser")]
        Response RegisterUser(UserInfo userInfo);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "addvendortoauction")]
        Response AddVendorToAuction(VendorDetails vendor, int reqID, string sessionID, int userID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "makeabid")]
        Response MakeABid(int reqID, int userID, double price, byte[] quotation, string quotationName, string sessionID,
            double tax, double freightcharges, string warranty, string payment, string duration, string validity,
            List<RequirementItems> quotationObject, int revised, double priceWithoutTax, string type, double discountAmount,
            List<RequirementTaxes> listRequirementTaxes, string otherProperties, bool ignorevalidations, double itemrevtotalprice, double vendorBidPrice,
            double revfreightCharges, double revpackingCharges, double revinstallationCharges, int surrogateId = 0, string surrogateComments = "");

        //[AuthInterceptor][OperationContract]
        //[WebInvoke(Method = "POST",
        //ResponseFormat = WebMessageFormat.Json,
        //BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //UriTemplate = "verifyotp")]
        //Response VerifyOTP(int OTP, int userID, string phone, int isMobile);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "verifyemailotp")]
        Response VerifyEmailOTP(int OTP, int userID, string email);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "addnewvendor")]
        UserInfo AddNewVendor(VendorInfo vendorInfo);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "addnewcustomer")]
        Response AddNewCustomer(VendorInfo customerInfo);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getvendors")]
        List<VendorDetails> GetVendors(string Categories, string locations, string sessionID, int uID, int evalID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getSharedLinkVendors?userid={userid}&vendorids={vendorids}&sessionid={sessionid}")]
        List<VendorDetails> GetSharedLinkVendors(int userid, string vendorids, string sessionid);


        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getUnBlockedVendors?userid={userid}&vendorids={vendorids}&sessionid={sessionid}")]
        List<VendorDetails> GetUnBlockedVendors(int userid, string vendorids, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getvendorsbycatnsubcat")]
        List<VendorDetails> GetVendorsByCatNSubCat(int[] Categories, string sessionID, int count, int uID, int evalID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getvendorswithoutcategories")]
        List<VendorDetails> GetVendorsWithOutCategories(int uID, int evalID, string sessionID, string searchString);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savecomment")]
        Response SaveComment(Comment com);
        // TODO: Add your service operations here

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "forgotpassword")]
        Response ForgotPassword(string email, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "changepassword")]
        Response ChangePassword(int userID, string username, string oldPass, string newPass);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "resetpassword")]
        Response resetpassword(string email, string sessionid, string NewPass, string ConfNewPass);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateuserinfo")]
        Response UpdateUserInfo(UserDetails user);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateuserbasicinfo")]
        Response UpdateUserBasicInfo(UserDetails user);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateuserprofileinfo")]
        Response UpdateUserProfileInfo(UserDetails user);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendotpforemail")]
        Response SendOTPForEmail(int userID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendcontactemail")]
        Response SendContactEmail(string Name, string Email, string Mobile, string Comment, string Industry);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendotp")]
        Response SendOTP(int userID, string phone = null);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendotpformobile")]
        Response SendOTPforMobile(string phone);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatecredentials")]
        Response UpdateCredentials(int userID, List<CredentialUpload> files, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleterequirement")]
        Response DeleteRequirement(int reqID, int userID, string sessionID, string reason);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleteuser")]
        Response DeleteUser(int userID, int referringUserID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "activateuser")]
        Response ActivateUser(int userID, int referringUserID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "userratings")]
        Response UserRatings(int uID, int userID, float rating, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateTimeTemporary")]
        Response UpdateTimeTemporary(int reqID, long ticks, int userID, string sessionID);

        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "verifydocuments")]
        Response VerifyDocuments(string phone, List<CredentialUpload> files, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DocumentVerification?phone={phone}")]
        Response DocumentVerification(string phone);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteLogin?phone={phone}")]
        Response DeleteLogin(string phone);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "TestGetOtp?phone={phone}")]
        Response TestGetOtp(string phone);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleteattachment")]
        Response DeleteAttachment(int userID, int reqID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateexpdelandpaydate")]
        Response UpadteExpDelAndPayDate(int reqid, DateTime date, string type, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "makeabidformobile")]
        Response MakeABidForMobile(int reqID, int userID, double price, string strQuotation, string quotationName, string sessionID, double tax);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "startNegotiation")]
        Response StartNegotiation(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "checkForRestartNegotiation")]
        Response checkForRestartNegotiation(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "restartnegotiation")]
        Response RestartNegotiation(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deletevendorfromauction")]
        Response DeleteVendorFromAuction(int userID, int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "testverifyuser?phone={phone}")]
        Response testVerifyUser(string phone);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getkeyvaluepairs?parameter={parameter}&compid={compid}")]
        KeyValuePair[] GetKeyValuePairs(string parameter, int compid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getbidhistory?reqid={reqID}&userID={userID}&sessionid={sessionID}")]
        List<Comment> GetBidHistory(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetItemBidHistory?reqid={reqID}&userID={userID}&sessionid={sessionID}")]
        List<RequirementItems> GetItemBidHistory(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "appversion?uid={uID}&appversioncode={appVersionCode}")]
        AppInfo AppVersion(int uID, double appVersionCode);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getIncotermProductConfig?incoTerm={incoTerm}&sessionid={sessionid}")]
        List<ProductConfig> GetIncotermProductConfig(string incoTerm, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "isnegotiationended?reqid={reqID}&sessionid={sessionID}")]
        Response IsNegotiationEnded(int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "saveuserphone")]
        Response SaveUserPhone(int uid, string model, string os, string token, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "uploadquotation")]
        Response UploadQuotation(List<RequirementItems> quotationObject, int userID, int reqID, string sessionID, double price,
            double tax, double freightcharges, double vendorBidPrice, string warranty, string payment, string duration,
            string validity, int revised, string DesfileName, double discountAmount, List<RequirementTaxes> listRequirementTaxes,
            string otherProperties, string gstNumber, double packingCharges,double packingChargesTaxPercentage, double packingChargesWithTax,
            double installationCharges, double installationChargesTaxPercentage, double installationChargesWithTax,double revpackingCharges,double revinstallationCharges, 
            double revpackingChargesWithTax, double revinstallationChargesWithTax, string selectedVendorCurrency, 
            string uploadType, List<FileUpload> multipleAttachments,

            double freightCharges,
            double freightChargesTaxPercentage, 
            double freightChargesWithTax,
            double revfreightCharges,
            double revfreightChargesWithTax,
            string INCO_TERMS, int TaxFiledValidation, string isVendAckChecked, string REQ_VEND_SPEC_JSON, int surrogateId = 0, string surrogateComments = "", string quotFreezeTime = "");

        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "uploadQuotationForSelectVendorCeilingPrices")]
        Response UploadQuotationForSelectVendorCeilingPrices(List<RequirementItems> quotationObject, int userID, int reqID, string sessionID, double price,
            double tax, double freightcharges, double vendorBidPrice, string warranty, string payment, string duration,
            string validity, int revised, string DesfileName, double discountAmount, List<RequirementTaxes> listRequirementTaxes,
            string otherProperties, string gstNumber, double packingCharges, double packingChargesTaxPercentage, double packingChargesWithTax,
            double installationCharges, double installationChargesTaxPercentage, double installationChargesWithTax, double revpackingCharges, double revinstallationCharges,
            double revpackingChargesWithTax, double revinstallationChargesWithTax, string selectedVendorCurrency,
            string uploadType, List<FileUpload> multipleAttachments,
            double freightCharges,
            double freightChargesTaxPercentage,
            double freightChargesWithTax,
            double revfreightCharges,
            double revfreightChargesWithTax,
            string INCO_TERMS, int TaxFiledValidation, string isVendAckChecked, int surrogateId = 0, string surrogateComments = "", string quotFreezeTime = "");

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "uploadquotationsfromexcel")]
        Response UploadQuotationsFromExcel(int reqID, int userID, string sessionID, byte[] quotationAttachment);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendtelegrammsg")]
        TelegramMsg SendTelegramMsg(TelegramMsg telegrammsg);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "requestSupport")]
        Response RequestSupport(TelegramMsg telegrammsg);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendpushnotification")]
        Response SendPushNotification(PushNotifications push);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "revquotationupload")]
        Response RevQuotationUpload(int reqID, int userID, double price, byte[] quotation, string quotationName, string sessionID, double tax);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "revquotationuploadformobile")]
        Response RevQuotationUploadForMobile(int reqID, int userID, double price, string strQuotation, string quotationName, string sessionID, double tax);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementreminders")]
        List<RequirementReminders> GetRequirementReminders();

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getquotationreminders?compId={compId}")]
        List<RequirementReminders> GetQuotationReminders(string compId);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getnegotiationstartalerts")]
        List<RequirementReminders> GetNegotiationStartAlerts();

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "requirementsettings?reqid={reqid}&sessionid={sessionid}")]
        List<RequirementSetting> GetRequirementSettings(int reqid, string sessionid);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "togglecounterbid")]
        Response ToggleCounterBid(int reqId, int isCounterBid, int user, string sessionId);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saverequirementsetting")]
        Response SaveRequirementSetting(RequirementSetting requirementSetting);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "quatationaprovel")]
        Response QuatationAprovel(int reqID, int customerID, int vendorID, bool value, string reason, string sessionID, 
            string action, decimal technicalScore);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "itemquotationapproval")]
        Response ItemQuotationApproval(int reqId, int customerId, int vendorId, int itemId, bool value, string comment, string sessionId);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "itemlevelquotationapproval")]
        Response ItemLevelQuotationApproval(List<RequirementItems> items, int vendorId, int reqId, int customerId, string sessionId);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "sendquotationapprovalstatusemail")]
        Response SendQuotationApprovalStatusEmail(List<RequirementItems> items, int vendorId, int reqId, int customerId, string sessionId);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savedifferentialfactor")]
        Response SaveDifferentialFactor(int reqID, int customerID, int vendorID, decimal value, string reason, string sessionID);

        //[AuthInterceptor][OperationContract]
        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "assignvendorstocustomer?phone={phone}")]
        //Response AssignVendorsToCustomer(string phone);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyvendors?userid={userID}&sessionid={sessionID}&PageSize={PageSize}&NumberOfRecords={NumberOfRecords}&searchString={searchString}&PRODUCT_ID={PRODUCT_ID}&IS_FROM={IS_FROM}&CHECKED_VENDORS={CHECKED_VENDORS}&searchRegion={searchRegion}")]
        List<UserDetails> GetCompanyVendors(int userID, string sessionID, int PageSize = 0, int NumberOfRecords = 0, string searchString = null, int PRODUCT_ID = 0,string IS_FROM = null, string CHECKED_VENDORS = null, string searchRegion = null);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "itemwiseselectvendor")]
        Response ItemWiseSelectVendor(int userID, int vendorID, int reqID, int itemID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyleads?userid={userID}&searchstring={searchString}&searchtype={searchType}&sessionid={sessionID}")]
        List<Requirement> GetCompanyLeads(int userID, string searchString, string searchType, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savenegotiationsettings")]
        Response SaveNegotiationSettings(NegotiationSettings NegotiationSettings);
        
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "vendorreminders")]
        Response VendorReminders(int reqID, int userID, string message, int[] vendorIDs, string[] vendorCompanyNames, string sessionID, string requestType);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorreminders?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        List<Reminders> GetVendorReminders(int reqID, int userID, string sessionID);

        //[AuthInterceptor][OperationContract]
        //[WebInvoke(Method = "POST",
        //BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //RequestFormat = WebMessageFormat.Json,
        //ResponseFormat = WebMessageFormat.Json,
        //UriTemplate = "generatenewquotation")]
        //Response GenerateNewQuotation(int reqID, int customerID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "activatecompanyvendor")]
        Response ActivateCompanyVendor(int customerID, int vendorID, int isValid, string sessionID);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.Bare,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "VendorStatusUpdate")]
        Response UpdateCompanyVendorStatus(VendorStatusUpdateModel model);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveVendorOtherCharges")]
        Response SaveVendorOtherCharges(int userID, VendorDetails[] vendorOtherChargesArr, string sessionID);
        

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.Bare,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "UpdateVendorScore")]
        Response UpdateVendorScore(VendorScoreUpdateModel model);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreportsrequirement?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        Requirement GetReportsRequirement(int reqID, int userID, string sessionID);


        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getqcsvendoritemassignments?reqid={reqid}&userid={userid}&qcsid={qcsid}&sessionid={sessionid}")]
        VendorItemAssignment[] GetQCSVendorItemAssignments(int reqid, int userid,int qcsid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorratings?userid={userid}&sessionid={sessionID}")]
        Ratings[] GetVendorRatings(string sessionID, int userid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "requirementvendoranalysis?vendorids={vendorids}&itemids={itemids}&sessionid={sessionid}")]
        KeyValuePair[] GetRequirementVendorAnalysis(string vendorids, string itemids, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savevendorrating")]
        Response SaveVendorRating(int rId, int reqId, int reviewerId, int revieweeId, decimal delivery, decimal quality, decimal emergency, decimal service, decimal response, string comments, string seesionId);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "importentity")]
        Response ImportEntity(ImportEntity entity);

        //#CB-0-2018-12-05
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savepricecap")]
        Response SavePriceCap(int reqID, string sessionID, int userID, string reqType, double priceCapValue, int isUnitPriceBidding, bool IS_CB_ENABLED, bool IS_CB_NO_REGRET);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuseraccess?userid={userID}&sessionid={sessionID}")]
        List<UserAccess> GetUserAccess(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveuseraccess")]
        Response SaveUserAccess(List<UserAccess> listUserAccess, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveReductionLevelSetting")]
        Response saveReductionLevelSetting(ReductionSetting reductionSetting);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getReductionSettings?reqid={reqid}&sessionid={sessionid}")]
        List<ReductionSetting> getReductionSettings(int reqid, string sessionid);


        #region Department Designation START

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdepartmentdesignations?userid={userID}&includetemp={includetemp}&sessionid={sessionID}")]
        List<Department> GetUserDepartmentDesignations(int userID, int includetemp, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetOtherInfo?companyId={companyId}&value={value}&sessionid={sessionid}&type={type}")]
        string GetOtherInfo(int companyId, string value, string sessionid, string type);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveuserdepartmentdesignation")]
        Response SaveUserDepartmentDesignation(List<Department> listUserDeptDesig, string sessionID);

        #endregion Department Designation END



        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanydepartments?userid={userID}&sessionid={sessionID}")]
        List<CompanyDepartments> GetCompanyDepartments(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanydepartment")]
        Response SaveCompanyDepartment(CompanyDepartments companyDepartment);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "deletedepartment")]
        Response DeleteDepartment(int deptID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveuserdepartments")]
        Response SaveUserDepartments(List<UserDepartments> listUserDepartments, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "saveworkcategory")]
        Response SaveWorkCategory(List<BudgetCodeEntity> listWorkCategories, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savebudgetcode")]
        Response SaveBudgetCode(List<BudgetCodeEntity> listBudgetCodes, string sessionID);


        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getworkcategories?userid={userID}&sessionid={sessionID}")]
        List<BudgetCodeEntity> GetWorkCategories(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getbudgetcodes?userid={userID}&sessionid={sessionID}")]
        List<BudgetCodeEntity> GetBudgetCodes(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdepartments?userid={userID}&sessionid={sessionID}")]
        List<UserDepartments> GetUserDepartments(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdesignations?userid={userID}&sessionid={sessionID}")]
        List<UserDesignations> GetUserDesignations(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdeptdesig?userid={userID}&sessionid={sessionID}")]
        List<UserDepartments> GetUserDeptDesig(int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompdeptdesig?compid={compID}&sessionid={sessionID}")]
        List<UserDepartments> GetCompanyDeptDesig(int compID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanydeptdesigtypes?userId={userid}&type={type}&sessionid={sessionID}")]
        List<CompanyDeptDesigTypes> GetCompanyDeptDesigTypes(int userId, string type, string sessionID);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveuserdeptdesig")]
        Response SaveUserDeptDesig(List<UserDepartments> listUserDeptDesig, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdepartmentusers?deptid={deptID}&userid={userID}&sessionid={sessionID}")]
        List<UserDepartments> GetDepartmentUsers(int deptID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanydesignations?userid={userID}&sessionid={sessionID}")]
        List<CompanyDesignations> GetCompanyDesignations(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanydesignations")]
        Response SaveCompanyDesignations(CompanyDesignations companyDesignations);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "deletedesignation")]
        Response DeleteDesignation(int desigID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savereqdepartments")]
        Response SaveReqDepartments(List<ReqDepartments> listReqDepartments, string sessionID);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "shareemaillinkstoVendors")]
        Response ShareemaillinkstoVendors(int userID, List<VendorDetails> vendorIDs, string sessionID);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savereqdeptdesig")]
        Response SaveReqDeptDesig(List<ReqDepartments> listReqDepartments, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdepartments?userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        List<ReqDepartments> GetReqDepartments(int userID, int reqID, string sessionID);



        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdeptdesig?userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        List<ReqDepartments> GetReqDeptDesig(int userID, int reqID, string sessionID);


        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getisauthorized?userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        Response GetIsAuthorized(int userID, int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanyconfiguration")]
        Response SaveCompanyConfiguration(List<CompanyConfiguration> listCompanyConfiguration, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyconfiguration?compid={compID}&configkey={configKey}&sessionid={sessionID}")]
        List<CompanyConfiguration> GetCompanyConfiguration(int compID, string configKey, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetfeildMappings?compid={compID}&configkey={configKey}&sessionid={sessionID}")]
        List<CompanyConfiguration> GetfeildMappings(int compID, string configKey, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanyerpfieldmapping")]
        Response SaveCompanyERPFieldMapping(List<PRFieldMapping> listCompanyERPFieldMapping, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyconfigurationforlogin?compid={compID}&configkey={configKey}&sessionid={sessionID}")]
        List<CompanyConfiguration> GetCompanyConfigurationForLogin(int compID, string configKey, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "reqtechsupport")]
        Response ReqTechSupport(ReqShare reqShare);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saverunningitemprice")]
        Response SaveRunningItemPrice(List<RequirementItems> itemsList, int userID, int reqID, double price, double vendorBidPrice, double freightcharges,
            double revfreightCharges, double revpackingCharges, double revinstallationCharges);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "assignvendortocompany")]
        User AssignVendorToCompany(int userID, string vendorPhone, string vendorEmail, string category, string[] subCategory, string sessionID, string altPhoneNum, string altEmail);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savemulticontacts")]
        Response SaveMultiContacts(int userID, string phone, string email, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmaterialreceiveddata?reqid={reqID}&sessionid={sessionID}")]
        MaterialReceived GetMaterialReceivedData(int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "materialreceived")]
        Response MaterialReceived(MaterialReceived materialreceived);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanycategories")]
        Response SaveCompanyCategories(int compID, int id, string category, string subcategory, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "updatepricecap")]
        Response UpdatePriceCap(int uID, int reqID, double price, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanycategories?compid={compID}&sessionid={sessionID}")]
        List<CategoryObj> GetCompanyCategories(int compID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savealternatecontacts")]
        Response SaveAlternateContacts(UserDetails user);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getalternatecontacts?userid={userID}&compid={compID}&sessionid={sessionID}")]
        List<UserDetails> GetAlternateContacts(int userID, int compID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saverequirementterms")]
        Response SaveRequirementTerms(List<RequirementTerms> listRequirementTerms, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementterms?userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        List<RequirementTerms> GetRequirementTerms(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "deleterequirementterms")]
        Response DeleteRequirementTerms(List<int> listTerms, string sessionID);










        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "resetpasswordbyotp")]
        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "resetpasswordbyotp?phone={phone}")]
        Response ResetPasswordByOTP(string phone, string capthatoken);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "opsGetandResetPass?phone={phone}" +
            "&newpass={newpass}&vc={verificationcode}&vt={verificationtype}")]
        Response opsGetandResetPass(string phone, string newpass, string verificationcode, string verificationtype);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "enablemarginfields")]
        Response EnableMarginFields(int isEnabled, int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorrequirements?userid={userID}&negotiationStarted={negotiationStarted}&sessionid={sessionID}")]
        List<Requirement> GetVendorRequirements(int userID, int negotiationStarted, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "itempreviousprice?productname={productname}&productno={productno}&brand={brand}&companyid={companyid}&sessionid={sessionid}")]
        VendorDetails ItemPreviousPrice(string productname, string productno, string brand, int companyid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getaltusers?clientid={clientID}&vendorid={vendorID}&sessionid={sessionID}")]
        List<User> GetAltUsers(int clientID, int vendorID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "validateuserlogin")]
        List<Company> ValidateUserLogin(string userLoginId, string email, string phone);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "uploadclientsidequotation")]
        List<RequirementItems> UploadClientSideQuotation(int reqID, int userID, string sessionID, byte[] quotationAttachment);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "uploadrequirementitemssaveexcel")]
        List<RequirementItems> uploadRequirementItemsSaveExcel(int reqID, bool isrfp, int userID, int compId, string sessionID, byte[] requirementItemsAttachment, int templateid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "uploadrequirementitemsceilingsave")]
        Response UploadRequirementItemsCeilingSave(int reqID, string sessionID, byte[] requirementItemsAttachment, int templateid);

        //  let url = domain + 'itempreviousprice?productname=' + itemDetails.productIDorName.trim() + '&productno=' + itemDetails.productIDorName.trim() + '&brand=' + itemDetails.productBrand.trim() + '&sessionid=' + itemDetails.sessionID;

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getotp?phone={phone}")]
        Response GetOTP(string phone);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "updatepassword")]
        Response UpdatePassword(int otp, string passCode, string phone);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdetailsbydeptdesigtypeid?compid={compID}" +
            "&depttypeid={deptTypeID}&desigtypeid={desigTypeID}&sessionid={sessionID}")]
        UserDetails GetUserDetailsByDeptDesigTypeID(int compID, int deptTypeID, int desigTypeID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getonlinestatus?userid={userID}&sessionid={sessionID}")]
        Response GetOnlineStatus(string sessionID, int userID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorproducts?userid={userID}&sessionid={sessionID}")]
        List<RequirementItems> GetVendorProducts(string sessionID, int userID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getactiveusers?compid={companyID}&sessionid={sessionID}")]
        List<ActiveUsers> GetActiveUsers(int companyID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getactivebuyers?userid={userID}&sessionid={sessionID}")]
        List<ActiveUsers> GetActiveBuyers(int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getusersloginstatus?userid={userID}&from={from}&to={to}&sessionid={sessionID}")]
        List<ActiveUsers> GetUsersLoginStatus(int userID, string from, string to, string sessionID);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getactiveuserstemplates?template={template}&userid={userID}&from={from}&to={to}&sessionid={sessionID}")]
        string GetActiveUsersTemplates(string template, int userID, string from, string to, string sessionID);

        #region COUNTER BID

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecbprices")]
        Response SaveCBPrices(VendorDetails auctionVendor, int userID, int isCustomer, string sessionID, string bidComments, int vendorID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecbpricescustomer")]
        Response SaveCBPricesCustomer(VendorDetails auctionVendor, int userID, int isCustomer, string sessionID, string bidComments, int vendorID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "freezecounterbid")]
        Response FreezeCounterBid(int reqID, int vendorID, string sessionID, string freezedBy, int freezeValue, string payment);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcbpricesaudit?reqid={reqID}&vendorid={vendorID}&sessionid={sessionID}")]
        List<CBPrices> GetCBPricesAudit(int reqID, int vendorID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "updatecbtime")]
        Response UpdateCBTime(DateTime cbEndTime, int userID, int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "cbstopquotations")]
        Response CBStopQuotations(int stopCBQuotations, int userID, int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "cbmarkascomplete")]
        Response CBMarkAsComplete(int isCBCompleted, int userID, int reqID, string sessionID);



        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "markAsCompleteREQ")]
        Response MarkAsCompleteREQ(int isREQCompleted, int userID, int reqID, string sessionID);

        #endregion COUNTER BID

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getemaillogs?from={from}&to={to}&compid={companyID}&sessionid={sessionid}")]
        List<EmailLogs> GetEmailLogs(string from, string to, int companyID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsmslogs?from={from}&to={to}&compid={companyID}&sessionid={sessionid}")]
        List<EmailLogs> GetSMSLogs(string from, string to, int companyID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyidbyvendorid?id={ID}")]
        EmailLogs GetCompanyidByVendorid(int ID);



        // # Catalogue Start Region

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getvendorsbyproducts")]
        List<VendorDetails> GetVendorsByProducts(string[] products, string sessionID, int uID, int evalID);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "token")]
        string LoginUserToken(string username, string password);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "searchgstin?gstin={gstin}&year={year}&sessionid={sessionid}")]
        string SearchGSTIN(string gstin, string year, string sessionid);


        // # Catalogue End Region


        // Category Based Vendors // 
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getVendorsbyCategories")]
        List<VendorDetails> GetVendorsByCategories(string Categories, string sessionID, int uID, int evalID);
        // Category Based Vendors //

        //Approve Vendor Through Workflow//
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "approveVendorThroughWorkflow")]
        Response ApproveVendorThroughWorkflow(int customerID, int vendorID, int isValid, string sessionID);
        //Approve Vendor Through Workflow//

        // Block Vendor //
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "blockUnBlockVendor")]
        Response BlockUnBlockVendor(int customerID, int vendorID, int isValid, string type, int unblockWfID, string sessionID);
        // Block Vendor //

        //Vendor Accepted Registartion//
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "acceptRegistration")]
        Response AcceptRegistration(int vendorID, int wfID, int compID, string sessionID);
        //Vendor Accepted Registartion//


        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getlastprice?companyid={companyid}&productIDorName={productIDorName}&sessionid={sessionid}")]
        List<LastPrices> GetLastPrice(int companyid, string productIDorName, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getToDoData?myDate={myDate}&userId={userId}&userType={userType}&sessionid={sessionid}")]
        List<Requirement> GetToDoData(string myDate, int userId, string userType, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "isvalidquotationupload?reqid={reqid}&sessionid={sessionid}")]
        bool IsValidQuotationUpload(string reqid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saverequirementitemlist")]
        Response SaveRequirementItemList(RequirementItems[] requirementitemlist, int reqid, int vendorid, int user, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanygstinfo?compid={compid}&sessionid={sessionid}")]
        CompanyGST[] GetCompanyGSTInfo(string compid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savecompanygstinfo")]
        Response SaveCompanyGSTInfo(CompanyGST companygst, int user, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deletecompanygstinfo")]
        Response DeleteCompanyGSTInfo(CompanyGST companygst, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savefile")]
        FileUpload SaveFile(FileUpload file, int user, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "SendQuotationReminders")]
        Response SendQuotationReminders(RequirementReminders reminder, string reminderMessage, string commMode);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getPreferredLogin")]
        Response GetPreferredLogin();

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorsapdetails?vendorid={vendorid}&reqid={reqid}&sessionid={sessionid}")]
        List<VendorInfo> GetVendorSAPDetails(int vendorid, int reqid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementvendorcodes?reqid={reqid}&sessionid={sessionid}")]
        List<VendorDetails> GetRequirementVendorCodes(int reqid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "searchrequirements?search={search}&excludeprlinked={excludeprlinked}&compid={compid}&sessionid={sessionid}")]
        List<RequirementLite> SearchRequirements(string search, bool excludeprlinked, int compid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyname")]
        string GetCompanyName();

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getDashboardIndicators?userId={userId}&compId={compId}&deptId={deptId}&sessionId={sessionId}&fromDate={fromDate}&toDate={toDate}")]
        DashboardIndicators GetDashboardIndicators(int userId, int compId, string deptId, string sessionId, string fromDate, string toDate);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAssignetTickets?sessionID={sessionID}")]
        List<OpsAssignTickets> GetAssignetTickets(string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "SaveAssignTickets")]
        Response SaveAssignTickets(List<OpsAssignTickets> AssignedTickets, List<OpsAssignTickets> UpdateData);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "validateuserrecaptcha")]
        Task<bool> ValidateUserRecaptcha(string userresponse);

        [AuthInterceptor]
        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendLoginOTP")]
        Response SendLoginOTP(string emailId);

        
        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "scanFile")]
        string ScanFile(string fileName, string content);

    }
}
