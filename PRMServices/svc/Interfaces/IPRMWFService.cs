﻿using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;
using System.Collections.Generic;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMWFService
    {

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrfqcijlist?userid={userID}&sessionid={sessionID}")]
        List<stringCIJ> GetRFQCIJList(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrfqindentlist?userid={userID}&sessionid={sessionID}")]
        List<MPIndent> GetRFQIndentList(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "workflows?compid={compID}&wfid={wfID}&deptid={deptID}&sessionid={sessionID}")]
        Workflow[] GetWorkflows(int compID, int wfID, int deptID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "itemworkflow?wfid={wfID}&moduleid={moduleID}&module={module}&sessionid={sessionID}&userid={userID}")]
        Workflow[] GetItemWorkflows(int wfID, int moduleID, string module, string sessionID, int userID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "workflowtrack?trackid={trackID}&sessionid={sessionID}")]
        WorkflowTrack[] GetWorkflowTrack(int trackID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "workflowaudit?trackid={trackID}&sessionid={sessionID}")]
        WorkflowAudit[] GetWorkflowAudit(int trackID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getusersforwfemails?approverid={approverID}&sessionid={sessionID}&deptid={deptID}&desigid={desigID}&type={type}&moduleName={moduleName}&moduleID={moduleID}")]
        List<Models.UserInfo> GetUsersForWFEmails(int approverID, string sessionID, int deptID, int desigID, string type, string moduleName = null, int moduleID = 0);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "isuserapproverforstage?approverid={approverID}&userid={userID}&sessionid={sessionID}")]
        bool IsUserApproverForStage(int approverID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmyworkflows?userid={userID}&status={status}&sessionid={sessionID}")]
        List<Workflow> GetMyWorkflows(int userID, string status, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmypendingapprovals?userid={userID}&deptid={deptID}&desigid={desigID}&sessionid={sessionID}")]
        List<WFPendingApprovals> GetMyPendingApprovals(int userID, string deptID, string desigID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmyindents?userid={userID}&sessionid={sessionID}")]
        List<MPIndent> GetMyIndents(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "dbgetmypendingapprovals?userid={userID}&sessionid={sessionID}")]
        List<MPIndent> DBGetMyPendingApprovals(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getseries?series={series}&seriestype={seriesType}&compid={compID}&deptid={deptID}&sessionid={sessionID}")]
        Models.Response GetSeries(string series, string seriesType, string sessionID, int compID, int deptID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrfqcreators?indentid={indentID}&sessionid={sessionID}")]
        List<int> GetRFQCreators(int indentID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "dbstatus?userid={userID}&sessionid={sessionID}" +
            "&department={department}&startdate={startDate}&enddate={endDate}&iscapex={isCapex}&ismedical={isMedical}")]
        DBStatus GetDBStatus(int userID, string sessionID, int department, string startDate, string endDate, int isCapex, int isMedical);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserwisedashboard?userid={userID}&sessionid={sessionID}" +
            "&department={department}&startdate={startDate}&enddate={endDate}&iscapex={isCapex}&ismedical={isMedical}")]
        List<DashboardUser> GetUserWiseDashboard(int userID, string sessionID, int department, string startDate, string endDate, int isCapex, int isMedical);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdeptwisedashboard?userid={userID}&sessionid={sessionID}" +
            "&department={department}&startdate={startDate}&enddate={endDate}&iscapex={isCapex}&ismedical={isMedical}")]
        List<DashboardUser> GetDeptWiseDashboard(int userID, string sessionID, int department, string startDate, string endDate, int isCapex, int isMedical);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getapprovallist?userid={userID}&deptid={deptID}&desigid={desigID}&type={type}&sessionid={sessionID}")]
        List<WFApproval> GetApprovalList(int userID, string deptID, string desigID, string type, string sessionID);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveworkflow")]
        Response SaveWorkflow(Workflow workflow);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveworkflowstage")]
        Response SaveWorkflowStage(WorkflowStages stage);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveworkflowaudit")]
        Response SaveWorkflowAudit(WorkflowAudit audit);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveworkflowtrack")]
        Response SaveWorkflowTrack(WorkflowTrack track);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "assignworkflow")]
        Response AssignWorkflow(int wID, int moduleID, int user, string sessionID, string SubModuleName = null, int SubModuleID = 0);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleteworkflow")]
        Response DeleteWorkflow(int wID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleteworkflowstage")]
        Response DeleteWorkflowStage(int stageID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saverfqcreators")]
        Response SaveRFQCreators(List<UserInfo> listUsers, int indentID, string sessionID);

        

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveuserstatus")]
        Response SaveUserStatus(string module, int id, string status, int userId, string sessionID);

    }
}
