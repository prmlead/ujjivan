﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMLogisticsService
    {
        #region GET Calls

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementdata?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        LogisticRequirement GetRequirementData(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getactiveleads?userid={userID}&sessionid={sessionID}")]
        List<LogisticRequirement> GetActiveLeads(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmyauctions?userid={userID}&sessionid={sessionID}")]
        List<LogisticRequirement> GetMyAuctions(int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getbidhistory?reqid={reqID}&userID={userID}&sessionid={sessionID}")]
        List<LogisticHistory> GetBidHistory(int reqID, int userID, string sessionID);
        
        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorreminders?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        List<Reminders> GetVendorReminders(int reqID, int userID, string sessionID);

        //[AuthInterceptor][OperationContract]
        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "itempreviousprice?companyid={companyid}&productIDorName={productIDorName}&netWeight={netWeight}" +
        //    "&natureOfGoods={natureOfGoods}&finalDestination={finalDestination}&sessionid={sessionid}")]
        //List<LogisticVendorDetails> ItemPreviousPrice(int companyid, string productIDorName, double netWeight, string natureOfGoods,
        //    string finalDestination, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "itempreviousprice?companyid={companyid}&productIDorName={productIDorName}&netWeight={netWeight}" +
           "&natureOfGoods={natureOfGoods}&finalDestination={finalDestination}&sessionid={sessionid}")]
        List<LogisticBestPrice> ItemPreviousPrice(int companyid, string productIDorName, double netWeight, string natureOfGoods,
           string finalDestination, string sessionid);


        #endregion

        #region POST Calls

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "requirementsave")]
        Response RequirementSave(LogisticRequirement requirement, byte[] attachment);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "submitquotation")]
        Response SubmitQuotation(List<LogisticRequirementItems> listLogisticReqItems, List<LogisticQuotationItems> listDeletedQuotationItems,
        LogisticVendorDetails vendorDetails, string sessionID, bool isRevised, bool isMakeBid = false);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveairlines")]
        Response SaveAirlines(List<LogisticRequirementItems> listLogisticReqItems, string sessionID, bool isRevised);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "rejectquotation")]
        Response RejectQuotation(int reqID, int userID, bool isRevised, string sessionID, string quotationRejectedComment, string revQuotationRejectedComment);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "updateauctionstart")]
        Response UpdateAuctionStart(int reqID, int userID, DateTime date, string sessionID, NegotiationSettings negotiationSettings);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "startnegotiation")]
        Response StartNegotiation(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "signalrfrontend")]
        Response signalRFrontEnd(int reqID, int userID, string sessionID, string methodName);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "updatebidtime")]
        Response UpdateBidTime(int reqID, int userID, long newTicks, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "stopbids")]
        Response StopBids(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "restartnegotiation")]
        Response RestartNegotiation(int reqID, int userID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "makebid")]
        Response MakeBid(int reqID, int userID, double bidPrice, List<LogisticRequirementItems> listRequirementItems, LogisticVendorDetails vendorDetails, 
        string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savepricecap")]
        Response SavePriceCap(int reqID, string sessionID, int userID, string reqType, double priceCapValue, int isUnitPriceBidding);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "updatepricecap")]
        Response UpdatePriceCap(int uID, int reqID, double price, string sessionID);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "vendorreminders")]
        Response VendorReminders(int reqID, int userID, string message, int[] vendorIDs, string[] vendorCompanyNames, string sessionID, string requestType);


        #endregion
    }
}
