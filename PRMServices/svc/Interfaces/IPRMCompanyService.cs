﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMCompanyService
    {

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "branch?compid={compid}&branchid={branchid}&sessionid={sessionid}")]
        List<Branch> GetCompanyBranch(int compid, int branchid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "project?compid={compid}&branchid={branchid}&sessionid={sessionid}")]
        List<Project> GetBranchProjects(int compid, int branchid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "userbranches?uid={uid}&sessionid={sessionid}")]
        List<Branch> GetUserBranches(int uid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "userprojects?uid={uid}&sessionid={sessionid}")]
        List<Project> GetUserProjects(int uid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savebranch")]
        Response SaveBranch(Branch item);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deletebranch")]
        Response DeleteBranch(Branch item);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveproject")]
        Response SaveProject(Project item);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleteproject")]
        Response DeleteProject(Project item);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "saveprojectrequirement")]
        Response SaveProjectRequirement(Project item);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saveuserbranch")]
        Response SaveUserBranch(int uid, int[] branches, int user, string sessionid);
    }
}
