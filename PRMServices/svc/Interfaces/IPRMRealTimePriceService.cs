﻿using PRM.Core.Domain.RealTimePrices;
using PRM.Core.Models;
using PRM.Core.Pagers;
using PRMServices.Models.Common;
using PRMServices.Models.RealTimePrice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPRMRealTimePriceService" in both code and config file together.
    [ServiceContract]
    public interface IPRMRealTimePriceService
    {

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "realtimeprice/upload_price/")]
        Response UploadPrices(RealTimePriceUploadModel model);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "realtimeprice/get_filter_data/{typeId}")]
        RealTimeFilterData GetCategories(string typeId);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "realtimeprice/get_prices/")]
        dynamic GetPrices(Pager pager);



        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "realtimeprice/get_report/")]
        IList<object[]> GetReport(RealTimeReportPostModel model);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "realtimeprice/save_setting/")]
        Response SaveRealTimePriceSettings(RealTimePriceSettingModel model);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "realtimeprice/get_setting/")]
        RealTimePriceSettingModel GetRealTimePriceSettings();

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "realtimeprice/send_notification/")]
        Response SendNotifications();
    }
}
