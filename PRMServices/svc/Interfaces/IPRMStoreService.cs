﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMStoreService
    {
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "stores?storeid={storeID}&storecode={storeCode}&compid={compID}&includebranch={includeBranch}&sessionid={sessionID}")]
        List<Store> GetStores(int storeID, string storeCode, int compID, int includeBranch, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "companystores?compid={compID}&parentstoreid={parentStoreID}&sessionid={sessionID}")]
        List<Store> GetCompanyStores(int compID, int parentStoreID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "storeitems?storeid={storeID}&itemid={itemID}&sessionid={sessionID}")]
        List<StoreItem> GetStoreItems(int storeID, int itemID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "storeitemaudit?storeid={storeID}&itemid={itemID}&sessionid={sessionID}")]
        List<StoreAudit> GetStoreItemAudit(int storeID, int itemID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "storeitemalert?storeid={storeID}&sessionid={sessionID}")]
        List<StoreItem> GetStoreItemAlert(int storeID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "storefinance?storeid={storeID}&consolidate={consolidate}&sessionid={sessionID}")]
        List<StoreFinance> GetStoreFinance(int storeID, int consolidate, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "storesitemstatus?compid={compID}&itemcode={itemCode}&sessionid={sessionID}")]
        List<StoreItem> GetStoresItemStatus(int compID, string itemCode, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "companyprops?compid={compID}&module={module}&sessionid={sessionID}")]
        List<CustomProperty> GetCompanyProps(int compID, string module, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "companypropvalues?entityid={entityID}&module={module}&sessionid={sessionID}")]
        List<CustomProperty> GetCompanyPropValues(int entityID, string module, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getstoreitemdetails?itemid={itemID}&notdispatched={notDispatched}&sessionid={sessionID}")]
        List<StoreItemDetails> GetStoreItemDetails(int itemID, int notDispatched, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "storeitemreceivedetails?grncode={grnCode}&storeid={storeID}&sessionid={sessionID}")]
        List<StoreItemReceive> GetStoreItemReceiveDetails(string grnCode, int storeID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "storeitemissuedetails?gincode={ginCode}&storeid={storeID}&sessionid={sessionID}")]
        List<StoreItemIssue> GetStoreItemIssueDetails(string ginCode, int storeID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "indentitemdetails?storeid={storeID}&gincode={ginCode}&grncode={grnCode}&sessionid={sessionID}")]
        List<StoreItemDetails> GetStoreIndentItemDetails(int storeID, string ginCode, string grnCode, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "uniqueitems?compid={compID}&itemname={itemName}&sessionid={sessionID}")]
        List<StoreItem> GetUniqueItems(int compID, string itemName, string sessionID);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savestore")]
        Response SaveStore(Store store);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savestoreitem")]
        Response SaveStoreItem(StoreItem item);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savestoreaudit")]
        Response SaveStoreAudit(StoreAudit audit);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savecompanyprop")]
        Response SaveCompanyProp(CustomProperty companyProp);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanypropvalue")]
        Response SaveCompanyPropValue(CustomProperty companyProp);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "transferstock")]
        Response TransferStock(int sourceStoreId, int destStoreId, string itemCode, int quantity, decimal itemPrice, int user, string SessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "importentity")]
        Response ImportEntity(ImportEntity entity);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "deletestore")]
        Response DeleteStore(int storeId, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "deletestoreitem")]
        Response DeleteStoreItem(int storeItemId, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "deletestoreitemdetails")]
        Response DeleteStoreItemDetails(int storeItemDetailsId, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savestoreitemdetails")]
        Response SaveStoreItemDetails(StoreItemDetails item);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savestoreitemreceive")]
        Response SaveStoreItemReceive(StoreItemReceive item);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savestoreitemissue")]
        Response SaveStoreItemIssue(StoreItemIssue item);
    }
}
