﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMManagement
    {
        #region Gets

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmngtdashboardstats?sessionid={sessionID}")]
        MngtDashboardStats GetMngtDashboardStats(string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmrktngdashboardstats?sessionid={sessionID}")]
        MngtDashboardStats GetMrktngDashboardStats(string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getUserslist?sessionid={sessionID}&userrole={userRole}&fromdate={fromDate}&todate={toDate}")]
        List<MngtUserDetails> GetUsersList(string sessionID, string userRole, string fromDate, string toDate);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getUserdetails?sessionid={sessionID}&userid={userId}")]
        MngtUserDetails GetUserDetails(string sessionID, int userId);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementslist?sessionid={sessionID}&status={status}&fromdate={fromDate}&todate={toDate}")]
        List<MngtRequirementDetails> GetRequirementsList(string sessionID, string status, string fromDate, string toDate);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementdetails?sessionid={sessionID}&reqid={reqId}")]
        MngtRequirementDetails GetRequirementDetails(string sessionID, int reqId);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getnoteslist?sessionid={sessionID}&notetype={noteType}&notetoid={noteToID}&userid={userID}")]
        List<MngtNotes> GetMngtNotesList(string sessionID, int noteToID, int userID, string noteType);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmngtrequirements?sessionid={sessionID}&userid={userID}")]
        List<MngtRequirementDetails> GetMngtRequirements(string sessionID, int userID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsearchuser?sessionid={sessionID}&searchkey={searchKey}&searchkeytype={searchKeyType}&searchrole={searchRole}")]
        List<MngtUserDetails> GetSearchUser(string sessionID, string searchKey, string searchKeyType, string searchRole);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmeetingslist?sessionid={sessionID}&userid={userID}")]
        List<MngtMeetings> GetMeetingsList(string sessionID, int userID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmeetings?sessionid={sessionID}&fromdate={fromDate}&todate={toDate}")]
        List<MngtCompanies> GetMeetings(string sessionID, DateTime fromDate, DateTime toDate);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserdashboardstats?sessionid={sessionID}&userid={userID}")]
        MngtDashboardStats GetUserDashboardStats(string sessionID, int userID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getinsidesalesusers?sessionid={sessionID}")]
        List<MngtUserDetails> GetInsideSalesUsers(string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "gettouchpointstatus?sessionid={sessionID}")]
        List<Response> GetTouchpointStatus(string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanies?sessionid={sessionID}&userid={userID}")]
        List<MngtCompanies> GetCompanies(string sessionID, int userID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "gettouchpoints?sessionid={sessionID}&userid={uID}&companyid={companyID}")]
        List<MngtTouchpoint> GetTouchpoints(string sessionID, int uID, int companyID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanycontacts?sessionid={sessionID}&companyid={companyID}")]
        List<MngtUserDetails> GetCompanyContacts(string sessionID, int companyID);


        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "insidesalesdashboard?sessionid={sessionID}&userid={userID}&usertype={userType}")]
        InsidesalesDashboard InsideSalesDashboard(string sessionID, int userID, string userType);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "insidesalesdailyreport?sessionid={sessionID}&userid={userID}&fromdate={fromDate}&todate={toDate}")]
        List<MngtUserDetails> InsideSalesDailyReport(string sessionID, int userID, DateTime fromDate, DateTime toDate);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "marketingdailyreport?sessionid={sessionID}&userid={userID}&fromdate={fromDate}&todate={toDate}")]
        List<MngtUserDetails> MarketingDailyReport(string sessionID, int userID, DateTime fromDate, DateTime toDate);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanynetworks?sessionid={sessionID}&userid={userID}&compid={compID}&linktype={linkType}")]
        List<CompanyNetworks> GetCompanyNetworks(string sessionID, int userID, int compID, string linkType);


        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getinsidesalesactions?sessionid={sessionID}&userid={userID}&actiontype={actionType}&fromdate={fromDate}&todate={toDate}")]
        List<MngtTouchpoint> GetInsideSalesActions(string sessionID, int userID, string actionType, DateTime fromDate, DateTime toDate);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmarketingactions?sessionid={sessionID}&userid={userID}&actiontype={actionType}&fromdate={fromDate}&todate={toDate}")]
        List<MngtTouchpoint> GetMarketingActions(string sessionID, int userID, string actionType, DateTime fromDate, DateTime toDate);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "mngtmeetingsreminders")]
        List<MngtMeetingsReminders> MngtMeetingsReminders();

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deletetouchpoint")]
        Response DeleteTouchPoint(string sessionID, int userID, int touchpointID);

        #endregion Gets

        #region Posts

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "verifyuser")]
        Response VerifyUser(string sessionID, int userID, string verificationType);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savenote")]
        Response SaveNote(string sessionID, int toID, int userID, string notes, string noteType);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savemngtrequirement")]
        Response SaveMngtRequirement(MngtRequirementDetails MngtRequirementDetails);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "updatedbvalues")]
        Response UpdateDbValues(UpdateDbValues UpdateDbValues);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "updatesinglevendorprice")]
        Response UpdateSingleVendorPrice(UpdateDbValues UpdateDbValues);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "assignvendorstocustomer")]
        Response AssignVendorsToCustomer(string sessionID, string customerID, string vendorID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savemeeting")]
        Response SaveMeeting(MngtMeetings MngtMeetings);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "mngtlogin")]
        Response MngtLogin(string phone, string password);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savetouchpoint")]
        Response SaveTouchpoint(MngtTouchpoint mngtTouchpoint);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompany")]
        Response SaveCompany(MngtCompanies mngtCompanies);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanycontacts")]
        MngtUserDetails SaveCompanyContacts(MngtUserDetails mngtUserDetails);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "assignusertocompany")]
        Response AssignUserToCompany(string sessionID, int compID, int userID, int assignedTo);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savecompanynetworks")]
        Response SaveCompanyNetworks(string sessionID, int compID, int userID, int compNetID, string link, string linkType, string userName);

        #endregion Posts    
    }
}
