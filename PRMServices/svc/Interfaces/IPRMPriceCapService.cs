﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;
using PRMServices.Models.Vendor;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMPriceCapService
    {
        #region Service Calls

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdata?reqid={reqID}&sessionid={sessionID}")]
        Requirement GetReqData(int reqID, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savepricecap")]
        Response SavePriceCap(int reqID, List<RequirementItems> listReqItems,
            double price, int isDiscountQuotation, string sessionID);

        #endregion
    }
}
