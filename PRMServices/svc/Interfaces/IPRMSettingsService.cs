﻿using PRM.Core.Domain.Configurations;
using PRM.Core.Domain.Configurations.Requirements;
using PRMServices.Models;
using PRMServices.Models.Settings.Registrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMSettingsService
    {


        /// <summary>
        /// Get registration fields
        /// </summary>
        /// <returns></returns>
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "settings/get_registration_setup")]
        List<RegistrationForm> GetRegistrationSettings();


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "settings/insert_reg_field")]
        Response InsertRegistraionField(RegistrationFormModel model);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "settings/update_reg_field")]
        Response UpdateRegistraionField(RegistrationFormModel model);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "settings/delete_reg_field/")]
        Response DeleteRegistraionField(int id);


        /// <summary>
        /// Get registration fields
        /// </summary>
        /// <returns></returns>
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "settings/get_requirement_setup")]
        List<RequirementForm> GetRequirementsSettings();


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "settings/insert_requirement_field")]
        Response InsertRequirementField(RequirementFormModel model);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "settings/update_requirement_field")]
        Response UpdateRequirementField(RequirementFormModel model);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "settings/delete_requirement_field/")]
        Response DeleteRequirementField(int id);

    }
}
