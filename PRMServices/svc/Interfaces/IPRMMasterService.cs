﻿using PRM.Core.Domain.Masters.ContactDetails;
using PRM.Core.Domain.Masters.DeliveryLocations;
using PRM.Core.Domain.Masters.DeliveryTerms;
using PRM.Core.Domain.Masters.GeneralTerms;
using PRM.Core.Models;
using PRM.Core.Pagers;
using PRMServices.Models.Masters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPRMMasterService" in both code and config file together.
    [ServiceContract]
    public interface IPRMMasterService
    {

        #region Contact details
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "contact_detail/insert")]
        Response InsertContactDetail(ContactDetailModel model);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "contact_detail/update")]
        Response UpdateContactDetail(ContactDetailModel model);



        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "contact_detail/getbypage")]
        dynamic GetByPageContactDetail(Pager model);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "contact_detail/getbyid/{id}")]
        ContactDetail GetByIdContactDetail(string id);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "contact_detail/delete")]
        Response DeleteContactDetail(int id);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
 RequestFormat = WebMessageFormat.Json,
 ResponseFormat = WebMessageFormat.Json,
 BodyStyle = WebMessageBodyStyle.Bare,
 UriTemplate = "contact_detail/get")]
        List<ContactDetail> GetContactDetail(ContactDetailGetModel model);


        #endregion


        #region Delivery location

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "delivery_location/insert")]
        Response InsertDeliveryLocation(DeliveryLocationModel model);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "delivery_location/update")]
        Response UpdateDeliveryLocation(DeliveryLocationModel model);



        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "delivery_location/getbypage")]
        dynamic GetByPageDeliveryLocation(Pager model);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "delivery_location/getbyid/{id}")]
        DeliveryLocation GetByIdDeliveryLocation(string id);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "delivery_location/delete")]
        Response DeleteDeliveryLocation(int id);




        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
 RequestFormat = WebMessageFormat.Json,
 ResponseFormat = WebMessageFormat.Json,
 BodyStyle = WebMessageBodyStyle.Bare,
 UriTemplate = "delivery_location/get")]
        List<DeliveryLocation> GetDeliveryLocation(DeliveryLocationGetModel model);

        #endregion

        #region Delivery term

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "delivery_term/insert")]
        Response InsertDeliveryTerm(DeliveryTermModel model);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "delivery_term/update")]
        Response UpdateDeliveryTerm(DeliveryTermModel model);



        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "delivery_term/getbypage")]
        dynamic GetByPageDeliveryTerm(Pager model);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "delivery_term/getbyid/{id}")]
        DeliveryTerm GetByIdDeliveryTerm(string id);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "delivery_term/delete")]
        Response DeleteDeliveryTerm(int id);



        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
 RequestFormat = WebMessageFormat.Json,
 ResponseFormat = WebMessageFormat.Json,
 BodyStyle = WebMessageBodyStyle.Bare,
 UriTemplate = "delivery_term/get")]
        List<DeliveryTerm> GetDeliveryTerms(DeliveryTermGetModel model);

        #endregion


        #region Payment term

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "payment_term/insert")]
        Response InsertPaymentTerm(PaymentTermModel model);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "payment_term/update")]
        Response UpdatePaymentTerm(PaymentTermModel model);



        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "payment_term/getbypage")]
        dynamic GetByPagePaymentTerm(Pager model);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "payment_term/getbyid/{id}")]
        PaymentTerm GetByIdPaymentTerm(string id);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "payment_term/delete")]
        Response DeletePaymentTerm(int id);



        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
 RequestFormat = WebMessageFormat.Json,
 ResponseFormat = WebMessageFormat.Json,
 BodyStyle = WebMessageBodyStyle.Bare,
 UriTemplate = "payment_term/get")]
        List<PaymentTerm> GetPaymnetTerms(PaymentTermGetModel model);

        #endregion


        #region General term

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "general_term/insert")]
        Response InsertGeneralTerm(GeneralTermModel model);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "general_term/update")]
        Response UpdateGeneralTerm(GeneralTermModel model);



        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "general_term/getbypage")]
        dynamic GetByPageGeneralTerm(Pager model);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "general_term/getbyid/{id}")]
        GeneralTerm GetByIdGeneralTerm(string id);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "general_term/delete")]
        Response DeleteGeneralTerm(int id);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Bare,
         UriTemplate = "general_term/get")]
        List<GeneralTerm> GetGeneralTerms(GeneralTermGetModel model);

        #endregion
    }
}
