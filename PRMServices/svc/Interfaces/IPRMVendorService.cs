﻿using PRM.Core.Models;
using PRMServices.Models.Vendor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPRMVendorService" in both code and config file together.
    [ServiceContract]
    public interface IPRMVendorService
    {
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "vendor/calculate_score/{userId}")]
        Response CalculateVendorScore(string userId);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "vendor/get_score/{userId}")]
        List<VendorScoreModel> GetVendorScore(string userId);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "vendor/save_score/")]
        Response SaveVendorScore(VendorScoreUpdateModel model);


        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "vendor/save_status/")]
        Response SaveVendorStatus(VendorStatusUpdateModel model);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "vendor/get_vendor_subcategory/{userId}")]
        int[] GetVendorSubCategories(string userId);





    }
}
