﻿using Microsoft.AspNet.SignalR;
using OfficeOpenXml;
using PRMServices.Common;
using PRMServices.Models;
using PRMServices.SignalR;
using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.ServiceModel.Activation;
using System.Threading.Tasks;
using System.Web;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMLogisticsService : IPRMLogisticsService
    {

        PRMServices prmServices = new PRMServices();
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();


        #region Logistic

        #region Logistic Gets

        public List<LogisticRequirement> GetMyAuctions(int userID, string sessionID)
        {
            List<LogisticRequirement> myAuctions = new List<LogisticRequirement>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("lgstc_GetRequirements", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LogisticRequirement requirement = new LogisticRequirement();
                        string[] arr = new string[] { };
                        byte[] next = new byte[] { };
                        requirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt16(row["REQ_ID"]) : -1;
                        requirement.CustomerID = userID;
                        requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                        if (requirement.EndTime == null)
                        {
                            requirement.EndTime = DateTime.MaxValue;
                        }
                        requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (requirement.StartTime == null)
                        {
                            requirement.StartTime = DateTime.MaxValue;
                        }
                        requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        requirement.Price = row["VEND_MIN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_PRICE"]) : 0;
                        double RunPrice = row["VEND_MIN_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_RUN_PRICE"]) : 0;
                        DateTime start = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (start < DateTime.Now && RunPrice > 0)
                        {
                            requirement.Price = RunPrice;
                        }
                        string POLink = row["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                        requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                        requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                        requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                        requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                        requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                        requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                        requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                        requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                        string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : 0;
                        requirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt16(row["IS_DISCOUNT_QUOTATION"]) : 0;
                        requirement.DaNo = row["DA_NUMBER"] != DBNull.Value ? Convert.ToString(row["DA_NUMBER"]) : string.Empty;

                        DateTime now = DateTime.Now;
                        if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                        {
                            DateTime date = Convert.ToDateTime(requirement.EndTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            requirement.TimeLeft = diff;
                            requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());

                        }
                        else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                        {
                            DateTime date = Convert.ToDateTime(requirement.StartTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            requirement.TimeLeft = diff;
                            requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                        }
                        else if (requirement.EndTime < now)
                        {
                            requirement.TimeLeft = -1;
                            if ((Status == GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && requirement.EndTime < now)
                            {

                                requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                                //EndNegotiation(requirement.RequirementID, requirement.CustomerID, sessionID);
                            }
                            else
                            {
                                requirement.Status = Status;
                            }

                        }
                        else if (requirement.StartTime == DateTime.MaxValue)
                        {
                            requirement.TimeLeft = -1;
                            requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                        }
                        if (Status == GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                        {
                            requirement.TimeLeft = -1;
                            requirement.Status = Status;
                        }
                        requirement.AuctionVendors = new List<LogisticVendorDetails>();
                        requirement.CustFirstName = string.Empty;
                        requirement.CustLastName = string.Empty;
                        requirement.SessionID = string.Empty;
                        requirement.ErrorMessage = string.Empty;

                        myAuctions.Add(requirement);
                    }
                }

            }
            catch (Exception ex)
            {
                LogisticRequirement auction = new LogisticRequirement();
                auction.ErrorMessage = ex.Message;

                myAuctions.Add(auction);
            }

            return myAuctions;
        }

        public List<LogisticRequirement> GetActiveLeads(int userID, string sessionID)
        {
            List<LogisticRequirement> myAuctions = new List<LogisticRequirement>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

            try
            {                
                Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("lgstc_GetActiveLeads", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LogisticRequirement requirement = new LogisticRequirement();
                        string[] arr = new string[] { };
                        byte[] next = new byte[] { };
                        requirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt16(row["REQ_ID"]) : -1;


                        requirement.CustomerID = userID;
                        requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                        if (requirement.EndTime == null)
                        {
                            requirement.EndTime = DateTime.MaxValue;
                        }
                        requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (requirement.StartTime == null)
                        {
                            requirement.StartTime = DateTime.MaxValue;
                        }
                        requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        requirement.Price = row["VEND_MIN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_PRICE"]) : 0;
                        double RunPrice = row["VEND_MIN_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_RUN_PRICE"]) : 0;
                        DateTime start = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (start < DateTime.Now && RunPrice > 0)
                        {
                            requirement.Price = RunPrice;
                        }
                        string POLink = row["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                        requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                        requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                        requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                        requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                        requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                        requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                        requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                        requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                        string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : 0;
                        requirement.SelectedVendorID = row["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt16(row["SELECTED_VENDOR_ID"]) : 0;

                        requirement.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;

                        requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;


                        DateTime now = DateTime.Now;
                        if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                        {
                            DateTime date = Convert.ToDateTime(requirement.EndTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            requirement.TimeLeft = diff;
                            requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());

                        }
                        else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                        {
                            DateTime date = Convert.ToDateTime(requirement.StartTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            requirement.TimeLeft = diff;
                            requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                        }
                        else if (requirement.EndTime < now)
                        {
                            requirement.TimeLeft = -1;
                            if ((Status == GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && requirement.EndTime < now)
                            {

                                requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                                //EndNegotiation(requirement.RequirementID, requirement.CustomerID, sessionID);
                            }
                            else
                            {
                                requirement.Status = Status;
                            }
                        }
                        else if (requirement.StartTime == DateTime.MaxValue)
                        {
                            requirement.TimeLeft = -1;
                            requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                        }
                        requirement.AuctionVendors = new List<LogisticVendorDetails>();
                        requirement.CustFirstName = string.Empty;
                        requirement.CustLastName = string.Empty;
                        requirement.SessionID = string.Empty;
                        requirement.ErrorMessage = string.Empty;

                        myAuctions.Add(requirement);
                    }
                }

            }
            catch (Exception ex)
            {
                LogisticRequirement auction = new LogisticRequirement();
                auction.ErrorMessage = ex.Message;

                myAuctions.Add(auction);
            }

            return myAuctions;
        }

        public LogisticRequirement GetRequirementData(int reqID, int userID, string sessionID)
        {
            LogisticRequirement requirement = new LogisticRequirement();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<RequirementTaxes> ListRequirementTaxes = new List<RequirementTaxes>();
            try
            {
                #region DB Connectins and Data

                Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("lgstc_GetRequirementData", sd);
                #endregion DB Connectins and Data
                requirement = GetRequirementDataFilter(reqID, userID, sessionID, ds);
            }
            catch (Exception ex)
            {
                LogisticRequirement req = new LogisticRequirement();
                req.ErrorMessage = ex.Message;
                return req;
            }

            return requirement;
        }

        public DataSet GetRequirementDataSet(int reqID, int userID, string sessionID)
        {
            DataSet ds = new DataSet();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                ds = sqlHelper.SelectList("lgstc_GetRequirementData", sd);
            }
            catch (Exception ex)
            {
                return ds;
            }

            return ds;
        }

        public LogisticRequirement GetRequirementDataFilter(int reqID, int userID, string sessionID, DataSet ds)
        {
            LogisticRequirement requirement = new LogisticRequirement();
            List<RequirementTaxes> ListRequirementTaxes = new List<RequirementTaxes>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {

                #region Requirement Details (Table 0)

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    string[] arr = new string[] { };
                    byte[] test = new byte[] { };

                    NegotiationSettings NegotiationSettings = new NegotiationSettings();

                    requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                    requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                    requirement.Subcategories = row["REQ_SUBCATEGORIES"] != DBNull.Value ? Convert.ToString(row["REQ_SUBCATEGORIES"]) : string.Empty;
                    requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                    requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                    requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                    requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                    requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                    requirement.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    requirement.CustFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.CustLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.DeliveryTime = row["REQ_DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_TIME"]) : string.Empty;
                    requirement.RequirementID = reqID;
                    int isPOSent = row["IS_PO_SENT"] != DBNull.Value ? Convert.ToInt32(row["IS_PO_SENT"]) : 0;
                    string fileName = row["REQ_ATTACHMENT"] != DBNull.Value ? Convert.ToString(row["REQ_ATTACHMENT"]) : string.Empty;
                    string POLink = row["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                    requirement.POLink = !string.IsNullOrEmpty(POLink) ? POLink : string.Empty;
                    string URL = fileName != string.Empty ? fileName : string.Empty;
                    requirement.AttachmentName = fileName;
                    requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    requirement.InclusiveTax = row["REQ_INCLUSIVE_TAX"] != DBNull.Value ? (Convert.ToInt32(row["REQ_INCLUSIVE_TAX"]) == 1 ? true : false) : false;
                    requirement.IncludeFreight = row["REQ_INCLUDE_FREIGHT"] != DBNull.Value ? (Convert.ToInt32(row["REQ_INCLUDE_FREIGHT"]) == 1 ? true : false) : false;
                    requirement.SuperUserID = row["SUPER_U_ID"] != DBNull.Value ? Convert.ToInt32(row["SUPER_U_ID"]) : requirement.CustomerID;
                    requirement.SelectedVendorID = row["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["SELECTED_VENDOR_ID"]) : requirement.CustomerID;
                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    requirement.MinReduceAmount = row["MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToInt32(row["MIN_REDUCE_AMOUNT"]) : 0;
                    requirement.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                    requirement.IsTabular = row["REQ_IS_TABULAR"] != DBNull.Value ? (Convert.ToInt32(row["REQ_IS_TABULAR"]) == 1 ? true : false) : false;
                    requirement.ReqComments = row["REQ_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REQ_COMMENTS"]) : string.Empty;
                    requirement.ExpStartTime = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.MaxValue;

                    if (requirement.MinReduceAmount > 0)
                    {
                        requirement.MinBidAmount = requirement.MinReduceAmount;
                    }
                    else
                    {
                        requirement.MinBidAmount = 0;
                    }

                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    requirement.IsStopped = row["IS_STOPPED"] != DBNull.Value ? (Convert.ToInt32(row["IS_STOPPED"]) == 1 ? true : false) : false;
                    requirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                    requirement.TimeZone = row["REQ_TIMEZONE"] != DBNull.Value ? Convert.ToString(row["REQ_TIMEZONE"]) : string.Empty;
                    requirement.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                    requirement.NegotiationDuration = row["NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["NEGOTIATION_DURATION"]) : string.Empty;
                    requirement.MinVendorComparision = row["VENDOR_COMPARISION_MIN_AMOUNT"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_COMPARISION_MIN_AMOUNT"]) : 0;
                    NegotiationSettings.MinReductionAmount = row["UD_MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_MIN_REDUCE_AMOUNT"]) : 0;
                    NegotiationSettings.RankComparision = row["UD_VENDOR_COMPARISION_MIN_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_VENDOR_COMPARISION_MIN_AMOUNT"]) : 0;
                    NegotiationSettings.NegotiationDuration = row["UD_NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["UD_NEGOTIATION_DURATION"]) : string.Empty;
                    requirement.ReqPDF = row["REQ_PDF"] != DBNull.Value ? Convert.ToInt32(row["REQ_PDF"]) : 0;
                    requirement.ReqPDFCustomer = row["REQ_PDF_CUSTOMER"] != DBNull.Value ? Convert.ToInt32(row["REQ_PDF_CUSTOMER"]) : 0;
                    requirement.ReportReq = row["REPORT_REQ"] != DBNull.Value ? Convert.ToInt32(row["REPORT_REQ"]) : 0;
                    requirement.ReportItemWise = row["REPORT_ITEM_WISE"] != DBNull.Value ? Convert.ToInt32(row["REPORT_ITEM_WISE"]) : 0;
                    requirement.ReqType = row["PARAM_REQ_TYPE"] != DBNull.Value ? Convert.ToString(row["PARAM_REQ_TYPE"]) : string.Empty;
                    requirement.PriceCapValue = row["PARAM_PRICE_CAP_VALUE"] != DBNull.Value ? Convert.ToDouble(row["PARAM_PRICE_CAP_VALUE"]) : 0;
                    requirement.CustCompID = row["CUST_COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["CUST_COMP_ID"]) : 0;
                    requirement.IsQuotationPriceLimit = row["IS_QUOTATION_PRICE_LIMIT"] != DBNull.Value ? (Convert.ToInt32(row["IS_QUOTATION_PRICE_LIMIT"]) == 1 ? true : false) : false;
                    requirement.QuotationPriceLimit = row["QUOTATION_PRICE_LIMIT"] != DBNull.Value ? Convert.ToInt32(row["QUOTATION_PRICE_LIMIT"]) : 0;
                    requirement.NoOfQuotationReminders = row["NO_OF_QUOTATION_REMINDERS"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_QUOTATION_REMINDERS"]) : 0;
                    requirement.RemindersTimeInterval = row["REMINDERS_TIME_INTERVAL"] != DBNull.Value ? Convert.ToInt32(row["REMINDERS_TIME_INTERVAL"]) : 0;
                    requirement.SuperUser = new User();
                    requirement.PostedUser = new User();
                    requirement.PostedUser.UserInfo = new UserInfo();
                    requirement.SuperUser.FirstName = row["SUPER_USER_NAME"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_NAME"]) : string.Empty;
                    requirement.SuperUser.Email = row["SUPER_USER_EMAIL"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_EMAIL"]) : string.Empty;
                    requirement.SuperUser.PhoneNum = row["SUPER_USER_PHONE"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_PHONE"]) : string.Empty;
                    requirement.PostedUser.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    requirement.PostedUser.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    requirement.PostedUser.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.PostedUser.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.PostedUser.UserInfo.Address = row["ADDRESS"] != DBNull.Value ? Convert.ToString(row["ADDRESS"]) : string.Empty;
                    requirement.IsUnitPriceBidding = row["IS_UNIT_PRICE_BIDDING"] != DBNull.Value ? Convert.ToInt32(row["IS_UNIT_PRICE_BIDDING"]) : 0;
                    requirement.MaterialDispachmentLink = row["MAT_DIS_LINK"] != DBNull.Value ? Convert.ToInt32(row["MAT_DIS_LINK"]) : 0;
                    requirement.MaterialReceivedLink = row["MR_LINK"] != DBNull.Value ? Convert.ToInt32(row["MR_LINK"]) : 0;
                    requirement.IndentID = row["INDENT_ID"] != DBNull.Value ? Convert.ToInt32(row["INDENT_ID"]) : 0;
                    requirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;
                    requirement.LastBidId = row["LAST_BID_ID"] != DBNull.Value ? Convert.ToInt16(row["LAST_BID_ID"]) : 0;
                    requirement.DaNo = row["DA_NUMBER"] != DBNull.Value ? Convert.ToString(row["DA_NUMBER"]) : string.Empty;

                    requirement.NegotiationSettings = NegotiationSettings;

                    DateTime now = DateTime.Now;
                    if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.EndTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                    }
                    else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.StartTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                    }
                    else if (requirement.EndTime < now)
                    {
                        requirement.TimeLeft = -1;
                        if ((Status == GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && requirement.EndTime < now)
                        {
                            requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                            //EndNegotiation(requirement.RequirementID, requirement.CustomerID, sessionID);
                        }
                        else
                        {
                            requirement.Status = Status;
                        }
                    }
                    else if (requirement.StartTime == DateTime.MaxValue)
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                    }
                    if (Status == GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = Status;
                    }

                    #endregion Requirement Details

                    #region Vendor Details (Table 1)

                    List<LogisticVendorDetails> vendorDetails = new List<LogisticVendorDetails>();

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row1 in ds.Tables[1].Rows)
                        {
                            LogisticVendorDetails vendor = LogisticsUtility.GetVendorsObject(row1);

                            vendorDetails.Add(vendor);
                        }
                    }

                    #endregion Vendor Details

                    #region Requirement Items (Table 2)

                    List<LogisticRequirementItems> ListRequirementItems = new List<LogisticRequirementItems>();

                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {
                            LogisticRequirementItems requirementitems = LogisticsUtility.GetReqItemsObject(row2);

                            ListRequirementItems.Add(requirementitems);
                        }
                    }

                    requirement.ListRequirementItems = ListRequirementItems;

                    #endregion Requirement Items

                    #region Quotation Items (Table 3)

                    List<LogisticQuotationItems> ListLogisticQuotationItems = new List<LogisticQuotationItems>();

                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row3 in ds.Tables[3].Rows)
                        {
                            LogisticQuotationItems QI = LogisticsUtility.GetQuotationItemsObject(row3);
                            ListLogisticQuotationItems.Add(QI);
                        }
                    }
                    requirement.Savings = Math.Round(requirement.Savings, 2);
                    #endregion Quotation Items

                    #region Maping Quotation Items With Req Items

                    #region Requirement Items for Vendor (Table 2)

                    #endregion Requirement Items for Vendor (Table 2)

                    foreach (var vd in vendorDetails)
                    {

                        List<LogisticRequirementItems> vendorLRI = new List<LogisticRequirementItems>();
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[2].Rows)
                            {
                                LogisticRequirementItems vendorRI = LogisticsUtility.GetReqItemsObject(row2);
                                vendorLRI.Add(vendorRI);
                            }
                        }

                        foreach (var VRI in vendorLRI)
                        {
                            VRI.ListQuotationItems = new List<LogisticQuotationItems>();

                            List<LogisticQuotationItems> vendorLQI = new List<LogisticQuotationItems>();
                            if (ds.Tables[3].Rows.Count > 0)
                            {
                                foreach (DataRow row3 in ds.Tables[3].Rows)
                                {
                                    LogisticQuotationItems vendorQI = LogisticsUtility.GetQuotationItemsObject(row3);
                                    vendorLQI.Add(vendorQI);
                                }
                            }

                            VRI.ListQuotationItems = vendorLQI.Where(v => v.ItemID == VRI.ItemID && v.VendorID == vd.VendorID).ToList();

                            if (VRI.ListQuotationItems.Count <= 0)
                            {
                                LogisticQuotationItems emptyObj = new LogisticQuotationItems();
                                emptyObj.RequirementID = reqID;
                                emptyObj.VendorID = vd.VendorID;
                                emptyObj.ItemID = VRI.ItemID;
                                emptyObj.IsEnabled = true;
                                VRI.ListQuotationItems.Add(emptyObj);
                            }


                        }

                        vd.ListReqItems = vendorLRI;

                    }

                    #endregion Maping Quotation Items With Req Items

                    #region vendor & customer Logics





                    requirement.CustomerReqAccess = false;
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        foreach (DataRow row4 in ds.Tables[4].Rows)
                        {
                            int reqCustomerId = row4["U_ID"] != DBNull.Value ? Convert.ToInt32(row4["U_ID"]) : 0;
                            if (userID == reqCustomerId)
                            {
                                requirement.CustomerReqAccess = true;
                            }
                        }
                    }
                    List<LogisticVendorDetails> v2 = new List<LogisticVendorDetails>();
                    if (requirement.StartTime > DateTime.Now)
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.TotalInitialPrice > 0).OrderBy(v => v.TotalInitialPrice).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.TotalInitialPrice > 0).OrderBy(v => v.TotalInitialPrice).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.InitialPrice == 0).ToList());

                    }
                    else
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.TotalRunningPrice > 0).OrderBy(v => v.TotalRunningPrice).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.TotalRunningPrice > 0).OrderBy(v => v.TotalRunningPrice).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.TotalRunningPrice == 0).ToList());

                    }
                    if (v2.Count > 0)
                    {
                        LogisticVendorDetails vendor1 = v2[0];
                        if (vendor1.TotalRunningPrice == 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else if (vendor1.TotalInitialPrice < vendor1.TotalRunningPrice && vendor1.TotalInitialPrice != 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else
                        {
                            requirement.Price = vendor1.TotalRunningPrice;
                        }
                    }
                    foreach (LogisticVendorDetails vendor in v2.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.TotalInitialPrice).ToList())
                    {
                        vendor.Rank = v2.IndexOf(vendor) + 1;
                    }
                    if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                    {
                        requirement.AuctionVendors = v2;
                    }
                    else
                    {
                        requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderBy(v => v.TotalInitialPrice).ToList();
                    }
                    requirement.SelectedVendor = requirement.AuctionVendors.FirstOrDefault(v => v.VendorID == requirement.SelectedVendorID);
                    #endregion vendor & customer Logics

                    #region Ranking

                    foreach (LogisticVendorDetails vendor in v2.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.TotalInitialPrice).ToList())
                    {
                        vendor.Rank = v2.IndexOf(vendor) + 1;
                    }
                    if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                    {
                        requirement.AuctionVendors = v2;
                    }
                    else
                    {
                        requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderBy(v => v.TotalInitialPrice).ToList();
                    }




                    if (userID > 0 && requirement.CustomerID != userID)
                    {
                        string userType = string.Empty;
                        DataSet dsType = sqlHelper.ExecuteQuery(string.Format("SELECT U_TYPE FROM userroles WHERE U_ID = {0};", userID));
                        if (dsType != null && dsType.Tables.Count > 0 && dsType.Tables[0].Rows.Count > 0 && dsType.Tables[0].Rows[0][0] != null)
                        {
                            userType = dsType.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(dsType.Tables[0].Rows[0][0].ToString()) : string.Empty;
                            if (userType == "VENDOR")
                            {
                                requirement.AuctionVendors = requirement.AuctionVendors.Where(vendor => vendor.VendorID == userID).ToList();
                            }
                        }
                    }

                    #endregion Ranking
                }
            }
            catch (Exception ex)
            {
                LogisticRequirement req = new LogisticRequirement();
                req.ErrorMessage = ex.Message;
                return req;
            }

            return requirement;
        }

        public List<LogisticHistory> GetBidHistory(int reqID, int userID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<LogisticHistory> bidHistory = new List<LogisticHistory>();
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("lgstc_GetBidHistory", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LogisticHistory history = new LogisticHistory();

                        history.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt16(row["REQ_ID"]) : -1;
                        history.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : -1;
                        history.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        history.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                        history.CreatedTime = row["BID_TIME"] != DBNull.Value ? Convert.ToDateTime(row["BID_TIME"]) : DateTime.MaxValue;
                        history.BidAmount = row["PRICE"] != DBNull.Value ? Convert.ToDouble(row["PRICE"]) : 0;
                        history.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                        history.RejectReson = row["REJECT_RESON"] != DBNull.Value ? Convert.ToString(row["REJECT_RESON"]) : string.Empty;
                        history.RevRejectReson = row["REV_REJECT_RESON"] != DBNull.Value ? Convert.ToString(row["REV_REJECT_RESON"]) : string.Empty;
                        bidHistory.Add(history);
                    }
                }
            }
            catch (Exception ex)
            {
                LogisticHistory history = new LogisticHistory();
                history.ErrorMessage = ex.Message;
                bidHistory.Add(history);
            }
            return bidHistory;
        }

        public List<Reminders> GetVendorReminders(int reqID, int userID, string sessionID)
        {
            List<Reminders> ListReminders = new List<Reminders>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("lgstc_GetVendorReminders", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Reminders reminders = new Reminders();

                        reminders.VendorIDs = row["VENDOR_ID"] != DBNull.Value ? Convert.ToString(row["VENDOR_ID"]) : string.Empty;
                        reminders.ReminderMessage = row["REMINDER_MESSAGE"] != DBNull.Value ? Convert.ToString(row["REMINDER_MESSAGE"]) : string.Empty;
                        reminders.sentOn = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MaxValue;
                        reminders.VendorCompanies = row["VENDOR_COMPANY"] != DBNull.Value ? Convert.ToString(row["VENDOR_COMPANY"]) : string.Empty;
                        reminders.RequestType = row["REQUEST_TYPE"] != DBNull.Value ? Convert.ToString(row["REQUEST_TYPE"]) : string.Empty;

                        ListReminders.Add(reminders);
                    }
                }
            }
            catch (Exception ex)
            {
                Reminders reminders = new Reminders();
                reminders.ErrorMessage = ex.Message;
                ListReminders.Add(reminders);
            }

            return ListReminders;
        }

        public List<LogisticBestPrice> ItemPreviousPrice(int companyid, string productIDorName, double netWeight, string natureOfGoods,
             string finalDestination, string sessionid)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<LogisticBestPrice> vendordetails = new List<LogisticBestPrice>();

            try
            {
                int isValidSession = Utilities.ValidateSession(sessionid);
                sd.Add("P_COMP_ID", companyid);
                sd.Add("P_PROD_ID", productIDorName);
                sd.Add("P_NET_WEIGHT", netWeight);
                sd.Add("P_NATURE_OF_GOODS", natureOfGoods);
                sd.Add("P_FINAL_DESTINATION", finalDestination);
                DataSet ds = sqlHelper.SelectList("lgstc_GetBestPrice", sd);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        LogisticBestPrice details = new LogisticBestPrice();
                        details.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        details.InitialPrice = row["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToInt32(row["REV_UNIT_PRICE"]) : 0;
                        details.CurrentTime = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]) : DateTime.Now;
                        details.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        details.Airline = row["AIRLINE"] != DBNull.Value ? Convert.ToString(row["AIRLINE"]) : string.Empty;
                        details.ProductIDorName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        details.NatureOfGoods = row["NATURE_OF_GOODS"] != DBNull.Value ? Convert.ToString(row["NATURE_OF_GOODS"]) : string.Empty;
                        details.StorageCondition = row["STORAGE_CONDITION"] != DBNull.Value ? Convert.ToString(row["STORAGE_CONDITION"]) : string.Empty;
                        details.PortOfLanding = row["PORT_OF_LANDING"] != DBNull.Value ? Convert.ToString(row["PORT_OF_LANDING"]) : string.Empty;
                        details.FinalDestination = row["FINAL_DESTINATION"] != DBNull.Value ? Convert.ToString(row["FINAL_DESTINATION"]) : string.Empty;
                        details.NetWeight = row["NET_WEIGHT"] != DBNull.Value ? Convert.ToDouble(row["NET_WEIGHT"]) : 0;
                        details.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;


                        vendordetails.Add(details);
                    }


                }
            }
            catch (Exception ex)
            {
                LogisticBestPrice details = new LogisticBestPrice();
                details.ErrorMessage = ex.Message;
                vendordetails.Add(details);
            }

            return vendordetails;
        }


        #endregion Logistic Gets

        #region Logistic Posts

        public Response RequirementSave(LogisticRequirement requirement, byte[] attachment)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            string folderPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL);

            UserDetails customerDetails = new UserDetails();

            if (requirement.CloneID > 0)
            {
                requirement.RequirementID = 0;
                foreach (var item in requirement.ListRequirementItems)
                {
                    item.ItemID = 0;
                }
            }

            try
            {
               
               Utilities.ValidateSession(requirement.SessionID);
                if (requirement.RequirementID > 0)
                {
                    LogisticRequirement oldReq = GetRequirementData(requirement.RequirementID, requirement.CustomerID, requirement.SessionID);
                    int[] vendorsToRemove = GetVendorsToRemove(requirement, oldReq);
                    foreach (int id in vendorsToRemove)
                    {
                        Response res = RemoveVendorFromAuction(id, requirement.RequirementID, requirement.SessionID, "VendoremailForRemoval", requirement);
                    }

                    int[] itemsToRemove = GetItemsToRemove(requirement, oldReq);
                    foreach (int id in itemsToRemove)
                    {
                        Response res = RemoveItemFromAuction(id, requirement.RequirementID, requirement.SessionID, "");
                    }
                }

                string fileName = string.Empty;

                List<Attachment> CustomerListAttachment = new List<Attachment>();
                List<Attachment> VendorListAttachment = new List<Attachment>();
                var filenameTemp = string.Empty;
                if (requirement.MultipleAttachments != null && requirement.MultipleAttachments.Count > 0)
                {
                    foreach (FileUpload fd in requirement.MultipleAttachments)
                    {
                        if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                        {
                            var attachName = string.Empty;
                            long tick = DateTime.Now.Ticks;
                            attachName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + tick + "_user" + requirement.CustomerID + "_" + fd.FileName);
                            SaveFile(attachName, fd.FileStream);
                            //SaveFileAsync(fileName, attachment);

                            attachName = "req" + tick + "_user" + requirement.CustomerID + "_" + fd.FileName;

                            Response res = SaveAttachment(attachName);
                            if (res.ErrorMessage != "")
                            {
                                response.ErrorMessage = res.ErrorMessage;
                            }

                            fd.FileID = res.ObjectID;


                            if (fd.FileID > 0)
                            {
                                Attachment singleAttachment = null;
                                var fileData = DownloadFile(Convert.ToString(fd.FileID), requirement.SessionID, attachment, filenameTemp);
                                if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                                {
                                    singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                                }

                                CustomerListAttachment.Add(singleAttachment);
                                VendorListAttachment.Add(singleAttachment);
                            }

                        }
                        else if (fd.FileID > 0)
                        {
                            Attachment singleAttachment = null;
                            var fileData = DownloadFile(Convert.ToString(fd.FileID), requirement.SessionID);
                            if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                            {
                                singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                            }

                            CustomerListAttachment.Add(singleAttachment);
                            VendorListAttachment.Add(singleAttachment);
                        }

                        fileName += Convert.ToString(fd.FileID) + ",";

                    }

                    fileName = fileName.Substring(0, fileName.Length - 1);

                }

                sd.Add("P_REQ_ID", requirement.RequirementID);
                sd.Add("P_REQ_TITLE", requirement.Title);
                sd.Add("P_DA_NUMBER", requirement.DaNo);
                sd.Add("P_REQ_DESC", requirement.Description);
                string categoryVal = string.Empty;
                foreach (string category in requirement.Category)
                {
                    categoryVal = category + ",";
                }
                categoryVal = categoryVal.Substring(0, categoryVal.Length - 1);
                sd.Add("P_REQ_CATEGORY", categoryVal);
                sd.Add("P_REQ_URGENCY", requirement.Urgency);
                sd.Add("P_REQ_BUDGET", requirement.Budget);
                sd.Add("P_REQ_DELIVERY_LOC", requirement.DeliveryLocation);
                sd.Add("P_REQ_TAXES", "");
                sd.Add("P_REQ_PAYMENT_TERMS", requirement.PaymentTerms);
                sd.Add("P_REQ_SUBCATEGORIES", requirement.Subcategories);
                sd.Add("P_U_ID", requirement.CustomerID);
                sd.Add("P_CLOSED", requirement.IsClosed);
                sd.Add("P_REQ_ATTACHMENT", fileName);
                sd.Add("P_END_TIME", requirement.EndTime);
                sd.Add("P_START_TIME", requirement.StartTime);
                sd.Add("P_DELIVERY_TIME", requirement.DeliveryTime);
                requirement.IncludeFreight = true;
                sd.Add("P_INCLUDE_FREIGHT", Convert.ToInt16(requirement.IncludeFreight));
                requirement.InclusiveTax = true;
                sd.Add("P_INCLUSIVE_TAX", Convert.ToInt16(requirement.InclusiveTax));
                sd.Add("P_REQ_COMMENTS", requirement.ReqComments);
                if (requirement.QuotationFreezTime == DateTime.MaxValue || requirement.QuotationFreezTime == DateTime.MinValue)
                {
                    requirement.QuotationFreezTime = DateTime.MaxValue;
                }

                sd.Add("P_QUOTATION_FREEZ_TIME", requirement.QuotationFreezTime);
                sd.Add("P_REQ_CURRENCY", requirement.Currency);
                sd.Add("P_REQ_TIMEZONE", requirement.TimeZoneID);
                sd.Add("P_REQ_IS_TABULAR", Convert.ToInt16(requirement.IsTabular));
                sd.Add("P_IS_QUOTATION_PRICE_LIMIT", Convert.ToInt32(requirement.IsQuotationPriceLimit));
                sd.Add("P_QUOTATION_PRICE_LIMIT", requirement.QuotationPriceLimit);
                sd.Add("P_NO_OF_QUOTATION_REMINDERS", requirement.NoOfQuotationReminders);
                sd.Add("P_REMINDERS_TIME_INTERVAL", requirement.RemindersTimeInterval);
                sd.Add("P_INDENT_ID", requirement.IndentID);
                sd.Add("P_EXP_START_TIME", requirement.ExpStartTime);
                sd.Add("P_IS_DISCOUNT_QUOTATION", requirement.IsDiscountQuotation);
                int isUnitPriceBidding = 0;
                if (requirement.IsTabular)
                {
                    isUnitPriceBidding = 1;
                }

                sd.Add("P_IS_UNIT_PRICE_BIDDING", Convert.ToInt32(isUnitPriceBidding));
                DataSet ds = sqlHelper.SelectList("lgstc_RequirementSave", sd);

                Attachment emilAttachmentReqPDF = null;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (requirement.RequirementID > 0 && requirement.DeleteQuotations)
                    {
                        string query = string.Format("DELETE FROM quotationreminders WHERE REQ_ID = {0};" +
                            " DELETE FROM quotationtaxes WHERE REQ_ID = {0};DELETE FROM logistic_quotations WHERE REQ_ID = {0};" +
                            " UPDATE logistic_auctiondetails SET IS_ACCEPTED = 0, VEND_INIT_PRICE = 0, VEND_RUN_PRICE = 0, QUOTATION_URL = null, " +
                            " VEND_TAXES = 0, VEND_TOTAL_PRICE = 0, VEND_TOTAL_PRICE_RUNNING = 0, IS_QUOTATION_REJECTED = 0, " +
                            " QUOTATION_REJECTED_REASON = '', VEND_FREIGHT = '', WARRANTY = '', PAYMENT = '', DURATION = '', " +
                            " VALIDITY = '', DISCOUNT = 0, NO_OF_QUOTATION_REMINDERS_SENT = 0, " +
                            //" MULTIPLE_ATTACHMENTS = null, " + 
                            " IS_QUOTATION_REJECTED = -1, " +
                            " OTHER_PROPERTIES = '', REV_PRICE = 0, " +
                            " REV_VEND_TOTAL_PRICE = 0 WHERE REQ_ID = {0}", requirement.RequirementID);

                        DataSet ds1 = sqlHelper.ExecuteQuery(query);
                    }
                }

                string quotationItems = string.Empty;
                if (requirement.ItemsAttachment == null)
                {
                    foreach (LogisticRequirementItems Item in requirement.ListRequirementItems)
                    {
                        Response response2 = new Response();
                        response2 = SaveRequirementItems(Item, response.ObjectID, requirement.SessionID, requirement.CustomerID);

                        if (response2.ErrorMessage == "")
                        {
                            if (requirement.IsTabular)
                            {
                                string xml = string.Empty;
                                xml = Utilities.GenerateEmailBody("SaveRequirementItemsXML");
                                xml = String.Format(xml, Item.ProductIDorName, Item.ProductNo, Item.ProductDescription, Item.ProductQuantity, Item.ProductBrand, Item.OthersBrands);
                                quotationItems += xml;
                            }
                        }
                    }
                }
                else
                {
                    DataTable currentData = new DataTable();
                    string sheetName = string.Empty;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        ms.Write(requirement.ItemsAttachment, 0, requirement.ItemsAttachment.Length);
                        using (ExcelPackage package = new ExcelPackage(ms))
                        {
                            currentData = Utilities.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                            sheetName = package.Workbook.Worksheets[1].Name;
                        }
                    }

                    if (sheetName.Equals("RequirementDetails", StringComparison.InvariantCultureIgnoreCase))
                    {
                        int count = 1;
                        foreach (DataRow row in currentData.Rows)
                        {
                            try
                            {
                                Response response2 = new Response();
                                LogisticRequirementItems item = new LogisticRequirementItems();
                                item.ItemID = (row.IsNull("ItemID") || row["ItemID"] == DBNull.Value || string.IsNullOrEmpty(row["ItemID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["ItemID"].ToString().Trim());
                                item.ProductIDorName = (row.IsNull("ProductName") || row["ProductName"] == DBNull.Value) ? string.Empty : Convert.ToString(row["ProductName"]).Trim();
                                item.ProductNo = (row.IsNull("ProductNumber") || row["ProductNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(row["ProductNumber"]).Trim();
                                item.ProductDescription = (row.IsNull("Description") || row["Description"] == DBNull.Value) ? string.Empty : Convert.ToString(row["Description"]).Trim();
                                item.ProductQuantity = (row.IsNull("Quantity") || row["Quantity"] == DBNull.Value || string.IsNullOrEmpty(row["Quantity"].ToString().Trim())) ? 0 : Convert.ToDouble(row["Quantity"].ToString().Trim());
                                item.ProductQuantityIn = (row.IsNull("Units") || row["Units"] == DBNull.Value) ? string.Empty : Convert.ToString(row["Units"]).Trim();
                                item.ProductBrand = (row.IsNull("PreferredBrand") || row["PreferredBrand"] == DBNull.Value) ? string.Empty : Convert.ToString(row["PreferredBrand"]).Trim();
                                item.OthersBrands = (row.IsNull("OtherBrands") || row["OtherBrands"] == DBNull.Value) ? string.Empty : Convert.ToString(row["OtherBrands"]).Trim();
                                response2 = SaveRequirementItems(item, response.ObjectID, requirement.SessionID, requirement.CustomerID);
                                count++;
                                if (response2.ErrorMessage == string.Empty)
                                {
                                    if (requirement.IsTabular)
                                    {
                                        string xml = string.Empty;
                                        xml = Utilities.GenerateEmailBody("SaveRequirementItemsXML");
                                        xml = String.Format(xml, item.ProductIDorName, item.ProductNo, item.ProductDescription, item.ProductQuantity, item.ProductBrand, item.OthersBrands);
                                        quotationItems += xml;
                                    }
                                }
                            }
                            catch
                            {
                                response.Message += "Row No. " + count + " has invalid values. Please check the excel sheet you uploaded.";
                                continue;
                            }
                        }
                    }
                }

                if (requirement.AuctionVendors.Count > 0)
                {
                    foreach (LogisticVendorDetails vendor in requirement.AuctionVendors)
                    {
                        Response response1 = new Response();
                        response1 = AddVendorToAuction(vendor, response.ObjectID, requirement.SessionID, requirement.CustomerID);
                    }
                }

                Task.Factory.StartNew(() =>
                {
                    customerDetails = GetUserDetails(requirement.CustomerID, requirement.SessionID);

                    if (requirement.AuctionVendors.Count > 0)
                    {
                        Attachment PRM360_VENDOR_USER_MANUAL = null;
                        UserInfo customerInfo = GetUserNew(requirement.CustomerID, requirement.SessionID);
                        foreach (LogisticVendorDetails vendor in requirement.AuctionVendors)
                        {

                            Response response1 = new Response();
                            response1.ObjectID = response.ObjectID;
                            response1.ErrorMessage = "";
                            if (ds.Tables[0].Rows[0][1] != null && response1.ErrorMessage == "" && requirement.IsSubmit == 1)
                            {
                                string body = string.Empty;
                                UserInfo user = GetUserNew(vendor.VendorID, requirement.SessionID);

                                User altUser = GetAlternateCommunications(requirement.CustomerID, vendor.VendorID, requirement.SessionID);

                                if (requirement.RequirementID < 1)
                                {

                                    string ScreenName = "ADD_REQUIREMENT";
                                    if (requirement.CheckBoxEmail == true)
                                    {
                                        body = Utilities.GenerateEmailBody("Vendoremail");
                                        body = String.Format(body, user.FirstName, user.LastName, response.ObjectID, requirement.Title, requirement.QuotationFreezTime, customerInfo.Institution, requirement.ExpStartTime);
                                        prmServices.SendEmailLGST(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "You have a new Requirement -ReqID: " + response.ObjectID + " Title: " + requirement.Title, body, requirement.SessionID, emilAttachmentReqPDF, VendorListAttachment).ConfigureAwait(false);
                                    }
                                    string body1 = Utilities.GenerateEmailBody("Vendorsms");
                                    if (requirement.CheckBoxSms == true)
                                    {

                                        body1 = String.Format(body1, user.FirstName, user.LastName, response.ObjectID, requirement.Title, requirement.QuotationFreezTime, customerInfo.Institution, requirement.ExpStartTime);
                                        body1 = body1.Replace("<br/>", "");
                                      //  prmServices.SendSMSLGST(string.Empty, user.PhoneNum + "," + user.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), requirement.SessionID).ConfigureAwait(false);
                                    }
                                    PushNotifications push = new PushNotifications();
                                    push.Message = body1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0];
                                    push.MessageType = ScreenName;
                                    push.RequirementID = response.ObjectID;
                                    prmServices.SendPushNotificationAsync(push, user).ConfigureAwait(false);
                                }
                                else
                                {
                                    if (response1.ObjectID < 0)
                                    {

                                        string ScreenName = "ADD_REQUIREMENT";
                                        if (requirement.CheckBoxEmail == true)
                                        {
                                            body = Utilities.GenerateEmailBody("Vendoremail");
                                            body = String.Format(body, user.FirstName, user.LastName, response.ObjectID, requirement.Title, requirement.QuotationFreezTime, customerInfo.Institution, requirement.ExpStartTime);
                                            prmServices.SendEmailLGST(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "You have a new Reqquirement - ReqID: " + response.ObjectID + " Title: " + requirement.Title, body, requirement.SessionID, emilAttachmentReqPDF, VendorListAttachment).ConfigureAwait(false);
                                        }
                                        string body1 = Utilities.GenerateEmailBody("Vendorsms");
                                        if (requirement.CheckBoxSms == true)
                                        {
                                            body1 = String.Format(body1, user.FirstName, user.LastName, response.ObjectID, requirement.Title, requirement.QuotationFreezTime, customerInfo.Institution, requirement.ExpStartTime);
                                            body1 = body1.Replace("<br/>", "");
                                           // prmServices.SendSMSLGST(string.Empty, user.PhoneNum + "," + user.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), requirement.SessionID).ConfigureAwait(false);
                                        }
                                        PushNotifications push = new PushNotifications();
                                        push.Message = body1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0];
                                        push.MessageType = ScreenName;
                                        push.RequirementID = response.ObjectID;

                                        prmServices.SendPushNotificationAsync(push, user).ConfigureAwait(false);

                                    }
                                    else
                                    {
                                        if (requirement.CheckBoxEmail == true)
                                        {
                                            body = Utilities.GenerateEmailBody("VendoremailForReqUpdate");
                                            body = String.Format(body, user.FirstName, user.LastName, requirement.RequirementID, requirement.Title, requirement.QuotationFreezTime, requirement.ExpStartTime);
                                            prmServices.SendEmailLGST(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "Requirement details updated for ReqID: " + response.ObjectID + " Title: " + requirement.Title, body, requirement.SessionID, emilAttachmentReqPDF, VendorListAttachment).ConfigureAwait(false);

                                        }
                                        if (requirement.CheckBoxSms == true)
                                        {
                                            string ScreenName = "UPDATE_REQUIREMENT";
                                            string body1 = Utilities.GenerateEmailBody("VendorsmsForReqUpdate");
                                            body1 = String.Format(body1, user.FirstName, user.LastName, requirement.RequirementID, requirement.Title, requirement.QuotationFreezTime, requirement.ExpStartTime);
                                            body1 = body1.Replace("<br/>", "");
                                           // prmServices.SendSMSLGST(string.Empty, user.PhoneNum + "," + user.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), requirement.SessionID).ConfigureAwait(false);
                                            PushNotifications push = new PushNotifications();
                                            push.Message = body1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0];
                                            push.MessageType = ScreenName;
                                            push.RequirementID = response.ObjectID;
                                            prmServices.SendPushNotificationAsync(push, user).ConfigureAwait(false);
                                        }
                                    }
                                }
                            }
                            if (response1.ErrorMessage != "")
                            {
                                throw new Exception(response1.ErrorMessage);
                            }
                        }
                    }

                    Attachment emilAttachmentReqPDFCustomer = null;
                    if (requirement.IsSubmit == 1)
                    {
                        LogisticRequirement req = GetRequirementData(response.ObjectID, requirement.CustomerID, requirement.SessionID);
                        UserInfo customer = GetUserNew(req.CustomerID, requirement.SessionID);
                        UserInfo superUser = GetSuperUser(req.SuperUserID, requirement.SessionID);
                        string emailBody = string.Empty;
                        emailBody = Utilities.GenerateEmailBody("PostRequirementemail");
                        string emailBody1 = String.Format(emailBody, customer.FirstName, customer.LastName, response.ObjectID, requirement.Title, requirement.QuotationFreezTime, requirement.Currency, requirement.ExpStartTime);
                        string emailBody2 = String.Format(emailBody, superUser.FirstName, superUser.LastName, response.ObjectID, requirement.Title, requirement.QuotationFreezTime, requirement.Currency, requirement.ExpStartTime);
                        prmServices.SendEmailLGST(customer.Email + "," + customer.AltEmail, "Your Requirement has been successfully posted! ReqID: " + response.ObjectID + " Title: " + requirement.Title, emailBody1, requirement.SessionID, emilAttachmentReqPDFCustomer, CustomerListAttachment).ConfigureAwait(false);
                        string body2 = Utilities.GenerateEmailBody("PostRequirementsms");
                        string body4 = String.Format(body2, customer.FirstName, customer.LastName, response.ObjectID, requirement.Title, requirement.QuotationFreezTime, requirement.Currency, requirement.ExpStartTime);
                        string body3 = String.Format(body2, superUser.FirstName, superUser.LastName, response.ObjectID, requirement.Title, requirement.QuotationFreezTime, requirement.Currency, requirement.ExpStartTime);
                        body3 = body3.Replace("<br/>", "");
                        body4 = body4.Replace("<br/>", "");
                       // prmServices.SendSMSLGST(string.Empty, customer.PhoneNum + "," + customer.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body4.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), requirement.SessionID).ConfigureAwait(false);
                        string bodyTelegram1 = Utilities.GenerateEmailBody("SaveRequirementTelegramsms");
                        string bodyTelegram = String.Format(bodyTelegram1, req.CustomerCompanyName, req.CustFirstName, req.CustLastName, req.Title, req.Description, req.PaymentTerms, req.Budget, req.DeliveryTime, requirement.QuotationFreezTime, requirement.Currency, requirement.ExpStartTime);
                        bodyTelegram = bodyTelegram.Replace("<br/>", "");
                        TelegramMsg tgMsg = new TelegramMsg();
                        tgMsg.Message = "REQUIREMENT POSTED: REQ ID :" + response.ObjectID + (bodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]);
                        prmServices.SendTelegramMsg(tgMsg);
                        if (req.SuperUserID != req.CustomerID)
                        {
                            string ScreenName = "ADD_REQUIREMENT";
                            prmServices.SendEmailLGST(superUser.Email + "," + superUser.AltEmail, "Your Requirement has been successfully posted! ReqID: " + response.ObjectID + " Title: " + requirement.Title, emailBody2, requirement.SessionID, emilAttachmentReqPDFCustomer, CustomerListAttachment).ConfigureAwait(false);
                           // prmServices.SendSMSLGST(string.Empty, superUser.PhoneNum + "," + superUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body3.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), requirement.SessionID).ConfigureAwait(false);
                            PushNotifications push = new PushNotifications();
                            push.Message = body3.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0];
                            push.MessageType = ScreenName;
                            push.RequirementID = response.ObjectID;
                            prmServices.SendPushNotificationAsync(push, superUser).ConfigureAwait(false);
                        }
                    }

                    int remindersCount = 0;
                    DateTime inputQuotationFreezeTime = DateTime.Now;
                    if (requirement.IsSubmit == 1)
                    {
                        Response responseSaveQuotationReminders = prmServices.SaveQuotationReminders(response.ObjectID, inputQuotationFreezeTime, "DELETE");
                    }

                    if (requirement.IsSubmit == 1)
                    {
                        remindersCount = requirement.NoOfQuotationReminders;
                        DateTime createdQuotationFreezTime = Convert.ToDateTime(requirement.QuotationFreezTime); ;
                        inputQuotationFreezeTime = Convert.ToDateTime(requirement.QuotationFreezTime);

                        for (int i = 1; i <= remindersCount; i++)
                        {
                            inputQuotationFreezeTime = prmServices.QuotationReminders(inputQuotationFreezeTime, requirement.RemindersTimeInterval, 9, 19, createdQuotationFreezTime);
                            Response responseSaveQuotationReminders = prmServices.SaveQuotationReminders(response.ObjectID, inputQuotationFreezeTime, "SAVE");
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response AddVendorToAuction(LogisticVendorDetails vendor, int reqID, string sessionID, int userID)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", vendor.VendorID);
                sd.Add("P_INIT_PRICE", vendor.InitialPrice);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_CREATED_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("lgstc_AddVendorToAuction", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SubmitQuotation(List<LogisticRequirementItems> listLogisticReqItems, List<LogisticQuotationItems> listDeletedQuotationItems,
        LogisticVendorDetails vendorDetails, string sessionID, bool isRevised, bool isMakeBid = false)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(sessionID);
                string query = string.Empty;
                foreach (var QI in listDeletedQuotationItems)
                {
                    query += string.Format("UPDATE logistic_quotations SET IS_DELETED = 1 WHERE QUOT_ID = {0};", QI.QuotationItemID);
                }

                DataSet ds = sqlHelper.ExecuteQuery(query);
                double quotaionPrice = 0;
                foreach (var RI in listLogisticReqItems)
                {
                    ds = new DataSet();
                    quotaionPrice += RI.ListQuotationItems[0].ItemPrice;
                    foreach (var QI in RI.ListQuotationItems)
                    {
                        ds = LogisticsUtility.SubmitQuotationObj(QI);
                    }

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    }
                }

                if (!isMakeBid && vendorDetails.RequirementID > 0 && vendorDetails.VendorID > 0)
                {
                    #region

                    if (isRevised)
                    {
                        query = string.Format("UPDATE logistic_auctiondetails SET WARRANTY = '{2}', VALIDITY = '{3}',OTHER_PROPERTIES = '{4}', " +
                            "PAYMENT = '{5}', DURATION = '{6}', REV_VEND_TOTAL_PRICE = {7}," +
                            "IS_REV_QUOTATION_REJECTED = {8} " +
                            "WHERE REQ_ID = {0} AND U_ID = {1};" +
                            "INSERT INTO logistic_audit(REQ_ID, U_ID, PRICE, BID_TIME) VALUES({0}, {1}, {7}, NOW());",
                            vendorDetails.RequirementID, vendorDetails.VendorID, vendorDetails.Warranty, vendorDetails.Validity,
                            vendorDetails.OtherProperties, vendorDetails.Payment, vendorDetails.Duration,
                            vendorDetails.RevVendorTotalPrice, -1);

                        ds = sqlHelper.ExecuteQuery(query);
                    }
                    else if (vendorDetails.RequirementID > 0 && vendorDetails.VendorID > 0)
                    {
                        if (vendorDetails.IsSent == 0)
                        {
                            quotaionPrice = 0;
                        }

                        query = string.Format("UPDATE logistic_auctiondetails " +
                            "SET VEND_INIT_PRICE = {2}, VEND_RUN_PRICE = {2}, VEND_TOTAL_PRICE = {2}, " +
                            "VEND_TOTAL_PRICE_RUNNING = {2}, REV_PRICE = {2}, REV_VEND_TOTAL_PRICE = {2}, " +
                            "IS_QUOTATION_REJECTED = {3}, IS_REV_QUOTATION_REJECTED = {3}," +
                            "WARRANTY = '{4}', VALIDITY = '{5}',OTHER_PROPERTIES = '{6}', " +
                            "PAYMENT = '{7}', DURATION = '{8}', " +
                            "IS_ACCEPTED_TC = {9}, IS_SENT = {10} " +
                            "WHERE REQ_ID = {0} AND U_ID = {1};" +
                            "INSERT INTO logistic_audit(REQ_ID, U_ID, PRICE, BID_TIME) VALUES({0}, {1}, {2}, NOW());",
                            vendorDetails.RequirementID, vendorDetails.VendorID, quotaionPrice, -1,
                            vendorDetails.Warranty, vendorDetails.Validity,
                            vendorDetails.OtherProperties, vendorDetails.Payment, vendorDetails.Duration, vendorDetails.IsAcceptedTC, vendorDetails.IsSent);

                        ds = sqlHelper.ExecuteQuery(query);
                    }

                    #endregion

                    signalR(vendorDetails.RequirementID, vendorDetails.VendorID, sessionID, "UPLOAD_QUOTATION");
                    LogisticRequirement req = new LogisticRequirement();
                    req = GetRequirementData(vendorDetails.RequirementID, vendorDetails.VendorID, sessionID);
                    alertsForRequirementActions(req.CustomerID, vendorDetails.VendorID, req, sessionID, "UPLOAD_QUOTATION");
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveAirlines(List<LogisticRequirementItems> listLogisticReqItems, string sessionID, bool isRevised)
        {
            string query = string.Empty;
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(sessionID);
                int i = 0;
                double quotaionPrice = 0;
                int reqID = 0;
                int vendorID = 0;
                if (!isRevised)
                {
                    foreach (var RI in listLogisticReqItems)
                    {
                        foreach (var QI in RI.ListQuotationItems)
                        {
                            if (QI.IsSelected > 0)
                            {
                                query += string.Format("UPDATE logistic_quotations SET IS_SELECTED = 1 WHERE QUOT_ID = {0};", QI.QuotationItemID);
                                if (i == 0)
                                {
                                    reqID = QI.RequirementID;
                                    vendorID = QI.VendorID;
                                }

                                i++;
                                quotaionPrice += QI.ItemPrice;
                            }
                            else
                            {
                                query += string.Format("UPDATE logistic_quotations SET IS_DELETED = 1 WHERE QUOT_ID = {0};", QI.QuotationItemID);                                
                            }
                        }

                        DataSet ds = sqlHelper.ExecuteQuery(query);
                    }

                    if (i > 0 && reqID > 0 && vendorID > 0)
                    {

                        query = string.Format("UPDATE logistic_auctiondetails " +
                            "SET VEND_INIT_PRICE = {2}, VEND_RUN_PRICE = {2}, VEND_TOTAL_PRICE = {2}, VEND_TOTAL_PRICE_RUNNING = {2}, " +
                            "REV_PRICE = {2}, REV_VEND_TOTAL_PRICE = {2}, IS_QUOTATION_REJECTED = {3} WHERE REQ_ID = {0} AND U_ID = {1};" +
                            "INSERT INTO logistic_audit(REQ_ID, U_ID, PRICE, BID_TIME) VALUES({0}, {1}, {2}, NOW());",
                            reqID, vendorID, quotaionPrice, 0);

                        DataSet ds = sqlHelper.ExecuteQuery(query);

                    }
                }
                else if (isRevised)
                {

                    reqID = listLogisticReqItems[0].ListQuotationItems[0].RequirementID;
                    vendorID = listLogisticReqItems[0].ListQuotationItems[0].VendorID;
                    query = string.Format("UPDATE logistic_auctiondetails " +
                            "SET IS_REV_QUOTATION_REJECTED = 0 WHERE REQ_ID = {0} AND U_ID = {1};",
                            reqID, vendorID);
                    DataSet ds = sqlHelper.ExecuteQuery(query);
                }

                LogisticRequirement req = new LogisticRequirement();
                req = GetRequirementData(reqID, vendorID, sessionID);

                if (!isRevised)
                {
                    alertsForRequirementActions(vendorID, req.CustomerID, req, sessionID, "APPROVE_QUOTATION");
                }
                else if (isRevised)
                {
                    alertsForRequirementActions(vendorID, req.CustomerID, req, sessionID, "APPROVE_REV_QUOTATION");
                }

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response RejectQuotation(int reqID, int userID, bool isRevised, string sessionID, string quotationRejectedComment, string revQuotationRejectedComment)
        {
            Response response = new Response();
            try
            {
                string query = string.Empty;
                Utilities.ValidateSession(sessionID);

                if (!isRevised)
                {
                    query = string.Format("UPDATE logistic_auctiondetails,logistic_audit " +
                            "SET IS_QUOTATION_REJECTED = 1, QUOTATION_REJECTED_REASON = '{2}',logistic_audit.REJECT_RESON = '{2}' " +
                            "WHERE logistic_auctiondetails.REQ_ID = {0} AND logistic_auctiondetails.U_ID = {1} " +
                            "AND logistic_audit.REQ_ID = {0} AND logistic_audit.U_ID = {1};",
                            reqID, userID, quotationRejectedComment); 
                    DataSet ds = sqlHelper.ExecuteQuery(query);
                }
                else if (isRevised)
                {
                    query = string.Format("UPDATE logistic_auctiondetails,logistic_audit " +
                            "SET IS_REV_QUOTATION_REJECTED = 1, REV_QUOTATION_REJECTED_REASON = '{2}',logistic_audit.REV_REJECT_RESON = '{2}' " +
                            "WHERE logistic_auctiondetails.REQ_ID = {0} AND logistic_auctiondetails.U_ID = {1} " +
                            "AND logistic_audit.REQ_ID = {0} AND logistic_audit.U_ID = {1};",
                            reqID, userID, revQuotationRejectedComment);

                    DataSet ds = sqlHelper.ExecuteQuery(query);
                }

                LogisticRequirement req = new LogisticRequirement();
                req = GetRequirementData(reqID, userID, sessionID);
                if (!isRevised)
                {
                    alertsForRequirementActions(userID, req.CustomerID, req, sessionID, "REJECT_QUOTATION");
                }
                else if (isRevised)
                {
                    alertsForRequirementActions(userID, req.CustomerID, req, sessionID, "REJECT_REV_QUOTATION");
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdateAuctionStart(int reqID, int userID, DateTime date, string sessionID, NegotiationSettings negotiationSettings)
        {
            string moreInfo = string.Empty;
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
               Utilities.ValidateSession(sessionID);
                List<Attachment> vendorAttachments = new List<Attachment>();
                string negotiationDuration = "0 0:35";
                negotiationDuration = negotiationDuration + ":00.0";
                
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_NEW_TIME", date);
                sd.Add("P_U_ID", userID);
                sd.Add("PARAM_END_TIME", negotiationDuration);
                sd.Add("P_MIN_REDUCE_AMOUNT", negotiationSettings.MinReductionAmount);
                sd.Add("P_VENDOR_COMPARISION_MIN_AMOUNT", negotiationSettings.RankComparision);
                sd.Add("P_NEGOTIATION_DURATION", negotiationSettings.NegotiationDuration);
                DataSet ds = sqlHelper.SelectList("lgstc_UpdateAuctionStart", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1].ToString();
                    if (response.ErrorMessage != "")
                    {
                        response.TimeLeft = -1;
                    }
                    else
                    {
                        long diff = Convert.ToInt64((date - DateTime.Now).TotalSeconds);
                        response.TimeLeft = diff;
                        signalR(reqID, userID, sessionID, "UPDATE_START_TIME");
                    }
                }

                LogisticRequirement req = new LogisticRequirement();
                req = GetRequirementData(reqID, userID, sessionID);
                foreach (var AV in req.AuctionVendors)
                {
                    alertsForRequirementActions(AV.VendorID, req.CustomerID, req, sessionID, "UPDATE_START_TIME");
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = moreInfo + ex.Message + ex.StackTrace;
            }

            return response;
        }

        public Response signalRFrontEnd(int reqID, int userID, string sessionID, string methodName)
        {
            Response response = new Response();
            signalR(reqID, userID, sessionID, methodName);
            return response;
        }

        public Response StartNegotiation(int reqID, int userID, string sessionID)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("lgstc_StartNegotiation", sd);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    //Task.Factory.StartNew(() => signalR(reqID, userID, sessionID, "START_NEGOTIATION"));
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdateBidTime(int reqID, int userID, long newTicks, string sessionID)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_CLOSE", newTicks);
                sd.Add("P_NEW_TIME", DateTime.Now.AddSeconds(newTicks));
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("lgstc_UpdateBidTime", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {
                        signalR(reqID, userID, sessionID, "UPDATE_BID_TIME");
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response StopBids(int reqID, int userID, string sessionID)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("lgstc_StopBids", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {
                        signalR(reqID, userID, sessionID, "STOP_BIDS");
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response RestartNegotiation(int reqID, int userID, string sessionID)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("lgstc_AddTimeToNegotiation", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    signalR(reqID, userID, sessionID, "RESTART_NEGOTIATION");
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response MakeBid(int reqID, int userID, double bidPrice, List<LogisticRequirementItems> listRequirementItems,
        LogisticVendorDetails vendorDetails, string sessionID)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_PRICE", bidPrice);
                DataSet ds = sqlHelper.SelectList("lgstc_MakeBid", sd);
                signalR(reqID, userID, sessionID, "MAKE_BID");
                List<LogisticQuotationItems> listQuotationItems = new List<LogisticQuotationItems>();
                Task.Factory.StartNew(() => SubmitQuotation(listRequirementItems, listQuotationItems, vendorDetails, sessionID, false, true));
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            return response;
        }

        public Response SavePriceCap(int reqID, string sessionID, int userID, string reqType, double priceCapValue, int isUnitPriceBidding)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_TYPE", reqType);
                sd.Add("P_PRICE_CAP", priceCapValue);
                sd.Add("P_IS_UNIT_PRICE_BIDDING", isUnitPriceBidding);
                DataSet ds = sqlHelper.SelectList("lgstc_SavePriceCap", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdatePriceCap(int uID, int reqID, double price, string sessionID)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", uID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_PRICE", price);
                DataSet ds = sqlHelper.SelectList("lgstc_UpdateSingleVendorPrice", sd);
                response.ObjectID = uID;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response VendorReminders(int reqID, int userID, string message, int[] vendorIDs, string[] vendorCompanyNames, string sessionID, string requestType)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            string VendorUID = "";
            string VendorCompanies = "";
            try
            {
                Utilities.ValidateSession(sessionID);
                LogisticRequirement requirement = GetRequirementData(reqID, userID, sessionID);
                UserDetails customer = GetUserDetails(userID, sessionID);
                foreach (int vendorID in vendorIDs)
                {
                    UserDetails vendor = GetUserDetails(vendorID, sessionID);
                    User altUser = GetAlternateCommunications(reqID, vendorID, sessionID);
                    if (requestType == "REMINDER")
                    {
                        string emailBody = string.Empty;
                        emailBody = Utilities.GenerateEmailBody("VendoremailForReqReminder");
                        emailBody = String.Format(emailBody, vendor.FirstName, vendor.LastName, reqID, requirement.Title, message, customer.FirstName, customer.LastName, customer.CompanyName);
                        SendEmail(vendor.Email + "," + vendor.AltEmail + "," + altUser.AltEmail, "ReqID: " + reqID + " Title: " + requirement.Title + " - Gentle Reminder from Customer Regarding Requirement : " + requirement.Title, emailBody, sessionID).ConfigureAwait(false);

                        string body2 = Utilities.GenerateEmailBody("VendorsmsForReqReminder");
                        body2 = String.Format(body2, vendor.FirstName, vendor.LastName, reqID, requirement.Title, message, customer.FirstName, customer.LastName, customer.CompanyName);
                        body2 = body2.Replace("<br/>", "");
                       // SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), sessionID).ConfigureAwait(false);
                    }
                    else if (requestType == "INTRO")
                    {
                        string emailBody = string.Empty;
                        emailBody = Utilities.GenerateEmailBody("VendoremailForIntro");
                        emailBody = String.Format(emailBody, vendor.FirstName, vendor.LastName, reqID, requirement.Title, message, customer.FirstName, customer.LastName, customer.CompanyName);
                        SendEmail(vendor.Email + "," + vendor.AltEmail + "," + altUser.AltEmail, "ReqID: " + reqID + " Title: " + requirement.Title + " - Gentle Reminder from Customer Regarding Requirement : " + requirement.Title, emailBody, sessionID).ConfigureAwait(false);

                        string body2 = Utilities.GenerateEmailBody("VendorsmsForIntro");
                        body2 = String.Format(body2, vendor.FirstName, vendor.LastName, reqID, requirement.Title, message, customer.FirstName, customer.LastName, customer.CompanyName);
                        body2 = body2.Replace("<br/>", "");
                      //  SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), sessionID).ConfigureAwait(false);
                    }


                    VendorUID += vendorID + ",";
                }

                VendorUID = VendorUID.Substring(0, VendorUID.Length - 1);

                foreach (string vendorCompanyName in vendorCompanyNames)
                {
                    VendorCompanies += vendorCompanyName + ",";
                }

                VendorCompanies = VendorCompanies.Substring(0, VendorCompanies.Length - 1);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REMINDER_MESSAGE", message);
                sd.Add("P_VENDOR_ID", VendorUID);
                sd.Add("P_VENDOR_COMPANY", VendorCompanies);
                sd.Add("P_REQUEST_TYPE", requestType);
                DataSet ds = sqlHelper.SelectList("lgstc_SaveVendorReminders", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion Logistic Posts

        #region Logistic Private

        private LogisticRequirement GetRequirementDataOfflinePrivate(int reqID, int userID, string sessionID)
        {
            LogisticRequirement requirement = new LogisticRequirement();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("lgstc_GetRequirementData", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    string[] arr = new string[] { };
                    byte[] test = new byte[] { };
                    requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                    requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                    requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                    requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                    requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                    requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                    requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                    requirement.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : 0;
                    requirement.CustFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.CustLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.DeliveryTime = row["REQ_DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_TIME"]) : string.Empty;
                    requirement.RequirementID = reqID;
                    int isPOSent = row["IS_PO_SENT"] != DBNull.Value ? Convert.ToInt16(row["IS_PO_SENT"]) : 0;
                    //requirement.AttachmentName = row["REQ_ATTACHMENT"] != DBNull.Value ? Convert.ToBase64String((byte[])row["REQ_ATTACHMENT"]) : string.Empty;
                    string fileName = row["REQ_ATTACHMENT"] != DBNull.Value ? Convert.ToString(row["REQ_ATTACHMENT"]) : string.Empty;
                    string URL = fileName != string.Empty ? fileName : string.Empty;
                    requirement.AttachmentName = URL;
                    requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    requirement.SuperUserID = row["SUPER_U_ID"] != DBNull.Value ? Convert.ToInt32(row["SUPER_U_ID"]) : requirement.CustomerID;
                    requirement.SelectedVendorID = row["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["SELECTED_VENDOR_ID"]) : requirement.CustomerID;
                    requirement.IsStopped = row["IS_STOPPED"] != DBNull.Value ? (Convert.ToInt32(row["IS_STOPPED"]) == 1 ? true : false) : false;
                    requirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;

                    requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                    requirement.TimeZone = row["REQ_TIMEZONE"] != DBNull.Value ? Convert.ToString(row["REQ_TIMEZONE"]) : string.Empty;

                    requirement.CustCompID = row["CUST_COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["CUST_COMP_ID"]) : 0;

                    requirement.IsQuotationPriceLimit = row["IS_QUOTATION_PRICE_LIMIT"] != DBNull.Value ? (Convert.ToInt32(row["IS_QUOTATION_PRICE_LIMIT"]) == 1 ? true : false) : false;
                    requirement.QuotationPriceLimit = row["QUOTATION_PRICE_LIMIT"] != DBNull.Value ? Convert.ToInt32(row["QUOTATION_PRICE_LIMIT"]) : 0;
                    requirement.NoOfQuotationReminders = row["NO_OF_QUOTATION_REMINDERS"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_QUOTATION_REMINDERS"]) : 0;
                    requirement.RemindersTimeInterval = row["REMINDERS_TIME_INTERVAL"] != DBNull.Value ? Convert.ToInt32(row["REMINDERS_TIME_INTERVAL"]) : 0;

                    requirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;

                    DateTime now = DateTime.Now;
                    if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.EndTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());

                    }
                    else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.StartTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                    }
                    else if (requirement.EndTime < now)
                    {
                        requirement.TimeLeft = -1;
                    }
                    else if (requirement.StartTime == DateTime.MaxValue)
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                    }
                    List<LogisticVendorDetails> vendorDetails = new List<LogisticVendorDetails>();
                    int rank = 1;
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row1 in ds.Tables[1].Rows)
                        {
                            LogisticVendorDetails vendor = new LogisticVendorDetails();

                            vendor.VendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                            vendor.VendorName = row1["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row1["VENDOR_NAME"]) : string.Empty;
                            vendor.InitialPrice = row1["VEND_INIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE"]) : 0;
                            vendor.RunningPrice = row1["VEND_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_RUN_PRICE"]) : 0;
                            vendor.City = row1["UAD_CITY"] != DBNull.Value ? Convert.ToString(row1["UAD_CITY"]) : string.Empty;
                            vendor.Taxes = row1["TAXES"] != DBNull.Value ? Convert.ToDouble(row1["TAXES"]) : 0;

                            vendor.TotalInitialPrice = row1["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE"]) : 0;
                            vendor.TotalRunningPrice = row1["VEND_TOTAL_PRICE_RUNNING"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE_RUNNING"]) : 0;
                            vendor.Rating = row1["RATING"] != DBNull.Value ? Convert.ToDouble(row1["RATING"]) : 0;

                            vendor.Taxes = row1["VEND_TAXES"] != DBNull.Value ? Convert.ToInt32(row1["VEND_TAXES"]) : 0;
                            vendor.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;

                            vendor.InitialPriceWithOutTaxFreight = row1["VEND_INIT_PRICE_WITHOUT_TAX"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE_WITHOUT_TAX"]) : 0;
                            vendor.VendorFreight = row1["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row1["VEND_FREIGHT"]) : 0;

                            vendor.Warranty = row1["WARRANTY"] != DBNull.Value ? Convert.ToString(row1["WARRANTY"]) : string.Empty;
                            vendor.Payment = row1["PAYMENT"] != DBNull.Value ? Convert.ToString(row1["PAYMENT"]) : string.Empty;
                            vendor.Duration = row1["DURATION"] != DBNull.Value ? Convert.ToString(row1["DURATION"]) : string.Empty;
                            vendor.Validity = row1["VALIDITY"] != DBNull.Value ? Convert.ToString(row1["VALIDITY"]) : string.Empty;

                            vendor.OtherProperties = row1["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row1["OTHER_PROPERTIES"]) : string.Empty;

                            vendor.IsQuotationRejected = row1["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_QUOTATION_REJECTED"]) : 0;
                            vendor.QuotationRejectedComment = row1["QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_REJECTED_REASON"]) : string.Empty;
                            vendor.PO = new RequirementPO();

                            vendor.PO.POLink = row1["PO_ATT_ID"] != DBNull.Value ? Convert.ToString(row1["PO_ATT_ID"]) : "0";

                            string fileName1 = row1["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_URL"]) : string.Empty;
                            vendor.Rating = row1["RATING"] != DBNull.Value ? Convert.ToDouble(row1["RATING"]) : 0;
                            string URL1 = fileName1 != string.Empty ? fileName1 : string.Empty;
                            vendor.QuotationUrl = URL1;

                            string fileName2 = row1["REV_QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_URL"]) : string.Empty;
                            string URL2 = fileName2 != string.Empty ? fileName2 : string.Empty;
                            vendor.RevQuotationUrl = URL2;


                            vendor.RevPrice = row1["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_PRICE"]) : 0;
                            vendor.RevVendorFreight = row1["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_FREIGHT"]) : 0;
                            vendor.RevVendorTotalPrice = row1["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE"]) : 0;

                            vendor.IsRevQuotationRejected = row1["IS_REV_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_REV_QUOTATION_REJECTED"]) : 0;
                            vendor.RevQuotationRejectedComment = row1["REV_QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_REJECTED_REASON"]) : string.Empty;
                            vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            vendor.Discount = row1["DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row1["DISCOUNT"]) : 0;
                            vendor.ReductionPercentage = row1["REDUCTION_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["REDUCTION_PERCENTAGE"]) : 0;

                            vendor.Vendor = new User();

                            vendor.Vendor.Email = row1["U_EMAIL"] != DBNull.Value ? Convert.ToString(row1["U_EMAIL"]) : string.Empty;
                            vendor.Vendor.PhoneNum = row1["U_PHONE"] != DBNull.Value ? Convert.ToString(row1["U_PHONE"]) : string.Empty;

                            vendor.PO.MaterialDispachmentLink = row1["MAT_DIS_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MAT_DIS_LINK"]) : 0;
                            vendor.PO.MaterialReceivedLink = row1["MR_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MR_LINK"]) : 0;

                            vendor.LandingPrice = row1["LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["LANDING_PRICE"]) : 0;
                            vendor.RevLandingPrice = row1["REV_LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["REV_LANDING_PRICE"]) : 0;
                            vendor.SumOfMargin = row1["PARAM_SUM_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_MARGIN"]) : 0;
                            vendor.SumOfInitialMargin = row1["PARAM_SUM_INITIAL_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_INITIAL_MARGIN"]) : 0;


                            vendor.LandingPrice = Math.Round(vendor.LandingPrice, 2);
                            vendor.RevLandingPrice = Math.Round(vendor.RevLandingPrice, 2);

                            vendor.ReductionPercentage = Math.Round(vendor.ReductionPercentage, 3);

                            vendor.Rank = rank;

                            vendorDetails.Add(vendor);
                            rank++;
                        }
                    }


                    if (requirement.IsDiscountQuotation == 2)
                    {
                        decimal maxInitialMargin = 0;
                        decimal maxClosedMargin = 0;

                        try
                        {
                            maxInitialMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfInitialMargin);
                            maxClosedMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfMargin);
                        }
                        catch
                        {
                            maxInitialMargin = 0;
                            maxClosedMargin = 0;
                        }


                        requirement.Savings = Convert.ToDouble(maxClosedMargin - maxInitialMargin);
                        requirement.Savings = Math.Round(requirement.Savings, 2);
                    }

                    requirement.AuctionVendors = vendorDetails;
                    List<LogisticVendorDetails> v2 = new List<LogisticVendorDetails>();
                    if (requirement.StartTime > DateTime.Now)
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.TotalInitialPrice > 0).OrderBy(v => v.TotalInitialPrice).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.TotalInitialPrice > 0).OrderBy(v => v.TotalInitialPrice).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.InitialPrice == 0).ToList());

                    }
                    else
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.TotalRunningPrice > 0).OrderBy(v => v.TotalRunningPrice).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.TotalRunningPrice > 0).OrderBy(v => v.TotalRunningPrice).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.TotalRunningPrice == 0).ToList());

                    }
                    if (v2.Count > 0)
                    {
                        LogisticVendorDetails vendor1 = v2[0];
                        if (vendor1.TotalRunningPrice == 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else if (vendor1.TotalInitialPrice < vendor1.TotalRunningPrice && vendor1.TotalInitialPrice != 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else
                        {
                            requirement.Price = vendor1.TotalRunningPrice;
                        }
                    }
                    foreach (LogisticVendorDetails vendor in v2.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.TotalInitialPrice).ToList())
                    {
                        vendor.Rank = v2.IndexOf(vendor) + 1;
                    }
                    if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                    {
                        requirement.AuctionVendors = v2;
                    }
                    else
                    {
                        requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderBy(v => v.TotalInitialPrice).ToList();
                    }

                    requirement.SelectedVendor = requirement.AuctionVendors.FirstOrDefault(v => v.VendorID == requirement.SelectedVendorID);

                    requirement.CustomerReqAccess = false;

                    if (ds.Tables[5].Rows.Count > 0)
                    {
                        foreach (DataRow row5 in ds.Tables[5].Rows)
                        {
                            int reqCustomerId = row5["U_ID"] != DBNull.Value ? Convert.ToInt32(row5["U_ID"]) : 0;

                            if (userID == reqCustomerId)
                            {
                                requirement.CustomerReqAccess = true;
                            }
                        }
                    }


                    int productSNo = 0;

                    List<LogisticRequirementItems> ListRequirementItems = new List<LogisticRequirementItems>();
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {

                            LogisticRequirementItems RequirementItems = new LogisticRequirementItems();
                            RequirementItems.ProductSNo = productSNo++;
                            RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                            RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                            RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                            RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                            RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                            RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                            RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;

                            ListRequirementItems.Add(RequirementItems);
                        }
                    }


                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[3].Rows)
                        {

                            int uID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                            int itemid = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;

                            double price = row2["PRICE"] != DBNull.Value ? Convert.ToDouble(row2["PRICE"]) : 0;
                            string name = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            string no = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            string des = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            string brand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;


                            if (uID == userID || requirement.CustomerReqAccess == true)
                            {
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<LogisticRequirementItems>().ItemPrice = price;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<LogisticRequirementItems>().ProductIDorName = name;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<LogisticRequirementItems>().ProductNo = no;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<LogisticRequirementItems>().ProductDescription = des;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<LogisticRequirementItems>().ProductBrand = brand;
                            }
                        }
                    }
                    requirement.ListRequirementItems = ListRequirementItems;

                    if (requirement.IsDiscountQuotation == 2)
                    {

                    }
                    if (requirement.IsDiscountQuotation == 1 || requirement.IsDiscountQuotation == 0)
                    {
                        foreach (LogisticVendorDetails vendor in v2.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.TotalInitialPrice).ToList())
                        {
                            vendor.Rank = v2.IndexOf(vendor) + 1;
                        }
                        if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                        {
                            requirement.AuctionVendors = v2;
                        }
                        else
                        {
                            requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderBy(v => v.TotalInitialPrice).ToList();
                        }
                    }
                    else
                    {



                        v2 = v2.OrderByDescending(v => v.SumOfMargin).ToList();

                        foreach (LogisticVendorDetails vendor in v2)
                        {

                            if (vendor.IsQuotationRejected == 0 && vendor.SumOfMargin > 0)
                            {
                                vendor.Rank = v2.IndexOf(vendor) + 1;
                            }
                        }


                        if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                        {

                            v2 = v2.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.Rank).ToList();

                            foreach (LogisticVendorDetails vendor in requirement.AuctionVendors)
                            {
                                if (vendor.IsQuotationRejected != 0)
                                {
                                    v2.Add(vendor);
                                }
                            }
                        }
                        else
                        {
                            v2 = v2.Where(v => v.VendorID == userID).OrderByDescending(v => v.SumOfMargin).ToList();
                        }

                        requirement.AuctionVendors = v2;


                    }

                    int taxSNo = 0;

                    List<RequirementTaxes> ListRequirementTaxes = new List<RequirementTaxes>();
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        foreach (DataRow row4 in ds.Tables[4].Rows)
                        {
                            RequirementTaxes requirementTaxes = new RequirementTaxes();
                            requirementTaxes.TaxSNo = taxSNo++;
                            int uID = row4["U_ID"] != DBNull.Value ? Convert.ToInt32(row4["U_ID"]) : 0;
                            if (uID == userID || requirement.CustomerReqAccess == true)
                            {
                                requirementTaxes.TaxID = row4["QT_ID"] != DBNull.Value ? Convert.ToInt32(row4["QT_ID"]) : 0;
                                requirementTaxes.TaxName = row4["TAX_NAME"] != DBNull.Value ? Convert.ToString(row4["TAX_NAME"]) : string.Empty;
                                requirementTaxes.TaxPercentage = row4["TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row4["TAX_PERCENTAGE"]) : 0;
                                requirementTaxes.IsTaxDeleted = row4["IS_TAX_DELETED"] != DBNull.Value ? Convert.ToInt32(row4["IS_TAX_DELETED"]) : 0;

                                ListRequirementTaxes.Add(requirementTaxes);
                            }
                        }
                        requirement.TaxSNoCount = taxSNo;
                    }
                    requirement.ListRequirementTaxes = ListRequirementTaxes;

                }
            }
            catch (Exception ex)
            {
                LogisticRequirement req = new LogisticRequirement();
                req.ErrorMessage = ex.Message;
                return req;
            }
            finally
            {
                // cmd.Connection.Close();
            }

            return requirement;
        }

        private int[] GetVendorsToRemove(LogisticRequirement requirement, LogisticRequirement oldReq)
        {
            List<int> oldVendorList = oldReq.AuctionVendors.Select(x => x.VendorID).ToList();
            List<int> newVendorList = requirement.AuctionVendors.Select(x => x.VendorID).ToList();
            int[] removedVendors = oldVendorList.Except(newVendorList).ToList().ToArray();
            return removedVendors;
        }

        private Response RemoveVendorFromAuction(int id, int reqID, string sessionID, string templateName, LogisticRequirement requirement)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            string ScreenName = "DELETE_VENDOR";
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", id);

                DataSet ds = sqlHelper.SelectList("lgstc_RemoveVendorFromAuction", sd);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {
                        UserInfo user = prmServices.GetUser(id, sessionID);
                        string body = Utilities.GenerateEmailBody("VendoremailForRemoval");
                        if (requirement.CheckBoxEmail == true)
                        {
                            body = String.Format(body, user.FirstName, user.LastName, reqID);
                            prmServices.SendEmailLGST(user.Email + "," + user.AltEmail, "ReqID: " + reqID + " - Please ignore the following requirement", body, sessionID).ConfigureAwait(false);
                        }
                        string body2 = Utilities.GenerateEmailBody("VendorsmsForRemoval");
                        if (requirement.CheckBoxSms == true)
                        {
                            body2 = String.Format(body2, user.FirstName, user.LastName, reqID);
                            body2 = body2.Replace("<br/>", "");
                          //  prmServices.SendSMSLGST(string.Empty, user.PhoneNum + "," + user.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), sessionID).ConfigureAwait(false);
                        }
                        PushNotifications push1 = new PushNotifications();
                        push1.Message = (body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]);
                        push1.MessageType = ScreenName;
                        push1.RequirementID = reqID;
                        prmServices.SendPushNotificationAsync(push1, user).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        private int[] GetItemsToRemove(LogisticRequirement requirement, LogisticRequirement oldReq)
        {
            List<int> oldItemsList = oldReq.ListRequirementItems.Select(x => x.ItemID).ToList();
            List<int> newItemsList = requirement.ListRequirementItems.Select(x => x.ItemID).ToList();
            int[] removedItems = oldItemsList.Except(newItemsList).ToList().ToArray();
            return removedItems;
        }

        private Response RemoveItemFromAuction(int id, int reqID, string sessionID, string templateName)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_ITEM_ID", id);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("lgstc_RemoveItemFromAuction", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        private Response SaveRequirementItems(LogisticRequirementItems RequirementItems, int reqID, string sessionID, int userID)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                string fileName = string.Empty;
                if (!string.IsNullOrEmpty(RequirementItems.AttachmentBase64))
                {
                    string[] stringSeparators = new string[] { "base64," };
                    string tempAttach = RequirementItems.AttachmentBase64;
                    if (RequirementItems.AttachmentBase64.ToLower().Contains("base64"))
                    {
                        tempAttach = RequirementItems.AttachmentBase64.Split(stringSeparators, StringSplitOptions.None)[1];
                    }

                    RequirementItems.ItemAttachment = Convert.FromBase64String(tempAttach);
                }

                if (RequirementItems.ItemAttachment != null && RequirementItems.ItemAttachment.Length > 0)
                {
                    long tick = DateTime.Now.Ticks;
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + tick + "_user" + userID + "_" + RequirementItems.AttachmentName);
                    SaveFile(fileName, RequirementItems.ItemAttachment);

                    fileName = "req" + tick + "_user" + userID + "_" + RequirementItems.AttachmentName;
                    Response res = SaveAttachment(fileName);
                    if (res.ErrorMessage != "")
                    {
                        response.ErrorMessage = res.ErrorMessage;
                    }
                    fileName = res.ObjectID.ToString();
                }
                else if (RequirementItems.ProductImageID > 0)
                {
                    fileName = Convert.ToString(RequirementItems.ProductImageID);
                }

                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_ITEM_ID", RequirementItems.ItemID);
                sd.Add("P_PROD_ID", RequirementItems.ProductIDorName);
                sd.Add("P_PROD_NO", RequirementItems.ProductNo);
                sd.Add("P_DESCRIPTION", RequirementItems.ProductDescription);
                sd.Add("P_QUANTITY", RequirementItems.ProductQuantity);
                sd.Add("P_QUANTITY_IN", RequirementItems.ProductQuantityIn);
                sd.Add("P_BRAND", RequirementItems.ProductBrand);
                sd.Add("P_OTHER_BRAND", RequirementItems.OthersBrands);
                sd.Add("P_TYPE_OF_PACKING", RequirementItems.TypeOfPacking);
                sd.Add("P_MODE_OF_SHIPMENT", RequirementItems.ModeOfShipment);
                sd.Add("P_NATURE_OF_GOODS", RequirementItems.NatureOfGoods);
                sd.Add("P_STORAGE_CONDITION", RequirementItems.StorageCondition);
                sd.Add("P_PORT_OF_LANDING", RequirementItems.PortOfLanding);
                sd.Add("P_FINAL_DESTINATION", RequirementItems.FinalDestination);
                sd.Add("P_DELIVERY_LOCATION", RequirementItems.DeliveryLocation);
                sd.Add("P_PREFERRED_AIRLINES", RequirementItems.PreferredAirlines);
                sd.Add("P_IMAGE_ID", fileName);
                sd.Add("P_U_ID", userID);
                sd.Add("P_IS_DELETED", RequirementItems.IsDeleted);
                sd.Add("P_IS_PALLETIZE", RequirementItems.Ispalletize);
                sd.Add("P_NET_WEIGHT", RequirementItems.NetWeight);
                sd.Add("P_PALLETIZE_QTY", RequirementItems.PalletizeQty);
                sd.Add("P_PALLETIZE_LENGTH", RequirementItems.PalletizeLength);
                sd.Add("P_PALLETIZE_BREADTH", RequirementItems.PalletizeBreadth);
                sd.Add("P_PALLETIZE_HEIGHT", RequirementItems.PalletizeHeight);
                sd.Add("P_CATALOGUE_ID", RequirementItems.CATALOGUE_ID);
                DataSet ds = sqlHelper.SelectList("lgstc_SaveRequirementItems", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion Logistic Private

        #region Logistic Alerts

        public User GetAlternateCommunications(int customerID, int vendorID, string sessionID)
        {
            User user = new User();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                sd.Add("P_CUSTOMER_ID", customerID);
                sd.Add("P_VENDOR_ID", vendorID);
                DataSet ds = sqlHelper.SelectList("lgstc_GetAlternateCommunications", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        user.AltPhoneNum += row["U_PHONE"] != DBNull.Value ? row["U_PHONE"].ToString() : string.Empty;
                        user.AltPhoneNum += ",";
                        user.AltEmail += row["U_EMAIL"] != DBNull.Value ? row["U_EMAIL"].ToString() : string.Empty;
                        user.AltEmail += ",";
                    }
                }
            }
            catch (Exception ex)
            {
                user.ErrorMessage = ex.Message;
            }
            return user;
        }

        public Response alertsForRequirementActions(int toID, int fromID, LogisticRequirement req, string sessionID, string action)
        {
            Response response = new Response();
            UserInfo userFrom = GetUserNew(fromID, sessionID);
            UserInfo userTo = GetUserNew(toID, sessionID);


            try
            {
                string emailBody = string.Empty;
                string smsBody = string.Empty;
                string bodyTelegram1 = string.Empty;
                string bodyTelegram = string.Empty;

                if (action == "UPLOAD_QUOTATION")
                {
                    User altUser = GetAlternateCommunications(toID, fromID, sessionID);

                    emailBody = Utilities.GenerateEmailBody("LGSTCS_QuotationUploadCustomer_EMAIL");
                    emailBody = String.Format(emailBody, userTo.FirstName, userTo.LastName, userTo.Institution,
                        userFrom.FirstName, userFrom.LastName, userFrom.Institution, req.RequirementID, req.Title);
                    SendEmail(userTo.Email + "," + userTo.AltEmail,
                        "Quotation Uploaded - ReqID: " + req.RequirementID + " Title: " + req.Title,
                        emailBody, sessionID, null, null).ConfigureAwait(false);

                    smsBody = Utilities.GenerateEmailBody("LGSTCS_QuotationUploadCustomer_SMS");
                    smsBody = String.Format(smsBody, userTo.FirstName, userTo.LastName, userTo.Institution,
                        userFrom.FirstName, userFrom.LastName, userFrom.Institution, req.RequirementID, req.Title);
                    smsBody = smsBody.Replace("<br/>", "");
                    //SendSMS(string.Empty, userTo.PhoneNum + "," + userTo.AltPhoneNum,
                    //    System.Web.HttpUtility.UrlEncode(smsBody.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]),
                    //    sessionID).ConfigureAwait(false);

                    bodyTelegram1 = Utilities.GenerateEmailBody("LGSTCS_QuotationUploadCustomer_SMS");
                    bodyTelegram = String.Format(bodyTelegram1, userTo.FirstName, userTo.LastName, userTo.Institution,
                        userFrom.FirstName, userFrom.LastName, userFrom.Institution, req.RequirementID, req.Title);
                    bodyTelegram = bodyTelegram.Replace("<br/>", "");
                    TelegramMsg tgMsg = new TelegramMsg();
                    tgMsg.Message = "Quotation Uploaded - ReqID: " + req.RequirementID + " Title: " + req.Title + " " + (bodyTelegram.Split(new string[] { "Thank You" },
                        StringSplitOptions.None)[0]);
                    prmServices.SendTelegramMsg(tgMsg);
                }
                else if (action == "APPROVE_QUOTATION" || action == "APPROVE_REV_QUOTATION")
                {
                    string subText = "";

                    if (action == "APPROVE_REV_QUOTATION")
                    {
                        subText = " Revised ";
                    }

                    User altUser = GetAlternateCommunications(fromID, toID, sessionID);

                    emailBody = Utilities.GenerateEmailBody("LGSTCS_QuotationApproved_EMAIL");
                    emailBody = String.Format(emailBody, userTo.FirstName, userTo.LastName, userTo.Institution,
                        userFrom.FirstName, userFrom.LastName, userFrom.Institution, req.RequirementID, req.Title);
                    SendEmail(userTo.Email + "," + userTo.AltEmail + "," + altUser.AltEmail,
                        subText + "Quotation Approved - ReqID: " + req.RequirementID + " Title: " + req.Title,
                        emailBody, sessionID, null, null).ConfigureAwait(false);

                    smsBody = Utilities.GenerateEmailBody("LGSTCS_QuotationApproved_SMS");
                    smsBody = String.Format(smsBody, userTo.FirstName, userTo.LastName, userTo.Institution,
                        userFrom.FirstName, userFrom.LastName, userFrom.Institution, req.RequirementID, req.Title);
                    smsBody = smsBody.Replace("<br/>", "");
                    //SendSMS(string.Empty, userTo.PhoneNum + "," + userTo.AltPhoneNum + "," + altUser.AltPhoneNum,
                    //    System.Web.HttpUtility.UrlEncode(smsBody.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]),
                    //    sessionID).ConfigureAwait(false);

                    bodyTelegram1 = Utilities.GenerateEmailBody("LGSTCS_QuotationApproved_SMS");
                    bodyTelegram = String.Format(bodyTelegram1, userTo.FirstName, userTo.LastName, userTo.Institution,
                        userFrom.FirstName, userFrom.LastName, userFrom.Institution, req.RequirementID, req.Title);
                    bodyTelegram = bodyTelegram.Replace("<br/>", "");
                    TelegramMsg tgMsg = new TelegramMsg();
                    tgMsg.Message = subText + "Quotation Approved - ReqID: " + req.RequirementID + " Title: " + req.Title + " " + (bodyTelegram.Split(new string[] { "Thank You" },
                        StringSplitOptions.None)[0]);
                    prmServices.SendTelegramMsg(tgMsg);
                }
                else if (action == "REJECT_QUOTATION" || action == "REJECT_REV_QUOTATION")
                {
                    string subText = "";

                    if (action == "REJECT_REV_QUOTATION")
                    {
                        subText = " Revised ";
                    }

                    User altUser = GetAlternateCommunications(fromID, toID, sessionID);

                    emailBody = Utilities.GenerateEmailBody("LGSTCS_QuotationRejected_EMAIL");
                    emailBody = String.Format(emailBody, userTo.FirstName, userTo.LastName, userTo.Institution,
                        userFrom.FirstName, userFrom.LastName, userFrom.Institution, req.RequirementID, req.Title);
                    SendEmail(userTo.Email + "," + userTo.AltEmail + "," + altUser.AltEmail,
                        subText + "Quotation Rejected - ReqID: " + req.RequirementID + " Title: " + req.Title,
                        emailBody, sessionID, null, null).ConfigureAwait(false);

                    smsBody = Utilities.GenerateEmailBody("LGSTCS_QuotationRejected_SMS");
                    smsBody = String.Format(smsBody, userTo.FirstName, userTo.LastName, userTo.Institution,
                        userFrom.FirstName, userFrom.LastName, userFrom.Institution, req.RequirementID, req.Title);
                    smsBody = smsBody.Replace("<br/>", "");
                    //SendSMS(string.Empty, userTo.PhoneNum + "," + userTo.AltPhoneNum + "," + altUser.AltPhoneNum,
                    //    System.Web.HttpUtility.UrlEncode(smsBody.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]),
                    //    sessionID).ConfigureAwait(false);

                    bodyTelegram1 = Utilities.GenerateEmailBody("LGSTCS_QuotationRejected_SMS");
                    bodyTelegram = String.Format(bodyTelegram1, userTo.FirstName, userTo.LastName, userTo.Institution,
                        userFrom.FirstName, userFrom.LastName, userFrom.Institution, req.RequirementID, req.Title);
                    bodyTelegram = bodyTelegram.Replace("<br/>", "");
                    TelegramMsg tgMsg = new TelegramMsg();
                    tgMsg.Message = subText + "Quotation Rejected - ReqID: " + req.RequirementID + " Title: " + req.Title + " " + (bodyTelegram.Split(new string[] { "Thank You" },
                        StringSplitOptions.None)[0]);
                    prmServices.SendTelegramMsg(tgMsg);
                }
                else if (action == "UPDATE_START_TIME")
                {
                    User altUser = GetAlternateCommunications(fromID, toID, sessionID);
                    emailBody = Utilities.GenerateEmailBody("LGSTCS_UpdateStarttime_EMAIL");
                    emailBody = String.Format(emailBody, userTo.FirstName, userTo.LastName, userTo.Institution,
                        userFrom.FirstName, userFrom.LastName, userFrom.Institution, req.RequirementID, req.Title, req.StartTime);
                    SendEmail(userTo.Email + "," + userTo.AltEmail + "," + altUser.AltEmail,
                        "Start time updated - ReqID: " + req.RequirementID + " Title: " + req.Title,
                        emailBody, sessionID, null, null).ConfigureAwait(false);

                    smsBody = Utilities.GenerateEmailBody("LGSTCS_UpdateStarttime_SMS");
                    smsBody = String.Format(smsBody, userTo.FirstName, userTo.LastName, userTo.Institution,
                        userFrom.FirstName, userFrom.LastName, userFrom.Institution, req.RequirementID, req.Title, req.StartTime);
                    smsBody = smsBody.Replace("<br/>", "");
                    //SendSMS(string.Empty, userTo.PhoneNum + "," + userTo.AltPhoneNum + "," + altUser.AltPhoneNum,
                    //    System.Web.HttpUtility.UrlEncode(smsBody.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]),
                    //    sessionID).ConfigureAwait(false);

                    bodyTelegram1 = Utilities.GenerateEmailBody("LGSTCS_UpdateStarttime_SMS");
                    bodyTelegram = String.Format(bodyTelegram1, userTo.FirstName, userTo.LastName, userTo.Institution,
                        userFrom.FirstName, userFrom.LastName, userFrom.Institution, req.RequirementID, req.Title, req.StartTime);
                    bodyTelegram = bodyTelegram.Replace("<br/>", "");
                    TelegramMsg tgMsg = new TelegramMsg();
                    tgMsg.Message = "Start time updated - ReqID: " + req.RequirementID + " Title: " + req.Title + " " + (bodyTelegram.Split(new string[] { "Thank You" },
                        StringSplitOptions.None)[0]);
                    prmServices.SendTelegramMsg(tgMsg);
                }

            }
            catch (Exception ex)
            {

            }

            return response;
        }

        #endregion Logistic Alerts

        #endregion Logistic

        #region PRMServices

        #region PRMServices Gets

        public Response GetAttachment(string attachmentID, string sessionID)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_ATT_ID", attachmentID);

                DataSet ds = sqlHelper.SelectList("cp_GetAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.Message = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][0].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public FileData GetAttachmentBase64(string attachmentID, string sessionID)
        {
            FileData filedata = new FileData();

            byte[] fileContent = null;
            String strFileData = null;
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_ATT_ID", attachmentID);
                DataSet ds = sqlHelper.SelectList("cp_GetAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    string message = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][0].ToString()) : string.Empty;

                    using (WebClient client = new WebClient())
                    {
                        fileContent = client.DownloadData("http://www.prm360.com" + Utilities.FILE_URL + message);
                        filedata.FileContent = Convert.ToBase64String(fileContent);
                        filedata.FileName = message;
                        filedata.FileExt = System.IO.Path.GetExtension(filedata.FileName);
                        filedata.ID = attachmentID;
                    }
                }
            }
            catch (Exception ex)
            {
                filedata.ErrorMessage = ex.Message;
                return null;
            }

            return filedata;
        }

        public UserDetails GetUserDetails(int userID, string sessionID)
        {
            PRMServices services = new PRMServices();
            return services.GetUserDetails(userID, sessionID);
        }

        public UserInfo GetSuperUser(int userID, string sessionID)
        {
            UserInfo user = new UserInfo();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_GetSuperUserDetails", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    user.UserID = row["U_ID"] != DBNull.Value ? row["U_ID"].ToString() : string.Empty;
                    user.FirstName = row["U_FNAME"] != DBNull.Value ? row["U_FNAME"].ToString() : string.Empty;
                    user.FirstName = row["U_FNAME"] != DBNull.Value ? row["U_FNAME"].ToString() : string.Empty;
                    user.LastName = row["U_LNAME"] != DBNull.Value ? row["U_LNAME"].ToString() : string.Empty;
                    user.Email = row["U_EMAIL"] != DBNull.Value ? row["U_EMAIL"].ToString() : string.Empty;
                    user.Username = row["LG_LOGIN"] != DBNull.Value ? row["LG_LOGIN"].ToString() : string.Empty;
                    user.PhoneNum = row["U_PHONE"] != DBNull.Value ? row["U_PHONE"].ToString() : string.Empty;
                    user.AddressLine1 = row["UAD_LINE1"] != DBNull.Value ? row["UAD_LINE1"].ToString() : string.Empty;
                    user.AddressLine2 = row["UAD_LINE2"] != DBNull.Value ? row["UAD_LINE2"].ToString() : string.Empty;
                    user.AddressLine3 = row["UAD_LINE3"] != DBNull.Value ? row["UAD_LINE3"].ToString() : string.Empty;
                    user.AddressPhoneNum = row["UAD_PHONE"] != DBNull.Value ? row["UAD_PHONE"].ToString() : string.Empty;
                    user.City = row["UAD_CITY"] != DBNull.Value ? row["UAD_CITY"].ToString() : string.Empty;
                    user.State = row["UAD_STATE"] != DBNull.Value ? row["UAD_STATE"].ToString() : string.Empty;
                    user.Country = row["UAD_COUNTRY"] != DBNull.Value ? row["UAD_COUNTRY"].ToString() : string.Empty;
                    user.Extension1 = row["UAD_EXT1"] != DBNull.Value ? row["UAD_EXT1"].ToString() : string.Empty;
                    user.Extension2 = row["UAD_EXT2"] != DBNull.Value ? row["UAD_EXT2"].ToString() : string.Empty;
                    user.Institution = row["INSTITUTION"] != DBNull.Value ? row["INSTITUTION"].ToString() : string.Empty;
                    user.UserData1 = row["UD_DATA1"] != DBNull.Value ? row["UD_DATA1"].ToString() : string.Empty;
                    user.UserType = row["U_TYPE"] != DBNull.Value ? row["U_TYPE"].ToString() : string.Empty;
                    user.ZipCode = row["UAD_ZIP"] != DBNull.Value ? row["UAD_ZIP"].ToString() : string.Empty;
                    user.Birthday = row["U_BIRTHDATE"] != DBNull.Value ? Convert.ToDateTime(row["U_BIRTHDATE"]) : DateTime.MaxValue;
                    user.isOTPVerified = row["isOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isOTPVerified"]) : 0;
                    user.isEmailOTPVerified = row["isEmailOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isEmailOTPVerified"]) : 0;
                    user.CredentialsVerified = row["CREDENTIALS_VERIFIED"] != DBNull.Value ? Convert.ToInt32(row["CREDENTIALS_VERIFIED"]) : 0;
                    user.RegistrationScore = row["U_REG_SCORE"] != DBNull.Value ? Convert.ToInt32(row["U_REG_SCORE"]) : 0;
                    user.IsSuperUser = row["IS_SUPER_USER"] != DBNull.Value ? (Convert.ToInt32(row["IS_SUPER_USER"]) == 1 ? true : false) : false;
                    user.AltEmail = row["U_ALTEMAIL"] != DBNull.Value ? row["U_ALTEMAIL"].ToString() : string.Empty;
                    user.AltPhoneNum = row["U_ALTPHONE"] != DBNull.Value ? row["U_ALTPHONE"].ToString() : string.Empty;
                }
            }
            catch (Exception ex)
            {
                user.ErrorMessage = ex.Message;
            }

            return user;
        }

        public List<CategoryObj> GetUserSubCategories(int userID, string sessionID)
        {
            List<CategoryObj> Categories = new List<CategoryObj>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_SESSION_ID", sessionID);
                DataSet ds = sqlHelper.SelectList("cp_GetUserSubCategoriesForUser", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        CategoryObj cat = new CategoryObj();
                        cat.ID = row["CAT_ID"] != DBNull.Value ? Convert.ToInt32(row["CAT_ID"]) : 0;
                        cat.Category = row["CATEGORY"] != DBNull.Value ? Convert.ToString(row["CATEGORY"]) : string.Empty;
                        cat.Subcategory = row["SUBCATEGORY"] != DBNull.Value ? Convert.ToString(row["SUBCATEGORY"]) : string.Empty;
                        Categories.Add(cat);
                    }
                }
            }
            catch (Exception ex)
            {
                CategoryObj c = new CategoryObj();
                c.ErrorMessage = ex.Message;
                Categories.Add(c);
            }
            return Categories;
        }

        public UserInfo GetUserNew(int userID, string sessionID)
        {
            UserInfo user = new UserInfo();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

            try
            {
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_GetUserData", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    UserInfo vendorDetails = GetCompanyVendorInfo(userID, sessionID);

                    DataRow row = ds.Tables[0].Rows[0];
                    user.UserID = userID.ToString();
                    user.SessionID = sessionID;
                    user.FirstName = row["U_FNAME"] != DBNull.Value ? row["U_FNAME"].ToString() : string.Empty;
                    //user.FirstName = row["U_FNAME"] != DBNull.Value ? row["U_FNAME"].ToString() : string.Empty;
                    user.LastName = row["U_LNAME"] != DBNull.Value ? row["U_LNAME"].ToString() : string.Empty;
                    user.Email = row["U_EMAIL"] != DBNull.Value ? row["U_EMAIL"].ToString() : string.Empty;
                    user.Username = row["LG_LOGIN"] != DBNull.Value ? row["LG_LOGIN"].ToString() : string.Empty;
                    user.Username = row["LG_LOGIN"] != DBNull.Value ? row["LG_LOGIN"].ToString() : string.Empty;
                    user.PhoneNum = row["U_PHONE"] != DBNull.Value ? row["U_PHONE"].ToString() : string.Empty;
                    user.AddressLine1 = row["UAD_LINE1"] != DBNull.Value ? row["UAD_LINE1"].ToString() : string.Empty;
                    user.AddressLine2 = row["UAD_LINE2"] != DBNull.Value ? row["UAD_LINE2"].ToString() : string.Empty;
                    user.AddressLine3 = row["UAD_LINE3"] != DBNull.Value ? row["UAD_LINE3"].ToString() : string.Empty;
                    user.AddressPhoneNum = row["UAD_PHONE"] != DBNull.Value ? row["UAD_PHONE"].ToString() : string.Empty;
                    user.City = row["UAD_CITY"] != DBNull.Value ? row["UAD_CITY"].ToString() : string.Empty;
                    user.State = row["UAD_STATE"] != DBNull.Value ? row["UAD_STATE"].ToString() : string.Empty;
                    user.Country = row["UAD_COUNTRY"] != DBNull.Value ? row["UAD_COUNTRY"].ToString() : string.Empty;
                    user.Extension1 = row["UAD_EXT1"] != DBNull.Value ? row["UAD_EXT1"].ToString() : string.Empty;
                    user.Extension2 = row["UAD_EXT2"] != DBNull.Value ? row["UAD_EXT2"].ToString() : string.Empty;
                    user.Institution = row["INSTITUTION"] != DBNull.Value ? row["INSTITUTION"].ToString() : string.Empty;
                    user.UserData1 = row["UD_DATA1"] != DBNull.Value ? row["UD_DATA1"].ToString() : string.Empty;
                    user.UserType = row["U_TYPE"] != DBNull.Value ? row["U_TYPE"].ToString() : string.Empty;
                    user.ZipCode = row["UAD_ZIP"] != DBNull.Value ? row["UAD_ZIP"].ToString() : string.Empty;
                    user.Birthday = row["U_BIRTHDATE"] != DBNull.Value ? Convert.ToDateTime(row["U_BIRTHDATE"]) : DateTime.MaxValue;
                    user.isOTPVerified = row["isOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isOTPVerified"]) : 0;
                    user.isEmailOTPVerified = row["isEmailOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isEmailOTPVerified"]) : 0;
                    user.CredentialsVerified = row["CREDENTIALS_VERIFIED"] != DBNull.Value ? Convert.ToInt32(row["CREDENTIALS_VERIFIED"]) : 0;
                    user.IsSuperUser = row["IS_SUPER_USER"] != DBNull.Value ? (Convert.ToInt32(row["IS_SUPER_USER"]) == 1 ? true : false) : false;
                    user.PhoneID = row["U_PHONE_TOKEN"] != DBNull.Value ? row["U_PHONE_TOKEN"].ToString() : string.Empty;
                    user.PhoneOS = row["U_PHONE_OS"] != DBNull.Value ? row["U_PHONE_OS"].ToString() : string.Empty;
                    user.Currency = row["PARAM_USER_CURRENCY"] != DBNull.Value ? row["PARAM_USER_CURRENCY"].ToString() : string.Empty;

                    user.AltPhoneNum = row["U_ALTPHONE"] != DBNull.Value ? row["U_ALTPHONE"].ToString() : string.Empty;
                    user.AltEmail = row["U_ALTEMAIL"] != DBNull.Value ? row["U_ALTEMAIL"].ToString() : string.Empty;

                    user.Address = row["ADDRESS"] != DBNull.Value ? Convert.ToString(row["ADDRESS"]) : string.Empty;


                    if (vendorDetails.RequestUserRole == "CUSTOMER")
                    {
                        user.FirstName = vendorDetails.FirstName;
                        user.LastName = vendorDetails.LastName;
                        //user.Email = vendorDetails.Email;
                        //user.PhoneNum = vendorDetails.PhoneNum;
                        user.Institution = vendorDetails.Institution;
                        user.IsPrimary = vendorDetails.IsPrimary;

                    }



                }
            }
            catch (Exception ex)
            {
                user.ErrorMessage = ex.Message;
            }
            return user;
        }



        #endregion PRMServices Gets

        #region PRMServices Posts

        #endregion PRMServices Posts

        #region PRMServices Private

        private async Task SaveFileAsync(string fileName, byte[] fileContent)
        {
            var allowedExtns = ConfigurationManager.AppSettings["SUPPORTED.FILE.EXT"].ToString().Split(',').ToList();
            var isValid = allowedExtns.Any(e => fileName.ToLower().EndsWith(e));
            if (isValid)
            {
                await Task.Run(() => Utilities.SaveFileBytes(fileContent, fileName));
            }
            else
            {
                logger.Warn("Unsupported file uploaded: " + fileName);
            }
        }

        private string SaveAttachments(int reqID, string sessionID, int userID, string attachmentID, int revised, string fileType)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_ATTACHMENT_ID", attachmentID);
                sd.Add("P_IS_REVISED", revised);
                sd.Add("P_FILE_TYPE", fileType);
                DataSet ds = sqlHelper.SelectList("cp_SaveQuotationAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response.ErrorMessage;
        }

        private Response SaveAttachment(string path)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                sd.Add("P_PATH", path);
                DataSet ds = sqlHelper.SelectList("cp_SaveAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        
        private string GetEnumDesc<T>(string value) where T : struct, IConvertible
        {
        var type = typeof(T);
        var memInfo = type.GetMember(value);
        var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
        var description = ((DescriptionAttribute)attributes[0]).Description;
        return description;
        }

        private FileData DownloadFile(string path, string sessioID, byte[] attachment = null, string fileName = "")
        {
        FileData fd = new FileData();

        try
        {

        //System.IO.Path.GetExtension
        if (attachment == null)
        {
            byte[] fileContent = null;
            using (WebClient client = new WebClient())
            {
                Response response = GetAttachment(path, sessioID);
                fd.FileName = response.Message;
                fileContent = client.DownloadData("https://www.prm360.com" + Utilities.FILE_URL + response.Message + "?sessionid=" + sessioID);
            }

            fd.FileStream = new MemoryStream(fileContent);
        }
        else
        {
            fd.FileName = fileName;
            fd.FileStream = new MemoryStream(attachment);
        }

        }
        catch
        {
        FileData fd1 = new FileData();
        return fd1;
        }
        finally
        {

        }
        return fd;

        }

        private void SaveFile(string fileName, byte[] fileContent)
        {
            var allowedExtns = ConfigurationManager.AppSettings["SUPPORTED.FILE.EXT"].ToString().Split(',').ToList();
            var isValid = allowedExtns.Any(e => fileName.ToLower().EndsWith(e));
            if (isValid)
            {
                Utilities.SaveFile(fileName, fileContent);
            }
            else
            {
                logger.Warn("Unsupported file uploaded: " + fileName);
            }
        }
        
        private string GenerateRequirementHTML(int reqID, LogisticRequirement requirement, string quotationItems, UserDetails customer, string folderPath)
        {

        if (string.IsNullOrEmpty(customer.LogoURL))
        {
        customer.LogoURL = "/img/logo.png";
        }

        customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");


        string html = string.Empty;

        if (!requirement.IsTabular)
        {
        html = "RequirementText.html";
        }
        if (requirement.IsTabular)
        {
        html = "TabularLogisticsRequirementText.html";
        requirement.Description = quotationItems;
        }

        string filestring = File.ReadAllText(folderPath + html);
        string htmlRows = string.Empty;
        try
        {
        htmlRows = String.Format(filestring,
        reqID.ToString(),
        requirement.Title.ToString(),
        requirement.Description.ToString(),
        requirement.ReqComments.ToString(),
        requirement.DeliveryLocation.ToString(),
        requirement.PaymentTerms.ToString(),
        requirement.DeliveryTime.ToString(),
        requirement.Urgency.ToString(),
        requirement.QuotationFreezTime.ToString(),
        requirement.Currency.ToString(),
        "",
        "http://prm360.com" + customer.LogoURL,
        customer.CompanyName.ToString()
        );
        }
        catch
        {

        }

        return htmlRows;
        }

        private string GenerateRequirementHTMLCustomer(int reqID, LogisticRequirement requirement, string quotationItems, UserDetails customer, string folderPath)
        {
        string vendorsList = "";
        if (requirement.AuctionVendors.Count > 0)
        {
        vendorsList = "<h4> Vendors List :</h4></br><P>";
        foreach (LogisticVendorDetails vendor in requirement.AuctionVendors)
        {
            vendorsList += vendor.CompanyName + ", ";
        }
        vendorsList = vendorsList.Substring(0, vendorsList.Length - 2);
        vendorsList = vendorsList + "</P>";
        }

        string html = string.Empty;

        if (!requirement.IsTabular)
        {
        html = "RequirementText.html";
        }
        if (requirement.IsTabular)
        {
        html = "TabularLogisticsRequirementText.html";
        requirement.Description = quotationItems;
        }

        string filestring = File.ReadAllText(folderPath + html);
        string htmlRows = string.Empty;
        try
        {
        htmlRows = String.Format(filestring,
        reqID.ToString(),
        requirement.Title.ToString(),
        requirement.Description.ToString(),
        requirement.ReqComments.ToString(),
        requirement.DeliveryLocation.ToString(),
        requirement.PaymentTerms.ToString(),
        requirement.DeliveryTime.ToString(),
        requirement.Urgency.ToString(),
        requirement.QuotationFreezTime.ToString(),
        requirement.Currency.ToString(),
        vendorsList.ToString(),
        "http://prm360.com" + customer.LogoURL,
        customer.CompanyName.ToString()
        );
        }
        catch
        {

        }
        return htmlRows;
        }

        private UserInfo GetCompanyVendorInfo(int userID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            UserInfo user = new UserInfo();
            try
            {
                sd.Add("P_U_ID", userID);
                sd.Add("P_SESSION_ID", sessionID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_GetCompanyVendorInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    user.UserID = userID.ToString();
                    user.SessionID = sessionID;
                    user.FirstName = row["U_FNAME"] != DBNull.Value ? row["U_FNAME"].ToString() : string.Empty;
                    user.LastName = row["U_LNAME"] != DBNull.Value ? row["U_LNAME"].ToString() : string.Empty;
                    user.Email = row["U_EMAIL"] != DBNull.Value ? row["U_EMAIL"].ToString() : string.Empty;
                    user.PhoneNum = row["U_PHONE"] != DBNull.Value ? row["U_PHONE"].ToString() : string.Empty;
                    user.Institution = row["COMP_NAME"] != DBNull.Value ? row["COMP_NAME"].ToString() : string.Empty;
                    user.IsPrimary = row["IS_PRIMARY"] != DBNull.Value ? Convert.ToInt32(row["IS_PRIMARY"]) : 0;

                    user.RequestUserRole = row["PARAM_REQUEST_ROLE"] != DBNull.Value ? row["PARAM_REQUEST_ROLE"].ToString() : string.Empty;
                }
            }
            catch (Exception ex)
            {
                user.ErrorMessage = ex.Message;
            }
            return user;
        }

        public async Task SendEmail(string To, string Subject, string Body, string sessionID = null, Attachment attachment = null, List<Attachment> ListAttachment = null, int reqID = 0)
        {

        try
        {
        Communication communication = new Communication();
        communication = GetCommunicationData("EMAIL_FROM", sessionID, reqID: reqID);
        if (communication.Company.SendEmails != 0 || string.IsNullOrEmpty(sessionID))
        {

            Body = Body.Replace("COMPANY_LOGO_ID", communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
            Body = Body.Replace("COMPANY_NAME", !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : "PRM360");
            Body = Body.Replace("USER_NAME", !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");
            Body = Body.Replace("COMPANY_ADDRESS", !string.IsNullOrEmpty(communication.CompanyAddress) ? communication.CompanyAddress : "");
            Body = Utilities.replaceDomain(Body);
            Subject = Utilities.replaceDomain(Subject);

                    SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString());
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
            smtpClient.Credentials = credentials;

            smtpClient.EnableSsl = false;

            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(Utilities.GetFromAddress(communication.User.Email), Utilities.GetFromDisplayName(communication.User.Email));
            mail.BodyEncoding = System.Text.Encoding.ASCII;
            if (attachment != null)
            {
                mail.Attachments.Add(attachment);
            }

            if (ListAttachment != null)
            {
                foreach (Attachment singleAttachment in ListAttachment)
                {
                    if (singleAttachment != null)
                    {
                        mail.Attachments.Add(singleAttachment);
                    }
                }
            }

            List<string> ToAddresses = To.Split(',').ToList<string>();
            foreach (string address in ToAddresses)
            {
                if (!string.IsNullOrEmpty(address))
                {
                    mail.To.Add(new MailAddress(address));
                }

            }

            //if (!string.IsNullOrEmpty(communication.User.Email))
            //{
            //    mail.CC.Add(communication.User.Email);
            //}

            mail.Subject = Subject;
            mail.IsBodyHtml = true;
            mail.Body = Body;

            //smtpClient.Send(mail);
            await smtpClient.SendMailAsync(mail);

            Response response = new Response();
            response.ObjectID = 1;
            //return response;
        }
        }
        catch (Exception ex)
        {
        Response response = new Response();
        response.ErrorMessage = ex.Message;
        //return response;
        }
        }

        public async Task<string> SendSMS(string from, string to, string message, string sessionID = null, int reqID = 0)
        {
            try
            {
                Communication communication = new Communication();
                communication = GetCommunicationData("SMS_FROM", sessionID, reqID: reqID);
                if (communication.Company.SendEmails != 0 || string.IsNullOrEmpty(sessionID))
                {
                    message = message.Replace("COMPANY_LOGO_ID", communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
                    message = message.Replace("COMPANY_NAME", !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : "PRM360");
                    message = message.Replace("USER_NAME", !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");

                    if (string.IsNullOrEmpty(from))
                    {
                        from = ConfigurationManager.AppSettings["SMSFROM"].ToString();

                        if (!string.IsNullOrEmpty(communication.FromAdd))
                        {
                            from = communication.FromAdd;
                        };
                    }

                    message = message.Replace("&", "and").Replace("<br/>", Environment.NewLine);

                    string strUrl = string.Format(ConfigurationManager.AppSettings["SMSURL"].ToString(), from, to, message);

                    using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                    {
                        HttpResponseMessage httpResponse = client.GetAsync(strUrl).Result;
                        httpResponse.EnsureSuccessStatusCode();
                        string result = httpResponse.Content.ReadAsStringAsync().Result;
                    }

                    return "";
                }
                else
                {
                    return "";
                }
            }
            catch
            {
                return "";
            }
        }

        public Communication GetCommunicationData(string communicationType, string sessionID = null, int reqID = 0)
        {
            Communication communication = new Communication();

            communication.User = new User();
            communication.Company = new Company();

            try
            {

                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                Utilities.ValidateSession(sessionID);


                sd.Add("P_SESSION", sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_C_TYPE", communicationType); // (SMS or EMAIL or TELEGRAM)
                DataSet ds = sqlHelper.SelectList("cp_GetCommunicationData", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];

                    communication.User.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    communication.User.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    communication.User.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    communication.User.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    communication.User.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;

                    communication.Company.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    communication.Company.CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                    communication.Company.SendEmails = row["SEND_EMAILS"] != DBNull.Value ? Convert.ToInt32(row["SEND_EMAILS"]) : 0;

                    communication.CompanyAddress = row["ADDRESS"] != DBNull.Value ? Convert.ToString(row["ADDRESS"]) : string.Empty;

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                    {

                        DataRow row1 = ds.Tables[1].Rows[0];

                        communication.FromAdd = row1["CONFIG_VALUE"] != DBNull.Value ? Convert.ToString(row1["CONFIG_VALUE"]) : string.Empty;
                        communication.DisplayName = row1["CONFIG_TEXT"] != DBNull.Value ? Convert.ToString(row1["CONFIG_TEXT"]) : string.Empty;

                    }
                }
            }
            catch (Exception ex)
            {
                communication.ErrorMessage = ex.Message;
            }
            return communication;
        }

        private LogisticRequirement signalR(int reqID, int userID, string sessionID, string methodName)
        {
            LogisticRequirement response = new LogisticRequirement();
            List<object> payLoad = new List<object>();
            PRMLogisticsService services = new PRMLogisticsService();

            DataSet ds = services.GetRequirementDataSet(reqID, userID, sessionID);
            int customerID = 0;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];
                customerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
            }

            response = services.GetRequirementDataFilter(reqID, customerID, sessionID, ds);
            response.MethodName = methodName;
            response.CallerID = userID;

            var context = GlobalHost.ConnectionManager.GetHubContext<LogisticsHub>();
            context.Clients.Group(Utilities.LogisticsGroupName + reqID).checkRequirement(response);

            return response;
        }

        #endregion  PRMServices Private

        #endregion PRMServices

    }
}