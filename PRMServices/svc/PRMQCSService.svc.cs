﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Common;
using PRMServices.Models;
using OfficeOpenXml;
using OfficeOpenXml.DataValidation;
using System.IO;
using System.Linq;
using System.Drawing;
using OfficeOpenXml.Style;
using PRMServices.SQLHelper;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMQCSService : IPRMQCSService
    {
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        private PRMServices prmservices = new PRMServices();
        private readonly int decimal_round = Convert.ToInt32(ConfigurationManager.AppSettings["ROUNDING_DECIMALS"]);
        #region Get

        public LiveBidding[] GetLiveBiddingReport(int reqID, int count, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            if (count <= 0)
            {
                count = 10;
            }

            List<LiveBidding> details = new List<LiveBidding>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_COUNT", count);
                DataSet ds = sqlHelper.SelectList("rp_GetLiveBiddingReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LiveBidding detail = ReportUtility.GetLiveBiddingReportObject(row, count);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                LiveBidding error = new LiveBidding();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public LiveBidding[] GetLiveBiddingReport2(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<LiveBidding> details = new List<LiveBidding>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                ReportsRequirement reportsrequirement = new ReportsRequirement();
                reportsrequirement = GetReqDetails(reqID, sessionID);
                TimeSpan duration = reportsrequirement.EndTime.Subtract(reportsrequirement.StartTime);
                int interval = 0;
                for (int a = 10; a < duration.Minutes; a = a + 10)
                {
                    interval = duration.Minutes - 10;
                }

                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetLiveBiddingReport2", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LiveBidding detail = ReportUtility.GetLiveBiddingReportObject2(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                LiveBidding error = new LiveBidding();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public ItemWiseReport[] GetItemWiseReport(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ItemWiseReport> details = new List<ItemWiseReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetItemWiseReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ItemWiseReport detail = ReportUtility.GetItemWiseReportObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                ItemWiseReport error = new ItemWiseReport();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public DeliveryTimeLine[] GetDeliveryTimeLineReport(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<DeliveryTimeLine> details = new List<DeliveryTimeLine>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetDeliveryTimeLineReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DeliveryTimeLine detail = ReportUtility.GetIDeliveryTimelineObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                DeliveryTimeLine error = new DeliveryTimeLine();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public DeliveryTimeLine[] GetPaymentTermsReport(int reqID, string sessionID)
        {
            List<DeliveryTimeLine> details = new List<DeliveryTimeLine>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetPaymentTermsReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DeliveryTimeLine detail = ReportUtility.GetIDeliveryTimelineObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                DeliveryTimeLine error = new DeliveryTimeLine();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public ReportsRequirement GetReqDetails(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            ReportsRequirement reportsrequirement = new ReportsRequirement();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetReqDetails", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    reportsrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    reportsrequirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    reportsrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    reportsrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    reportsrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    reportsrequirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    reportsrequirement.NegotiationDuration = row["NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["NEGOTIATION_DURATION"]) : string.Empty;
                    reportsrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    reportsrequirement.IsTabular = row["REQ_IS_TABULAR"] != DBNull.Value ? (Convert.ToInt32(row["REQ_IS_TABULAR"]) == 1 ? true : false) : false;
                    reportsrequirement.IsUnitPriceBidding = row["IS_UNIT_PRICE_BIDDING"] != DBNull.Value ? Convert.ToInt32(row["IS_UNIT_PRICE_BIDDING"]) : 0;
                    reportsrequirement.NoOfVendorsInvited = row["NO_OF_VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_INVITED"]) : 0;
                    reportsrequirement.NoOfVendorsParticipated = row["NO_OF_VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_PARTICIPATED"]) : 0;
                }
            }
            catch (Exception ex)
            {

            }

            return reportsrequirement;
        }

        public string GetTemplates(string template, int compID, int userID, int reqID, string sessionID)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region addvendors
            if (template.ToLower().Contains("addvendors"))
            {
                PRMServices service = new PRMServices();
                List<CategoryObj> categories = service.GetCategories(userID);
                var uniqueCategories = categories.Select(o => o.Category).Distinct().Where(o=>o.Trim()!=string.Empty).ToList();
                List<string> currencies = new List<string>(new string[] { "INR", "USD", "JPY", "GBP", "CAD", "AUD", "HKD", "EUR", "CNY" });
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("AddVendor");
                wsSheet1.Cells["A1"].Value = "FirstName";
                wsSheet1.Cells["B1"].Value = "LasName";
                wsSheet1.Cells["C1"].Value = "Email";
                wsSheet1.Cells["D1"].Value = "PhoneNumber";
                wsSheet1.Cells["E1"].Value = "CompanyName";
                wsSheet1.Cells["F1"].Value = "Category";
                wsSheet1.Cells["G1"].Value = "Currency";
                wsSheet1.Cells["H1"].Value = "KnownSince";
                var validation = wsSheet1.DataValidations.AddListValidation("G2:G100000");
                validation.ShowErrorMessage = true;
                validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                validation.ErrorTitle = "Invalid Currency";
                validation.Error = "Invalid currency selected";
                foreach(var currency in currencies)
                {
                    validation.Formula.Values.Add(currency);
                }

                var catValidation = wsSheet1.DataValidations.AddListValidation("F2:F100000");
                catValidation.ShowErrorMessage = true;
                catValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                catValidation.ErrorTitle = "Invalid Category";
                catValidation.Error = "Invalid category entered";
                int count = 1;
                foreach (var cat in uniqueCategories)
                {
                    wsSheet1.Cells["AA" + count.ToString()].Value = cat;
                    count++;
                }

                catValidation.Formula.ExcelFormula = "AA1:AA" + (count-1).ToString();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }                    
            }
            #endregion

            #region vendorquotation
            if (template.ToUpper().Contains("MARGIN_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("MARGIN_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";                
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";

                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;                
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors) {

                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);                  
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    Requirement req = service.GetRequirementData(entry.Value.RequirementID, entry.Key, sessionID);

                    string vendorRemarks = "";

                    foreach (var vendor in entry.Value.AuctionVendors) {
                        if (vendor.VendorID == entry.Key) {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems) {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["L" + index].Value = item.ItemID;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["M" + index].Value = 0;
                        wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["P" + index].Value = vendorRemarks;
                        wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                        wsSheet1.Cells["R" + index].Value = req.Title;
                        wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString();
                        wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                        var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                        wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, decimal_round);
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region revvendorquotation
            if (template.ToUpper().Contains("MARGIN_REV_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("MARGIN_REV_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";

                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;                
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    Requirement req = service.GetRequirementData(entry.Value.RequirementID, entry.Key, sessionID);
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["L" + index].Value = item.ItemID;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["M" + index].Value = 0;
                        wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["P" + index].Value = vendorRemarks;
                        wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                        wsSheet1.Cells["R" + index].Value = req.Title;
                        wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString(); ;
                        wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                        var costPrice = 100 * item.UnitMRP / (100 + gst) * (1 + (item.UnitDiscount / 100));
                        wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, decimal_round);
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region unitprice
            #region vendorquotation
            if (template.ToUpper().Contains("UNIT_PRICE_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_PRICE_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "PRICE";
                wsSheet1.Cells["D1"].Value = "GST";
                wsSheet1.Cells["E1"].Value = "FREIGHT";
                wsSheet1.Cells["F1"].Value = "TOTAL_PRICE";
                wsSheet1.Cells["G1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["H1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["I1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["J1"].Value = "PRODUCT_NO";
                wsSheet1.Cells["K1"].Value = "BRAND";
                wsSheet1.Cells["L1"].Value = "QUANTITY";
                wsSheet1.Cells["M1"].Value = "UNITS";
                wsSheet1.Cells["N1"].Value = "DESCRIPTION";
                wsSheet1.Cells["O1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["F2:F6000"].Formula = "IF(A2 > 0, L2*C2*(1+(D2/100)), \"\")";
                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["C" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["D" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["E" + index].Value = 0;//entry.Value.AuctionVendors[0].RevVendorFreight;
                        //wsSheet1.Cells["F" + index].Value = item.RevItemPrice;
                        wsSheet1.Cells["G" + index].Value = 0;
                        wsSheet1.Cells["H" + index].Value = item.ItemID;
                        wsSheet1.Cells["I" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["J" + index].Value = item.ProductNo;
                        wsSheet1.Cells["K" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["L" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["M" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["N" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["O" + index].Value = item.RequirementID;
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion
            if (template.ToUpper().Contains("UNIT_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "PRICE";
                wsSheet1.Cells["D1"].Value = "GST";
                wsSheet1.Cells["E1"].Value = "FREIGHT";
                wsSheet1.Cells["F1"].Value = "TOTAL_PRICE";
                wsSheet1.Cells["G1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["H1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["I1"].Value = "Main Equipment/Vehicle";
                wsSheet1.Cells["J1"].Value = "Part_No";
                wsSheet1.Cells["K1"].Value = "Make";
                wsSheet1.Cells["L1"].Value = "Specification Of Item";
                wsSheet1.Cells["M1"].Value = "QTY/UOM";
                wsSheet1.Cells["N1"].Value = "Units";
                wsSheet1.Cells["O1"].Value = "Description";
                wsSheet1.Cells["P1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["F2:F6000"].Formula = "IF(A2 > 0, C2*M2*(1+(D2/100)), \"\")";
                wsSheet1.Cells["A:P"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["C:E"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["C:E"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["C" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["D" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["E" + index].Value = 0;//entry.Value.AuctionVendors[0].RevVendorFreight;
                        wsSheet1.Cells["G" + index].Value = 0;
                        wsSheet1.Cells["H" + index].Value = item.ItemID;
                        wsSheet1.Cells["I" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["J" + index].Value = item.ProductNo;
                        wsSheet1.Cells["K" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["L" + index].Value = item.OthersBrands;
                        wsSheet1.Cells["M" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["N" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["O" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["P" + index].Value = item.RequirementID;
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            if (template.ToUpper().Contains("UNIT_BIDDING"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_BIDDING");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["C1"].Value = "Main Equipment/Vehicle";
                wsSheet1.Cells["D1"].Value = "Part_No";
                wsSheet1.Cells["E1"].Value = "Make";
                wsSheet1.Cells["F1"].Value = "Specification Of Item";
                wsSheet1.Cells["G1"].Value = "Revised Unit Price";
                wsSheet1.Cells["H1"].Value = "QTY/UOM";
                wsSheet1.Cells["I1"].Value = "GST";
                wsSheet1.Cells["J1"].Value = "Revised Item Price";
                wsSheet1.Cells["K1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["L1"].Value = "Unit Price";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(G2 > 0, G2*H2*(1 + I2/100), \"\")";
                wsSheet1.Cells["A:J"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["G:G"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["G:G"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = item.RequirementID;
                        wsSheet1.Cells["C" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["D" + index].Value = item.ProductNo;
                        wsSheet1.Cells["E" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["F" + index].Value = item.OthersBrands;
                        wsSheet1.Cells["G" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["H" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["I" + index].Value = (item.SGst + item.CGst + item.IGst);
                        wsSheet1.Cells["K" + index].Value = item.ItemID;
                        wsSheet1.Cells["L" + index].Value = item.UnitPrice;
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #region 
            if (template.ToUpper().Contains("UNIT_PRICE_BIDDING_DATA"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                requirement.AuctionVendors = requirement.AuctionVendors.Where(v => v.CompanyName != "PRICE_CAP" && v.IsQuotationRejected != 1).ToList();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_PRICE_BIDDING_DATA");

                //wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                //wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                //wsSheet1.Cells["C1"].Value = "PRICE";
                //wsSheet1.Cells["D1"].Value = "REVISED_PRICE";
                //wsSheet1.Cells["E1"].Value = "GST";
                //wsSheet1.Cells["F1"].Value = "FREIGHT";
                //wsSheet1.Cells["G1"].Value = "REVISED_FREIGHT";
                //wsSheet1.Cells["H1"].Value = "TOTAL_PRICE";
                //wsSheet1.Cells["I1"].Value = "REVISED_TOTAL_PRICE";
                //wsSheet1.Cells["J1"].Value = "ITEM_ID";
                //wsSheet1.Cells["K1"].Value = "PRODUCT_ID";
                //wsSheet1.Cells["L1"].Value = "PRODUCT_NO";
                //wsSheet1.Cells["M1"].Value = "BRAND";
                //wsSheet1.Cells["N1"].Value = "QUANTITY";
                //wsSheet1.Cells["O1"].Value = "UNITS";
                //wsSheet1.Cells["P1"].Value = "DESCRIPTION";

                //VENDOR_ID VENDOR_NAME PRICE REVISED_PRICE   GST FREIGHT REVISED_FREIGHT TOTAL_PRICE 
                //REVISED_TOTAL_PRICE ITEM_ID PRODUCT_ID 
                //PRODUCT_NO  BRAND QUANTITY    UNITS DESCRIPTION

                //VENDOR_NAME Vendor Rank, Product Name, UNIT,    QUANTITY, Quoted Unit Price,   
                //Quoted GST,  Final Bid Price, Total Quoted Price(inc GST),    Total Bid Price(inc GST),   
                //Savings %, Saving Value, Vendor wise total saving %, Vendor wise total saving Value

                wsSheet1.Cells["A1"].Value = "VENDOR NAME";
                wsSheet1.Cells["B1"].Value = "Vendor Rank";
                wsSheet1.Cells["C1"].Value = "Product Name";
                wsSheet1.Cells["D1"].Value = "UNIT";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "Quoted Unit Price";
                wsSheet1.Cells["G1"].Value = "Quoted GST";
                wsSheet1.Cells["H1"].Value = "Final Bid Price";
                wsSheet1.Cells["I1"].Value = "Total Quoted Price(inc GST)";
                wsSheet1.Cells["J1"].Value = "Total Bid Price(inc GST)";
                wsSheet1.Cells["K1"].Value = "Savings %";
                wsSheet1.Cells["L1"].Value = "Saving Value";
                wsSheet1.Cells["M1"].Value = "Vendor wise total saving %";
                wsSheet1.Cells["N1"].Value = "Vendor wise total saving Value";
                //wsSheet1.Cells["O1"].Value = "IT";
                //wsSheet1.Cells["P1"].Value = "RT";
               

                var calcColumns = wsSheet1.Cells["A1:N1"];
                wsSheet1.Cells.AutoFitColumns();
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumns.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                //calcColumns.Style.Border.Bottom.Color.SetColor(Color.Red);
                calcColumns.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                calcColumns.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                calcColumnsFont.Bold = true;
                //calcColumnsFont.Size = 16;
                calcColumnsFont.Color.SetColor(Color.White);
                
                //calcColumnsFont.Italic = true;


                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }


                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    entry.Value.AuctionVendors[0].savingsPercentage = 0;
                    entry.Value.AuctionVendors[0].savings = 0;

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        //K
                        item.savingsPercentage = Math.Round(
                            (((item.ItemPrice - item.RevItemPrice)
                            / (item.ItemPrice)) * 100),
                            decimal_round);
                        //L
                        item.savings = Math.Round((item.ItemPrice - item.RevItemPrice), decimal_round);

                    }

                    //M
                    entry.Value.AuctionVendors[0].savingsPercentage = Math.Round(
                        (((entry.Value.AuctionVendors[0].TotalInitialPrice - entry.Value.AuctionVendors[0].RevVendorTotalPrice)
                        / (entry.Value.AuctionVendors[0].TotalInitialPrice)) * 100),
                        decimal_round);

                    //N
                    entry.Value.AuctionVendors[0].savings = Math.Round((entry.Value.AuctionVendors[0].TotalInitialPrice - entry.Value.AuctionVendors[0].RevVendorTotalPrice), decimal_round);

                }




                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    double freightcharges = 0;
                    double revfreightcharges = 0;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                            freightcharges = 0;// vendor.VendorFreight;
                            revfreightcharges = 0;//vendor.RevVendorFreight;
                        }
                    }

                    var from = index;
                    var to = index;

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;

                        wsSheet1.Cells["A" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        if(from == to)
                        {
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].Rank;
                        }
                        wsSheet1.Cells["C" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["D" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["F" + index].Value = item.UnitPrice;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["H" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["I" + index].Value = item.ItemPrice;
                        wsSheet1.Cells["J" + index].Value = item.RevItemPrice;
                        wsSheet1.Cells["K" + index].Value = item.savingsPercentage;
                        wsSheet1.Cells["L" + index].Value = item.savings;
                        wsSheet1.Cells["M" + index].Value = entry.Value.AuctionVendors[0].savingsPercentage;
                        wsSheet1.Cells["N" + index].Value = entry.Value.AuctionVendors[0].savings;
                        //wsSheet1.Cells["O" + index].Value = entry.Value.AuctionVendors[0].TotalInitialPrice;
                        //wsSheet1.Cells["P" + index].Value = entry.Value.AuctionVendors[0].RevVendorTotalPrice;


                        //wsSheet1.Cells["A" + index].Value = entry.Key;
                        //wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        //wsSheet1.Cells["C" + index].Value = item.UnitPrice;// entry.Value.AuctionVendors[0].RunningPrice;
                        //wsSheet1.Cells["D" + index].Value = item.RevUnitPrice; // entry.Value.AuctionVendors[0].RevVendorTotalPrice;
                        //wsSheet1.Cells["E" + index].Value = item.CGst + item.SGst + item.IGst;
                        //wsSheet1.Cells["F" + index].Value = entry.Value.AuctionVendors[0].VendorFreight;
                        //wsSheet1.Cells["G" + index].Value = entry.Value.AuctionVendors[0].RevVendorFreight;
                        //wsSheet1.Cells["H" + index].Value = entry.Value.AuctionVendors[0].TotalInitialPrice;
                        //wsSheet1.Cells["I" + index].Value = entry.Value.AuctionVendors[0].RevVendorTotalPrice;
                        //wsSheet1.Cells["J" + index].Value = item.ItemID;
                        //wsSheet1.Cells["K" + index].Value = item.ProductIDorName;
                        //wsSheet1.Cells["L" + index].Value = item.ProductNo;
                        //wsSheet1.Cells["M" + index].Value = item.ProductBrand;
                        //wsSheet1.Cells["N" + index].Value = item.ProductQuantity;
                        //wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        //wsSheet1.Cells["P" + index].Value = item.ProductDescription;

                        

                        wsSheet1.Cells.AutoFitColumns();

                        index++;
                        to = index;
                    }
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Merge = true;
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Style.Font.Size = 16;

                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Merge = true;
                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Style.Font.Size = 16;

                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Merge = true;
                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Style.Font.Size = 16;
                    

                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion
            #endregion

            #region bulk margin quotations
            if (template.ToUpper().Contains("BULK_MARGIN_VEND_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("BULK_MARGIN_VEND_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";
                wsSheet1.Cells["U1"].Value = "QUANTITY_ORIG";
                wsSheet1.Cells["V1"].Value = "QUOTATION_STATUS";
                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;
                wsSheet1.Column(21).Hidden = true;
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                int index = 2;
                List<Requirement> requirementList = service.GetVendorRequirements(userID, 14, sessionID);
                foreach (Requirement req in requirementList)
                {
                    if (req.AuctionVendors.Count == 0)
                    {
                        continue;
                    }
                    if (req.PostedOn >= DateTime.UtcNow.AddMonths(-1) && req.IsDiscountQuotation == 2 && (req.StartTime == null || req.StartTime > DateTime.UtcNow))
                    {
                        if(req.RequirementID > 0 && req.PostedOn >= DateTime.UtcNow.AddMonths(-1))
                        {
                            foreach (var item in req.ListRequirementItems)
                            {
                                var gst = item.CGst + item.SGst + item.IGst;
                                wsSheet1.Cells["A" + index].Value = userID;
                                wsSheet1.Cells["B" + index].Value = req.AuctionVendors[0].CompanyName;
                                wsSheet1.Cells["L" + index].Value = item.ItemID;
                                wsSheet1.Cells["C" + index].Value = item.ProductNo;
                                wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                                wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                                wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                                wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                                wsSheet1.Cells["M" + index].Value = 0;
                                wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                                wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                                wsSheet1.Cells["P" + index].Value = req.AuctionVendors[0].OtherProperties;
                                wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                                wsSheet1.Cells["R" + index].Value = req.Title;
                                wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString(); ;
                                wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                                wsSheet1.Cells["U" + index].Value = item.ProductQuantity;
                                wsSheet1.Cells["V" + index].Value = req.AuctionVendors[0].IsQuotationRejected == -1 ? "Pending/Not Uploaded" : (req.AuctionVendors[0].IsQuotationRejected == 0 ? "Approved" : "Rejected");
                                var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                                wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, decimal_round);
                                index++;
                            }
                        }
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region bulk margin bidding
            if (template.ToUpper().Contains("BULK_MARGIN_VEND_BIDDING"))
            {
                PRMServices service = new PRMServices();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("BULK_MARGIN_VEND_BIDDING");
                wsSheet1.Cells["A1"].Value = "REQ_ID";
                wsSheet1.Cells["B1"].Value = "REQ_TITLE";
                wsSheet1.Cells["C1"].Value = "POSTED_DATE";
                wsSheet1.Cells["D1"].Value = "NEGOTIATION_DATE";
                wsSheet1.Cells["E1"].Value = "VENDOR_ID";
                wsSheet1.Cells["F1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["G1"].Value = "HSN_CODE";
                wsSheet1.Cells["H1"].Value = "MANUFACTURER_NAME";
                wsSheet1.Cells["I1"].Value = "BRAND";
                wsSheet1.Cells["J1"].Value = "QUANTITY";
                wsSheet1.Cells["K1"].Value = "COST_PRICE";
                wsSheet1.Cells["L1"].Value = "REV_COST_PRICE";
                wsSheet1.Cells["M1"].Value = "DIFFERENCE";
                wsSheet1.Cells["N1"].Value = "GST";
                wsSheet1.Cells["O1"].Value = "MRP";
                wsSheet1.Cells["P1"].Value = "REV_NET_PRICE";
                wsSheet1.Cells["Q1"].Value = "REV_MARGIN_AMOUNT";
                wsSheet1.Cells["R1"].Value = "REV_MARGIN_PERC";
                wsSheet1.Cells["S1"].Value = "ITEM_ID";
                wsSheet1.Cells["T1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["U1"].Value = "TOTAL_INIT_PRICE";
                wsSheet1.Cells["V1"].Value = "REV_TOTAL_PRICE";
                wsSheet1.Cells["W1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["X1"].Value = "UNITS";
                wsSheet1.Cells["Y1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Z1"].Value = "RANK";
                wsSheet1.Cells["AA1"].Value = "REV_COST_PRICE_ORIG";

                #region excelStyling                

                wsSheet1.Cells["R2:R6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Cells["A:AA"].AutoFitColumns();
                wsSheet1.Column(10).Hidden = true;
                wsSheet1.Column(11).Hidden = true;
                wsSheet1.Column(14).Hidden = true;
                wsSheet1.Column(15).Hidden = true;
                wsSheet1.Column(21).Hidden = true;
                wsSheet1.Column(27).Hidden = true;
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#DAF7A6");
                wsSheet1.Cells["P:P"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["P:P"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["Q:Q"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["Q:Q"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["R:R"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["R:R"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["L:L"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["L:L"].Style.Fill.BackgroundColor.SetColor(colFromHex1);
                var calcColumns = wsSheet1.Cells["P:R"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling
                
                wsSheet1.Cells["M2:M6000"].Formula = "IF(E2 > 0, ABS(K2 - L2), \"\")";
                wsSheet1.Cells["P2:P6000"].Formula = "IF(E2 > 0, L2*(1+(N2/100)), \"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                List<Requirement> requirementList = service.GetVendorRequirements(userID, 1, sessionID);
                int index = 2;
                foreach (Requirement req in requirementList)
                {
                    if(req.AuctionVendors.Count > 0 && (req.AuctionVendors[0].IsQuotationRejected == 1 || req.AuctionVendors[0].IsRevQuotationRejected == 1))
                    {
                        continue;
                    }
                    if (req.PostedOn >= DateTime.UtcNow.AddMonths(-1) && req.IsDiscountQuotation == 2 && (req.StartTime < DateTime.UtcNow && req.EndTime > DateTime.UtcNow))
                    {
                        if(req.AuctionVendors == null)
                        {
                            continue;
                        }
                        foreach (var item in req.ListRequirementItems)
                        {
                            var gst = item.CGst + item.SGst + item.IGst;
                            var revCostPrice = (item.UnitMRP * 100 * 100) / ((item.RevUnitDiscount * 100) + 10000 + (item.RevUnitDiscount * gst) + (gst * 100));
                            wsSheet1.Cells["A" + index].Value = req.RequirementID; // entry.Key;
                            wsSheet1.Cells["B" + index].Value = req.Title;  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["C" + index].Value = req.PostedOn.ToString();  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["D" + index].Value = req.StartTime.ToString();  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["E" + index].Value = userID;  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["F" + index].Value = req.AuctionVendors[0].CompanyName;  // entry.Value.AuctionVendors[0].CompanyName;

                            wsSheet1.Cells["G" + index].Value = item.ProductNo;
                            wsSheet1.Cells["H" + index].Value = item.OthersBrands;
                            wsSheet1.Cells["I" + index].Value = item.ProductBrand;
                            wsSheet1.Cells["J" + index].Value = item.ProductQuantity;
                            wsSheet1.Cells["L" + index].Value = Math.Round(revCostPrice, decimal_round);

                            wsSheet1.Cells["N" + index].Value = item.CGst + item.SGst + item.IGst;
                            wsSheet1.Cells["O" + index].Value = item.UnitMRP;
                            wsSheet1.Cells["S" + index].Value = item.ItemID;
                            wsSheet1.Cells["T" + index].Value = 0;
                            wsSheet1.Cells["U" + index].Value = req.AuctionVendors[0].TotalInitialPrice;
                            wsSheet1.Cells["V" + index].Value = req.AuctionVendors[0].RevVendorTotalPrice;
                            wsSheet1.Cells["W" + index].Value = item.ProductIDorName;
                            wsSheet1.Cells["X" + index].Value = item.ProductQuantityIn;
                            wsSheet1.Cells["Y" + index].Value = req.AuctionVendors[0].OtherProperties;
                            wsSheet1.Cells["Z" + index].Value = req.AuctionVendors[0].Rank > 0 ? req.AuctionVendors[0].Rank.ToString() : "NA";
                            wsSheet1.Cells["AA" + index].Value = Math.Round(revCostPrice, decimal_round);

                            if (req.IsRevUnitDiscountEnable == 0)
                            {
                                wsSheet1.Cells["Q" + index].Value = 0;
                                wsSheet1.Cells["R" + index].Value = 0;
                            }
                            if (req.IsRevUnitDiscountEnable == 1)
                            {
                                wsSheet1.Cells["Q" + index].Formula = "IF(E" + index + " > 0, ABS(O" + index + " - P" + index + "), \"\")";
                                wsSheet1.Cells["R" + index].Formula = "IFERROR(Q" + index + "/P" + index + "*100,\"\")";
                            }

                            var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                            wsSheet1.Cells["K" + index].Value = Math.Round(costPrice, decimal_round);
                            index++;
                        }
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            return Convert.ToBase64String(ms.ToArray());
        }

        public ExcelRequirement GetReqReportForExcel(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            ExcelRequirement excelrequirement = new ExcelRequirement();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetReqReportForExcel", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[3].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[3].Rows[0];
                    excelrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    excelrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    excelrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.UtcNow;
                    excelrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.UtcNow;
                    excelrequirement.CurrentDate = prmservices.GetDate();
                    excelrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    excelrequirement.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    excelrequirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_ENABLED"]) : 0;
                    excelrequirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_COMPLETED"]) : 0;
                    excelrequirement.PostedByFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    excelrequirement.PostedByLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[4].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[4].Rows[0];
                    excelrequirement.PreSavings = row["PRE_SAVING"] != DBNull.Value ? Convert.ToDouble(row["PRE_SAVING"]) : 0;
                    excelrequirement.PostSavings = row["REV_SAVING"] != DBNull.Value ? Convert.ToDouble(row["REV_SAVING"]) : 0;
                }

                List<ExcelVendorDetails> ListRV = new List<ExcelVendorDetails>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ExcelVendorDetails RV = new ExcelVendorDetails();

                        #region VENDOR DETAILS
                        RV.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        RV.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        RV.VendorFreight = row["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["VEND_FREIGHT"]) : 0;
                        RV.RevVendorFreight = row["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_FREIGHT"]) : 0;
                        RV.TotalInitialPrice = row["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_TOTAL_PRICE"]) : 0;
                        RV.RevVendorTotalPrice = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
                        RV.Warranty = row["WARRANTY"] != DBNull.Value ? Convert.ToString(row["WARRANTY"]) : string.Empty;
                        RV.Validity = row["VALIDITY"] != DBNull.Value ? Convert.ToString(row["VALIDITY"]) : string.Empty;
                        RV.OtherProperties = row["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row["OTHER_PROPERTIES"]) : string.Empty;
                        RV.Duration = row["DURATION"] != DBNull.Value ? Convert.ToString(row["DURATION"]) : string.Empty;
                        RV.Payment = row["PAYMENT"] != DBNull.Value ? Convert.ToString(row["PAYMENT"]) : string.Empty;
                        RV.GSTNumber = row["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row["GST_NUMBER"]) : string.Empty;
                        RV.SelectedVendorCurrency = row["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row["SELECTED_VENDOR_CURRENCY"]) : string.Empty;


                        RV.FREIGHT_CHARGES = row["FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES"]) : 0; //*
                        RV.FREIGHT_CHARGES_TAX_PERCENTAGE = row["FREIGHT_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.FREIGHT_CHARGES_WITH_TAX = row["FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_WITH_TAX"]) : 0;
                        RV.REV_FREIGHT_CHARGES = row["REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES"]) : 0;
                        RV.REV_FREIGHT_CHARGES_WITH_TAX = row["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0; //*
                        RV.RevVendorFreightCB = row["REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_CB"]) : 0;

                        RV.InstallationCharges = row["INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES"]) : 0; //*
                        RV.InstallationChargesTaxPercentage = row["INSTALLATION_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.InstallationChargesWithTax = row["INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                        RV.RevinstallationCharges = row["REV_INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES"]) : 0; //*
                        RV.RevinstallationChargesWithTax = row["REV_INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                        RV.RevinstallationChargesCB = row["REV_INSTALLATION_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_CB"]) : 0;
                        RV.RevinstallationChargesWithTaxCB = RV.RevinstallationChargesCB + ((RV.RevinstallationChargesCB / 100) * (RV.InstallationChargesTaxPercentage));

                        RV.PackingCharges = row["PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES"]) : 0; //*
                        RV.PackingChargesTaxPercentage = row["PACKING_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.PackingChargesWithTax = row["PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_WITH_TAX"]) : 0;
                        RV.RevpackingCharges = row["REV_PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES"]) : 0; //*
                        RV.RevpackingChargesWithTax = row["REV_PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_WITH_TAX"]) : 0;
                        RV.RevpackingChargesCB = row["REV_PACKING_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_CB"]) : 0;
                        RV.RevpackingChargesWithTaxCB = RV.RevpackingChargesCB + ((RV.RevpackingChargesCB / 100) * (RV.PackingChargesTaxPercentage));

                        RV.RevVendorTotalPriceCB = row["REV_VEND_TOTAL_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE_CB"]) : 0;

                        RV.INCO_TERMS = row["INCO_TERMS"] != DBNull.Value ? Convert.ToString(row["INCO_TERMS"]) : string.Empty;

                        #endregion VENDOR DETAILS

                        ListRV.Add(RV);
                    }
                }

                List<ExcelRequirementItems> ListRITemp = new List<ExcelRequirementItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        ExcelRequirementItems RI = new ExcelRequirementItems();
                        #region ITEM DETAILS
                        RI.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                        RI.ProductIDorName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        RI.ProductNo = row["PROD_NO"] != DBNull.Value ? Convert.ToString(row["PROD_NO"]) : string.Empty;
                        RI.ProductDescription = row["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row["DESCRIPTION"]) : string.Empty;
                        RI.ProductQuantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
                        RI.ProductQuantityIn = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
                        RI.ProductBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                        RI.OthersBrands = row["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row["OTHER_BRAND"]) : string.Empty;
                        RI.ItemLastPrice = row["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row["LAST_ITEM_PRICE"]) : 0;
                        RI.I_LLP_DETAILS = row["I_LLP_DETAILS"] != DBNull.Value ? Convert.ToString(row["I_LLP_DETAILS"]) : string.Empty;
                        RI.ITEM_L1_PRICE = row["ITEM_L1_PRICE"] != DBNull.Value ? Convert.ToDouble(row["ITEM_L1_PRICE"]) : 0;
                        RI.ITEM_L1_COMPANY_NAME = row["ITEM_L1_COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["ITEM_L1_COMPANY_NAME"]) : string.Empty;
                        RI.ITEM_L2_PRICE = row["ITEM_L2_PRICE"] != DBNull.Value ? Convert.ToDouble(row["ITEM_L2_PRICE"]) : 0;
                        RI.ITEM_L2_COMPANY_NAME = row["ITEM_L2_COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["ITEM_L2_COMPANY_NAME"]) : string.Empty;

                        RI.ReqVendors = ListRV.Select(item => (ExcelVendorDetails)item.Clone()).ToList().ToArray();
                        #endregion ITEM DETAILS

                        ListRITemp.Add(RI);
                    }
                }

                List<ExcelQuotationPrices> ListQ = new List<ExcelQuotationPrices>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        ExcelQuotationPrices Q = new ExcelQuotationPrices();

                        #region QUOTATION DETAILS
                        Q.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        Q.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                        Q.UnitPrice = row["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["UNIT_PRICE"]) : 0;
                        Q.RevUnitPrice = row["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE"]) : 0;
                        Q.CGst = row["C_GST"] != DBNull.Value ? Convert.ToDouble(row["C_GST"]) : 0;
                        Q.SGst = row["S_GST"] != DBNull.Value ? Convert.ToDouble(row["S_GST"]) : 0;
                        Q.IGst = row["I_GST"] != DBNull.Value ? Convert.ToDouble(row["I_GST"]) : 0;
                        Q.ItemPrice = row["PRICE"] != DBNull.Value ? Convert.ToDouble(row["PRICE"]) : 0;
                        Q.RevItemPrice = row["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE"]) : 0;
                        Q.IsRegret = row["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row["IS_REGRET"]) > 0 ? true : false) : false;
                        Q.RegretComments = row["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REGRET_COMMENTS"]) : string.Empty;
                        Q.ItemLevelInitialComments = row["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
                        Q.ItemLevelRevComments = row["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
                        Q.VendorUnits = row["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row["VENDOR_UNITS"]) : string.Empty;
                        Q.VendorBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                        Q.ItemFreightCharges = row["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_CHARGES"]) : 0;
                        Q.ItemRevFreightCharges = row["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_REV_FREIGHT_CHARGES"]) : 0;
                        Q.ItemFreightTAX = row["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_TAX"]) : 0;
                        Q.RevItemPriceCB = row["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE_CB"]) : 0;
                        Q.RevUnitPriceCB = row["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE_CB"]) : 0;
                        #endregion QUOTATION DETAILS

                        ListQ.Add(Q);

                    }
                }

                for (int i = 0; i < ListRITemp.Count; i++)
                {
                    for (int v = 0; v < ListRITemp[i].ReqVendors.Length; v++)
                    {
                        ListRITemp[i].ReqVendors[v].QuotationPrices = new ExcelQuotationPrices();
                        try
                        {
                            ListRITemp[i].ReqVendors[v].QuotationPrices = ListQ.Where(quotation => quotation.VendorID == ListRITemp[i].ReqVendors[v].VendorID && quotation.ItemID == ListRITemp[i].ItemID).FirstOrDefault();
                        }
                        catch
                        {
                            ListRITemp[i].ReqVendors[v].QuotationPrices = new ExcelQuotationPrices();
                        }
                    }
                }

                excelrequirement.ReqItems = ListRITemp.ToArray();

                foreach (ExcelRequirementItems item in excelrequirement.ReqItems)
                {
                    if (item.ReqVendors != null)
                    {
                        List<ExcelVendorDetails> vendors = new List<ExcelVendorDetails>();
                        vendors = item.ReqVendors.Where(v => v.RevVendorTotalPrice > 0).OrderBy(v => v.RevVendorTotalPrice).ToList();
                        item.ReqVendors = vendors.ToArray();
                    }
                }

            }
            catch (Exception ex)
            {
                excelrequirement.ErrorMessage = ex.Message;
            }

            return excelrequirement;
        }

        public List<ConsolidatedReport> GetConsolidatedReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ConsolidatedReport> consolidatedReports = new List<ConsolidatedReport>();
            List<RequirementUsersByRank> revisedReport = new List<RequirementUsersByRank>();
            List<RequirementUsersByRank> initialReport = new List<RequirementUsersByRank>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.UtcNow.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.UtcNow.ToString();
                }

                //DateTime fromDate = Convert.ToDateTime(from);
                //DateTime toDate = Convert.ToDateTime(to);

                //sd.Add("P_FROM_DATE", Convert.ToDateTime(from).ToUniversalTime());
                //sd.Add("P_TO_DATE", Convert.ToDateTime(to).ToUniversalTime());

                sd.Add("P_FROM_DATE", from);
                sd.Add("P_TO_DATE", to);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetConsolidatedReports", sd);


                #region Revised Details (Table 1)

                //List<RevisedDetails> revisedDetails = new List<RevisedDetails>();

                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[1].Rows)
                    {
                        RequirementUsersByRank revised = new RequirementUsersByRank();
                        revised.UID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                        revised.RequirementID = row1["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row1["REQ_ID"]) : 0;
                        revised.RevBasePrice = row1["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_BASE_PRICE"]) : 0;
                        revised.InitBasePrice = row1["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["INIT_BASE_PRICE"]) : 0;
                        revised.RevVendTotalPrice = row1["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE"]) : 0;
                        revised.Rank = row1["RN"] != DBNull.Value ? Convert.ToInt32(row1["RN"]) : 0;
                        revised.Quantity = row1["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row1["QUANTITY"]) : 0;
                        revised.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;
                        revised.SelectedVendorCurrency = row1["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row1["SELECTED_VENDOR_CURRENCY"]) : string.Empty;

                        revisedReport.Add(revised);
                    }
                }

                #endregion Revised Details

                #region initial details (table 2)

                // list<initialdetails> initialdetails = new list<initialdetails>();

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        RequirementUsersByRank initial = new RequirementUsersByRank();

                        //U_ID, REQ_ID, REV_BASE_PRICE, INIT_BASE_PRICE, RN, QUANTITY, COMPANY_NAME
                        initial.UID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                        initial.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                        initial.RevBasePrice = row2["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_BASE_PRICE"]) : 0;
                        initial.InitBasePrice = row2["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["INIT_BASE_PRICE"]) : 0;
                        initial.VendTotalPrice = row2["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["VEND_TOTAL_PRICE"]) : 0;
                        initial.Rank = row2["RN"] != DBNull.Value ? Convert.ToInt32(row2["RN"]) : 0;
                        initial.Quantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                       // initial.CompanyName = row2["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row2["COMPANY_NAME"]) : string.Empty;

                        initialReport.Add(initial);
                    }
                }

                #endregion requirement items

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        ConsolidatedReport report = new ConsolidatedReport();
                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.UID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                        report.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        report.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                       // report.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                        report.Closed = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        report.NoOfVendorsInvited = row["NoOfVendorsInvited"] != DBNull.Value ? Convert.ToInt32(row["NoOfVendorsInvited"]) : 0;
                        report.NoOfvendorsParticipated = row["NoOfvendorsParticipated"] != DBNull.Value ? Convert.ToInt32(row["NoOfvendorsParticipated"]) : 0;
                        report.ReqCategory = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]) : string.Empty;
                        

                        RequirementUsersByRank InitialUser = initialReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL1 = revisedReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL2 = revisedReport.Where(a => a.Rank == 2 && a.RequirementID == report.RequirementID).FirstOrDefault();

                        #region Initial L1 START
                        if (InitialUser != null) {
                            report.IL1_UID = InitialUser.UID;
                            report.IL1_RevBasePrice = InitialUser.RevBasePrice;
                            report.IL1_InitBasePrice = InitialUser.InitBasePrice;
                            report.IL1_L1InitialBasePrice = InitialUser.L1InitialBasePrice;
                            report.IL1_VendTotalPrice = InitialUser.VendTotalPrice;
                            report.IL1_Rank = InitialUser.Rank;
                            report.IL1_Quantity = InitialUser.Quantity;
                            report.IL1_CompanyName = InitialUser.CompanyName;
                            report.IL1_SelectedVendorCurrency = InitialUser.SelectedVendorCurrency;
                        }

                        #endregion Initial L1 END

                        #region Revised RL1 START
                        if (RevisedUserL1 != null)
                        {
                            report.RL1_UID = RevisedUserL1.UID;
                            report.RL1_RevBasePrice = RevisedUserL1.RevBasePrice;
                            report.RL1_InitBasePrice = RevisedUserL1.InitBasePrice;
                            report.RL1_L1InitialBasePrice = RevisedUserL1.L1InitialBasePrice;
                            report.RL1_RevVendTotalPrice = RevisedUserL1.RevVendTotalPrice;
                            report.RL1_Rank = RevisedUserL1.Rank;
                            report.RL1_Quantity = RevisedUserL1.Quantity;
                            report.RL1_CompanyName = RevisedUserL1.CompanyName;
                            report.RL1_SelectedVendorCurrency = RevisedUserL1.SelectedVendorCurrency;
                        }

                        #endregion Revised RL1 END

                        #region Revised RL2 START
                        if (RevisedUserL2 != null)
                        {
                            report.RL2_UID = RevisedUserL2.UID;
                            report.RL2_RevBasePrice = RevisedUserL2.RevBasePrice;
                            report.RL2_InitBasePrice = RevisedUserL2.InitBasePrice;
                            report.RL2_L1InitialBasePrice = RevisedUserL2.L1InitialBasePrice;
                            report.RL2_RevVendTotalPrice = RevisedUserL2.RevVendTotalPrice;
                            report.RL2_Rank = RevisedUserL2.Rank;
                            report.RL2_Quantity = RevisedUserL2.Quantity;
                            report.RL2_CompanyName = RevisedUserL2.CompanyName;
                            report.RL2_SelectedVendorCurrency = RevisedUserL2.SelectedVendorCurrency;
                            #endregion Revised RL2 END
                        }

                        report.BasePriceSavings = report.IL1_VendTotalPrice - report.RL1_RevVendTotalPrice;
                        if (report.BasePriceSavings > 0)
                        {
                            report.BasePriceSavings = Math.Round(report.BasePriceSavings, decimal_round);
                        }
                        else
                        {
                            report.BasePriceSavings = 0;
                        }

                        report.SavingsPercentage = (100 - (report.RL1_RevVendTotalPrice / report.IL1_VendTotalPrice) * 100);
                        if (report.SavingsPercentage > 0)
                        {
                            report.SavingsPercentage = Math.Round(report.SavingsPercentage, decimal_round);
                        }
                        else
                        {
                            report.SavingsPercentage = 0;
                        }


                        //report.SavingsPercentage = Math.Round(report.SavingsPercentage, decimal_round);
                        //report.RevisedUserL1 = RevisedUserL1;
                        //report.RevisedUserL2 = RevisedUserL2;

                        consolidatedReports.Add(report);
                    }

                    consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();





                }
            }
            catch (Exception ex)
            {
                ConsolidatedReport recuirementerror = new ConsolidatedReport();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public List<LogisticConsolidatedReport> GetLogisticConsolidatedReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<LogisticConsolidatedReport> consolidatedReports = new List<LogisticConsolidatedReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.UtcNow.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.UtcNow.ToString();
                }

                DateTime fromDate = Convert.ToDateTime(from);
                DateTime toDate = Convert.ToDateTime(to);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetLogisticConsolidatedReports", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LogisticConsolidatedReport report = new LogisticConsolidatedReport();
                       
                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.InvoiceNumber = row["INVOICE_NUMBER"] != DBNull.Value ? Convert.ToString(row["INVOICE_NUMBER"]) : string.Empty;
                        report.InvoiceDate = row["INVOICE_DATE"] != DBNull.Value ? Convert.ToDateTime(row["INVOICE_DATE"]) : DateTime.MaxValue;
                        report.ConsigneeName = row["CONSIGNEE_NAME"] != DBNull.Value ? Convert.ToString(row["CONSIGNEE_NAME"]) : string.Empty;
                        report.Destination = row["FINAL_DESTINATION"] != DBNull.Value ? Convert.ToString(row["FINAL_DESTINATION"]) : string.Empty;
                        report.ProductName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        report.ShippingMode = row["MODE_OF_SHIPMENT"] != DBNull.Value ? Convert.ToString(row["MODE_OF_SHIPMENT"]) : string.Empty;
                        report.QTY = row["NET_WEIGHT"] != DBNull.Value ? Convert.ToInt32(row["NET_WEIGHT"]) : 0;
                        report.ChargeableWt = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
                        report.DGFee = row["FORWORD_DG_FEE"] != DBNull.Value ? Convert.ToDouble(row["FORWORD_DG_FEE"]) : 0;
                        report.AMS = row["AMS"] != DBNull.Value ? Convert.ToString(row["AMS"]) : string.Empty;
                        report.MISC = row["MISC"] != DBNull.Value ? Convert.ToString(row["MISC"]) : string.Empty;
                        // report.MenzinesCharges = row["L2_NAME"] != DBNull.Value ? Convert.ToDouble(row["L2_NAME"]) : 0;
                        // report.ChaCharges = row["L1_INITIAL_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row["L1_INITIAL_BASE_PRICE"]) : 0;
                        report.FreightBidAmount = row["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_PRICE"]) : 0;
                        // report.GrandTotal = row["BASE_PRICE_SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["BASE_PRICE_SAVINGS"]) : 0;
                        consolidatedReports.Add(report);
                    }

                    // consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();
                }
            }
            catch (Exception ex)
            {
                LogisticConsolidatedReport recuirementerror = new LogisticConsolidatedReport();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public List<ConsolidatedBasePriceReports> GetConsolidatedBasePriceReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ConsolidatedBasePriceReports> consolidatedReports = new List<ConsolidatedBasePriceReports>();
            List<RequirementUsersByRank> revisedReport = new List<RequirementUsersByRank>();
            List<RequirementUsersByRank> initialReport = new List<RequirementUsersByRank>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.UtcNow.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.UtcNow.ToString();
                }

                //DateTime fromDate = Convert.ToDateTime(from);
                //DateTime toDate = Convert.ToDateTime(to);
                sd.Add("P_FROM_DATE", from);
                sd.Add("P_TO_DATE", to);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetConsolidatedBasePriceReports", sd);


                #region Revised Details (Table 1)

                //List<RevisedDetails> revisedDetails = new List<RevisedDetails>();

                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[1].Rows)
                    {
                        RequirementUsersByRank revised = new RequirementUsersByRank();
                        revised.UID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                        revised.RequirementID = row1["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row1["REQ_ID"]) : 0;
                        revised.RevBasePrice = row1["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_BASE_PRICE"]) : 0;
                        revised.InitBasePrice = row1["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["INIT_BASE_PRICE"]) : 0;
                        revised.Rank = row1["RN"] != DBNull.Value ? Convert.ToInt32(row1["RN"]) : 0;
                        revised.Quantity = row1["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row1["QUANTITY"]) : 0;
                        revised.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;

                        revisedReport.Add(revised);
                    }
                }

                #endregion Revised Details

                #region initial details (table 2)

                // list<initialdetails> initialdetails = new list<initialdetails>();

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        RequirementUsersByRank initial = new RequirementUsersByRank();

                        //U_ID, REQ_ID, REV_BASE_PRICE, INIT_BASE_PRICE, RN, QUANTITY, COMPANY_NAME
                        initial.UID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                        initial.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                        initial.RevBasePrice = row2["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_BASE_PRICE"]) : 0;
                        initial.InitBasePrice = row2["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["INIT_BASE_PRICE"]) : 0;
                        initial.Rank = row2["RN"] != DBNull.Value ? Convert.ToInt32(row2["RN"]) : 0;
                        initial.Quantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                        // initial.CompanyName = row2["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row2["COMPANY_NAME"]) : string.Empty;

                        initialReport.Add(initial);
                    }
                }

                #endregion requirement items

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        ConsolidatedBasePriceReports report = new ConsolidatedBasePriceReports();
                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.UID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                        report.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        report.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                        // report.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                        report.Closed = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        report.NoOfVendorsInvited = row["NoOfVendorsInvited"] != DBNull.Value ? Convert.ToInt32(row["NoOfVendorsInvited"]) : 0;
                        report.NoOfvendorsParticipated = row["NoOfvendorsParticipated"] != DBNull.Value ? Convert.ToInt32(row["NoOfvendorsParticipated"]) : 0;
                        report.ReqCategory = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]) : string.Empty;


                        RequirementUsersByRank InitialUser = initialReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL1 = revisedReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL2 = revisedReport.Where(a => a.Rank == 2 && a.RequirementID == report.RequirementID).FirstOrDefault();

                        #region Initial L1 START
                        if (InitialUser != null)
                        {
                            report.IL1_UID = InitialUser.UID;
                            report.IL1_RevBasePrice = InitialUser.RevBasePrice;
                            report.IL1_InitBasePrice = InitialUser.InitBasePrice;
                            report.IL1_L1InitialBasePrice = InitialUser.L1InitialBasePrice;
                            report.IL1_Rank = InitialUser.Rank;
                            report.IL1_Quantity = InitialUser.Quantity;
                            report.IL1_CompanyName = InitialUser.CompanyName;
                        }

                        #endregion Initial L1 END

                        #region Revised RL1 START
                        if (RevisedUserL1 != null)
                        {
                            report.RL1_UID = RevisedUserL1.UID;
                            report.RL1_RevBasePrice = RevisedUserL1.RevBasePrice;
                            report.RL1_InitBasePrice = RevisedUserL1.InitBasePrice;
                            report.RL1_L1InitialBasePrice = RevisedUserL1.L1InitialBasePrice;
                            report.RL1_Rank = RevisedUserL1.Rank;
                            report.RL1_Quantity = RevisedUserL1.Quantity;
                            report.RL1_CompanyName = RevisedUserL1.CompanyName;
                        }

                        #endregion Revised RL1 END

                        #region Revised RL2 START
                        if (RevisedUserL2 != null)
                        {
                            report.RL2_UID = RevisedUserL2.UID;
                            report.RL2_RevBasePrice = RevisedUserL2.RevBasePrice;
                            report.RL2_InitBasePrice = RevisedUserL2.InitBasePrice;
                            report.RL2_L1InitialBasePrice = RevisedUserL2.L1InitialBasePrice;
                            report.RL2_Rank = RevisedUserL2.Rank;
                            report.RL2_Quantity = RevisedUserL2.Quantity;
                            report.RL2_CompanyName = RevisedUserL2.CompanyName;
                            #endregion Revised RL2 END
                        }

                        report.BasePriceSavings = report.IL1_InitBasePrice - report.RL1_RevBasePrice;
                        if (report.BasePriceSavings > 0)
                        {
                            report.BasePriceSavings = Math.Round(report.BasePriceSavings, decimal_round);
                        }
                        else
                        {
                            report.BasePriceSavings = 0;
                        }

                        report.SavingsPercentage = (100 - (report.RL1_RevBasePrice / report.IL1_InitBasePrice) * 100);
                        if (report.SavingsPercentage > 0)
                        {
                            report.SavingsPercentage = Math.Round(report.SavingsPercentage, decimal_round);
                        }
                        else
                        {
                            report.SavingsPercentage = 0;
                        }


                        //report.SavingsPercentage = Math.Round(report.SavingsPercentage, decimal_round);
                        //report.RevisedUserL1 = RevisedUserL1;
                        //report.RevisedUserL2 = RevisedUserL2;

                        consolidatedReports.Add(report);
                    }

                    consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();





                }
            }
            catch (Exception ex)
            {
                ConsolidatedBasePriceReports recuirementerror = new ConsolidatedBasePriceReports();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        #endregion

        #region Post

        #endregion

        #region Private


        private void SaveFile(string fileName, byte[] fileContent)
        {
            var allowedExtns = ConfigurationManager.AppSettings["SUPPORTED.FILE.EXT"].ToString().Split(',').ToList();
            var isValid = allowedExtns.Any(e => fileName.ToLower().EndsWith(e));

            if (isValid)
            {
                Utilities.SaveFile(fileName, fileContent);
            }
            else
            {
                //logger.Warn("Unsupported file uploaded: " + fileName);
            }
        }

        private Response SaveAttachment(string path)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {   
                sd.Add("P_PATH", path);
                DataSet ds = sqlHelper.SelectList("cp_SaveAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }        

        #endregion
    }
}
