﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Data;
using System.Reflection;
using PRMServices.Models.Catalog;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Web;
using PRMServices.Models;
using System.Net;
using System.ComponentModel;
using PRM.Core.Common;
using PRMServices.SQLHelper;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Xml;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PRMRealTimePriceService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PRMRealTimePriceService.svc or PRMRealTimePriceService.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMOpsService : IPRMOpsService
    {

        PRMServices prmops = new PRMServices();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        private string GetConnectionString(string domain = null)
        {

            if(string.IsNullOrEmpty(domain))
            {
                //domain = "MySQLConnectionString";
                domain = "SQLConnectionString";
            }
            
            return ConfigurationManager.ConnectionStrings[domain].ConnectionString;
            
        }

        public List<OpsClients> GetClients(string sessionID,string from, string to)
        {
            List<OpsClients> clients = new List<OpsClients>();

            try
            {
                //string query = $@"SELECT * FROM clients WHERE ClientID > 0 and IsValid = 1 order by CL_CompName asc;";
                string body = GetOpsQueries("GetClients");
                body = String.Format(body);
                DataSet d = sqlHelper.ExecuteQuery(body);

                if (d != null && d.Tables.Count > 0)
                {
                    foreach (DataRow row in d.Tables[0].Rows)
                    {
                        OpsClients client = new OpsClients();

                        client.ClientID = row["ClientID"] != DBNull.Value ? Convert.ToInt32(row["ClientID"]) : 0;
                        client.CL_CompID = row["CL_CompID"] != DBNull.Value ? Convert.ToInt32(row["CL_CompID"]) : 0;
                        client.IslogisticEnabled = row["IS_LOGISTIC_ENABLED"] != DBNull.Value ? Convert.ToInt32(row["IS_LOGISTIC_ENABLED"]) : 0;
                        client.IsFarwordEnabled = row["IS_FARWORD_ENABLED"] != DBNull.Value ? Convert.ToInt32(row["IS_FARWORD_ENABLED"]) : 0;
                        client.CL_CompName = row["CL_CompName"] != DBNull.Value ? Convert.ToString(row["CL_CompName"]) : string.Empty;
                        client.CompanyName = row["CompanyName"] != DBNull.Value ? Convert.ToString(row["CompanyName"]) : string.Empty;
                        client.CL_Domain = row["CL_Domain"] != DBNull.Value ? Convert.ToString(row["CL_Domain"]) : string.Empty;
                        client.DateCreated = row["DateCreated"] != DBNull.Value ? Convert.ToDateTime(row["DateCreated"]) : DateTime.Now;
                        client.DateModified = row["DateModified"] != DBNull.Value ? Convert.ToDateTime(row["DateModified"]) : DateTime.Now;
                        client.CreatedBy = row["CreatedBy"] != DBNull.Value ? Convert.ToInt32(row["CreatedBy"]) : 0;
                        client.ModifiedBy = row["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(row["ModifiedBy"]) : 0;
                        client.IsValid = row["IsValid"] != DBNull.Value ? Convert.ToInt32(row["IsValid"]) : 0;
                        client.ClientType = row["ClientType"] != DBNull.Value ? Convert.ToString(row["ClientType"]) : string.Empty;
                        client.ClientStatus = row["ClientStatus"] != DBNull.Value ? Convert.ToString(row["ClientStatus"]) : string.Empty;
                        client.ClientDB = row["ClientDB"] != DBNull.Value ? Convert.ToString(row["ClientDB"]) : string.Empty;


                        clients.Add(client);
                    }

                    foreach (var c in clients)
                    {
                        c.ReqInfo = new OpsClientReqInfo();
                        c.VendorInfo = new OpsClientVendorInfo();
             
                        string body1 = GetOpsQueries("GetClientsData");
                        body1 = String.Format(body1, from, to, c.CreatedBy);
                        DataSet ds1 = sqlHelper.ExecuteQuery(body1);

                        if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0 && ds1.Tables[0].Rows[0][0] != null)
                        {
                            c.ReqInfo.OpenRequirements = ds1.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString()) : 0;
                            c.ReqInfo.TodayNegotiation = ds1.Tables[1].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds1.Tables[1].Rows[0][0].ToString()) : 0;
                            c.ReqInfo.PendingQuatation = ds1.Tables[2].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds1.Tables[2].Rows[0][0].ToString()) : 0;
                            c.ReqInfo.LiveNegotiation = ds1.Tables[3].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds1.Tables[3].Rows[0][0].ToString()) : 0;
                            c.ReqInfo.ClosedRequirements = ds1.Tables[4].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds1.Tables[4].Rows[0][0].ToString()) : 0;
                            c.ReqInfo.ClientSavingsAchive = ds1.Tables[5].Rows[0][0] != DBNull.Value ? Convert.ToDouble(ds1.Tables[5].Rows[0][0].ToString()) : 0;
                            c.VendorInfo.NewVendors = ds1.Tables[6].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds1.Tables[6].Rows[0][0].ToString()) : 0;
                            //c.ReqInfo.REQ_POSTED_ON = ds1.Tables[8].Rows[0][0] != DBNull.Value ? Convert.ToDateTime(ds1.Tables[0].Rows[0][0].ToString()) : DateTime.Now;
                            //c.CompanyName = ds1.Tables[7].Rows[0][0] != DBNull.Value ? Convert.ToString(ds1.Tables[7].Rows[0][0].ToString()) : string.Empty;
                            c.ReqInfo.VednorNeedTraining = ds1.Tables[9].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds1.Tables[9].Rows[0][0].ToString()) : 0;

                        }

                    }

                }
            }
            catch (Exception ex)
            {
                OpsClients recuirementerror = new OpsClients();
                recuirementerror.ErrorMessage = ex.Message;
                clients.Add(recuirementerror);
            }
            return clients;
        }
         
        public List<OpsRequirement> GetClientsRequirements(string sessionID, string domain,string from, string to,int ClientID,int auctionType)
        {
            List<OpsRequirement> clientsrequirement = new List<OpsRequirement>();

            try
            {
                if (auctionType == 0)
                {

                    // auctionType = Reverse
                    string body = GetOpsQueries("GetClientsRequirements");
                    body = String.Format(body, from, to, ClientID);
                    DataSet ds = sqlHelper.ExecuteQuery(body);


                    if (ds != null && ds.Tables.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            OpsRequirement clientreq = new OpsRequirement();


                            clientreq.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                            clientreq.REQ_POSTED_ON = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.Now;
                            clientreq.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                            clientreq.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                            clientreq.VendorsInvited = row["VendorsInvited"] != DBNull.Value ? Convert.ToInt32(row["VendorsInvited"]) : 0;
                            clientreq.IS_QUOTATION_REJECTED = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToString(row["IS_QUOTATION_REJECTED"]) : string.Empty;
                            clientreq.VendorsOnline = row["VendorsOnline"] != DBNull.Value ? Convert.ToInt32(row["VendorsOnline"]) : 0;
                            clientreq.NumberOfBids = row["NumberOfBids"] != DBNull.Value ? Convert.ToInt32(row["NumberOfBids"]) : 0;
                            //clientreq.NotBidded = row["NotBidded"] != DBNull.Value ? Convert.ToInt32(row["NotBidded"]) : 0;
                            clientreq.QUOTATION_FREEZ_TIME = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.Now;
                            clientreq.EXP_START_TIME = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.Now;
                            clientreq.CB_END_TIME = row["CB_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["CB_END_TIME"]) : DateTime.Now;
                            clientreq.START_TIME = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                            if (clientreq.START_TIME == null)
                            {
                                clientreq.START_TIME = DateTime.MaxValue;
                            }
                            clientreq.END_TIME = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                            if (clientreq.END_TIME == null)
                            {
                                clientreq.END_TIME = DateTime.MaxValue;
                            }
                            clientreq.CLOSED = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                            string CLOSED = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                            clientreq.RecuirementValue = row["RecuirementValue"] != DBNull.Value ? Convert.ToDouble(row["RecuirementValue"]) : 0;
                            clientreq.SAVINGS = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                            clientreq.SAVINGS_BEFORE_AUCTION = row["SAVINGS_BEFORE_AUCTION"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS_BEFORE_AUCTION"]) : 0;
                            clientreq.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;
                            clientreq.CustomerName = row["CustomerName"] != DBNull.Value ? Convert.ToString(row["CustomerName"]) : string.Empty;
                            clientreq.PostedByrName = row["PostedByName"] != DBNull.Value ? Convert.ToString(row["PostedByName"]) : string.Empty;
                            clientreq.CustomerPhone = row["CustomerPhone"] != DBNull.Value ? Convert.ToString(row["CustomerPhone"]) : string.Empty;
                            clientreq.CustomerMail = row["CustomerMail"] != DBNull.Value ? Convert.ToString(row["CustomerMail"]) : string.Empty;
                            clientreq.ContactDetails = row["CONTACT_DETAILS"] != DBNull.Value ? Convert.ToString(row["CONTACT_DETAILS"]) : string.Empty;
                            clientreq.IsCapitalReq = row["IS_CAPITAL_REQ"] != DBNull.Value ? (Convert.ToInt32(row["IS_CAPITAL_REQ"]) == 1 ? true : false) : false;
                            clientreq.IsSplitEnabled = row["IS_SPLIT_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_SPLIT_ENABLED"]) == 1 ? true : false) : false;
                            clientreq.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_ENABLED"]) == 1 ? true : false) : false;
                            clientreq.ClientID = row["ClientID"] != DBNull.Value ? Convert.ToInt32(row["ClientID"]) : 0;
                            clientreq.ReqItems = row["ReqItems"] != DBNull.Value ? Convert.ToInt32(row["ReqItems"]) : 0;
                            clientreq.LOT_ID = row["LOT_ID"] != DBNull.Value ? Convert.ToInt32(row["LOT_ID"]) : 0;
                            clientreq.LOT_ORDER = row["LOT_ORDER"] != DBNull.Value ? Convert.ToInt32(row["LOT_ORDER"]) : 0;


                            string fileName = row["req_attachment"] != DBNull.Value ? Convert.ToString(row["req_attachment"]) : string.Empty;
                            string URL = fileName != string.Empty ? fileName : string.Empty;
                            clientreq.AttachmentName = fileName;

                            DateTime now = DateTime.Now;
                            if (clientreq.END_TIME != DateTime.MaxValue && clientreq.END_TIME > now && clientreq.START_TIME < now)
                            {
                                DateTime date = Convert.ToDateTime(clientreq.END_TIME);
                                long diff = Convert.ToInt64((date - now).TotalSeconds);
                                clientreq.TimeLeft = diff;
                                clientreq.CLOSED = prmops.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                            }
                            else if (clientreq.START_TIME != DateTime.MaxValue && clientreq.START_TIME > now)
                            {
                                DateTime date = Convert.ToDateTime(clientreq.START_TIME);
                                long diff = Convert.ToInt64((date - now).TotalSeconds);
                                clientreq.TimeLeft = diff;
                                clientreq.CLOSED = prmops.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                            }
                            else if (clientreq.END_TIME < now)
                            {
                                clientreq.TimeLeft = -1;
                                if ((CLOSED == prmops.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || CLOSED == prmops.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && clientreq.END_TIME < now)
                                {
                                    clientreq.CLOSED = prmops.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                                    //prmops.EndNegotiation(clientreq.RequirementID, clientreq.CustomerID, sessionID);
                                }
                                else
                                {
                                    clientreq.CLOSED = CLOSED;
                                }
                                //if (isPOSent == 0 && requirement.Status != GetEnumDesc<PRMStatus>(PRMStatus.POGenerated.ToString()))
                                //{
                                //    requirement.Status = "CLOSED";
                                //}
                            }
                            else if (clientreq.START_TIME == DateTime.MaxValue)
                            {
                                clientreq.TimeLeft = -1;
                                clientreq.CLOSED = prmops.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                            }
                            if (CLOSED == prmops.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                            {
                                clientreq.TimeLeft = -1;
                                clientreq.CLOSED = CLOSED;
                            }

                            if (clientreq.IS_CB_ENABLED == true && clientreq.CB_END_TIME != DateTime.Now)
                            {
                                clientreq.CLOSED = CLOSED;
                            }

                            clientsrequirement.Add(clientreq);
                        }

                    }
                }

                //if (auctionType == 1)
                //{
                //    // auctionType = Logistics

                //    string query = String.Format(" " +
                //        " SELECT rd.REQ_ID, rd.U_ID, rd.REQ_TITLE, rd.SAVINGS, rd.SAVINGS_BEFORE_AUCTION, rd.EXP_START_TIME, rd.CLOSED, rd.QUOTATION_FREEZ_TIME, rd.END_TIME, rd.START_TIME, " +
                //        " rd.IS_DISCOUNT_QUOTATION, rd.req_attachment,rd.REQ_POSTED_ON, " +
                //        " (SELECT COUNT(*) " +
                //        " FROM negotiationaudit na " +
                //        " WHERE na.REQ_ID = rd.REQ_ID and date(DATE_CREATED) between DATE('{0}') and DATE('{1}')) as NumberOfBids ,  " +
                //        " (SELECT COUNT(*) " +
                //        " FROM logistic_auctiondetails " +
                //        // " WHERE REQ_ID = rd.REQ_ID and(QUOTATION_URL <= 0 or QUOTATION_URL is null) and IS_DELETED = 0 " +
                //        " WHERE REQ_ID = rd.REQ_ID and (IS_SENT = 0) and IS_DELETED = 0 " +
                //        " and date(DATE_CREATED) between DATE('{0}') and DATE('{1}')) AS IS_QUOTATION_REJECTED, " +
                //        " (SELECT CLOSED " +
                //        " FROM logistic_requirementdetails " +
                //        " WHERE REQ_ID = rd.U_ID and date(DATE_CREATED) between DATE('{0}') and DATE('{1}')) AS NegotiationendTime, " +
                //        " (select count(*)   " +
                //        " from logistic_auctiondetails   " +
                //        " where req_id = rd.req_id and IS_DELETED = 0 and date(DATE_CREATED) between DATE('{0}') and DATE('{1}')) AS VendorsInvited, " +
                //        " (select COUNT(*) " +
                //        " from sessions s inner " +
                //        " join logistic_auctiondetails ad1 on s.U_ID = ad1.U_ID " +
                //        " where ad1.REQ_ID = rd.REQ_ID " +
                //        " and s.DATE_MODIFIED between rd.START_TIME and rd.END_TIME and date(s.DATE_MODIFIED) between DATE('{0}') and DATE('{1}')) as VendorsOnline ,  " +
                //        " (select MAX(VEND_INIT_PRICE) " +
                //        " from logistic_auctiondetails " +
                //        " where req_id = rd.req_id AND IS_PARTICIPATED = 1 " +
                //        " AND VEND_INIT_PRICE > 0 AND IS_DELETED = 0 and date(DATE_CREATED) between DATE('{0}') and DATE('{1}')) AS RecuirementValue, " +
                //        " rd.CLOSED as status ,  " +
                //        " (select concat(user.U_FNAME, ' ', user.U_LNAME) from user where user.U_ID = {2}) as CustomerName ,  " +
                //        " (select concat(user.U_FNAME, ' ' ,user.U_LNAME) from user where user.U_ID = rd.U_ID) as PostedByName , " +
                //        " (select U_PHONE from user where user.U_ID = rd.U_ID) as CustomerPhone ,  " +
                //        " (select U_EMAIL from user where user.U_ID = rd.U_ID) as CustomerMail,  " +
                //        " (select U_ID from userdata where IS_SUPER_USER = 1 and userdata.U_ID = {2}) as ClientID ,  " +
                //        " (select COUNT(ITEM_ID) from logistic_requirementitems " +
                //        " where REQ_ID = rd.REQ_ID) as ReqItems " +
                //        " FROM logistic_requirementdetails rd WHERE rd.U_ID in  " +
                //        " (select u_id " +
                //        " from userdata " +
                //        " where SUPER_U_ID = GetSuperUserID({2})) and date(rd.REQ_POSTED_ON) between DATE('{0}') and DATE('{1}') or date(rd.REQ_POSTED_ON) = date('{1}') ORDER BY  rd.REQ_ID DESC "
                //        , from, to, ClientID);
                //    cmd.CommandType = CommandType.Text;
                //    MySqlDataAdapter myDA1 = new MySqlDataAdapter(cmd);
                //    DataSet ds1 = new DataSet();
                //    myDA1.Fill(ds1);
                //    cmd.Connection.Close();




                //    if (ds1 != null && ds1.Tables.Count > 0)
                //    {
                //        foreach (DataRow row in ds1.Tables[0].Rows)
                //        {
                //            OpsRequirement clientreq = new OpsRequirement();


                //            clientreq.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                //            clientreq.REQ_POSTED_ON = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.Now;
                //            clientreq.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                //            clientreq.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                //            clientreq.VendorsInvited = row["VendorsInvited"] != DBNull.Value ? Convert.ToInt32(row["VendorsInvited"]) : 0;
                //            clientreq.IS_QUOTATION_REJECTED = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToString(row["IS_QUOTATION_REJECTED"]) : string.Empty;
                //            clientreq.VendorsOnline = row["VendorsOnline"] != DBNull.Value ? Convert.ToInt32(row["VendorsOnline"]) : 0;
                //            clientreq.NumberOfBids = row["NumberOfBids"] != DBNull.Value ? Convert.ToInt32(row["NumberOfBids"]) : 0;
                //            //clientreq.NotBidded = row["NotBidded"] != DBNull.Value ? Convert.ToInt32(row["NotBidded"]) : 0;
                //            clientreq.QUOTATION_FREEZ_TIME = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.Now;
                //            clientreq.EXP_START_TIME = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.Now;
                //            clientreq.START_TIME = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                //            if (clientreq.START_TIME == null)
                //            {
                //                clientreq.START_TIME = DateTime.MaxValue;
                //            }
                //            clientreq.END_TIME = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                //            if (clientreq.END_TIME == null)
                //            {
                //                clientreq.END_TIME = DateTime.MaxValue;
                //            }
                //            clientreq.CLOSED = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                //            string CLOSED = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                //            clientreq.RecuirementValue = row["RecuirementValue"] != DBNull.Value ? Convert.ToDouble(row["RecuirementValue"]) : 0;
                //            clientreq.SAVINGS = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                //            clientreq.SAVINGS_BEFORE_AUCTION = row["SAVINGS_BEFORE_AUCTION"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS_BEFORE_AUCTION"]) : 0;
                //            clientreq.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;
                //            clientreq.CustomerName = row["CustomerName"] != DBNull.Value ? Convert.ToString(row["CustomerName"]) : string.Empty;
                //            clientreq.PostedByrName = row["PostedByName"] != DBNull.Value ? Convert.ToString(row["PostedByName"]) : string.Empty;
                //            clientreq.CustomerPhone = row["CustomerPhone"] != DBNull.Value ? Convert.ToString(row["CustomerPhone"]) : string.Empty;
                //            clientreq.CustomerMail = row["CustomerMail"] != DBNull.Value ? Convert.ToString(row["CustomerMail"]) : string.Empty;
                //            //clientreq.ContactDetails = row["CONTACT_DETAILS"] != DBNull.Value ? Convert.ToString(row["CONTACT_DETAILS"]) : string.Empty;
                //            //clientreq.IsCapitalReq = row["IS_CAPITAL_REQ"] != DBNull.Value ? (Convert.ToInt32(row["IS_CAPITAL_REQ"]) == 1 ? true : false) : false;
                //            //clientreq.IsSplitEnabled = row["IS_SPLIT_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_SPLIT_ENABLED"]) == 1 ? true : false) : false;
                //            clientreq.ClientID = row["ClientID"] != DBNull.Value ? Convert.ToInt32(row["ClientID"]) : 0;
                //            clientreq.ReqItems = row["ReqItems"] != DBNull.Value ? Convert.ToInt32(row["ReqItems"]) : 0;
                //            string fileName = row["req_attachment"] != DBNull.Value ? Convert.ToString(row["req_attachment"]) : string.Empty;
                //            string URL = fileName != string.Empty ? fileName : string.Empty;
                //            clientreq.AttachmentName = fileName;

                //            DateTime now = DateTime.Now;
                //            if (clientreq.END_TIME != DateTime.MaxValue && clientreq.END_TIME > now && clientreq.START_TIME < now)
                //            {
                //                DateTime date = Convert.ToDateTime(clientreq.END_TIME);
                //                long diff = Convert.ToInt64((date - now).TotalSeconds);
                //                clientreq.TimeLeft = diff;
                //                clientreq.CLOSED = prmops.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                //            }
                //            else if (clientreq.START_TIME != DateTime.MaxValue && clientreq.START_TIME > now)
                //            {
                //                DateTime date = Convert.ToDateTime(clientreq.START_TIME);
                //                long diff = Convert.ToInt64((date - now).TotalSeconds);
                //                clientreq.TimeLeft = diff;
                //                clientreq.CLOSED = prmops.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                //            }
                //            else if (clientreq.END_TIME < now)
                //            {
                //                clientreq.TimeLeft = -1;
                //                if ((CLOSED == prmops.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || CLOSED == prmops.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && clientreq.END_TIME < now)
                //                {
                //                    clientreq.CLOSED = prmops.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                //                    prmops.EndNegotiation(clientreq.RequirementID, clientreq.CustomerID, sessionID);
                //                }
                //                else
                //                {
                //                    clientreq.CLOSED = CLOSED;
                //                }
                //                //if (isPOSent == 0 && requirement.Status != GetEnumDesc<PRMStatus>(PRMStatus.POGenerated.ToString()))
                //                //{
                //                //    requirement.Status = "CLOSED";
                //                //}
                //            }
                //            else if (clientreq.START_TIME == DateTime.MaxValue)
                //            {
                //                clientreq.TimeLeft = -1;
                //                clientreq.CLOSED = prmops.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                //            }
                //            if (CLOSED == prmops.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                //            {
                //                clientreq.TimeLeft = -1;
                //                clientreq.CLOSED = CLOSED;
                //            }

                //            clientsrequirement.Add(clientreq);
                //        }

                //    }

                //}

                //if (auctionType == 2)
                //{
                //    List<OpsRequirement> opsreq = OPSUtilities.GetClientRequirementsDyn(domain, from, to, ClientID, auctionType, sessionID);
                //    clientsrequirement = opsreq;
                //}
            }
            catch (Exception ex)
            {
                OpsRequirement recuirementerror = new OpsRequirement();
                recuirementerror.ErrorMessage = ex.Message;
                clientsrequirement.Add(recuirementerror);
            }
            
            //using (MySqlCommand cmd = new MySqlCommand()) 
            //{
            //    using (cmd.Connection = new MySqlConnection(GetConnectionString(domain)))
            //    {
                    
            //    }
            //}
            return clientsrequirement;
        }


        public List<OpsRequirementVendors> GetClientsVendors(int REQ_ID, string sessionID, string domain, int ClientID, int auctionType)
        {
            List<OpsRequirementVendors> requirementVendors = new List<OpsRequirementVendors>();
            List<OpsVendorDetails> requirementVendors1 = new List<OpsVendorDetails>();
            
             List<OpsClients> clients = new List<OpsClients>();

            try
            {
                if (auctionType == 0)
                {
                    // auctionType = Reverse
                    string query1 = GetOpsQueries("GetClientsVendors1");
                    query1 = String.Format(query1, REQ_ID);
                    DataSet ds = sqlHelper.ExecuteQuery(query1);

                    string query2 = GetOpsQueries("GetClientsVendors2");
                    query2 = String.Format(query2, REQ_ID);
                    DataSet ds1 = sqlHelper.ExecuteQuery(query2);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            OpsRequirementVendors vendor = new OpsRequirementVendors();

                            vendor.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;

                            vendor.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                            vendor.REQ_TITLE = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                            vendor.REQ_POSTED_ON = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.Now;
                            vendor.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                            vendor.IS_QUOTATION_REJECTED = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToString(row["IS_QUOTATION_REJECTED"]) : string.Empty;
                            vendor.COMPANY_NAME = row["INSTITUTION"] != DBNull.Value ? Convert.ToString(row["INSTITUTION"]) : string.Empty;
                            vendor.VendorName = row["VendorName"] != DBNull.Value ? Convert.ToString(row["VendorName"]) : string.Empty;
                            vendor.U_PHONE = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                            vendor.U_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                            //vendor.VendorPhone = row["VendorPhone"] != DBNull.Value ? Convert.ToString(row["VendorPhone"]) : string.Empty;
                            vendor.QUOTATION_FREEZ_TIME = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.Now;
                            vendor.EXP_START_TIME = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.Now;
                            vendor.START_TIME = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.Now;
                            vendor.IsOtpVerified = row["isOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isOTPVerified"]) : 0;
                            vendor.IsEmailVerified = row["isEmailOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isEmailOTPVerified"]) : 0;
                            //vendor.LastActiveTime = GetLastActiveTime(sessionID, vendor.VendorID);

                            requirementVendors.Add(vendor);
                        }

                    }
                    if (ds1 != null && ds1.Tables.Count > 0)
                    {
                        foreach (DataRow row in ds1.Tables[0].Rows)
                        {
                            OpsVendorDetails vendor1 = new OpsVendorDetails();
                            vendor1.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                            vendor1.IsPrimary = row["IS_PRIMARY"] != DBNull.Value ? Convert.ToInt32(row["IS_PRIMARY"]) : 0;
                            vendor1.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                            vendor1.VendorPhone = row["VendorPhone"] != DBNull.Value ? Convert.ToString(row["VendorPhone"]) : string.Empty;
                            vendor1.VendorAtlMail = row["VendorAtlMail"] != DBNull.Value ? Convert.ToString(row["VendorAtlMail"]) : string.Empty;
                            vendor1.COMPANY_NAME = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                            requirementVendors1.Add(vendor1);
                        }

                    }

                    foreach (OpsRequirementVendors vend in requirementVendors)
                    {
                        vend.OpsVendorDetails = requirementVendors1.Where(a => a.U_ID == vend.U_ID).ToList();

                    }

                }

                //if (auctionType == 1)
                //{
                //    // auctionType = Logistics
                //    cmd.CommandText = String.Format(" SELECT logistic_auctiondetails.REQ_ID, logistic_requirementdetails.REQ_TITLE, logistic_requirementdetails.REQ_POSTED_ON, logistic_auctiondetails.U_ID, " +
                //                                    " logistic_auctiondetails.IS_QUOTATION_REJECTED, " +
                //                                    "  userdata.INSTITUTION,concat(user.U_FNAME, ' ', user.U_LNAME) VendorName, user.U_PHONE, user.U_EMAIL, " +
                //                                    " user.U_ALTPHONE, logistic_requirementdetails.QUOTATION_FREEZ_TIME, " +
                //                                    " logistic_requirementdetails.START_TIME, logistic_requirementdetails.EXP_START_TIME " +
                //                                    " FROM logistic_auctiondetails " +
                //                                    " INNER JOIN logistic_requirementdetails ON logistic_auctiondetails.REQ_ID = logistic_requirementdetails.REQ_ID " +
                //                                    " INNER JOIN user ON logistic_auctiondetails.U_ID = user.U_ID " +
                //                                    " INNER JOIN userdata ON logistic_auctiondetails.U_ID = userdata.U_ID " +
                //                                    " where logistic_requirementdetails.REQ_ID = {0} ORDER BY logistic_requirementdetails.QUOTATION_FREEZ_TIME DESC; ", REQ_ID);
                //    cmd.CommandType = CommandType.Text;
                //    MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                //    DataSet ds = new DataSet();
                //    myDA.Fill(ds);
                //    cmd.Connection.Close();

                //    cmd.CommandText = String.Format(" SELECT v.U_ID,v.U_PHONE as VendorPhone,v.U_EMAIL as VendorAtlMail,company.COMPANY_NAME " +
                //                                    " FROM logistic_auctiondetails " +
                //                                    " INNER JOIN logistic_requirementdetails ON logistic_auctiondetails.REQ_ID = logistic_requirementdetails.REQ_ID " +
                //                                    " INNER JOIN company ON logistic_auctiondetails.U_ID = company.CREATED_BY " +
                //                                    " INNER JOIN user ON logistic_auctiondetails.U_ID = user.U_ID " +
                //                                    " INNER JOIN vendors v ON user.U_ID = v.U_ID " +
                //                                    " where logistic_requirementdetails.REQ_ID = {0} ORDER BY logistic_requirementdetails.QUOTATION_FREEZ_TIME DESC; ", REQ_ID);
                //    cmd.CommandType = CommandType.Text;
                //    MySqlDataAdapter myDA1 = new MySqlDataAdapter(cmd);
                //    DataSet ds1 = new DataSet();
                //    myDA1.Fill(ds1);
                //    cmd.Connection.Close();



                //    if (ds != null && ds.Tables.Count > 0)
                //    {
                //        foreach (DataRow row in ds.Tables[0].Rows)
                //        {
                //            OpsRequirementVendors vendor = new OpsRequirementVendors();

                //            vendor.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                //            vendor.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                //            vendor.REQ_TITLE = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                //            vendor.REQ_POSTED_ON = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.Now;
                //            vendor.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                //            vendor.IS_QUOTATION_REJECTED = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToString(row["IS_QUOTATION_REJECTED"]) : string.Empty;
                //            vendor.COMPANY_NAME = row["INSTITUTION"] != DBNull.Value ? Convert.ToString(row["INSTITUTION"]) : string.Empty;
                //            vendor.VendorName = row["VendorName"] != DBNull.Value ? Convert.ToString(row["VendorName"]) : string.Empty;
                //            vendor.U_PHONE = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                //            vendor.U_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                //            //vendor.VendorPhone = row["VendorPhone"] != DBNull.Value ? Convert.ToString(row["VendorPhone"]) : string.Empty;
                //            vendor.QUOTATION_FREEZ_TIME = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.Now;
                //            vendor.EXP_START_TIME = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.Now;
                //            vendor.START_TIME = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.Now;
                //            // vendor.VendorAtlMail = row["VendorAtlMail"] != DBNull.Value ? Convert.ToString(row["VendorAtlMail"]) : string.Empty;
                //            //vendor.LastActiveTime = GetLastActiveTime(sessionID, vendor.VendorID);

                //            requirementVendors.Add(vendor);
                //        }

                //    }
                //    if (ds1 != null && ds1.Tables.Count > 0)
                //    {
                //        foreach (DataRow row in ds1.Tables[0].Rows)
                //        {
                //            OpsVendorDetails vendor1 = new OpsVendorDetails();
                //            vendor1.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                //            vendor1.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                //            vendor1.VendorPhone = row["VendorPhone"] != DBNull.Value ? Convert.ToString(row["VendorPhone"]) : string.Empty;
                //            vendor1.VendorAtlMail = row["VendorAtlMail"] != DBNull.Value ? Convert.ToString(row["VendorAtlMail"]) : string.Empty;
                //            vendor1.COMPANY_NAME = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                //            requirementVendors1.Add(vendor1);
                //        }

                //    }


                //    foreach (OpsRequirementVendors vend in requirementVendors)
                //    {
                //        vend.OpsVendorDetails = requirementVendors1.Where(a => a.U_ID == vend.U_ID).ToList();

                //    }

                //}

                //if (auctionType == 2)
                //{
                //    List<OpsRequirementVendors> opsvend = OPSUtilities.GetClientsVendorsDyn(REQ_ID, sessionID, domain, ClientID, auctionType);
                //    requirementVendors = opsvend;
                //}

            }
            catch (Exception ex)
            {
                OpsRequirementVendors recuirementerror = new OpsRequirementVendors();
                recuirementerror.ErrorMessage = ex.Message;
                requirementVendors.Add(recuirementerror);
            }

            return requirementVendors;
        }

        public OpsRequirementVendors GetVendorDetails(int vendorID,string domain,string sessionid)
        {
            OpsRequirementVendors details = new OpsRequirementVendors();

            try
            {
                string query = GetOpsQueries("GetVendorDetails");
                query = String.Format(query, vendorID);
                DataSet ds = sqlHelper.ExecuteQuery(query);

                if (ds != null && ds.Tables.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    details.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    details.COMPANY_NAME = row["INSTITUTION"] != DBNull.Value ? Convert.ToString(row["INSTITUTION"]) : string.Empty;
                    details.VendorName = row["VendorName"] != DBNull.Value ? Convert.ToString(row["VendorName"]) : string.Empty;
                    details.U_PHONE = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    details.U_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    details.IsOtpVerified = row["isOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isOTPVerified"]) : 0;
                    details.IsEmailVerified = row["isEmailOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isEmailOTPVerified"]) : 0;
                    details.EmailOtp = row["EMAIL_OTP"] != DBNull.Value ? Convert.ToInt32(row["EMAIL_OTP"]) : 0;
                    details.SmsOtp = row["OTP"] != DBNull.Value ? Convert.ToInt32(row["OTP"]) : 0;
                    details.VendorLoginID = row["LG_LOGIN"] != DBNull.Value ? Convert.ToString(row["LG_LOGIN"]) : string.Empty;
                    details.VendorLoginPass = row["LG_PASSWORD"] != DBNull.Value ? Convert.ToString(row["LG_PASSWORD"]) : string.Empty;

                }
            }
            catch (Exception ex)
            {

            }


            return details;
        }

        public Response SendOTP(int userID, string domain, string phone, int issmsverified, int isemailverified, string email)
        {
            Response response = new Response();
            PRMServices prm = new PRMServices();

            if (!string.IsNullOrEmpty(phone) && phone.Length == 10
                && (phone.StartsWith("6") || phone.StartsWith("7") || phone.StartsWith("8") || phone.StartsWith("9")))
            {

            }
            else
            {
                // return response;
            }

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                string query1 = String.Format("SELECT U_ID FROM userdata WHERE U_ID = {0};", userID);
                DataSet ds = sqlHelper.ExecuteQuery(query1);
                User info = new User();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    info.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                }

                if (info.UserID != 0)
                {
                    OpsRequirementVendors user = GetVendorDetails(info.UserID, domain, null);
                    if (issmsverified == 0)
                    {
                        Random random = new Random();
                        int num = 0;
                        //int num = random.Next(100001, 1000000);
                        if (user.SmsOtp != 0)
                        {
                            num = user.SmsOtp;
                        }
                        else
                        {
                            num = random.Next(100001, 1000000);
                        }
                        // num = random.Next(100001, 1000000);
                        string query2 = GetOpsQueries("SendOTPSMS");
                        query2 = String.Format(query2, num, userID);
                        DataSet ds1 = sqlHelper.ExecuteQuery(query2);
                        prm.SendSMS(string.Empty, phone, Utilities.OTP_MSG.Replace("$$$$", num.ToString()),0,0 ,"", null).ConfigureAwait(false);
                    }
                    if (isemailverified == 0)
                    {
                        Random random1 = new Random();
                        int num1 = random1.Next(10, 99);
                        int num2 = random1.Next(1000, 9999);
                        int num3 = 0;
                        if (user.EmailOtp != 0)
                        {
                            num3 = user.EmailOtp;
                        }
                        else
                        {
                            num3 = int.Parse(num1.ToString() + num2.ToString());
                        }

                        string query3 = GetOpsQueries("SendOTPEMAIL");
                        query3 = String.Format(query3, num3, userID);
                        DataSet ds2 = sqlHelper.ExecuteQuery(query3);
                        string body = Utilities.GenerateEmailBody("emailOTP");
                        body = String.Format(body, num3);
                        prm.SendEmail(email, "PRM360 Email Verification", body, 0, 0, "", null, null).ConfigureAwait(false);
                    }

                }

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response GetPassword(string type,string field,string key, string domain)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString(domain)))
                { 
                    try
                    {
                        string query = string.Empty;
                        
                        if (type == "phone")
                        {

                            query = string.Format("select ul.lg_password from userlogins ul inner join user u on u.u_id = ul.u_id where u.u_phone = {0};", field);                
                            
                        }
                        else if(type == "uid")
                        {
                            query = string.Format("select lg_password from userlogins ul where ul.u_id = {0};", field);
                        }
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();

                        if (!string.IsNullOrEmpty(query))
                        {
                            cmd.CommandText = query;
                            cmd.CommandType = CommandType.Text;
                            myDA.Fill(ds);

                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                            {
                                response.Message = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][0].ToString()) : string.Empty;
                            }
                            cmd.Parameters.Clear();

                        }
                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response;
        }
        public List<OpsRequirementVendors> GetVendorsOnline(int REQ_ID, string sessionID, int userid, string domain, int auctionType)
        {
            List<OpsRequirementVendors> requirementVendors = new List<OpsRequirementVendors>();
           
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString(domain)))
                {
                    try
                    {
                        if (auctionType == 0)
                        {
                            // auctionType = Reverse
                            var client = new WebClient();
                            var content = client.DownloadString("https://" + domain + ".prm360.com/WEBDEV/svc/PRMService.svc/REST/getdate?uid=" + userid);
                            cmd.CommandText = String.Format("SELECT auctiondetails.REQ_ID, auctiondetails.U_ID, " +

                                                            " company.COMPANY_NAME, concat(user.U_FNAME, ' ',user.U_LNAME) VendorName,user.U_EMAIL,user.U_PHONE " +

                                                            " FROM auctiondetails " +
                                                            " INNER JOIN requirementdetails ON auctiondetails.REQ_ID = requirementdetails.REQ_ID " +
                                                            " INNER JOIN company ON auctiondetails.U_ID = company.CREATED_BY " +
                                                            " INNER JOIN user ON auctiondetails.U_ID = user.U_ID where requirementdetails.REQ_ID = {0} ", REQ_ID);
                            cmd.CommandType = CommandType.Text;
                            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                            DataSet ds = new DataSet();
                            myDA.Fill(ds);
                            cmd.Connection.Close();
                            if (ds != null && ds.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    OpsRequirementVendors vendor = new OpsRequirementVendors();
                                    //var client = new WebClient();
                                    //var content = client.DownloadString("https://prm360.com/WEBDEV/svc/PRMService.svc/REST/getonlinestatus?userid=" + vendor.U_ID + "&sessionid=" + sessionID);
                                    vendor.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                                    vendor.VendorName = row["VendorName"] != DBNull.Value ? Convert.ToString(row["VendorName"]) : string.Empty;
                                    vendor.U_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                                    vendor.U_PHONE = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                                    vendor.COMPANY_NAME = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                                    requirementVendors.Add(vendor);
                                }
                            }
                        }

                        if (auctionType == 1)
                        {
                            // auctionType = Logistics
                            var client = new WebClient();
                            var content = client.DownloadString("https://" + domain + ".prm360.com/WEBDEV/svc/PRMService.svc/REST/getdate?uid=" + userid);
                            cmd.CommandText = String.Format("SELECT logistic_auctiondetails.REQ_ID, logistic_auctiondetails.U_ID, " +

                                                            " company.COMPANY_NAME, concat(user.U_FNAME, ' ',user.U_LNAME) VendorName,user.U_EMAIL,user.U_PHONE " +

                                                            " FROM logistic_auctiondetails " +
                                                            " INNER JOIN logistic_requirementdetails ON logistic_auctiondetails.REQ_ID = logistic_requirementdetails.REQ_ID " +
                                                            " INNER JOIN company ON logistic_auctiondetails.U_ID = company.CREATED_BY " +
                                                            " INNER JOIN user ON logistic_auctiondetails.U_ID = user.U_ID where logistic_requirementdetails.REQ_ID = {0} ", REQ_ID);
                            cmd.CommandType = CommandType.Text;
                            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                            DataSet ds = new DataSet();
                            myDA.Fill(ds);
                            cmd.Connection.Close();
                            if (ds != null && ds.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    OpsRequirementVendors vendor = new OpsRequirementVendors();
                                    //var client = new WebClient();
                                    //var content = client.DownloadString("https://prm360.com/WEBDEV/svc/PRMService.svc/REST/getonlinestatus?userid=" + vendor.U_ID + "&sessionid=" + sessionID);
                                    vendor.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                                    vendor.VendorName = row["VendorName"] != DBNull.Value ? Convert.ToString(row["VendorName"]) : string.Empty;
                                    vendor.U_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                                    vendor.U_PHONE = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                                    vendor.COMPANY_NAME = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                                    requirementVendors.Add(vendor);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        OpsRequirementVendors recuirementerror = new OpsRequirementVendors();
                        recuirementerror.ErrorMessage = ex.Message;
                        requirementVendors.Add(recuirementerror);
                    }
                }
            }

            return requirementVendors;
        }

        public static object getCacheObject(string cacheKey)
        {
            if (ServerConfig.CachingEnabled)
            {
                return WCFCache.Current[cacheKey];
            }

            return null;
        }

        public List<OpsRequirementVendors> GetPendingQuotations(int REQ_ID, int ClientID, string domain,int auctionType, string sessionID)
        {
            List<OpsRequirementVendors> pendingQuotations = new List<OpsRequirementVendors>();

            try
            {
                if (auctionType == 0)
                {
                    // auctionType = Reverse
                    string query = GetOpsQueries("GetPendingQuotationsReg");
                    query = String.Format(query, ClientID, REQ_ID);
                    DataSet ds = sqlHelper.ExecuteQuery(query);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            OpsRequirementVendors quotation = new OpsRequirementVendors();

                            quotation.COMPANY_NAME = row["INSTITUTION"] != DBNull.Value ? Convert.ToString(row["INSTITUTION"]) : string.Empty;
                            quotation.VendorName = row["VendorName"] != DBNull.Value ? Convert.ToString(row["VendorName"]) : string.Empty;
                            quotation.U_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                            quotation.U_PHONE = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                            quotation.VendorID = row["VendorID"] != DBNull.Value ? Convert.ToInt32(row["VendorID"]) : 0;
                            quotation.RequirementID = row["RequirementID"] != DBNull.Value ? Convert.ToInt32(row["RequirementID"]) : 0;
                            quotation.ReqUserID = row["ReqUserID"] != DBNull.Value ? Convert.ToInt32(row["ReqUserID"]) : 0;


                            pendingQuotations.Add(quotation);
                        }

                    }
                }
                
            }
            catch (Exception ex)
            {
                OpsRequirementVendors recuirementerror = new OpsRequirementVendors();
                recuirementerror.ErrorMessage = ex.Message;
                pendingQuotations.Add(recuirementerror);
            }
            
            return pendingQuotations;
        }

        public List<OpsRequirementVendors> GetAllQuotationsVendors(int REQ_ID, int ClientID, string domain,int auctionType, string sessionID)
        {
            List<OpsRequirementVendors> pendingQuotations = new List<OpsRequirementVendors>();

            try
            {
                if (auctionType == 0)
                {
                    string query = GetOpsQueries("GetAllQuotationsVendorsReg");
                    query = String.Format(query, ClientID, REQ_ID);
                    DataSet ds = sqlHelper.ExecuteQuery(query);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            OpsRequirementVendors quotation = new OpsRequirementVendors();

                            quotation.COMPANY_NAME = row["INSTITUTION"] != DBNull.Value ? Convert.ToString(row["INSTITUTION"]) : string.Empty;
                            quotation.VendorName = row["VendorName"] != DBNull.Value ? Convert.ToString(row["VendorName"]) : string.Empty;
                            quotation.U_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                            quotation.U_PHONE = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                            quotation.VendorID = row["VendorID"] != DBNull.Value ? Convert.ToInt32(row["VendorID"]) : 0;
                            quotation.RequirementID = row["RequirementID"] != DBNull.Value ? Convert.ToInt32(row["RequirementID"]) : 0;
                            quotation.ReqUserID = row["ReqUserID"] != DBNull.Value ? Convert.ToInt32(row["ReqUserID"]) : 0;
                            quotation.IS_QUOTATION_REJECTED = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToString(row["IS_QUOTATION_REJECTED"]) : string.Empty;
                            quotation.QUOTATION_URL = row["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row["QUOTATION_URL"]) : string.Empty;


                            pendingQuotations.Add(quotation);
                        }

                    }
                }

                //if (auctionType > 0)
                //{
                //    List<OpsRequirementVendors> remindVendors = OPSUtilities.GetAllQuotationsVendorsDyn(REQ_ID, ClientID, domain, auctionType, sessionID);
                //    pendingQuotations = remindVendors;
                //}

            }
            catch (Exception ex)
            {
                OpsRequirementVendors recuirementerror = new OpsRequirementVendors();
                recuirementerror.ErrorMessage = ex.Message;
                pendingQuotations.Add(recuirementerror);
            }
            finally
            {
                //cmd.Connection.Close();
            }

            return pendingQuotations;
        }

        public PRMServices GetReminders(int REQ_ID,string domain,int ClientID,string sessionID)
        {
            PRMServices prmops = new PRMServices();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString(domain)))
                {
                    try
                    {
                        int isValidSession = Utilities.ValidateSession(sessionID);
                        domain = GetDomain(domain);
                        var client = new WebClient();
                        var content = client.DownloadString("https://" + domain + ".prm360.com/WEBDEV/svc/PRMService.svc/REST/getvendorreminders?reqid=" + REQ_ID + "&userid=" + ClientID  + "&sessionID=" + sessionID);
                        //string requestType = "REMINDER";
                        //prmops.VendorReminders(REQ_ID, ClientID, message, vendorIDs, vendorCompanyNames, sessionID, requestType, domain);
                         
                    }
                    catch (Exception ex)
                    {
                       
                    }
                }
            }
           

            return prmops;
        }

        private List<OpsClients> GetClientDB(string sessionID)
        {
            List<OpsClients> clients = new List<OpsClients>();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        string query = $@"SELECT * FROM clients WHERE ClientID > 0 and IsValid = 1 ;";
                        DataSet ds = sqlHelper.ExecuteQuery(query);
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                OpsClients client = new OpsClients();
                                client.ClientDB = row["ClientDB"] != DBNull.Value ? Convert.ToString(row["ClientDB"]) : string.Empty;
                                clients.Add(client);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        OpsClients recuirementerror = new OpsClients();
                        recuirementerror.ErrorMessage = ex.Message;
                        clients.Add(recuirementerror);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return clients;
        }


        public string GetDomain(string domain)
        {
            string domain1 = "";
            try
            {
                var d = ConfigurationManager.AppSettings[domain].ToString();
                // if (domain == "syngenetestdb") { domain = "syngene"; }
                domain1 = d;
            }
            catch (Exception ex) { }
            return domain1;
        }

        public Response ChangePassword(int userID, string username, string oldPass, string newPass, string domain)
        {
            PRMServices prm = new PRMServices();
            Response response = new Response();
            string trimPassword = oldPass.Trim();
            string trimPassword1 = newPass.Trim();
            try
            {
                string query = String.Format("SELECT U_ID FROM [user] WHERE U_ID = {0};", userID);
                DataSet ds1 = sqlHelper.ExecuteQuery(query);
                
                string message = string.Empty;
                User info = new User();
                if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds1.Tables[0].Rows[0];
                    info.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                }

                if (info.UserID != 0)
                {
                    string query1 = GetOpsQueries("ChangePassword");
                    query1 = String.Format(query1, newPass, info.UserID);
                    DataSet ds2 = sqlHelper.ExecuteQuery(query1);
                    OpsRequirementVendors user = GetVendorDetails(info.UserID, domain, null);
                    string body = prm.GenerateEmailBody("ChangePassWordemail");
                    body = String.Format(body, user.VendorName, "", newPass);
                    prm.SendEmail(user.U_EMAIL, "PRM360 Password Reset", body,0,0,"").ConfigureAwait(false);
                    string bodysms = prm.GenerateEmailBody("ChangePassWordsms");
                    bodysms = String.Format(body, user.VendorName, "", newPass);
                    prm.SendSMS(string.Empty, user.U_PHONE, bodysms, 0,0,"", null).ConfigureAwait(false);
                }
                else
                {
                    response.ErrorMessage = "User does not exist";
                }


            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            finally
            {
               // cmd.Connection.Close();
            }


            return response;
        }

        public OpsClients GetClientDetails(string sessionID, string domain)
        {
            OpsClients client = new OpsClients();
            try
            {


                string query = string.Format("SELECT * FROM clients WHERE ClientID > 0 and IsValid = 1 and ClientDB = '{0}' order by CL_CompName asc;", domain);
                DataSet ds = sqlHelper.ExecuteQuery(query);

                if (ds != null && ds.Tables.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    client.ClientID = row["ClientID"] != DBNull.Value ? Convert.ToInt32(row["ClientID"]) : 0;
                    client.CL_CompID = row["CL_CompID"] != DBNull.Value ? Convert.ToInt32(row["CL_CompID"]) : 0;
                    client.IslogisticEnabled = row["IS_LOGISTIC_ENABLED"] != DBNull.Value ? Convert.ToInt32(row["IS_LOGISTIC_ENABLED"]) : 0;
                    client.IsFarwordEnabled = row["IS_FARWORD_ENABLED"] != DBNull.Value ? Convert.ToInt32(row["IS_FARWORD_ENABLED"]) : 0;
                    client.CL_CompName = row["CL_CompName"] != DBNull.Value ? Convert.ToString(row["CL_CompName"]) : string.Empty;
                    client.CompanyName = row["CompanyName"] != DBNull.Value ? Convert.ToString(row["CompanyName"]) : string.Empty;
                    client.CL_Domain = row["CL_Domain"] != DBNull.Value ? Convert.ToString(row["CL_Domain"]) : string.Empty;
                    client.DateCreated = row["DateCreated"] != DBNull.Value ? Convert.ToDateTime(row["DateCreated"]) : DateTime.Now;
                    client.DateModified = row["DateModified"] != DBNull.Value ? Convert.ToDateTime(row["DateModified"]) : DateTime.Now;
                    client.CreatedBy = row["CreatedBy"] != DBNull.Value ? Convert.ToInt32(row["CreatedBy"]) : 0;
                    client.ModifiedBy = row["ModifiedBy"] != DBNull.Value ? Convert.ToInt32(row["ModifiedBy"]) : 0;
                    client.IsValid = row["IsValid"] != DBNull.Value ? Convert.ToInt32(row["IsValid"]) : 0;
                    client.ClientType = row["ClientType"] != DBNull.Value ? Convert.ToString(row["ClientType"]) : string.Empty;
                    client.ClientStatus = row["ClientStatus"] != DBNull.Value ? Convert.ToString(row["ClientStatus"]) : string.Empty;
                    client.ClientDB = row["ClientDB"] != DBNull.Value ? Convert.ToString(row["ClientDB"]) : string.Empty;
                    //client.IsPREnabled = row["IS_PR"] != DBNull.Value ? Convert.ToInt32(row["IS_PR"]) : 0;
                    //client.ClientSource = row["CLIENT_SOURCE"] != DBNull.Value ? Convert.ToString(row["CLIENT_SOURCE"]) : string.Empty;
                    //client.ClientURL = row["CLIENT_URL"] != DBNull.Value ? Convert.ToString(row["CLIENT_URL"]) : string.Empty;

                }
            }
            catch (Exception ex)
            {
                OpsClients recuirementerror = new OpsClients();
                recuirementerror.ErrorMessage = ex.Message;

            }
            finally
            {
               // cmd.Connection.Close();
            }
            return client;
        }

        public string GetReportsTemplates(string name, string domain, string from, string to, string sessionID)
        {
            string str = string.Empty;
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region Report
            OpsClients client = GetClientDetails(sessionID, domain);
            if (name.ToUpper().Contains("REG_RFQ_REPORT"))
            {
                string query = String.Format("select RD.REQ_ID, RD.REQ_TITLE, RD.DATE_CREATED, " +
                           " (SELECT U_FNAME FROM [user] where U_ID = RD.U_ID) AS Posted_By, RD.CLOSED, " +
                           " (SELECT COUNT(*) FROM auctiondetails ad WHERE REQ_ID = RD.REQ_ID AND IS_DELETED = 0 AND U_ID NOT IN(dbo.GetPricecapSuperUID())) AS VENDORS_INVITED, " +
                           " (SELECT COUNT(*) FROM auctiondetails WHERE REQ_ID = RD.REQ_ID AND IS_DELETED = 0 AND IS_ACCEPTED = 1 and U_ID NOT IN(dbo.GetPricecapSuperUID())) AS VENDORS_PARTICIPATED, " +
                           " (SELECT MIN(VEND_TOTAL_PRICE) FROM auctiondetails WHERE REQ_ID = rd.req_id AND IS_DELETED = 0 and is_accepted = 1 " +
                            " AND U_ID NOT IN(dbo.GetPricecapSuperUID()) AND VEND_TOTAL_PRICE > 0) AS MIN_INITIAL_QUOTATION_PRICE, " +
                            " (RD.SAVINGS + RD.SAVINGS_BEFORE_NEGOTIATION) AS SAVINGS " +
                            " from requirementdetails RD where U_ID in (select u_id from userdata where SUPER_U_ID = (select SUPER_U_ID from userdata where U_ID = {0}))  " +
                            " AND CLOSED != 'DELETED' " +
                            " and CONVERT(date, RD.DATE_CREATED) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}'); "
                            , client.CreatedBy, from, to);
                DataSet ds = sqlHelper.ExecuteQuery(query);


                if (ds != null && ds.Tables.Count > 0)
                {


                    List<Requirement> requirement = new List<Requirement>();
                    ExcelPackage ExcelPkg = new ExcelPackage();
                    ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("TRANSACTION DATA");

                    int lineNumber = 1;

                    wsSheet1.Cells["A" + lineNumber].Value = "RFQ TRANSACTION REPORT";
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Merge = true;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Size = 16;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));

                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Color.SetColor(Color.White);
                    lineNumber++;
                    lineNumber++;

                    wsSheet1.Cells["A" + lineNumber].Value = "Req ID";
                    wsSheet1.Cells["B" + lineNumber].Value = "Req Title Type";
                    wsSheet1.Cells["C" + lineNumber].Value = "Posted Date";
                    wsSheet1.Cells["D" + lineNumber].Value = "Posted By";
                    wsSheet1.Cells["E" + lineNumber].Value = "Status";
                    wsSheet1.Cells["F" + lineNumber].Value = "Invited Vendors";
                    wsSheet1.Cells["G" + lineNumber].Value = "Participated Vendors";
                    wsSheet1.Cells["H" + lineNumber].Value = "Min. Init Quotation Price";
                    wsSheet1.Cells["I" + lineNumber].Value = "Savings";




                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Bold = true;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Color.SetColor(Color.White);

                    lineNumber++;

                    wsSheet1.Cells["A:I"].AutoFitColumns();
                    wsSheet1.Protection.IsProtected = false;
                    wsSheet1.Protection.AllowSelectLockedCells = false;


                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        var postedBy = "";
                        int vendorsInvited = 0;
                        int vendorsParticipated = 0;
                        double minInitQuotPrice = 0;
                        Requirement req = new Requirement();

                        req.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        req.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        req.PostedOn = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                        postedBy = row["Posted_By"] != DBNull.Value ? Convert.ToString(row["Posted_By"]) : string.Empty;
                        req.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        vendorsInvited = row["VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_INVITED"]) : 0;
                        vendorsParticipated = row["VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_PARTICIPATED"]) : 0;
                        minInitQuotPrice = row["MIN_INITIAL_QUOTATION_PRICE"] != DBNull.Value ? Convert.ToDouble(row["MIN_INITIAL_QUOTATION_PRICE"]) : 0;
                        req.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;

                        wsSheet1.Cells["A" + lineNumber].Value = req.RequirementID;
                        wsSheet1.Cells["B" + lineNumber].Value = req.Title;
                        wsSheet1.Cells["C" + lineNumber].Value = Convert.ToString(req.PostedOn);
                        wsSheet1.Cells["D" + lineNumber].Value = postedBy;
                        wsSheet1.Cells["E" + lineNumber].Value = req.Status;
                        wsSheet1.Cells["F" + lineNumber].Value = vendorsInvited;
                        wsSheet1.Cells["G" + lineNumber].Value = vendorsParticipated;
                        wsSheet1.Cells["H" + lineNumber].Value = minInitQuotPrice;
                        wsSheet1.Cells["I" + lineNumber].Value = req.Savings;


                        wsSheet1.Cells.AutoFitColumns();

                        lineNumber++;
                    }


                    ExcelPkg.SaveAs(ms);
                    if (ms != null)
                    {
                        contents = ms.ToArray();
                    }

                }

            }

            if (name.ToUpper().Contains("LOGISTICS_RFQ_DATA"))
            {
                string query = String.Format("select RD.REQ_ID, RD.REQ_TITLE, RD.DATE_CREATED,  " +
                          " (SELECT U_FNAME FROM [user] where U_ID = RD.U_ID) AS Posted_By, RD.CLOSED, " +
                           " (SELECT COUNT(*) FROM logistic_auctiondetails ad WHERE REQ_ID = RD.REQ_ID AND IS_DELETED = 0 AND U_ID NOT IN(dbo.GetPricecapSuperUID())) AS VENDORS_INVITED, " +
                           " (SELECT COUNT(*) FROM logistic_auctiondetails WHERE REQ_ID = RD.REQ_ID AND IS_DELETED = 0 and VEND_RUN_PRICE IS NOT NULL and U_ID NOT IN(dbo.GetPricecapSuperUID())) AS VENDORS_PARTICIPATED, " +
                            " (SELECT MIN(VEND_TOTAL_PRICE) FROM logistic_auctiondetails WHERE REQ_ID = rd.req_id AND IS_DELETED = 0 AND U_ID NOT IN(dbo.GetPricecapSuperUID()) AND VEND_TOTAL_PRICE > 0) AS MIN_INITIAL_QUOTATION_PRICE, " +
                          " (RD.SAVINGS + RD.SAVINGS_BEFORE_NEGOTIATION) AS SAVINGS " +
                          " from logistic_requirementdetails RD where U_ID in (select u_id from userdata where SUPER_U_ID = (select SUPER_U_ID from userdata where U_ID = {0}))  " +
                          " AND CLOSED != 'DELETED' " +
                          " and CONVERT(date, RD.DATE_CREATED) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}'); "
                        , client.CreatedBy, from, to);
                DataSet ds = sqlHelper.ExecuteQuery(query);

                if (ds != null && ds.Tables.Count > 0)
                {
                    List<Requirement> requirement = new List<Requirement>();
                    ExcelPackage ExcelPkg = new ExcelPackage();
                    ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("LOGISTICS TRANSACTION DATA");
                    int lineNumber = 1;

                    wsSheet1.Cells["A" + lineNumber].Value = "CONSOLIDATED REPORT";
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Merge = true;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Size = 16;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Color.SetColor(Color.White);
                    lineNumber++;
                    lineNumber++;

                    wsSheet1.Cells["A" + lineNumber].Value = "Req ID";
                    wsSheet1.Cells["B" + lineNumber].Value = "Req Title Type";
                    wsSheet1.Cells["C" + lineNumber].Value = "Posted Date";
                    wsSheet1.Cells["D" + lineNumber].Value = "Posted By";
                    wsSheet1.Cells["E" + lineNumber].Value = "Status";
                    wsSheet1.Cells["F" + lineNumber].Value = "Invited Vendors";
                    wsSheet1.Cells["G" + lineNumber].Value = "Participated Vendors";
                    wsSheet1.Cells["H" + lineNumber].Value = "Min. Init Quotation Price";
                    wsSheet1.Cells["I" + lineNumber].Value = "Savings";

                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Bold = true;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Color.SetColor(Color.White);

                    lineNumber++;

                    wsSheet1.Cells["A:I"].AutoFitColumns();
                    wsSheet1.Protection.IsProtected = false;
                    wsSheet1.Protection.AllowSelectLockedCells = false;

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        var postedBy = "";
                        int vendorsInvited = 0;
                        int vendorsParticipated = 0;
                        double minInitQuotPrice = 0;
                        Requirement req = new Requirement();

                        req.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        req.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        req.PostedOn = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                        postedBy = row["Posted_By"] != DBNull.Value ? Convert.ToString(row["Posted_By"]) : string.Empty;
                        req.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        vendorsInvited = row["VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_INVITED"]) : 0;
                        vendorsParticipated = row["VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_PARTICIPATED"]) : 0;
                        minInitQuotPrice = row["MIN_INITIAL_QUOTATION_PRICE"] != DBNull.Value ? Convert.ToDouble(row["MIN_INITIAL_QUOTATION_PRICE"]) : 0;
                        req.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;

                        wsSheet1.Cells["A" + lineNumber].Value = req.RequirementID;
                        wsSheet1.Cells["B" + lineNumber].Value = req.Title;
                        wsSheet1.Cells["C" + lineNumber].Value = Convert.ToString(req.PostedOn);
                        wsSheet1.Cells["D" + lineNumber].Value = postedBy;
                        wsSheet1.Cells["E" + lineNumber].Value = req.Status;
                        wsSheet1.Cells["F" + lineNumber].Value = vendorsInvited;
                        wsSheet1.Cells["G" + lineNumber].Value = vendorsParticipated;
                        wsSheet1.Cells["H" + lineNumber].Value = minInitQuotPrice;
                        wsSheet1.Cells["I" + lineNumber].Value = req.Savings;


                        wsSheet1.Cells.AutoFitColumns();

                        lineNumber++;
                    }
                    ExcelPkg.SaveAs(ms);
                    if (ms != null)
                    {
                        contents = ms.ToArray();
                    }
                }
            }

            if (name.ToUpper().Contains("SUB_USERS"))
            {
                string query = String.Format("select u.*,(SELECT CONCAT(U_FNAME, ' ',U_LNAME) FROM [user] where U_ID = u.CREATED_BY) AS CREATED_BY_NAME from [user] u " +
                                    " inner join userroles ur on ur.u_id = u.u_id " +
                                    " inner join userdata ud on ud.U_ID = u.U_ID " +
                                    " where CONVERT(date, u.DATE_CREATED) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}') " +
                                    " and ur.U_TYPE = 'CUSTOMER' " +
                                    " and ud.SUPER_U_ID = {0} " +
                                    " group by u.U_ID,u.U_GENDER,u.U_FNAME,u.U_LNAME,u.U_EMAIL,u.U_PHONE,u.U_ALTPHONE,u.U_BIRTHDATE,u.DATE_CREATED,u.DATE_MODIFIED,u.CREATED_BY,u.MODIFIED_BY,u.U_ALTEMAIL, u.U_ALTPHONE; "
                            , client.CreatedBy, from, to);
                DataSet ds = sqlHelper.ExecuteQuery(query);

                if (ds != null && ds.Tables.Count > 0)
                {
                    ExcelPackage ExcelPkg = new ExcelPackage();
                    ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("SUB USERS");

                    int lineNumber = 1;

                    wsSheet1.Cells["A" + lineNumber].Value = "SUB USERS";
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Merge = true;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Size = 16;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));

                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Color.SetColor(Color.White);
                    lineNumber++;
                    lineNumber++;

                    wsSheet1.Cells["A" + lineNumber].Value = "User ID";
                    wsSheet1.Cells["B" + lineNumber].Value = "User";
                    wsSheet1.Cells["C" + lineNumber].Value = "E-Mail";
                    wsSheet1.Cells["D" + lineNumber].Value = "Phone";
                    wsSheet1.Cells["E" + lineNumber].Value = "Created On";
                    wsSheet1.Cells["F" + lineNumber].Value = "Created By";

                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Bold = true;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Color.SetColor(Color.White);

                    lineNumber++;
                    wsSheet1.Cells["A:F"].AutoFitColumns();
                    wsSheet1.Protection.IsProtected = false;
                    wsSheet1.Protection.AllowSelectLockedCells = false;

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        var FNAME = "";
                        var LNAME = "";
                        FNAME = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        LNAME = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;

                        wsSheet1.Cells["A" + lineNumber].Value = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        wsSheet1.Cells["B" + lineNumber].Value = FNAME + " " + LNAME;
                        wsSheet1.Cells["C" + lineNumber].Value = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        wsSheet1.Cells["D" + lineNumber].Value = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        wsSheet1.Cells["E" + lineNumber].Value = Convert.ToString(row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now);
                        wsSheet1.Cells["F" + lineNumber].Value = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;

                        wsSheet1.Cells.AutoFitColumns();

                        lineNumber++;
                    }
                    ExcelPkg.SaveAs(ms);
                    if (ms != null)
                    {
                        contents = ms.ToArray();
                    }
                }
            }

            if (name.ToUpper().Contains("VENDORS_REGISTERED"))
            {
                string query = String.Format("select u.*,(SELECT CONCAT(U_FNAME, ' ',U_LNAME) FROM [user] where U_ID = v.CREATED_BY) AS CREATED_BY_NAME  from [user] u " +
                                  "  inner join userroles ur on ur.u_id = u.u_id " +
                                   " inner join vendors v on v.U_ID = u.U_ID " +
                                   " where CONVERT(date, u.DATE_CREATED) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}') " +
                                   " and v.COMP_ID = dbo.getcompanyid({0}) and v.IS_PRIMARY = 1 " +
                                   " group by u.U_ID,u.U_GENDER,u.U_FNAME,u.U_LNAME,u.U_EMAIL,u.U_PHONE,u.U_ALTPHONE,u.U_BIRTHDATE,u.DATE_CREATED,u.DATE_MODIFIED,u.CREATED_BY,u.MODIFIED_BY,u.U_ALTEMAIL, u.U_ALTPHONE, v.CREATED_BY; "
                            , client.CreatedBy, from, to);
                DataSet ds = sqlHelper.ExecuteQuery(query);

                if (ds != null && ds.Tables.Count > 0)
                {
                    ExcelPackage ExcelPkg = new ExcelPackage();
                    ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("VENDORS");

                    int lineNumber = 1;

                    wsSheet1.Cells["A" + lineNumber].Value = "VENDORS";
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Merge = true;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Size = 16;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));

                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Color.SetColor(Color.White);
                    lineNumber++;
                    lineNumber++;

                    wsSheet1.Cells["A" + lineNumber].Value = "User ID";
                    wsSheet1.Cells["B" + lineNumber].Value = "User";
                    wsSheet1.Cells["C" + lineNumber].Value = "E-Mail";
                    wsSheet1.Cells["D" + lineNumber].Value = "Phone";
                    wsSheet1.Cells["E" + lineNumber].Value = "Created On";
                    wsSheet1.Cells["F" + lineNumber].Value = "Created By";

                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Bold = true;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                    wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Color.SetColor(Color.White);

                    lineNumber++;
                    wsSheet1.Cells["A:F"].AutoFitColumns();
                    wsSheet1.Protection.IsProtected = false;
                    wsSheet1.Protection.AllowSelectLockedCells = false;

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        var FNAME = "";
                        var LNAME = "";
                        FNAME = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        LNAME = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;

                        wsSheet1.Cells["A" + lineNumber].Value = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        wsSheet1.Cells["B" + lineNumber].Value = FNAME + " " + LNAME;
                        wsSheet1.Cells["C" + lineNumber].Value = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        wsSheet1.Cells["D" + lineNumber].Value = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        wsSheet1.Cells["E" + lineNumber].Value = Convert.ToString(row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now);
                        wsSheet1.Cells["F" + lineNumber].Value = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;

                        wsSheet1.Cells.AutoFitColumns();

                        lineNumber++;
                    }
                    ExcelPkg.SaveAs(ms);
                    if (ms != null)
                    {
                        contents = ms.ToArray();
                    }
                }
            }

            if (name.ToUpper().Contains("COMPANY_DEPT"))
            {
                string query = String.Format(" select * from companydepartments where COMP_ID = GetCompanyID({0}); "
                            , client.CreatedBy, from, to);
                DataSet ds = sqlHelper.ExecuteQuery(query);


                if (ds != null && ds.Tables.Count > 0)
                {
                    ExcelPackage ExcelPkg = new ExcelPackage();
                    ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("COMPANY DEPARTMENTS");

                    int lineNumber = 1;

                    wsSheet1.Cells["A" + lineNumber].Value = "COMPANY DEPARTMENTS";
                    wsSheet1.Cells["A" + lineNumber + ":B" + lineNumber].Merge = true;
                    wsSheet1.Cells["A" + lineNumber + ":B" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":B" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":B" + lineNumber].Style.Font.Size = 16;
                    wsSheet1.Cells["A" + lineNumber + ":B" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":B" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));

                    wsSheet1.Cells["A" + lineNumber + ":B" + lineNumber].Style.Font.Color.SetColor(Color.White);
                    lineNumber++;
                    lineNumber++;

                    wsSheet1.Cells["A" + lineNumber].Value = "Department Code";
                    wsSheet1.Cells["B" + lineNumber].Value = "Description";


                    wsSheet1.Cells["A" + lineNumber + ":B" + lineNumber].Style.Font.Bold = true;
                    wsSheet1.Cells["A" + lineNumber + ":B" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":B" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                    wsSheet1.Cells["A" + lineNumber + ":B" + lineNumber].Style.Font.Color.SetColor(Color.White);

                    lineNumber++;
                    wsSheet1.Cells["A:B"].AutoFitColumns();
                    wsSheet1.Protection.IsProtected = false;
                    wsSheet1.Protection.AllowSelectLockedCells = false;

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        wsSheet1.Cells["A" + lineNumber].Value = row["DEPT_CODE"] != DBNull.Value ? Convert.ToString(row["DEPT_CODE"]) : string.Empty;
                        wsSheet1.Cells["B" + lineNumber].Value = row["DEPT_DESC"] != DBNull.Value ? Convert.ToString(row["DEPT_DESC"]) : string.Empty;

                        wsSheet1.Cells.AutoFitColumns();

                        lineNumber++;
                    }
                    ExcelPkg.SaveAs(ms);
                    if (ms != null)
                    {
                        contents = ms.ToArray();
                    }
                }
            }

            if (name.ToUpper().Contains("ACTIVE_PR_USERS"))
            {
                string query = String.Format("select * from (select u.*,(SELECT CONCAT(U_FNAME, ' ',U_LNAME) FROM [user] where U_ID = u.CREATED_BY) AS CREATED_BY_NAME from [user] u " +
                                " inner join userroles ur on ur.u_id = u.u_id " +
                                 " inner join userdata ud on ud.U_ID = u.U_ID " +
                               " inner join(select * from requirementdetails where CONVERT(date, DATE_CREATED) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}')) rd on rd.U_ID = u.u_id " +
                               " left join(select * from pr_details where CONVERT(date, CREATED_DATE) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}')) pr on pr.CREATED_BY = u.u_id " +
                               " where ur.U_TYPE = 'CUSTOMER' " +
                               " and ud.SUPER_U_ID = 6316 " +
                               " union all " +
                               " select u.*,(SELECT CONCAT(U_FNAME, ' ',U_LNAME) FROM [user] where U_ID = u.CREATED_BY) AS CREATED_BY_NAME from [user] u " +
                               " inner join userroles ur on ur.u_id = u.u_id " +
                                "     inner join userdata ud on ud.U_ID = u.U_ID " +
                                 "    inner join (select *from pr_details where CONVERT(date, CREATED_DATE) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}'))   pr on pr.CREATED_BY = u.u_id " +
                                " where ur.U_TYPE = 'CUSTOMER' " +
                               " and ud.SUPER_U_ID = {0}) temp group by U_ID; "
                     , client.CreatedBy, from, to);
                DataSet ds = sqlHelper.ExecuteQuery(query);

                if (ds != null && ds.Tables.Count > 0)
                {
                    ExcelPackage ExcelPkg = new ExcelPackage();
                    ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("ACTIVE PR USERS");

                    int lineNumber = 1;

                    wsSheet1.Cells["A" + lineNumber].Value = "ACTIVE PR USERS";
                    wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Merge = true;
                    wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Font.Size = 16;
                    wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));

                    wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Font.Color.SetColor(Color.White);
                    lineNumber++;
                    lineNumber++;

                    wsSheet1.Cells["A" + lineNumber].Value = "User ID";
                    wsSheet1.Cells["B" + lineNumber].Value = "User";
                    wsSheet1.Cells["C" + lineNumber].Value = "E-Mail";
                    wsSheet1.Cells["D" + lineNumber].Value = "Phone";
                    wsSheet1.Cells["E" + lineNumber].Value = "Created On";
                    wsSheet1.Cells["F" + lineNumber].Value = "Created By";
                    wsSheet1.Cells["G" + lineNumber].Value = "Country Code";

                    wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Font.Bold = true;
                    wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                    wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Font.Color.SetColor(Color.White);

                    lineNumber++;
                    wsSheet1.Cells["A:G"].AutoFitColumns();
                    wsSheet1.Protection.IsProtected = false;
                    wsSheet1.Protection.AllowSelectLockedCells = false;

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        var FNAME = "";
                        var LNAME = "";
                        FNAME = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        LNAME = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;

                        wsSheet1.Cells["A" + lineNumber].Value = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        wsSheet1.Cells["B" + lineNumber].Value = FNAME + " " + LNAME;
                        wsSheet1.Cells["C" + lineNumber].Value = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        wsSheet1.Cells["D" + lineNumber].Value = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        wsSheet1.Cells["E" + lineNumber].Value = Convert.ToString(row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now);
                        wsSheet1.Cells["F" + lineNumber].Value = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;
                        wsSheet1.Cells["G" + lineNumber].Value = row["COUNTRY_CODE"] != DBNull.Value ? Convert.ToString(row["COUNTRY_CODE"]) : string.Empty;

                        wsSheet1.Cells.AutoFitColumns();

                        lineNumber++;
                    }
                    ExcelPkg.SaveAs(ms);
                    if (ms != null)
                    {
                        contents = ms.ToArray();
                    }
                }
            }

            if (name.ToUpper().Contains("ACTIVE_USERS"))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    using (cmd.Connection = new MySqlConnection(GetConnectionString(domain)))
                    {
                        cmd.CommandText = String.Format("select u.*,(SELECT CONCAT(U_FNAME, ' ',U_LNAME) FROM [user] where U_ID = u.CREATED_BY) AS CREATED_BY_NAME from [user] u " +
                                       " inner join userroles ur on ur.u_id = u.u_id " +
                                       " inner join userdata ud on ud.U_ID = u.U_ID " +
                                       " inner join(select * from requirementdetails where CONVERT(date, DATE_CREATED) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}')) rd on rd.U_ID = u.u_id " +
                                       " where ur.U_TYPE = 'CUSTOMER' " +
                                       " and ud.SUPER_U_ID = {0} " +
                                       " group by u.U_ID,u.U_GENDER,u.U_FNAME,u.U_LNAME,u.U_EMAIL,u.U_PHONE,u.U_ALTPHONE,u.U_BIRTHDATE,u.DATE_CREATED,u.DATE_MODIFIED,u.CREATED_BY,u.MODIFIED_BY,u.U_ALTEMAIL, u.U_ALTPHONE; "
                         , client.CreatedBy, from, to);
                        cmd.CommandType = CommandType.Text;
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();


                        if (ds != null && ds.Tables.Count > 0)
                        {
                            ExcelPackage ExcelPkg = new ExcelPackage();
                            ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("ACTIVE USERS");

                            int lineNumber = 1;

                            wsSheet1.Cells["A" + lineNumber].Value = "ACTIVE USERS";
                            wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Merge = true;
                            wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Size = 16;
                            wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));

                            wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Color.SetColor(Color.White);
                            lineNumber++;
                            lineNumber++;

                            wsSheet1.Cells["A" + lineNumber].Value = "User ID";
                            wsSheet1.Cells["B" + lineNumber].Value = "User";
                            wsSheet1.Cells["C" + lineNumber].Value = "E-Mail";
                            wsSheet1.Cells["D" + lineNumber].Value = "Phone";
                            wsSheet1.Cells["E" + lineNumber].Value = "Created On";
                            wsSheet1.Cells["F" + lineNumber].Value = "Created By";

                            wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Bold = true;
                            wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                            wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Color.SetColor(Color.White);

                            lineNumber++;
                            wsSheet1.Cells["A:F"].AutoFitColumns();
                            wsSheet1.Protection.IsProtected = false;
                            wsSheet1.Protection.AllowSelectLockedCells = false;

                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                var FNAME = "";
                                var LNAME = "";
                                FNAME = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                                LNAME = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;

                                wsSheet1.Cells["A" + lineNumber].Value = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                                wsSheet1.Cells["B" + lineNumber].Value = FNAME + " " + LNAME;
                                wsSheet1.Cells["C" + lineNumber].Value = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                                wsSheet1.Cells["D" + lineNumber].Value = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                                wsSheet1.Cells["E" + lineNumber].Value = Convert.ToString(row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now);
                                wsSheet1.Cells["F" + lineNumber].Value = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;

                                wsSheet1.Cells.AutoFitColumns();

                                lineNumber++;
                            }
                            ExcelPkg.SaveAs(ms);
                            if (ms != null)
                            {
                                contents = ms.ToArray();
                            }
                        }
                    }
                }
            }

            if (name.ToUpper().Contains("SUNPHARMA_USERS"))
            {
                string query = String.Format("select u.*,(SELECT CONCAT(U_FNAME, ' ',U_LNAME) FROM [user] where U_ID = u.CREATED_BY) AS CREATED_BY_NAME from [user] u " +
                     " inner join userroles ur on ur.u_id = u.u_id inner join userdata ud on ud.U_ID = u.U_ID " +
                     " inner join(select * from requirementdetails where CONVERT(date, DATE_CREATED) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}')) rd on rd.U_ID = u.u_id " +
                     " where ur.U_TYPE = 'CUSTOMER' and ud.SUPER_U_ID = {0} group by U_ID " +
                     " union " +
                     " select u.*, (SELECT CONCAT(U_FNAME, ' ', U_LNAME) FROM [user] where U_ID = u.CREATED_BY) AS CREATED_BY_NAME from [user] u " +
                     " inner join userroles ur on ur.u_id = u.u_id inner join userdata ud on ud.U_ID = u.U_ID " +
                     " inner join (select *from sap_openpo_comments where CONVERT(date, CREATED) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}')) oc on oc.CREATED_BY = u.u_id " +
                     " where ur.U_TYPE = 'CUSTOMER' and ud.SUPER_U_ID = {0} group by U_ID " +
                     " union " +
                     " select u.*,(SELECT CONCAT(U_FNAME, ' ', U_LNAME) FROM [user] where U_ID = u.CREATED_BY) AS CREATED_BY_NAME from [user] u " +
                     " inner join userroles ur on ur.u_id = u.u_id inner join userdata ud on ud.U_ID = u.U_ID  " +
                     " inner join (select *from fwd_requirementdetails where CONVERT(date, DATE_CREATED) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}')) rd on rd.U_ID = u.u_id " +
                     " where ur.U_TYPE = 'CUSTOMER' and ud.SUPER_U_ID = {0} group by U_ID; "
                    , client.CreatedBy, from, to);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0)
                {
                    ExcelPackage ExcelPkg = new ExcelPackage();
                    ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("SUB USERS");

                    int lineNumber = 1;

                    wsSheet1.Cells["A" + lineNumber].Value = "SUB USERS";
                    wsSheet1.Cells["A" + lineNumber + ":H" + lineNumber].Merge = true;
                    wsSheet1.Cells["A" + lineNumber + ":H" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":H" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":H" + lineNumber].Style.Font.Size = 16;
                    wsSheet1.Cells["A" + lineNumber + ":H" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":H" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));

                    wsSheet1.Cells["A" + lineNumber + ":H" + lineNumber].Style.Font.Color.SetColor(Color.White);
                    lineNumber++;
                    lineNumber++;

                    wsSheet1.Cells["A" + lineNumber].Value = "User ID";
                    wsSheet1.Cells["B" + lineNumber].Value = "User";
                    wsSheet1.Cells["C" + lineNumber].Value = "E-Mail";
                    wsSheet1.Cells["D" + lineNumber].Value = "Phone";
                    wsSheet1.Cells["E" + lineNumber].Value = "Created On";
                    wsSheet1.Cells["F" + lineNumber].Value = "Created By";
                    wsSheet1.Cells["G" + lineNumber].Value = "Alt.Phone";
                    wsSheet1.Cells["H" + lineNumber].Value = "Alt.Email";

                    wsSheet1.Cells["A" + lineNumber + ":H" + lineNumber].Style.Font.Bold = true;
                    wsSheet1.Cells["A" + lineNumber + ":H" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":H" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                    wsSheet1.Cells["A" + lineNumber + ":H" + lineNumber].Style.Font.Color.SetColor(Color.White);

                    lineNumber++;
                    wsSheet1.Cells["A:H"].AutoFitColumns();
                    wsSheet1.Protection.IsProtected = false;
                    wsSheet1.Protection.AllowSelectLockedCells = false;

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        var FNAME = "";
                        var LNAME = "";
                        FNAME = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        LNAME = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;

                        wsSheet1.Cells["A" + lineNumber].Value = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        wsSheet1.Cells["B" + lineNumber].Value = FNAME + " " + LNAME;
                        wsSheet1.Cells["C" + lineNumber].Value = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        wsSheet1.Cells["D" + lineNumber].Value = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        wsSheet1.Cells["E" + lineNumber].Value = Convert.ToString(row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now);
                        wsSheet1.Cells["F" + lineNumber].Value = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;
                        wsSheet1.Cells["G" + lineNumber].Value = row["U_ALTPHONE"] != DBNull.Value ? Convert.ToString(row["U_ALTPHONE"]) : string.Empty;
                        wsSheet1.Cells["H" + lineNumber].Value = row["U_ALTEMAIL"] != DBNull.Value ? Convert.ToString(row["U_ALTEMAIL"]) : string.Empty;

                        wsSheet1.Cells.AutoFitColumns();

                        lineNumber++;
                    }
                    ExcelPkg.SaveAs(ms);
                    if (ms != null)
                    {
                        contents = ms.ToArray();
                    }
                }
            }

            if (name.ToUpper().Contains("REQUIREMENTS_COUNT"))
            {
                string query = String.Format("select RD.REQ_ID, RD.REQ_TITLE, RD.DATE_CREATED, " +
                              " (SELECT U_FNAME FROM [user] where U_ID = RD.U_ID) AS Posted_By, RD.CLOSED, " +
                              " (SELECT COUNT(*) FROM auctiondetails WHERE REQ_ID = RD.REQ_ID AND IS_DELETED = 0 AND U_ID NOT IN(dbo.GetPricecapSuperUID())) AS VENDORS_INVITED, " +
                              " (SELECT COUNT(*) FROM auctiondetails WHERE REQ_ID = RD.REQ_ID AND IS_DELETED = 0 AND U_ID NOT IN(dbo.GetPricecapSuperUID()) AND IS_QUOTATION_REJECTED = 0) AS VENDORS_PARTICIPATED, " +
                              " (SELECT MIN(VEND_TOTAL_PRICE) FROM auctiondetails WHERE REQ_ID = RD.REQ_ID AND IS_DELETED = 0 AND U_ID NOT IN(dbo.GetPricecapSuperUID()) AND IS_QUOTATION_REJECTED = 0 AND VEND_TOTAL_PRICE > 0) AS MIN_INITIAL_QUOTATION_PRICE, " +
                              " (RD.SAVINGS + RD.SAVINGS_BEFORE_NEGOTIATION) AS SAVINGS " +
                              " from requirementdetails RD where U_ID in (select u_id from userdata where SUPER_U_ID = (select SUPER_U_ID from userdata where U_ID = {0})) AND CLOSED != 'DELETED' " +
                              " union " +
                              " select RD.REQ_ID, RD.REQ_TITLE, RD.DATE_CREATED, " +
                              " (SELECT U_FNAME FROM [user] where U_ID = RD.U_ID) AS Posted_By, RD.CLOSED, " +
                              " (SELECT COUNT(*) FROM fwd_auctiondetails WHERE REQ_ID = RD.REQ_ID AND IS_DELETED = 0 AND U_ID NOT IN(dbo.GetPricecapSuperUID())) AS VENDORS_INVITED, " +
                              " (SELECT COUNT(*) FROM fwd_auctiondetails WHERE REQ_ID = RD.REQ_ID AND IS_DELETED = 0 AND U_ID NOT IN(dbo.GetPricecapSuperUID()) AND IS_QUOTATION_REJECTED = 0) AS VENDORS_PARTICIPATED, " +
                              " (SELECT MIN(VEND_TOTAL_PRICE) FROM fwd_auctiondetails WHERE REQ_ID = RD.REQ_ID AND IS_DELETED = 0 AND U_ID NOT IN(dbo.GetPricecapSuperUID()) AND IS_QUOTATION_REJECTED = 0 AND VEND_TOTAL_PRICE > 0) AS MIN_INITIAL_QUOTATION_PRICE, " +
                              " (RD.SAVINGS + RD.SAVINGS_BEFORE_NEGOTIATION) AS SAVINGS " +
                              " from fwd_requirementdetails RD where U_ID in (select u_id from userdata where SUPER_U_ID = (select SUPER_U_ID from userdata where U_ID = {0})) AND CLOSED != 'DELETED'; "
                           , client.CreatedBy, from, to);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0)
                {


                    List<Requirement> requirement = new List<Requirement>();
                    ExcelPackage ExcelPkg = new ExcelPackage();
                    ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("REQUIREMENTS COUNT");

                    int lineNumber = 1;

                    wsSheet1.Cells["A" + lineNumber].Value = "REQUIREMENTS COUNT";
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Merge = true;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Size = 16;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));

                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Color.SetColor(Color.White);
                    lineNumber++;
                    lineNumber++;

                    wsSheet1.Cells["A" + lineNumber].Value = "Req ID";
                    wsSheet1.Cells["B" + lineNumber].Value = "Req Title Type";
                    wsSheet1.Cells["C" + lineNumber].Value = "Posted Date";
                    wsSheet1.Cells["D" + lineNumber].Value = "Posted By";
                    wsSheet1.Cells["E" + lineNumber].Value = "Status";
                    wsSheet1.Cells["F" + lineNumber].Value = "Invited Vendors";
                    wsSheet1.Cells["G" + lineNumber].Value = "Participated Vendors";
                    wsSheet1.Cells["H" + lineNumber].Value = "Min. Init Quotation Price";
                    wsSheet1.Cells["I" + lineNumber].Value = "Savings";




                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Bold = true;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                    wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Color.SetColor(Color.White);

                    lineNumber++;

                    wsSheet1.Cells["A:I"].AutoFitColumns();
                    wsSheet1.Protection.IsProtected = false;
                    wsSheet1.Protection.AllowSelectLockedCells = false;


                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        var postedBy = "";
                        int vendorsInvited = 0;
                        int vendorsParticipated = 0;
                        double minInitQuotPrice = 0;
                        Requirement req = new Requirement();

                        req.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        req.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        req.PostedOn = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                        postedBy = row["Posted_By"] != DBNull.Value ? Convert.ToString(row["Posted_By"]) : string.Empty;
                        req.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        vendorsInvited = row["VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_INVITED"]) : 0;
                        vendorsParticipated = row["VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_PARTICIPATED"]) : 0;
                        minInitQuotPrice = row["MIN_INITIAL_QUOTATION_PRICE"] != DBNull.Value ? Convert.ToDouble(row["MIN_INITIAL_QUOTATION_PRICE"]) : 0;
                        req.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;

                        wsSheet1.Cells["A" + lineNumber].Value = req.RequirementID;
                        wsSheet1.Cells["B" + lineNumber].Value = req.Title;
                        wsSheet1.Cells["C" + lineNumber].Value = Convert.ToString(req.PostedOn);
                        wsSheet1.Cells["D" + lineNumber].Value = postedBy;
                        wsSheet1.Cells["E" + lineNumber].Value = req.Status;
                        wsSheet1.Cells["F" + lineNumber].Value = vendorsInvited;
                        wsSheet1.Cells["G" + lineNumber].Value = vendorsParticipated;
                        wsSheet1.Cells["H" + lineNumber].Value = minInitQuotPrice;
                        wsSheet1.Cells["I" + lineNumber].Value = req.Savings;


                        wsSheet1.Cells.AutoFitColumns();

                        lineNumber++;
                    }


                    ExcelPkg.SaveAs(ms);
                    if (ms != null)
                    {
                        contents = ms.ToArray();
                    }

                }

            }

            #endregion Report
            str = Convert.ToBase64String(ms.ToArray());
            return str;
        }

        public Response SendPassword(int userID, string domain, string phone, string email)
        {
            Response response = new Response();
            PRMServices prm = new PRMServices();

            if (!string.IsNullOrEmpty(phone) && phone.Length == 10
                && (phone.StartsWith("6") || phone.StartsWith("7") || phone.StartsWith("8") || phone.StartsWith("9")))
            {

            }
            else
            {
                // return response;
            }

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                OpsRequirementVendors user = GetVendorDetails(userID, domain, null);
                var webSite = ConfigurationManager.AppSettings[domain].ToString();
                //var message = ConfigurationManager.AppSettings["sendPasswordEmail"].ToString();
                //message = message.Replace("@BR@", Environment.NewLine);
                //message = message.Replace("FN", user.VendorName);
                //message = message.Replace("$$$$", user.VendorLoginPass);
                //message = message.Replace("####", webSite);

                string body = prm.GenerateEmailBody("sendpasswordemail");
                body = String.Format(body, user.VendorName, user.VendorLoginPass, webSite);
                string body1 = prm.GenerateEmailBody("sendpasswordsms");
                body1 = String.Format(body, user.VendorName, user.VendorLoginPass, webSite);
                prm.SendEmail(email, "Password Reminder", body, 0, 0, "", null, null).ConfigureAwait(false);
                prm.SendSMS(string.Empty, phone, body1, 0, 0, "", null).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public List<OpsRequirementVendors> GetVendorNeedTraining(string sessionID, string domain, string from, string to, int ClientID, int auctionType)
        {
            List<OpsRequirementVendors> vendorNeedTraning = new List<OpsRequirementVendors>();
            try
            {
                if (auctionType == 0)
                {
                    // auctionType = Reverse   RD.BIDDING_TYPE,
                    string query = GetOpsQueries("GetVendorNeedTraining");
                    query = String.Format(query, from, to, ClientID);
                    DataSet ds = sqlHelper.ExecuteQuery(query);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            OpsRequirementVendors vendorTraining = new OpsRequirementVendors();

                            vendorTraining.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                            vendorTraining.REQ_TITLE = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                            vendorTraining.TranierName = row["TRAINER_NAME"] != DBNull.Value ? Convert.ToString(row["TRAINER_NAME"]) : string.Empty;
                            vendorTraining.COMPANY_NAME = row["comp_name"] != DBNull.Value ? Convert.ToString(row["comp_name"]) : string.Empty;
                            vendorTraining.U_EMAIL = row["u_email"] != DBNull.Value ? Convert.ToString(row["u_email"]) : string.Empty;
                            vendorTraining.U_PHONE = row["u_phone"] != DBNull.Value ? Convert.ToString(row["u_phone"]) : string.Empty;
                            vendorTraining.REQ_POSTED_ON = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.Now;
                            vendorTraining.QUOTATION_FREEZ_TIME = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.Now;
                            vendorTraining.EXP_START_TIME = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.Now;
                            vendorTraining.BIDDING_TYPE = row["BIDDING_TYPE"] != DBNull.Value ? Convert.ToString(row["BIDDING_TYPE"]) : string.Empty;
                            vendorTraining.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                            vendorTraining.TOTAL_QUOTATIONS = row["TOTAL_QUOTATIONS"] != DBNull.Value ? Convert.ToInt32(row["TOTAL_QUOTATIONS"]) : 0;
                            vendorTraining.CLOSED = row["closed"] != DBNull.Value ? Convert.ToString(row["closed"]) : string.Empty;
                            vendorTraining.VendorName = row["VendorName"] != DBNull.Value ? Convert.ToString(row["VendorName"]) : string.Empty;
                            vendorTraining.IsEmailVerified = row["isEmailOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isEmailOTPVerified"]) : 0;
                            vendorTraining.IsOtpVerified = row["isOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isOTPVerified"]) : 0;
                            vendorNeedTraning.Add(vendorTraining);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                OpsRequirementVendors recuirementerror = new OpsRequirementVendors();
                recuirementerror.ErrorMessage = ex.Message;
                vendorNeedTraning.Add(recuirementerror);
            }
            finally
            {
                //cmd.Connection.Close();
            }
            return vendorNeedTraning;
        }

        public OpsReports GetReports(string domain, string from, string to, string sessionID)
        {
            OpsReports reports = new OpsReports();
            OpsClients client = GetClientDetails(sessionID, domain);

            try
            {
                string query = String.Format("" +
                   // REG_RFQ_REPORT
                   "select RD.REQ_ID, RD.REQ_TITLE, RD.DATE_CREATED, " +
                   " (SELECT U_FNAME FROM [user] where U_ID = RD.U_ID) AS Posted_By, RD.CLOSED, " +
                   " (SELECT COUNT(*) FROM auctiondetails ad WHERE REQ_ID = RD.REQ_ID AND IS_DELETED = 0 AND U_ID NOT IN(dbo.GetPricecapSuperUID())) AS VENDORS_INVITED, " +
                   " (SELECT COUNT(*) FROM auctiondetails WHERE REQ_ID = RD.REQ_ID AND IS_DELETED = 0 AND IS_ACCEPTED = 1 and U_ID NOT IN(dbo.GetPricecapSuperUID())) AS VENDORS_PARTICIPATED, " +
                   " (SELECT MIN(VEND_TOTAL_PRICE) FROM auctiondetails WHERE REQ_ID = rd.req_id AND IS_DELETED = 0 and is_accepted = 1 " +
                    " AND U_ID NOT IN(dbo.GetPricecapSuperUID()) AND VEND_TOTAL_PRICE > 0) AS MIN_INITIAL_QUOTATION_PRICE, " +
                    " (RD.SAVINGS + RD.SAVINGS_BEFORE_NEGOTIATION) AS SAVINGS " +
                    " from requirementdetails RD where U_ID in (select u_id from userdata where SUPER_U_ID = (select SUPER_U_ID from userdata where U_ID = {0}))  " +
                    " AND CLOSED != 'DELETED' " +
                    " and CONVERT(DATE, RD.DATE_CREATED) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}')  ; " +
                //and DATE(RD.DATE_CREATED) between DATE('{1}') and DATE('{2}')
                //  SUB_USER
                "select u.*,(SELECT CONCAT(U_FNAME, ' ',U_LNAME) FROM [user] where U_ID = u.CREATED_BY) AS CREATED_BY_NAME from [user] u " +
                            " inner join userroles ur on ur.u_id = u.u_id " +
                            " inner join userdata ud on ud.U_ID = u.U_ID " +
                            " where CONVERT(DATE, u.DATE_CREATED) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}')   " +
                            " and ur.U_TYPE = 'CUSTOMER' " +
                            " and ud.SUPER_U_ID = {0}; " +
                            
                //VENDOR REGISTERED
                "select u.*,(SELECT CONCAT(U_FNAME, ' ',U_LNAME) FROM [user] where U_ID = v.CREATED_BY) AS CREATED_BY_NAME  from [user] u " +
                          "  inner join userroles ur on ur.u_id = u.u_id " +
                           " inner join vendors v on v.U_ID = u.U_ID " +
                           " where CONVERT(DATE, u.DATE_CREATED) BETWEEN CONVERT(DATE, '{1}') and CONVERT(DATE, '{2}') " +
                           " and v.COMP_ID = getcompanyid({0}) and v.IS_PRIMARY = 1; " 
                           
                    , client.CreatedBy, from, to);

                DataSet ds = sqlHelper.ExecuteQuery(query);

                if (ds != null && ds.Tables.Count > 0)
                {
                    List<OpsRequirement> reqList = new List<OpsRequirement>();
                    List<OpsUserDetails> subUserList = new List<OpsUserDetails>();
                    List<OpsUserDetails> vendorsList = new List<OpsUserDetails>();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        OpsRequirement reportsReq = new OpsRequirement();


                        reportsReq.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        reportsReq.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        reportsReq.REQ_POSTED_ON = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                        reportsReq.ReqPostedBy = row["Posted_By"] != DBNull.Value ? Convert.ToString(row["Posted_By"]) : string.Empty;
                        reportsReq.CLOSED = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        reportsReq.VendorsInvited = row["VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_INVITED"]) : 0;
                        reportsReq.VendorsParticipated = row["VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_PARTICIPATED"]) : 0;
                        reportsReq.MinIbitialQuotationPrice = row["MIN_INITIAL_QUOTATION_PRICE"] != DBNull.Value ? Convert.ToDouble(row["MIN_INITIAL_QUOTATION_PRICE"]) : 0;
                        reportsReq.SAVINGS = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                        reqList.Add(reportsReq);
                    }

                    reports.ReqTransactions = reqList;

                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        OpsUserDetails reportsSubUser = new OpsUserDetails();

                        reportsSubUser.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        reportsSubUser.UserName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        reportsSubUser.U_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        reportsSubUser.U_PHONE = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        reportsSubUser.DateCreated = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                        reportsSubUser.CreatedByName = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;
                        subUserList.Add(reportsSubUser);
                    }
                    reports.SubUsersRegistered = subUserList;
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        OpsUserDetails reportsVendors = new OpsUserDetails();

                        reportsVendors.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        reportsVendors.UserName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        reportsVendors.U_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        reportsVendors.U_PHONE = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        reportsVendors.DateCreated = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                        reportsVendors.CreatedByName = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;
                        vendorsList.Add(reportsVendors);
                    }

                    
                    
                    reports.VendorsRegistered = vendorsList;
                }
            }
            catch (Exception ex)
            {
                OpsReports recuirementerror = new OpsReports();
                recuirementerror.ErrorMessage = ex.Message;
            }
            finally
            {
                //cmd.Connection.Close();
            }
            return reports;
        }


        public Response SaveVendorTraining(OpsVendorTraining vendTrain,string domain,int clientID,int isUpdate)
        {
            Response response = new Response();
            try
            {
                //int isValidSession = ValidateSession(requirementSetting.SessionID, null);
                string query = "";
                if (isUpdate == 0)
                {
                    query = GetOpsQueries("SaveVendorTrainingInsert");
                    query = String.Format(query, vendTrain.VendorID, vendTrain.ReqID, vendTrain.TrainedContactName, vendTrain.ContactPhone, vendTrain.ContactEmail,
                        vendTrain.VendorPhone, vendTrain.VendorEmail, vendTrain.TrainerName, vendTrain.TrainerID, vendTrain.TickectNo, vendTrain.Comments, vendTrain.TrainingMode, vendTrain.AgentID, vendTrain.Status);
                    sqlHelper.ExecuteNonQuery_IUD(query);
                }
                if (isUpdate == 1)
                {
                    query = GetOpsQueries("SaveVendorTrainingUpdate");
                    query = string.Format(query, vendTrain.Comments, vendTrain.Status, vendTrain.TrainedContactName, vendTrain.VendorPhone, vendTrain.VendorEmail,
                                    vendTrain.VT_ID, vendTrain.VendorID, vendTrain.ReqID);
                    sqlHelper.ExecuteNonQuery_IUD(query);
                }
               
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }


        public List<OpsVendorTraining> GetVendorTrainingHistory(int vtID, int reqID, int vendorID, string domain)
        {
            List<OpsVendorTraining> details = new List<OpsVendorTraining>();
            try
            {
                string query = "";
                //string query = String.Format(" select * from vendortraining WHERE VENDOR_ID = {2} and RFQ_ID = {1} order by VT_ID desc  ", vtID, reqID, vendorID);
                if (reqID > 0)
                {
                    query = GetOpsQueries("GetVendorTrainingHistoryREQID");
                }
                else
                {
                    query = GetOpsQueries("GetVendorTrainingHistory");
                }

                query = String.Format(query, vtID, reqID, vendorID);
                DataSet ds = sqlHelper.ExecuteQuery(query);


                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        OpsVendorTraining vendorTraining = new OpsVendorTraining();

                        vendorTraining.VT_ID = row["VT_ID"] != DBNull.Value ? Convert.ToInt32(row["VT_ID"]) : 0;
                        vendorTraining.VendorID = row["VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_ID"]) : 0;
                        vendorTraining.ReqID = row["RFQ_ID"] != DBNull.Value ? Convert.ToInt32(row["RFQ_ID"]) : 0;
                        vendorTraining.TrainedContactName = row["TRAINED_CONTACT_NAME"] != DBNull.Value ? Convert.ToString(row["TRAINED_CONTACT_NAME"]) : string.Empty;
                        vendorTraining.ContactPhone = row["CONTACT_PHONE"] != DBNull.Value ? Convert.ToString(row["CONTACT_PHONE"]) : string.Empty;
                        vendorTraining.ContactEmail = row["CONTACT_EMAIL"] != DBNull.Value ? Convert.ToString(row["CONTACT_EMAIL"]) : string.Empty;
                        vendorTraining.VendorPhone = row["VENDOR_PHONE"] != DBNull.Value ? Convert.ToString(row["VENDOR_PHONE"]) : string.Empty;
                        vendorTraining.VendorEmail = row["VENDOR_EMAIL"] != DBNull.Value ? Convert.ToString(row["VENDOR_EMAIL"]) : string.Empty;
                        vendorTraining.TrainerName = row["TRAINER_NAME"] != DBNull.Value ? Convert.ToString(row["TRAINER_NAME"]) : string.Empty;
                        vendorTraining.TrainerID = row["TRAINER_ID"] != DBNull.Value ? Convert.ToString(row["TRAINER_ID"]) : string.Empty;
                        vendorTraining.TickectNo = row["TICKECT_NO"] != DBNull.Value ? Convert.ToString(row["TICKECT_NO"]) : string.Empty;
                        vendorTraining.Comments = row["COMMENTS"] != DBNull.Value ? Convert.ToString(row["COMMENTS"]) : string.Empty;
                        vendorTraining.TrainingMode = row["TRAINING_MODE"] != DBNull.Value ? Convert.ToString(row["TRAINING_MODE"]) : string.Empty;
                        vendorTraining.AgentID = row["AGENT_ID"] != DBNull.Value ? Convert.ToString(row["AGENT_ID"]) : string.Empty;
                        vendorTraining.DATE_CREATED = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                        // vendorTraining.ReqPostedDate = row["RFQ_POSTED_DATE"] != DBNull.Value ? Convert.ToDateTime(row["RFQ_POSTED_DATE"]) : DateTime.Now;
                        vendorTraining.DATE_MODIFIED = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]) : DateTime.Now;
                        vendorTraining.TrainingTime = row["TRAINING_TIME"] != DBNull.Value ? Convert.ToDateTime(row["TRAINING_TIME"]) : DateTime.Now;
                        vendorTraining.Status = row["STATUS"] != DBNull.Value ? Convert.ToString(row["STATUS"]) : string.Empty;


                        details.Add(vendorTraining);
                    }

                }
            }
            catch (Exception ex)
            {
                OpsVendorTraining error = new OpsVendorTraining();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details;
        }


        public List<OpsVendorTraining> GetALLVendorTrainingDetails(string sessionID, string domain, string from, string to)
        {
            List<OpsVendorTraining> details = new List<OpsVendorTraining>();
            try
            {
                string query = GetOpsQueries("GetALLVendorTrainingDetails");
                query = String.Format(query, from, to);
                //string query = String.Format(" select * from vendortraining vt WHERE CONVERT(date, vt.DATE_CREATED) BETWEEN CONVERT(DATE, '{0}') and CONVERT(DATE, '{1}'); ", from, to);
                DataSet ds = sqlHelper.ExecuteQuery(query);

                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        OpsVendorTraining vendorTraining = new OpsVendorTraining();

                        vendorTraining.VT_ID = row["VT_ID"] != DBNull.Value ? Convert.ToInt32(row["VT_ID"]) : 0;
                        vendorTraining.VendorID = row["VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_ID"]) : 0;
                        vendorTraining.ReqID = row["RFQ_ID"] != DBNull.Value ? Convert.ToInt32(row["RFQ_ID"]) : 0;
                        vendorTraining.TrainedContactName = row["TRAINED_CONTACT_NAME"] != DBNull.Value ? Convert.ToString(row["TRAINED_CONTACT_NAME"]) : string.Empty;
                        vendorTraining.ContactPhone = row["CONTACT_PHONE"] != DBNull.Value ? Convert.ToString(row["CONTACT_PHONE"]) : string.Empty;
                        vendorTraining.ContactEmail = row["CONTACT_EMAIL"] != DBNull.Value ? Convert.ToString(row["CONTACT_EMAIL"]) : string.Empty;
                        vendorTraining.VendorPhone = row["VENDOR_PHONE"] != DBNull.Value ? Convert.ToString(row["VENDOR_PHONE"]) : string.Empty;
                        vendorTraining.VendorEmail = row["VENDOR_EMAIL"] != DBNull.Value ? Convert.ToString(row["VENDOR_EMAIL"]) : string.Empty;
                        vendorTraining.TrainerName = row["TRAINER_NAME"] != DBNull.Value ? Convert.ToString(row["TRAINER_NAME"]) : string.Empty;
                        vendorTraining.TrainerID = row["TRAINER_ID"] != DBNull.Value ? Convert.ToString(row["TRAINER_ID"]) : string.Empty;
                        vendorTraining.TickectNo = row["TICKECT_NO"] != DBNull.Value ? Convert.ToString(row["TICKECT_NO"]) : string.Empty;
                        vendorTraining.Comments = row["COMMENTS"] != DBNull.Value ? Convert.ToString(row["COMMENTS"]) : string.Empty;
                        vendorTraining.TrainingMode = row["TRAINING_MODE"] != DBNull.Value ? Convert.ToString(row["TRAINING_MODE"]) : string.Empty;
                        vendorTraining.AgentID = row["AGENT_ID"] != DBNull.Value ? Convert.ToString(row["AGENT_ID"]) : string.Empty;
                        vendorTraining.DATE_CREATED = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                        // vendorTraining.ReqPostedDate = row["RFQ_POSTED_DATE"] != DBNull.Value ? Convert.ToDateTime(row["RFQ_POSTED_DATE"]) : DateTime.Now;
                        vendorTraining.DATE_MODIFIED = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]) : DateTime.Now;
                        vendorTraining.TrainingTime = row["TRAINING_TIME"] != DBNull.Value ? Convert.ToDateTime(row["TRAINING_TIME"]) : DateTime.Now;
                        vendorTraining.Status = row["STATUS"] != DBNull.Value ? Convert.ToString(row["STATUS"]) : string.Empty;


                        details.Add(vendorTraining);
                    }

                }
            }
            catch (Exception ex)
            {
                OpsVendorTraining error = new OpsVendorTraining();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details;
        }


        public List<OpsRequirement> GetRegularRfqReport(string domain, string from, string to, string sessionID)
        {
            List<OpsRequirement> requirements = new List<OpsRequirement>();
            OpsClients client = GetClientDetails(sessionID, domain);

            try
            {

                string query = GetOpsQueries("GetRegularRfqReport");
                query = String.Format(query, client.CreatedBy, from, to);
                DataSet ds = sqlHelper.ExecuteQuery(query);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    OpsRequirement req = new OpsRequirement();

                    req.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    req.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    req.REQ_POSTED_ON = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                    req.UserName = row["Posted_By"] != DBNull.Value ? Convert.ToString(row["Posted_By"]) : string.Empty;
                    req.CLOSED = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    req.VendorsInvited = row["VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_INVITED"]) : 0;
                    req.VendorsParticipated = row["VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_PARTICIPATED"]) : 0;
                    req.MinIbitialQuotationPrice = row["MIN_INITIAL_QUOTATION_PRICE"] != DBNull.Value ? Convert.ToDouble(row["MIN_INITIAL_QUOTATION_PRICE"]) : 0;
                    req.SAVINGS = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    requirements.Add(req);
                }
            }
            catch (Exception ex)
            {
                OpsRequirement recuirementerror = new OpsRequirement();
                recuirementerror.ErrorMessage = ex.Message;
            }


            return requirements;
        }

        public List<OpsRequirement> GetLogisticsRfqReport(string domain, string from, string to, string sessionID)
        {
            List<OpsRequirement> requirements = new List<OpsRequirement>();
            OpsClients client = GetClientDetails(sessionID, domain);

            try
            {

                string query = GetOpsQueries("GetLogisticsRfqReport");
                query = String.Format(query, client.CreatedBy, from, to);
                DataSet ds = sqlHelper.ExecuteQuery(query);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    OpsRequirement req = new OpsRequirement();


                    req.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    req.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    req.REQ_POSTED_ON = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                    req.UserName = row["Posted_By"] != DBNull.Value ? Convert.ToString(row["Posted_By"]) : string.Empty;
                    req.CLOSED = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    req.VendorsInvited = row["VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_INVITED"]) : 0;
                    req.VendorsParticipated = row["VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_PARTICIPATED"]) : 0;
                    req.MinIbitialQuotationPrice = row["MIN_INITIAL_QUOTATION_PRICE"] != DBNull.Value ? Convert.ToDouble(row["MIN_INITIAL_QUOTATION_PRICE"]) : 0;
                    req.SAVINGS = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    requirements.Add(req);
                }
            }
            catch (Exception ex)
            {
                OpsRequirement recuirementerror = new OpsRequirement();
                recuirementerror.ErrorMessage = ex.Message;
            }

            return requirements;
        }

        public List<OpsUserDetails> GetSubUserDataReport(string domain, string from, string to, string sessionID)
        {
            List<OpsUserDetails> users = new List<OpsUserDetails>();
            OpsClients client = GetClientDetails(sessionID, domain);
            try
            {

                string query = GetOpsQueries("GetSubUserDataReport");
                query = String.Format(query, client.CreatedBy, from, to);
                DataSet ds = sqlHelper.ExecuteQuery(query);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    OpsUserDetails req = new OpsUserDetails();

                    var FNAME = "";
                    var LNAME = "";
                    FNAME = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    LNAME = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;

                    req.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    req.UserName = FNAME + " " + LNAME;
                    req.U_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    req.U_PHONE = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    req.DateCreated = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                    req.CreatedByName = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;

                    users.Add(req);
                }
            }
            catch (Exception ex)
            {
                OpsUserDetails recuirementerror = new OpsUserDetails();
                recuirementerror.ErrorMessage = ex.Message;
            }

            return users;
        }

        public List<OpsUserDetails> GetVendorRegisteredReport(string domain, string from, string to, string sessionID)
        {
            List<OpsUserDetails> users = new List<OpsUserDetails>();
            OpsClients client = GetClientDetails(sessionID, domain);
            try
            {
                string query = GetOpsQueries("GetVendorRegisteredReport");
                query = String.Format(query, client.CreatedBy, from, to);
                DataSet ds = sqlHelper.ExecuteQuery(query);


                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    OpsUserDetails req = new OpsUserDetails();

                    var FNAME = "";
                    var LNAME = "";
                    FNAME = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    LNAME = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;

                    req.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    req.UserName = FNAME + " " + LNAME;
                    req.U_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    req.U_PHONE = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    req.DateCreated = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                    req.CreatedByName = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;

                    users.Add(req);
                }
            }
            catch (Exception ex)
            {
                OpsUserDetails recuirementerror = new OpsUserDetails();
                recuirementerror.ErrorMessage = ex.Message;
            }


            return users;
        }


        public List<OpsRequirement> GetVendorRfqData(string domain, string from, string to, string email, string phone, string sessionID)
        {
            List<OpsRequirement> requirements = new List<OpsRequirement>();
            OpsClients client = GetClientDetails(sessionID, domain);
            try {
                string query = GetOpsQueries("GetVendorRfqData");
                query = String.Format(query, client.CreatedBy, from, to, email, phone);
                DataSet ds = sqlHelper.ExecuteQuery(query);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    OpsRequirement req = new OpsRequirement();
                    req.Vendor = new User();

                    req.Vendor.Company = row["VendorCompany"] != DBNull.Value ? Convert.ToString(row["VendorCompany"]) : string.Empty;
                    req.Vendor.FirstName = row["VendorName"] != DBNull.Value ? Convert.ToString(row["VendorName"]) : string.Empty;
                    req.Vendor.PhoneNum = row["VendorPhone"] != DBNull.Value ? Convert.ToString(row["VendorPhone"]) : string.Empty;
                    req.Vendor.Email = row["VendorEmail"] != DBNull.Value ? Convert.ToString(row["VendorEmail"]) : string.Empty;
                    req.Vendor.IsOtpVerified = row["isOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isOTPVerified"]) : 0;
                    req.Vendor.IsEmailVerified = row["isEmailOTPVerified"] != DBNull.Value ? Convert.ToInt32(row["isEmailOTPVerified"]) : 0;
                    req.Vendor.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    req.AttachmentName = row["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row["QUOTATION_URL"]) : string.Empty;
                    req.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    req.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    req.REQ_POSTED_ON = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                    req.UserName = row["Posted_By"] != DBNull.Value ? Convert.ToString(row["Posted_By"]) : string.Empty;
                    req.CLOSED = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    string CLOSED = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    req.QUOTATION_FREEZ_TIME = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.Now;
                    req.EXP_START_TIME = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.Now;
                    req.CB_END_TIME = row["CB_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["CB_END_TIME"]) : DateTime.Now;
                    //req.VendorsInvited = row["VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_INVITED"]) : 0;
                    //req.VendorsParticipated = row["VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_PARTICIPATED"]) : 0;
                    req.SAVINGS = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    req.IsCapitalReq = row["IS_CAPITAL_REQ"] != DBNull.Value ? (Convert.ToInt32(row["IS_CAPITAL_REQ"]) == 1 ? true : false) : false;
                    req.IsSplitEnabled = row["IS_SPLIT_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_SPLIT_ENABLED"]) == 1 ? true : false) : false;
                    req.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_ENABLED"]) == 1 ? true : false) : false;
                    req.LOT_ID = row["LOT_ID"] != DBNull.Value ? Convert.ToInt32(row["LOT_ID"]) : 0;
                    req.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;
                    req.START_TIME = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    if (req.START_TIME == null)
                    {
                        req.START_TIME = DateTime.MaxValue;
                    }
                    req.END_TIME = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    if (req.END_TIME == null)
                    {
                        req.END_TIME = DateTime.MaxValue;
                    }

                    DateTime now = DateTime.Now;
                    if (req.END_TIME != DateTime.MaxValue && req.END_TIME > now && req.START_TIME < now)
                    {
                        DateTime date = Convert.ToDateTime(req.END_TIME);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        req.TimeLeft = diff;
                        req.CLOSED = prmops.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                    }
                    else if (req.START_TIME != DateTime.MaxValue && req.START_TIME > now)
                    {
                        DateTime date = Convert.ToDateTime(req.START_TIME);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        req.TimeLeft = diff;
                        req.CLOSED = prmops.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                    }
                    else if (req.END_TIME < now)
                    {
                        req.TimeLeft = -1;
                        if ((CLOSED == prmops.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || CLOSED == prmops.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && req.END_TIME < now)
                        {
                            req.CLOSED = prmops.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                            prmops.EndNegotiation(req.RequirementID, req.CustomerID, sessionID);
                        }
                        else
                        {
                            req.CLOSED = CLOSED;
                        }
                        //if (isPOSent == 0 && requirement.Status != GetEnumDesc<PRMStatus>(PRMStatus.POGenerated.ToString()))
                        //{
                        //    requirement.Status = "CLOSED";
                        //}
                    }
                    else if (req.START_TIME == DateTime.MaxValue)
                    {
                        req.TimeLeft = -1;
                        req.CLOSED = prmops.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                    }
                    if (CLOSED == prmops.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                    {
                        req.TimeLeft = -1;
                        req.CLOSED = CLOSED;
                    }

                    if (req.IS_CB_ENABLED == true && req.CB_END_TIME != DateTime.Now)
                    {
                        req.CLOSED = CLOSED;
                    }


                    requirements.Add(req);
                }

            }
            catch (Exception ex)
            {
                OpsRequirement recuirementerror = new OpsRequirement();
                recuirementerror.ErrorMessage = ex.Message;
            }
            
            return requirements;
        }

        public string GetOpsQueries(string TemplateName)
        {
            string templateFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
            string body = string.Empty;
            //body = Engine.Razor.RunCompile(File.ReadAllText(templateFolderPath + "/EmailTemplates/" + TemplateName), "Email", null, model);
            XmlDocument doc = new XmlDocument();
            doc.Load(templateFolderPath + "/EmailTemplates/OpsDataQueries.xml");
            XmlNode node = doc.DocumentElement.SelectSingleNode(TemplateName);
            body = node.InnerText;

            return body;
        }
    }

}
