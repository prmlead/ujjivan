﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Common;
using PRMServices.Models;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;
using System.Text.RegularExpressions;
using SendGrid.Helpers.Mail;
using System.Globalization;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMPOService : IPRMPOService
    {
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #region DESC

        public POVendor GetDesPoInfo(int reqID, int userID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            POVendor PoObject = new POVendor();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("po_GetDesPoInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    PoObject = POUtility.GetDescPoObject(row);
                }
            }
            catch (Exception ex)
            {
                PoObject.ErrorMessage = ex.Message;
            }

            return PoObject;
        }

        public List<UserDetails> GetVendors(int reqID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<UserDetails> listUser = new List<UserDetails>();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("po_getVendors", sd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        UserDetails user = new UserDetails();
                        user.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : 0;
                        user.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        user.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                        user.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        user.Price = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
                        listUser.Add(user);
                    }
                }
            }
            catch (Exception ex)
            {
                UserDetails user = new UserDetails();
                user.ErrorMessage = ex.Message;
                listUser.Add(user);
            }

            return listUser;
        }

        public DispatchTrack GetDescDispatch(int poID, int dtID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            DispatchTrack dispatchtrack = new DispatchTrack();
            try
            {
                sd.Add("P_PO_ID", poID);
                sd.Add("P_DT_ID", dtID);
                DataSet ds = sqlHelper.SelectList("po_GetDesDispatchInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    dispatchtrack = POUtility.GetDescDispatchObject(row);
                }
            }
            catch (Exception ex)
            {
                dispatchtrack.ErrorMessage = ex.Message;
            }

            return dispatchtrack;
        }

        public Response SaveDescPoInfo(POVendor povendor)
        {
            Utilities.ValidateSession(povendor.SessionID);
            Response response = new Response();
            try
            {
                string fileName = string.Empty;
                if (povendor.POFile != null)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + povendor.POFile.FileName);
                    SaveFile(fileName, povendor.POFile.FileStream);
                    fileName = "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + povendor.POFile.FileName;
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    povendor.POLink = fileName;
                }
                else
                {
                    PRMServices prm = new PRMServices();
                    Requirement req = prm.GetRequirementData(povendor.ReqID, povendor.CreatedBy, povendor.SessionID);
                    Requirement reqVendor = prm.GetRequirementData(povendor.ReqID, povendor.VendorID, povendor.SessionID);
                    UserDetails Vendor = prm.GetUserDetails(povendor.VendorID, povendor.SessionID);
                    UserDetails Customer = prm.GetUserDetails(povendor.CreatedBy, povendor.SessionID);
                    int margin = 12;
                    long tick = DateTime.Now.Ticks;
                    PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateDesPO(povendor, req, reqVendor, Vendor, Customer), PdfSharp.PageSize.A4, margin);
                    pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf"));
                    fileName = "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf";
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    povendor.POLink = fileName;
                }

                DataSet ds = POUtility.SaveDescPoInfoEntity(povendor);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdatePOStatus(int poID, string status, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            Response response = new Response();
            try
            {
                string query = string.Format("UPDATE poinformation SET PO_STATUS = '{0}' WHERE PO_ID = {1};", status, poID);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                response.ObjectID = 0;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        
        public Response SaveDesDispatchTrack(DispatchTrack dispatchtrack, POVendor povendor)
        {
            Utilities.ValidateSession(dispatchtrack.SessionID);
            Response response = new Response();
            try
            {
                string fileName = string.Empty;
                if (dispatchtrack.POFile != null)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + dispatchtrack.POFile.FileName);
                    SaveFile(fileName, dispatchtrack.POFile.FileStream);
                    fileName = "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + dispatchtrack.POFile.FileName;
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    dispatchtrack.DispatchLink = fileName;
                }
                else if (1 == 0)
                {
                    PRMServices prm = new PRMServices();
                    Requirement req = prm.GetRequirementData(povendor.ReqID, povendor.CreatedBy, povendor.SessionID);
                    Requirement reqVendor = prm.GetRequirementData(povendor.ReqID, povendor.VendorID, povendor.SessionID);
                    UserDetails Vendor = prm.GetUserDetails(povendor.VendorID, povendor.SessionID);
                    UserDetails Customer = prm.GetUserDetails(povendor.CreatedBy, povendor.SessionID);
                    int margin = 12;
                    long tick = DateTime.Now.Ticks;
                    PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateDesPO(povendor, req, reqVendor, Vendor, Customer), PdfSharp.PageSize.A4, margin);
                    pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf"));
                    fileName = "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf";
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    povendor.POLink = fileName;
                }

                DataSet ds = POUtility.SaveDesDispatchTrackObject(dispatchtrack, povendor);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion DESC

        #region ITEM

        public VendorPO GetVendorPoList(int reqID, int userID, int poID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            VendorPO vendorpo = new VendorPO();
            vendorpo.Vendor = new UserDetails();
            vendorpo.Req = new Requirement();
            vendorpo.ListPOItems = new List<POItems>();

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetPoList", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    vendorpo.Vendor = POUtility.GetVendorPoObject(row);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];
                    vendorpo.Req = POUtility.GetVendorPoReqObject(row1);
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        POItems poitems = new POItems();
                        poitems = POUtility.GetVendorPoItemsObject(row2);
                        vendorpo.ListPOItems.Add(poitems);
                    }
                }
            }
            catch (Exception ex)
            {
                vendorpo.ErrorMessage = ex.Message;
            }

            return vendorpo;
        }

        public VendorPO GetVendorPoInfo(int reqID, int userID, int poID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            VendorPO vendorpo = new VendorPO();
            vendorpo.Vendor = new UserDetails();
            vendorpo.Req = new Requirement();
            vendorpo.ListPOItems = new List<POItems>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetVendorPoInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    vendorpo.Vendor = POUtility.GetVendorPoObject(row);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];
                    vendorpo.Req = POUtility.GetVendorPoReqObject(row1);
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        POItems poitems = new POItems();
                        poitems = POUtility.GetVendorPoItemsObject(row2);
                        vendorpo.ListPOItems.Add(poitems);
                    }

                    if (vendorpo.ListPOItems != null && vendorpo.ListPOItems.Count > 0)
                    {
                        vendorpo.PurchaseOrderID = string.Empty;
                        if (vendorpo.ListPOItems.Any(p => !string.IsNullOrEmpty(p.PurchaseID)))
                        {
                            vendorpo.PurchaseOrderID = vendorpo.ListPOItems.First(p => !string.IsNullOrEmpty(p.PurchaseID)).PurchaseID;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                vendorpo.ErrorMessage = ex.Message;
            }

            return vendorpo;
        }

        public List<POSchedule> GetPOScheduler(string ponumber, string sessionID)
        {
            List<POSchedule> details = new List<POSchedule>();
            string query = $"SELECT * FROM PODeliverySchedule WHERE PURCHASE_ORDER_ID = '{ponumber}'";
            DataSet ds = sqlHelper.ExecuteQuery(query);
            CORE.DataNamesMapper<POSchedule> mapper = new CORE.DataNamesMapper<POSchedule>();
            details = mapper.Map(ds.Tables[0]).ToList();

            return details;
        }

        public List<PendingPO> GetAssignedPO(int userId,string sessionID)
        {
            List<PendingPO> details = new List<PendingPO>();
            string query = $@"SELECT PURCHASE_ORDER_ID, 
                (CASE WHEN PO_ID IS NULL OR PO_STATUS = 'PENDING' THEN 'PENDING'  ELSE 'COMPLETE'  END) AS PO_STATUS, 
                (select count(po_id) from poinformation where vendor_id = '{userId}') as TOTAL_COUNT FROM poinformation where vendor_id = '{userId}' 
                GROUP BY PO_ID, PURCHASE_ORDER_ID, PO_STATUS";
            DataSet ds = sqlHelper.ExecuteQuery(query);
            CORE.DataNamesMapper<PendingPO> mapper = new CORE.DataNamesMapper<PendingPO>();
            details = mapper.Map(ds.Tables[0]).ToList();

            return details;
        }

        public List<PendingPO> GetCompanyPendingPOS(int compID, string sessionID, bool IsVendor, int UserId)
        {
            List<PendingPO> details = new List<PendingPO>();
            string query = "";
            if (!IsVendor) {
                //query = $"select PO_ID,REQ_ID,VENDOR_ID,PURCHASE_ORDER_ID,EXPECTED_DELIVERY_DATE,PO_STATUS from poinformation where REQ_ID in (select REQ_ID from requirementdetails where getcompanyid(U_ID) = getcompanyid('{UserId}')) and PO_STATUS = 'PENDING' GROUP BY PURCHASE_ORDER_ID ORDER BY CREATED DESC";
                query = $"select po.PO_ID,po.REQ_ID,VENDOR_ID,po.PURCHASE_ORDER_ID,EXPECTED_DELIVERY_DATE,PO_STATUS,PO_TOTAL_PRICE,PO_IS_SEND_TO_VENDOR, " +
                    $"po.CREATED,VENDOR_DELIVERY_DATE,rd.REQ_CURRENCY,concat(v.U_FNAME, ' ', v.U_LNAME) as SUPPLIER_NAME, " +
                    $"count(distinct po.ITEM_ID) as NO_OF_ITEMS ,group_concat(distinct(ri.CATALOGUE_ITEM_ID)) as PRODUCT_ID, " +
                    $"group_concat(distinct(ri.PROD_ID)) as ITEMS, group_concat(distinct(cat.categoryName)) as CATEGORIES, " +
                    $"group_concat(distinct(cd.DEPT_CODE)) as DEPARTMENTS,group_concat(distinct ps.DELIVERY_DATE) as DELIVERY_DATE, " +
                    $"case when(select((select sum(VENDOR_PO_QUANTITY) from poinformation where PURCHASE_ORDER_ID = po.PURCHASE_ORDER_ID)= " +
                    $"(select sum(RECEIVED_QUANTITY) from dispatchtrack where PURCHASE_ORDER_ID = po.PURCHASE_ORDER_ID) = 1)) then 'DELIVERED' else 'PARTIAL' end as DELIVERY_STATUS, " +
                    $"(select count(DT_ID) - ifnull(count(RECEIVED_DATE), 0) from dispatchtrack where PURCHASE_ORDER_ID = po.PURCHASE_ORDER_ID) as AWAITING_RECEIPT " +
                    $"from poinformation po " +
                    $"inner join requirementdetails rd on po.REQ_ID = rd.REQ_ID " +
                    $"inner join vendors v on po.VENDOR_ID = v.V_ID " +
                    $"left join dispatchtrack dt on dt.PO_ID = po.PO_ID " +
                    $"left join requirementitems ri on ri.REQ_ID = po.REQ_ID " +
                    $"left join cm_productcategory pc on pc.ProductId = ri.CATALOGUE_ITEM_ID " +
                    $"left join cm_category cat on cat.CategoryId = pc.CategoryId " +
                    $"left join requirementdepartments rdept on rdept.REQ_ID = PO.REQ_ID " +
                    $"left join companydepartments cd on cd.DEPT_ID = rdept.DEPT_ID " +
                    $"LEFT join podeliveryschedule ps on ps.PURCHASE_ORDER_ID=PO.PURCHASE_ORDER_ID " +
                    $"where po.REQ_ID  in (select REQ_ID from requirementdetails where getcompanyid(U_ID) = getcompanyid('{UserId}')) " +
                    $"GROUP BY PURCHASE_ORDER_ID ORDER BY CREATED DESC ";
            } else {
                query = $"select PO_ID,REQ_ID,VENDOR_ID,PURCHASE_ORDER_ID,EXPECTED_DELIVERY_DATE,PO_STATUS from poinformation where VENDOR_ID = '{UserId}' and PO_STATUS = 'PENDING' GROUP BY PURCHASE_ORDER_ID ORDER BY CREATED DESC";
            }
            DataSet ds = sqlHelper.ExecuteQuery(query);
            CORE.DataNamesMapper<PendingPO> mapper = new CORE.DataNamesMapper<PendingPO>();
            details = mapper.Map(ds.Tables[0]).ToList();

            return details;
        }



        public List<POSchedule> GetDeliverySchedules(string poNumbers, string sessionID)
        {
            List<POSchedule> details = new List<POSchedule>();
            string query = $"SELECT PURCHASE_ORDER_ID,DELIVERY_DATE,QUANITTY, " +
                $" (SELECT count(ITEM_ID) FROM PODeliverySchedule WHERE find_in_set(PURCHASE_ORDER_ID,'{poNumbers}') and DELIVERY_DATE > UTC_TIMESTAMP()) as TOTAL_COUNT" +
                $" FROM PODeliverySchedule WHERE find_in_set(PURCHASE_ORDER_ID,'{poNumbers}') and DELIVERY_DATE > UTC_TIMESTAMP(); ";
            DataSet ds = sqlHelper.ExecuteQuery(query);
            CORE.DataNamesMapper<POSchedule> mapper = new CORE.DataNamesMapper<POSchedule>();
            details = mapper.Map(ds.Tables[0]).ToList();

            return details;
        }

        public List<PaymentInfo> GetPendingPayments(int compid, string sessionid)
        {
            List<PaymentInfo> details = new List<PaymentInfo>();
            string query = $"select PD.PAYMENT_STATUS,PD.PAYMENT_DATE,PD.MODIFIED,PD.PAYMENT_CODE,PD.DISPATCH_CODE,PD.PURCHASE_ORDER_ID,PD.PAYMENT_AMOUNT,dt.INVOICE_AMOUNT,"+
                $"ri.PROD_ID,cat.categoryName,dt.INVOICE_NUMBER,dt.RECEIVED_DATE,(select count(*) From paymentdetails) as TotalPayments,group_concat(distinct(ri.PROD_ID)) as ITEMS,group_concat(distinct(cat.categoryName)) as CATEGORIES, " +
                $"group_concat(distinct(cd.DEPT_CODE)) as DEPARTMENTS,pr.pr_number from paymentdetails PD" +
                $" left join dispatchtrack dt on dt.PO_ID = PD.PO_ID left join poinformation po on po.PO_ID = PD.PO_ID" +
                $" left join requirementitems ri on ri.REQ_ID = po.REQ_ID left join pr_items prit on prit.req_id = ri.REQ_ID" +
                $" left join pr_details pr on pr.pr_id = prit.pr_id left join cm_productcategory pc on pc.ProductId = ri.CATALOGUE_ITEM_ID" +
                $" left join cm_category cat on cat.CategoryId = pc.CategoryId" +
                $" left join requirementdepartments rd on rd.REQ_ID = PO.REQ_ID left join companydepartments cd on cd.DEPT_ID = rd.DEPT_ID" +
                $" where pd.po_id > 0 and cd.comp_id = {compid} group by PD.PAYMENT_CODE" +
                $" ORDER BY 1 DESC";
            DataSet ds = sqlHelper.ExecuteQuery(query);

            try
            {
                if (ds!=null & ds.Tables.Count > 0)
                {
                    foreach(var row in ds.Tables[0].AsEnumerable())
                    {
                        PaymentInfo detail = new PaymentInfo();
                        detail.PaymentDate = row["PAYMENT_DATE"] != DBNull.Value ? Convert.ToDateTime(row["PAYMENT_DATE"]) : DateTime.MaxValue;
                        detail.InvoiceDate = row["MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["MODIFIED"]) : DateTime.MaxValue;
                        detail.PaymentCode = row["PAYMENT_CODE"] != DBNull.Value ? Convert.ToString(row["PAYMENT_CODE"]) : string.Empty;

                        detail.DispatchCode = row["DISPATCH_CODE"] != DBNull.Value ? Convert.ToString(row["DISPATCH_CODE"]) : string.Empty;
                        detail.PrNumber = row["pr_number"] != DBNull.Value ? Convert.ToString(row["pr_number"]) : string.Empty;
                        detail.POOrderId = row["PURCHASE_ORDER_ID"] != DBNull.Value ? Convert.ToString(row["PURCHASE_ORDER_ID"]) : string.Empty;
                        detail.PaymentAmount = row["PAYMENT_AMOUNT"] != DBNull.Value ? Convert.ToDecimal(row["PAYMENT_AMOUNT"]) : 0;
                        detail.InvoiceAmount = row["INVOICE_AMOUNT"] != DBNull.Value ? Convert.ToString(row["INVOICE_AMOUNT"]) : string.Empty;
                        detail.TotalPayments = row["TotalPayments"] != DBNull.Value ? Convert.ToInt32(row["TotalPayments"]) : 0;

                        detail.PaymentDateTemp = row["PAYMENT_DATE"] != DBNull.Value ? Convert.ToDateTime(row["PAYMENT_DATE"]) : DateTime.MaxValue;
                        detail.InvoiceDateTemp = row["MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["MODIFIED"]) : DateTime.MaxValue;
                        detail.ItemName = row["ITEMS"] != DBNull.Value ? Convert.ToString(row["ITEMS"]) : string.Empty;
                        detail.Department = row["DEPARTMENTS"] != DBNull.Value ? Convert.ToString(row["DEPARTMENTS"]) :string.Empty;
                        detail.Category = row["CATEGORIES"] != DBNull.Value ? Convert.ToString(row["CATEGORIES"]) : string.Empty;
                        detail.InvoiceNumber = row["INVOICE_NUMBER"] != DBNull.Value ? Convert.ToString(row["INVOICE_NUMBER"]) : string.Empty;
                        detail.ReceivedDate = row["RECEIVED_DATE"] != DBNull.Value ? Convert.ToDateTime(row["RECEIVED_DATE"]) : DateTime.MaxValue;
                        detail.PaymentStatus = row["PAYMENT_STATUS"] != DBNull.Value ? Convert.ToString(row["PAYMENT_STATUS"]) : string.Empty;
                        //var days = detail.PaymentDateTemp.Day - detail.InvoiceDateTemp.Day;



                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                
            }


            return details;
        }


        public List<Filter> GetFiltersOnLoadData(int compid, string sessionid)
        {
            List<Filter> details = new List<Filter>();
            string query = $"select categoryid as ID,categoryname as `NAME`,'CATEGORY' AS `TYPE` from cm_category where companyid = {compid} " +
                $"and isvalid = 1 union select dept_id AS ID,dept_code AS `NAME`,'DEPARTMENT' AS `TYPE` from companydepartments " +
                $"where comp_id = {compid} and is_valid = 1; ";
            DataSet ds = sqlHelper.ExecuteQuery(query);

            try
            {
                if (ds != null & ds.Tables.Count > 0)
                {
                    foreach (var row in ds.Tables[0].AsEnumerable())
                    {
                        Filter detail = new Filter();
                        detail.ID = row["ID"] != DBNull.Value ? Convert.ToInt32(row["ID"]) : 0;
                        detail.NAME = row["NAME"] != DBNull.Value ? Convert.ToString(row["NAME"]) : string.Empty;
                        detail.TYPE = row["TYPE"] != DBNull.Value ? Convert.ToString(row["TYPE"]) : string.Empty;

                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {

            }


            return details;
        }


        public POInvoice[] GetPOInvoiceDetails(string ponumber, string sessionid)
        {
            List<POInvoice> details = new List<POInvoice>();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $"SELECT * FROM POInvoiceDetails WHERE PO_NUMBER = '{ponumber}'";
                DataSet ds = sqlHelper.ExecuteQuery(query);
                CORE.DataNamesMapper<POInvoice> mapper = new CORE.DataNamesMapper<POInvoice>();
                details = mapper.Map(ds.Tables[0]).ToList();

                if (details != null && details.Count > 0)
                {
                    foreach (var detail in details)
                    {
                        if (!string.IsNullOrEmpty(detail.ATTACHMENTS))
                        {
                            ds = sqlHelper.ExecuteQuery($"SELECT * FROM attachmentdetails WHERE ATT_ID IN ({detail.ATTACHMENTS})");
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            {
                                detail.AttachmentsArray = new List<FileUpload>();
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    detail.AttachmentsArray.Add(new FileUpload()
                                    {
                                        FileID = Convert.ToInt32(row["ATT_ID"]),
                                        FileName = Convert.ToString(row["ATT_PATH"]),
                                    }); ;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return details.ToArray();
        }

        public Response SavePOInvoice(POInvoice details)
        {
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(details.SessionID);
                if (details != null && !string.IsNullOrEmpty(details.PO_NUMBER))
                {
                    if (details.AttachmentsArray != null && details.AttachmentsArray.Count > 0)
                    {
                        string fileName = string.Empty;
                        foreach (FileUpload fd in details.AttachmentsArray)
                        {
                            if (fd.FileStream != null && !string.IsNullOrWhiteSpace(fd.FileName))
                            {
                                fd.FileName = Regex.Replace(fd.FileName, @"[^0-9a-zA-Z.]+", "PRM_");
                                if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                                {
                                    var attachName = string.Empty;
                                    long tick = DateTime.UtcNow.Ticks;
                                    attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "POINVOICE_" + tick + "_VENDOR_" + details.VENDOR_ID + "_" + fd.FileName);
                                    SaveFile(attachName, fd.FileStream);
                                    attachName = "POINVOICE_" + tick + "_VENDOR_" + details.VENDOR_ID + "_" + fd.FileName;

                                    Response res = SaveAttachment(attachName);
                                    if (res.ErrorMessage != "")
                                    {
                                        response.ErrorMessage = res.ErrorMessage;
                                    }

                                    fd.FileID = res.ObjectID;
                                }
                            }

                            fileName += Convert.ToString(fd.FileID) + ",";
                        }

                        fileName = fileName.Substring(0, fileName.Length - 1);
                        details.ATTACHMENTS = fileName;
                    }

                    string query = $@"INSERT INTO [dbo].[POInvoiceDetails] ([COMP_ID], [PO_NUMBER], [VENDOR_CODE], [VENDOR_ID], [INVOICE_NUMBER], [INVOICE_AMOUNT], [ATTACHMENTS], [COMMENTS], [STATUS], 
                                [DATE_CREATED], [DATE_MODIFIED], [CREATED_BY], [MODIFIED_BY])      
                                VALUES (0, '{details.PO_NUMBER}', '{details.VENDOR_CODE}', {details.VENDOR_ID}, '{details.INVOICE_NUMBER}', {details.INVOICE_AMOUNT}, 
                                '{details.ATTACHMENTS}', '{details.COMMENTS}', '{details.STATUS}', GETUTCDATE(), GETUTCDATE(), {details.VENDOR_ID}, {details.VENDOR_ID})";
                    sqlHelper.ExecuteNonQuery_IUD(query);
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.ObjectID = -1;
            }

            return response;
        }

        public Response SavePOAttachments(POScheduleDetails details)
        {
            Response response = new Response();

            try
            {
                Utilities.ValidateSession(details.SessionID);
                if (details != null && !string.IsNullOrEmpty(details.PO_NUMBER))
                {
                    if (details.AttachmentsArray != null && details.AttachmentsArray.Count > 0)
                    {
                        string fileName = string.Empty;
                        foreach (FileUpload fd in details.AttachmentsArray)
                        {
                            if (fd.FileStream != null && !string.IsNullOrWhiteSpace(fd.FileName))
                            {
                                fd.FileName = Regex.Replace(fd.FileName, @"[^0-9a-zA-Z.]+", "PRM_");
                                if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                                {
                                    var attachName = string.Empty;
                                    long tick = DateTime.UtcNow.Ticks;
                                    attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PO_" + tick + "_PONUMBER_" + details.PO_NUMBER + "_" + fd.FileName);
                                    SaveFile(attachName, fd.FileStream);
                                    attachName = "PO_" + tick + "_PONUMBER_" + details.PO_NUMBER + "_" + fd.FileName;

                                    Response res = SaveAttachment(attachName);
                                    if (res.ErrorMessage != "")
                                    {
                                        response.ErrorMessage = res.ErrorMessage;
                                    }

                                    fd.FileID = res.ObjectID;
                                }
                            }

                            fileName += Convert.ToString(fd.FileID) + ",";
                        }
                        fileName = fileName.Substring(0, fileName.Length - 1);
                        details.ATTACHMENTS = fileName;
                    }

                    string query = $@"UPDATE POScheduleDetails SET ATTACHMENTS = '{details.ATTACHMENTS}' WHERE PO_NUMBER = '{details.PO_NUMBER}' and VENDOR_CODE = '{details.VENDOR_CODE}'";
                    sqlHelper.ExecuteNonQuery_IUD(query);
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.ObjectID = -1;
            }

            return response;
        }

        public Response PoApproval(POScheduleDetails details, bool isPoApprove, string isPoRejectComments, int currentUserID)
        {
            Response response = new Response();
            PRMServices prm = new PRMServices();
            try
            {
                Utilities.ValidateSession(details.SessionID);
                UserInfo Vendor = prm.GetUserNew(details.VENDOR_ID, details.SessionID);
                UserInfo Customer = prm.GetUserNew(currentUserID, details.SessionID);
                if (details != null && !string.IsNullOrEmpty(details.PO_NUMBER))
                {
                    string status = isPoApprove == true ? "APPROVED" : "REJECTED";
                    string statusTemp = isPoApprove == true ? "Approved" : "Rejected";
                    string query = $@"UPDATE POScheduleDetails SET VENDOR_ACK_STATUS = '{status}', VENDOR_ACK_REJECT_COMMENTS = '{isPoRejectComments}',  DATE_MODIFIED = utc_timestamp, MODIFIED_BY = {currentUserID} 
                                 WHERE PO_NUMBER = '{details.PO_NUMBER}'";
                    sqlHelper.ExecuteNonQuery_IUD(query);

                    string body = prm.GenerateEmailBody("VendoremailForAcknowledege");
                    body = String.Format(body, Vendor.FirstName, Vendor.LastName, status, Customer.FirstName, Customer.LastName, details.PO_NUMBER, statusTemp == "Rejected" ? isPoRejectComments : "-");
                    prm.SendEmail(Vendor.Email + "," + Vendor.AltEmail, "PO Number: " + details.PO_NUMBER + " - Acknowledge" + " " + statusTemp, body, 0, details.VENDOR_ID, "", details.SessionID).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.ObjectID = -1;
            }

            return response;
        }

        public Response DeletePOInvoice(string ponumber, string invoicenumber, string sessionid)
        {
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                if (!string.IsNullOrEmpty(ponumber) && !string.IsNullOrEmpty(invoicenumber))
                {
                    string query = $@"DELETE FROM [dbo].[POInvoiceDetails] WHERE PO_NUMBER = '{ponumber}' AND INVOICE_NUMBER = '{invoicenumber}'";
                    sqlHelper.ExecuteNonQuery_IUD(query);
                    response.ObjectID = 1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.ObjectID = -1;
            }

            return response;
        }

        public Byte[] ConvertTOPDF(string name,string ponumber,string htmlStr,string sessionid) 
        {
            Utilities.ValidateSession(sessionid);
            Byte[] response = null;
            using (MemoryStream ms = new MemoryStream())
            {
                var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(htmlStr, PdfSharp.PageSize.A4);
                pdf.Save(ms);
                response = ms.ToArray();
            }
            return response;
        }

        public Response SavePOSchedule(POSchedule details)
        {
            Response response = new Response();
            Utilities.ValidateSession(details.SessionID);
            var deliveryDate = details.DELIVERY_DATE.HasValue ? details.DELIVERY_DATE.Value : DateTime.UtcNow.AddMonths(3);
            string query = $@"INSERT INTO podeliveryschedule (PURCHASE_ORDER_ID, ITEM_ID, QUANITTY, COMMENTS, DELIVERY_DATE, DATE_CREATED, DATE_MODIFIED, CREATED_BY, MODIFIED_BY)
            VALUES ('{details.PURCHASE_ORDER_ID}', {details.ITEM_ID},  {details.QUANITTY}, '{details.COMMENTS}', '{deliveryDate.ToString("yyyy-MM-dd")}', UTC_TIMESTAMP(), UTC_TIMESTAMP(), 0, 0); ";

            sqlHelper.ExecuteNonQuery_IUD(query);

            response.ObjectID = 1;

            return response;
        }

        public Response SavePODetails(POScheduleDetails details)
        {
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(details.SessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                sd.Add("P_PO_ID", details.PO_ID);
                sd.Add("P_COMP_ID", details.COMP_ID);
                sd.Add("P_PRM_PO_NUMBER", details.PRM_PO_NUMBER);
                sd.Add("P_PO_NUMBER", details.PO_NUMBER);
                sd.Add("P_PO_TYPE", details.PO_TYPE);
                sd.Add("P_VENDOR_CODE", details.VENDOR_CODE);
                sd.Add("P_VENDOR_ID", details.VENDOR_ID);
                sd.Add("P_TOTAL_VALUE", details.TOTAL_VALUE);
                sd.Add("P_BILL_TO_LOCATION_ID", details.BILL_TO_LOCATION_ID);
                sd.Add("P_SHIP_TO_LOCATION_ID", details.SHIP_TO_LOCATION_ID);
                sd.Add("P_ORG_ID", details.ORG_ID);
                sd.Add("P_PO_COMMENTS", details.PO_COMMENTS);
                sd.Add("P_PO_CREATOR", details.PO_CREATOR);
                sd.Add("P_DEPARTMENT_CODE", details.DEPARTMENT_CODE);
                sd.Add("P_PO_STATUS", details.PO_STATUS);
                sd.Add("P_QCS_ID", details.QCS_ID);
                sd.Add("P_ATTACHMENTS", details.ATTACHMENTS);
                sd.Add("P_WF_ID", details.WF_ID);
                sd.Add("P_USER", details.ModifiedBY);

                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("po_SavePODetails", sd);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    details.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    details.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }

                if (string.IsNullOrEmpty(details.ErrorMessage) && details.ObjectID > 0)
                {
                    foreach (POScheduleDetailsItems item in details.PO_ITEMS)
                    {
                        item.PO_ID = details.ObjectID;
                        SavePOItems(item, details.SessionID);
                    }

                    sd = new SortedDictionary<object, object>();
                    sd.Add("P_U_ID", details.ModifiedBY);
                    sd.Add("P_MODULE_ID", details.PO_ID > 0 ? details.PO_ID : details.ObjectID);
                    sd.Add("P_MODULE_NAME", "PO_V1");
                    sqlHelper.SelectList("cp_SavePOApprovalWorkflow", sd);

                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                logger.Error("Error in saving PO Details------->>>", ex.Message);
            }

            return response;
        }

        public Response SavePOItems(POScheduleDetailsItems item, string sessionid)
        {
            Response details = new Response();

            try
            {
                Utilities.ValidateSession(sessionid);
                //if (pritems.ATTACHMENTSARR != null && pritems.ATTACHMENTSARR.Count > 0)
                //{
                //    string fileName = string.Empty;
                //    foreach (FileUpload fd in pritems.ATTACHMENTSARR)
                //    {
                //        if (fd.FileStream != null && !string.IsNullOrWhiteSpace(fd.FileName))
                //        {
                //            string companyName = ConfigurationManager.AppSettings["COMPANY_NAME"].ToString();
                //            if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                //            {
                //                var attachName = string.Empty;
                //                long tick = DateTime.UtcNow.Ticks;
                //                attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + fd.FileName);
                //                prm.SaveFile(attachName, fd.FileStream);
                //                attachName = fd.FileName;

                //                Response res = prm.SaveAttachment(attachName);

                //                fd.FileID = res.ObjectID;
                //            }
                //        }

                //        fileName += Convert.ToString(fd.FileID) + ",";
                //    }

                //    fileName = fileName.Substring(0, fileName.Length - 1);
                //    pritems.ATTACHMENTS = fileName;
                //}

                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PO_ITEM_ID", item.PO_ITEM_ID);
                sd.Add("P_PO_ID", item.PO_ID);
                sd.Add("P_PO_LINE_ITEM", item.PO_LINE_ITEM);
                sd.Add("P_LINE_TYPE", item.LINE_TYPE);
                sd.Add("P_ORDER_QTY", item.ORDER_QTY);
                sd.Add("P_UNIT_PRICE", item.UNIT_PRICE);
                sd.Add("P_CGST", item.CGST);
                sd.Add("P_SGST", item.SGST);
                sd.Add("P_IGST", item.IGST);
                sd.Add("P_DELIVERY_DATE", item.DELIVERY_DATE);
                sd.Add("P_PRODUCT_ID", item.PRODUCT_ID);
                sd.Add("P_CATEGORY_ID", item.CATEGORY_ID);
                sd.Add("P_SHIP_TO_ORGANIZATION_CODE", item.SHIP_TO_ORGANIZATION_CODE);
                sd.Add("P_SHIP_TO_LOCATION_ID", item.SHIP_TO_LOCATION_ID);
                sd.Add("P_PR_NUMBERS", item.PR_NUMBERS);
                sd.Add("P_PR_ITEM_IDS", item.PR_LINE_ITEMS);
                sd.Add("P_PENDING_QTY", item.PENDING_QTY);
                sd.Add("P_USER", item.ModifiedBY);

                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("pr_SavePOItems", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
                item.PO_ITEM_ID = Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }

            return details;
        }

        public Response SaveVendorPOInfo(VendorPO vendorpo)
        {
            Utilities.ValidateSession(vendorpo.SessionID);
            Response response = new Response();
            Requirement req = new Requirement();
            Requirement vendorreq = new Requirement();
            UserDetails customer = new UserDetails();
            UserDetails vendor = new UserDetails();
            PRMPRService prmpr = new PRMPRService();
            try
            {
                PRMServices prm = new PRMServices();
                string folderPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL);
                req = prm.GetRequirementData(vendorpo.Req.RequirementID, vendorpo.Req.CustomerID, vendorpo.SessionID);
                vendorreq = prm.GetRequirementData(vendorpo.Req.RequirementID, vendorpo.Vendor.UserID, vendorpo.SessionID);
                vendor = prm.GetUserDetails(vendorpo.Vendor.UserID, vendorpo.SessionID);
                customer = prm.GetUserDetails(vendorpo.Req.CustomerID, vendorpo.SessionID);
                string fileName = string.Empty;


                int poSeries = 0;
                if (vendorpo.ListPOItems[0].POID == 0)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_MODIFIED_BY", vendorpo.Req.CustomerID);
                    DataSet ds1 = sqlHelper.SelectList("po_genSeries", sd);
                    if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0 && ds1.Tables[0].Rows[0][0] != null)
                    {
                        poSeries = ds1.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString()) : -1;
                        // response.Message = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    }

                    //vendorpo.PurchaseOrderID = prmpr.generatePRNumber("", "PO", vendorpo.SessionID, vendorpo.ListPOItems[0].COMP_ID, vendorpo.ListPOItems[0].DEPT_ID, vendorpo.ListPOItems[0].PurchaseID);
                    vendorpo.PurchaseOrderID = "PO-" + DateTime.Now.Ticks.ToString();
                }
                else
                {
                    vendorpo.PurchaseOrderID =  vendorpo.ListPOItems[0].PurchaseID;
                }


                if (vendorpo.POFile != null)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + req.RequirementID + "_user" + vendor.UserID + "_" + vendorpo.POFile.FileName);
                    SaveFile(fileName, vendorpo.POFile.FileStream);
                    fileName = "req" + req.RequirementID + "_user" + vendor.UserID + "_" + vendorpo.POFile.FileName;
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    vendorpo.POLink = fileName;
                }
                else
                {
                    long nowTicks = DateTime.Now.Ticks;
                    int margin = 16;
                    //PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateItemizedPO(vendorpo, req, vendorreq, customer, vendor), PdfSharp.PageSize.A4, margin);
                    //pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf"));
                    //fileName = "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    List<KeyValuePair<string, DataTable>> poTbl = new List<KeyValuePair<string, DataTable>>();
                    poTbl = PdfUtilities.MakeDataTablePo(vendorpo, req, vendorreq, customer, vendor);
                    PdfUtilities.MakeDataTablePoToPdf(poTbl, @folderPath + "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf", customer, vendor, vendorpo);
                    fileName = "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    vendorpo.POLink = fileName;
                }


                foreach (POItems POItem in vendorpo.ListPOItems)
                {
                    POItem.PurchaseID = vendorpo.PurchaseOrderID;
                    if (!string.IsNullOrEmpty(vendorpo.PurchaseOrderID) && !POItem.IsSelected)
                    {
                        string query = $"DELETE FROM poinformation WHERE REQ_ID = {vendorpo.Req.RequirementID} AND VENDOR_ID = {vendorpo.Vendor.UserID} AND ITEM_ID = {POItem.ItemID}";
                        sqlHelper.ExecuteNonQuery_IUD(query);
                    }
                    else
                    {
                        if (POItem.IsSelected)
                        {
                            DataSet ds = POUtility.SavePOItemEntity(POItem, vendorpo, req, vendor);
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                            {
                                response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            }

                            if (response.ObjectID > 0 && vendorpo.Req.IsContract && POItem.IsCoreProductCategory > 0)
                            {
                                var contactStartTime = POItem.ContractStartTime.HasValue ? POItem.ContractStartTime.Value : DateTime.UtcNow;
                                var contactEndTime = POItem.ContractEndTime.HasValue ? POItem.ContractEndTime.Value : DateTime.UtcNow.AddMonths(3);
                                string contractQuery = $@"INSERT INTO productcontractdetails (ProductId, U_ID, Number, Value, Quantity, AvailedQuantity, document, StartTime, EndTime, IsValid, CompanyName) VALUES 
                                ({POItem.CatalogProductId}, {vendorpo.Vendor.UserID}, {POItem.ItemID}, {POItem.VendorTotalPrice}, {POItem.VendorPOQuantity}, 0, '', '{contactStartTime.ToString("yyyy-MM-dd")}', '{contactEndTime.ToString("yyyy-MM-dd")}', 1, '{vendorpo.Vendor.FirstName + ' ' + vendorpo.Vendor.LastName}'); ";
                                sqlHelper.ExecuteNonQuery_IUD(contractQuery);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }


        public List<DispatchTrack> GetDispatchTrackList(string poorderid, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Utilities.ValidateSession(sessionID);
            List<DispatchTrack> listDispatchTrack = new List<DispatchTrack>();
            Requirement req = new Requirement();
            UserDetails vendor = new UserDetails();
            try
            {
                sd.Add("P_PURCHASE_ORDER_ID", poorderid);
                DataSet ds = sqlHelper.SelectList("po_GetDispatchTrackList", sd);
                List<POItems> listDispatcPOItems = new List<POItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];
                    req = POUtility.GetRequirementDetails(row1);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    DataRow row2 = ds.Tables[2].Rows[0];
                    vendor = POUtility.GetVendorDetails(row2);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DispatchTrack dispatchtrack = new DispatchTrack();
                        dispatchtrack = POUtility.GetDispatchTrackObject(row);
                        dispatchtrack.VendorPOObject = new VendorPO();
                        dispatchtrack.VendorPOObject.Req = new Requirement();
                        dispatchtrack.VendorPOObject.Req = req;
                        dispatchtrack.VendorPOObject.Vendor = vendor;
                        listDispatchTrack.Add(dispatchtrack);
                    }
                }
            }
            catch (Exception ex)
            {
                DispatchTrack dispatchtrack = new DispatchTrack();
                dispatchtrack.ErrorMessage = ex.Message;
                listDispatchTrack.Add(dispatchtrack);
            }

            return listDispatchTrack;
        }

        public List<DispatchTrack> GetDispatchTrack(string poorderid, string dcode, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<DispatchTrack> listDispatchTrack = new List<DispatchTrack>();
            try
            {
                sd.Add("P_PURCHASE_ORDER_ID", poorderid);
                sd.Add("P_DISPATCH_CODE", dcode);
                DataSet ds = sqlHelper.SelectList("po_GetDispatchTrack", sd);
                List<POItems> listDispatcPOItems = new List<POItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        POItems dispatchPoItem = new POItems();
                        dispatchPoItem = POUtility.GetDispatchPoItem(row);
                        listDispatcPOItems.Add(dispatchPoItem);
                    }
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DispatchTrack dispatchtrack = new DispatchTrack();
                        dispatchtrack = POUtility.GetDispatchTrackObject(row);
                        dispatchtrack.POItemsEntity = listDispatcPOItems.Where(p => p.PurchaseID == dispatchtrack.PurchaseID).ToList();
                        listDispatchTrack.Add(dispatchtrack);
                    }
                }
            }
            catch (Exception ex)
            {
                DispatchTrack dispatchtrack = new DispatchTrack();
                dispatchtrack.ErrorMessage = ex.Message;
                listDispatchTrack.Add(dispatchtrack);
            }

            return listDispatchTrack;
        }

        public Response SaveDispatchTrack(DispatchTrack dispatchtrack, string requestType)
        {
            Utilities.ValidateSession(dispatchtrack.SessionID);
            PRMNotifications notifications = new PRMNotifications();
            Response response = new Response();
            Requirement newreq = new Requirement();
            Requirement vendorreq = new Requirement();
            UserDetails customer = new UserDetails();
            UserDetails vendor = new UserDetails();
            try
            {
                PRMServices prm = new PRMServices();
                string folderPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL);
                string fileName = string.Empty;
                newreq = prm.GetRequirementData(dispatchtrack.POItemsEntity[0].ReqID, 0, dispatchtrack.SessionID);
                vendorreq = prm.GetRequirementData(dispatchtrack.POItemsEntity[0].ReqID, dispatchtrack.POItemsEntity[0].VendorID, dispatchtrack.SessionID);
                customer = prm.GetUserDetails(newreq.CustomerID, dispatchtrack.SessionID);
                vendor = prm.GetUserDetails(dispatchtrack.POItemsEntity[0].VendorID, dispatchtrack.SessionID);
                if (requestType == "DISPATCH")
                {
                    string mrrTable = string.Empty;
                    foreach (POItems poItem in dispatchtrack.POItemsEntity)
                    {
                        string xml = string.Empty;
                        xml = notifications.GenerateEmailBody("MrrDispatchXML");
                        xml = String.Format(xml, poItem.ProductIDorName, poItem.VendorPOQuantity, poItem.SumDispatchQuantity, poItem.SumRecivedQuantity, poItem.DispatchQuantity);
                        mrrTable += xml;

                    }

                    long nowTicks = DateTime.Now.Ticks;
                    int margin = 16;
                    //PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateMRRPO(dispatchtrack, newreq, mrrTable, customer), PdfSharp.PageSize.A4, margin);
                    //pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PurchaseID_" + dispatchtrack.PurchaseID + "_DispatchCode_" + dispatchtrack.DispatchCode + ".pdf"));


                    List<KeyValuePair<string, DataTable>> poTbl = new List<KeyValuePair<string, DataTable>>();
                    poTbl = PdfUtilities.MakeDataTableDispatch(dispatchtrack, newreq, vendorreq, customer, vendor);
                    PdfUtilities.MakeDataTableDispatchToPdf(poTbl, @folderPath + "MaterialDispatch" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf", customer, vendor, dispatchtrack);
                    fileName = "MaterialDispatch" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    dispatchtrack.DispatchLink = fileName;
                }

                if (requestType == "RECEIVE")
                {
                    string mrrTable = string.Empty;
                    foreach (POItems poItem in dispatchtrack.POItemsEntity)
                    {
                        string xml = string.Empty;
                        xml = notifications.GenerateEmailBody("MrrReceiveXML");
                        xml = String.Format(xml, poItem.ProductIDorName, poItem.VendorPOQuantity, poItem.SumDispatchQuantity, poItem.SumRecivedQuantity, poItem.SumReturnQuantity, poItem.DispatchQuantity, poItem.RecivedQuantity, poItem.ReturnQuantity);
                        mrrTable += xml;

                    }

                    long nowTicks = DateTime.Now.Ticks;
                    int margin = 16;
                    List<KeyValuePair<string, DataTable>> poTbl = new List<KeyValuePair<string, DataTable>>();
                    poTbl = PdfUtilities.MakeDataTableMRR(dispatchtrack, newreq, vendorreq, customer, vendor);
                    PdfUtilities.MakeDataTableMRRToPdf(poTbl, @folderPath + "ASN" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf", customer, vendor, dispatchtrack);
                    fileName = "ASN" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    //PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateMRRPOReportpdf(dispatchtrack, newreq, mrrTable, customer), PdfSharp.PageSize.A4, margin);
                    //pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PurchaseID_" + dispatchtrack.PurchaseID + "_ReceivedCode_" + dispatchtrack.RecivedCode + ".pdf"));
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    dispatchtrack.RecivedLink = Convert.ToInt32(fileName);
                }

                Response res = SaveAttachment(fileName);
                fileName = res.ObjectID.ToString();
                foreach (POItems poItem in dispatchtrack.POItemsEntity)
                {
                    if(poItem.IsCoreProductCategory > 0)
                    {
                        DataSet ds = POUtility.SaveDispatchTrackObject(dispatchtrack, poItem, requestType);
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public PaymentTrack GetPaymentTrack(int vendorID, int poID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            PaymentTrack paymenttrack = new PaymentTrack();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                sd.Add("P_VENDOR_ID", vendorID);
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetPaymentTrack", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    UserDetails Vendor = new UserDetails();
                    DataRow row = ds.Tables[0].Rows[0];
                    Vendor = POUtility.GetVendorPoObject(row);
                    paymenttrack.Vendor = Vendor;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    Requirement Req = new Requirement();
                    DataRow row1 = ds.Tables[1].Rows[0];
                    Req = POUtility.GetVendorPoReqObject(row1);
                    paymenttrack.Req = Req;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    POItems PO = new POItems();
                    DataRow row2 = ds.Tables[2].Rows[0];
                    PO = POUtility.GetPoObject(row2);
                    paymenttrack.PO = PO;
                }

                List<DispatchTrack> ListDispatchObject = new List<DispatchTrack>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[3].Rows.Count > 0 && ds.Tables[3].Rows[0][0] != null)
                {
                    foreach (DataRow row3 in ds.Tables[3].Rows)
                    {
                        DispatchTrack DispatchObject = new DispatchTrack();
                        DispatchObject = POUtility.GetPaymentTrackObject(row3);
                        ListDispatchObject.Add(DispatchObject);
                    }

                    paymenttrack.DispatchObject = ListDispatchObject;
                }

                List<PaymentInfo> ListPaymentInfo = new List<PaymentInfo>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[4].Rows.Count > 0 && ds.Tables[4].Rows[0][0] != null)
                {
                    foreach (DataRow row4 in ds.Tables[4].Rows)
                    {
                        PaymentInfo paymentinfo = new PaymentInfo();
                        paymentinfo = POUtility.GetPaymentInfoObject(row4);
                        ListPaymentInfo.Add(paymentinfo);
                    }

                    paymenttrack.PaymentInfoObject = ListPaymentInfo;
                }
            }
            catch (Exception ex)
            {
                paymenttrack.ErrorMessage = ex.Message;
            }

            return paymenttrack;
        }

        public Response SavePaymentInfo(PaymentInfo paymentinfo)
        {
            Utilities.ValidateSession(paymentinfo.SessionID);
            Response response = new Response();
            try
            {
                DataSet ds = POUtility.SavePaymentInfoObject(paymentinfo);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public bool CheckUniqueIfExists(string param, string idtype, string sessionID)
        {
            bool response = false;
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PARAM", param);
                sd.Add("P_ID_TYPE", idtype);
                DataSet ds = sqlHelper.SelectList("cp_CheckUniqueIfExists", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    int result = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (result > 0)
                    {
                        response = true;
                    }
                    else
                    {
                        response = false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return response;
        }
        public SAPOEntity[] GetRequirementPO(int compid, int reqid, string sessionid)
        {
            List<SAPOEntity> details = new List<SAPOEntity>();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $"SELECT * FROM POGenerateDetails WHERE COMP_ID = {compid} AND REQ_ID = {reqid}";
                CORE.DataNamesMapper<SAPOEntity> mapper = new CORE.DataNamesMapper<SAPOEntity>();
                var dataset = sqlHelper.ExecuteQuery(query);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }

            return details.ToArray();
        }

        public SAPOEntity[] GetPOItems(string ponumber, string quotno, string sessionid)
        {
            List<SAPOEntity> details = new List<SAPOEntity>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PO_NUMBER", ponumber);
                sd.Add("P_QUOT_NO", quotno);

                CORE.DataNamesMapper<SAPOEntity> mapper = new CORE.DataNamesMapper<SAPOEntity>();
                var dataset = sqlHelper.SelectList("PO_GetPOItems", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }

            return details.ToArray();
        }

        public List<POFieldMapping> GetFilterValues(int compID, string sessionID)
        {
            List<POFieldMapping> details = new List<POFieldMapping>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compID);
                CORE.DataNamesMapper<POFieldMapping> mapper = new CORE.DataNamesMapper<POFieldMapping>();
                var dataset = sqlHelper.SelectList("po_GetPOFilterValues", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }

            return details;
        }

        #endregion ITEM

        #region OPENPO

        public List<POScheduleDetails> GetPOScheduleList(int compid, string uid, int poid, string search, string categoryid, string productid, string supplier, string postatus, string deliverystatus, string plant,
             string fromdate, string todate, int page, int pagesize, string sessionid)
        {
            List<POScheduleDetails> details = new List<POScheduleDetails>();
            try
            {
                search = "%" + search + "%";
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_U_ID", uid);
                sd.Add("P_PO_ID", poid);
                sd.Add("P_SEARCH", search);
                sd.Add("P_CATEGORY_ID", categoryid);
                sd.Add("P_PRODUCT_ID", productid);
                sd.Add("P_PO_STATUS", postatus);
                sd.Add("P_DELIVERY_STATUS", deliverystatus);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                sd.Add("P_PAGE", page);
                sd.Add("P_PAGE_SIZE", pagesize);
                CORE.DataNamesMapper<POScheduleDetails> mapper = new CORE.DataNamesMapper<POScheduleDetails>();
                var dataset = sqlHelper.SelectList("po_GetPODetails", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                if (details != null && details.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    var row = dataset.Tables[0].Rows[0];                   
                }

                if (poid > 0 && details != null && details.Count > 0 && dataset.Tables[1].Rows.Count > 0)
                {
                    var row = dataset.Tables[1].Rows[0];
                    details[0].PR_NUMBER = row["PR_NUMBER"] != DBNull.Value ? Convert.ToString(row["PR_NUMBER"]) : "";
                    details[0].IS_BUDGETED = row["IS_BUDGETED"] != DBNull.Value ? Convert.ToString(row["IS_BUDGETED"]) : "";
                    details[0].BUDGET_ALLOCATED = row["BUDGET_ALLOCATED"] != DBNull.Value ? Convert.ToDecimal(row["BUDGET_ALLOCATED"]) : 0;
                    details[0].BUDGET_AVAILABLE = row["BUDGET_AVAILABLE"] != DBNull.Value ? Convert.ToDecimal(row["BUDGET_AVAILABLE"]) : 0;
                    details[0].BUDGET_USED = row["BUDGET_USED"] != DBNull.Value ? Convert.ToDecimal(row["BUDGET_USED"]) : 0;
                    details[0].TENTATIVE_VALUE = row["TENTATIVE_VALUE"] != DBNull.Value ? Convert.ToDecimal(row["TENTATIVE_VALUE"]) : 0;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<POScheduleDetailsItems> GetPOScheduleItems(int poid, string sessionid)
        {
            List<POScheduleDetailsItems> details = new List<POScheduleDetailsItems>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PO_ID", poid);
                CORE.DataNamesMapper<POScheduleDetailsItems> mapper = new CORE.DataNamesMapper<POScheduleDetailsItems>();
                var dataset = sqlHelper.SelectList("po_GetPOItems", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                if (dataset.Tables.Count > 1 && dataset.Tables[1].Rows.Count > 0)
                {
                    CORE.DataNamesMapper<GRNItem> grnMapper = new CORE.DataNamesMapper<GRNItem>();
                    List<GRNItem> grnList = grnMapper.Map(dataset.Tables[1]).ToList();
                    if (grnList != null && grnList.Count > 0)
                    {
                        foreach(var po in details)
                        {
                            po.GRNItems = new List<GRNItem>();
                            //po.GRNItems = grnList.Where(g => (g.PO_NUMBER == po.PO_NUMBER && g.PO_LINE_ITEM == po.PO_LINE_ITEM)).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<CArrayKeyValue> GetPOScheduleFilters(int compid, string sessionid)
        {
            List<CArrayKeyValue> details = new List<CArrayKeyValue>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                CORE.DataNamesMapper<POScheduleDetailsItems> mapper = new CORE.DataNamesMapper<POScheduleDetailsItems>();
                var dataset = sqlHelper.SelectList("po_GetPOFilters", sd);

                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "CATEGORY";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[0].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key = row["CATEGORY_ID"] != DBNull.Value ? Convert.ToInt32(row["CATEGORY_ID"]) : 0;
                        keyValuePair.Value = row["CategoryCode"] != DBNull.Value ? Convert.ToString(row["CategoryCode"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 1 && dataset.Tables[1].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "PRODUCT";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[1].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key = row["PRODUCT_ID"] != DBNull.Value ? Convert.ToInt32(row["PRODUCT_ID"]) : 0;
                        keyValuePair.Key1 = row["ProductCode"] != DBNull.Value ? Convert.ToString(row["ProductCode"]) : "";
                        keyValuePair.Value = row["ProductName"] != DBNull.Value ? Convert.ToString(row["ProductName"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 2 && dataset.Tables[2].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "VENDORS";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[2].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key1 = row["VENDOR_COMPANY"] != DBNull.Value ? Convert.ToString(row["VENDOR_COMPANY"]) : "";
                        keyValuePair.Value = row["VENDOR_COMPANY"] != DBNull.Value ? Convert.ToString(row["VENDOR_COMPANY"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 3 && dataset.Tables[3].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "PO_STATUS";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[3].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key1 = row["PO_STATUS"] != DBNull.Value ? Convert.ToString(row["PO_STATUS"]) : "";
                        keyValuePair.Value = row["PO_STATUS"] != DBNull.Value ? Convert.ToString(row["PO_STATUS"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 4 && dataset.Tables[4].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "PO_CREATOR";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[4].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key1 = row["PO_CREATOR"] != DBNull.Value ? Convert.ToString(row["PO_CREATOR"]) : "";
                        keyValuePair.Value = row["PO_CREATOR"] != DBNull.Value ? Convert.ToString(row["PO_CREATOR"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<GRNDetails> GetGRNDetailsList(int compid, string uid, string search, string supplier, string fromdate, string todate, int page, int pagesize, string sessionid)
        {
            List<GRNDetails> details = new List<GRNDetails>();
            try
            {
                search = "%" + search + "%";
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_U_ID", uid);
                sd.Add("P_SEARCH", search);
                sd.Add("P_SUPPLIER", supplier);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                sd.Add("P_PAGE", page);
                sd.Add("P_PAGE_SIZE", pagesize);
                CORE.DataNamesMapper<GRNDetails> mapper = new CORE.DataNamesMapper<GRNDetails>();
                var dataset = sqlHelper.SelectList("po_GetGRNDetailsList", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response SavePOVendorAck(string ponumber, string poitemline, string status, string vendordeliverydate, string vendordeliverydateString, string comments, int user,int isVendPoAck, List<FileUpload> vendorAttachments, List<string> ExistingVendorAttachments, string sessionid)
        {
            Response detail = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                string VENDOR_ATTACHEMNTS = "";
                if (vendorAttachments != null && !string.IsNullOrEmpty(ponumber))
                {
                    if (vendorAttachments.Count > 0)
                    {
                        string fileName = string.Empty;
                        foreach (FileUpload fd in vendorAttachments)
                        {
                            if (fd.FileStream != null && !string.IsNullOrWhiteSpace(fd.FileName))
                            {
                                fd.FileName = Regex.Replace(fd.FileName, @"[^0-9a-zA-Z.]+", "PRM_");
                                if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                                {
                                    var attachName = string.Empty;
                                    long tick = DateTime.UtcNow.Ticks;
                                    attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PO_" + tick + "_PONUMBER_" + ponumber + "_" + fd.FileName);
                                    SaveFile(attachName, fd.FileStream);
                                    attachName = "PO_" + tick + "_PONUMBER_" + ponumber + "_" + fd.FileName;

                                    Response res = SaveAttachment(attachName);
                                    if (res.ErrorMessage != "")
                                    {
                                        detail.ErrorMessage = res.ErrorMessage;
                                    }

                                    fd.FileID = res.ObjectID;
                                }
                            }
                            fileName += Convert.ToString(fd.FileID) + ",";

                        }
                        fileName = fileName.Substring(0, fileName.Length - 1);
                        VENDOR_ATTACHEMNTS = fileName;
                    }
                }
                if (ExistingVendorAttachments.Count > 0)
                {
                    VENDOR_ATTACHEMNTS = !string.IsNullOrEmpty(VENDOR_ATTACHEMNTS) ?  VENDOR_ATTACHEMNTS + "," : VENDOR_ATTACHEMNTS;
                    foreach (string attachID in ExistingVendorAttachments)
                    {
                        if (!string.IsNullOrEmpty(attachID))
                        {
                            VENDOR_ATTACHEMNTS += Convert.ToString(attachID) + ",";
                        }
                    }
                }
                if (!string.IsNullOrEmpty(VENDOR_ATTACHEMNTS))
                {
                    VENDOR_ATTACHEMNTS = VENDOR_ATTACHEMNTS.Substring(0, VENDOR_ATTACHEMNTS.Length - 1);
                }

                //VENDOR_ATTACHEMNTS = fileName;
                DateTime? vendordeliverydateTemp = null;
                //vendordeliverydateTemp = !string.IsNullOrEmpty(vendordeliverydate) ? Convert.ToDateTime(vendordeliverydate, CultureInfo.InvariantCulture) : null;
                if (!string.IsNullOrEmpty(vendordeliverydate)) {
                    vendordeliverydateTemp = Convert.ToDateTime(vendordeliverydate, CultureInfo.InvariantCulture);
                } 

                string query = $@"UPDATE POScheduleDetails SET VENDOR_ACK_STATUS = '{status}', VENDOR_ACK_COMMENTS = '{comments}', VENDOR_EXPECTED_DELIVERY_DATE = '{vendordeliverydateTemp}',VENDOR_EXPECTED_DELIVERY_DATE_STRING = '{vendordeliverydateString}',  DATE_MODIFIED = utc_timestamp, MODIFIED_BY = {user}, 
                                VENDOR_ATTACHEMNTS = '{VENDOR_ATTACHEMNTS}',IS_PO_ACK = {isVendPoAck} WHERE PO_NUMBER = '{ponumber}' AND PO_LINE_ITEM = '{poitemline}'";
                if (string.IsNullOrEmpty(poitemline))
                {
                    query = $@"UPDATE POScheduleDetails SET VENDOR_ACK_STATUS = '{status}', VENDOR_ACK_COMMENTS = '{comments}', VENDOR_EXPECTED_DELIVERY_DATE = '{vendordeliverydateTemp}',VENDOR_EXPECTED_DELIVERY_DATE_STRING = '{vendordeliverydateString}',  DATE_MODIFIED = utc_timestamp, MODIFIED_BY = {user},
                                VENDOR_ATTACHEMNTS = '{VENDOR_ATTACHEMNTS}',IS_PO_ACK = {isVendPoAck} WHERE PO_NUMBER = '{ponumber}'";
                }
                logger.Debug("query >>>>>" + query);
                sqlHelper.ExecuteNonQuery_IUD(query);
                detail.ObjectID = 1;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return detail;
        }

        public Response SavePOVendorQuantityAck(string ponumber, string poitemline, string status, string comments, int isVendPoAck, decimal quantity, int user, string sessionid)
        {
            Response detail = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $@"UPDATE POScheduleDetails SET VENDOR_ACK_STATUS = '{status}', VENDOR_ACK_COMMENTS = '{comments}', IS_PO_ACK = {isVendPoAck}, ACK_QTY = ORDER_QTY, ACK_DATE = utc_timestamp
                            DATE_MODIFIED = utc_timestamp, MODIFIED_BY = {user}, WHERE PO_NUMBER = '{ponumber}'";
                if (!string.IsNullOrEmpty(poitemline))
                {
                    query = $@"UPDATE POScheduleDetails SET VENDOR_ACK_STATUS = '{status}', VENDOR_ACK_COMMENTS = '{comments}', IS_PO_ACK = {isVendPoAck}, ACK_QTY = {quantity}, ACK_DATE = utc_timestamp
                            DATE_MODIFIED = utc_timestamp, MODIFIED_BY = {user}, WHERE PO_NUMBER = '{ponumber}' AND PO_LINE_ITEM = '{poitemline}'";
                }
                logger.Debug("query >>>>>" + query);
                sqlHelper.ExecuteNonQuery_IUD(query);
                detail.ObjectID = 1;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return detail;
        }
        public Response SendPOEmails(string sessionid)
        {
            Response response = new Response();

            try
            {
                List<POScheduleDetails> details = new List<POScheduleDetails>();
                //Utilities.ValidateSession(sessionID);
                string query = $@"SELECT PO_NUMBER, VENDOR_COMPANY, V.U_EMAIL, PO.DATE_CREATED, PO.DATE_MODIFIED, EMAIL_SENT_DATE FROM POScheduleDetails PO 
                                    INNER JOIN vendors V ON V.U_ID = PO.VENDOR_ID AND IS_PRIMARY = 1
									WHERE CONVERT(varchar, PO.DATE_MODIFIED, 102) = CONVERT(varchar, GETUTCDATE(), 102)
                                    GROUP BY PO_NUMBER, VENDOR_COMPANY, V.U_EMAIL, PO.DATE_CREATED, PO.DATE_MODIFIED, EMAIL_SENT_DATE;";

                CORE.DataNamesMapper<POScheduleDetails> mapper = new CORE.DataNamesMapper<POScheduleDetails>();
                var dataset = sqlHelper.ExecuteQuery(query);
                if (dataset != null && dataset.Tables.Count > 0)
                {
                    details = mapper.Map(dataset.Tables[0]).ToList();
                }

                if (details != null && details.Count > 0)
                {
                    Utilities utilities = new Utilities();
                    string siteLink = ConfigurationManager.AppSettings["SITE_LINK"].ToString().Replace("prm360.html", "list-pendingPOOverall/");
                    string companyName = ConfigurationManager.AppSettings["COMPANY_NAME"].ToString();
                    List<string> emailSentPOs = new List<string>();
                    string newPOEmailBody = $@"Dear VENDOR_NAME, <br/><br/>A new Purchase Order No. PO_NUMBER has been approved and released. Please login to vendor portal to review more details. <br/><br/> Link: PO_LINK 
                                            <br/><br/>Kindly acknowledge the receipt of the same in the portal. <br/><br/> Thanks<br/> {companyName}";
                    string updatePOEmailBody = $@"Dear VENDOR_NAME, <br/><br/>We have a new update on Purchase Order No. PO_NUMBER Kindly review the updates in your login. Please login to vendor portal to review more details. <br/><br/> Link: PO_LINK 
                                            <br/><br/>Kindly acknowledge the receipt of the same in the portal. <br/><br/> Thanks<br/> {companyName}";
                    foreach (var detail in details)
                    {
                        try
                        {
                            if (!string.IsNullOrWhiteSpace(detail.VENDOR_PRIMARY_EMAIL))
                            {
                                if (detail.DATE_CREATED.HasValue && detail.DATE_MODIFIED.HasValue && detail.DATE_CREATED.Value == detail.DATE_MODIFIED.Value && (!detail.EMAIL_SENT_DATE.HasValue || detail.EMAIL_SENT_DATE.Value == null))
                                {
                                    newPOEmailBody = newPOEmailBody.Replace("VENDOR_NAME", detail.VENDOR_COMPANY);
                                    newPOEmailBody = newPOEmailBody.Replace("PO_NUMBER", detail.PO_NUMBER);
                                    newPOEmailBody = newPOEmailBody.Replace("PO_LINK", siteLink + detail.PO_NUMBER);
                                    utilities.SendSMTPEmail(detail.VENDOR_PRIMARY_EMAIL, $"Purchase Order No. : {detail.PO_NUMBER}", newPOEmailBody);
                                    if (!emailSentPOs.Contains(detail.PO_NUMBER))
                                    {
                                        emailSentPOs.Add(detail.PO_NUMBER);
                                    }
                                }
                                else if (detail.DATE_CREATED.HasValue && detail.DATE_MODIFIED.HasValue && detail.DATE_CREATED.Value != detail.DATE_MODIFIED.Value && detail.EMAIL_SENT_DATE.HasValue && detail.EMAIL_SENT_DATE.Value != null &
                                    detail.DATE_MODIFIED.Value > detail.EMAIL_SENT_DATE.Value)
                                {
                                    updatePOEmailBody = updatePOEmailBody.Replace("VENDOR_NAME", detail.VENDOR_COMPANY);
                                    updatePOEmailBody = updatePOEmailBody.Replace("PO_NUMBER", detail.PO_NUMBER);
                                    updatePOEmailBody = updatePOEmailBody.Replace("PO_LINK", siteLink + detail.PO_NUMBER);
                                    utilities.SendSMTPEmail(detail.VENDOR_PRIMARY_EMAIL, $"Update Notification: Purchase Order : {detail.PO_NUMBER}", updatePOEmailBody);
                                    if (!emailSentPOs.Contains(detail.PO_NUMBER))
                                    {
                                        emailSentPOs.Add(detail.PO_NUMBER);
                                    }
                                }
                            }
                        }
                        catch (Exception ex1)
                        {

                        }
                    }

                    if (emailSentPOs != null && emailSentPOs.Count > 0)
                    {
                        query = $@"UPDATE POScheduleDetails SET EMAIL_SENT_DATE = GETUTCDATE() WHERE PO_NUMBER IN ({string.Join(",", emailSentPOs.Select(e => "'" + e + "'"))})";
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion


        #region ASN

        public List<ASNDetails> GetASNDetails(int compid, int asnid, string asncode, string ponumber, string grncode, int vendorid, string sessionid)
        {
            List<ASNDetails> details = new List<ASNDetails>();
            try
            {
                Utilities.ValidateSession(sessionid);
                asncode = asncode == "0" ? string.Empty : asncode;
                ponumber = ponumber == "0" ? string.Empty : ponumber;
                grncode = grncode == "0" ? string.Empty : grncode;
                string query = string.Empty;
                query = $"SELECT * FROM ASNDetails WHERE ";
                bool conditions = false;
                if (vendorid > 0)
                {
                    query += $" {(conditions? " AND " : "")} VENDOR_ID = {vendorid}";
                    conditions = true;
                }
                if (asnid > 0)
                {
                    query += $" {(conditions ? " AND " : "")} ASN_ID = {asnid}";
                    conditions = true;
                }

                if (!string.IsNullOrWhiteSpace(asncode))
                {
                    query += $" {(conditions ? " AND " : "")} ASN_CODE = '{asncode}'";
                    conditions = true;
                }

                if (!string.IsNullOrWhiteSpace(ponumber))
                {
                    query += $" {(conditions ? " AND " : "")} PO_NUMBER = '{ponumber}'";
                    conditions = true;
                }

                if (!string.IsNullOrWhiteSpace(grncode))
                {
                    query += $" {(conditions ? " AND " : "")} GRN_CODE = '{grncode}'";
                    conditions = true;
                }

                if (compid > 0)
                {
                    query += $" {(conditions ? " AND " : "")} PO_NUMBER IN (SELECT PO_NUMBER FROM POScheduleDetails WHERE COMP_ID = {compid})";
                    conditions = true;
                }

                query += $" ORDER BY DATE_MODIFIED DESC";

                CORE.DataNamesMapper<ASNDetails> mapper = new CORE.DataNamesMapper<ASNDetails>();
                var dataTable = sqlHelper.SelectQuery(query);
                details = mapper.Map(dataTable).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response SaveASNDetails(ASNDetails details)
        {
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(details.SessionID);

                if (details != null)
                {
                    string query = string.Empty;
                    if (details.ASN_ID > 0)
                    {
                        query = $@"UPDATE [dbo].[ASNDetails] SET ASN_TYPE = '{details.ASN_TYPE}', 
                                  DELIVERY_DATE = '{details.DELIVERY_DATE?.ToString("yyyy-MM-dd")}', SHIPMENT_DATE = '{details.SHIPMENT_DATE?.ToString("yyyy-MM-dd")}', 
                                  DOCUMENT_DATE = '{details.DOCUMENT_DATE?.ToString("yyyy-MM-dd")}', CUSTOMER_BATCH = '{details.CUSTOMER_BATCH}', 
                                  SHIP_FROM_LOCATION = '{details.SHIP_FROM_LOCATION}', CUSTOMER_LOCATION = '{details.CUSTOMER_LOCATION}', 
                                  SHIP_TO_LOCATION = '{details.SHIP_TO_LOCATION}', UNLOADING_POINT = '{details.UNLOADING_POINT}', SHIPPED_THROUGH = '{details.SHIPPED_THROUGH}', 
                                   MANUFACTURED_DATE = '{details.MANUFACTURED_DATE?.ToString("yyyy-MM-dd")}', BEST_BEFORE_DATE = '{details.BEST_BEFORE_DATE?.ToString("yyyy-MM-dd")}', 
                                  TOTAL_WEIGHT_DETAILS = '{details.TOTAL_WEIGHT_DETAILS}',TOTAL_VOLUME_DETAILS = '{details.TOTAL_VOLUME_DETAILS}', FREIGHT_INVOICE_NO = '{details.FREIGHT_INVOICE_NO}', 
                                  FREIGHT_TOTAL_INVOICE_AMOUNT = '{details.FREIGHT_TOTAL_INVOICE_AMOUNT}', FREIGHT_TAX = '{details.FREIGHT_TAX}', 
                                  INVOICE_NUMBER = '{details.INVOICE_NUMBER}', INVOICE_AMOUNT = '{details.INVOICE_AMOUNT}', SERVICE_CODE = '{details.SERVICE_CODE}', 
                                  SERVICE_COMPLETION_DATE = '{details.SERVICE_COMPLETION_DATE?.ToString("yyyy-MM-dd")}', SERVICE_COMPLETED_DATE = '{details.SERVICE_COMPLETED_DATE?.ToString("yyyy-MM-dd")}', 
                                  SERVICE_DOCUMENT_DATE = '{details.SERVICE_DOCUMENT_DATE?.ToString("yyyy-MM-dd")}', 
                                  SERVICE_CUSTOMER_LOCATION = '{details.SERVICE_CUSTOMER_LOCATION}', SERVICE_LOCATION = '{details.SERVICE_LOCATION}', SERVICE_BY = '{details.SERVICE_BY}', COMMENTS = '{details.COMMENTS}', 
                                  RECEIVED_CODE = '{details.RECEIVED_CODE}', 
                                  RECEIVED_BY = '{details.RECEIVED_BY}', RECEIVED_COMMENTS = '{details.RECEIVED_COMMENTS}', RECEIVED_DATE = '{details.RECEIVED_DATE?.ToString("yyyy-MM-dd")}',
                                 DATE_MODIFIED = '{DateTime.UtcNow.ToString("yyyy-MM-dd")}', MODIFIED_BY = {details.MODIFIED_BY} WHERE ASN_ID = {details.ASN_ID}";

                        sqlHelper.ExecuteNonQuery_IUD(query);
                    }
                    else
                    {
                        query = @"INSERT INTO [dbo].[ASNDetails]";

                        query += $@"SELECT {details.COMP_ID}, '{details.PO_NUMBER}', '{details.GRN_CODE}', '{details.VENDOR_CODE}', {details.VENDOR_ID}, 'ASN{DateTime.Now.Ticks}', '{details.ASN_TYPE}', 
                                  '{details.DELIVERY_DATE?.ToString("yyyy-MM-dd")}', '{details.SHIPMENT_DATE?.ToString("yyyy-MM-dd")}', 
                                  '{details.DOCUMENT_DATE?.ToString("yyyy-MM-dd")}', '{details.CUSTOMER_BATCH}', '{details.SHIP_FROM_LOCATION}', '{details.CUSTOMER_LOCATION}', 
                                  '{details.SHIP_TO_LOCATION}', '{details.UNLOADING_POINT}', '{details.SHIPPED_THROUGH}', 
                                   '{details.MANUFACTURED_DATE?.ToString("yyyy-MM-dd")}', '{details.BEST_BEFORE_DATE?.ToString("yyyy-MM-dd")}', 
                                  '{details.TOTAL_WEIGHT_DETAILS}', '{details.TOTAL_VOLUME_DETAILS}', '{details.FREIGHT_INVOICE_NO}', '{details.FREIGHT_TOTAL_INVOICE_AMOUNT}', '{details.FREIGHT_TAX}', 
                                   '{details.INVOICE_NUMBER}', '{details.INVOICE_AMOUNT}', '{details.SERVICE_CODE}', 
                                  '{details.SERVICE_COMPLETION_DATE?.ToString("yyyy-MM-dd")}', '{details.SERVICE_COMPLETED_DATE?.ToString("yyyy-MM-dd")}', '{details.SERVICE_DOCUMENT_DATE?.ToString("yyyy-MM-dd")}', 
                                   '{details.SERVICE_CUSTOMER_LOCATION}', '{details.SERVICE_LOCATION}', '{details.SERVICE_BY}', '{details.COMMENTS}', '{details.RECEIVED_CODE}', 
                                  '{details.RECEIVED_BY}', '{details.RECEIVED_COMMENTS}', '{details.RECEIVED_DATE?.ToString("yyyy-MM-dd")}', '{DateTime.UtcNow.ToString("yyyy-MM-dd")}', '{DateTime.UtcNow.ToString("yyyy-MM-dd")}', {details.CREATED_BY}, {details.MODIFIED_BY}";

                        sqlHelper.ExecuteNonQuery_IUD(query);
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.ObjectID = -1;
            }

            return response;
        }
        #endregion


        #region Private

        private void SaveFile(string fileName, byte[] fileContent)
        {
            var allowedExtns = ConfigurationManager.AppSettings["SUPPORTED.FILE.EXT"].ToString().Split(',').ToList();
            var isValid = allowedExtns.Any(e => fileName.ToLower().EndsWith(e));
            if (isValid)
            {
                Utilities.SaveFile(fileName, fileContent);
            }
            else
            {
                //logger.Warn("Unsupported file uploaded: " + fileName);
            }
        }

        private Response SaveAttachment(string path)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PATH", path);
                DataSet ds = sqlHelper.SelectList("cp_SaveAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        private string GenerateDesPO(POVendor povendor, Requirement req, Requirement reqVendor, UserDetails vendor, UserDetails customer)
        {
            string taxRows = string.Empty;
            double totalPrice = Convert.ToDouble(povendor.Price);
            foreach (RequirementTaxes tax in reqVendor.ListRequirementTaxes)
            {
                string tableRows = "<tr>";
                tableRows += "<td colspan=1>" + tax.TaxName + "</td>";
                tableRows += "<td>" + tax.TaxPercentage + "%</td>";
                tableRows += "</tr>";
                taxRows += tableRows;                
            }

            string html = string.Empty;
            html = "DescPO.html";

            string filestring = File.ReadAllText(HttpContext.Current.Server.MapPath(Utilities.FILE_URL + html));
            string htmlRows = string.Empty;
            try
            {
                htmlRows = String.Format(filestring,
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString(),
                customer.Address.ToString(),
                customer.PhoneNum.ToString(),
                customer.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                povendor.PurchaseID.ToString(),
                vendor.CompanyName.ToString(), // 7
                vendor.Address.ToString(),
                vendor.PhoneNum.ToString(),
                vendor.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                req.Title.ToString(),
                req.Description.ToString(), // 13
                reqVendor.AuctionVendors[0].RevPrice.ToString(),
                taxRows,
                0,
                totalPrice.ToString(),
                povendor.ExpectedDeliveryDate.ToString(),
                povendor.DeliveryAddress.ToString(),
                povendor.Comments.ToString(),
                customer.FirstName.ToString() + " " + customer.LastName.ToString()
               );
            }
            catch
            {

            }

            return htmlRows;
        }

        
        #endregion
    }
    
}
 