﻿prmApp

    .controller('materialsPurchaseIndentCtrl', function ($scope, $http, $state, domain, $filter, $window, $log, $stateParams, $timeout, auctionsService, fwdauctionsService, userService, SignalRFactory, fileReader, growlService, workflowService, $rootScope, cijIndentService, rfqService) {

        $scope.sessionID = userService.getUserToken();
        $scope.currentID = userService.getUserId();
        $scope.userdetails = userService.getUserId();
        $scope.date = new Date();
        $scope.isCapex = 0;
        $scope.isError = false;
        $scope.multiFileUpload = [];

        $scope.cijID = 0;

        if ($stateParams.indentID) {
            $scope.indentID = $stateParams.indentID;
        } else {
            $scope.indentID = 0;
        };

        $scope.isEdit = false;

        $scope.isEditFunction = function (val) {
            $scope.isEdit = val;
        };

        $scope.EditCijIndentDetails = function (val, type) {
            var params = {
                id: $scope.indentID,
                type: type,
                val: val,
                sessionID: $scope.sessionID
            }

            cijIndentService.EditCijIndentDetails(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Saved Successfully.", "success");
                    }
                })
        };

        $scope.deptType = 0;

        $scope.isEditDisable = false;
        $scope.isSaveDisable = false;

        $scope.isItemsRejected = false;

        $scope.isReject = false;

        $scope.date = new Date();

        $scope.date = new moment($scope.date).format("DD-MM-YYYY");

        $scope.myDeptUser = [];
        $scope.BudgetDept = [];
        $scope.PurchaseDept = [];
        $scope.PurchaseAssignedUsers = [];
        $scope.currentDept = [];

        $scope.isPurchaseHod = false;

        $scope.seriesDB = 0;
        $scope.seriesDBDeptCode = '';

        $scope.BudgetCodes = [];

        $scope.workflowList = [];
        $scope.itemWorkflow = [];
        $scope.workflowObj = {};
        $scope.workflowObj.workflowID = 0;
        $scope.currentStep = 0;
        $scope.orderInfo = 0;
        $scope.assignToShow = '';

        $scope.GetBudgetCodes = function () {
            auctionsService.GetBudgetCodes($scope.currentID, $scope.sessionID)
                .then(function (response) {
                    $scope.BudgetCodes = response;
                    //if ($scope.deptType == 6 || $scope.deptType == 5) {
                    //    $scope.BudgetCodes = $filter('filter')($scope.BudgetCodes, function (item) {
                    //        return item.deptID === $scope.deptID;
                    //    })
                    //}

                    if ($rootScope.rootDetails.deptTypeID == 6 || $rootScope.rootDetails.deptTypeID == 5) {
                        $scope.BudgetCodes = $filter('filter')($scope.BudgetCodes, function (item) {
                            return item.deptID === $rootScope.rootDetails.deptId;
                        })
                    }
                })
        }

        $scope.GetBudgetCodes();

        $scope.GetSeries = function (series, deptID) {

            cijIndentService.GetSeries(series, 'INDENT', deptID)
                .then(function (response) {
                    $scope.seriesDB = response.objectID;
                    $scope.seriesDBDeptCode = response.message;

                    if ($scope.seriesDB <= 0) {
                        $scope.seriesDB = 1;
                    }

                    if ($scope.indentDetails.indNo == '' || $scope.indentDetails.indNo == null || $scope.indentDetails.indNo == undefined) {

                        if ($scope.seriesDB < 10) {
                            $scope.indentDetails.indNo = 'KIMS/' + $scope.seriesDBDeptCode + '/INDENT/000000000' + $scope.seriesDB;
                        }
                        if ($scope.seriesDB > 9 && $scope.seriesDB < 100) {
                            $scope.indentDetails.indNo = 'KIMS/' + $scope.seriesDBDeptCode + '/INDENT/00000000' + $scope.seriesDB;
                        }
                        if ($scope.seriesDB > 99 && $scope.seriesDB < 1000) {
                            $scope.indentDetails.indNo = 'KIMS/' + $scope.seriesDBDeptCode + '/INDENT/0000000' + $scope.seriesDB;
                        }
                        if ($scope.seriesDB > 999 && $scope.seriesDB < 10000) {
                            $scope.indentDetails.indNo = 'KIMS/' + $scope.seriesDBDeptCode + '/INDENT/000000' + $scope.seriesDB;
                        }

                        if ($scope.seriesDB > 9999 && $scope.seriesDB < 100000) {
                            $scope.indentDetails.indNo = 'KIMS/' + $scope.seriesDBDeptCode + '/INDENT/00000' + $scope.seriesDB;
                        }
                        if ($scope.seriesDB > 99999 && $scope.seriesDB < 1000000) {
                            $scope.indentDetails.indNo = 'KIMS/' + $scope.seriesDBDeptCode + '/INDENT/0000' + $scope.seriesDB;
                        }

                        if ($scope.seriesDB > 999999 && $scope.seriesDB < 10000000) {
                            $scope.indentDetails.indNo = 'KIMS/' + $scope.seriesDBDeptCode + '/INDENT/000' + $scope.seriesDB;
                        }
                        if ($scope.seriesDB > 9999999 && $scope.seriesDB < 100000000) {
                            $scope.indentDetails.indNo = 'KIMS/' + $scope.seriesDBDeptCode + '/INDENT/00' + $scope.seriesDB;
                        }
                        if ($scope.seriesDB > 99999999 && $scope.seriesDB < 100000000) {
                            $scope.indentDetails.indNo = 'KIMS/' + $scope.seriesDBDeptCode + '/INDENT/0' + $scope.seriesDB;
                        }
                    }

                })
        }


        $scope.indentID = 0;

        $scope.indentDetails = {
            cij: {},
            listDeleteItems: [],
            isMedical: 1,
            requestDate: $scope.date,
            isValid: 1,
            userPriority: 'MEDIUM'
        };

        $scope.indentDetails.cij.cijID = 0;

        $scope.indentDetails.listIndentItems = [];
        $scope.indentDetails.listDeleteItems = [];
        $scope.indentDetails.userDetailsList = [];

        $scope.item = {
            sNO: 1,
            materialDescription: '',
            modelDescription: '',
            existFunctional: 0,
            existNonFunctional: 0,
            requiredQuantity: 0,
            units: '',
            purposeOfMaterial: '',
            IndentItemID: 0,
            fileName: {
                fileStream: [],
                fileName: ''
            }
        };


        $scope.uploadFile =
            {
                Link: 0,
                LinkName: ''
            }


        $scope.indentDetails.listIndentItems.push($scope.item);

        $scope.compID = userService.getUserCompanyId();


        if ($stateParams.indentID) {
            $scope.indentID = $stateParams.indentID;
        } else {
            $scope.indentID = 0;
        };

        if ($scope.indentID > 0) {
            $scope.isDisable = true;
        }
        else {
            $scope.isDisable = false;

            $scope.GetSeries('', $rootScope.rootDetails.deptId);
        };
        if ($stateParams.indentObj && $stateParams.indentObj.cloneId > 0) {
            $scope.GetSeries('', $scope.deptID);
        }

        $scope.GetRFQCIJList = function () {
            rfqService.GetRFQCIJList(userService.getUserId(), $scope.sessionID)
                .then(function (response) {
                    $scope.cijList = response;

                    $scope.cijList.forEach(function (item, index) {
                        var cijObj = $.parseJSON(item.cij);
                        item.cij = cijObj;
                    });
                })
        }

        $scope.GetRFQCIJList();

        $scope.GetCIJ = function (val) {
            cijIndentService.GetCIJ(val, userService.getUserToken())
                .then(function (response) {
                    $scope.cijString = response;

                    var cijObj = $.parseJSON($scope.cijString.cij);

                    $scope.cij = cijObj;

                    $scope.indentDetails.approximateCostRange = $scope.cij.approximateCostRange;
                    $scope.indentDetails.department = $scope.cij.departmentName;
                    $scope.indentDetails.deptID = $scope.cij.deptID;
                    $scope.indentDetails.userPriority = $scope.cijString.userPriority;
                    $scope.indentDetails.expectedDelivery = $scope.cijString.expectedDelivery;
                    $scope.indentDetails.userStatus = $scope.cijString.userStatus;

                    if ($scope.cij.assetType > 0) {
                        $scope.indentDetails.isMedical = 0;
                    }
                    else if ($scope.cij.assetType <= 0) {
                        $scope.indentDetails.isMedical = 1;
                    }


                    //$scope.indentDetails.approvedBy = $scope.cij.assessedAndApprovedBy.name;
                    //$scope.indentDetails.purchaseIncharge = $scope.cij.purchaseDepartmentUse.signature.Text;

                })
        };
        //$scope.GetCIJ();

        $scope.uploadFile = { linkID: 0, linkName: '' };

        $scope.deleteAttachment = function (attchment) {
            $scope.multiFileUpload = _.filter($scope.multiFileUpload, function (file) {
                return file.linkID !== attchment.linkID;
            });

            $scope.indentDetails.attachments = JSON.stringify($scope.multiFileUpload);
        };

        $scope.saveFileUpload = function (id, fileName, itemid) {

            var params = {
                fileupload: $scope.fileUpload
            };

            cijIndentService.saveFileUpload(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        //growlService.growl(response.errorMessage, "inverse");
                    }
                    else {

                        if (id == 'purposequotation') {
                            $scope.cij.purpose.Link = response.objectID;
                        }
                        else if (id == "MPIndentAttachment") {
                            $scope.uploadFile.linkID = response.objectID;
                            $scope.uploadFile.linkName = fileName;
                            $scope.multiFileUpload.push($scope.uploadFile);
                            $scope.uploadFile = { linkID: 0, linkName: '' };

                            $scope.indentDetails.attachments = JSON.stringify($scope.multiFileUpload);
                        }
                        else if (id == "itemFileUploadquotation") {
                            $scope.indentDetails.listIndentItems[itemid].fileName.fileName = response.objectID;

                            //$scope.item.itemFileUpload.Link = response.objectID;
                        }
                    }
                })
        }

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {


                    $scope.fileUpload = {
                        fileStream: [],
                        fileName: ''
                    };


                    if (id == "purposequotation") {
                        var bytearray = new Uint8Array(result);
                        $scope.fileUpload.fileStream = $.makeArray(bytearray);
                        $scope.fileUpload.fileName = $scope.file.name;
                        $scope.cij.purpose.LinkName = $scope.file.name;
                        $scope.saveFileUpload(id);
                    }
                    else if (id == 'MPIndentAttachment') {
                        var bytearray = new Uint8Array(result);
                        $scope.fileUpload.fileStream = $.makeArray(bytearray);
                        $scope.fileUpload.fileName = $scope.file.name;
                        $scope.saveFileUpload(id, $scope.file.name);
                    }
                    else if (id == "itemFileUploadquotation") {
                        var bytearray = new Uint8Array(result);
                        $scope.fileUpload.fileStream = $.makeArray(bytearray);
                        $scope.fileUpload.fileName = $scope.file.name;
                        $scope.indentDetails.listIndentItems[itemid].fileName.fileStream = $.makeArray(bytearray);
                        $scope.indentDetails.listIndentItems[itemid].fileName.fileName = $scope.file.name;
                        $scope.saveFileUpload(id, $scope.file.name, itemid);
                    }
                });

        }


        $scope.GetCijItems = function (val) {
            if (!$stateParams.indentObj) {
                cijIndentService.GetCijItems(val, userService.getUserToken())
                    .then(function (response) {

                        $scope.indentDetails.listIndentItems = response;
                        var i = 0;


                        if ($scope.indentDetails.listIndentItems.length > 0) {
                            $scope.indentDetails.listIndentItems.forEach(function (item, index) {
                                i = i + 1;
                                item.sNO = i;
                            });
                            //console.log('$scope.indentDetails.listIndentItems');
                            //console.log($scope.indentDetails.listIndentItems);
                            //console.log('$scope.indentDetails.listIndentItems'); 
                        } else {
                            $scope.indentDetails.listDeleteItems = [];
                            $scope.indentDetails.listIndentItems = [];
                            $scope.indentDetails.listIndentItems.push($scope.item);
                            //console.log('$scope.indentDetails.listIndentItems');  
                            //console.log($scope.indentDetails.listIndentItems);  
                            //console.log('$scope.indentDetails.listIndentItems');  
                        }


                    })
            }
        };

        if ($scope.indentDetails.requestDate == '') {
            $scope.indentDetails.requestDate == $scope.date;
        }

        $scope.GetIndentDetails = function (indID) {
            cijIndentService.GetIndentDetails(indID, $scope.sessionID)
                .then(function (response) {
                    $scope.indentDetails = response;


                    $scope.isCapex = $scope.indentDetails.isCapex;


                    if ($stateParams.indentObj && $stateParams.indentObj.cloneId > 0) {
                        if ($scope.indentDetails.listIndentItems && $scope.indentDetails.listIndentItems.length > 0) {


                            $scope.indentDetails.listIndentItems = _.filter($scope.indentDetails.listIndentItems, function (item) {
                                return item.rejectedQuantity > 0;
                            });


                            $scope.indentDetails.indNo = '';
                            $scope.indentDetails.requestDate == $scope.date;
                            $scope.indentDetails.indentID == 0;
                            $scope.indentID = 0;
                            $scope.indentDetails.cloneIndID = $stateParams.indentObj.cloneId;
                            //$scope.GetSeries();

                            $scope.indentDetails.listIndentItems.forEach(function (item, index) {
                                if (item.rejectedQuantity > 0) {
                                    item.indentItemID = 0;
                                    item.requiredQuantity = item.rejectedQuantity;
                                }
                            });
                        }
                    }



                    $scope.indentDetails.listIndentItems.forEach(function (item, index) {
                        if (item.rejectedQuantity > 0) {
                            $scope.isItemsRejected = true;
                        }
                    });




                    $scope.multiFileUpload = ($scope.indentDetails.indID > 0 && $scope.indentDetails.attachments != "") ? $.parseJSON($scope.indentDetails.attachments) : [];
                    //$scope.listIndentItems = $scope.indentDetails.listIndentItems;

                    $scope.indentDetails.listDeleteItems = [];

                    var i = 0;

                    if ($scope.indentDetails.listIndentItems && $scope.indentDetails.listIndentItems.length > 0) {
                        $scope.indentDetails.listIndentItems.forEach(function (item, index) {
                            i = i + 1;
                            item.sNO = i;
                        });
                    } else {
                        $scope.indentDetails.listDeleteItems = [];
                        $scope.indentDetails.listIndentItems = [];
                        $scope.indentDetails.listIndentItems.push($scope.item);
                    }

                    if ($scope.indentDetails.requestDate == '') {
                        $scope.indentDetails.requestDate == $scope.date;
                    }

                    if ($scope.indentDetails.cij.cijID > 0) {
                        $scope.isCapex = 1;
                        //$scope.GetCijItems($scope.indentDetails.cij.cijID);
                        $scope.GetCIJ($scope.indentDetails.cij.cijID);
                    }


                });
        }

        if ($scope.indentID > 0) {
            $scope.GetIndentDetails($scope.indentID);
        }
        else if ($stateParams.indentObj && $stateParams.indentObj.cloneId > 0) {
            $scope.GetIndentDetails($stateParams.indentObj.cloneId);
        };



        $scope.changeCIJ = function (val) {

            if (val > 0) {
                $scope.indentDetails.cij.cijID = val;
                $scope.indentDetails.deptID = $scope.indentDetails.cij.deptID;
                $scope.GetCIJ(val);
                $scope.GetCijItems(val);
            } else {
                $scope.indentDetails.cij.cijID = 0;
                $scope.indentDetails.listDeleteItems = [];
                $scope.indentDetails.listIndentItems = [];
                $scope.indentDetails.listIndentItems.push($scope.item);
            }
        };


        $scope.addItem = function () {

            $scope.item = {
                sNO: 0,
                materialDescription: '',
                existFunctional: 0,
                existNonFunctional: 0,
                requiredQuantity: 0,
                purposeOfMaterial: '',
                IndentItemID: 0,
                fileName: {
                    fileStream: [],
                    fileName: ''
                }
            };

            $scope.indentDetails.listIndentItems.push($scope.item);

            var i = 0;

            $scope.indentDetails.listIndentItems.forEach(function (item, index) {
                i = i + 1;
                item.sNO = i;
            });

        };


        $scope.delItem = function (val, id) {

            if ($scope.indentDetails.listIndentItems.length > 1) {

                if (id > 0) {
                    $scope.indentDetails.listDeleteItems.push(id);
                }

                $scope.indentDetails.listIndentItems.splice(val, 1);


                var i = 0;

                $scope.indentDetails.listIndentItems.forEach(function (item, index) {
                    i = i + 1;
                    item.sNO = i;
                });
            }

        };




        $scope.SaveIndentDetails = function () {

            $scope.isSaveDisable = true;

            if ($scope.isCapex != 1) {
                $scope.indentDetails.cij.cijID = 0;
            }

            $scope.validate($scope.cij);
            if ($scope.isError == false) {

                $scope.indentDetails.indID = $scope.indentID;
                $scope.indentDetails.cij.compID = userService.getUserCompanyId();
                $scope.indentDetails.sessionID = $scope.sessionID;
                $scope.indentDetails.userID = $scope.currentID;
                $scope.indentDetails.createdBy = $scope.indentDetails.userID;
                $scope.indentDetails.isCapex = parseInt($scope.isCapex);

                var params = {
                    mpindent: $scope.indentDetails
                };

                cijIndentService.SaveIndentDetails(params)
                    .then(function (response) {
                        if ($scope.indentID > 0 && $scope.itemWorkflow.length > 0) {
                            $scope.workflowObj.workflowID = $scope.itemWorkflow[0].workflowID;
                        }
                        if ($scope.workflowObj.workflowID > 0) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                                $scope.isSaveDisable = false;
                            }
                            else {
                                $scope.assignWorkflow(response.objectID);
                                //$scope.isSaveDisable = false;
                            }
                        } else {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                                $scope.isSaveDisable = false;
                            }
                            else {
                                growlService.growl("INDENT Saved Successfully.", "success");
                                //$window.history.back();
                                $scope.goToIndentList();
                            }
                        }

                        //if (response.errorMessage != '') {
                        //    growlService.growl(response.errorMessage, "inverse");
                        //}
                        //else {
                        //    growlService.growl("Indent Saved Successfully.", "success");
                        //    //$window.history.back();
                        //    $scope.goToIndentList();
                        //}
                    })
            }
        };

        $scope.goToIndentList = function (id) {
            var url = $state.href('indentList', {});
            window.open(url, '_self');
        };

        $scope.cancelClick = function () {
            var url = $state.href('indentList', {});
            window.open(url, '_self');
            //$window.history.back();
        };

        $scope.deptCode = 0;
        $scope.deptID = 0;
        $scope.userDesigID = 0;
        $scope.desigID = 0;
        $scope.desigCode = 0;





        $scope.getWorkflows = function () {
            workflowService.getWorkflowList()
                .then(function (response) {
                    //$scope.workflowList = response;
                    $scope.workflowList = [];
                    $scope.workflowListTemp = response;

                    $scope.workflowListTemp.forEach(function (item, index) {

                        if (item.WorkflowModule == 'INDENT') {
                            $scope.workflowList.push(item);
                        }

                        //$scope.workflowList = $scope.workflowList.filter(function (item) {
                        //    return item.deptID == $scope.deptID;
                        //});

                        $scope.workflowList = $scope.workflowList.filter(function (item) {
                            return item.deptID == $rootScope.rootDetails.deptId;
                        });
                    });
                });
        };

        $scope.getWorkflows();

        $scope.GetUserDeptDesig = function () {
            auctionsService.GetUserDeptDesig($scope.currentID, userService.getUserToken())
                .then(function (response) {
                    if (response && response.length > 0) {
                        $scope.UserDeptDesig = response;

                        $scope.UserDeptDesig.forEach(function (item, index) {



                            if (item.isValid == true) {
                                $scope.deptCode = item.companyDepartments.deptCode;
                                $scope.deptID = item.companyDepartments.deptID;
                                $scope.deptType = item.companyDepartments.selectedDeptType.typeID;

                                //$scope.getWorkflows();

                                //if ($scope.indentID > 0) {
                                //}
                                //else {
                                //    $scope.GetSeries('', $scope.deptID);
                                //};
                                //if ($stateParams.indentObj && $stateParams.indentObj.cloneId > 0) {
                                //    $scope.GetSeries('', $scope.deptID);
                                //}

                                //$scope.GetBudgetCodes();

                                if ($scope.indentID > 0) {
                                    $scope.deptType = item.companyDepartments.selectedDeptType.typeID;
                                } else {
                                    $scope.indentDetails.department = item.companyDepartments.deptCode;
                                    $scope.indentDetails.deptID = item.companyDepartments.deptID;
                                    $scope.deptType = item.companyDepartments.selectedDeptType.typeID;
                                    ////console.log($scope.deptType);
                                }


                                item.listUserDesignations.forEach(function (item2, index2) {
                                    if (item2.isValid == true) {
                                        $scope.userDesigID = item2.userDesigID;
                                        $scope.desigID = item2.companyDesignations.desigID;
                                        $scope.desigCode = item2.companyDesignations.desigCode;

                                        if ($scope.desigCode.toUpperCase() == 'END USER') {
                                            $scope.isEndUser == true;
                                            //$scope.isEditDisable = true;
                                        }
                                    }
                                })

                                if (($scope.deptCode.toUpperCase() == 'PURCHASE' || $scope.deptCode.toUpperCase() == 'PURCHASE DEPARTMENT' || $scope.deptCode.toUpperCase() == 'PURCHASE DEPARTMENT') && ($scope.desigCode.toUpperCase() == 'HOD' || $scope.desigCode.toUpperCase() == 'HEAD OF PROCUREMENT' || $scope.desigCode.toUpperCase() == 'PURCHASE HEAD')) {
                                    $scope.isPurchaseHod = true;
                                }

                            }



                        });

                        //$scope.checkflow();
                    }
                })
        };

        $scope.GetUserDeptDesig();



        $scope.GetRFQCreators = function () {
            rfqService.GetRFQCreators($scope.indentID)
                .then(function (response) {
                    $scope.RFQCreators = response;
                    $scope.PurchaseDept.forEach(function (item3, index3) {
                        //console.log($scope.PurchaseDept);

                    })

                });
        };

        $scope.GetRFQCreators();




        ////////////////////////
        // WORK FLOW  
        ///////////////////////







        $scope.isWorkflowCompleted = false;

        $scope.getItemWorkflow = function () {
            workflowService.getItemWorkflow(0, $scope.indentID, 'INDENT')
                .then(function (response) {
                    $scope.itemWorkflow = response;
                    //console.log($scope.itemWorkflow);
                    if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                        $scope.currentStep = 0;

                        var count = 0;

                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {


                            if (track.status == 'APPROVED') {
                                $scope.isWorkflowCompleted = true;
                                $scope.orderInfo = track.order;
                                $scope.assignToShow = track.status;

                            }
                            else {
                                $scope.isWorkflowCompleted = false;
                            }



                            if (track.status == 'REJECTED' && count == 0) {
                                count = count + 1;
                            }

                            if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                                count = count + 1;
                                $scope.IsUserApproverForStage(track.approverID);
                                $scope.currentAccess = track.order;
                            }

                            if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                                $scope.currentStep = track.order;
                                return false;
                            }
                        });
                    }
                });

        };


        if ($scope.indentID > 0) {
            $scope.getItemWorkflow();
        }


        $scope.isSaveEnabled = function () {
            if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                if ($scope.itemWorkflow[0].WorkflowTracks[0].status == 'PENDING' || $scope.itemWorkflow[0].WorkflowTracks[0].status == 'HOLD' || $scope.itemWorkflow[0].WorkflowTracks[0].status == 'REJECTED') {
                    return false;
                }
            }
            else {
                return false;
            }

            return true;
        };




        $scope.updateTrack = function (step, status) {

            $scope.commentsError = '';

            if ($scope.isReject) {
                $scope.commentsError = 'Please Save Rejected Items/Qty';
                return false;
            }

            if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
                $scope.commentsError = 'Please enter comments';
                return false;
            }

            step.status = status;
            step.sessionID = $scope.sessionID;
            step.modifiedBy = $scope.currentID;
            step.moduleCode = $scope.indentDetails.indNo;

            step.moduleName = 'INDENT';

            workflowService.SaveWorkflowTrack(step)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        //growlService.growl("INDENT Saved Successfully.", "success");
                        $scope.getItemWorkflow();
                        location.reload();
                    }
                })
        };

        $scope.assignWorkflow = function (moduleID) {
            workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: $scope.currentID, sessionID: $scope.sessionID }))
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                        $scope.isSaveDisable = false;
                    }
                    else {
                        //growlService.growl("INDENT Saved Successfully.", "success");
                        //$window.history.back();
                        $scope.goToIndentList();
                        //$scope.goToCIJ();
                    }
                })
        };



        $scope.IsUserApprover = false;

        $scope.functionResponse = false;
        $scope.IsUserApproverForStage = function (approverID) {
            workflowService.IsUserApproverForStage(approverID, $scope.currentID)
                .then(function (response) {
                    $scope.IsUserApprover = response;
                });
        };

        ////////////////////////
        // WORK FLOW  
        ///////////////////////

        $scope.dateFormat = function (date) {
            return new moment(date).format("DD-MM-YYYY HH:mm");
        };


        $scope.goToCIJID = function (id) {
            var url = $state.href('cij', { "cijID": id });
            window.open(url, '_self');
        };

        $scope.goToIndent = function (id) {
            var url = $state.href('materialsPurchaseIndent', { "indentID": id });
            window.open(url, '_self');
        }


        $scope.GetUserDetails = function () {
            auctionsService.GetUserDetails($scope.userdetails, $scope.sessionID)
                .then(function (response) {
                    $scope.userDetailsList = response;

                    if ($scope.indentID > 0) {

                    } else if ($scope.indentDetails.requestedBy == '' || $scope.indentDetails.requestedBy == undefined) {

                        $scope.indentDetails.requestedBy = $scope.userDetailsList.firstName + $scope.userDetailsList.lastName;
                    }
                });
        };

        $scope.GetUserDetails();

        $scope.validate = function () {

            $scope.isValidToSubmit = false;
            $scope.errorMessage = '';
            $scope.tableValidation = false;

            $scope.isError = false;

            $scope.isErrorDepartment = $scope.isApproxErrorTotalCost = $scope.isErrorMaterialDescription =
                $scope.isErrorRequiredQuantity = $scope.isErrorPurposeOfMaterial = $scope.isErrorworkflowID =
                $scope.isErrorCIJ = false;

            $scope.isErrorCIJStyle = $scope.isApproxErrorTotalCostStyle = $scope.isErrorDepartmentStyle = $scope.isErrorworkflowIDStyle = {};

            if ($scope.isCapex > 0 && ($scope.indentDetails.cij.cijID == 0 || $scope.indentDetails.cij.cijID == undefined || $scope.indentDetails.cij.cijID == null || $scope.indentDetails.cij.cijID == '')) {
                $scope.isErrorCIJ = true;
                $scope.isError = true;
                $scope.errorMessage = 'Select CIJ';
                $scope.isSaveDisable = false;
                $scope.isErrorCIJStyle = { "border": "1px solid #ff0000" };
            }

            if ($scope.indentDetails.department == 0 || $scope.indentDetails.department == "" || $scope.indentDetails.department == undefined) {
                $scope.isErrorDepartment = true;
                $scope.isError = true;
                $scope.errorMessage = 'Department is Missing';
                $scope.isSaveDisable = false;
                $scope.isErrorDepartmentStyle = { "border": "1px solid #ff0000" };
            }
            if ($scope.indentDetails.approximateCostRange <= 0 || $scope.indentDetails.approximateCostRange == "" || $scope.indentDetails.approximateCostRange == null) {
                $scope.isApproxErrorTotalCost = true;
                $scope.isError = true;
                $scope.errorMessage = 'Approximate Cost Range is Missing';
                $scope.isSaveDisable = false;
                $scope.isApproxErrorTotalCostStyle = { "border": "1px solid #ff0000" };
            }

            if ($scope.indentID == 0 && ($scope.workflowObj.workflowID == null || $scope.workflowObj.workflowID == "" || $scope.workflowObj.workflowID == undefined)) {
                $scope.isErrorworkflowID = true;
                $scope.isError = true;
                $scope.errorMessage = 'workflow ID is Missing';
                $scope.isSaveDisable = false;
                $scope.isErrorworkflowIDStyle = { "border": "1px solid #ff0000" };
            }

            $scope.indentDetails.listIndentItems.forEach(function (item, index) {

                item.isErrorRequiredQuantity = false;
                item.isErrorBudgetCode = false;
                item.isErrorMaterialDescription = false;
                item.isErrorPurposeOfMaterial = false;
                item.isErrorMaterialUnits = false;

                item.style = {};

                if (item.units == null || item.units == '' || item.units == undefined) {
                    $scope.isError = true;
                    $scope.errorMessage = 'Material Units is Missing';
                    $scope.units = true;
                    $scope.tableValidation = true;
                    item.isErrorMaterialUnits = true;
                    $scope.isSaveDisable = false;
                }

                if (item.budgetID == null || item.budgetID == '' || item.budgetID == undefined || item.budgetID <= 0) {
                    $scope.isError = true;
                    $scope.errorMessage = 'Budget code is Missing';
                    $scope.isErrorBudgetCode = true;
                    $scope.tableValidation = true;

                    item.isErrorBudgetCode = true;
                    $scope.isSaveDisable = false;
                    item.style = { "border": "1px solid #ff0000" };
                }

                if (item.materialDescription == null || item.materialDescription == '' || item.materialDescription == undefined) {
                    $scope.isError = true;
                    $scope.errorMessage = 'Material Description is Missing';
                    $scope.isErrorMaterialDescription = true;
                    $scope.tableValidation = true;

                    item.isErrorMaterialDescription = true;
                    $scope.isSaveDisable = false;

                }

                if (item.requiredQuantity <= 0 || item.requiredQuantity == undefined || item.requiredQuantity == '') {
                    $scope.isError = true;
                    $scope.errorMessage = 'Required Quantity is Missing';
                    $scope.isErrorRequiredQuantity = true;
                    $scope.tableValidation = true;

                    item.isErrorRequiredQuantity = true;
                    $scope.isSaveDisable = false;
                }

                if (item.purposeOfMaterial == null || item.purposeOfMaterial == '' || item.purposeOfMaterial == undefined) {
                    $scope.isError = true;
                    $scope.errorMessage = 'Purpose Of Material is Missing';
                    $scope.isErrorPurposeOfMaterial = true;
                    $scope.tableValidation = true;

                    item.isErrorPurposeOfMaterial = true;
                    $scope.isSaveDisable = false;
                }
            });

            if ($scope.tableValidation) {
                $scope.isSaveDisable = false;
                return false;
            }

        };



        $scope.RFQCreators = [];

        $scope.GetRFQCreators = function () {
            rfqService.GetRFQCreators($scope.indentID)
                .then(function (response) {
                    $scope.RFQCreators = response;
                });
        };

        $scope.GetRFQCreators();



        $scope.getSubUserData = function () {

            var params = { "userid": $scope.currentID, "sessionid": $scope.sessionID }

            userService.getSubUsersData(params)
                .then(function (response) {
                    $scope.activeUserObj = response;

                    if ($scope.isPurchaseHod == true) {

                        var obj = {
                            firstName: userService.getFirstname(),
                            lastName: userService.getLastname(),
                            userID: userService.getUserId(),
                            isIndentAssigned: false
                        }

                        $scope.PurchaseDept.push(obj);
                    }

                    $scope.activeUserObj.forEach(function (item, index) {

                        //$scope.RFQCreators.forEach(function (RFQCreatorItem, index) {
                        //    if (item.userID == RFQCreatorItem || RFQCreatorItem == userService.getUserId()) {
                        //        item.isIndentAssigned = true;
                        //    }
                        //})

                        if (item.isValid == true) {

                            auctionsService.GetUserDeptDesig(item.userID, userService.getUserToken())
                                .then(function (response) {
                                    item.UserDeptDesig = response;

                                    item.UserDeptDesig.forEach(function (item2, index2) {
                                        if (item2.isValid == true) {

                                            if (item2.companyDepartments.deptID == $scope.deptID) {
                                                $scope.currentDept.push(item);
                                            }

                                            if (item2.companyDepartments.deptCode.toUpperCase() == 'DIRECTOR') {
                                                $scope.myDeptUser.push(item);

                                                $scope.myDeptUser.forEach(function (item3, index3) {
                                                    if (item3.isValid == true) {
                                                        item3.UserDeptDesig.forEach(function (item4, index4) {
                                                            if (item4.isValid == true) {
                                                                item4.listUserDesignations.forEach(function (item5, index5) {
                                                                    if (item5.isValid == true) {
                                                                        if (item5.companyDesignations.desigCode.toUpperCase() == 'DIRECTOR') {

                                                                            //$scope.isEndUser = true;
                                                                            //$scope.isEditDisable = true;

                                                                            $scope.HODName = item3.firstName + ' ' + item3.lastName;
                                                                            $scope.HODPhone = item3.phoneNum
                                                                            if ($scope.cijID > 0) {
                                                                            } else {
                                                                                $scope.indentDetails.purchaseDirector = $scope.HODName;
                                                                            }
                                                                        }
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }






                                            if (item2.companyDepartments.deptCode.toUpperCase() == 'BUDGET' || item2.companyDepartments.deptCode.toUpperCase() == 'FINANCE') {
                                                $scope.BudgetDept.push(item);
                                            }
                                            if (item2.companyDepartments.deptCode.toUpperCase() == 'PURCHASE' || item2.companyDepartments.deptCode.toUpperCase() == 'PURCHASE DEPARTMENT' || item2.companyDepartments.deptCode.toUpperCase() == 'PURCHASE DEPARTMENT') {
                                                $scope.PurchaseDept.push(item);


                                                $scope.PurchaseDept.forEach(function (item3, index3) {
                                                    if (item3.isValid == true) {
                                                        item3.UserDeptDesig.forEach(function (item4, index4) {
                                                            if (item4.isValid == true) {
                                                                item4.listUserDesignations.forEach(function (item5, index5) {
                                                                    if (item5.isValid == true) {
                                                                        if (item5.companyDesignations.desigCode.toUpperCase() == 'HOD' || item5.companyDesignations.desigCode.toUpperCase() == 'HEAD OF PROCUREMENT' || item5.companyDesignations.desigCode.toUpperCase() == 'PURCHASE HEAD') {
                                                                            $scope.purchaseName = item3.firstName + ' ' + item3.lastName;
                                                                            $scope.purchasePhone = item3.phoneNum
                                                                            if ($scope.cijID > 0) {
                                                                            } else {
                                                                                $scope.indentDetails.purchaseIncharge = $scope.purchaseName;


                                                                            }
                                                                        }
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })

                                            }
                                        }
                                    })



                                    $scope.BudgetDept.forEach(function (item3, index3) {
                                        if (item3.isValid == true) {
                                            item3.UserDeptDesig.forEach(function (item4, index4) {
                                                if (item4.isValid == true) {
                                                    item4.listUserDesignations.forEach(function (item5, index5) {
                                                        if (item5.isValid == true) {
                                                            if (item5.companyDesignations.desigCode.toUpperCase() == 'HOD') {
                                                                $scope.HODBudgetName = item3.firstName + ' ' + item3.lastName;
                                                                $scope.HODBudgetPhone = item3.phoneNum
                                                                if ($scope.cijID > 0) {
                                                                } else {
                                                                    $scope.cij.budgetApprovedBy.name = $scope.HODBudgetName;
                                                                    $scope.cij.budgetApprovedBy.mobile = $scope.HODBudgetPhone;
                                                                }
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })




                                    $scope.myDeptUser.forEach(function (item3, index3) {
                                        if (item3.isValid == true) {
                                            item3.UserDeptDesig.forEach(function (item4, index4) {
                                                if (item4.isValid == true) {
                                                    item4.listUserDesignations.forEach(function (item5, index5) {
                                                        if (item5.isValid == true) {
                                                            if (item5.companyDesignations.desigCode.toUpperCase() == 'DIRECTOR') {

                                                                //$scope.isEndUser = true;
                                                                //$scope.isEditDisable = true;

                                                                $scope.HODName = item3.firstName + ' ' + item3.lastName;
                                                                $scope.HODPhone = item3.phoneNum
                                                                if ($scope.cijID > 0) {
                                                                } else {
                                                                    $scope.indentDetails.purchaseDirector = $scope.HODName;
                                                                }
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })




                                    $scope.currentDept.forEach(function (item31, index3) {
                                        if (item31.isValid == true) {
                                            item31.UserDeptDesig.forEach(function (item4, index4) {
                                                if (item4.isValid == true) {
                                                    item4.listUserDesignations.forEach(function (item5, index5) {
                                                        if (item5.isValid == true) {
                                                            if (item5.companyDesignations.desigCode.toUpperCase() == 'HOD') {



                                                                $scope.currentHODName = item31.firstName + ' ' + item31.lastName;
                                                                $scope.currentHODPhone = item31.phoneNum
                                                                if ($scope.cijID > 0) {
                                                                } else {
                                                                    $scope.indentDetails.approvedBy = $scope.currentHODName;
                                                                }
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })


                                    $scope.PurchaseDept.forEach(function (item3, index3) {


                                        $scope.RFQCreators.forEach(function (uid, index3) {
                                            //console.log("**************");
                                            //console.log(item3);

                                            if (item3.userID == uid) {
                                                item3.isIndentAssigned = true;
                                            }


                                        })



                                        if (item3.isValid == true) {
                                            item3.UserDeptDesig.forEach(function (item4, index4) {
                                                if (item4.isValid == true) {
                                                    item4.listUserDesignations.forEach(function (item5, index5) {
                                                        if (item5.isValid == true) {
                                                            if (item5.companyDesignations.desigCode.toUpperCase() == 'HOD' || item5.companyDesignations.desigCode.toUpperCase() == 'HEAD OF PROCUREMENT' || item5.companyDesignations.desigCode.toUpperCase() == 'PURCHASE HEAD') {
                                                                $scope.purchaseName = item3.firstName + ' ' + item3.lastName;
                                                                $scope.purchasePhone = item3.phoneNum
                                                                if ($scope.cijID > 0) {
                                                                } else {
                                                                    $scope.indentDetails.purchaseIncharge = $scope.purchaseName;


                                                                }
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                })

                        }

                        $scope.GetUserDeptDesig();

                    });

                });
        };

        $scope.getSubUserData();

        $scope.consoleLog = function (cal) {
            //console.log(cal);
        };

        $scope.SaveRFQCreators = function () {
            $scope.PurchaseDept.forEach(function (item3, index3) {

                var PurchaseAssignedUsersObj = {
                    firstName: item3.firstName,
                    lastName: item3.lastName,
                    userID: item3.userID,
                    isIndentAssigned: item3.isIndentAssigned
                };

                $scope.PurchaseAssignedUsers.push(PurchaseAssignedUsersObj);

            });

            var params = {
                listUsers: $scope.PurchaseAssignedUsers,
                indentID: $scope.indentID,
                sessionID: $scope.sessionID
            };

            workflowService.SaveRFQCreators(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Saved Successfully.", "success");
                        //$window.history.back();
                        //$scope.goToIndentList();
                        //$scope.goToCIJ();
                    }
                })
        };








        /* ITEM LEVEL REJECTION */

        $scope.rejectStyle = { 'background-color': 'orange' };

        $scope.RejectIndentItems = function () {

            $scope.isReject = false;

            var params = {
                listItems: $scope.indentDetails.listIndentItems,
                indentID: $scope.indentID,
                sessionID: $scope.sessionID,
                userID: $scope.currentID
            };

            cijIndentService.RejectIndentItems(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Saved Successfully.", "success");
                        location.reload();
                    }
                });

        };

        $scope.reject = function () {

            $scope.indentDetails.listIndentItems.forEach(function (item, index) {
                item.rejectReason = '';
                item.isRejected = 0;
                item.rejectedQuantity = 0;
            });

            $scope.isReject = true;
        }

        $scope.cloneIndent = function (indID) {
            let tempObj = {};
            tempObj.cloneId = indID;
            $state.go('materialsPurchaseIndent', { 'indentID': 0, indentObj: tempObj });
        };




        //console.log($stateParams);


        /* ITEM LEVEL REJECTION */





    });