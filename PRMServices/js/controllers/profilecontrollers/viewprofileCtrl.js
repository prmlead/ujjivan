prmApp

    .controller('viewprofileCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$stateParams", "$timeout", "auctionsService", "userService", "SignalRFactory", "fileReader", "growlService",
        function ($scope, $http, $state, domain, $filter, $stateParams, $timeout, auctionsService, userService, SignalRFactory, fileReader, growlService) {
            $scope.userId = $stateParams.Id;
            $scope.viewProfileUserId = $stateParams.Id;
            $scope.userObj = {
                //logoURL: '../js/resources/images/chart.png'
            };


            $scope.vendorGstDetailsObj = {
                filing: {},
                isError: false,
                error: 'test'
            };

            this.userdetails = {};
            $scope.userRatings = [];
            $scope.isCommentPosted = false;
            $scope.sessionid = userService.getUserToken();
            $scope.ratingRequirement = 0;
            $scope.userOverallRating = 0;
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

            $scope.subcategories = '';
            $scope.categories = '';
            $scope.isValidToSubmitReview = false;

            $scope.vendorStatistics = {};

            /*getuserdetails START*/
            userService.getProfileDetails({ "userid": $scope.userId, "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.userObj = response;
                    if ($scope.userObj.stn) {
                        $scope.getGSTFilingDetails($scope.userObj.stn);
                        //$scope.getGSTCompanyDetails($scope.userObj.stn);
                    } else {
                        $scope.vendorGstDetailsObj.isError = true;
                        $scope.vendorGstDetailsObj.error = 'Vendor GST number missing.';
                    }

                    var data = response.establishedDate;
                    var date = new Date(parseInt(data.substr(6)));
                    $scope.userObj.establishedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();



                    for (i = 0; i < $scope.userObj.subcategories.length; i++) {

                        $scope.categories = '';

                        $scope.subcategories += $scope.userObj.subcategories[i].subcategory + "; ";

                    }

                    if ($scope.userObj.subcategories) {

                        $scope.userObj.subcategories.forEach(function (item, index) {

                            var n = false;
                            n = $scope.categories.includes(item.category + ";");
                            if (n == false) {
                                $scope.categories += item.category + "; ";
                            }
                        });
                    }


                    if ($scope.categories == '') {
                        $scope.categories = $scope.userObj.category + "; ";
                    }
                    $scope.userObj.createdOn = new moment($scope.userObj.createdOn).format("DD-MM-YYYY");
                    $scope.userObj.oemKnownSince = new moment($scope.userObj.oemKnownSince).format("DD-MM-YYYY");
                    $scope.userObj.aboutUs = $scope.userObj.aboutUs.replace(/\u000a/g, "<br />");
                    $scope.userObj.address = $scope.userObj.address.replace(/\u000a/g, "<br />");



                });
            /*getuserdetails END*/

            $scope.getNumber = function (num) {
                return new Array(num);
            };

            $scope.saveUserComments = function () {
                var vendorRating = {};
                vendorRating.rId = 0;
                vendorRating.reqId = $scope.ratingRequirement;
                vendorRating.revieweeId = $scope.userId;
                vendorRating.rating = $scope.userRatingsJson;
                vendorRating.comments = $scope.review;
                userService.SaveVendorRating(vendorRating)
                    .then(function (response) {
                        $scope.isCommentPosted = true;
                        $scope.review = '';
                        $scope.ratingRequirement = 0;
                        $scope.userOverallRating = 0;
                        $scope.userRatingsJson = {
                            deliveryValue: 0,
                            qualityValue: 0,
                            emergencyValue: 0,
                            serviceValue: 0,
                            responseValue: 0
                        };

                        if (response && response.errorMessage === '') {
                            growlService.growl("Review Saved successfully.", "success");
                            $scope.getVendorRatings();
                        } else {
                            growlService.growl("Review failed to save.", "inverse");
                        }
                    });
            };

            $scope.addnewdetailsobj = {
                alternateID: 0,
                altUserID: $scope.userId,
                userID: $scope.userId,
                firstName: '',
                lastName: '',
                department: '',
                designation: '',
                email: '',
                phoneNum: '',
                companyId: userService.getUserCompanyId(),
                companyName: $scope.userObj.companyName,
                isPrimary: 0,
                isValid: 1,
                sessionID: userService.getUserToken()
            }



            $scope.addNewDetails = function () {
                $scope.addnewdetailsobj.companyName = $scope.userObj.companyName;

                var params = {
                    "user": $scope.addnewdetailsobj
                };

                auctionsService.newDetails(params)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Data Saved successfully.");
                            this.addUser = 0;
                            $state.reload();
                        }
                    });

            };

            $scope.getAlternateContacts = function () {
                auctionsService.getalternatecontacts($scope.userId, userService.getUserCompanyId(), userService.getUserToken())
                    .then(function (response) {
                        $scope.alternateContacts = response;
                    });
            };

            $scope.getAlternateContacts();

            $scope.isDelete = false;

            $scope.editAltContacts = function (param, isValid) {

                //window.scrollTo(0, 250);

                var elmnt = document.getElementById("contactInfo");
                elmnt.scrollIntoView();


                $scope.isDelete = false;

                if (isValid == 1) {
                    $scope.isDelete = false;
                }
                else {
                    $scope.isDelete = true;
                }

                $scope.show = true;
                $scope.addnewdetailsobj.alternateID = param.alternateID;
                $scope.addnewdetailsobj.firstName = param.firstName;
                $scope.addnewdetailsobj.lastName = param.lastName;
                $scope.addnewdetailsobj.email = param.email;
                $scope.addnewdetailsobj.phoneNum = parseInt(param.phoneNum);
                $scope.addnewdetailsobj.department = param.department;
                $scope.addnewdetailsobj.designation = param.designation;
                $scope.addnewdetailsobj.isValid = isValid;
            }


            $scope.show = false;

            $scope.display = function (val) {

                $scope.addnewdetailsobj = {
                    alternateID: 0,
                    altUserID: $scope.userId,
                    userID: $scope.userId,
                    firstName: '',
                    lastName: '',
                    department: '',
                    designation: '',
                    lastName: '',
                    email: '',
                    phoneNum: '',
                    companyId: userService.getUserCompanyId(),
                    companyName: $scope.userObj.companyName,
                    isPrimary: 0,
                    isValid: 1,
                    sessionID: userService.getUserToken()
                };

                $scope.isDelete = false;
                $scope.show = val;
            };

            $scope.isGSTNumber = '';
            $scope.gstFile = 0;
            $scope.getUserCredentials = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getusercredentials?sessionid=' + userService.getUserToken() + "&userid=" + $scope.userId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.CredentialsResponce = response.data;
                    $scope.isGSTVerified = 0;
                    $scope.CredentialsResponce.forEach(function (item, index) {
                        if (item.fileType == 'STN') {
                            $scope.isGSTVerified = item.isVerified;
                            $scope.gstNumber = item.credentialID;
                            if (item.fileLink > 0) {
                                $scope.gstFile = item.fileLink;
                            }

                        }

                    })


                })
            };

            $scope.getUserCredentials();

            $scope.getuserinfo = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getuserinfo?sessionid=' + userService.getUserToken() + "&userid=" + $scope.userId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.userinfo = response.data;


                })
            };

            $scope.getuserinfo();

            $scope.onlineStatusObj = 'Not logged in';
            $scope.GetOnlineStatus = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getonlinestatus?sessionid=' + userService.getUserToken() + "&userid=" + $scope.userId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.onlineStatusObj = response.data;
                })
            };

            $scope.GetOnlineStatus();

            $scope.myAuctions = [];
            $scope.changeOnHover = true; // default test value
            $scope.maxValue = 5; // default test value
            $scope.userRatingsJson = {
                deliveryValue: 0,
                qualityValue: 0,
                emergencyValue: 0,
                serviceValue: 0,
                responseValue: 0
            };

            $scope.vendorProducts = [];
            $scope.GetVendorProducts = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getvendorproducts?sessionid=' + userService.getUserToken() + "&userid=" + $scope.userId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.vendorProducts = response.data;
                });
            };

            $scope.GetVendorProducts();

            auctionsService.vendorstatistics($scope.userId, userService.getUserToken())
                .then(function (response) {
                    $scope.vendorStatistics = response;
                });

            auctionsService.getactiveleads({ "userid": $scope.viewProfileUserId, "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.myAuctions = response;
                });

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                }
            };

            $scope.validateUserReview = function () {
                if ($scope.userRatingsJson.deliveryValue > 0 && $scope.userRatingsJson.qualityValue > 0 && $scope.userRatingsJson.emergencyValue > 0
                    && $scope.userRatingsJson.serviceValue > 0 && $scope.userRatingsJson.responseValue > 0 && $scope.review != '') {
                    $scope.isValidToSubmitReview = true;
                }
            };

            $scope.getVendorRatings = function () {
                userService.GetVendorRatings($scope.userId)
                    .then(function (response) {
                        $scope.userRatings = response;
                        $scope.userRatings.forEach(function (item, index) {
                            item.DATE_CREATED = $scope.GetDateconverted(item.DATE_CREATED);
                            item.overalRating = parseInt((item.DELIERY_RATING + item.EMERGENCY_RATING + item.QUALITY_RATING + item.RESPONSE_RATING + item.SERVICE_RATING) / 5);
                            $scope.userOverallRating = $scope.userOverallRating + ((item.DELIERY_RATING + item.EMERGENCY_RATING + item.QUALITY_RATING + item.RESPONSE_RATING + item.SERVICE_RATING) / 5);
                        });

                        if ($scope.userRatings && $scope.userRatings.length > 0) {
                            $scope.userOverallRating = parseInt($scope.userOverallRating / $scope.userRatings.length);
                        }

                    });
            };

            $scope.getVendorRatings();

            $scope.getGSTFilingDetails = function (gst) {
                $scope.vendorGstDetailsObj.isError = false;
                $scope.vendorGstDetailsObj.filing = null;
                userService.getGSTDetails({ "gstin": gst, "year": '2019-20', "sessionid": userService.getUserToken() })
                    .then(function (response) {

                        var vendorGSTFilingDetailsJson = response;//'{"error":false,"data":{"EFiledlist":[{"valid":"Y","mof":"ONLINE","dof":"21-12-2019","ret_prd":"112019","rtntype":"GSTR3B","arn":"AA3611192835414","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"10-12-2019","ret_prd":"102019","rtntype":"GSTR3B","arn":"AA361019397525T","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"09-12-2019","ret_prd":"092019","rtntype":"GSTR1","arn":"AA360919514416U","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"09-12-2019","ret_prd":"092019","rtntype":"GSTR3B","arn":"AA3609195140138","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"23-09-2019","ret_prd":"082019","rtntype":"GSTR3B","arn":"AA360819275807D","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"17-09-2019","ret_prd":"072019","rtntype":"GSTR3B","arn":"AA3607193931202","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"27-07-2019","ret_prd":"062019","rtntype":"GSTR1","arn":"AA360619397611P","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"27-07-2019","ret_prd":"042019","rtntype":"GSTR3B","arn":"AA360419423368X","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"27-07-2019","ret_prd":"052019","rtntype":"GSTR3B","arn":"AA360519397330X","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"27-07-2019","ret_prd":"062019","rtntype":"GSTR3B","arn":"AA360619397246K","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"25-05-2019","ret_prd":"032019","rtntype":"GSTR1","arn":"AA360319530586X","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"22-05-2019","ret_prd":"032019","rtntype":"GSTR3B","arn":"AA360319525963U","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"25-04-2019","ret_prd":"022019","rtntype":"GSTR3B","arn":"AA3602194018174","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"22-04-2019","ret_prd":"012019","rtntype":"GSTR3B","arn":"AA360119415081L","status":"Filed"}]}}';
                        console.log(JSON.parse(vendorGSTFilingDetailsJson));
                        if (vendorGSTFilingDetailsJson) {
                            let tempObj = JSON.parse(vendorGSTFilingDetailsJson);
                            if (!tempObj.error) {
                                $scope.vendorGstDetailsObj.filing = tempObj.data.EFiledlist;
                            } else {
                                $scope.vendorGstDetailsObj.isError = true;
                                $scope.vendorGstDetailsObj.error = tempObj.data.error.message;
                            }
                        }
                    });
            };

        }]);