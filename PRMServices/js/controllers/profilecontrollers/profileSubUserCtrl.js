﻿prmApp.
    controller('profileSubUserCtrl', function ($scope, growlService, userService, $filter, $log, auctionsService, $state, $http, domain) {
        $scope.isCustomer = userService.getUserType();
        $scope.sessionid = userService.getUserToken();
        $scope.companyID = userService.getUserCompanyId();

        var self = this;

        this.addUser = 0;
        this.editUser = 0;

        $scope.passwordStrength = '';
        $scope.checkpwdStrength = {
            "height": "25px"
        };
        let strongRegularExp = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
        let mediumRegularExp = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
        /*pagination code*/
        $scope.totalItems = 0;
        $scope.totalVendors = 0;
        $scope.totalSubuser = 0;
        $scope.totalInactiveVendors = 0;
        $scope.totalLeads = 0;
        $scope.currentPage = 1;
        $scope.currentPage2 = 1;
        $scope.itemsPerPage = 10;
        $scope.itemsPerPage2 = 10;
        $scope.maxSize = 10;

        //$scope.checkPhoneUniqueResult = false;
        $scope.checkCompanyUniqueResult = false;
        $scope.checkPANUniqueResult = false;
        $scope.checkTINUniqueResult = false;
        $scope.checkSTNUniqueResult = false;
        $scope.checkSUBLOGINUniqueResult = false;
        $scope.isValidToSubmit = true;

        $scope.userObj = {};
        $scope.subUsers = [];
        $scope.inactiveSubUsers = [];
        $scope.subUsers1 = [];
        // SUB USER DEPARTMENT AND DESIGNATION SELECTION //
        $scope.currentUser = userService.getUserId();
        // SUB USER DEPARTMENT AND DESIGNATION SELECTION //
        $scope.searchKeyword = '';
        $scope.isEditUser = false;
        $scope.subUserID = 0;
        $scope.addnewuserobj = {};

        $scope.selectedCurrency = {};
        $scope.currencies = [];
        $scope.addnewuserobj.companyLocation = [];
        $scope.addnewuserobj.companyLocationSearch = [];

        $scope.mindate = moment();

        $scope.companyBranches =
            [
                {
                    display: 'NATIONAL',
                    value: 1
                },
                {
                    display: 'INTERNATIONAL',
                    value: 2
                }
            ];

        $scope.temporaryObj = {
            email: '',
            phone: '',
            loginID: ''
        };

        //$scope.temporaryObj.email = '';
        //$scope.temporaryObj.phone = '';
        //$scope.temporaryObj.loginID = '';

        $scope.subuservalidityParams = {

            //userValidityFrom: '',
            //userValidityTo: '',
            dateFrom: '',
            dateTo: ''
        }

        $scope.showAccess = false;
        $scope.showUserDepartments = false;
        $scope.showUserDesignations = false;
        $scope.showUserDeptDesig = false;

        $scope.checkValidFrom = '';
        $scope.checkValidTo = '';

        $scope.UserName = '';

        $scope.UserDeptDesig = [];

        // SUB USER DEPARTMENT AND DESIGNATION SELECTION //
        $scope.selectedDepartmentDesignations = [];
        // SUB USER DEPARTMENT AND DESIGNATION SELECTION //

        $scope.showIsSuperUser = function () {
            if (userService.getUserObj().isSuperUser) {
                return true;
            } else {
                return false;
            }
        };

        $scope.showIsSuperUser();

        $scope.companyLocation = [];
        $scope.companyLocationSearch = [];
        $scope.GetCompanyConfiguration = function () {
            $scope.companyLocation = [];
            auctionsService.GetCompanyConfiguration($scope.companyID, 'USER_LOCATION', userService.getUserToken())
                .then(function (unitResponse) {

                    $scope.companyConfigList = unitResponse;
                    var countriesTemp = [];
                    $scope.companyConfigList.forEach(function (item, index) {
                        if (item.configKey == 'USER_LOCATION' && item.compID == $scope.companyID) {
                            $scope.companyLocation.push(item);
                            $scope.companyLocationSearch = angular.copy($scope.companyLocation);
                            $scope.addnewuserobj.companyLocation = angular.copy($scope.companyLocation);
                           
                        }
                    });

                });

        }
        $scope.GetCompanyConfiguration();
        userService.getUserDataNoCache()
            .then(function (response) {
                $scope.userObj = response;
            });


        $scope.getSubUserData = function () {
            userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.subUsers = $filter('filter')(response, { isValid: true });
                   
                    $scope.inactiveSubUsers = $filter('filter')(response, { isValid: false });

                    $scope.subUsers.forEach(function (user, userIndex) {
                        user.dateFrom = userService.toLocalDate(user.dateFrom);
                        user.dateTo = userService.toLocalDate(user.dateTo);
                    })
                    $scope.totalItems = $scope.subUsers.length;
                    $scope.subUsers1 = $filter('filter')(response, { isValid: true });
                });
        };

        $scope.getSubUserData();

        // SUB USER DEPARTMENT AND DESIGNATION SELECTION //
        $scope.getDesignations = function (department) {
            $scope.selectedDepartmentDesignations = [];
            if (department) {
                $scope.selectedDepartmentDesignations = department.listDesignation;
            }
        };
        // SUB USER DEPARTMENT AND DESIGNATION SELECTION //

        $scope.deleteUser = function (userid) {
            userService.deleteUser({ "userID": userid, "referringUserID": userService.getUserId() })
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("User deactivated Successfully", "inverse");
                        $scope.getSubUserData();
                    }
                });
        };


        $scope.activateUser = function (userid) {
            userService.activateUser({ "userID": userid, "referringUserID": userService.getUserId() })
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("User added Successfully", "success");
                        $scope.getSubUserData();
                    }
                });
        };


        $scope.getCategories = function () {
            auctionsService.getCategories(userService.getUserId())
                .then(function (response) {
                    $scope.categories = response;
                    $scope.addVendorCats = _.uniq(_.map(response, 'category'));
                    $scope.categoriesdata = response;
                    $scope.showCategoryDropdown = true;
                })
        }

        $scope.getCategories();


        $scope.getKeyValuePairs = function (parameter) {
            auctionsService.getKeyValuePairs(parameter)
                .then(function (response) {
                    if (parameter == "CURRENCY") {
                        $scope.currencies = response;
                    } else if (parameter == "TIMEZONES") {
                        $scope.timezones = response;
                    }
                    $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                    $scope.selectedCurrency = $scope.selectedCurrency[0];
                });
        };

        $scope.getKeyValuePairs('CURRENCY');
        $scope.getKeyValuePairs('TIMEZONES');

        $scope.isLoginIDUniq = '';

        $scope.userValidityFromValidation = $scope.userValidityToValidation = $scope.PasswordValidation = $scope.emailValidation = $scope.emailregxvalidation = false;

        $scope.loadLocation = function () {
            let selectedBranch = $scope.branchCodes.filter(function (branch) {
                return branch.RH_ID === $scope.addnewuserobj.RH_ID;
            });

            if (selectedBranch && selectedBranch.length > 0) {
                $scope.addnewuserobj.LOCATION_ID = selectedBranch[0].LOCATION_ID
                $scope.addnewuserobj.LOCATION_CODE = selectedBranch[0].LOCATION_CODE
            }
        };

        this.AddNewUser = function (userVal, userid) {
            $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            $scope.userValidityFromValidation = $scope.userValidityToValidation = $scope.FromValidation = $scope.ToValidation = false;
            $scope.showValidity = false;

            if (!$scope.addnewuserobj.firstName) {
                return;
            }

            if (!$scope.addnewuserobj.lastName) {
                return;
            }

            if (!$scope.addnewuserobj.email) {
                $scope.emailValidation = true;
                return false;
            } else if (!$scope.emailRegx.test($scope.addnewuserobj.email)) {
                $scope.emailregxvalidation = true;
                return false;
            }

            if (isNaN($scope.addnewuserobj.phoneNum)) {
                $scope.showMessage = true;
                $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
            } else {
                $scope.showMessage = false;
                $scope.msg = '';
            }

            //if ($scope.addnewuserobj.password1 == '' || $scope.addnewuserobj.password1 == undefined) {
            //    $scope.PasswordValidation = true;
            //    return false;
            //}

            //if ($scope.addnewuserobj.currency == '' || $scope.addnewuserobj.currency == undefined) {
            //    growlService.growl('Please select Currency', "inverse");
            //    return false;
            //}

            if ($scope.addnewuserobj.loginid == '' || $scope.addnewuserobj.loginid == undefined) {
                $scope.loginValidation = true;
                return false;
            }
            //else if ($scope.isLoginIDUniq == true) {
            //    growlService.growl('Login ID already exists', "inverse");
            //    return false;
            //}



            if ($scope.addnewuserobj.userValidityFrom == null || $scope.addnewuserobj.userValidityFrom == '') {
                $scope.FromValidation = true;
                return false;
            }

            if ($scope.addnewuserobj.userValidityTo == null || $scope.addnewuserobj.userValidityTo == '') {
                $scope.ToValidation = true;
                return false;
            }

            var ts = userService.toUTCTicks($scope.addnewuserobj.userValidityFrom);
            var m = moment(ts);
            var validFrom = new Date(m);
            var milliseconds = parseInt(validFrom.getTime() / 1000.0);
            $scope.addnewuserobj.validityFrom = "/Date(" + milliseconds + "000+0530)/";


            var ts = userService.toUTCTicks($scope.addnewuserobj.userValidityTo);
            var m = moment(ts);
            var validTo = new Date(m);
            var milliseconds = parseInt(validTo.getTime() / 1000.0);
            $scope.addnewuserobj.validityTo = "/Date(" + milliseconds + "000+0530)/";

            var CurrentDateToLocal = '';

            auctionsService.getdate()
                .then(function (GetDateResponse) {
                    CurrentDateToLocal = userService.toLocalDate(GetDateResponse);
                })

            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
            var m = moment(ts);
            var deliveryDate = new Date(m);
            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

            // var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));
            var CurrentDate = $scope.mindate;

            var validFromLocal = userService.toLocalDate(validFrom);
            var ts1 = moment(validFromLocal, "DD-MM-YYYY HH:mm").valueOf();
            var m1 = moment(ts1);
            var validFromTemp = new Date(m1);

            var validToLocal = userService.toLocalDate(validTo);
            var ts2 = moment(validToLocal, "DD-MM-YYYY HH:mm").valueOf();
            var m2 = moment(ts2);
            var validToTemp = new Date(m2);

            if (!$scope.checkValidFrom) {
                if (validFromTemp < CurrentDate) {
                    $scope.userValidityFromValidation = true;
                    return false;
                } else {
                    if (validFromTemp >= validToTemp) {
                        $scope.userValidityToValidation = true;

                        return false;
                    }
                }
            }

            else if ($scope.checkValidFrom) {
                if (validToTemp < CurrentDate || validFromTemp >= validToTemp) {
                    $scope.userValidityToValidation = true;

                    return false;
                }
            }

            if ($scope.isEditUser) {
                $scope.addnewuserobj.userID = userid;
            }

            $scope.addnewuserobj.userType = userService.getUserType();
            $scope.addnewuserobj.username = $scope.addnewuserobj.email;
            $scope.addnewuserobj.companyName = $scope.userObj.institution;
            $scope.addnewuserobj.currency = 0;//$scope.addnewuserobj.currency.key;
            $scope.addnewuserobj.isSubUser = userVal;
            $scope.addnewuserobj.isEditUser = $scope.isEditUser;
            $scope.addnewuserobj.oldEmail = $scope.temporaryObj.email;
            $scope.addnewuserobj.oldPhone = $scope.temporaryObj.phone;
            userService.addnewuser($scope.addnewuserobj)
                .then(function (response) {

                    if ($scope.addnewuserobj.isEditUser) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.addnewuserobj.phoneNum = $scope.addnewuserobj.phoneNumTemp;
                            $scope.addnewuserobj.email = $scope.addnewuserobj.emailTemp;
                            $scope.checkPhoneUniqueResult = false;
                            $scope.checkEmailUniqueResult = false;
                            $scope.checkSUBLOGINUniqueResult = false;
                            //if (response.errorMessage.includes("Email")) {
                            //    $scope.checkEmailUniqueResult = true;
                            //} else if (response.errorMessage.includes("Phone")) {
                            //    $scope.checkPhoneUniqueResult = true;
                            //} else if (response.errorMessage.includes("Login")) {
                            //    $scope.checkSUBLOGINUniqueResult = true;
                            //}
                        } else {
                            this.addUser = 0;
                            $scope.addnewuserobj = {};

                            growlService.growl("Updated successfully.", "success");
                            $scope.isEditUser = false;
                            $state.reload();
                        }
                    }
                    else {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {


                            // SUB USER DEPARTMENT AND DESIGNATION SELECTION // 
                            var UserDeptDesig = [];
                            if ($scope.addnewuserobj.dept && $scope.addnewuserobj.desig) {
                                $scope.addnewuserobj.dept.isValid = 1;
                                $scope.addnewuserobj.dept.isAssignedToUser = false;
                                $scope.addnewuserobj.dept.userID = response.objectID;
                                $scope.addnewuserobj.desig.isValid = 1;
                                $scope.addnewuserobj.desig.isAssignedToUser = false;
                                $scope.addnewuserobj.desig.userID = response.objectID;
                                $scope.addnewuserobj.dept.listDesignation = [];
                                $scope.addnewuserobj.dept.listDesignation.push($scope.addnewuserobj.desig);
                                UserDeptDesig.push($scope.addnewuserobj.dept);
                                $scope.AssignUserDepartmentDesignation(UserDeptDesig, false);
                            }
                            // SUB USER DEPARTMENT AND DESIGNATION SELECTION // 

                            this.addUser = 0;
                            $scope.addnewuserobj = {};
                            growlService.growl("User added successfully.", "success");
                            $state.reload();
                        }
                    }

                });
        };

        $scope.sendUniqueCall = true;
        $scope.checkUserUniqueResult = function (idtype, inputvalue) {
            $scope.loginValidation = false;
            if (inputvalue == "" || inputvalue == undefined) {
                $scope.loginValidation = true;
                return false;
            }

            if (!$scope.checkPhoneUniqueResult) {
                $scope.checkPhoneUniqueResult = false;
            }

            if (!$scope.checkEmailUniqueResult) {
                $scope.checkEmailUniqueResult = false;
            }


            if (!$scope.checkSUBLOGINUniqueResult) {
                $scope.checkSUBLOGINUniqueResult = false;
            }

            if ($scope.isEditUser == false) {
                userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                    if (idtype === "COMPANY") {
                        $scope.checkCompanyUniqueResult = !response;
                    }
                    else if (idtype === "PAN") {
                        $scope.checkPANUniqueResult = !response;
                    }
                    else if (idtype === "TIN") {
                        $scope.checkTINUniqueResult = !response;
                    }
                    else if (idtype === "STN") {
                        $scope.checkSTNUniqueResult = !response;
                    } else if (idtype == "PHONE") {
                        if ($scope.checkPhoneUniqueResult = !response) {
                            $scope.checkPhoneUniqueResult = !response;
                            $scope.isSaveDisabled = true;
                        } else {
                            $scope.isSaveDisabled = false;
                        }
                    }
                    else if (idtype == "EMAIL") {
                        if ($scope.checkEmailUniqueResult = !response) {
                            $scope.checkEmailUniqueResult = !response;
                            $scope.isSaveDisabled = true;
                        } else {
                            $scope.isSaveDisabled = false;
                        }
                    }
                    else if (idtype == "SUB_LOGIN_ID") {
                        if ($scope.checkSUBLOGINUniqueResult = !response) {
                            $scope.checkSUBLOGINUniqueResult = !response;
                            $scope.isLoginIDUniq = $scope.checkSUBLOGINUniqueResult;
                            $scope.isSaveDisabled = true;
                        } else {
                            $scope.isSaveDisabled = false;
                        }

                        //$scope.checkSUBLOGINUniqueResult = !response;
                        //$scope.isLoginIDUniq = $scope.checkSUBLOGINUniqueResult;
                    }

                    //$scope.isValidToSubmit = !($scope.checkCompanyUniqueResult || $scope.checkPANUniqueResult || $scope.checkPANUniqueResult || $scope.checkPhoneUniqueResult || $scope.checkEmailUniqueResult || $scope.checkSUBLOGINUniqueResult);
                });

            }
            else if ($scope.isEditUser === true) {
                if (idtype == "PHONE") {
                    if ($scope.temporaryObj.phone != inputvalue) {
                        $scope.sendUniqueCall = true;
                    } else {
                        $scope.sendUniqueCall = false;
                        $scope.checkPhoneUniqueResult = false;
                        $scope.isSaveDisabled = false;
                    }
                }

                if (idtype == "EMAIL") {
                    if ($scope.temporaryObj.email != inputvalue) {
                        $scope.sendUniqueCall = true;
                    } else {
                        $scope.sendUniqueCall = false;
                        $scope.checkEmailUniqueResult = false;
                        $scope.isSaveDisabled = false;
                    }
                }

                if (idtype == "SUB_LOGIN_ID") {
                    if ($scope.temporaryObj.loginID != inputvalue) {
                        $scope.sendUniqueCall = true;
                    } else {
                        $scope.sendUniqueCall = false;
                        $scope.checkSUBLOGINUniqueResult = false;
                        $scope.isSaveDisabled = false;
                    }
                }

                if (idtype == "COMPANY") {

                    $scope.checkCompanyUniqueResult = !response;
                }
                else if (idtype == "PAN") {
                    $scope.checkPANUniqueResult = !response;
                }
                else if (idtype == "TIN") {
                    $scope.checkTINUniqueResult = !response;
                }
                else if (idtype == "STN") {
                    $scope.checkSTNUniqueResult = !response;
                }

                if ($scope.sendUniqueCall) {
                    userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                        
                        if (idtype == "PHONE") {
                            if ($scope.checkPhoneUniqueResult = !response) {
                                $scope.checkPhoneUniqueResult = !response;
                                $scope.isSaveDisabled = true;
                            } else {
                                $scope.isSaveDisabled = false;
                            }


                        }
                        else if (idtype == "EMAIL") {
                            if ($scope.checkEmailUniqueResult = !response) {
                                $scope.checkEmailUniqueResult = !response;
                                $scope.isSaveDisabled = true;
                            } else {
                                $scope.isSaveDisabled = false;
                            }
                        }

                        else if (idtype == "SUB_LOGIN_ID") {
                            //if ($scope.checkSUBLOGINUniqueResult = !response) {
                            //    $scope.checkSUBLOGINUniqueResult = !response;
                            //    $scope.isLoginIDUniq = $scope.checkSUBLOGINUniqueResult;
                            //}
                            if ($scope.checkSUBLOGINUniqueResult = !response) {
                                $scope.checkSUBLOGINUniqueResult = !response;
                                $scope.isSaveDisabled = true;
                            } else {
                                $scope.isSaveDisabled = false;
                            }


                        }


                        //$scope.isValidToSubmit = !($scope.checkCompanyUniqueResult || $scope.checkPANUniqueResult || $scope.checkPANUniqueResult || $scope.checkPhoneUniqueResult || $scope.checkEmailUniqueResult || $scope.checkSUBLOGINUniqueResult);
                    });
                }
            }
        };



        //$scope.checkUserEmailUnique = function (idtype, inputvalue) {
        //    if (inputvalue == "" || inputvalue == undefined) {
        //        return false;
        //    }
        //    // $scope.checkPhoneUniqueResult = false;
        //    $scope.checkEmailUniqueResult = false;
        //    // $scope.checkCompanyUniqueResult = false;
        //    // $scope.checkPANUniqueResult = false;
        //    // $scope.checkTINUniqueResult = false;
        //    // $scope.checkSTNUniqueResult = false;
        //    userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
        //        if (idtype == "EMAIL") {
        //            $scope.checkEmailUniqueResult = !response;
        //        }
        //    });
        //};

        //$scope.disableTscBsc = false;

        $scope.loadAccess = function (action, userID, userName) {
            $scope.subuserentitlements = [];
            $scope.subuserentitlementstemp = [];
            $scope.subuserentitlementstemptest = [];
            $scope.subuserentitlementsRetainValuesTemp = [];
            $scope.subuserentitlementsonlytscbsc = [];
            $scope.showAccess = action;
            $scope.UserName = userName;
            if (action) {
                userService.getuseraccess(userID, userService.getUserToken())
                    .then(function (responseObj) {
                        if (responseObj && responseObj.length > 0) {
                            $scope.subuserentitlementstemp = responseObj;
                            //$scope.subuserentitlementstemptest = responseObj;
                            $scope.subuserentitlementstemp.forEach(function (item, index) {
                                item.isDisable = false;
                                if (item.acsID != 600000) {
                                    $scope.subuserentitlements.push(item);
                                    $scope.subuserentitlementstemptest = angular.copy($scope.subuserentitlements);
                                    //$scope.subuserentitlements.forEach(function (item2, index) {
                                    //    if (item2.isEntitled) {
                                    //        item.isDisable = true;
                                    //    }
                                    //})
                                } else {
                                    $scope.subuserentitlementsonlytscbsc.push(item);
                                }
                            })


                            $scope.disableValues();
                        }
                    })
            }
        };

        // start user validity 14/3/2019 Thu

        $scope.fromDisabled = false;
        $scope.userValidityFromDisabled = false;

        $scope.validateUser = function (action, userID, userName) {
            $scope.subuservalidity = [];
            $scope.showValidity = action;
            $scope.UserValidID = userID;
            $scope.UserName = userName;
            if (action) {
                userService.getuservalidity($scope.companyID, userService.getUserToken())
                    .then(function (responseObj) {
                        if (responseObj && responseObj.length > 0) {
                            $scope.subuservalidity = responseObj;
                            $scope.subuservalidity.forEach(function (item, itemindex) {

                                $scope.userValidityFromValidation = false;
                                $scope.userValidityToValidation = false;
                                item.userValidityFrom = userService.toLocalDate(item.dateFrom);
                                item.userValidityTo = userService.toLocalDate(item.dateTo);

                                if (String(item.userValidityFrom).includes('9999') || String(item.userValidityFrom).includes('1000') || String(item.userValidityFrom).includes('10000')) {
                                    item.userValidityFrom = '';
                                }

                                if (String(item.userValidityTo).includes('9999') || String(item.userValidityTo).includes('1000') || String(item.userValidityTo).includes('10000')) {
                                    item.userValidityTo = '';
                                }



                                if ($scope.UserValidID == item.userID) {
                                    $scope.subuservalidityParams.userValidityFrom = item.userValidityFrom;
                                    $scope.subuservalidityParams.userValidityTo = item.userValidityTo;

                                    // $scope.checkValidFrom = $scope.subuservalidityParams.userValidityFrom;
                                    // $scope.checkValidTo = $scope.subuservalidityParams.userValidityTo;

                                    if ($scope.checkValidFrom != '') {
                                        $scope.userValidityFromDisabled = true;
                                    } else {
                                        $scope.userValidityFromDisabled = false;
                                    }


                                }


                            })
                            // $scope.auctionItem.quotationFreezTime = userService.toLocalDate($scope.auctionItem.quotationFreezTime);
                        }
                    })
            }
        };
        // End user validity 

        //$scope.change = function () {

        //        $scope.isEditUser = false;

        //}

        // EDIT SUB USER START



        $scope.userDepartment = false;
        $scope.userDesignation = false;


        $scope.editUser = function (user) {
            $scope.addnewuserobj.phoneNumTemp = 0;
            $scope.addnewuserobj.emailTemp = '';
            $scope.userValidityFromValidation = false;
            $scope.userValidityToValidation = false;
            $scope.subUserID = user.userID;
            $scope.isEditUser = true;
            $scope.addnewuserobj = user;
            $scope.addnewuserobj.phoneNum = parseInt($scope.addnewuserobj.phoneNum);
            $scope.addnewuserobj.phoneNumTemp = $scope.addnewuserobj.phoneNum;
            $scope.addnewuserobj.emailTemp = $scope.addnewuserobj.email;

            $scope.addnewuserobj.companyLocation = angular.copy($scope.companyLocationSearch);
            $scope.addnewuserobj.companyLocationSearch = angular.copy($scope.companyLocationSearch);
            $scope.currencies.forEach(function (item, index) {
                if (item.value == $scope.addnewuserobj.currency) {
                    return $scope.addnewuserobj.currency = item;
                }
            })
            $scope.addnewuserobj.loginid = user.subUserLoginID;
            $scope.addnewuserobj.password1 = user.subUserPassword;

            $scope.temporaryObj.email = $scope.addnewuserobj.email;
            $scope.temporaryObj.phone = $scope.addnewuserobj.phoneNum;
            $scope.temporaryObj.loginID = $scope.addnewuserobj.loginid;

            //$scope.addnewuserobj.userValidityFrom = userService.toLocalDate($scope.addnewuserobj.dateFrom);
            //$scope.addnewuserobj.userValidityTo = userService.toLocalDate($scope.addnewuserobj.dateTo);

            $scope.addnewuserobj.userValidityFrom = user.dateFrom;
            $scope.addnewuserobj.userValidityTo = user.dateTo;

            $scope.checkValidFrom = $scope.addnewuserobj.userValidityFrom;
            $scope.checkValidTo = $scope.addnewuserobj.userValidityTo;

            $scope.addnewuserobj.dept = user.DEPARTMENT;
            $scope.addnewuserobj.desig = user.DESIGNATION;
            $scope.addnewuserobj.sapUserId = user.sapUserId;
            $scope.userValidityFromDisabled = true;
            $scope.userDepartment = true;
            $scope.userDesignation = true;

            $scope.loadLocation();
        };

        // EDIT SUB USER END



        $scope.cancel = function () {
            $scope.addnewuserobj = {};
            $scope.userValidityFromDisabled = false;
            $scope.isEditUser = false;
        }




        $scope.validateAccess = function (isEntitled, accessType, acsId) {
            if (accessType == 'View Requirement' && isEntitled == false) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = false;
                    }
                    if (item.accessType == 'Quotations Verification') {
                        item.isEntitled = false;
                    }
                    if (item.accessType == 'Negotiation Schedule') {
                        item.isEntitled = false;
                    }
                    if (item.accessType == 'Live Negotiation (Bidding Process)') {
                        item.isEntitled = false;
                    }
                    if (item.accessType == 'Po Generation') {
                        item.isEntitled = false;
                    }
                })
            };

            if (accessType == 'Requirement Posting' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true;
                    }
                });
            }

            if (accessType == 'Quotations Verification' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = true;
                    }
                });
            }

            if (accessType == 'Negotiation Schedule' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = true;
                    }

                    if (item.accessType == 'Quotations Verification') {
                        item.isEntitled = true;
                    }
                });
            }

            if (accessType == 'Live Negotiation (Bidding Process)' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = true;
                    }

                    if (item.accessType == 'Quotations Verification') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Negotiation Schedule') {
                        item.isEntitled = true;
                    }
                });
            }

            if (accessType == 'Po Generation' && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    if (item.accessType == 'View Requirement') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Requirement Posting') {
                        item.isEntitled = true;
                    }

                    if (item.accessType == 'Quotations Verification') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Negotiation Schedule') {
                        item.isEntitled = true;
                    }
                    if (item.accessType == 'Live Negotiation (Bidding Process)') {
                        item.isEntitled = true;
                    }
                });
            }

            if (acsId == 600000 && isEntitled == true) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    //if (item.accessType == 'View Requirement') {
                    //    item.isEntitled = true;
                    //}
                    //if (item.accessType == 'Requirement Posting') {
                    //    item.isEntitled = true;
                    //}

                    //if (item.accessType == 'Quotations Verification') {
                    //    item.isEntitled = true;
                    //}
                    //if (item.accessType == 'Negotiation Schedule') {
                    //    item.isEntitled = true;
                    //}
                    //if (item.accessType == 'Live Negotiation (Bidding Process)') {
                    //    item.isEntitled = true;
                    //}
                    //if (item.) {

                    //}
                    item.isEntitled = false;
                });
            } else if (acsId == 600000 && isEntitled == false) {
                $scope.subuserentitlements = [];
                $scope.subuserentitlementstemptest.forEach(function (item, index) {
                    if (item.isEntitled) {
                        item.isEntitled = true;
                    }
                })

                $scope.subuserentitlements = $scope.subuserentitlementstemptest;

            }

            $scope.disableValues();
        };


        $scope.tempsubuserentitlements = [];

        $scope.saveUserAccess = function () {

            //if ($scope.subuserentitlementsonlytscbsc[0].isEntitled) {
            //$scope.subuserentitlements.push($scope.subuserentitlementsonlytscbsc[0]);
            //}
            $scope.tempsubuserentitlements = [];

            $scope.tempsubuserentitlements = angular.copy($scope.subuserentitlements);
            $scope.tempsubuserentitlements.push($scope.subuserentitlementsonlytscbsc[0]);

            var params = {
                "listUserAccess": $scope.tempsubuserentitlements,
                "sessionID": userService.getUserToken()
            };

            userService.saveUserAccess(params)
                .then(function (response) {
                    if (response.errorMessage) {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {

                        growlService.growl("Access Levels Defined Successfully.", "success");
                        $scope.showAccess = false;
                    }
                })
        };


        $scope.userValidityFromValidation = $scope.userValidityToValidation = false;
        $scope.saveUserValidity = function () {

            $scope.userValidityFromValidation = $scope.userValidityToValidation = false;


            if ($scope.subuservalidityParams.userValidityFrom == null || $scope.subuservalidityParams.userValidityFrom == '') {
                $scope.userValidityFromValidation = true;
                return false;
            }

            if ($scope.subuservalidityParams.userValidityTo == null || $scope.subuservalidityParams.userValidityTo == '') {
                $scope.userValidityToValidation = true;
                return false;
            }



            var ts = userService.toUTCTicks($scope.subuservalidityParams.userValidityFrom);
            var m = moment(ts);
            var validFrom = new Date(m);
            var milliseconds = parseInt(validFrom.getTime() / 1000.0);
            $scope.subuservalidityParams.dateFrom = "/Date(" + milliseconds + "000+0530)/";


            var ts = userService.toUTCTicks($scope.subuservalidityParams.userValidityTo);
            var m = moment(ts);
            var validTo = new Date(m);
            var milliseconds = parseInt(validTo.getTime() / 1000.0);
            $scope.subuservalidityParams.dateTo = "/Date(" + milliseconds + "000+0530)/";




            auctionsService.getdate()
                .then(function (GetDateResponse) {
                    //var CurrentDate = moment(new Date(parseInt(GetDateResponse.substr(6))));


                    var CurrentDateToLocal = userService.toLocalDate(GetDateResponse);

                    var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                    var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));

                    //var currDate = CurrentDate._d;
                    //var m = moment(currDate);
                    //var currDate1 = new Date(m);
                    var validFromLocal = userService.toLocalDate(validFrom);
                    var ts = moment(validFromLocal, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var validFromTemp = new Date(m);

                    var validToLocal = userService.toLocalDate(validTo);
                    var ts = moment(validToLocal, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var validToTemp = new Date(m);


                    //var validFrom1 = userService.toLocalDate(validFrom);
                    // var validTo1 = userService.toLocalDate(validTo);
                    //if ($scope.checkValidFrom == '' || $scope.checkValidFrom == undefined) {
                    //    if (validFromTemp < CurrentDate) {
                    //        $scope.userValidityFromValidation = true;
                    //        return false;
                    //    } else {
                    //        if (validFromTemp >= validToTemp) {
                    //            $scope.userValidityToValidation = true;

                    //            return false;
                    //        }
                    //    }
                    //}

                    //else if ($scope.checkValidFrom != '' || $scope.checkValidFrom != undefined) {
                    //    if (validToTemp < CurrentDate || validFromTemp >= validToTemp) {
                    //        $scope.userValidityToValidation = true;

                    //        return false;
                    //    }
                    //}


                    var params = {
                        "userID": $scope.UserValidID,
                        "listUserValidity": $scope.subuservalidityParams,
                        "sessionID": userService.getUserToken()
                    };

                    userService.saveUserValidity(params)
                        .then(function (response) {

                            if (response.errorMessage) {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                                growlService.growl("Validity assigned Successfully.", "success");
                                $scope.showValidity = false;
                            }
                        })
                })
        }

        $scope.loadUserDepartments = function (action, userID, userName) {
            $scope.subUserDepartments = [];
            $scope.showUserDepartments = action;
            $scope.UserName = userName;
            if (action) {
                auctionsService.GetUserDepartments(userID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.subUserDepartments = response;
                        }
                    })
            }
        };


        $scope.SaveUserDepartments = function () {
            var params = {
                "listUserDepartments": $scope.subUserDepartments,
                "sessionID": userService.getUserToken()
            };
            auctionsService.SaveUserDepartments(params)
                .then(function (response) {
                    if (response.errorMessage) {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Access Levels Defined Successfully.", "success");
                        $scope.showUserDepartments = false;
                    }
                })
        };


        $scope.loadUserDesignations = function (action, userID, userName) {
            $scope.subUserDesignations = [];
            $scope.showUserDesignations = action;
            $scope.UserName = userName;
            if (action) {
                auctionsService.GetUserDesignations(userID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.subUserDesignations = response;
                        }
                    })
            }
        };


        $scope.SaveUserDesignations = function () {
            var params = {
                "listUserDesignations": $scope.subUserDesignations,
                "sessionID": userService.getUserToken()
            };
            auctionsService.SaveUserDesignations(params)
                .then(function (response) {
                    if (response.errorMessage) {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Access Levels Defined Successfully.", "success");
                        $scope.showUserDesignations = false;
                    }
                })
        };


        $scope.GetMyDesignations = function () {
            auctionsService.GetUserDesignations(userService.getUserId(), userService.getUserToken())
                .then(function (response) {
                    if (response && response.length > 0) {
                        $scope.myDesignations = response;
                        $scope.myDesignations = _.filter($scope.myDesignations, function (x) { return x.isValid == true; });
                    }
                })
        };

        //$scope.GetMyDesignations();


        //$scope.GetUserDeptDesig = function (action, userID, userName) {

        //    $scope.vendorID = userID;

        //    $scope.subUserDeptDesig = [];
        //    $scope.showUserDeptDesig = action;
        //    $scope.UserName = userName;
        //    if (action) {
        //        auctionsService.GetUserDeptDesig(userID, userService.getUserToken())
        //            .then(function (response) {
        //                if (response && response.length > 0) {
        //                    $scope.UserDeptDesig = response;
        //                }
        //            })
        //    }
        //};

        //$scope.GetUserDeptDesig();

        $scope.SaveUserDeptDesig = function (UserDeptDesig) {

            var params = {
                "listUserDeptDesig": UserDeptDesig,
                "sessionID": userService.getUserToken()
            };

            auctionsService.SaveUserDeptDesig(params)
                .then(function (response) {
                    if (response.errorMessage) {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Saved Successfully.", "success");
                        $scope.showUserDeptDesig = false;
                    }
                })
        };

        $scope.loadUserDeptDesig = function (action, userID, userName) {
            $scope.subUserDeptDesig = [];
            $scope.showUserDeptDesig = action;
            $scope.UserName = userName;
            if (action) {
                auctionsService.GetUserDeptDesig(userID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.subUserDeptDesig = response;
                        }
                    })
            }
        };

        // SUB USER DEPARTMENT AND DESIGNATION SELECTION //
        $scope.GetUserDepartmentDesignations = function (action, userID, userName, loadWidget) {
            // SUB USER DEPARTMENT AND DESIGNATION SELECTION //

            $scope.vendorID = userID;

            $scope.subUserDeptDesig = [];
            // SUB USER DEPARTMENT AND DESIGNATION SELECTION //
            $scope.showUserDeptDesig = loadWidget;
            // SUB USER DEPARTMENT AND DESIGNATION SELECTION //
            $scope.UserName = userName;
            if (action) {
                auctionsService.GetUserDepartmentDesignations(userID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.UserDeptDesig = response;
                        }
                    })
            }
        };


        $scope.selectDeptDesig = function (deptIndex, desigIndex) {
            $scope.UserDeptDesig.forEach(function (item, index) {
                item.listDesignation.forEach(function (item1, index1) {
                    if (desigIndex != index1) {
                        item1.isValid = false;
                    }
                });
            });



            $scope.UserDeptDesig.forEach(function (item, index) {
                item.isValid = false;
                item.listDesignation.forEach(function (item1, index1) {
                    if (item1.isValid == true) {
                        item.isValid = true;
                    }
                });
            });
        };

        // SUB USER DEPARTMENT AND DESIGNATION SELECTION //
        $scope.AssignUserDepartmentDesignation = function (UserDeptDesig, showConfirmation) {

            if (showConfirmation) {
                var validUser = _.find(UserDeptDesig, { isAssignedToUser: true });                
            }

            var params = {
                "listUserDeptDesig": UserDeptDesig,
                "sessionID": userService.getUserToken()
            };

            if (showConfirmation) {
                swal({
                    title: "Are You Sure ?",
                    //text: $scope.message,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                },
                    function () {
                        $scope.saveUserDepartmentDesignation(params, showConfirmation);
                    });
            }
            else {
                $scope.saveUserDepartmentDesignation(params, showConfirmation);
            }
        };

        $scope.saveUserDepartmentDesignation = function (params, showConfirmation) {
            var title = 'Success';
            var message = "Department Saved Successfully. Kindly Inform User to Re-Login to View the Latest Changes";
            var type = "success";

            auctionsService.SaveUserDepartmentDesignation(params)
                .then(function (response) {
                    if (response.errorMessage !== '') {
                        title = 'Error';
                        message = response.errorMessage;
                        type = "warning";
                    }

                    if (showConfirmation) {
                        setTimeout(function () {
                            swal({
                                title: title,
                                text: message,
                                type: type,
                                showCancelButton: false,
                                confirmButtonColor: "#F44336",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    if (response.errorMessage == '') {
                                        location.reload();
                                        $scope.showUserDeptDesig = false;
                                        var type = "success"
                                    }
                                });
                        }, 1000);
                    }
                });
        };
        // SUB USER DEPARTMENT AND DESIGNATION SELECTION //



        $scope.ispassword = false;
        $scope.loginAndPassSame = false;
        $scope.checkpassword = function () {
            if ($scope.addnewuserobj.password1)
            {
                $scope.passwordStrength = '';
                if (strongRegularExp.test($scope.addnewuserobj.password1))
                {
                    $scope.passwordStrength = 'Strong';
                    $scope.checkpwdStrength["color"] = "green";
                } else if (mediumRegularExp.test($scope.addnewuserobj.password1))
                {
                    $scope.passwordStrength = 'Medium';
                    $scope.checkpwdStrength["color"] = "orange";
                } else {
                    $scope.passwordStrength = 'Weak';
                    $scope.checkpwdStrength["color"] = "red";
                }

                if ($scope.addnewuserobj.loginid == $scope.addnewuserobj.password1)
                {
                    $scope.ispassword = true;
                    $scope.loginAndPassSame = true;
                    swal("Warning!", 'Login ID and Password cannot be same');
                }
            }
            else {
                $scope.ispassword = false;
                $scope.loginAndPassSame = false;
            }
        }




        $scope.disableValues = function () {
            $scope.temporaryArr = [];
            $scope.temporaryArr1 = [];
            $scope.subuserentitlements.forEach(function (item) {
                if (item.isEntitled) {
                    $scope.temporaryArr.push(item);
                }
            });

            $scope.subuserentitlementsonlytscbsc.forEach(function (item) {
                if (item.isEntitled) {
                    $scope.temporaryArr1.push(item);
                }
            });

            if ($scope.temporaryArr.length > 0) {
                $scope.subuserentitlementsonlytscbsc[0].isDisable = true;
            } else {
                $scope.subuserentitlementsonlytscbsc[0].isDisable = false;
            }


            if ($scope.temporaryArr1.length > 0) {
                $scope.subuserentitlements.forEach(function (item, index) {
                    item.isDisable = true;
                });
            } else {
                $scope.subuserentitlements.forEach(function (item, index) {
                    item.isDisable = false;
                });
            }



        };

        $scope.searchTable = function (value) {
            console.log($scope.searchKeyword)
            if (value) {
               
                $scope.subUsers = _.filter($scope.subUsers1, function (item) {
                    return (item.email.toUpperCase().indexOf(value.toUpperCase()) > -1 ||
                        (item.firstName + ' ' + item.lastName).toUpperCase().indexOf(value.toUpperCase()) > -1 ||
                        item.phoneNum.indexOf(value) > -1

                    );
                });
            } else {
                $scope.subUsers = $scope.subUsers1;
            }

            $scope.totalItems = $scope.subUsers.length;
        }

        $scope.searchTableLocation = function (str, locations) {
            str1 = str.toUpperCase();
            if ($scope.companyLocationSearch.length == 0) {
                $scope.LocationError = "No Locations available";
                $scope.isSaveDisabled = true;
            } else {
                $scope.addnewuserobj.companyLocation = $scope.addnewuserobj.companyLocationSearch.filter(function (req) {
                    if (String(req.configText).toUpperCase().includes(str1) == true) {
                        $scope.LocationError = '';
                        $scope.isSaveDisabled = false;
                        return (String(req.configText).toUpperCase().includes(str1) == true);
                    } else {
                        $scope.LocationError = 'Please select locations from dropdown only';
                        $scope.isSaveDisabled = true;
                    }

                });
            }


        }

        // (locationObj,location,isChecked)
        $scope.addnewuserobj.SelectedLocationArray = [];
        $scope.addnewuserobj.selectedLocations = '';
        $scope.addnewuserobj.selectedLocationsText = '';
        $scope.fillValue = function (locationObj, location, isChecked) {
            $scope.locationArray = [];
            $scope.tempLocationArray = [];
            var checkedLocations = _(locationObj)
                .filter(item => item.isChecked)
                .map('configValue')
                .value();
            var checkedLocationsText = _(locationObj)
                .filter(item => item.isChecked)
                .map('configText')
                .value();

            if (location.configValue == "ALL_LOCATIONS") {
                locationObj = locationObj.map(function (loc) {
                    loc.isChecked = true;
                    return loc;
                });
                var checkedLocations = _(locationObj)
                    .map('configValue')
                    .value();
                var checkedLocationsText = _(locationObj)
                    .map('configText')
                    .value();
            }
            if (checkedLocations.length < locationObj.length && location.configValue != "ALL_LOCATIONS") {
                locationObj = locationObj.map(function (loc) {
                    if (loc.configValue == "ALL_LOCATIONS")
                        loc.isChecked = false;
                });
            }
            if (checkedLocations.length < locationObj.length) {
                const index = checkedLocations.indexOf("ALL_LOCATIONS");
                if (index > -1) {
                    checkedLocations.splice(index, 1);
                }
            }
            if (checkedLocationsText.length < locationObj.length) {
                const index = checkedLocationsText.indexOf("ALL_LOCATIONS");
                if (index > -1) {
                    checkedLocationsText.splice(index, 1);
                }
            }
            $scope.addnewuserobj.selectedLocations = checkedLocations.join(',');

            $scope.addnewuserobj.selectedLocations1 = checkedLocationsText.join(',');
        };

        $scope.getBranches = function () {
            $scope.branchCodes = [];
            $http({
                method: 'get',
                url: domain + 'getregionhierarchy?compid=' + $scope.companyID + '&sessionid=' + $scope.sessionid,
                encodeuri: true,
                headers: { 'content-type': 'application/json' }
            }).then(function (response) {
                if (response && response.data && response.data.length > 0) {
                    $scope.branchCodes = response.data;
                }
            });
        };

        $scope.getBranches();
    });