﻿prmApp
    .controller('marginComparativesCtrl', ["$scope", "$state", "$log", "$stateParams", "userService", "auctionsService", "$window", "$timeout",
        function ($scope, $state, $log, $stateParams, userService, auctionsService, $window, $timeout) {
        $scope.reqId = $stateParams.reqID;

        $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
        if (!$scope.isCustomer) {
            $state.go('home');
        };

        $scope.userId = userService.getUserId();
        $scope.sessionid = userService.getUserToken();

        $scope.listRequirementTaxes = [];
        $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;


        $scope.getCustomerData = function (userId) {
            auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userId })
                .then(function (response) {
                    $scope.auctionItem = response;
                    $scope.auctionItem.auctionVendors = _.filter($scope.auctionItem.auctionVendors, function (x) { return x.companyName !== 'PRICE_CAP'; });
                    
                    if ($scope.auctionItem.auctionVendors.length > 0) {
                        $scope.getRequirementData();
                    };

                });
        }

        $scope.getVendorData = function (item) {
            auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': item.vendorID })
                .then(function (response) {
                    $scope.auctionItemVendor = response;

                    item.auctionItemVendor = $scope.auctionItemVendor;
                    
                    








                    item.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                        item.Gst = item.cGst + item.sGst + item.iGst;

                        if ($scope.auctionItem.isDiscountQuotation == 2) {


                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item = $scope.handlePrecision(item, 2);
                            item.Gst = item.cGst + item.sGst;
                            item = $scope.handlePrecision(item, 2);


                            //item.costPrice = (1 + (item.Gst / 100));                            
                            //item.costPrice = item.costPrice + (item.unitDiscount / 100);
                            //item.costPrice = item.unitMRP / item.costPrice;
                            //item.revCostPrice = item.unitMRP / (1 + (item.Gst / 100) + (item.revUnitDiscount / 100));



                            item.costPrice = (item.unitMRP * 100 * 100) / ((item.unitDiscount * 100) + 10000 + (item.unitDiscount * item.Gst) + (item.Gst * 100));
                            item.revCostPrice = (item.unitMRP * 100 * 100) / ((item.revUnitDiscount * 100) + 10000 + (item.revUnitDiscount * item.Gst) + (item.Gst * 100));

                            item.netPrice = item.costPrice * (1 + item.Gst / 100);
                            item.marginAmount = item.unitMRP - item.netPrice;
                            item = $scope.handlePrecision(item, 2);
                        }
                       
                    })












                        


                    $log.info(item.auctionItemVendor.listRequirementItems[0].productIDorName);

                });
        }

        $scope.enableWarranty = false;
        $scope.enablePayment = false;
        $scope.enableDelivery = false;
        $scope.enablePrice = false;
        $scope.enableOther = false;

        $scope.getRequirementData = function () {            
            $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                if (item.vendorID > 0) {
                    item.auctionItemVendor = [];
                    $scope.getVendorData(item);
                }

                if (item.warranty !== '' && item.warranty !== null) {
                    $scope.enableWarranty = true;
                }
                if (item.payment !== '' && item.payment !== null) {
                    $scope.enablePayment = true;
                }
                if (item.duration !== '' && item.duration !== null) {
                    $scope.enableDelivery = true;
                }
                if (item.validity !== '' && item.validity !== null) {
                    $scope.enablePrice = true;
                }
                if (item.otherProperties !== '' && item.otherProperties !== null) {
                    $scope.enableOther = true;
                }

            });
            //$log.info($scope.auctionItem);
        }

        $scope.getCustomerData($scope.userId);

        //$scope.auctionItem.auctionVendors.auctionItemVendor.auctionVendors[0].listRequirementItems.productIDorName;

        $scope.precisionRound = function (number, precision) {
            var factor = Math.pow(10, precision);
            return Math.round(number * factor) / factor;
        }

        $scope.handlePrecision = function (item, precision) {

            item.sGst = $scope.precisionRound(parseFloat(item.sGst), precision);
            item.cGst = $scope.precisionRound(parseFloat(item.cGst), precision);
            item.Gst = $scope.precisionRound(parseFloat(item.Gst), precision);
            item.unitMRP = $scope.precisionRound(parseFloat(item.unitMRP), precision);
            item.unitDiscount = $scope.precisionRound(parseFloat(item.unitDiscount), precision + 6);
            item.costPrice = $scope.precisionRound(parseFloat(item.costPrice), precision);
            item.revCostPrice = $scope.precisionRound(parseFloat(item.revCostPrice), precision);
            item.netPrice = $scope.precisionRound(parseFloat(item.netPrice), precision);
            item.marginAmount = $scope.precisionRound(parseFloat(item.marginAmount), precision);
            $scope.totalprice = $scope.precisionRound(parseFloat($scope.totalprice), precision);
            $scope.revtotalprice = $scope.precisionRound(parseFloat($scope.revtotalprice), precision);
            return item;
        };



        $scope.doPrint = false;

        $scope.printReport = function () {
            $scope.doPrint = true;
            $timeout(function () {
                $window.print();
                $scope.doPrint = false
            }, 1000);
        }

    }]);