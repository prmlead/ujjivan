﻿prmApp
    .controller('catalogProductAddCtrl', ['$scope', '$state', '$window', 'catalogService', 'userService', 'growlService', '$filter', 'fileReader',
        function ($scope, $state, $window, catalogService, userService, growlService, $filter, fileReader) {

            /* */

            $scope.compId = userService.getUserCompanyId();
            $scope.catalogCompId = userService.getUserCatalogCompanyId();
            $scope.companyItemUnits = [];

            $scope.VendorsList = [];
            $scope.VendorsTemp = [];
            $scope.VendorsTemp1 = [];
            $scope.selectedA = [];
            $scope.selectedB = [];
            $scope.productVendObj = {
                prodVend: []
                // prodVendTemp: []
            };


            $scope.product = {
                itemAttachments: [],
                contractManagement: []
            };

            $scope.searchingProducts = '';
            $scope.contractDetailsList =
            {
                pcId: 0,
                number: 0,
                value: 0,
                quantity: 0,
                availedQuantity: 0,
                startTime: '',
                endTime: '',
                U_ID: '',
                companyName: '',
                documents: '',
                errorColorNum: '1px solid #e4e7ea',
                errorColorVal: '1px solid #e4e7ea',
                errorColorQty: '1px solid #e4e7ea',
                errorColorSt: '1px solid #e4e7ea',
                errorColorNme: '1px solid #e4e7ea'
            };

            //$scope.product.contractManagement.push($scope.contractDetailsList);
            $scope.showMessage = function (input) {
                var show = input.$invalid && (input.$dirty || input.$touched);
                return show;
            };

            $scope.addNewProd = function () {

                $scope.productObj = {
                    prodId: 0,
                    compId: $scope.catalogCompId,
                    prodCode: $scope.product.productCode,
                    prodName: $scope.product.productName,
                    prodHSNCode: $scope.product.productHSNCode,
                    prodNo: $scope.product.productNo,
                    prodDesc: $scope.product.productDesc,
                    prodQty: $scope.product.productQuantityIn,
                    isValid: 1,
                    prodAlternateUnits: $scope.product.productAlternativeQuantityIn,
                    unitConversion: $scope.product.unitConversion,
                    shelfLife: $scope.product.productShelfLife,
                    productVolume: $scope.product.productWeightVolume,

                    productGST: $scope.product.productGST,
                    prefferedBrand: $scope.product.preferredBrand,
                    alternateBrand: $scope.product.alternateBrand,
                    totPurchaseQty: $scope.product.totalPurchaseQty,
                    inTransit: $scope.product.inTransit,
                    leadTime: $scope.product.leadTime,
                    departments: $scope.product.departments,
                    deliveryTerms: $scope.product.deliveryTerms,
                    termsConditions: $scope.product.termsConditions,

                    contractManagement: $scope.product.contractManagement,
                    casNumber: $scope.product.casNumber,
                    mfcdCode: $scope.product.mfcdCode,
                    city: $scope.product.city,
                    ModifiedBy: userService.getUserId(),
                    listVendorDetails: [],
                    multipleAttachments: $scope.product.itemAttachments,
                    productRate: $scope.product.productRate

                };

                $scope.productObj.contractManagement.forEach(function (item, index) {
                    if (!item.number) {
                        item.errorColorNum = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.value) {
                        item.errorColorVal = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.quantity) {
                        item.errorColorQty = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.startTime) {
                        item.errorColorSt = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.endTime) {
                        item.errorColorSe = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.companyName) {
                        item.errorColorNme = '1px solid red';
                        $scope.errorValidation = true;
                    }

                    if (item.startTime) {
                        let ts = userService.toUTCTicks(item.startTime);
                        let m = moment(ts);
                        let contractStartDate = new Date(m);
                        let milliseconds = parseInt(contractStartDate.getTime() / 1000.0);
                        item.startTime = "/Date(" + milliseconds + "000+0530)/";
                    }
                    if (item.endTime) {
                        let ts = userService.toUTCTicks(item.endTime);
                        let m = moment(ts);
                        let contractEndDate = new Date(m);
                        let milliseconds = parseInt(contractEndDate.getTime() / 1000.0);
                        item.endTime = "/Date(" + milliseconds + "000+0530)/";
                    }
                });

                $scope.searchvendorstring = '';
                //$scope.searchVendors('');

                var selectedVendors = $scope.vendorsList.filter(function (vendor) {
                    return (vendor.isAssignedToProduct == true || vendor.isAssignedToProduct > 0);
                });

                $scope.productObj.listVendorDetails = selectedVendors;

                var param = {
                    reqProduct: $scope.productObj,
                    sessionID: userService.getUserToken()
                };

                catalogService.addproduct(param)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("product Added Successfully.", "success");
                            $scope.goToProductEdit(response.repsonseId);
                            $scope.productId = response.repsonseId;
                            $scope.SaveProductCategories();
                        }
                    });
            };

            $scope.validateMatry = function () {
                $scope.prodEditDetails.contractManagement.forEach(function (item, index) {

                    if (item.number) {
                        item.errorColorNum = '1px solid #e4e7ea';
                    }
                    if (item.value) {
                        item.errorColorVal = '1px solid #e4e7ea';
                    }
                    if (item.quantity) {
                        item.errorColorQty = '1px solid #e4e7ea';
                    }
                    if (item.startTime) {
                        item.errorColorSt = '1px solid #e4e7ea';
                    }
                    if (item.endTime) {
                        item.errorColorSe = '1px solid #e4e7ea';
                    }
                    if (item.companyName) {
                        item.errorColorNme = '1px solid #e4e7ea';
                    }
                });
            };

            $scope.contractIndexValue = 0;
            $scope.contractIndex = function (value) {
                $scope.contractIndexValue = value;
            };

            $scope.addCatalogue = function () {
                $window.scrollBy(0, 50);
                $scope.itemnumber = $scope.product.contractManagement.length;
                $scope.contractDetailsList =
                {
                    pcId: 0,
                    number: 0,
                    value: 0,
                    quantity: 0,
                    availedQuantity: 0,
                    startTime: '',
                    endTime: '',
                    U_ID: '',
                    companyName: '',
                    documents: '',
                    errorColorNum: '1px solid #e4e7ea',
                    errorColorVal: '1px solid #e4e7ea',
                    errorColorQty: '1px solid #e4e7ea',
                    errorColorSt: '1px solid #e4e7ea',
                    errorColorNme: '1px solid #e4e7ea'
                };

                $scope.product.contractManagement.push($scope.contractDetailsList);
            };

            $scope.multipleitemsAttachments = [];

            $scope.getFile = function () {
                $scope.progress = 0;

                //$scope.file = $("#attachement")[0].files[0];
                $scope.multipleitemsAttachments = $("#attachement")[0].files;

                $scope.multipleitemsAttachments = Object.values($scope.multipleitemsAttachments);


                $scope.multipleitemsAttachments.forEach(function (item, index) {

                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };

                            var bytearray = new Uint8Array(result);

                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;

                            if (!$scope.product.itemAttachments) {
                                $scope.product.itemAttachments = [];
                            }

                            $scope.product.itemAttachments.push(fileUpload);

                        });
                });
            };

            $scope.SaveProductCategories = function () {
                var param = {
                    prodId: $scope.productId,
                    compId: $scope.catalogCompId,
                    catIds: $scope.selectedNodes,
                    user: userService.getUserId(),
                    sessionId: userService.getUserToken()
                };

                catalogService.updateProductCategories(param)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            // growlService.growl("product details saved Successfully.", "success");
                            //  $scope.getProductDetails();
                        }
                    });
            };

            $scope.getCompanyQtyItems = function () {
                catalogService.GetCompanyConfiguration($scope.catalogCompId, "ITEM_UNITS", userService.getUserToken())
                    .then(function (unitResponse) {
                        console.log('comp ids');
                        $scope.companyItemUnits = unitResponse;
                    });
            };
            $scope.getCompanyQtyItems();


            //// in catalogue product ctrl
            //$scope.GetCompanyVendors = function () {
            //    $scope.params =
            //        {
            //            "userID": userService.getUserId(),
            //            "sessionID": userService.getUserToken()
            //        }

            //        userService.GetCompanyVendors($scope.params)
            //            .then(function (response) {
            //                $scope.vendorsList = response;

            //                $scope.VendorsTemp1 = $scope.vendorsList;


            //                //for (var j in $scope.productVendObj.prodVend) {
            //                //    for (var i in $scope.vendorsList) {
            //                //        if ($scope.vendorsList[i].vendorName == $scope.productVendObj.prodVend[j].vendorName) {
            //                //            $scope.vendorsList.splice(i, 1);
            //                //        }
            //                //    }
            //                //}

            //                $scope.VendorsTemp = $scope.vendorsList;
            //                //$scope.searchVendors('');
            //            });
            //};

            //$scope.GetCompanyVendors();

            //$scope.selectForA = function (item) {
            //    var index = $scope.selectedA.indexOf(item);
            //    if (index > -1) {
            //        $scope.selectedA.splice(index, 1);
            //    } else {
            //        $scope.selectedA.splice($scope.selectedA.length, 0, item);
            //    }
            //    for (i = 0; i < $scope.selectedA.length; i++) {
            //        $scope.productVendObj.prodVend.push($scope.selectedA[i]);
            //        $scope.vendorsList.splice($scope.vendorsList.indexOf($scope.selectedA[i]), 1);
            //        $scope.VendorsTemp.splice($scope.VendorsTemp.indexOf($scope.selectedA[i]), 1);
            //    }
            //    $scope.reset();
            //}

            //$scope.selectForB = function (item) {
            //    var index = $scope.selectedB.indexOf(item);
            //    if (index > -1) {
            //        $scope.selectedB.splice(index, 1);
            //    } else {
            //        $scope.selectedB.splice($scope.selectedA.length, 0, item);
            //    }
            //  //  var data = [];
            //    for (i = 0; i < $scope.selectedB.length; i++) {
            //        $scope.vendorsList.push($scope.selectedB[i]);
            //        $scope.VendorsTemp.push($scope.selectedB[i]);
            //        $scope.productVendObj.prodVend.splice($scope.productVendObj.prodVend.indexOf($scope.selectedB[i]), 1);
            //    }
            //    $scope.reset();
            //}

            //$scope.reset = function () {
            //    $scope.selectedA = [];
            //    $scope.selectedB = [];
            //}



            $scope.searchvendorstring = '';
            $scope.totalItems1 = -1;
            $scope.searchVendors = function (value) {
                if (value) {
                    value = String(value).toUpperCase();
                    $scope.vendorsLoading = true;
                    $scope.searchingVendors = value;
                    var output = [];
                    output = $scope.GetCompanyVendors(0, $scope.searchingVendors);
                } else {
                    $scope.searchingVendors = '';
                    $scope.GetCompanyVendors(0, $scope.searchingVendors);
                }
            };

            $scope.searchvendorprodstring = '';
            $scope.totalItemsProd1 = -1;
            $scope.searchVendorsProd = function (value) {
                value = String(value).toUpperCase();
                $scope.vendorsListProduct = $scope.VendorsTemp1.filter(function (item) {
                    return (String(item.companyName).toUpperCase().includes(value) == true); //  || String(item.vendorCode).toUpperCase().includes(value) == true
                });
                $scope.totalItemsProd1 = $scope.vendorsListProduct.length;
                $scope.totalItemsProd = $scope.vendorsListProduct.length;
            };

            $scope.deleteContract = function (val) {

                $scope.product.contractManagement = $scope.product.contractManagement.filter(function (obj, index) {
                    return index != val;
                });
            };

            $scope.prodVendorSelected = function (userId, companyName) {
                $scope.product.contractManagement.forEach(function (item, index) {
                    if (index == $scope.contractIndexValue) {
                        item.companyName = companyName;
                        item.U_ID = userId;
                    }
                });
            };

            $scope.goToProductEdit = function (prodId) {
               
                    $state.go("productEdit", { "productId": prodId, "viewId": "edit" });
                
            };

            $scope.goToProductView = function (prodId) {
                
                    $state.go("productView", { "productId": prodId, "viewId": "view" });
                
            };


            // Pagination For Overall Vendors//
            $scope.page = 0;
            $scope.PageSize = 50;
            $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
            $scope.totalCount = 0;
            // Pagination For Overall Vendors//


            $scope.GetCompanyVendors = function (IsPaging, searchString) {
                if (searchString) {
                    $scope.fetchRecordsFrom = 0;
                }
                $scope.vendorsLoading = true;
                $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken(), "PageSize": $scope.fetchRecordsFrom, "NumberOfRecords": $scope.PageSize, "searchString": searchString ? searchString : "", PRODUCT_ID: +$scope.productId, IS_FROM: 'CATALOGUE', CHECKED_VENDORS: $scope.checkedVendors ? $scope.checkedVendors : "" };

                if (IsPaging === 1) {
                    if ($scope.page > -1) {
                        $scope.page++;
                        $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
                        $scope.params.PageSize = $scope.fetchRecordsFrom;
                        $scope.params.searchString = searchString ? searchString : "";
                    }

                    if ($scope.totalCount != $scope.vendorsList.length) {
                        userService.GetCompanyVendors($scope.params)
                            .then(function (response) {
                                if (response) {
                                    for (var a in response) {
                                        $scope.vendorsList.push(response[+a]);
                                    }
                                    $scope.displayFirst();
                                    $scope.VendorsTemp1 = $scope.vendorsList;
                                    $scope.vendorsLoading = false;
                                    $scope.totalCount = $scope.vendorsList[0].totalVendors;
                                }
                            });
                    }
                } else {
                    userService.GetCompanyVendors($scope.params)
                        .then(function (response) {
                            $scope.vendorsList = response;
                            $scope.displayFirst();
                            $scope.VendorsTemp1 = $scope.vendorsList;
                        });
                }
            };

            $scope.GetCompanyVendors();

            //$scope.GetProductVendors = function () {
            //    $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken() }
            //    userService.GetCompanyVendors($scope.params)
            //        .then(function (response) {
            //            $scope.vendorsList = response;
            //            $scope.vendorDisplayCollecton = [].concat($scope.vendorsList);
            //        });
            //};

            //$scope.GetProductVendors();




            //$scope.getFile = function () {
            //    $scope.file = $("#excelquotationproducts")[0].files[0];
            //    fileReader.readAsDataUrl($scope.file, $scope)
            //        .then(function (result) {
            //            var bytearray = new Uint8Array(result);
            //            $scope.entityObj.attachment = $.makeArray(bytearray);
            //            $scope.importEntity();
            //        });
            //}


            $scope.getcategories = function () {
                //catalogService.getcategories($scope.compId)
                catalogService.GetProductSubCategories(0, 0, $scope.catalogCompId)
                    .then(function (response) {
                        $scope.nodes = response;
                        $scope.getSelectedNodes();
                    });
            };

            $scope.getcategories();
            $scope.selectedCategories = '';
            $scope.getSelectedNodes = function () {
                $scope.selectedCategories = [];
                $scope.selectedNodes = '0';

                $scope.nodes.forEach(function (item, index) {
                    $scope.getSelectedNodesIteration(item, $scope.selectedCategories);
                });
            };

            $scope.getSelectedNodesIteration = function (selectedNode, selectedCat) {
                var selectedItem = { "catName": selectedNode.catName, "nodes": [] };
                if (selectedNode.nodeChecked) {

                    $scope.selectedNodes += ',' + selectedNode.catId;

                    if (selectedNode.nodes != null) {
                        selectedNode.nodes.forEach(function (item, index) {
                            if (item.nodeChecked) {
                                $scope.getSelectedNodesIteration(item, selectedItem.nodes);
                            }

                        });
                    }

                    selectedCat.push(selectedItem);
                }
            };

            $scope.checkChanged = function (selectedNode, selectedNodeScope) {
                $scope.selectChildNodes(selectedNode, selectedNode.nodeChecked);
                $scope.selectParentNode(selectedNode.nodeChecked, selectedNodeScope);
                $scope.getSelectedNodes();
            };

            $scope.selectChildNodes = function (childNode, ischecked) {
                childNode.nodeChecked = ischecked;
                childNode.nodes.forEach(function (item, index) {
                    $scope.selectChildNodes(item, ischecked);
                });
            };

            $scope.selectParentNode = function (ischecked, selectedNodeScope) {
                if (selectedNodeScope) {
                    var parentScope = selectedNodeScope.$parent;
                    if (parentScope && parentScope.node && parentScope.node.catId > 0) {
                        if (ischecked) {
                            parentScope.node.nodeChecked = ischecked;
                        }
                        else {
                            if (parentScope.node.nodes && parentScope.node.nodes.length > 0) {
                                if ($filter('filter')(parentScope.node.nodes, { 'nodeChecked': true }).length <= 0) {
                                    parentScope.node.nodeChecked = ischecked;
                                }
                            }
                            else {
                                parentScope.node.nodeChecked = ischecked;
                            }
                        }

                        $scope.selectParentNode(ischecked, parentScope);
                    }
                }
            };

            $scope.pushvendors = function () {
                var checkedvendorIDs = _($scope.vendorsList)
                    .filter(vendor => vendor.isAssignedToProduct)
                    .map('userID')
                    .value();
                $scope.checkedVendors = checkedvendorIDs.join(',');
            };


            $scope.displayFirst = function () {
                $scope.vendorsList = _.orderBy($scope.vendorsList, 'IS_SELECTED', 'desc');
                $scope.vendorsList.forEach(function (item, index) {
                    if (item.IS_SELECTED === 1) {
                        item.isAssignedToProduct = true;
                    }
                });
            };

            $scope.retrieveProductFromSAP = function (productcode) {
                console.log(productcode);
                catalogService.GetMaterialDetailsFromSAP({ productCode: productcode, sessionid: userService.getUserToken() })
                    .then(function (response) {
                        if (response && !response.errorMessage) {
                            console.log(response);
                            $scope.product = {
                                productCode: response.prodCode,
                                productName: response.prodName,
                                productHSNCode: response.prodHSNCode,
                                productNo: response.prodNo,
                                productDesc: response.prodDesc,
                                productQuantityIn: response.prodQty,
                                productAlternativeQuantityIn: response.prodAlternateUnits,
                                unitConversion: response.unitConversion,
                                casNumber: response.casNumber,
                                mfcdCode: response.mfcdCode,
                                listVendorDetails: [],
                                itemAttachments: [],
                                contractManagement: []
                            };
                        } else {
                            swal("Error!", "Could not find product with given Product Code", "error");
                        }
                    });
            };

        }]);



prmApp
    .controller('catalogProductsCtrl', ['$scope', '$state', '$window', 'catalogService', 'userService', '$filter', 'growlService', 'fileReader',
        function ($scope, $state, $window, catalogService, userService, $filter, growlService, fileReader) {

            /* */

            $scope.ExcelProducts = [];
            $scope.selectedIndex = -1;
            $scope.compId = userService.getUserCompanyId();
            $scope.catalogCompId = userService.getUserCatalogCompanyId();;
            $scope.showLeftMenu = true;
            var today = moment();
            $scope.propertyDate = today.add('days').format('YYYY-MM-DD');


            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                }
            };
            var listViewDisplayView = $window.localStorage.getItem("settings.catalogue.view");
            $scope.listView = function () {
                $window.localStorage.setItem("settings.catalogue.view", "list");
                $scope.listColor = '#377ba0';
                $scope.gridColor = '#313131';
                $scope.listViewDisplay = true;
                $scope.gridViewDisplay = false;
            }
            $scope.gridView = function () {
                $window.localStorage.setItem("settings.catalogue.view", "grid");
                $scope.gridColor = '#377ba0';
                $scope.listColor = '#313131';
                $scope.listViewDisplay = false;
                $scope.gridViewDisplay = true;
            }

            if (listViewDisplayView == "list") {
                $scope.listView();
            } else {
                $scope.gridView();
            }

            //$scope.totalItems = 0;
            //$scope.currentPage = 1;
            //$scope.currentPage2 = 1;
            //$scope.itemsPerPage = 10;
            //$scope.itemsPerPage2 = 10;
            //$scope.maxSize = 10;

            //$scope.setPage = function (pageNo) {
            //    $scope.currentPage = pageNo;
            //};

            //$scope.pageChanged = function () {
            //};

            // Pagination //
            $scope.loaderMore = false;
            $scope.scrollended = false;

            $scope.page = 0;
            var page = 0;
            $scope.PageSize = 500;
            var totalData = 0; 
            $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
            $scope.totalProducts = 0;
            // Pagination //

            $scope.products = [];
            $scope.products1 = [];
            
            $scope.regionArray =
                [
                    {
                        id: 0,
                        name: 'All'
                    },
                    {
                        id: 1,
                        name: 'India'
                    }
                ];

            $scope.searchRegion = $scope.regionArray[0].name;


            $scope.getProducts = function (IsPaging,searchString) {

                if (searchString || $scope.searchRegion) {
                    $scope.fetchRecordsFrom = 0;
                }

                //if ($scope.searchRegion && $scope.searchRegion != 'All') {
                //    $scope.fetchRecordsFrom = 0;
                //}

                if (IsPaging === 1)
                {
                    if ($scope.page > -1)
                    {
                        $scope.page++;
                        $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
                        $scope.loaderMore = true;
                        $scope.LoadMessage = ' Loading page ' + $scope.fetchRecordsFrom + ' of ' + $scope.PageSize + ' data...';
                        $scope.result = "color-green";
                        var paramCompId = $scope.catalogCompId;
                        if ($scope.totalProducts != $scope.products.length) {
                            catalogService.getProducts(paramCompId, $scope.fetchRecordsFrom, $scope.PageSize, searchString ? searchString : "", $scope.searchRegion != 'All' ? $scope.searchRegion : "")
                                .then(function (response) {
                                    totalData = response.length;
                                    if (totalData === 0) {
                                        //$scope.loaderMore = false;
                                        //$scope.scrollended = true;
                                        //$scope.LoadMessage = 'No more data...!';
                                        //$scope.result = "color-red";
                                    } else {
                                        for (var a in response) {
                                            $scope.products.push(response[+a]);
                                            $scope.products1.push(response[+a]);
                                        }
                                        $scope.loaderMore = false;
                                    }
                                    if ($scope.products && $scope.products.length > 0) {
                                        $scope.totalProducts = $scope.products[0].totalProducts;
                                    }

                                });
                        }
                    }
                }//unlimited load data while scroll
                else 
                {
                    //$scope.loaderMore = true;
                    //$scope.LoadMessage = ' Loading page ' + $scope.fetchRecordsFrom + ' of ' + $scope.PageSize + ' data...';
                    //$scope.result = "color-green";
                    
                    var paramCompId1 = $scope.catalogCompId;
                    catalogService.getProducts(paramCompId1, $scope.fetchRecordsFrom, $scope.PageSize, searchString ? searchString : "", $scope.searchRegion != 'All' ? $scope.searchRegion : "")
                        .then(function (response) {
                            $scope.products = response;

                            if ($scope.products && $scope.products.length > 0) {
                                $scope.totalProducts = $scope.products[0].totalProducts;
                            }


                        });
                }//default load data while pageload
            };
            $scope.getProducts();



            $scope.InactiveProduct = function (prod,type) {
                swal({
                    title: "Are You Sure!",
                    text: type == 'Inactive' ? 'Do You want to Inactivate this Item' : 'Do You want to Activate this Item',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    closeOnConfirm: true
                }, function () {
                    $scope.productdelObj = {
                        prodId: prod.prodId,
                        compId: $scope.compId,
                        prodCode: prod.prodCode,
                        prodName: prod.prodName,
                        isValid: type =='Inactive'?2:1,
                        ModifiedBy: userService.getUserId()
                    };
                    var param = {
                        reqProduct: $scope.productdelObj,
                        sessionID: userService.getUserToken()
                    }

                    catalogService.deleteProduct(param)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                if (type == 'Inactive') {
                                    growlService.growl("Product has been Inactivated Successfully", "success");
                                } else {
                                    growlService.growl("Product has been Activated Successfully", "success");
                                }
                                $scope.getProducts();
                            }
                        });
                });

            }

            $scope.goToProductView = function (prodId) {
               

                    $state.go("productView", { "productId": prodId, "viewId": "view" });
                
            };

            $scope.goToProductEdit = function (prodId) {
                $scope.checkProdEdit = 0;
                $scope.productEditObj = {
                    prodId: prodId,
                    compId: $scope.catalogCompId,
                    isValid: 0,
                    ModifiedBy: userService.getUserId()
                };
                var param = {
                    reqProduct: $scope.productEditObj,
                    sessionID: userService.getUserToken()
                };

                catalogService.isProdEditAllowed(param)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            $scope.checkProdEdit = response.repsonseId;
                            //  growlService.growl(response.errorMessage, "inverse");
                            
                                $state.go("productEdit", { "productId": prodId, "viewId": "edit" });
                            
                        }
                        else {
                            
                                $state.go("productEdit", { "productId": prodId, "viewId": "edit" });
                            
                        }
                    });
            };

            $scope.goToProductAdd = function () {
                $state.go("productAdd");
            };

            $scope.entityObj = {
                entityName: 'products',
                attachment: [],
                userid: userService.getUserId(),
                sessionID: userService.getUserToken()
            };


            $scope.getFile = function () {
                $scope.file = $("#excelquotationproducts")[0].files[0];
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        var bytearray = new Uint8Array(result);
                        $scope.entityObj.attachment = $.makeArray(bytearray);
                        $scope.importEntity();
                    });
            };

            $scope.importEntity = function () {

                var params = {
                    "entity": $scope.entityObj,
                    compId: $scope.catalogCompId,
                    user: userService.getUserId(),
                    sessionId: userService.getUserToken()
                };

                catalogService.importentity(params)
                    .then(function (response) {


                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        if (response.repsonseId > 0) {
                            growlService.growl(response.repsonseId + " products saved successfully.", "success");
                            location.reload();
                        }
                    });
            };

            $scope.getProperties = function () {
                $scope.Properties = [];

                catalogService.getProperties($scope.catalogCompId, 0, 99)
                    .then(function (response) {
                        $scope.PropertiesRaw = response;
                        //console.log(JSON.stringify($scope.properties));
                        $scope.PropertiesRaw.forEach(function (item, index) {
                            var Property = {
                                "propId": item.propId,
                                "propName": item.propName,
                                //"propDesc": item.propDesc,
                                "propDataType": item.propDataType,
                                //"propChecked": item.propValue == null ? false : true,
                                "propOptions": item.propOptions,
                                //"propValue": item.propDataType == "multi" ? (item.propValue == null ? item.propValue : item.propValue.split("^^")) : item.propValue,
                                //"propIsValid": item.isValid,
                                //"propModified": false,
                                //"propdateCreated": item.dateCreated,
                                //"propdateModified": item.dateModified
                                "propType": 'attr',
                            };

                            $scope.Properties.push(Property);
                        });
                        //$scope.Properties.push({
                        //    "propId": 0,
                        //    "propName": "Created Date",
                        //    "propDataType": "date",
                        //    "propOptions": "",

                        //});
                        //$scope.Properties.push({
                        //    "propId": 0,
                        //    "propName": "Modified Date",
                        //    "propDataType": "date",
                        //    "propOptions": "",

                        //});

                        $scope.Properties.push({
                            "propId": 0,
                            "propType": "cat",
                            "propName": "Categories",
                            "propDataType": "text",
                            "propOptions": "",

                        });
                        $scope.Properties.push({
                            "propId": 0,
                            "propType": "prod",
                            "propName": "Product Name",
                            "propDataType": "text",
                            "propOptions": "",

                        });
                        $scope.Properties.push({
                            "propId": 0,
                            "propType": "prod",
                            "propName": "Created",
                            "propDataType": "date",
                            "propOptions": "",

                        });
                        $scope.Properties.push({
                            "propId": 0,
                            "propType": "prod",
                            "propName": "Last Modified",
                            "propDataType": "date",
                            "propOptions": "",

                        });
                    });
            };

            $scope.getProperties();

            $scope.searchTable = function (str) {
                var filterText = angular.lowercase(str);

                $scope.searchingProducts = filterText;

                //console.log(filterText);
                //if (!str || str == '' || str == undefined || str == null) {
                //    $scope.products = $scope.products1;
                //}
                //else {
                //    $scope.products = $scope.products1.filter(function (products) {
                //        return (String(angular.lowercase(products.prodName)).includes(filterText) == true || String(angular.lowercase(products.prodNo)).includes(filterText) == true || String(angular.lowercase(products.prodHSNCode)).includes(filterText) == true);
                //    });
                //}
                //  $scope.totalItems = $scope.products.length;

                $scope.getProducts(0, $scope.searchingProducts);
            };

            $scope.showLeftMenuFn = function () {
                $scope.showLeftMenu = !$scope.showLeftMenu;
            };

            $scope.valueChanged = function (val) {
                $scope.selectedPropertyval = val;
            };

            $scope.filter = [];
            $scope.filterObj = {};
            $scope.savefilters = function () {

                $scope.filterObj = {
                    filterType: $scope.selectedProperty.propType,
                    filterField: $scope.selectedProperty.propName,
                    filterCond: $scope.selectedPropertyCond,
                    filterValue: $scope.selectedPropertyval,
                    filterFieldDataType: $scope.selectedProperty.propDataType
                };

                if ($scope.selectedIndex >= 0) {
                    $scope.filter[$scope.selectedIndex] = $scope.filterObj;
                }
                else {
                    $scope.filter.push($scope.filterObj);
                }

                // get products
                $scope.getproductsbyfilter();
                $scope.selectedIndex = -1;
                $scope.clearSelections();

            };

            $scope.clearSelections = function () {
                $scope.selectedProperty = null;
                $scope.selectedPropertyCond = null;
                $scope.selectedPropertyval = null;
                $scope.selectedPropertyval1 = null;
                $scope.selectedPropertyval2 = null;
                $scope.selectedPropertyval3 = null;
                $scope.selectedPropertyval4 = null;
                $scope.selectedPropertyval5 = null;
            };

            $scope.showfilterResetBtn = false;
            $scope.getproductsbyfilter = function () {

                var param = {
                    compId: $scope.catalogCompId,
                    sessionId: userService.getUserToken(),
                    prodFilters: $scope.filter
                }

                catalogService.getproductsbyfilter(param)
                    .then(function (response) {
                        $scope.products1 = response;
                        $scope.products = response;
                        if ($scope.products.length > 0) {
                            $scope.productsLoaded = true;
                            $scope.totalItems = $scope.products.length;
                        } else {
                            $scope.productsLoaded = false;
                            $scope.totalItems = 0;


                        }

                    });

                if ($scope.filter.length > 0) {
                    $scope.showfilterResetBtn = true;
                }
                else {
                    $scope.showfilterResetBtn = false;
                }
            };

            $scope.addfilter = function () {

                $scope.selectedProperty = '',
                    $scope.selectedPropertyCond = '',
                    $scope.selectedPropertyval = '';
            };

            $scope.removefilter = function (item) {
                var index = $scope.filter.indexOf(item);
                $scope.filter.splice(index, 1);

                $scope.getproductsbyfilter();

            };

            $scope.editFilter = function (item, index) {

                $scope.selectedProperty.propName = item.filterField;
                $scope.selectedPropertyCond = item.filterCond;
                $scope.selectedPropertyval = item.filterValue;
                $scope.selectedIndex = index;

            };


            $scope.resetFilter = function () {
                $scope.filter = [];
                $scope.getproductsbyfilter();

            };



            $scope.exportTemplateToExcel = function () {
                var mystyle = {
                    sheetid: 'products',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                alasql('SELECT "prodName" as [ProductName], "ITEM_NO" as [ProductNo], "ITEM_HSNCODE" as [HSNCode], "ITEM_UNITS" as [QuantityUnits], "ITEM_DESC" as [ProductDescription], "ITEM_CATEGORIES" as [ProductCategories],"ALTERNATIVE_UNITS" as [AlternativeQuantityUnits],"SHELF_LIFE" as [ShelfLife] INTO XLSX(?,{headers:true,sheetid: "products", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["products.xlsx", $scope.ExcelProducts]);
                //"UNIT_CONVERSION" as [UnitConversion],"PRODUCT_VOLUME" as [ProductVolume],
            };

            $scope.searchBasedOnRegion = function (searchVal) {
                if (searchVal) {
                    $scope.fetchRecordsFrom = 0;
                    $scope.searchRegion = searchVal;
                    $scope.getProducts(0, $scope.searchingProducts);
                }
            };

            $scope.showIsSuperUser = function () {
                if (userService.getUserObj().isSuperUser) {
                    return true;
                } else {
                    return false;
                }
            }

        }]);

