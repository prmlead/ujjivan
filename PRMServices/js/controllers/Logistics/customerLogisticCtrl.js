﻿prmApp

    // #region =========================================================================
    // AUCTION ITEM
    // #endregion =========================================================================

    .controller('customerLogisticCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state", "$timeout", "auctionsService",
        "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog", "reportingService", "logisticServices", "logisticsHubName",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, logisticServices, logisticsHubName) {

            // #region Initializers

            var liveId = "";
            var id = $scope.reqId = $stateParams.Id;
            auctionsService.GetIsAuthorized(userService.getUserId(), $scope.reqId, userService.getUserToken())
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                        $state.go('home');
                        return false;
                    }
                });
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            $scope.signalRCustomerAccess = false;
            $scope.Loding = false;
            $scope.makeaBidLoding = false;
            $scope.showTimer = false;
            $scope.userIsOwner = false;
            $scope.sessionid = userService.getUserToken();
            $scope.allItemsSelected = true;
            $scope.RevQuotationfirstvendor = "";
            $scope.disableBidButton = false;
            $scope.nonParticipatedMsg = '';
            $scope.quotationRejecteddMsg = '';
            $scope.quotationNotviewedMsg = '';
            $scope.revQuotationRejecteddMsg = '';
            $scope.revQuotationNotviewedMsg = '';
            $scope.quotationApprovedMsg = '';
            $scope.revQuotationApprovedMsg = '';
            $scope.reduceBidAmountNote = '';
            $scope.incTaxBidAmountNote = '';
            $scope.noteForBidValue = '';
            $scope.disableDecreaseButtons = true;
            $scope.disableAddButton = true;
            $scope.NegotiationEnded = false;
            $scope.uploadQuotationButtonMsg = 'The Submit Quotation option would be available only if prices are entered above.';
            $scope.uploadQuotationTaxValidationMsg = 'Please check all Tax Feilds';
            $scope.currentID = -1;
            $scope.timerStyle = { 'color': '#000' };
            $scope.savingsStyle = { 'color': '#228B22' };
            $scope.restartStyle = { 'color': '#f00' };
            $scope.bidAttachement = [];
            $scope.bidAttachementName = "";
            $scope.bidAttachementValidation = false;
            $scope.bidPriceEmpty = false;
            $scope.bidPriceValidation = false;
            $scope.showStatusDropDown = false;
            $scope.vendorInitialPrice = 0;
            $scope.showGeneratePOButton = false;
            $scope.isDeleted = false;
            $scope.ratingForVendor = 0;
            $scope.ratingForCustomer = 0;
            $scope.vendorApprovals = [];
            var requirementHub;
            $scope.auctionItem = {
                auctionVendors: [],
                listRequirementItems: [],
                listRequirementTaxes: []
            }
            $scope.checkConnection = function () {
                if (requirementHub) {
                    return requirementHub.getStatus();
                } else {
                    return 0;
                }
            };
            var requirementData = {};
            $scope.quotationAttachment = null;
            $scope.days = 0;
            $scope.hours = 0;
            $scope.mins = 0;
            $scope.NegotiationSettings = {
                negotiationDuration: ''
            };
            $scope.divfix = {};
            $scope.divfix3 = {};
            $scope.divfix1 = {};
            $scope.divfix2 = {};
            $scope.divfixMakeabid = {};
            $scope.divfixMakeabidError = {};
            $scope.boxfix = {};
            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
            $scope.auctionItem.reqType = 'REGULAR';
            $scope.auctionItem.priceCapValue = 0;
            $scope.auctionItem.isUnitPriceBidding = 1;
            $scope.newPriceToBeQuoted = 0;
            $scope.formRequest = {
                selectedVendor: {},
                priceCapValue: 0,
                priceCapValueMsg: ''
            };
            $scope.makeABidDisable = false;
            $scope.vendorBidPrice = 0;
            $scope.revvendorBidPrice = 0;
            $scope.auctionStarted = true;
            $scope.customerBtns = true;
            $scope.showVendorTable = true;
            $scope.toprankerName = "";
            $scope.vendorRank = 0;
            $scope.quotationUrl = "";
            $scope.revquotationUrl = "";
            $scope.vendorBtns = false;
            $scope.vendorQuotedPrice = 0;
            $scope.startBtns = false;
            $scope.commentsvalidation = false;
            $scope.enableMakeBids = false;
            $scope.price = "";
            $scope.startTime = '';
            $scope.customerID = userService.getUserId();
            $scope.priceSwitch = 0;
            $scope.poDetails = {};
            $scope.bidHistory = {};
            $scope.vendorID = 0;
            $scope.validity = '';
            $scope.warranty = 'As Per OEM';
            $scope.duration = $scope.auctionItem.deliveryTime;
            $scope.payment = $scope.auctionItem.paymentTerms;
            $scope.isQuotationRejected = -1;
            $scope.starttimecondition1 = 0;
            $scope.starttimecondition2 = 0;
            $scope.isL1RevQuotation = false;
            $scope.isL1Quotation = false;
            $scope.notviewedcompanynames = '';
            $scope.customerListRequirementTerms = [];
            $scope.customerDeliveryList = [];
            $scope.customerPaymentlist = [];
            $scope.listTerms = [];
            $scope.listRequirementTerms = [];
            $scope.deliveryRadio = false;
            $scope.deliveryList = [];
            $scope.paymentRadio = false;
            $scope.paymentlist = [];
            $scope.AmountSaved = 0;
            $scope.updatedeliverydateparams = {
                date: ''
            };
            $scope.updatepaymentdateparams = {
                date: ''
            };
            $scope.Loding = false;
            $scope.priceCapError = false;
            $scope.makeaBidLoding = false;
            $scope.vendorsFromPRM = 1;
            $scope.vendorsFromSelf = 2;
            $scope.totalprice = 0;
            $scope.taxs = 0;
            $scope.vendorTaxes = 0;
            $scope.freightcharges = 0;
            $scope.discountAmount = 0;
            $scope.totalpriceinctaxfreight = 0;
            $scope.vendorBidPriceWithoutDiscount = 0;
            $scope.revtotalprice = 0;
            $scope.revtaxs = 0;
            $scope.revvendorTaxes = $scope.vendorTaxes;
            $scope.revfreightcharges = 0;
            $scope.revtotalpriceinctaxfreight = 0;
            $scope.priceValidationsVendor = '';
            $scope.calculatedSumOfAllTaxes = 0;
            $scope.taxValidation = false;
            $scope.ItemPriceValidation = false;
            $scope.discountfreightValidation = false;
            $scope.gstValidation = false;
            $scope.taxEmpty = false;
            $scope.rejectreson = '';
            $scope.rejectresonValidation = false;
            $scope.QuatationAprovelvalue = true;
            $scope.submitButtonShow = 0;
            $scope.NegotiationSettingsValidationMessage = '';
            $scope.vendorIndex = false;
            $scope.listReqItems = [];
            $scope.popupVendor = [];
            $scope.TotalAirlinesCost = 0;
            $scope.TotalItemsValidAirlines = 0;
            $scope.SelectedAirlines = 0;
            $scope.deletedAirlines = [];
            $scope.enableAll = true;       
            $scope.isRestartDisable = false;

          
            $scope.starreturn = false;

            // #endregion Initializers

            // #region Functions

            $scope.getData = function (methodName, callerID) {

                var params = {
                    reqid: $stateParams.Id,
                    sessionid: userService.getUserToken(),
                    userid: userService.getUserId()
                }

                logisticServices.GetRequirementData(params)
                    .then(function (response) {
                        console.log('=======response========');
                        console.log(response);
                        console.log('=======response========');
                        $scope.setAuctionInitializer(response, methodName, callerID);
                    });
            };

            $scope.setAuctionInitializer = function (response, methodName, callerID) {

                $scope.auctionItem = response;

                $scope.notviewedcompanynames = '';
                $scope.isRestartDisable = false;
                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    if (item.companyName == 'PRICE_CAP') {
                        if (index == 1 && item.isRevQuotationRejected == 0) {
                            $scope.isRestartDisable = true;
                        }
                    } else {
                        if (index == 0 && item.isRevQuotationRejected == 0) {
                            $scope.isRestartDisable = true;
                        }
                    }

                    item.quotationStatus = '';
                    if ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')
                    {
                        if (item.isQuotationRejected < 0) {
                            item.quotationStatusStyle = { 'color': '#e62739' };
                            item.quotationStatus = 'PENDING';
                        }
                        else if (item.isQuotationRejected == 0)
                        {
                            item.quotationStatusStyle = { 'color': '#67bd6a' };
                            item.quotationStatus = 'APPROVED';
                        }
                        else if (item.isQuotationRejected > 0)
                        {
                            item.quotationStatusStyle = { 'color': '#e62739' };
                            item.quotationStatus = 'REJECTED';
                        }
                    }
                    if ($scope.auctionItem.status == 'Negotiation Ended')
                    {
                        if (item.isRevQuotationRejected < 0) {
                            item.quotationStatusStyle = { 'color': '#e62739' };
                            item.quotationStatus = 'PENDING';
                        }
                        else if (item.isRevQuotationRejected == 0) {
                            item.quotationStatusStyle = { 'color': '#67bd6a' };
                            item.quotationStatus = 'APPROVED';
                        }
                        else if (item.isRevQuotationRejected > 0) {
                            item.quotationStatusStyle = { 'color': '#e62739' };
                            item.quotationStatus = 'REJECTED';
                        }
                    }

                    if (item.quotationUrl != '' && item.isQuotationRejected == -1) {
                        $scope.Loding = false;
                        $scope.notviewedcompanynames += item.companyName + ', ';
                        $scope.starreturn = true;
                    }
                })

             

                if (!$scope.auctionItem.multipleAttachments) {
                    $scope.auctionItem.multipleAttachments = [];
                }
                $scope.auctionItem.attFile = response.attachmentName;
                if ($scope.auctionItem.attFile != '' && $scope.auctionItem.attFile != null && $scope.auctionItem.attFile != undefined) {


                    var attchArray = $scope.auctionItem.attFile.split(',');

                    attchArray.forEach(function (att, index) {

                        var fileUpload = {
                            fileStream: [],
                            fileName: '',
                            fileID: att
                        };

                        $scope.auctionItem.multipleAttachments.push(fileUpload);
                    })

                }


                $scope.notviewedcompanynames = $scope.notviewedcompanynames.substring(0, $scope.notviewedcompanynames.length - 2);

                // #region Negotiation Settings
                $scope.NegotiationSettings = $scope.auctionItem.NegotiationSettings;
                var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                $scope.days = parseInt(duration[0]);
                duration = duration[1];
                duration = duration.split(":", 4);
                $scope.hours = parseInt(duration[0]);
                $scope.mins = parseInt(duration[1]);
                $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                // #endregion Negotiation Settings

                $scope.auctionItem.quotationFreezTime = new moment($scope.auctionItem.quotationFreezTime).format("DD-MM-YYYY HH:mm");
                $scope.auctionItem.expStartTime = new moment($scope.auctionItem.expStartTime).format("DD-MM-YYYY HH:mm");

                $scope.description = $scope.auctionItem.description.replace(/\u000a/g, "</br>");
                $scope.deliveryLocation = $scope.auctionItem.deliveryLocation.replace(/\u000a/g, "</br>");
                $scope.paymentTerms = $scope.auctionItem.paymentTerms.replace(/\u000a/g, "</br>");
                $scope.deliveryTime = $scope.auctionItem.deliveryTime.replace(/\u000a/g, "</br>");

                var id = parseInt(userService.getUserId());
                var result = $scope.auctionItem.auctionVendors.filter(function (obj) {
                    return obj.vendorID == id;
                });
                if (id != $scope.auctionItem.customerID && id != $scope.auctionItem.superUserID && !$scope.auctionItem.customerReqAccess && result.length == 0) {
                    swal("Access denied", "You do not have access to this requirement because you are not part of this requirements process.", 'error');
                    $state.go('home');
                } else {
                    $scope.setFields();
                }

               
            };

            $scope.setFields = function () {
                if ($scope.auctionItem.status == "CLOSED") {
                    $scope.mactrl.skinSwitch('green');
                    if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                        $scope.errMsg = "Negotiation has been completed. You can generate the Purchase Order by pressing the button below.";
                    }
                    else {
                        $scope.errMsg = "Negotiation has completed.";
                    }
                    $scope.showStatusDropDown = false;
                    $scope.showGeneratePOButton = true;
                } else if ($scope.auctionItem.status == "UNCONFIRMED" || $scope.auctionItem.status == "NOTSTARTED") {
                    $scope.mactrl.skinSwitch('teal');
                    $scope.errMsg = "Negotiation has not started yet.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = false;
                    $scope.timeLeftMessage = "Negotiation Starts in: ";
                    $scope.startBtns = true;
                    $scope.customerBtns = false;
                } else if ($scope.auctionItem.status == "STARTED") {
                    $scope.mactrl.skinSwitch('orange');
                    $scope.errMsg = "Negotiation has started.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = true;

                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                    $scope.timeLeftMessage = "Negotiation Ends in: ";
                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                    $scope.startBtns = false;
                    $scope.customerBtns = true;
                } else if ($scope.auctionItem.status == "DELETED") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "This requirement has been cancelled.";
                    $scope.showStatusDropDown = false;
                    $scope.isDeleted = true;
                } else if ($scope.auctionItem.status == "Negotiation Ended") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "Negotiation has been completed.";
                    $scope.showStatusDropDown = false;
                } else if ($scope.auctionItem.status == "Vendor Selected") {
                    $scope.mactrl.skinSwitch('bluegray');
                    if ($scope.auctionItem.isTabular) {
                        $scope.errMsg = "Please select vendors for all items in order to provide Purchase Order Information.";
                    } else {
                        $scope.errMsg = "Please click the button below to provide the Purchase Order information.";
                    }
                    $scope.showStatusDropDown = false;
                } else if ($scope.auctionItem.status == "PO Processing") {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.errMsg = "The PO has been generated. Please find the PO here: ";
                    $scope.showStatusDropDown = false;
                } else {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.showStatusDropDown = true;
                }
                if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                    $scope.userIsOwner = true;
                    if ($scope.userIsOwner && !$rootScope.isUserEntitled(546654) && $scope.auctionItem.status == "STARTED") {
                        swal("Error!", "Live negotiation Access Denined", "error");
                        $state.go('home');
                    }
                    if ($scope.userIsOwner && !$rootScope.isUserEntitled(591159)) {
                        swal("Error!", "View Requirement Access Denined", "error");
                        $state.go('home');
                    }
                    $scope.options = ['PO Sent', 'Material Received', 'Payment Processing', 'Payment Released'];
                    $scope.options.push($scope.auctionItem.status);
                }
                var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                auctionsService.GetDateLogistics()
                    .then(function (responseFromServer) {
                        var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                        $log.debug(dateFromServer);
                        var curDate = dateFromServer;

                        var myEpoch = curDate.getTime();
                        $scope.timeLeftMessage = "";
                        if (start > myEpoch) {
                            $scope.auctionStarted = false;
                            $scope.timeLeftMessage = "Negotiation Starts in: ";
                            $scope.startBtns = true;
                            $scope.customerBtns = false;
                        } else {
                            $scope.auctionStarted = true;

                            // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                            $scope.timeLeftMessage = "Negotiation Ends in: ";
                            // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                            $scope.startBtns = false;
                            $scope.customerBtns = true;
                        }
                        if ($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() && !$scope.auctionItem.customerReqAccess) {
                            $scope.startBtns = false;
                            $scope.customerBtns = false;
                        }
                        if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 1) {
                            $scope.showTimer = false;
                            $scope.disableButtons();
                        } else {
                            $scope.showTimer = true;
                        }
                        var date = $scope.auctionItem.postedOn.split('+')[0].split('(')[1];
                        var newDate = new Date(parseInt(parseInt(date)));
                        $scope.auctionItem.postedOn = newDate.toString().replace('Z', '');

                        $scope.postedOnForExcel = new moment($scope.auctionItem.postedOn).format("DD-MM-YYYY HH:mm");
                        $scope.startTimeForExcel = new moment($scope.auctionItem.startTime).format("DD-MM-YYYY HH:mm");
                        $scope.endTimeForExcel = new moment($scope.auctionItem.endTime).format("DD-MM-YYYY HH:mm");

                        var minPrice = 0;
                        if ($scope.auctionItem.status == "NOTSTARTED") {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].initialPrice : 0;
                        } else {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].runningPrice : 0;
                        }
                        for (var i in $scope.auctionItem.auctionVendors) {

                            $scope.nonParticipatedMsg = '';
                            $scope.quotationRejecteddMsg = '';
                            $scope.quotationNotviewedMsg = '';
                            $scope.revQuotationRejecteddMsg = '';
                            $scope.revQuotationNotviewedMsg = '';
                            $scope.quotationApprovedMsg = '';
                            $scope.revQuotationApprovedMsg = '';

                            var vendor = $scope.auctionItem.auctionVendors[i]
                            
                            if (i == 0 && vendor.initialPrice != 0) {
                                minPrice = vendor.initialPrice;
                            } else {
                                if (vendor.initialPrice < minPrice && vendor.initialPrice != 0) {
                                    minPrice = vendor.initialPrice;
                                }
                            }
                            $scope.vendorInitialPrice = minPrice;
                            var runningMinPrice = 0;
                            if ($scope.auctionItem.auctionVendors[i].runningPrice > 0 && $scope.auctionItem.auctionVendors[i].runningPrice < $scope.vendorInitialPrice) {
                                runningMinPrice = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            if ($scope.auctionItem.auctionVendors[i].runningPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].runningPrice = 'NA';
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = 'NA';
                                $scope.auctionItem.auctionVendors[i].rank = 'NA';
                            } else {
                                $scope.vendorRank = vendor.rank;
                                if (vendor.rank == 1) {
                                    $scope.toprankerName = vendor.vendorName;
                                    if (userService.getUserId() == vendor.vendorID) {
                                        $scope.options = ['PO Accepted', 'Material Dispatched', 'Payment Acknowledged'];
                                        $scope.options.push($scope.auctionItem.status);
                                        if ($scope.auctionItem.status == "STARTED") {
                                            $scope.enableMakeBids = true;
                                        }
                                    }
                                }
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            if ($scope.auctionItem.auctionVendors[i].initialPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].initialPrice = 'NA';
                                $scope.nonParticipatedMsg = 'You have missed an opportunity to participate in this Negotiation.';
                            }
                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 1) {
                                $scope.quotationRejecteddMsg = 'Your quotation rejected by the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == -1) {
                                $scope.quotationNotviewedMsg = 'Your quotation submitted to the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 1) {
                                $scope.revQuotationRejecteddMsg = 'Your Rev.Quotation rejected by the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == -1) {
                                $scope.revQuotationNotviewedMsg = 'Your Rev.Quotation submitted to the customer.';
                            }

                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 0) {
                                $scope.quotationApprovedMsg = 'Your Quotation Approved by the customer.';
                            }
                            if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 0) {
                                $scope.revQuotationApprovedMsg = 'Your Rev.Quotation Approved by the customer.';
                            }

                        }
                        $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.timeLeft);
                        $('.datetimepicker').datetimepicker({
                            useCurrent: false,
                            icons: {
                                time: 'glyphicon glyphicon-time',
                                date: 'glyphicon glyphicon-calendar',
                                up: 'glyphicon glyphicon-chevron-up',
                                down: 'glyphicon glyphicon-chevron-down',
                                previous: 'glyphicon glyphicon-chevron-left',
                                next: 'glyphicon glyphicon-chevron-right',
                                today: 'glyphicon glyphicon-screenshot',
                                clear: 'glyphicon glyphicon-trash',
                                close: 'glyphicon glyphicon-remove'

                            },
                            minDate: curDate
                        });
                    })


                $scope.reduceBidAmountNote = 'Specify the value to be reduce from bid Amount.';
                $scope.incTaxBidAmountNote = '';
                $scope.noteForBidValue = '';

                if ($scope.auctionItem.status == "STARTED") {
                    liveId = "live";
                }
                else {
                    liveId = "";
                }

            };

            $scope.disableButtons = function () {
                $scope.buttonsDisabled = true;
            }

            $scope.enableButtons = function () {
                $scope.buttonsDisabled = false;
            }
            
            $scope.changeVendorIndex = function (vendor) {
                $scope.listReqItems = [];
                $scope.popupVendorTemp = [];
                $scope.popupVendorTemp = vendor;
                $scope.popupVendor = [];
                $scope.popupVendor = vendor;

                $scope.TotalAirlinesCost = 0;
                $scope.TotalAirlinesRevCost = 0;
                $scope.TotalItemsValidAirlines = 0;
                $scope.SelectedAirlines = 0;
                $scope.popupVendor.listReqItems.forEach(function (item1, index1) {                    
                    item1.listQuotationItems.forEach(function (QI, index2) {
                        if (QI.itemPrice > 0 && index2 == 0) {
                            $scope.TotalItemsValidAirlines++;
                        }
                        if (QI.isSelected == 1) {
                            $scope.SelectedAirlines++;
                            $scope.TotalAirlinesCost += QI.itemPrice;
                            $scope.TotalAirlinesRevCost += QI.revitemPrice;
                        }
                    })
                })
            }

            $scope.SelectAirline = function (reqItem, quotationItem, index) {
                $scope.popupVendor.listReqItems.forEach(function (item1, index1) {
                    if (reqItem.itemID == item1.itemID) {
                        item1.listQuotationItems.forEach(function (QI, index2) {
                            if (QI.quotationItemID == quotationItem.quotationItemID) {
                                QI.isSelected = 1;
                            }
                            else {
                                QI.isSelected = 0;
                            }
                        })
                    }
                })

                $scope.TotalAirlinesCost = 0;
                $scope.TotalItemsValidAirlines = 0;
                $scope.SelectedAirlines = 0;
                $scope.popupVendor.listReqItems.forEach(function (item1, index1) {
                    
                    item1.listQuotationItems.forEach(function (QI, index2) {
                        if (QI.itemPrice > 0 && index2 == 0) {
                            $scope.TotalItemsValidAirlines++;
                        }
                        if (QI.isSelected == 1) {
                            $scope.SelectedAirlines++;
                            $scope.TotalAirlinesCost += QI.itemPrice;
                        }
                    })
                })

            }

            $scope.ResetSelection = function () {
                $scope.popupVendor.listReqItems.forEach(function (item1, index1) {
                    item1.listQuotationItems.forEach(function (QI, index2) {
                        QI.isSelected = 0;
                        $scope.TotalAirlinesCost = 0;
                        $scope.TotalAirlinesRevCost = 0;
                        $scope.SelectedAirlines = 0;
                    })
                })
            }

            $scope.SaveAirlines = function () {

                var isRevised = false;

                if ($scope.auctionItem.status == 'Negotiation Ended') {
                    isRevised = true
                };

                var params = {
                    'listLogisticReqItems': $scope.popupVendor.listReqItems,
                    'sessionID': $scope.sessionid,
                    'isRevised': isRevised
                };

                logisticServices.SaveAirlines(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            swal({
                                title: "Done!",
                                text: 'Saved Successfully',
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });
                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
            }



            $scope.RejectQuotation = function () {

                var isRevised = false;

                if ($scope.auctionItem.status == 'Negotiation Ended') {
                    isRevised = true
                };

                // console.log($scope.popupVendor); ///////////

                var params = {
                    'reqID': $scope.reqId,
                    'userID': $scope.popupVendor.vendorID,
                    'sessionID': $scope.sessionid,
                    'isRevised': isRevised,
                    'quotationRejectedComment': $scope.popupVendor.quotationRejectedComment,
                    'revQuotationRejectedComment': $scope.popupVendor.revQuotationRejectedComment
                };

                logisticServices.RejectQuotation(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            swal({
                                title: "Done!",
                                text: 'Saved Successfully',
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });
                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
            }

           

            $scope.updateAuctionStart = function () {

                $scope.priceCapError = false;
                $scope.Loding = true;
                $scope.NegotiationSettingsValidationMessage = '';

                $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
                if ($scope.NegotiationSettingsValidationMessage != '') {
                    $scope.Loding = false;
                    return;
                }

              

                if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || $scope.auctionItem.auctionVendors[1].isQuotationRejected != 0) {
                    $scope.Loding = false;
                    growlService.growl("Please approve atleast 2 vendor to update start time", "inverse");
                    return;
                }

                var isError = false;
                $scope.auctionItem.auctionVendors.forEach(function (item5, index5) {

                    if (item5.isQuotationRejected == -1 && item5.initialPrice > 0) {
                        $scope.Loding = false;
                        swal("Not Allowed", "You are not allowed to schedule the Negotiation time until all the Vendor Quotations are validated. (They should be either approved/rejected). Some of the vendor quotations are yet to be validated. Kindly review them and retry.", "error");
                        isError = true;
                        return;
                    }
                })
                if (isError == true) {
                    return;
                }


                var params = {};

                var startValue = $("#startTimeDate").val(); //$scope.startTime; //Need fix on this.

                if (startValue && startValue != null && startValue != "") {

                    var ts = moment(startValue, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var auctionStartDate = new Date(m);

                    auctionsService.GetDateLogistics()
                        .then(function (response1) {
                            var CurrentDate = moment(new Date(parseInt(response1.substr(6))));
                            if (CurrentDate >= auctionStartDate) {
                                $scope.Loding = false;
                                swal("Done!", "Your Negotiation Start Time should be greater than current time.", "error");
                                return;
                            }

                            

                            var milliseconds = parseInt(auctionStartDate.getTime() / 1000.0);
                            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                            $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                                if (item.companyName == "PRICE_CAP") {
                                    if (item.totalRunningPrice > $scope.auctionItem.auctionVendors[0].totalRunningPrice) {
                                        //$scope.priceCapError = true;
                                    }
                                }

                                //if (item.isQuotationRejected == -1) {
                                //    $scope.Loding = false;
                                //    swal("Not Allowed", "You are not allowed to schedule the Negotiation time until all the Vendor Quotations are validated. (They should be either approved/rejected). The following vendors quotations are yet to be validated. (" + $scope.notviewedcompanynames + ")", "error");
                                //    return;
                                //}
                            })

                            //if ($scope.starreturn) {
                            //    $scope.Loding = false;
                            //    swal("Not Allowed", "You are not allowed to schedule the Negotiation time until all the Vendor Quotations are validated. (They should be either approved/rejected). The following vendors quotations are yet to be validated. (" + $scope.notviewedcompanynames + ")", "error");
                            //    return;
                            //}

                            //if ($scope.auctionItem.auctionVendors.length == 0 || $scope.auctionItem.auctionVendors[0].quotationUrl == "" || $scope.auctionItem.auctionVendors[1].quotationUrl == "" || $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || $scope.auctionItem.auctionVendors[1].isQuotationRejected != 0 || $scope.starttimecondition1 != 0 || $scope.starttimecondition2 != 0) {
                            //    $scope.Loding = false;
                            //    swal("Not Allowed", "You are not allowed to create a start time until at least 2 vendors Approved.", "error");
                            //    return;
                            //}
                            //if ($scope.auctionItem.auctionVendors[0].companyName == "") {
                            //    $scope.Loding = false;
                            //    swal("Not Allowed", "You are not allowed to create a start time until at least 2 vendors Approved.", "error");
                            //    return;
                            //}
                            if ($scope.priceCapError == true) {
                                $scope.Loding = false;
                                swal("Not Allowed", "Price CAP Value should be less then all the vendors price.", "error");
                                return;
                            }
                            else {
                                $scope.Loding = true;
                                
                                params.reqID = id;
                                params.userID = userService.getUserId();
                                params.date = "/Date(" + milliseconds + "000+0530)/";
                                params.sessionID = userService.getUserToken();
                                params.negotiationSettings = $scope.NegotiationSettings;

                                logisticServices.updateauctionstart(params)
                                    .then(function (response) {

                                        $scope.Loding = false;
                                        swal("Done!", "Your Negotiation Start Time Updated Successfully!", "success");
                                        $scope.Loding = false;
                                        var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                                        auctionsService.GetDateLogistics()
                                            .then(function (response) {
                                                var curDate = new Date(parseInt(response.substr(6)));

                                                var myEpoch = curDate.getTime();
                                                if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess) && start > myEpoch) {
                                                    $scope.startBtns = true;
                                                    $scope.customerBtns = false;
                                                } else {

                                                    $scope.startBtns = false;
                                                    $scope.disableButtons();
                                                    $scope.customerBtns = true;
                                                }

                                                if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 0) {
                                                    $scope.showTimer = false;
                                                } else {

                                                    $scope.showTimer = true;
                                                }
                                            })
                                }) 
                            }
                        })

                    $scope.Loding = false;

                }
                else {
                    $scope.Loding = false;
                    alert("Please enter the date and time to update Start Time to.");
                }
                $scope.Loding = false;
            }

            $scope.updateTime = function (time) {
                var isDone = false;
                $scope.disableDecreaseButtons = true;
                $scope.disableAddButton = true;
                $scope.$on('timer-tick', function (event, args) {
                    var temp = event.targetScope.countdown;

                    if (!isDone) {
                        if (time < 0 && temp + time < 0) {
                            growlService.growl("You cannot reduce the time when it is already below 60 seconds", "inverse");
                            isDone = true;
                            return false;
                        }

                        isDone = true;
                        var params = {};
                        params.reqID = id;
                        params.sessionID = userService.getUserToken();
                        params.userID = userService.getUserId();
                        params.newTicks = ($scope.countdownVal + time);



                        logisticServices.UpdateBidTime(params)
                            .then(function (response) {
                                if (response.UpdateBidTimeResult.errorMessage == '' || response.UpdateBidTimeResult.errorMessage == null) {
                                    
                                } else {
                                    swal("Error!", response.UpdateBidTimeResult.errorMessage, "error");
                                }
                            });
                        
                    }
                });
            }

            $scope.stopBids = function () {
                swal({
                    title: "Are you sure?",
                    text: "The Negotiation will be stopped after one minute.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, Stop Bids!",
                    closeOnConfirm: true
                }, function () {
                    var params = {};
                    params.reqID = id;
                    params.sessionID = userService.getUserToken();
                    params.userID = userService.getUserId();

                    logisticServices.StopBids(params)
                        .then(function (response) {
                            if (response.StopBidsResult.errorMessage == '' || response.StopBidsResult.errorMessage == null) {
                                //$scope.$broadcast('timer-set-countdown-seconds', 60);
                                //$scope.disableButtons();
                                //swal("Done!", "Negotiation time reduced to one minute.", "success");
                            } else {
                                swal("Error!", response.StopBidsResult.errorMessage, "error");
                            }
                        });                    
                });
            };

            $scope.recalculate = function () {

                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                params.methodName = 'REFRESH';

                logisticServices.signalRFrontEnd(params)
                    .then(function (response) {
                        if (response.signalRFrontEndResult.errorMessage == '' || response.signalRFrontEndResult.errorMessage == null) {
                            swal("Done!", "A refresh command has been sent to everyone.", "success");
                        } else {
                            swal("Error!", response.StopBidsResult.errorMessage, "error");
                        }
                    });
                
            }

            $scope.RestartNegotiation = function () {
                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                $scope.isnegotiationended = false;
                $scope.NegotiationEnded = false;
                $scope.invokeSignalR('RestartNegotiation', parties);



                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();

                logisticServices.RestartNegotiation(params)
                    .then(function (response) {
                        if (response.errorMessage == '' || response.errorMessage == null) {
                            $scope.isnegotiationended = false;
                            $scope.NegotiationEnded = false;
                        } else {
                            swal("Error!", response.StopBidsResult.errorMessage, "error");
                        }
                    });


            }
            
            $scope.enableOrDisable = function (enableAll) {
                $scope.popupVendor.listReqItems.forEach(function (RI, indexRI) {
                    RI.isEnabled = enableAll;
                })
            }

            $scope.savepricecap = function () {
                //$scope.l1Price = $scope.auctionItem.auctionVendors[0].totalPriceIncl;

                //$scope.l1CompnyName = $scope.auctionItem.auctionVendors[0].companyName;

                //if ($scope.l1CompnyName == 'PRICE_CAP') {
                //    $scope.l1Price = $scope.auctionItem.auctionVendors[1].totalPriceIncl;
                //}

                //if ($scope.auctionItem.reqType == 'PRICE_CAP' && $scope.auctionItem.priceCapValue >= $scope.l1Price) {
                //    swal("Error!", 'Price CAP Value should be less then all the vendors price', "error");
                //    return;
                //}

                //if ($scope.auctionItem.reqType == 'PRICE_CAP' && ($scope.auctionItem.priceCapValue <= 0 || $scope.auctionItem.priceCapValue == undefined || $scope.auctionItem.priceCapValue == '')) {
                //    swal("Error!", 'Please Enter Valid Price cap', "error");
                //    return;
                //}

                var params = {
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': userService.getUserToken(),
                    'userID': userService.getUserId(),
                    'reqType': $scope.auctionItem.reqType,
                    'priceCapValue': $scope.auctionItem.priceCapValue,
                    'isUnitPriceBidding': $scope.auctionItem.isUnitPriceBidding
                };

                logisticServices.savepricecap(params).then(function (req) {
                    if (req.errorMessage == '') {
                        swal({
                            title: "Done!",
                            text: 'Saved Successfully',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                            });
                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            }

            $scope.updatePriceCap = function (auctionVendor) {
                let priceDetails = {
                        uID: auctionVendor.vendorID,
                        reqID: $scope.reqId,
                        price: $scope.formRequest.priceCapValue,
                        sessionID: userService.getUserToken()
                    };

                logisticServices.UpdatePriceCap(priceDetails)
                        .then(function (response) {
                            if (response.errorMessage == '') {
                                

                                growlService.growl('Successfully updated price cap.', "success");
                                $scope.formRequest.priceCapValueMsg = '';
                                auctionVendor.totalPriceIncl = $scope.formRequest.priceCapValue;
                                var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();

                                $scope.invokeSignalR('updatePriceCap', parties);
                                location.reload();
                            }
                            else {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        });
                
            }


            // #endregion Functions
            
            // #region Function Calls

            $scope.getData();

            // #endregion Function Calls

            // #region SignalR Setup

            $scope.parties = $scope.reqId + "$" + userService.getUserId() + "$" + userService.getUserToken();

            var logisticsHub = SignalRFactory('', 'logisticsHub');

            $scope.checkConnection = function () {
                if (logisticsHub) {
                    return logisticsHub.getStatus();
                } else {
                    return 0;
                }
            };

            $scope.reconnectHub = function () {
                if (logisticsHub) {
                    if (logisticsHub.getStatus() == 0) {
                        logisticsHub.reconnect();
                        return true;
                    }
                } else {
                    logisticsHub = SignalRFactory('', 'logisticsHub');
                }

            }

            $scope.invokeSignalR = function (methodName, params, callback) {
                if ($scope.checkConnection() == 1) {
                    logisticsHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    })
                } else {
                    $scope.reconnectHub();
                    logisticsHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    })
                }
            }

            $scope.invokeSignalR('JoinGroup', 'logisticsHub' + $scope.reqId);

            $scope.signalRFrontEnd = function (methodName) {

                var params = {
                    'reqID': $scope.reqId,
                    'sessionID': $scope.sessionid,
                    'userID': $scope.customerID,
                    'methodName': methodName
                };

                logisticServices.signalRFrontEnd(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                        
                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
            }

            // #endregion SignalR Setup

            // #region SignalR Responce

            logisticsHub.on('checkRequirement', function (req) {

                if (req.methodName == 'UPDATE_BID_TIME') {
                    $scope.setAuctionInitializer(req, req.methodName, req.callerID);
                    growlService.growl("Negotiation time updated successfully.", "success");
                }
                else
                if (req.methodName == 'STOP_BIDS')
                {
                    $scope.setAuctionInitializer(req, req.methodName, req.callerID);
                    growlService.growl("Negotiation time updated to 1 min successfully, Negotiation will end in 1 min", "success");
                }
                else
                if (req.methodName == 'RESTART_NEGOTIATION') {
                    location.reload();
                }
                else
                if (req.methodName == 'MAKE_BID') {
                    $scope.setAuctionInitializer(req, req.methodName, req.callerID);
                    growlService.growl("A vendor has made a bid.", "success");
                }
                else {
                    $scope.setAuctionInitializer(req, req.methodName, req.callerID);
                }
                $log.info('checkRequirement - $ignalR - ' + req.methodName);
            })

            // #endregion SignalR Responce

            // #region Countdown 
            
            $scope.$on('timer-tick', function (event, args) {
                $scope.countdownVal = event.targetScope.countdown;
                if (event.targetScope.countdown < 61 && !$scope.disableStopBids && !$scope.disablereduceTime) {
                    $timeout($scope.disableButtons(), 1000);
                }
                if (event.targetScope.countdown > 60) {
                    $timeout($scope.enableButtons(), 1000);
                }
                if (event.targetScope.countdown == 60 || event.targetScope.countdown == 59 || event.targetScope.countdown == 30 || event.targetScope.countdown == 29) {
                    //var msie = $(document) [0].documentMode;
                    var ua = window.navigator.userAgent;
                    var msieIndex = ua.indexOf("MSIE ");
                    var msieIndex2 = ua.indexOf("Trident");
                    // if is IE (documentMode contains IE version)
                    var params = {};
                    params.reqID = id;
                    params.sessionID = userService.getUserToken();
                    params.userID = userService.getUserId();
                    if (msieIndex > 0 || msieIndex2 > 0) {
                        $scope.getData();
                    }
                }
                if (event.targetScope.countdown <= 0) {
                    if ($scope.auctionStarted && ($scope.auctionItem.status == "CLOSED" || $scope.auctionItem.status == "STARTED")) {
                        $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0].runningPrice;
                    }
                    else if ($scope.auctionItem.status == "NOTSTARTED") {
                        var params = {};
                        params.reqID = id;
                        params.sessionID = userService.getUserToken();
                        params.userID = userService.getUserId();
                        logisticServices.StartNegotiation(params)
                            .then(function (response) {
                                if (response.errorMessage == '') {

                                }
                            })     
                        location.reload();
                    }
                }
                if (event.targetScope.countdown <= 120) {
                    $scope.timerStyle = {
                        'color': '#f00',
                        '-webkit - animation': 'flash linear 1s infinite',
                        'animation': 'flash linear 1s infinite'
                    };
                }

                //$scope.timerFloat = $('#reqTitle').visible();
                $scope.timerFloat = $('#reqTitle').show();

                if ($scope.auctionItem.status == 'STARTED' && !$scope.timerFloat) {
                    $scope.divfix = {
                        'bottom': '2%',
                        'left': '3%',
                        'position': 'fixed',
                        'z-index': '3000'
                    };
                    $scope.divfix3 = {
                        'bottom': '9%',
                        'left': '3%',
                        'position': 'fixed',
                        'z-index': '3000'
                    };
                    $scope.divfix1 = {
                        'bottom': '8%',
                        'left': '3%',
                        'position': 'fixed',
                        'z-index': '3000'
                    };
                    $scope.divfix2 = {
                        'bottom': '13%',
                        'left': '3%',
                        'position': 'fixed',
                        'z-index': '3000'
                    };
                    $scope.divfixMakeabid = {
                        'bottom': '14%',
                        'left': '10%',
                        'position': 'fixed',
                        'z-index': '3000'
                    };
                    $scope.divfixMakeabidError = {
                        'bottom': '13%',
                        'left': '18%',
                        'position': 'fixed',
                        'z-index': '3000',
                        'background-color': 'lightgrey'
                    };

                    if (!$scope.isCustomer) {
                        $scope.boxfix = {
                            'background-color': 'lightgrey',
                            'width': '280px',
                            'height': '120px',
                            'padding': '25px',
                            'margin': '25px',
                            'bottom': '-2%',
                            'left': '0%',
                            'position': 'fixed',
                            'z-index': '3000'
                        }
                    }
                    else {
                        $scope.boxfix = {
                            'background-color': 'lightgrey',
                            'width': '280px',
                            'height': '90px',
                            'padding': '25px',
                            'margin': '25px',
                            'bottom': '-2%',
                            'left': '0%',
                            'position': 'fixed',
                            'z-index': '3000'
                        }
                    }
                }
                else {
                    $scope.divfix = {};
                    $scope.divfix3 = {};
                    $scope.divfix1 = {};
                    $scope.divfix2 = {};
                    $scope.divfixMakeabid = {};
                    $scope.divfixMakeabidError = {};
                    $scope.boxfix = {};
                };
                if ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') {
                    if ($scope.auctionItem.isUnitPriceBidding == 1 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isTabular == true) {
                        $scope.divBorder = {
                            'background-color': '#f5b2b2'
                        };
                    }
                    if ($scope.auctionItem.isUnitPriceBidding == 0 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isTabular == true) {
                        $scope.divBorder1 = {
                            'background-color': '#f5b2b2'
                        };
                    }
                }
                if (event.targetScope.countdown > 120 && $scope.auctionItem.status == 'NOTSTARTED') {
                    $scope.timerStyle = { 'color': '#000' };
                }
                if (event.targetScope.countdown > 120 && $scope.auctionItem.status != 'NOTSTARTED') {
                    $scope.timerStyle = { 'color': '#228B22' };
                }
                if (event.targetScope.countdown <= 60 && $scope.auctionItem.status == 'STARTED') {
                    $scope.disableDecreaseButtons = true;
                }
                if (event.targetScope.countdown > 60 && $scope.auctionItem.status == 'STARTED') {
                    $scope.disableDecreaseButtons = false;
                }
                if (event.targetScope.countdown <= 0 && $scope.auctionItem.status == 'NOTSTARTED') {
                    $scope.showTimer = false;
                    window.location.reload();
                }
                if (event.targetScope.countdown < 1 && $scope.auctionItem.status == 'STARTED') {
                    $scope.showTimer = false;
                    $scope.auctionItem.status = 'Negotiation Ended'; // cus
                    swal({
                        title: "Done!",
                        text: "Negotiation completed.",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            location.reload();
                        $log.info("Negotiation Ended.");
                    });
                }
                if (event.targetScope.countdown <= 3) {
                    $scope.disableAddButton = true;
                }
                if (event.targetScope.countdown < 2) {
                    $scope.disableBidButton = true;
                }
                if (event.targetScope.countdown > 3) {
                    $scope.disableAddButton = false;
                }
                if (event.targetScope.countdown >= 2) {
                    $scope.disableBidButton = false;
                }

            });

            // #endregion Countdown 

            // #region Validations

            $scope.NegotiationTimeValidation = function (days, hours, mins, minReduction, rankComparision) {

                $scope.days = days;
                $scope.hours = hours;
                $scope.mins = mins;

                $scope.NegotiationSettingsValidationMessage = '';

                if ($scope.auctionItem.isDiscountQuotation != 2) {
                    if (minReduction < 1 || minReduction == undefined) {
                        $scope.NegotiationSettingsValidationMessage = 'Set Min 1 for Min.Amount reduction';
                        return;
                    }

                    if (rankComparision < 1 || rankComparision == undefined) {
                        $scope.NegotiationSettingsValidationMessage = 'Set Min 1 for Rank Comparision price';
                        return;
                    }

                    if (minReduction >= 1 && rankComparision > minReduction) {
                        $scope.NegotiationSettingsValidationMessage = 'Please enter Valid Rank Comparision price less than Min. Amount reduction';
                        return;
                    }
                } else if ($scope.auctionItem.isDiscountQuotation == 2) {
                    $scope.NegotiationSettings.minReductionAmount = 0;
                    $scope.NegotiationSettings.rankComparision = 0;
                }

                if (days == undefined || days < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Days';
                    return;
                }
                if (hours < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (mins < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
                    return;
                }
                if (mins >= 60 || mins == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
                    return;
                }
                if (hours > 24 || hours == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (hours == 24 && mins > 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours & Minuts';
                    return;
                }
                if (mins < 5 && hours == 0 && days == 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Min 5 Minutes';
                    return;
                }
            }

            // #endregion Validations
            
            // #region Navigations

            $scope.editRequirement = function () {
                $state.go('form.addnewlogistic', { 'Id': $stateParams.Id });
            };

            // #endregion Navigations


            // Table Excel Export
            $scope.loadExcel = false;
            $scope.notLoadExcel = true;

            $scope.downloadExcel = function () {
                $scope.loadExcel = true;
                $scope.notLoadExcel = false;

                setTimeout(function () {
                    document.getElementById("downloadExcelButton").click();
                    $scope.$apply(function () {
                        $scope.loadExcel = false;
                        $scope.notLoadExcel = true;
                    });
                }, 1000)
            }
            // Table Excel Export

    }]);