prmApp
    .controller('descDispatchChallanCtrl', ["$scope", "$state", "$stateParams", "$window", "userService", "growlService", "fileReader", "$log", "poService",
        function ($scope, $state, $stateParams, $window, userService, growlService, fileReader, $log, poService) {
        $scope.reqID = $stateParams.reqID;

        $scope.descPo = {};
        $scope.descDispatch = {};
        $scope.vendors = [];

        $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

        $scope.getDesPoInfo = function () {
            poService.getdespoinfo($stateParams.reqID)
                .then(function (response) {
                    $scope.descPo = response;

                    $scope.GetDescDispatch();
                    //$scope.descPo.expectedDeliveryDate = new moment($scope.descPo.expectedDeliveryDate).format("DD-MM-YYYY");
                });
        }

        

        $scope.GetDescDispatch = function () {
            poService.GetDescDispatch($scope.descPo.poID, 0)
                .then(function (response) {
                    $scope.descDispatch = response;                    
                });
        }

        $scope.getDesPoInfo();
        









        $scope.getFile1 = function (id, doctype, ext) {
            $scope.progress = 0;
            $scope.file = $("#" + id)[0].files[0];
            $scope.docType = doctype + "." + ext;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    if (id == "poFile") {
                        $scope.descDispatch.poFile = { "fileName": '', 'fileStream': null };
                        var bytearray = new Uint8Array(result);
                        $scope.descDispatch.poFile.fileStream = $.makeArray(bytearray);
                        $scope.descDispatch.poFile.fileName = $scope.file.name;
                    }

                });
        };

        $scope.SaveDesDispatchTrack = function () {

            $scope.descPo.sessionID = userService.getUserToken();
            $scope.descPo.createdBy = userService.getUserId();
            $scope.descPo.status = 'PO_GENERATED';

            $scope.descDispatch.sessionID = userService.getUserToken();

            var ts = moment($scope.descPo.expectedDelivery, "DD-MM-yyyy HH:mm").valueOf();
            var m = moment(ts);
            var deliveryDate = new Date(m);
            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
            $scope.descPo.expectedDelivery = "/Date(" + milliseconds + "000+0530)/";

            var params = {
                'dispatchtrack': $scope.descDispatch,
                'povendor': $scope.descPo
            }
            poService.SaveDesDispatchTrack(params)
                .then(function (response) {
                    if (response.errorMessage == '') {

                        $scope.UpdatePOStatus('DISPATCH_CALLAN_GENERATED');

                        swal({
                            title: "Done!",
                            text: "Dispatch Challan Generated Successfully.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                            });
                    }
                });
        }
        



        $scope.UpdatePOStatus = function (status) {

            var params = {
                'poID': $scope.descPo.poID,
                'status': status,
                'sessionID': userService.getUserToken()
            }

            poService.UpdatePOStatus(params)
            .then(function (response) {
                if (response.errorMessage != '') {
                    swal({
                        title: "Done!",
                        text: "Purchase Orders Sent Successfully.",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            //$state.go('view-requirement');
                            $state.go('view-requirement', { 'Id': response.requirementID });
                        });
                }
            });


        }

        $scope.goToDescDispatchChallan = function () {
            var url = $state.href("desc-dispatch-challan", { "reqID": $scope.descPo.reqID, "poID": $scope.descPo.poID });
            window.open(url, '_blank');
        }

       

    }]);