﻿prmApp
    .controller('storegrnCtrl', ["$scope", "$state", "$stateParams", "$window", "$log", "userService", "storeService", "fileReader", "growlService",
        function ($scope, $state, $stateParams, $window, $log, userService, storeService, fileReader, growlService) {
            $scope.storeID = $stateParams.storeID;
            $scope.selectedStoreItems = [];
            $scope.sessionID = userService.getUserToken();
            $scope.companyStoreItems = [];
            $scope.companyStoreItemsTemp = [];
            $scope.storeItemSerialNos = [];
            $scope.storeItemSerialNosSelected = {};
            $scope.currentStoreItemID = 0;
            
            $scope.generateGrnError = false;
            $scope.receiveDateError = false;
            $scope.receiveDateVal = '';

            var GenRandom = {

                Stored: [],

                Job: function () {
                    var newId = Date.now().toString().substr(6); // or use any method that you want to achieve this string

                    if (!this.Check(newId)) {
                        this.Stored.push(newId);
                        return newId;
                    }

                    return this.Job();
                },

                Check: function (id) {
                    for (var i = 0; i < this.Stored.length; i++) {
                        if (this.Stored[i] == id) return true;
                    }
                    return false;
                }

            };

            $scope.grnObject = {
                grnID: 0,
                grnCode: "GRN-" + GenRandom.Job(),
                orderQty: 0,
                receivedQty: 0,
                ginCodeLink: '',
                sessionID: $scope.sessionID,
                modifiedBy: userService.getUserId()
            };

            $scope.grnObject.ginCodeLink = $stateParams.linkgin;

            $scope.searchKey = '';

            if ($stateParams.itemsArr) {
                $scope.companyStoreItems = $stateParams.itemsArr;
                if ($scope.companyStoreItems.length > 0) {
                    $scope.grnObject.storeItems = $scope.companyStoreItems;
                }
            }

            $scope.cancelClick = function () {
                $window.history.back();
            }

            $scope.JsonDate = function (dateVal) {
                //return dateVal;
                var ts = moment(dateVal, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var tempDate = new Date(m);
                var milliseconds = parseInt(tempDate.getTime() / 1000.0);
                return "/Date(" + milliseconds + "000+0530)/";
            }

            $scope.generateGrn = function () {
                $scope.generateGrnValidations();
                if (!$scope.generateGrnError) {

                    if ($scope.companyStoreItems.length > 0) {
                        var savedItems = 0;
                        $.each($scope.grnObject.storeItems, function (key, item) {
                            item.inStock = item.receivedQty;
                            item.totalStock = item.orderQty;
                        });

                        var ts = moment($scope.receiveDateVal, "DD-MM-YYYY HH:mm").valueOf();
                        var m = moment(ts);
                        var receiveDate = new Date(m);
                        var milliseconds = parseInt(receiveDate.getTime() / 1000.0);
                        $scope.grnObject.receiveDate = "/Date(" + milliseconds + "000+0530)/";
                        $.each($scope.grnObject.storeItems, function (key, item) {
                            $.each(item.itemDetails, function (key, details) {
                                details.warrantyDate = $scope.JsonDate(details.warrantyDate);
                            });
                        });

                        $scope.grnObject.ginCodeLink = $stateParams.linkgin;
                        var params = {
                            "item": $scope.grnObject
                        };

                        storeService.saveStoreItemGRN(params)
                            .then(function (response) {
                                if (response.errorMessage != '') {
                                    growlService.growl(response.errorMessage, "inverse");
                                }
                                else {
                                    growlService.growl("GIN details saved successfully.", "success");
                                    if ($stateParams.linkgin != '') {
                                        $state.go("storeitems", { "storeID": $stateParams.storeID, "storeObj": null, "itemID": 0 });
                                    } else {
                                        $window.history.back();
                                    }
                                }
                            });
                    }
                }
            }

            $scope.itemDetailsSelect = function (itemDetails) {
                $scope.storeItemSerialNosSelected[itemDetails.itemID] = _.filter($scope.storeItemSerialNos[itemDetails.itemID], function (item) {
                    return item.isSelected == true;
                });

                $.each($scope.grnObject.storeItems, function (key, item) {
                    if (item.itemID == itemDetails.itemID) {
                        item.itemDetails = $scope.storeItemSerialNosSelected[item.itemID];
                    }
                });
            };

            $scope.handleDate = function (date) {
                return new moment(date).format("DD-MMM-YYYY");
            }

            $scope.selectSerialNo = function (storeItem) {
                $scope.currentStoreItemID = storeItem.itemID;
                if (!$scope.storeItemSerialNos[storeItem.itemID] || $scope.storeItemSerialNos[storeItem.itemID].length <= 0) {
                    $scope.storeItemSerialNos[storeItem.itemID] = [];

                    for (i = 0; i <= storeItem.receivedQty - 1; i++) {
                        var storeDetailsObj = {
                            warrantyDate: "01-01-2020",
                            warrantyNo: "",
                            serialNo: ""
                        };

                        $scope.storeItemSerialNos[storeItem.itemID].push(storeDetailsObj);
                    }

                    $.each($scope.grnObject.storeItems, function (key, item) {
                        if (item.itemID == storeItem.itemID) {
                            item.itemDetails = $scope.storeItemSerialNos[item.itemID];
                        }
                    });
                }
            }

            $scope.search = function () {
                $scope.companyStoreItemsTemp = _.filter($scope.companyStoreItems, function (item) {
                    return item.itemCode.indexOf($scope.searchKey) > -1;
                });
            }

            $scope.generateGrnValidations = function () {

                $scope.generateGrnError = false;
                $scope.receiveDateError = false;


                if ($scope.receiveDateVal == undefined || $scope.receiveDateVal == '') {
                    $scope.receiveDateError = true;
                    $scope.generateGrnError = true;
                }

            };

        }]);