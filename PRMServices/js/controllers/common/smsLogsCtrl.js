﻿
prmApp
    .controller('smsLogsCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "$stateParams", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, $stateParams, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {
          
            $scope.userId = userService.getUserId();
            $scope.userObj = {
                //logoURL: '../js/resources/images/chart.png'
            };

            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            this.userdetails = {};
            $scope.sessionId = userService.getUserToken();
            $scope.companyID = userService.getUserCompanyId();

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.totalVendors = 0;
            $scope.totalSubuser = 0;
            $scope.totalInactiveVendors = 0;
            $scope.totalLeads = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 8;
            $scope.itemsPerPage2 = 8;
            $scope.maxSize = 8;
           // $scope.status = 'ALL';
            $scope.reqStatus = 'ALL';
            $scope.reqType = 'ALL';
            $scope.requirement = 'ALL';   
            $scope.type = '';
            $scope.temprequirements = [];
            $scope.Search = [];

            $scope.activeUsers = [];
            $scope.activeBuyers = [];
            $scope.activeUsersOnline = [];
            $scope.GetUsersLoginStatus = [];

            //var allObj = {
            //    userDeptID: 0,
            //    userID: 0,
            //    userName: "ALL"
            //}
            //$scope.status = 0;
            //$scope.requirementStatus.push(allObj);

            $scope.reportToDate = moment().format('YYYY-MM-DD');
            $scope.reportFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                }
            };

            $scope.pageChanged = function () {
                //$scope.getAuctions();
            };
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };


            $scope.SMSLogs = [];
            $scope.SMSLogsTemp = [];

            $scope.GetSMSLogs = function () {
                
                
                auctionsService.GetSMSLogs($scope.reportFromDate, $scope.reportToDate,$scope.companyID, $scope.sessionId)
                    .then(function (response) {
                       
                        $scope.SMSLogs = response;

                        $scope.SMSLogs.forEach(function (item, itemIndex) {

                            item.messageDate = userService.toLocalDate(item.messageDate);
                            if (item.requirement == 'VENDOR_REGISTER' || item.requirement == 'COMMUNICATIONS' || item.requirement == 'USER_REGISTER') {

                                item.reqStatus = '--';
                                item.reqType = -1;

                            }
                        })

                        $scope.SMSLogs1 = $scope.SMSLogs;
                        $scope.SMSLogsTemp = $scope.SMSLogs;
                        $scope.tempStatusFilter = angular.copy($scope.SMSLogs);
                        $scope.tempReqTypeFilter = angular.copy($scope.SMSLogs);
                        $scope.temprequirements = angular.copy($scope.SMSLogs);
                        $scope.totalItems = $scope.SMSLogs.length;
                    })

            }

            $scope.GetSMSLogs();


            
            $scope.getStatusFilter = function (reqStatus, requirementMsgtype) {
                $scope.filterArray = [];
                if (reqStatus == 'ALL' && requirementMsgtype == 'ALL') {
                    $scope.SMSLogs = $scope.SMSLogs1;
                    $scope.SMSLogsTemp = $scope.SMSLogs1;

                } else if (reqStatus == 'ALL' && requirementMsgtype != 'ALL') {
                    $scope.filterArray = $scope.tempStatusFilter.filter(function (item) {
                        if (reqStatus == 'ALL') {
                            return (item.requirement).toLowerCase() == (requirementMsgtype).toLowerCase();
                        }
                    });
                    $scope.SMSLogs = $scope.filterArray;
                    $scope.SMSLogsTemp = $scope.filterArray;

                } else if (reqStatus != 'ALL' && requirementMsgtype == 'ALL') {
                    $scope.filterArray = $scope.tempStatusFilter.filter(function (item) {
                        if (requirementMsgtype == 'ALL') {
                            return (item.reqStatus).toLowerCase() == (reqStatus).toLowerCase();
                        }
                    });
                    $scope.SMSLogs = $scope.filterArray;
                    $scope.SMSLogsTemp = $scope.filterArray;

                } else if (reqStatus != 'ALL' && requirementMsgtype != 'ALL') {
                    $scope.filterArray = $scope.tempStatusFilter.filter(function (item) {
                        if ((item.reqStatus).toLowerCase() == (reqStatus).toLowerCase() && (item.requirement).toLowerCase() == (requirementMsgtype).toLowerCase()) {
                            return item;
                        }
                    });
                    $scope.SMSLogs = $scope.filterArray;
                    $scope.SMSLogsTemp = $scope.filterArray;
                }

                $scope.totalItems = $scope.SMSLogs.length;
                $scope.tempreq = $scope.SMSLogs;
                $scope.temprequirements = $scope.tempreq;

            }

            $scope.getReqTypeFilter = function (filterVal) {
                $scope.filterArray = [];
                if (filterVal == 'ALL') {
                    $scope.SMSLogs = $scope.SMSLogs1;

                } else {
                    $scope.filterArray = $scope.tempReqTypeFilter.filter(function (item) {
                        return item.reqType == filterVal;
                    });
                    $scope.SMSLogs = $scope.filterArray;
                }

                $scope.totalItems = $scope.SMSLogs.length;

            }

            $scope.getRequirementFilter = function (filterVal) {
                $scope.filterArray = [];
                if (filterVal == 'ALL') {
                    $scope.SMSLogs = $scope.temprequirements;
                    $scope.SMSLogsTemp = $scope.temprequirements;

                } else {
                    $scope.filterArray = $scope.temprequirements.filter(function (item) {
                        return item.requirement == filterVal;
                    });
                    $scope.SMSLogs = $scope.filterArray;
                    $scope.SMSLogsTemp = $scope.filterArray;
                    $scope.Search = $scope.filterArray;
                }

                $scope.totalItems = $scope.SMSLogs.length;

            }

            $scope.searchTable = function (str) {

                str = String(str).toUpperCase();

                $scope.SMSLogs = $scope.SMSLogsTemp.filter(function (req) {
                    req.FullName = req.firstName.concat(' ' + req.lastName);

                    return (String(req.reqID).includes(str) == true || String(req.email).toUpperCase().includes(str) == true || String(req.phoneNum).toUpperCase().includes(str) == true || String(req.institution).toUpperCase().includes(str) == true ||
                        String(req.FullName).toUpperCase().includes(str) == true || String(req.requirement).toUpperCase().includes(str) == true || String(req.altEmail).toUpperCase().includes(str) == true || String(req.altPhoneNum).toUpperCase().includes(str) == true);
                });

                $scope.totalItems = $scope.SMSLogs.length;
            }
        

            $scope.getReport = function (name) {
                reportingService.GetLogsTemplates(name, $scope.reportFromDate, $scope.reportToDate, $scope.companyID, $scope.sessionId);
            }



        }]);