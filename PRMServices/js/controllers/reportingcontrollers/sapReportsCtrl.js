﻿prmApp

    .controller('sapReportsCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$log", "$stateParams", "$timeout", "auctionsService",
        "userService", "SignalRFactory", "fileReader", "growlService", "reportingService","$uibModal",
        function ($scope, $http, $state, domain, $filter, $log, $stateParams, $timeout, auctionsService,
            userService, SignalRFactory, fileReader, growlService, reportingService, $uibModal) {

            $scope.minDateMoment = moment();

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 5;
            $scope.maxSize = 5; //Number of pager buttons to show

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/

            $scope.openPRArr = [];
            $scope.openPRArrInitial = [];
            $scope.openPRPivot = [];
            $scope.openPRPivotData = [];

            $scope.filterModelPlant = '0';
            $scope.filterModelPurchaseUser = '0';
            $scope.filterModelExclusion = '0';
            $scope.filterDateRange = 0;
            $scope.accessTypes = [];
            $scope.permission = 'VIEW';
            $scope.poitemcomments = [];
            $scope.lastUpdatedDate = "";
            
            $scope.getLastUpdatedDate = function () {
                reportingService.GetLastUpdatedDate('sap_openpr')
                    .then(function (response) {
                        $log.info(response);
                        $scope.lastUpdatedDate = response.message;                        
                    });
            }

            $scope.getUserAccess = function () {
                reportingService.GetSapAccess(userService.getUserId())
                    .then(function (response) {
                        $log.info(response);
                        $scope.accessTypes = response;
                        if ($scope.accessTypes && $scope.accessTypes.length > 0) {
                            $scope.filterModelPurchaseUser = $scope.accessTypes[0].objectID;
                            $scope.permission = $scope.accessTypes[0].errorMessage;
                            $scope.getOpenPR(0, $scope.filterModelPurchaseUser, $scope.filterModelExclusion);
                            $scope.getOpenPRPivot(0, $scope.filterModelPurchaseUser, $scope.filterModelExclusion);
                        }
                    });
            }

            $scope.getUserAccess();
            $scope.getLastUpdatedDate();

            $scope.getOpenPR = function (plant, purchase, exclusion) {
                $scope.serchString = '';
                reportingService.GetOpenPR(0, plant, purchase, exclusion)//userService.getUserCompanyId()
                    .then(function (response) {
                        $log.info(response);

                        if ((plant == 0 || plant == '' || plant == null || plant == undefined) && (purchase == 0 || purchase == '' || purchase == null || purchase == undefined)) {
                            $scope.openPRArrInitialPageLoadData = response;
                        }

                        $scope.openPRArrInitial = response;
                        $scope.openPRArr = response;
                        $log.info('---------------------------------------------------->$scope.openPRArr');
                        $log.info($scope.openPRArr);
                        $log.info('---------------------------------------------------->$scope.openPRArr');
                        $scope.openPRArrInitial.forEach(function (item) {
                            item.REQUISITION_DATE1 = moment(item.REQUISITION_DATE).format('DD-MM-YYYY');
                            //item.LEAD_TIME1 = moment(item.LEAD_TIME).format('DD-MM-YYYY');
                            item.DELIV_DATE_FROM_TO1 = moment(item.DELIV_DATE_FROM_TO).format('DD-MM-YYYY');
                            item.DELIVERY_DATE1 = moment(item.DELIVERY_DATE).format('DD-MM-YYYY');   
                        });
                        $scope.openPRArr.forEach(function (item) {
                            if (item.NWSTATUS == 'REVIEW PENDING') {
                                item.color = 'Red';
                            } else if (item.NWSTATUS == 'REVIEWED BUT ACTION PENDING') {
                                item.color = 'Yellow';
                            } else if (item.NWSTATUS == 'REVIEWED AND ACTION TAKEN') {
                                item.color = 'GreenYellow';
                            }
                            if (item.NEW_DELIVERY_DATE_COMM == null) {
                                item.NEW_DELIVERY_DATE_COMM = '-';
                            } else {
                            item.NEW_DELIVERY_DATE_COMM = moment(item.NEW_DELIVERY_DATE_COMM).format('DD-MM-YYYY');
                            }
                        });



                        /*PAGINATION CODE START*/
                        $scope.totalItems = $scope.openPRArr.length;
                        /*PAGINATION CODE END*/
                    });
            }


            $scope.save = false;
            $scope.showSave = function (status) {//poitem, 
                //$scope.selectedPOItem = poitem;
                $scope.save = true;
            }

            $scope.RequisitionDateArray = [
                {
                    id: 0,
                    code: 'ALL',
                    from: 0,
                    to: 100000
                },
                {
                    id: 1,
                    code: '0 to 7 Days',
                    from: 0,
                    to: 7
                },
                {
                    id: 2,
                    code: '7 to 14 Days',
                    from: 7,
                    to: 14
                },
                {
                    id: 3,
                    code: '14 to 21 Days',
                    from: 14,
                    to: 21
                },
                {
                    id: 4,
                    code: '21 to 35 Days',
                    from: 21,
                    to: 35
                },
                {
                    id: 5,
                    code: '35 to 60 Days',
                    from: 35,
                    to: 60
                },
                {
                    id: 6,
                    code: '60 to 100 Days',
                    from: 60,
                    to: 100
                },
                {
                    id: 7,
                    code: '> 100',
                    from: 100,
                    to: 100000
                }

            ]
            $scope.filterString = '';
            $scope.filter = function(name)
            {
                $scope.serchString = '';
                $scope.openPRArr = [];
                $scope.openPRArrTEMP = [];

                
                //if ($scope.filterDateRange > 0) {
                //    var filterParams = $scope.RequisitionDateArray.filter(function (item) {
                //        return item.id == $scope.filterDateRange    ;
                //    });
                    
                //    var todayTemp = moment();
                //    var today = moment();
                //    var fromDate = new moment().add('days', -(filterParams[0].from));
                //    var toDate = new moment().add('days', -(filterParams[0].to));
                //    $scope.filterFromDate = fromDate.format('DD-MM-YYYY');
                //    $scope.filterToDate = toDate.format('DD-MM-YYYY');
                    
                //    $scope.openPRArr = $scope.openPRArrInitial.filter(function (item) {
                //        if (item.REQUISITION_DATE)
                //        {
                //            var date = new moment(item.REQUISITION_DATE);

                //            //(Convert.ToDateTime(d.REQUISITION_DATE) >= today.AddDays(-6) && Convert.ToDateTime(d.REQUISITION_DATE) <= today)

                //            return date >= toDate && date <= fromDate;
                //        }
                //    });
                //}
                //else
                //{

                //    $scope.openPRArr = $scope.openPRArrInitial;
                //}
                $scope.filterString = name;
                 $scope.openPRArrTEMP = $scope.openPRArrInitial.filter(function (item) {
                         return item.FILTER_STRING == name;
                 });


                if (!$scope.$$phase) {
                    $scope.$apply(function () {
                        $scope.openPRArr = $scope.openPRArrTEMP;
                        $scope.totalItems = $scope.openPRArr.length;
                    })
                } else {
                    $scope.openPRArr = $scope.openPRArrTEMP;
                    $scope.totalItems = $scope.openPRArr.length;
                }
               
                var options = {
                     behavior: 'smooth'
                };
                $scope.scrollDown = function (options) {
                   window.scrollBy(0 , 400);
                   //window.scrollBy({
                   //    behavior: 'smooth'
                       
                     
                   //});
                }

                $scope.scrollDown();
               
                   
            }

            $scope.filterGraph = function () {
                $scope.serchString = '';
                $scope.openPRPivot = [];
                $scope.openPRArr = [];
                $scope.openPRPivotData = [];
                $scope.openPRArrInitial = [];
                $scope.getOpenPRPivot($scope.filterModelPlant, $scope.filterModelPurchaseUser,$scope.filterModelExclusion);
                $scope.getOpenPR($scope.filterModelPlant, $scope.filterModelPurchaseUser, $scope.filterModelExclusion);
            }

            $scope.getOpenPRPivot = function (plant, purchase, exclusion) {
                reportingService.GetOpenPRPivot(0, plant, purchase, exclusion)//userService.getUserCompanyId()
                    .then(function (response) {
                        $log.info(response);
                        $scope.openPRPivot = response;
                        if ($scope.openPRPivot && $scope.openPRPivot.length > 0) {


                            $scope.openPRPivot.forEach(function (obj, key) {
                                var temp = {
                                    "name": obj.key1,
                                    "y": obj.value1
                                };

                                $scope.openPRPivotData.push(temp);
                            });

                            loadHighCharts();
                        }
                    });
            }

            //$scope.getOpenPR(0, 0);
            //$scope.getOpenPRPivot(0,0);

            function loadHighCharts() {
                Highcharts.chart('container', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Open PR'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        title: {
                            text: 'Total No.of PR\'s'
                        }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                            },
                            point: {
                                events: {
                                    click: function () {
                                        //$scope.filterDateRange = $scope.RequisitionDateArray[this.x + 1].id;
                                        //$log.info($scope.RequisitionDateArray[this.x]);
                                        $scope.filter(this.name);

                                      
                                        
                                        //if (!$scope.$$phase) {
                                        //    $scope.$apply(function () {
                                        //        $scope.filter(this.name);
                                        //    })
                                        //} else {
                                        //    $scope.filter(this.name);
                                        //}
                                    }
                                }
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
                    },

                    "series": [
                        {
                            "name": "PR COUNT",
                            "colorByPoint": true,
                            "data": $scope.openPRPivotData
                        }
                    ],
                    "drilldown": {
                        
                    }
                });
            }
            
            $scope.ImportEntity = {};

            $scope.getFile1 = function (id, doctype, ext) {
                $scope.progress = 0;
                $scope.file = $("#" + id)[0].files[0];
                $scope.docType = doctype + "." + ext;
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        

                        if (id == "attachment") {
                            if (ext.toLowerCase() != "xlsx") {
                                swal("Error!", "File type should be XSLX.", "error");
                                return;
                            }
                            var bytearray = new Uint8Array(result);
                            $scope.ImportEntity.attachment = $.makeArray(bytearray);
                            $scope.ImportEntity.attachmentFileName = $scope.file.name;
                            $scope.importEntity();
                        }

                        

                    });
            };

            $scope.importEntity = function () {
                var params = {
                    "entity": {
                        entityID: userService.getUserCompanyId(),
                        attachment: $scope.ImportEntity.attachment,
                        userid: parseInt(userService.getUserId()),
                        entityName: 'Open PR',
                        sessionid: userService.getUserToken()
                    }
                };

                auctionsService.importEntity(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            swal({
                                title: "Thanks!",
                                text: "Saved Successfully",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });
                        } else {
                            swal({
                                title: "Failed!",
                                text: response.errorMessage,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });
                        }
                    })
            };


            $scope.showComments = function (poitem, type) {
                $scope.selectedPOItem = poitem;
                $scope.poitemcomments = [];
                reportingService.GetPoComments(poitem.PURCHASING_DOCUMENT, poitem.ITEM, type)
                    .then(function (response) {
                        $log.info(response);
                        $scope.poitemcomments = response;

                        $scope.poitemcomments.forEach(function (item, index) {
                            item.newdeliverdate = new moment(item.newdeliverdate).format("DD-MM-YYYY");
                            if (item.newdeliverdate.indexOf('-9999') > -1) {
                                item.newdeliverdate = "";
                            }
                        })




                    });
            };
            $scope.newdeliverdateValidation = false;
            $scope.savePoComments = function (type,category) {
                var params = {
                    pono: $scope.selectedPOItem.PURCHASE_REQUISITION,
                    itemno: $scope.selectedPOItem.ITEM_OF_REQUISITION,
                    quantity: $scope.poquantity,
                    comments: $scope.pocomments,
                    newdeliverdate: $scope.newdeliverdate,
                    materialdescription: $scope.selectedPOItem.SHORT_TEXT,
                    apiname: $scope.selectedPOItem.API_NAME,
                    type: type,
                    userid: parseInt(userService.getUserId()),
                    sessionid: userService.getUserToken(),
                    openpr: $scope.selectedPOItem,
                    category:category,
                    status: $scope.selectedPOItem.NWSTATUS

                    
                }
                if (($scope.category == '' || $scope.category == null || $scope.category == undefined)) {
                    $scope.categoryValidation = true;
                    return false;
                }

                if (($scope.category == 'SHORTAGE')) {
                    if ($scope.newdeliverdate == '' || $scope.newdeliverdate == null || $scope.newdeliverdate <= CurrentDate) {
                        $scope.newdeliverdateValidation = true;
                        //growlService.growl("Please enter valid New Delivery Date.", "inverse");
                        return false;
                    } 
                } else {
                    $scope.newdeliverdateValidation = false;
                }

                var ts = moment(params.newdeliverdate, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                params.newdeliverdate = "/Date(" + milliseconds + "000+0530)/";
                var CurrentDate = moment(new Date());
                //$scope.newdeliverdate


                reportingService.SavePoComments(params)
                    .then(function (response) {
                      // $log.info("resp>>>>>>"+response);

                       
                       
                        $scope.newdeliverdate = '';
                        $scope.poquantity = 0;
                        $scope.pocomments = '';
                        //$scope.commentType = '';
                        $scope.category = '';
                        growlService.growl("Saved successfully.", "success");
                        $scope.newdeliverdateValidation = false;
                        $scope.showComments($scope.selectedPOItem, type);
                        // in show comments
                        $scope.openPRArr.forEach(function (item, index) {
                            if (item.PURCHASE_REQUISITION == $scope.selectedPOItem.PURCHASE_REQUISITION && item.ITEM_OF_REQUISITION == $scope.selectedPOItem.ITEM_OF_REQUISITION) {
                                item.CATEGORY = category;
                                item.NWSTATUS = 'REVIEW PENDING';
                                item.color = 'Red';
                            }
                        })

                     });
            };


            $scope.updateStatus = function (poitem, status, index) {
                $scope.selectedPOItem = poitem;

                if ($scope.selectedPOItem.NWSTATUS == '' || $scope.selectedPOItem.NWSTATUS == null || $scope.selectedPOItem.NWSTATUS == undefined) {
                    return;
                }

                var params = {
                    pono: $scope.selectedPOItem.PURCHASE_REQUISITION,
                    itemno: $scope.selectedPOItem.ITEM_OF_REQUISITION,
                    userid: parseInt(userService.getUserId()),
                    sessionid: userService.getUserToken(),
                    status: $scope.selectedPOItem.NWSTATUS

                }
                //if (($scope.category == '' || $scope.category == null || $scope.category == undefined)) {
                //    $scope.categoryValidation = true;
                //    return false;
                //}

                //if (($scope.newdeliverdate == '' || $scope.newdeliverdate == null || $scope.newdeliverdate <= CurrentDate)) {
                //    $scope.newdeliverdateValidation = true;
                //    return false;
                //}

                var ts = moment(params.newdeliverdate, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                params.newdeliverdate = "/Date(" + milliseconds + "000+0530)/";
                var CurrentDate = moment(new Date());
                //$scope.newdeliverdate


                reportingService.UpdatePoStatus(params)
                    .then(function (response) {
                        //     $log.info("resp>>>>>>"+response);
                  //      $scope.status = '';
                        growlService.growl("Status Saved successfully.", "success");
                       // $scope.showComments($scope.selectedPOItem, type);


                        $scope.openPRArr.forEach(function (item, index) {
                            if (item.PURCHASE_REQUISITION == response.prNo && item.ITEM_OF_REQUISITION == response.prItemNo) {
                                item.NWSTATUS = response.errorMessage;
                                if (item.NWSTATUS == 'REVIEW PENDING') {
                                    item.color = 'Red';
                                } else if (item.NWSTATUS == 'REVIEWED BUT ACTION PENDING') {
                                    item.color = 'Yellow';
                                } else if (item.NWSTATUS == 'REVIEWED AND ACTION TAKEN') {
                                    item.color = 'GreenYellow';
                                }
                            }
                        });

                    });
            };

            $scope.showComments = function (poitem, type) {
                $scope.selectedPOItem = poitem;
                $scope.pritemcomments = [];
                reportingService.GetPoComments(poitem.PURCHASE_REQUISITION, poitem.ITEM_OF_REQUISITION, type)
                    .then(function (response) {
                        $log.info(response);
                        $scope.pritemcomments = response;
                        $scope.itemCategories = [];
                        //$scope.openPRArr[index].category = [];
                        $scope.pritemcomments.forEach(function (item, index) {
                            item.newdeliverdate = new moment(item.currentTime).format("DD-MM-YYYY");
                            if (item.newdeliverdate.indexOf('-9999') > -1) {
                                item.newdeliverdate = "";
                            }
                            //if (item.remarksType == 'PR_SHORTAGE') {
                            //    item.remarksType = "Shortage Comments";
                            //} else if (item.remarksType == 'PR_PURCHASE') {
                            //    item.remarksType = "Purchase Comments";
                            //}
                        })
                        
                       
                    });
            };

            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'OpenPR',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };
                $log.info('$scope.openPRArr');
                $log.info($scope.openPRArr);
                $log.info('$scope.openPRArr');
                alasql.fn.decimalRound = function (x) { return Math.round(x); };   
                alasql(' SELECT PLANT_NAME as [Plant Name], PURCHASER as [Purchaser], PURCHASE_REQUISITION as [PR No.], ' +
                    ' ITEM_OF_REQUISITION as [PR Item No], REQUISITION_DATE1 as [PR Creat. Date], MATERIAL as [Material], ' +
                    ' SHORT_TEXT as [Material Desc.], API_NAME as [API Name], UNIT_OF_MEASURE as [UOM], decimalRound(QUANTITY_REQUESTED) as [PR Qty.], ' +
                    ' decimalRound(QUANTITY_ORDERED) as [Orderd Qty.], LEAD_TIME as [Lead Time], DELIV_DATE_FROM_TO1 as [Delivery Date], ' +
                    ' DELIVERY_DATE1 as [Ideal Delivery Date], CATEGORY as [Category], NWSTATUS as [Status], NEW_DELIVERY_DATE_COMM as [New Delivery Date], ' +
                    ' COMMENTS as [Comments] INTO XLSX(?, { '+
                    ' headers: true, sheetid: "RequirementDetails", ' +
                    ' style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ? ', ["OpenPR.xlsx", $scope.openPRArr]);
            }

            $scope.retrieveShortageReport = function () {
                var mystyle = {
                    sheetid: 'OpenPR',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                reportingService.GetOpenPRReport(userService.getUserCompanyId())
                    .then(function (response) {
                        $log.info(response);
                        response.forEach(function (item) {
                            item.DELIVERY_DATE1 = moment(item.DELIVERY_DATE).format('DD-MM-YYYY');
                            item.PR_DELV_DATE1 = moment(item.PR_DELV_DATE).format('DD-MM-YYYY');
                            item.NEW_DELIVERY_DATE1 = moment(item.NEW_DELIVERY_DATE).format('DD-MM-YYYY');
                            item.REQUISITION_DATE1 = moment(item.REQUISITION_DATE).format('DD-MM-YYYY');
                            item.COMMENTS = item.COMMENTS == 'null' ? '' : item.COMMENTS;
                        });
                        alasql.fn.decimalRound = function (x) { return Math.round(x); };   
                        alasql('SELECT PLANT_NAME as [Plant], MATERIAL as [Material], SHORT_TEXT as [Material Desc.], API_NAME as [API Name], ' +
                    '  PR_NUMBER as [PR Number], UNIT_OF_MEASURE as [UOM], decimalRound(QUANTITY_REQUESTED) as [PR Qty], PURCHASE_REQUISITION as [PO Number], ' +
                    '  decimalRound(QUANTITY_ORDERED) as [Order Quantity], DELIVERY_DATE1 as [PR Delivery Date], LEAD_TIME as [Lead Time], ' +
                    '  DELIVERY_DATE1 as [Ideal Delivery Date], COMMENTS as [Shortage Comments], ' +
                    '  PURCHASER as [Purchaser], NEW_DELIVERY_DATE1 as [New Delivery Date] INTO ' +
                    '  XLSX(?,{headers:true,sheetid: "OpenPO", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["OpenPOShortage.xlsx", response]);
                    });
            }

            $scope.sapReport = function () {
                var mystyle = {
                    sheetid: 'OpenPR',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                reportingService.GetOpenPRReport(userService.getUserCompanyId())
                    .then(function (GetOpenPRReportResponse) {
                        $log.info(GetOpenPRReportResponse);


                        reportingService.GetOpenPOReport(userService.getUserCompanyId())
                        .then(function (response) {
                            $log.info(response);

                            var excelArray = [];

                            GetOpenPRReportResponse.forEach(function (item) {
                                //PR
                                item.DELIVERY_DATE1 = moment(item.DELIVERY_DATE).format('DD-MM-YYYY');
                                item.PR_DELV_DATE1 = moment(item.PR_DELV_DATE).format('DD-MM-YYYY');
                                item.NEW_DELIVERY_DATE1 = moment(item.NEW_DELIVERY_DATE).format('DD-MM-YYYY');
                                item.REQUISITION_DATE1 = moment(item.REQUISITION_DATE).format('DD-MM-YYYY');
                                item.COMMENTS = item.COMMENTS == 'null' ? '' : item.COMMENTS;


                                item.IDEAL_DELIVERY_DATE1 = item.DELIVERY_DATE1;
                                item.STILL_TO_BE_DELIVERED_QTY = '';
                                item.CATEGORY = 'PR';

                                item.QUANTITY_REQUESTED = Math.round(item.QUANTITY_REQUESTED);

                                excelArray.push(item);

                            });

                            response.forEach(function (item) {
                                item.PO_DATE1 = moment(item.PO_DATE).format('DD-MM-YYYY');
                                item.PR_CREATE_DATE1 = moment(item.PR_CREATE_DATE).format('DD-MM-YYYY');
                                item.PO_DELV_DATE1 = moment(item.PO_DELV_DATE).format('DD-MM-YYYY');
                                item.IDEAL_DELIVERY_DATE1 = moment(item.IDEAL_DELIVERY_DATE).format('DD-MM-YYYY');
                                item.PR_DELV_DATE1 = moment(item.PR_DELV_DATE).format('DD-MM-YYYY');
                                item.NEW_DELIVERY_DATE1 = moment(item.NEW_DELIVERY_DATE).format('DD-MM-YYYY');
                                item.COMMENTS = item.COMMENTS == 'null' ? '' : item.COMMENTS;

                                item.UNIT_OF_MEASURE = item.ORDER_UNIT;
                                item.PURCHASE_REQUISITION = item.PURCHASING_DOCUMENT;
                                item.QUANTITY_ORDERED = item.ORDER_QUANTITY;
                                item.DELIVERY_DATE1 = item.PR_DELV_DATE1;
                                item.UNIT_OF_MEASURE = item.ORDER_UNIT;
                                item.QUANTITY_REQUESTED = '';
                                item.CATEGORY = 'PO';

                                item.STILL_TO_BE_DELIVERED_QTY = Math.round(item.STILL_TO_BE_DELIVERED_QTY);

                                excelArray.push(item);

                            });


                            alasql.fn.decimalRound = function (x) { return Math.round(x); };
                            alasql('SELECT ' +
                            ' PLANT_NAME as [Plant], ' +
                            ' MATERIAL as [Material Code],' +
                            ' SHORT_TEXT as [Material Description],' +
                            ' API_NAME as [API Name], ' +
                            ' PR_NUMBER as [PR Number],' +
                            ' UNIT_OF_MEASURE as [UOM],' +

                            ' QUANTITY_REQUESTED as [PR Qty],' +

                            ' PURCHASE_REQUISITION as [PO Number], ' +
                            ' decimalRound(QUANTITY_ORDERED) as [Order Quantity],' +

                            ' STILL_TO_BE_DELIVERED_QTY as [Still to be delivered (qty)], ' +

                            ' DELIVERY_DATE1 as [PR Delivery Date],' +
                            ' LEAD_TIME as [Lead Time], ' +
                            ' IDEAL_DELIVERY_DATE1 as [Ideal Delivery Date],' +

                            ' CATEGORY as [Category (Open PR/Open PO)], ' +

                            ' COMMENTS as [Shortage Comments], ' +
                            ' PURCHASER as [Purchaser],' +

                            ' NEW_DELIVERY_DATE1 as [New Delivery Date] ' +

                            ' INTO  XLSX(?,{headers:true,sheetid: "Shortage Report", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["Shortage Report.xlsx", excelArray]);



                        });



                    });


            };

            $scope.serchString = '';
            $scope.SearchPRData = function (str) {
                if (str && str != '' && str != null && str != undefined && str.length > 0) {
                    str = String(str).toUpperCase();
                    $scope.openPRArr = $scope.openPRArrInitialPageLoadData.filter(function (item) {
                        //return (String(item.PURCHASER).toUpperCase().includes(str) == true);
                        return (String(item.SHORT_TEXT).toUpperCase().includes(str) == true ||
                            String(item.API_NAME).toUpperCase().includes(str) == true ||
                            String(item.MATERIAL).toUpperCase().includes(str) == true);
                    });

                    if (!$scope.$$phase) {
                        $scope.$apply(function () {                            
                            $scope.totalItems = $scope.openPRArr.length;
                        })
                    } else {                        
                        $scope.totalItems = $scope.openPRArr.length;
                    }

                }
                else {
                    $scope.openPRArr = $scope.openPRArrInitialPageLoadData;
                    $scope.totalItems = $scope.openPRArr.length;
                }
            }


            //$scope.searchTable = function (str) {
            //    str = String(str).toUpperCase();
            //    $scope.listPOUser = $scope.listPOUserInitial.filter(function (item) {
            //        return (String(item.PR_NUMBER).toUpperCase().includes(str) == true
            //            || String(item.PURCHASE_ORDER_ID).toUpperCase().includes(str) == true);
            //    });
            //    $scope.totalItems = $scope.listPOUser.length;
            //}

            $scope.clearForm = function () {
                $scope.category = '';
                $scope.newdeliverdate = '';
                $scope.pocomments = '';
            }

            
        }]);