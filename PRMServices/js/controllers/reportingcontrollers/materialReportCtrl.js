﻿
prmApp
    .controller('materialReportCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService","catalogService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, catalogService) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;
            $scope.consalidatedReport = [];
            $scope.userDepartmentIDs = [];
            /*pagination code*/
            $scope.totalItems = 0;
            $scope.totalItems10 = 8;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 8;
            $scope.maxSize = 8;
            $scope.compID = userService.getUserCompanyId();
            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                
            };

            $scope.reports = 1;

           

            $scope.productsList = [];
            $scope.productsListTemp = [];
            $scope.getProducts = function () {
                catalogService.GetMaterialProducts($scope.compID)
                    .then(function (response) {
                        $scope.productsList = response;
                        $scope.productsListTemp = $scope.productsList;
                        $scope.totalItems = $scope.productsList.length;
                        //$scope.productsListRaw = response;
                        //$scope.productsListRaw.forEach(function (item, index) {
                        //        $scope.productsList.push({ "prodName": item.prodName, "prodId": item.prodId });
                        //});
                    });
               
            }
            $scope.getProducts();

            $scope.splitEnabled = 0;

            $scope.GetProdDataReport = function (ReportType, catItemId, name, code) {

                
                $scope.ProdVendorData = [];
                $scope.ProdVendorDataTemp = [];
                $scope.ProdPPSData = [];
                $scope.ProdPPSDataTemp = [];
                $scope.ProdVendorData.VendorCompanyArray = [];

                $scope.isLoadingPrices = 'Loading Data';
                $scope.productNameData = name;
                $scope.productCodeData = code;
                var params = {
                    vendorid: $scope.stateParamVendorId,
                    reportType: ReportType,
                    catitemid: catItemId,
                    sessionid: userService.getUserToken()
                };

                catalogService.GetProdDataReport(params)
                    .then(function (response) {
                        if (response) {
                            if (ReportType == 'MATERIAL_REPORT') {
                                $scope.ProdVendorData = response;
                                $scope.ProdVendorDataTemp = response;

                                if ($scope.ProdVendorData.length > 0) {
                                    $scope.isLoadingPrices = '';
                                } else {
                                    $scope.isLoadingPrices = 'No data availble';
                                }
                                $scope.ProdVendorData.forEach(function (data, dataIndex) {
                                    data.LAST_MODIFIED_DATE = new moment(data.LAST_MODIFIED_DATE).format("DD-MM-YYYY");
                                    data.REQ_POSTED_ON = new moment(data.REQ_POSTED_ON).format("DD-MM-YYYY");
                                    $scope.splitEnabled = data.IS_SPLIT_ENABLED;

                                    if (data.VEND_PROD_DETAILS) {
                                        data.VEND_PROD_DETAILS.forEach(function (details) {
                                            if (details.FOR_TO_PAY === 1) {

                                                details.REV_UNIT_FREIGHT = 'NA';

                                            }
                                        });
                                    }
                                });
                            }

                            if (ReportType == 'PPS_REPORT') {

                                $scope.ProdPPSData = response;
                                $scope.ProdPPSDataTemp = response;

                                if ($scope.ProdPPSData.length > 0) {
                                    $scope.isLoadingPrices = '';
                                } else {
                                    $scope.isLoadingPrices = 'No data availble';
                                }
                                $scope.ProdPPSData.forEach(function (data, dataIndex) {

                                    data.ppsJSONArray = [];

                                    if (data.PR_CODE == '' || data.PR_CODE == undefined) {
                                        data.PR_CODE = 'NA';
                                    }
                                    if (data.FOR_TO_PAY == 1) {
                                        data.REV_UNIT_FREIGHT = 'NA';

                                    }
                                    data.DATE_CREATED = new moment(data.DATE_CREATED).format("DD-MM-YYYY");
                                    data.DTAE_MODIFIED = new moment(data.DTAE_MODIFIED).format("DD-MM-YYYY");

                                    data.ppsJSONArray = $.parseJSON(data.PPS_FIELDS_PO_JSON);

                                    data.ppsJSONArray.forEach(function (details,detailsIndex) {
                                        if (details.fieldName == "BASIC") {
                                            data.RevUnitPrice = details.fieldData;
                                            if (details.fieldData == "" || details.fieldData == null || details.fieldData == undefined || details.fieldData == 0 || details.fieldData <= 0) {
                                                data.RevUnitPrice = 'NA';
                                            }
                                        }
                                        if (details.fieldName == "FREIGHT") {
                                            data.RevUnitFreight = details.fieldData;
                                            if (details.fieldData == "" || details.fieldData == null || details.fieldData == undefined || details.fieldData == 0 || details.fieldData <= 0) {
                                                data.RevUnitFreight = 'NA';
                                            }
                                        }
                                        if (details.fieldName == "PO NUMBER") {
                                            data.PoNumber = details.fieldData;
                                            if (details.fieldData == "" || details.fieldData == null || details.fieldData == undefined) {
                                                data.PoNumber = 'NA';
                                            }
                                        }
                                        
                                    })
                                })
                            }
                         
                        } else {
                            $scope.isLoadingPrices = 'No data availble';
                        }
                    })
            };

           

            $scope.searchTable = function (str) {
                var string = str.toLowerCase();
                $scope.productsList = $scope.productsListTemp.filter(function (req) {
                    return (String(req.prodNo).toLowerCase(string).includes(string) == true || String(req.prodName).toLowerCase(string).includes(string) == true);
                  
                });

                $scope.totalItems = $scope.productsList.length;
            }

            $scope.searchTableMaterial = function (str) {
                var string = str.toLowerCase();
                $scope.ProdVendorData = $scope.ProdVendorDataTemp.filter(function (req) {
                    return (String(req.QUANTITY).toLowerCase(string).includes(string) == true || String(req.REQ_ID).toLowerCase(string).includes(string) == true || String(req.VENDOR_COMPANY).toLowerCase(string).includes(string) == true);

                });

                // $scope.totalItems = $scope.ProdPPSData.length;
            }

            $scope.searchTablePPS = function (str) {
                var string = str.toLowerCase();
                $scope.ProdPPSData = $scope.ProdPPSDataTemp.filter(function (req) {
                    return (String(req.VENDOR_NAME).toLowerCase(string).includes(string) == true || String(req.REQ_ID).toLowerCase(string).includes(string) == true);

                });

                // $scope.totalItems = $scope.ProdPPSData.length;
            }


            $scope.getReport = function (name, isCompleteReport) {
                
                if (isCompleteReport) {
                    reportingService.downloadConsolidatedTemplate(name, "0001-01-01", "9999-12-12", userService.getUserId());
                } else {
                    reportingService.downloadConsolidatedTemplate(name, $scope.reportFromDate, $scope.reportToDate, userService.getUserId());
                }
            }

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                }
            };

        }]);