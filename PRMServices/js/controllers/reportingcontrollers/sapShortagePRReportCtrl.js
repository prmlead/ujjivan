﻿prmApp

    .controller('sapShortagePRReportCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$log", "$stateParams", "$timeout",
        "auctionsService", "reportingService", "userService", "SignalRFactory", "fileReader", "growlService",
        function ($scope, $http, $state, domain, $filter, $log, $stateParams, $timeout,
            auctionsService, reportingService, userService, SignalRFactory, fileReader, growlService) {

            $scope.deliveryMissed = 0;
            $scope.deliveryAlert = 0;
            $scope.timeAvailable = 0;

            $scope.minDateMoment = moment();

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 8;
            $scope.maxSize = 5; //Number of pager buttons to show
            $scope.errorMessage = '';
            $scope.color = '';
            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/
            
            $scope.openPRArr = [];
            $scope.openPRArrForSortig = $scope.openPRArr;
            $scope.openChildPRArr = [];
            $scope.openPRArrInitial = [];
            $scope.openPRPivot = [];
            $scope.openPRPivotData = [];
            $scope.filterModelPurchaseUser = '0';
            $scope.category = '0';
            $scope.filterDateRange = 0;
            $scope.pritemcomments = [];
            $scope.selectedPRItem;
            $scope.prquantity = 0;
            $scope.prcomments = '';
            $scope.selectedGraphFilter;
            $scope.monthWiseReport = {};
            $scope.tileFilter = '';
            $scope.filterModelPlant = '0';
            $scope.filterModelExclusion = '0';
            $scope.selectedMonth = '';
            $scope.displayDetails = false;
            $scope.tileFilterTextToDisplay = '';
            $scope.showDetailedView = false;
            $scope.accessTypes = [];
            $scope.permission = 'VIEW';
            $scope.lastUpdatedDate = "";
            $scope.commentType = "SHORTAGE";

            $scope.getLastUpdatedDate = function () {
                reportingService.GetLastUpdatedDate('sap_openpr')
                    .then(function (response) {
                        $scope.lastUpdatedDate = response.message;
                    });
            }

            $scope.getUserAccess = function () {
                reportingService.GetSapAccess(userService.getUserId())
                    .then(function (response) {
                        $scope.accessTypes = response;
                        if ($scope.accessTypes && $scope.accessTypes.length > 0) {
                            $scope.filterModelPurchaseUser = $scope.accessTypes[0].objectID;
                            $scope.permission = $scope.accessTypes[0].errorMessage;
                            $scope.getOpenPR(0, $scope.filterModelPurchaseUser, 0);
                        }
                    });
            }

            $scope.getUserAccess();
            $scope.getLastUpdatedDate();
            
            // in pr
            $scope.openPRArrInitialPageLoadData = [];
            $scope.getOpenPR = function (plant, purchase, exclusionList) {
                $scope.serchString = '';
                reportingService.GetOpenPRShortageReport(0, 0, 0, 0)
                    .then(function (response) {
                        $log.info(response);

                        if ($scope.openPRArrInitialPageLoadData.length == 0) {
                            $scope.openPRArrInitialPageLoadData = response;
                        }
                        $scope.openPRArr = response;
                        $scope.openPRArrInitial = response;

                        $scope.openPRArr.forEach(function (item) {
                            item.PO_DATE1 = moment(item.PO_DATE).format('DD-MM-YYYY');
                            item.PO_DELV_DATE1 = moment(item.PO_DELV_DATE).format('DD-MM-YYYY');


                            item.PR_CREATE_DATE1 = moment(item.REQUISITION_DATE).format('DD-MM-YYYY');
                            item.IDEAL_DELIVERY_DATE1 = moment(item.DELIVERY_DATE).format('DD-MM-YYYY');
                            item.PR_DELV_DATE1 = moment(item.DELIVERY_DATE).format('DD-MM-YYYY');


                            item.REQUISITION_DATE1 = moment(item.REQUISITION_DATE).format('DD-MM-YYYY');
                            item.DELIV_DATE_FROM_TO1 = moment(item.DELIV_DATE_FROM_TO).format('DD-MM-YYYY');
                            item.DELIVERY_DATE1 = moment(item.DELIVERY_DATE).format('DD-MM-YYYY');


                            if (item.NWSTATUS == 'REVIEW PENDING') {
                                item.color = 'Red';
                            } else if (item.NWSTATUS == 'REVIEWED BUT ACTION PENDING') {
                                item.color = 'Yellow';
                            } else if (item.NWSTATUS == 'REVIEWED AND ACTION TAKEN') {
                                item.color = 'GreenYellow';
                            }
                            if (item.NEW_DELIVERY_DATE_COMM == null) {
                                item.NEW_DELIVERY_DATE_COMM = '-';
                            } else {
                                item.NEW_DELIVERY_DATE_COMM = moment(item.NEW_DELIVERY_DATE_COMM).format('DD-MM-YYYY');
                            }
                        });
                        $scope.totalItems = $scope.openPRArr.length;
                    });
            }


            $scope.getShortageOpenPRFilter = function (category, idealFromDate, idealToDate, filterModelPurchaseUser) {
                if (filterModelPurchaseUser > 0 && category != "0") {
                    $scope.openPRArr = $scope.openPRArrInitial.filter(function (item) {
                        if (filterModelPurchaseUser == item.PURCHASING_GROUP && category == item.CATEGORY) {
                            return item;
                        }
                    })
                }
                else if (category != "0") {
                    $scope.openPRArr = $scope.openPRArrInitial.filter(function (item) {
                        if (category == item.CATEGORY) {
                            return item;
                        }
                    })
                }
                else if (filterModelPurchaseUser > 0) {
                    $scope.openPRArr = $scope.openPRArrInitial.filter(function (item) {
                        if (filterModelPurchaseUser == item.PURCHASING_GROUP) {
                            return item;
                        }
                    })
                }
                else {
                    $scope.openPRArr = $scope.openPRArrInitial;
                }


                if (idealFromDate && idealToDate) {

                    var tsIdealFromDate = moment(idealFromDate, "DD-MM-YYYY HH:mm").valueOf();
                    tsIdealFromDate = moment(tsIdealFromDate);
                    tsIdealFromDate = new Date(tsIdealFromDate);

                    var tsIdealToDate = moment(idealToDate, "DD-MM-YYYY HH:mm").valueOf();
                    tsIdealToDate = moment(tsIdealToDate);
                    tsIdealToDate = new Date(tsIdealToDate);

                    $scope.openPRArr = $scope.openPRArr.filter(function (item) {
                        console.log('------------------------------>');
                        console.log(idealFromDate);
                        console.log(item.IDEAL_DELIVERY_DATE1);
                        console.log(idealToDate);
                        console.log(tsIdealFromDate <= new Date(moment(moment(item.IDEAL_DELIVERY_DATE1, "DD-MM-YYYY HH:mm").valueOf())));
                        console.log(tsIdealToDate >= new Date(moment(moment(item.IDEAL_DELIVERY_DATE1, "DD-MM-YYYY HH:mm").valueOf())));
                        console.log('------------------------------>');
                        if (tsIdealFromDate <= new Date(moment(moment(item.IDEAL_DELIVERY_DATE1, "DD-MM-YYYY HH:mm").valueOf())) &&
                            tsIdealToDate >= new Date(moment(moment(item.IDEAL_DELIVERY_DATE1, "DD-MM-YYYY HH:mm").valueOf()))) {
                            return item;
                        }
                    })
                    $scope.totalItems = $scope.openPRArr.length;
                } else
                {
                    $scope.openPRArr = $scope.openPRArr;
                    $scope.totalItems = $scope.openPRArr.length;
                }
            }


            $scope.exportShortageReportExcel = function () {
                var mystyle = {
                    sheetid: 'PR_Report',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };
                alasql.fn.decimalRound = function (x) { return Math.round(x); };   
                alasql('SELECT PLANT_NAME as [Plant Name], ' +
                    ' PURCHASER as [Purch. Name], ' +
                    ' PURCHASE_REQUISITION as [PR No.], ' +
                    ' ITEM_OF_REQUISITION as [PR Item No], ' +
                    ' REQUISITION_DATE1 as [PR Creat. Date], ' +
                    ' MATERIAL as [Material], ' +
                    ' SHORT_TEXT as [Material Desc.], ' +
                    ' API_NAME as [API Name], ' +
                    ' UNIT_OF_MEASURE as [UOM], ' +
                    ' decimalRound(QUANTITY_REQUESTED) as [PR Qty.], ' +
                    ' decimalRound(QUANTITY_ORDERED) as [Orderd Qty.], ' +
                    ' LEAD_TIME as [Lead Time], ' +
                    ' DELIV_DATE_FROM_TO1 as [Delivery Date], ' +
                    ' IDEAL_DELIVERY_DATE1 as [Ideal Delivery Date], ' +
                    ' CATEGORY as [Category], ' +
                    ' NWSTATUS as [STATUS], ' +
                    ' COMMENTS as [COMMENTS] ' +
                    
                    //' PURCHASER as [LP], PLANT_NAME as [Plant], MATERIAL as [MATERIAL], SHORT_TEXT as [DESCRIPTION], API_NAME as [API], NAME_OF_SUPPLIER as [SUPPLIER], '+
                    //' PR_CREATE_DATE1 as [PR DATE],PO_DATE1 as [PO DATE],PR_DELV_DATE1 as [PR Delivery Date],IDEAL_DELIVERY_DATE1 as [Ideal Delivery Date], ORDER_UNIT as [ORDER UNIT], ' +
                    //' decimalRound(PR_QUAN) as [PR Qty],decimalRound(ORDER_QUANTITY) as [PR Qty], decimalRound(STILL_TO_BE_DELIVERED_QTY) as [Open Qty], ' +
                    //' PR_NUMBER as [PR Number], PR_ITM_NO as [PR LN], PURCHASING_DOCUMENT as [PR Number], ITEM as [PR LINE ITEM], USER_ID as [PR Creator], MULTIPLE_PR_TEXT as [Multiple PR], ' +
                    //' IMPORT_LOCAL as [Supp. Country], LEAD_TIME as [Lead Time],' +
                    //' CATEGORY as [Category], NWSTATUS as [Status], NEW_DELIVERY_DATE_COMM as [New Delivery Date], COMMENTS as [Comments] ' +
                    
                
                    ' INTO XLSX(?, { headers: true, sheetid: "PR_Report", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ', ["PR_Report.xlsx", $scope.openPRArr]);
            }


            $scope.scrollWin = function (id, scrollPosition) {
                var elmnt = document.getElementById(id);
                elmnt.scrollIntoView();

                if (scrollPosition == 'BOTTOM')
                {
                    window.scroll({ bottom: elmnt.offsetBottom });
                }
                else {
                    window.scroll({ top: elmnt.offsetTop });
                }
            };

            document.body.scrollTop = 0; // For Chrome, Safari and Opera 
            document.documentElement.scrollTop = 0; // For IE and Firefox



        }]);