﻿prmApp
    .controller('locationCodesCtrl', ["$scope", "userService", "auctionsService", "growlService", "$http", "domain",
        "$location", "$anchorScroll",
        function ($scope, userService, auctionsService, growlService, $http, domain, $location, $anchorScroll) {

            $scope.userId = userService.getUserId();
            $scope.sessionId = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();

            $scope.addNewLocationView = false;

            $scope.location = {
                userID: $scope.userId,
                sessionID: $scope.sessionId,
                LOCATION_CODE: '',
                LOCATION_NAME: '',
                LOCATION_TYPE: '',
                LOCATION_ADDR: '',
                compID: $scope.compId
            };

            $scope.locationCodes = [];

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 5;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/

            $scope.editLocation = function (location, isEdit) {
                $scope.addNewLocationView = true;
                if (!isEdit) {
                    $scope.location = {
                        userID: $scope.userId,
                        sessionID: $scope.sessionId,
                        LOCATION_CODE: '',
                        LOCATION_NAME: '',
                        LOCATION_TYPE: '',
                        LOCATION_ADDR: '',
                        compID: $scope.compId
                    };
                } else {
                    $scope.location = {
                        LOCATION_ID: location.LOCATION_ID,
                        COMP_ID: $scope.compId,
                        LOCATION_CODE: location.LOCATION_CODE,
                        LOCATION_NAME: location.LOCATION_NAME,
                        LOCATION_TYPE: location.LOCATION_TYPE,
                        LOCATION_ADDR: location.LOCATION_ADDR,
                        CREATED_BY: $scope.userId,
                        sessionID: $scope.sessionId
                    };
                }                
            };

            $scope.getLocations = function () {
                $scope.locationCodes = [];
                $http({
                    method: 'get',
                    url: domain + 'getlocations?compid=' + $scope.compId + '&sessionid=' + $scope.sessionId,
                    encodeuri: true,
                    headers: { 'content-type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data && response.data.length > 0) {
                        $scope.locationCodes = response.data;
                        $scope.totalItems = $scope.locationCodes.length;
                    }
                });
            };

            $scope.getLocations();

            $scope.closeLocationView = function () {
                $scope.addNewLocationView = false;
            };

            $scope.SaveLocation = function () {
                if (!$scope.location.LOCATION_CODE) {
                    growlService.growl("Please enter location code.", 'inverse');
                    return;
                }

                if (!$scope.location.LOCATION_NAME) {
                    growlService.growl("Please enter location name.", 'inverse');
                    return;
                }

                if (!$scope.location.LOCATION_TYPE) {
                    growlService.growl("Please select location type.", 'inverse');
                    return;
                }

                let locationObj = {
                    LOCATION_ID: $scope.location.LOCATION_ID,
                    COMP_ID: $scope.compId,
                    LOCATION_CODE: $scope.location.LOCATION_CODE,
                    LOCATION_NAME: $scope.location.LOCATION_NAME,
                    LOCATION_TYPE: $scope.location.LOCATION_TYPE,
                    LOCATION_ADDR: $scope.location.LOCATION_ADDR,
                    CREATED_BY: $scope.userId,
                    sessionID: $scope.sessionId
                };

                var params = {
                    "locationDetails": locationObj
                };

                auctionsService.SaveLocation(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Location details saved successfully.", "success");
                            $scope.getLocations();
                        }

                        $scope.location = {
                            userID: $scope.userId,
                            sessionID: $scope.sessionId,
                            LOCATION_CODE: '',
                            LOCATION_NAME: '',
                            LOCATION_TYPE: '',
                            LOCATION_ADDR: '',
                            compID: $scope.compId
                        };
                    });
            };

        }]);

