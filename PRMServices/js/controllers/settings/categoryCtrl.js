﻿prmApp

    .controller('categoryCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, $http, domain, $rootScope, fileReader, $filter, $log) {
            $scope.isCustomer = userService.getUserType();
            $scope.sessionid = userService.getUserToken();
            $scope.userObj = {};
            $scope.selectedCategory = {};
            $scope.filteredCategories = [];
            userService.getUserDataNoCache()
                .then(function (response) {
                    $scope.userObj = response;

                })




            $scope.categoriesList = [];
            $scope.uniqueCategories = [];

            $scope.categoriesListLength = $scope.categoriesList.length;

            $scope.categoryObj = {
                id: 0,
                category: '',
                subcategory: ''
            };


            $scope.editCategoriesFunction = function (category, value) {
                category.editCategories = value;
            };

            $scope.cancelEditCategories = function (category, value) {
                category.editCategories = value;
            };


            $scope.addCatToArray = function (categoryObj) {
                categoryObj.compID = userService.getUserCompanyId();
                categoryObj.sessionID = userService.getUserToken();

                auctionsService.SaveCompanyCategories(categoryObj)
                    .then(function (response) {

                        if (response.errorMessage == '') {
                            growlService.growl("Data Saved Successfully", "success");

                            $scope.categoryObj = {
                                id: 0,
                                category: $scope.selectedCategory.category,
                                subcategory: ''
                            };

                        }
                        else {
                            growlService.growl(response.errorMessage, "inverse");
                        }

                        $scope.GetCompanyCategories();

                        location.reload();



                    });
            }


            $scope.loadSubCategories = function () {
                $scope.selectedCategory = JSON.parse($scope.selectedCategory);
                $scope.filteredCategories = _.filter($scope.categoriesList, function (x) { return x.category.toUpperCase() === $scope.selectedCategory.category.toUpperCase(); });
                $log.info($scope.filteredCategories);
            };

            $scope.GetCompanyCategories = function () {

                auctionsService.GetCompanyCategories(userService.getUserCompanyId(), userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.categoriesList = response;
                            $scope.uniqueCategories = _.map(_.groupBy($scope.categoriesList, function (cat) {
                                return cat.category;
                            }), function (grouped) {
                                return grouped[0];
                            });

                            $log.info($scope.uniqueCategories);
                            $scope.categoriesListLength = $scope.categoriesList.length;

                        }
                    })
            }

            $scope.GetCompanyCategories();


            $scope.addSubCat = function (val) {
                $scope.categoryObj.category = val
                window.scrollTo(0, 0);
            };


        }]);