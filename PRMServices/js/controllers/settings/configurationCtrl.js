﻿prmApp
    .controller('configurationCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "$http", "domain", "PRMPRServices",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, $http, domain, PRMPRServices) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            $scope.companyRegions = [];

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            $scope.totalItems1 = 0;
            $scope.currentPage1 = 1;
            $scope.itemsPerPage1 = 10;
            $scope.maxSize1 = 5;


            $scope.setPage1 = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged1 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/
            $scope.showConfiguration = false;
            $scope.showPlant = false;

            $scope.showTable = function (val) {
                if (val == 1) {
                    $scope.showConfiguration = true;
                    $scope.showPlant = false;
                } else {
                    $scope.showConfiguration = false;
                    $scope.showPlant = true;
                }
            }

            $scope.companyConfigurations = [];
            $scope.companyConfigurations1 = [];

            $scope.configuration = {
                compConfigID: 0,
                compID: $scope.compID,
                configKey: '',
                configText: '',
                configValue1: '',
                configValue2: '',
                configType: '',
                configValue: '',
                isValid: true,
            };
            //$scope.location = {
            //    compConfigID: 0,
            //    compID: 0,
            //    configKey: '',
            //    configText: '',
            //    configType: '',
            //    configValue: '',
            //    isValid: true,
            //};
            //$scope.region = {
            //    compConfigID: 0,
            //    compID: 0,
            //    configKey: '',
            //    configText: '',
            //    configType: '',
            //    configValue: '',
            //    isValid: true,
            //};

            $scope.allMappingsData = [];
            $scope.locationMappings = [];
            $scope.companyLocation = [];
            $scope.companyLocationRegions = [];

            $scope.addnewconfigView = false;

            $scope.GetCompanyConfigurations = function () {
                $scope.companyLocation = [];
                $scope.companyLocationRegions = [];
                $scope.allTypes = 'USER_LOCATION,LOCATION_REGION'
                auctionsService.GetCompanyConfiguration(userService.getUserCompanyId(), $scope.allTypes, userService.getUserToken())
                    .then(function (response) {
                        $scope.companyConfigurations = response;
                        $scope.companyConfigurations1 = response;

                        $scope.companyConfigurations.forEach(function (item, index) {
                            if (item.configKey == 'USER_LOCATION') {
                                $scope.companyLocation.push(item);
                                $scope.totalItems = $scope.companyLocation.length;
                            } else if (item.configKey == 'LOCATION_REGION') {
                                $scope.companyLocationRegions.push(item);
                            }
                        });

                        //$scope.totalItems = $scope.companyConfigurations.length;
                    })
            };

            $scope.GetCompanyConfigurations();

            $scope.GetfeildMappings = function () {
                auctionsService.GetfeildMappings(userService.getUserCompanyId(), 'LOCATION_MAPPTING', userService.getUserToken())
                    .then(function (response) {
                        $scope.allMappingsData = response;

                        $scope.allMappingsData.forEach(function (item, index) {
                            if (item.configKey == 'LOCATION_MAPPTING') {
                                $scope.locationMappings.push(item);
                            }
                        });

                        $scope.totalItems = $scope.allMappingsData.length;
                    })
            }


            $scope.SaveCompanyConfigurations = function () {
                $scope.configuration.compID = $scope.compID;
                $scope.configuration.userID = $scope.userID;
                $scope.configuration.configKey = 'USER_LOCATION';
                $scope.configuration.configValue2 = $scope.configuration.configValue2.replace(/\n/g, "<br />");
                $scope.configuration.configValue2 = $scope.configuration.configValue2.replace(/\t/g, "/t");
                var params = {
                    "listCompanyConfiguration": [$scope.configuration],
                    "sessionID": userService.getUserToken()
                };
                //auctionsservice.savecompanyconfiguration(params)
                $http({
                    method: 'POST',
                    url: domain + 'savecompanyconfiguration',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                })
                    .then(function (response) {
                        if (response && response.data && !response.data.errorMessage) {
                            growlService.growl('Successfully Saved', 'success');
                            $scope.GetCompanyConfigurations();
                            $scope.addnewconfigView = false;
                            $scope.configuration = {
                                compConfigID: 0,
                                compID: 0,
                                configKey: '',
                                configText: '',
                                configValue1: '',
                                configValue2: '',
                                configType: '',
                                configValue: '',
                                isValid: true,
                            };
                            $scope.companyConfigurationForm.$setUntouched();
                        }
                        else {
                            growlService.growl(response.data.errorMessage, "inverse");
                        }
                    });

            };

            $scope.editConfiguration = function (companyConfiguration) {
                $scope.addnewconfigView = true;
                $scope.configuration = companyConfiguration;

                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox

            };


            $scope.closeEditConfiguration = function () {
                //$scope.addnewconfigView = false;
                $scope.configuration = {
                    compConfigID: 0,
                    compID: 0,
                    configKey: '',
                    configText: '',
                    configValue1: '',
                    configValue2: '',
                    configType: '',
                    configValue: '',
                    isValid: true,
                };

                $scope.companyConfigurationForm.$setUntouched();

            };

            //Configuration//

            $scope.companyPlants = [];
            $scope.companyPlants1 = [];

            $scope.plant = {
                FIELD_PK_ID: 0,
                FIELD_VALUE: '',
                FIELD_NAME: ''
            };


            $scope.disablefields = false;

            $scope.GetCompanyERPFieldMappings = function () {
                var params = {
                    "type": "'PLANTS'"
                };
                PRMPRServices.getprfieldmapping(params)
                    .then(function (response) {
                        $scope.companyPlants = response;
                        $scope.companyPlants1 = response;
                        $scope.totalItems1 = $scope.companyPlants.length;
                    })
            }

            $scope.GetCompanyERPFieldMappings();


            $scope.SaveCompanyERPFieldMappings = function (type) {
                $scope.plant.FIELD_TYPE = type;
                $scope.plant.FIELD_NAME = $scope.configuration.configText;
                $scope.plant.FIELD_VALUE = $scope.configuration.configValue;
                var params = {
                    "listCompanyERPFieldMapping": [$scope.plant],
                    "sessionID": userService.getUserToken()
                };
                $http({
                    method: 'POST',
                    url: domain + 'savecompanyerpfieldmapping',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                })
                    .then(function (response) {
                        if (response && response.data && !response.data.errorMessage) {
                            growlService.growl('Successfully Saved', 'success');
                            $scope.GetCompanyERPFieldMappings();
                            $scope.addnewplantView = false;
                            $scope.disablefields = false;
                            $scope.plant = {
                                FIELD_PK_ID: 0,
                                FIELD_VALUE: '',
                                FIELD_NAME: ''
                            };
                        }
                        else {
                            growlService.growl(response.data.errorMessage, "inverse");
                        }
                    });

            };

            $scope.editPlant = function (companyPlant) {
                if (companyPlant.FIELD_PK_ID == 0) {
                    $scope.disablefields = false;
                } else {
                    $scope.disablefields = true;
                }
                $scope.addnewplantView = true;
                $scope.plant = companyPlant;

                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox
            };

            $scope.closeEditPlant = function () {
                $scope.disablefields = false;
                $scope.addnewplantView = false;
                $scope.plant = {
                    FIELD_PK_ID: 0,
                    FIELD_VALUE: '',
                    FIELD_NAME: ''
                };

            };


            /*PAGINATION CODE*/
            $scope.totalItems1 = 0;
            $scope.currentPage1 = 1;
            $scope.itemsPerPage1 = 10;
            $scope.maxSize1 = 5;


            $scope.setPage1 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged1 = function () {
            };

            $scope.totalItems2 = 0;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize2 = 5;


            $scope.setPage2 = function (pageNo) {
                $scope.currentPage2 = pageNo;
            };

            $scope.pageChanged2 = function () {
            };

            $scope.totalItems3 = 0;
            $scope.currentPage3 = 1;
            $scope.itemsPerPage3 = 10;
            $scope.maxSize3 = 5;


            $scope.setPage3 = function (pageNo) {
                $scope.currentPage3 = pageNo;
            };

            $scope.pageChanged3 = function () {
            };
            /*PAGINATION CODE*/


            $scope.region = {
                userID: $scope.userID,
                regName: '',
                branchName: '',
                locationName: '',
                compID: $scope.compID,
                rbType: '',
                rbID:0
            };
            $scope.createNewRegionView = false;
            $scope.createNewBranchView = false;
            $scope.createNewLocationView = false;
            $scope.createNewMapView = false;
            $scope.mappingArr = [];

            $scope.openRegion = function (type) {
                if (type === "REGION") {
                    $scope.createNewRegionView = true;
                }
                else if (type === "BRANCH") {
                    $scope.createNewBranchView = true;
                }
                else if (type === "LOCATION") {
                    $scope.createNewLocationView = true;
                }
                else if (type === "MAP") {
                    $scope.createNewMapView = true;
                    $scope.mapObject =
                    {
                        userID: +$scope.userID,
                        compID: +$scope.compID,
                        regName: '',
                        branchName: '',
                        locationName: '',
                        style: '',
                        rbID: 0,
                        mrbID:0
                    };
                    $scope.mappingArr.push($scope.mapObject);
                }
            };

            $scope.CompanyRegions = [];

            $scope.saveCompanyRegion = function (type) {
                $scope.CompanyRegions = [];

                if (type === 'REGION') {
                    if (!$scope.region.regName) {
                        growlService.growl("Please Enter Region Name.", 'inverse');
                        return;
                    }
                    $scope.region.rbType = 'REGION'
                }

                if (type === 'BRANCH') {
                    if (!$scope.region.branchName) {
                        growlService.growl("Please Enter Branch Name.", 'inverse');
                        return;
                    }
                    $scope.region.rbType = 'BRANCH'
                }

                if (type === 'LOCATION') {
                    if (!$scope.region.locationName) {
                        growlService.growl("Please Enter Location Name.", 'inverse');
                        return;
                    }
                    $scope.region.rbType = 'LOCATION'
                }

                $scope.CompanyRegions.push($scope.region);

                var params = {
                    "ListCompanyRegion": $scope.CompanyRegions,
                    "sessionID": userService.getUserToken()
                };

                auctionsService.SaveCompanyRegion(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl($scope.region.rbType + " Saved Successfully.", "success");
                            $scope.createNewRegionView = false;
                            $scope.createNewBranchView = false;
                            $scope.createNewLocationView = false;
                            $scope.region = {
                                userID: +$scope.userID,
                                regName: '',
                                branchName: '',
                                locationName: '',
                                compID: +$scope.compID,
                                rbType: ''
                            };
                            $scope.getcompanyregion();
                        }
                    });
            };

            $scope.MapCompanyRegionLocation = function () {

                if ($scope.mappingArr.length > 0) {
                    $scope.mappingArr.forEach(function (item,index) {
                        item.style = '';
                        item.regStyleName = '';
                        item.branchStyleName = '';
                        item.locationStyleName = '';

                        item.regName = _.find($scope.mappingArr, { rbID: item.regionID }).regName;
                        item.branchName = _.find($scope.mappingArr, { rbID: item.branchID }).branchName;
                        item.locationName = _.find($scope.mappingArr, { rbID: item.delvLocationID }).locationName;
                        item.compID = +$scope.compID;
                        item.userID = +$scope.userID;
                        
                        if (!(item.regName)) {
                            item.regStyleName = "ERROR";
                            item.style = 'ERROR';
                        }
                        if (!(item.branchName)) {
                            item.branchStyleName = "ERROR";
                            item.style = 'ERROR';
                        }
                        if (!(item.locationName)) {
                            item.locationStyleName = "ERROR";
                            item.style = 'ERROR';
                        }
                    });
                }

                let success = true;
                success = $scope.validateMandatoryFields();

                if (!success) {
                    return;
                }

                var params = {
                    "mapRegionArr": $scope.mappingArr,
                    "sessionID": userService.getUserToken()
                };

                auctionsService.MapCompanyRegionLocation(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl($scope.region.rbType + " Saved Successfully.", "success");
                            $scope.createNewMapView = false;
                            //$scope.mapObject = { "regName": '', "branchName": '', "locationName": '' };
                            //$scope.getcompanyregion();
                        }
                    });
            };


            $scope.validate = function (obj,type) {
                if (type === 'REGION') {
                    obj.regStyleName = '';
                }

                if (type === 'BRANCH') {
                    obj.branchStyleName = '';
                }

                if (type === 'LOCATION') {
                    obj.locationStyleName = '';
                }
            };

            $scope.validateMandatoryFields = function () {
                let isValid = false;
                isValid = _.some($scope.mappingArr, function (item) {
                    return !item.style;
                });
                return isValid;
            };


            $scope.addToMapping = function () {
                $scope.mapObject =
                {
                    userID: +$scope.userID,
                    compID: +$scope.compID,
                    regName: '',
                    branchName: '',
                    locationName: '',
                    regionID: 0,
                    branchID: 0,
                    delvLocationID: 0
                };
                $scope.mappingArr.push($scope.mapObject);
            };

            $scope.delFromMapping = function (index) {
                if ($scope.mappingArr.length > 0) {
                    $scope.mappingArr.splice(index, 1);
                }
            };


            $scope.closeEditRegion = function () {
                $scope.createNewRegionView = false;
                $scope.createNewBranchView = false;
                $scope.createNewLocationView = false;
                $scope.createNewMapView = false;
                $scope.region = {
                    userID: +$scope.userID,
                    regName: '',
                    branchName: '',
                    locationName: '',
                    compID: +$scope.compID,
                    //isValid: true,
                    rbType: ''
                };

            };
            $scope.regions = [];
            $scope.branches = [];
            $scope.locations = [];
            $scope.getcompanyregion = function () {
                auctionsService.GetCompanyRegion($scope.compID, $scope.userID, $scope.sessionID)
                    .then(function (response) {
                        $scope.regions = [];
                        $scope.branches = [];
                        $scope.locations = [];
                        $scope.companyRegions = response;
                        $scope.companyRegions.forEach(function (item,index) {
                            if (item.rbType == "REGION") {
                                $scope.regions.push(item);
                            }

                            if (item.rbType == "BRANCH") {
                                $scope.branches.push(item);
                            }

                            if (item.rbType == "LOCATION") {
                                $scope.locations.push(item);
                            }
                        });
                        $scope.totalItems1 = $scope.regions.length;
                        $scope.totalItems2 = $scope.branches.length;
                        $scope.totalItems3 = $scope.locations.length;
                    });
            };
            $scope.getcompanyregion();

            $scope.editValue = function (val, type) {
                if (type === 'REGION') {
                    $scope.createNewRegionView = true;
                    //scrollTop = angular.element("#regName").offsetTop;
                }
                else if (type === 'BRANCH') {
                    $scope.createNewBranchView = true;
                    //scrollTop = angular.element("#branchName").offsetTop;
                }
                if (type === 'LOCATION') {
                    $scope.createNewLocationView = true;
                    //scrollTop = angular.element("#locationName").offsetTop;
                }
                $scope.region = val;
            };

            $scope.DeleteDepartment = function (companyRegion) {

                var params = {
                    compID: companyRegion.compID,
                    sessionID: $scope.sessionID
                };

                auctionsService.DeleteRegion(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Deleted Successfully.", "success");
                            $scope.getcompanyregion();
                            //$window.history.back();
                        }
                    });
                //$scope.createNewRegionView = false;
            };

        }]);