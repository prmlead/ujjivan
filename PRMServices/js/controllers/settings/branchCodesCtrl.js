﻿prmApp
    .controller('branchCodesCtrl', ["$scope", "userService", "auctionsService", "growlService", "$http", "domain",
        "$location", "$anchorScroll",
        function ($scope, userService, auctionsService, growlService, $http, domain, $location, $anchorScroll) {

            $scope.userId = userService.getUserId();
            $scope.sessionId = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.locationCodes = [];
            $scope.uniqueRegions = [];
            $scope.addNewBranchView = false;

            $scope.branch = {
                RH_ID: 0,
                userID: $scope.userId,
                sessionID: $scope.sessionId,
                compID: $scope.compId,
                BRANCH_CODE: '',
                BRANCH_NAME: '',
                REGION_CODE: '',
                LOCATION_CODE: '',
                LOCATION_ID: 0,
                selectedLocation: 0
            };

            $scope.branchCodes = [];

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 5;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/


            $scope.editBranch = function (branch, isEdit) {
                $scope.addNewBranchView = true;
                if (!isEdit) {
                    $scope.branch = {
                        RH_ID: 0,
                        userID: $scope.userId,
                        sessionID: $scope.sessionId,
                        compID: $scope.compId,
                        BRANCH_CODE: '',
                        BRANCH_NAME: '',
                        REGION_CODE: '',
                        LOCATION_CODE: ''
                    };
                } else {
                    $scope.branch = {
                        userID: $scope.userId,
                        sessionID: $scope.sessionId,
                        compID: $scope.compId,
                        RH_ID: branch.RH_ID,
                        BRANCH_CODE: branch.BRANCH_CODE,
                        BRANCH_NAME: branch.BRANCH_NAME,
                        REGION_CODE: branch.REGION_CODE,
                        LOCATION_CODE: branch.LOCATION_CODE,
                        selectedLocation: branch.LOCATION_ID
                    };
                }                
            };

            $scope.getBranches = function () {
                $scope.uniqueRegions = [];
                $scope.branchCodes = [];
                $http({
                    method: 'get',
                    url: domain + 'getregionhierarchy?compid=' + $scope.compId + '&sessionid=' + $scope.sessionId,
                    encodeuri: true,
                    headers: { 'content-type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data && response.data.length > 0) {
                        $scope.branchCodes = response.data;
                        $scope.totalItems = $scope.branchCodes.length;
                        $scope.branchCodes.forEach(function (item, index) {
                            if (!$scope.uniqueRegions.includes(item.REGION_CODE)) {
                                $scope.uniqueRegions.push(item.REGION_CODE);
                            }
                        });
                    }
                });
            };

            $scope.getBranches();

            $scope.closeBranchView = function () {
                $scope.addNewBranchView = false;
            };

            $scope.SaveBranch = function () {
                if (!$scope.branch.REGION_CODE) {
                    growlService.growl("Please enter region details.", 'inverse');
                    return;
                }

                if (!$scope.branch.BRANCH_NAME) {
                    growlService.growl("Please enter branch name.", 'inverse');
                    return;
                }

                if (!$scope.branch.BRANCH_CODE) {
                    growlService.growl("Please enter branch code.", 'inverse');
                    return;
                }

                if (!$scope.branch.selectedLocation) {
                    growlService.growl("Please select location code.", 'inverse');
                    return;
                }

                let branchObj = {
                    RH_ID: $scope.branch.RH_ID,
                    REGION_CODE: $scope.branch.REGION_CODE,
                    BRANCH_NAME: $scope.branch.BRANCH_NAME,
                    BRANCH_CODE: $scope.branch.BRANCH_CODE,
                    LOCATION_ID: $scope.branch.selectedLocation ? $scope.branch.selectedLocation : 0,
                    COMP_ID: $scope.compId,
                    CREATED_BY: $scope.userId,
                    sessionID: $scope.sessionId
                };

                var params = {
                    "details": branchObj
                };

                auctionsService.SaveRegionHierarchy(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Branch details saved successfully.", "success");
                            $scope.getBranches();
                        }

                        $scope.branch = {
                            RH_ID: 0,
                            userID: $scope.userId,
                            sessionID: $scope.sessionId,
                            compID: $scope.compId,
                            BRANCH_CODE: '',
                            BRANCH_NAME: '',
                            REGION_CODE: '',
                            LOCATION_CODE: ''
                        };
                    });
            };

            $scope.getLocations = function () {
                $scope.locationCodes = [];
                $http({
                    method: 'get',
                    url: domain + 'getlocations?compid=' + $scope.compId + '&sessionid=' + $scope.sessionId,
                    encodeuri: true,
                    headers: { 'content-type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data && response.data.length > 0) {
                        $scope.locationCodes = response.data;
                    }
                });
            };

            $scope.getLocations();

        }]);

