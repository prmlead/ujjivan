﻿prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('evaluationCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService", "$http", "$window", "domain", "fileReader", "growlService", "$log", "$filter", "ngDialog", "techevalService", "fwdauctionsService",
        function ($state, $stateParams, $scope, auctionsService, userService, $http, $window, domain, fileReader, growlService, $log, $filter, ngDialog, techevalService, fwdauctionsService) {

            $scope.evalution = 0;

            $scope.ShowView = function () {
                //If DIV is visible it will be hidden and vice versa.
                $scope.evalution = $scope.evalution ? 0 : 1;
            }
        }]);