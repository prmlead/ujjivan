@echo off
DEL ..\dist\vendor.CACHE.js
DEL ..\dist\prm360.min.CACHE.css
set "list=resources\js\jquery.min.js resources\js\angular.min.js resources\js\angular-messages.js resources\js\angular-resource.min.js resources\js\angular-sanitize.js resources\js\angular-ui-router.min.js resources\js\jquery.signalR-2.2.0.js  resources\js\angular-signalr-hub.js resources\js\angular-animate.min.js resources\js\angularjs-datetime-picker.js resources\js\angular-storage.min.js resources\js\angular-timer.min.js resources\js\angular-input-stars.js resources\js\loading-bar.js resources\js\bootstrap.min.js resources\js\custom.js resources\js\isteven-multi-select.js resources\js\jquery.slimscroll.js resources\js\moment.min.js resources\js\moment-tz.min.js resources\js\angular-moment-picker.min.js resources\js\moment-timezone-with-data-1970-2030.js resources\js\ngDialog.min.js resources\js\ng-table.min.js resources\js\sidebar-nav.min.js resources\js\switchery.min.js resources\js\ui-bootstrap-tpls.min.js resources\js\waves.js resources\js\xlsx.core.min.js resources\js\alasql.min.js resources\js\nouislider.min.js resources\js\jquery.nouislider.min.js resources\js\ocLazyLoad.min.js resources\js\highcharts.js resources\js\highcharts-more.js resources\js\solid-gauge.js resources\js\funnel.js resources\js\exporting.js resources\js\sweet-alert.min.js resources\js\bootstrap-growl.min.js resources\js\tablesaw.js resources\js\dashboard3.js resources\js\bootstrap-datetimepicker.min.js resources\js\double-scroll-bars.min.js resources\js\angular-ui-tree.min.js  resources\js\angular-tree-widget.min.js resources\js\angular-recursion.min.js resources\js\angular-animate.min.js resources\js\angular-recursion.js resources\js\jstree.min.js resources\js\tableToExcel.js resources\js\angular-filter.min.js resources\js\cdnjs-cloudflare-humanize-duration.min.js resources\js\cdnjs-cloudflare-jquery-ui.min.js resources\js\cdnjs-cloudflare-lodash.js resources\js\cdnjs-cloudflare-ng-file-upload-all.min.js resources\js\cdnjs-cloudflare-smart-table.min.js resources\js\cdnjs-cloudflare-sortable.min.js resources\js\cdnjs-cloudflare-jspdf.min.js resources\js\angularjs-dropdown-multiselect.min.js resources\js\ng-pattern-restrict.min.js resources\js\toastr.min.js resources\js\html2pdf.bundle.min.js resources\js\angular-recaptcha.min.js resources\js\angular-idle.min.js"
(
 for %%i in (%list%) do (
	echo %%i
	type %%i >> ..\dist\vendor.CACHE.js
	echo. >> ..\dist\vendor.CACHE.js
  )
)


set "list=resources\css\prmCustom.css resources\css\bootstrap.min.css resources\css\sidebar-nav.min.css resources\css\materialdesignicons.min.css resources\css\animate.css resources\css\fontawesome-all.css resources\css\style.css resources\css\angular-moment-picker.min.css resources\css\lodash.min.css resources\css\steps.css resources\css\sweet-alert.css resources\css\isteven-multi-select.css resources\css\ngDialog.min.css resources\css\tablesaw.css resources\css\default.css resources\css\bootstrap-datetimepicker.min.css resources\css\prmCustomAdhok.css resources\css\angular-ui-tree.min.css resources\css\angular-ui-tree.css resources\css\app.css resources\css\tabs.min.css resources\css\tabs.min.css resources\css\style.min.css resources\css\Apmcstyle.css resources\css\cdnjs-cloudflare-ng-sortable.min.css"
(
 for %%i in (%list%) do (
	echo %%i
	type %%i >> ..\dist\prm360.min.CACHE.css
	echo. >> ..\dist\prm360.min.CACHE.css
  )
)

REM resources\css\app.min.1.css resources\css\app.min.2.css 