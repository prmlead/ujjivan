prmApp.factory("fileReader",
    ["$q", "$log", "$http", "domain", function ($q, $log, $http, domain) {
 
        var onLoad = function(reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };
 
        var onError = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };
 
        var onProgress = function(reader, scope) {
            return function (event) {
                scope.$broadcast("fileProgress",
                    {
                        total: event.total,
                        loaded: event.loaded
                    });
            };
        };
 
        var getReader = function(deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);
            reader.onprogress = onProgress(reader, scope);
            return reader;
        };
 
        var readAsDataURL = function (file, scope) {
            var deferred = $q.defer();
             
            var reader = getReader(deferred, scope);

            //reader.onload = function () {
            //    var fileName = file.name;

            //    var base64Content = arrayBufferToBase64(reader.result);

            //    $http.post(domain + 'scanFile', {
            //        fileName: fileName,
            //        content: base64Content
            //    }, {
            //        headers: { 'Content-Type': 'application/json' }
            //    }).then(function (response) {
            //        //if (response.data == "VirusFound") {
            //        //    deferred.reject({ error: "File contains a virus." });
            //        //} else {
            //        //    deferred.resolve(reader.result);
            //        //}

            //        deferred.resolve(reader.result);

            //    }).catch(function (error) {
            //        deferred.reject({ error: "Error scanning file" });
            //    });
            //};


            //reader.onerror = function () {
            //    deferred.reject({ error: "Error reading file." });
            //};

            reader.readAsArrayBuffer(file);
             
            return deferred.promise;
        };
 
        return {
            readAsDataUrl: readAsDataURL  
        };
    }]);

// Function to convert ArrayBuffer to Base64
function arrayBufferToBase64(arrayBuffer) {
    var binary = '';
    var bytes = new Uint8Array(arrayBuffer);
    var len = bytes.byteLength;

    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }

    return btoa(binary);
}


    // =========================================================================
    // Auction Details Services
    // =========================================================================
/*

(function (module) {
     
    var fileReader = function ($q, $log) {
 
        var onLoad = function(reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };
 
        var onError = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };
 
        var onProgress = function(reader, scope) {
            return function (event) {
                scope.$broadcast("fileProgress",
                    {
                        total: event.total,
                        loaded: event.loaded
                    });
            };
        };
 
        var getReader = function(deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);
            reader.onprogress = onProgress(reader, scope);
            return reader;
        };
 
        var readAsDataURL = function (file, scope) {
            var deferred = $q.defer();
             
            var reader = getReader(deferred, scope);         
            reader.readAsArrayBuffer(file);
             
            return deferred.promise;
        };
 
        return {
            readAsDataUrl: readAsDataURL  
        };
    };
 
    module.factory("fileReader",
                   ["$q", "$log", fileReader]);
 
}(angular.module("prmApp")));*/