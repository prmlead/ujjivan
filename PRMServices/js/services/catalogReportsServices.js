prmApp

    .service('catalogReportsServices', ["catalogReportsServicesDomain", "userService", "httpServices", function (catalogReportsServicesDomain, userService, httpServices) {

        var catalogReportsServices = this;
        
        catalogReportsServices.GetUserDepartments = function (params) {
            let url = catalogReportsServicesDomain + 'getuserdepartments?u_id=' + params.u_id + '&is_super_user=' + params.is_super_user + '&sessionid=' + userService.getUserToken();
             return httpServices.get(url);
        };

        catalogReportsServices.GetDepartmentCategories = function (params) {
            let url = catalogReportsServicesDomain + 'getdepartmentcategories';
            return httpServices.post(url, params);
        };

        catalogReportsServices.GetCategorySubcategories = function (params) {
            let url = catalogReportsServicesDomain + 'getcategorysubcategories';
            return httpServices.post(url, params);
        };

        catalogReportsServices.GetCategoryProducts = function (params) {
            let url = catalogReportsServicesDomain + 'getcategoryproducts';
            return httpServices.post(url, params);
        };

        catalogReportsServices.GetProductAnalysis = function (params) {
            let url = catalogReportsServicesDomain + 'getproductanalysis?u_id=' + params.u_id + '&prod_id=' + params.prod_id + '&call_code=' + params.call_code + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogReportsServices.GetVendorsAndOrders = function (params) {
            let url = catalogReportsServicesDomain + 'getvendorsandorders?u_id=' + params.u_id + '&prod_id=' + params.prod_id + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogReportsServices.GetMostOrdersAndVendors = function (params) {
            let url = catalogReportsServicesDomain + 'getmostordersandvendors?u_id=' + params.u_id + '&prod_id=' + params.prod_id + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogReportsServices.GetProductRequirements = function (params) {
            let url = catalogReportsServicesDomain + 'getproductrequirements?u_id=' + params.u_id + '&prod_id=' + params.prod_id + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogReportsServices.GetProductPriceHistory = function (params) {
            let url = catalogReportsServicesDomain + 'getproductpricehistory?u_id=' + params.u_id + '&prod_id=' + params.prod_id + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        return catalogReportsServices;
}]);