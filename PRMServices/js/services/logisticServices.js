prmApp
    .service('logisticServices', ["logisticDomain", "userService", "httpServices",
        function (logisticDomain, userService, httpServices) {
            var logisticServices = this;

            logisticServices.GetRequirementData = function (params) {
                let url = logisticDomain + 'getrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
                return httpServices.get(url);
            };

            logisticServices.getactiveleads = function (params) {
                let url = logisticDomain + 'getactiveleads?userid=' + params.userid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            logisticServices.getmyAuctions = function (params) {
                let url = logisticDomain + 'getmyauctions?userid=' + params.userid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            logisticServices.GetBidHistory = function (params) {
                let url = logisticDomain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
                return httpServices.get(url);
            };

            logisticServices.GetVendorReminders = function (params) {
                let url = logisticDomain + 'getvendorreminders?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            logisticServices.getPreviousItemPrice = function (itemDetails) {
                let url = logisticDomain + 'itempreviousprice?companyid=' + itemDetails.compID + '&productIDorName=' + itemDetails.productIDorName.trim() + '&netWeight=' + itemDetails.netWeight + '&natureOfGoods=' + itemDetails.natureOfGoods + '&finalDestination=' + itemDetails.finalDestination + '&sessionid=' + itemDetails.sessionID;
                return httpServices.get(url);
            };

            //logisticServices.getPreviousItemPriceReport = function (itemDetails) {
            //    let url = logisticDomain + 'itempreviouspricereport?companyid=' + itemDetails.compID + '&productId=' + itemDetails.productId.trim() + '&netWeight=' + itemDetails.netWeight + '&natureOfGoods=' + itemDetails.natureOfGoods + '&finalDestination=' + itemDetails.finalDestination + '&sessionid=' + itemDetails.sessionID;
            //    return httpServices.get(url);
            //};




            logisticServices.signalRFrontEnd = function (params) {
                let url = logisticDomain + 'signalrfrontend';
                return httpServices.post(url, params);
            };
            
            logisticServices.RequirementSave = function (params) {
                var url = logisticDomain + 'requirementsave';
                return httpServices.post(url, params);
            };

            logisticServices.SubmitQuotation = function (params) {
                var url = logisticDomain + 'submitquotation';
                return httpServices.post(url, params);
            };

            logisticServices.SaveAirlines = function (params) {
                var url = logisticDomain + 'saveairlines';
                return httpServices.post(url, params);
            };

            logisticServices.RejectQuotation = function (params) {
                var url = logisticDomain + 'rejectquotation';
                return httpServices.post(url, params);
            };

            logisticServices.updateauctionstart = function (params) {
                let url = logisticDomain + 'updateauctionstart';
                return httpServices.post(url, params);
            };

            logisticServices.updateauctionstart = function (params) {
                let url = logisticDomain + 'updateauctionstart';
                return httpServices.post(url, params);
            };

            logisticServices.StartNegotiation = function (params) {
                let url = logisticDomain + 'startnegotiation';
                return httpServices.post(url, params);
            };

            logisticServices.UpdateBidTime = function (params) {
                let url = logisticDomain + 'updatebidtime';
                return httpServices.post(url, params);
            };

            logisticServices.StopBids = function (params) {
                let url = logisticDomain + 'stopbids';
                return httpServices.post(url, params);
            };

            logisticServices.RestartNegotiation = function (params) {
                let url = logisticDomain + 'restartnegotiation';
                return httpServices.post(url, params);
            };

            logisticServices.MakeBid = function (params) {
                let url = logisticDomain + 'makebid';
                return httpServices.post(url, params);
            };

            logisticServices.savepricecap = function (params) {
                let url = logisticDomain + 'savepricecap';
                return httpServices.post(url, params);
            };

            logisticServices.UpdatePriceCap = function (params) {
                let url = logisticDomain + 'updatepricecap';
                return httpServices.post(url, params);
            };

            logisticServices.vendorreminders = function (params) {
                var url = logisticDomain + 'vendorreminders';
                return httpServices.post(url, params);
            };
            
            return logisticServices;

        }]);