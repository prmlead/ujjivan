prmApp

    .service('fileUpload', ['$http', function ($http) {
        this.uploadFileToUrl = function (file, uploadUrl) {
            var fd = new FormData();
            fd.append('file', file);

            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })

                .success(function () {
                })

                .error(function () {
                });
        }
    }])

    // =========================================================================
    // Auction Details Services
    // =========================================================================
    .service('auctionsService', ["$http", "store", "$state", "$rootScope", "domain", "version", "httpServices",
        function ($http, store, $state, $rootScope, domain, version, httpServices) {
            //var domain = 'http://182.18.169.32/services/';
            var auctions = this;
            auctions.getdate = function () {
                var date = new Date();
                var time = date.getTime();
                return $http({
                    method: 'GET',
                    url: domain + 'getdate?time=' + time,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                    //data: params
                }).then(function (response) {
                    var list = [];
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            list = response.data;
                        }
                        return list;
                    } else {
                        //console.log(response.data[0].errorMessage);
                    }
                }, function (result) {
                    //console.log("date error");
                });
            }

            auctions.GetDateLogistics = function () {
                var date = new Date();
                var time = date.getTime();
                return $http({
                    method: 'GET',
                    url: domain + 'getdatelogistics?time=' + time,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                    //data: params
                }).then(function (response) {
                    var list = [];
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            list = response.data;
                        }
                        return list;
                    } else {
                        //console.log(response.data[0].errorMessage);
                    }
                }, function (result) {
                    //console.log("date error");
                });
            }

            auctions.GetServerDateTime = function () {
                let url = domain + 'getserverdatetime';
                return httpServices.get(url);
            };


            auctions.CheckSystemDateTime = function () {
                auctions.GetServerDateTime()
                    .then(function (response) {

                        var d = new Date();

                        var year = new Date().getUTCFullYear();
                        var month = new Date().getUTCMonth();
                        month = month + 1;
                        var day = new Date().getUTCDate();
                        var hour = new Date().getUTCHours();
                        var min = new Date().getUTCMinutes();
                        var sec = new Date().getUTCSeconds();


                        var diffSec = response.SEC - sec;
                        if (diffSec >= 0) {

                        }
                        else if (diffSec < 0) {
                            diffSec = -(diffSec);
                        } else {
                            diffSec = 0;
                        }

                        console.log('*******************Local*******************');
                        console.log(' LY: ' + year + ' SY: ' + response.YEAR +
                            ' LM: ' + month + ' SM: ' + response.MONTH +
                            ' LD: ' + day + ' SD: ' + response.DAY +
                            ' LH: ' + hour + ' SH: ' + response.HOUR +
                            ' LM: ' + min + ' SM: ' + response.MIN +
                            ' LS: ' + sec + ' SS: ' + response.SEC);
                        console.log('*******************Server*******************');

                        if (year != response.YEAR ||
                            month != response.MONTH ||
                            day != response.DAY ||
                            hour != response.HOUR ||
                            min != response.MIN) {
                            swal({
                                title: "Please update system time correctly",
                                text: "thanks.",
                                type: "warning",
                                showCancelButton: false,
                                confirmButtonColor: "#F44336",
                                confirmButtonText: "OK",
                                closeOnConfirm: true
                            }, function () {
                                location.reload();
                            })
                        }

                    })
            };

            auctions.getPreviousItemPrice = function (itemDetails) {
                let url = domain + 'itempreviousprice?companyid=' + itemDetails.compID + '&productname=' + itemDetails.productIDorName.trim() + '&productno=' + itemDetails.productNo.trim() + '&brand=' + itemDetails.productBrand.trim() + '&sessionid=' + itemDetails.sessionID;
                return httpServices.get(url);
            };

            auctions.getIncotermProductConfig = function (incoTerm, sessionid) {
                let url = domain + 'getIncotermProductConfig?incoTerm=' + incoTerm.trim() + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.getVendorSAPDetails = function (params) {
                let url = domain + 'getvendorsapdetails?vendorid=' + params.vendorid + '&reqid=' + params.reqid + '&sessionid=' + params.sessionid;
                return httpServices.get(url);
            };

            auctions.isnegotiationended = function (reqid, sessionid) {
                let url = domain + 'isnegotiationended?reqid=' + reqid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.selectVendor = function (params) {
                var url = domain + 'selectvendor';
                return httpServices.post(url, params);
            };

            auctions.getauctions = function (params) {
                let url = domain + 'getrunningauctions?section=' + params.section + '&userid=' + params.userid + '&sessionid=' + params.sessionid + '&limit=' + params.limit;
                return httpServices.get(url);
            };

            auctions.rateVendor = function (params) {
                var url = domain + 'userratings';
                return httpServices.post(url, params);
            };

            auctions.vendorstatistics = function (uid, sessionid) {
                let url = domain + 'getvendorstatistics?userid=' + uid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.getrevisedquotations = function (params) {
                var url = domain + 'getrevisedquotations';
                return httpServices.post(url, params);
            };

            auctions.updateStatus = function (params) {
                var url = domain + 'updatestatus';
                return httpServices.post(url, params);
            };

            auctions.uploadquotationsfromexcel = function (params) {
                var url = domain + 'uploadquotationsfromexcel';
                return httpServices.post(url, params);
            };

            auctions.itemwiseselectvendor = function (params) {
                var url = domain + 'itemwiseselectvendor';
                return httpServices.post(url, params);
            };


            auctions.updatedeliverdate = function (params) {
                var url = domain + 'updateexpdelandpaydate';
                return httpServices.post(url, params);
            };

            auctions.reqTechSupport = function (params) {
                var url = domain + 'reqtechsupport';
                return httpServices.post(url, params);
            };

            auctions.getrequirementsreport = function (params) {
                var url = domain + 'getrequirementsreport?userid=' + params.userid + '&sessionid=' + params.sessionid;
                return httpServices.get(url);
            }

            auctions.SaveRunningItemPrice = function (params) {
                var url = domain + 'saverunningitemprice';
                return httpServices.post(url, params);
            };

            auctions.updatepaymentdate = function (params) {
                var url = domain + 'updateexpdelandpaydate';
                return httpServices.post(url, params);
            };


            auctions.StartNegotiation = function (params) {
                var url = domain + 'startNegotiation';
                return httpServices.post(url, params);
            };

            auctions.checkForRestartNegotiation = function (params) {
                var url = domain + 'checkForRestartNegotiation';
                return httpServices.post(url, params);
            };

            auctions.RestartNegotiation = function (params) {
                var url = domain + 'restartnegotiation';
                return httpServices.post(url, params);
            };

            auctions.DeleteVendorFromAuction = function (params) {
                var url = domain + 'deletevendorfromauction';
                return httpServices.post(url, params);
            };

            auctions.generatepo = function (params) {
                var data = {
                    reqPO: params
                };
                var url = domain + 'generatepoforuser';
                return httpServices.post(url, data);
            };

            auctions.vendorreminders = function (params) {
                var url = domain + 'vendorreminders';
                return httpServices.post(url, params);
            };


            auctions.GetLastPrice = function (itemDetails) {
                let url = domain + 'getlastprice?productIDorName=' + itemDetails.productIDorName.trim() + '&companyid=' + itemDetails.compID + '&sessionid=' + itemDetails.sessionID;
                return httpServices.get(url);
            };


            auctions.GetUserDepartmentDesignations = function (userid, sessionid, includetemp) {
                if (!includetemp || includetemp === '0') {
                    includetemp = 0;
                } else {
                    includetemp = 1;
                }

                var url = domain + 'getuserdepartmentdesignations?userid=' + userid + '&includetemp=' + includetemp + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };


            auctions.SaveCompanyDepartment = function (params) {
                var url = domain + 'savecompanydepartment';
                return httpServices.post(url, params);
            };

            auctions.SaveCompanyDesignations = function (params) {
                var url = domain + 'savecompanydesignations';
                return httpServices.post(url, params);
            };

            auctions.SaveCompanyRegion = function (params) {
                var url = domain + 'saveCompanyRegion';
                return httpServices.post(url, params);
            };

            auctions.MapCompanyRegionLocation = function (params) {
                var url = domain + 'mapCompanyRegionLocation';
                return httpServices.post(url, params);
            };
            
            auctions.GetCompanyRegion = function (compid, userid, sessionid) {
                let url = domain + 'getcompanyregion?compid=' + compid + '&userid=' + userid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };
            auctions.DeleteRegion = function (params) {
                var url = domain + 'deleteregion';
                return httpServices.post(url, params);
            };
            auctions.saveAttachment = function (params) {
                var url = domain + 'savefile';
                return httpServices.post(url, params);
            };

            auctions.GetCompanyDeptDesigTypes = function (userID, type, sessionid) {
                return $http({
                    method: 'GET',
                    url: domain + 'getcompanydeptdesigtypes?userid=' + userID + '&type=' + type + '&sessionid=' + sessionid,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    var list = {};
                    if (response && response.data) {
                        return response.data;
                    }
                }, function (result) {
                    //console.log("date error");
                });
            };



            auctions.DeleteDepartment = function (params) {
                var url = domain + 'deletedepartment';
                return httpServices.post(url, params);
            };

            auctions.DeleteDesignation = function (params) {
                var url = domain + 'deletedesignation';
                return httpServices.post(url, params);
            };

            auctions.GetCompanyDepartments = function (userid, sessionid) {
                var url = domain + 'getcompanydepartments?userid=' + userid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.GetCurrencyFactors = function (userid, sessionid, compid) {
                var url = domain + 'getcurrencyfactors?userid=' + userid + '&sessionid=' + sessionid + '&compid=' + compid;
                return httpServices.get(url);
            };

            auctions.GetCurrencyFactorAudit = function (currencycode, sessionid, compid) {
                var url = domain + 'getcurrencyfactoraudit?currencycode=' + currencycode + '&sessionid=' + sessionid + '&compid=' + compid;
                return httpServices.get(url);
            };

            auctions.SaveCurrencyFactor = function (params) {
                var url = domain + 'savecurrencyfactor';
                return httpServices.post(url, params);
            };

            auctions.SaveLocation = function (params) {
                var url = domain + 'savelocation';
                return httpServices.post(url, params);
            };

            auctions.SaveRegionHierarchy = function (params) {
                var url = domain + 'saveregionhierarchy';
                return httpServices.post(url, params);
            };

            auctions.GetCompanyDesignations = function (userid, sessionid) {
                var url = domain + 'getcompanydesignations?userid=' + userid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.GetUserDepartments = function (userid, sessionid) {
                var url = domain + 'getuserdepartments?userid=' + userid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.GetUserDesignations = function (userid, sessionid) {
                var url = domain + 'getuserdesignations?userid=' + userid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };


            auctions.GetUserDeptDesig = function (userid, sessionid) {
                var url = domain + 'getuserdeptdesig?userid=' + userid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.GetCompDeptDesig = function (compid, sessionid) {
                var url = domain + 'getcompdeptdesig?compid=' + compid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.GetAssignetTickets = function (sessionid) {
                let url = domain + 'GetAssignetTickets?sessionID=' + sessionid;
                return httpServices.get(url);
            };

            auctions.SaveAssignTickets = function (params) {
                let url = domain + 'SaveAssignTickets';
                return httpServices.post(url, params);

            };

            //auctions.SaveUserDeptDesig = function (params) {
            //    var url = domain + 'saveuserdeptdesig';
            //    return httpServices.post(url, params);
            //};


            auctions.SaveUserDepartmentDesignation = function (params) {
                var data = params;
                return $http({
                    method: 'POST',
                    url: domain + 'saveuserdepartmentdesignation',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: data
                }).then(function (response) {
                    var list = {};
                    if (response && response.data) {
                        list = response.data;
                        return list;
                    } else {

                    }
                }, function (result) {
                    //console.log(result);
                });
            }

            auctions.GetReqDeptDesig = function (userid, reqid, sessionid) {
                var url = domain + 'getreqdeptdesig?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.SaveReqDeptDesig = function (params) {
                var url = domain + 'savereqdeptdesig';
                return httpServices.post(url, params);
            };

            auctions.SaveUserDepartments = function (params) {
                var url = domain + 'saveuserdepartments';
                return httpServices.post(url, params);
            };

            auctions.GetDepartmentUsers = function (deptid, userid, sessionid) {
                var url = domain + 'getdepartmentusers?deptid=' + deptid + '&userid=' + userid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.GetReqDepartments = function (userid, reqid, sessionid) {
                let url = domain + 'getreqdepartments?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.SaveReqDepartments = function (params) {
                let url = domain + 'savereqdepartments';
                return httpServices.post(url, params);
            };

            auctions.shareemaillinkstoVendors = function (params) {
                let url = domain + 'shareemaillinkstoVendors';
                return httpServices.post(url, params);
            };

            auctions.GetSharedLinkVendors = function (params) {
                let url = domain + 'getSharedLinkVendors?userid=' + params.userID + '&vendorids=' + params.vendorIDs + '&sessionid=' + params.sessionID;
                return httpServices.get(url);
            };

            auctions.GetUnBlockedVendors = function (params) {
                let url = domain + 'getUnBlockedVendors?userid=' + params.userID + '&vendorids=' + params.vendorIDs + '&sessionid=' + params.sessionID;
                return httpServices.get(url);
            };

            auctions.saveVendorOtherCharges = function (params) {
                let url = domain + 'saveVendorOtherCharges';
                return httpServices.post(url, params);
            };

            auctions.saveQCSVendorAssignments = function (params) {
                let url = domain + 'saveQCSVendorAssignments';
                return httpServices.post(url, params);
            };

            auctions.getQCSVendorItemAssignments = function (params) {
                let url = domain + 'getqcsvendoritemassignments?reqid=' + params.reqid + '&userid=' + params.userid + '&qcsid=' + params.qcsid + '&sessionid=' + params.sessionid;
                return httpServices.get(url, params);
            };

            auctions.AssignVendorToCompany = function (params) {
                let url = domain + 'assignvendortocompany';
                return httpServices.post(url, params);
            };

            auctions.saveVendorOtherCharges = function (params) {
                let url = domain + 'saveVendorOtherCharges';
                return httpServices.post(url, params);
            };

            auctions.GetCompanyConfiguration = function (compid, configkey, sessionid) {
                let url = domain + 'getcompanyconfiguration?compid=' + compid + '&configkey=' + configkey + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.GetfeildMappings = function (compid, configkey, sessionid) {
                let url = domain + 'GetfeildMappings?compid=' + compid + '&configkey=' + configkey + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.getRequirementVendorCodes = function (reqid, sessionid) {
                let url = domain + 'getrequirementvendorcodes?reqid=' + reqid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.SearchRequirements = function (params) {
                params.excludeprlinked = params.excludeprlinked ? true : false;
                let url = domain + 'searchrequirements?search=' + params.search + '&excludeprlinked=' + params.excludeprlinked + '&compid=' + params.compid + '&sessionid=' + params.sessionid;
                return httpServices.get(url);
            };

            auctions.GetCompanyConfigurationForLogin = function (compid, configkey, sessionid) {
                let url = domain + 'getcompanyconfigurationforlogin?compid=' + compid + '&configkey=' + configkey + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.GetCompanyCurrencies = function (compid, sessionid) {
                let url = domain + 'companycurrencies?compid=' + compid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.GetPreferredLogin = function () {
                let url = domain + 'GetPreferredLogin';
                return httpServices.get(url);
            };

            auctions.GetIsAuthorized = function (userid, reqid, sessionid) {
                let url = domain + 'getisauthorized?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.materialdispatch = function (params) {
                var data = {
                    reqMat: params
                };
                let url = domain + 'materialdispatch';
                return httpServices.post(url, data);
            };

            auctions.materialReceived = function (params) {
                var data = {
                    materialreceived: params
                };
                let url = domain + 'materialreceived';
                return httpServices.post(url, data);
            };

            auctions.paymentdetails = function (params) {
                var data = {
                    paymentDets: params
                };
                let url = domain + 'paymentdetails';
                return httpServices.post(url, data);
            };

            auctions.generatePOinServer = function (params) {
                let url = domain + 'generatepo';
                return httpServices.post(url, params);
            };
            auctions.getDashboardStatsNew = function (params) {
                let url = domain + 'getdashboardstats1?userid=' + params.userid + '&sessionid=' + params.sessionid + '&fromdate=' + params.fromDate + '&todate=' + params.toDate/* + '&depts='*/ /*+ params.filterDepts*/;
                return httpServices.get(url);
            };

            auctions.getDashboardStats = function (params) {
                let url = domain + 'getdashboardstats?userid=' + params.userid + '&sessionid=' + params.sessionid + '&fromdate=' + params.fromDate + '&todate=' + params.toDate/* + '&depts='*/ /*+ params.filterDepts*/;
                return httpServices.get(url);
            };

            auctions.getCustomerDashboard = function (params) {
                let url = domain + 'getcustomerdashboard?userid=' + params.userid + '&sessionid=' + params.sessionid + '&fromdate=' + params.fromDate + '&todate=' + params.toDate/* + '&depts='*/ /*+ params.filterDepts*/;
                return httpServices.get(url);
            };

            auctions.getDashboardIndicators = function (params) {
                let url = domain + 'getDashboardIndicators?userId=' + params.userId + '&compId=' + params.compId + '&deptId=' + params.deptId + '&sessionId=' + params.sessionId + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate;
                return httpServices.get(url);
            };

            auctions.getCategories = function (params) {
                let url = domain + 'getcategories?userid=' + (params ? params : -1);
                return httpServices.get(url);
            };

            auctions.getKeyValuePairs = function (params, compid) {
                if (!compid) {
                    compid = 0;
                }

                let url = domain + 'getkeyvaluepairs?parameter=' + params + '&compid=' + compid;
                return httpServices.get(url);
            };

            auctions.getmyAuctions = function (params) {
                if (!params.isRFP) {
                    params.isRFP = false;
                }

                let url = domain + 'getmyauctions?userid=' + params.userid + '&page=' + params.page + '&limit=' + params.limit + '&reqstatus=' + params.reqstatus + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&onlyrfq=' + params.isRFP + '&sessionid=' + params.sessionid;
                return httpServices.get(url);
            };

            auctions.getactiveleads = function (params) {
                let url = domain + 'getactiveleads?userid=' + params.userid + '&page=' + params.page + '&limit=' + params.limit + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            auctions.getrequirementdata = function (params) {
                let url = domain + 'getrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
                return httpServices.get(url);
            };

            auctions.getReportrequirementdata = function (params) {
                let url = domain + 'getreportrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid + "&excludePriceCap=" + params.excludePriceCap;
                return httpServices.get(url);
            };

            auctions.getLPPByProductIds = function (params) {
                let url = domain + 'getlppbyproductids?productids=' + params.productids + "&ignorereqid=" + params.ignorereqid + "&compid=" + params.compid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            auctions.GetReportsRequirement = function (params) {
                let url = domain + 'getreportsrequirement?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            auctions.GetVendorReminders = function (params) {
                let url = domain + 'getvendorreminders?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            auctions.GetBidHistory = function (params) {
                let url = domain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
                return httpServices.get(url);
            };

            auctions.GetItemBidHistory = function (params) {
                let url = domain + 'GetItemBidHistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
                return httpServices.get(url);
            };

            auctions.getpodetails = function (params) {
                let url = domain + 'getpodetails?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
                return httpServices.get(url);
            };

            auctions.getcomments = function (params) {
                let url = domain + 'getcomments?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            auctions.getpricecomparison = function (params) {
                let url = domain + 'getpricecomparison?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            auctions.GetPriceComparisonPreNegotiation = function (params) {
                let url = domain + 'getpricecomparisonprenegotiation?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            auctions.savecomment = function (params) {
                var comment = {
                    "com": {
                        "requirementID": params.reqID,
                        "firstName": "",
                        "lastName": "",
                        "userID": params.userID,
                        "commentText": params.commentText,
                        "replyCommentID": -1,
                        "commentID": -1,
                        "errorMessage": "",
                        "sessionID": params.sessionID
                    }
                };

                let url = domain + 'savecomment';
                return httpServices.post(url, comment);
            };


            auctions.deleteAttachment = function (Attaachmentparams) {
                let url = domain + 'deleteattachment';
                return httpServices.post(url, Attaachmentparams);
            };


            auctions.postrequirementdata = function (params) {
                var myDate = new Date(); // Your timezone!
                var myEpoch = parseInt(myDate.getTime() / 1000);
                params.minBidAmount = 0;
                params.postedOn = "/Date(" + myEpoch + "000+0000)/";
                params.timeLeft = -1;
                params.price = -1;
                var requirement = {
                    "requirement": {
                        "title": params.title,
                        "reqType": params.isRFP ? 2 : 1,
                        "description": params.description,
                        "category": params.category,
                        "subcategories": params.subcategories,
                        "urgency": params.urgency,
                        "budget": params.budget,
                        "attachmentName": params.attachmentName,
                        "deliveryLocation": params.deliveryLocation,
                        "taxes": params.taxes,
                        "paymentTerms": params.paymentTerms,
                        "lotId": params.lotId,
                        "requirementID": params.requirementID,
                        "customerID": params.customerID,
                        "isClosed": params.isClosed,
                        "endTime": null,
                        "sessionID": params.sessionID,
                        "errorMessage": "",
                        "timeLeft": -1,
                        "price": -1,
                        "auctionVendors": params.auctionVendors,
                        "startTime": null,
                        "status": "",
                        "postedOn": "/Date(" + myEpoch + "000+0000)/",
                        "custLastName": params.customerLastname,
                        "custFirstName": params.customerFirstname,
                        "deliveryTime": params.deliveryTime,
                        "includeFreight": params.includeFreight,
                        "inclusiveTax": params.inclusiveTax,
                        "minBidAmount": 0,
                        "checkBoxEmail": params.checkBoxEmail,
                        "checkBoxSms": params.checkBoxSms,
                        "quotationFreezTime": params.quotationFreezTime,
                        "currency": params.currency,
                        "timeZoneID": params.timeZoneID,
                        "listRequirementItems": params.listRequirementItems,
                        "isTabular": params.isTabular,
                        "reqComments": params.reqComments,
                        "itemsAttachment": params.itemsAttachment,
                        "isSubmit": params.isSubmit,
                        "isQuotationPriceLimit": params.isQuotationPriceLimit,
                        "quotationPriceLimit": params.quotationPriceLimit,
                        "noOfQuotationReminders": params.noOfQuotationReminders,
                        "remindersTimeInterval": params.remindersTimeInterval,
                        "custCompID": params.custCompID,
                        "customerEmails": params.customerEmails,
                        "customerCompanyName": params.customerCompanyName,
                        "deleteQuotations": params.deleteQuotations,
                        "indentID": params.indentID,
                        "expStartTime": params.expStartTime,
                        "cloneID": params.cloneID,
                        "isDiscountQuotation": params.isDiscountQuotation,
                        "contactDetails": params.contactDetails,
                        "generalTC": params.generalTC,
                        "isRevUnitDiscountEnable": params.isRevUnitDiscountEnable,
                        "multipleAttachments": params.multipleAttachments,
                        "contractStartTime": params.contractStartTime,
                        "contractEndTime": params.contractEndTime,
                        "isContract": params.isContract,
                        "prCode": params.prCode,
                        "PR_ID": params.PR_ID,
                        "PR_ITEM_IDS": params.selectedPRItemIds,
                        "isTechScoreReq": params.isTechScoreReq,
                        "auditComments": params.auditComments,
                        "biddingType": params.biddingType,
                        "templateId": params.templateId,
                        "projectName": params.projectName,
                        "projectDetails": params.projectDetails,
                        "REQ_SPEC_JSON": params.REQ_SPEC_JSON
                    }, "attachment": params.attachment
                };

                let url = domain + 'requirementsave';
                return httpServices.post(url, requirement);
            };

            auctions.saveRequirementItemList = function (params) {
                let url = domain + 'saverequirementitemlist';
                return httpServices.post(url, params);
            };

            auctions.endNegotiation = function (params) {
                let url = domain + 'endnegotiation';
                return httpServices.post(url, params);
            };

            auctions.IsValidQuotationUpload = function (params) {
                let url = domain + 'isvalidquotationupload?reqid=' + params.reqId + '&sessionid=' + params.sessionid;
                return httpServices.get(url, params);
            };

            auctions.getCompanyGSTInfo = function (params) {
                let url = domain + 'getcompanygstinfo?compid=' + params.companyId + '&sessionid=' + params.sessionid;
                return httpServices.get(url, params);
            };

            auctions.saveCompanyGSTInfo = function (params) {
                let url = domain + 'savecompanygstinfo';
                return httpServices.post(url, params);
            };

            auctions.deleteGSTInfo = function (params) {
                let url = domain + 'deletecompanygstinfo';
                return httpServices.post(url, params);
            };

            auctions.updatebidtime = function (params) {
                let url = domain + 'updatebidtime';
                return httpServices.post(url, params);
            };

            auctions.makeabid = function (params) {
                let url = domain + 'makeabid';
                return httpServices.post(url, params);
            };

            auctions.uploadQuotation = function (params) {
                let url = domain + 'uploadquotation';
                return httpServices.post(url, params);
            };


            auctions.UploadQuotationForSelectVendorCeilingPrices = function (params) {
                let url = domain + 'uploadQuotationForSelectVendorCeilingPrices';
                return httpServices.post(url, params);
            };

            auctions.revquotationupload = function (params) {
                let url = domain + 'revquotationupload';
                return httpServices.post(url, params);
            };

            auctions.QuatationAprovel = function (params) {
                let url = domain + 'quatationaprovel';
                return httpServices.post(url, params);
            };

            auctions.ItemQuotationApproval = function (params) {
                let url = domain + 'itemquotationapproval';
                return httpServices.post(url, params);
            };

            auctions.ItemLevelQuotationApproval = function (params) {
                let url = domain + 'itemlevelquotationapproval';
                return httpServices.post(url, params);
            };

            auctions.SendQuotationApprovalStatusEmail = function (params) {
                let url = domain + 'sendquotationapprovalstatusemail';
                return httpServices.post(url, params);
            };

            auctions.getRequirementSettings = function (params) {
                let url = domain + 'requirementsettings?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
                return httpServices.get(url);
            };

            auctions.getRequirementVendorAnalysis = function (params) {
                let url = domain + 'requirementvendoranalysis?vendorids=' + params.vendorids + '&itemids=' + params.itemids + '&sessionid=' + params.sessionid;
                return httpServices.get(url);
            };

            auctions.validateCurrencyRate = function (params) {
                let url = domain + 'validatecurrencyrate?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
                return httpServices.get(url);
            };

            auctions.refreshRequirementCurrency = function (params) {
                let url = domain + 'refreshrequirementcurrency?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
                return httpServices.get(url);
            };

            auctions.saveRequirementSetting = function (params) {
                let url = domain + 'saverequirementsetting';
                return httpServices.post(url, params);
            };

            auctions.toggleCounterBid = function (params) {
                let url = domain + 'togglecounterbid';
                return httpServices.post(url, params);
            };

            auctions.SaveDifferentialFactor = function (params) {
                let url = domain + 'savedifferentialfactor';
                return httpServices.post(url, params);
            };

            auctions.savepricecap = function (params) {
                let url = domain + 'savepricecap';
                return httpServices.post(url, params);
            };

            auctions.GetCompanyLeads = function (params) {
                let url = domain + 'getcompanyleads?userid=' + params.userid + '&searchstring=' + params.searchstring + '&searchtype=' + params.searchtype + '&sessionid=' + params.sessionid;
                return httpServices.get(url);
            };

            auctions.updateauctionstart = function (params) {
                let url = domain + 'updateauctionstart';
                return httpServices.post(url, params);
            };

            auctions.SaveReqNegSettings = function (params) {
                let url = domain + 'SaveReqNegSettings';
                return httpServices.post(url, params);
            };

            auctions.GetMaterialReceivedData = function (params) {
                let url = domain + 'getmaterialreceiveddata?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
                return httpServices.get(url);
            };

            auctions.SaveCompanyCategories = function (params) {
                let url = domain + 'savecompanycategories';
                return httpServices.post(url, params);
            };

            auctions.GetCompanyCategories = function (compID, sessionID) {
                let url = domain + 'getcompanycategories?compid=' + compID + '&sessionid=' + sessionID;
                return httpServices.get(url);
            };

            auctions.newDetails = function (params) {
                let url = domain + 'savealternatecontacts';
                return httpServices.post(url, params);
            };

            auctions.getalternatecontacts = function (userid, compid, sessionID) {
                let url = domain + 'getalternatecontacts?userid=' + userid + '&compid=' + compid + '&sessionid=' + sessionID;
                return httpServices.get(url);
            };

            auctions.SaveRequirementTerms = function (params) {
                let url = domain + 'saverequirementterms';
                return httpServices.post(url, params);
            };

            auctions.GetRequirementTerms = function (userid, reqid, sessionID) {
                let url = domain + 'getrequirementterms?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionID;
                return httpServices.get(url);
            };

            auctions.DeleteRequirementTerms = function (params) {
                let url = domain + 'deleterequirementterms';
                return httpServices.post(url, params);
            };

            /*auctions.saveCIJ = function (params) {
                let url = domain + 'savecij';
                return httpServices.post(url, params);
            };
    
            auctions.GetCIJ = function (cijid, sessionid) {
                let url = domain + 'getcij?cijid=' + cijid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };
    
            auctions.GetIndentDetails = function (indentID, sessionid) {
                let url = domain + 'getindentdetails?indentid=' + indentID + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };
    
            auctions.SaveIndentDetails = function (params) {
                let url = domain + 'saveindentdetails';
                return httpServices.post(url, params);
            };
    
            auctions.GetCIJList = function (compid, sessionid) {
                let url = domain + 'getcijlist?compid=' + compid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };
    
            auctions.GetIndentList = function (compid, sessionid) {
                let url = domain + 'getindentlist?compid=' + compid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };
    
            auctions.saveFileUpload = function (params) {
                let url = domain + 'savefileupload';
                return httpServices.post(url, params);
            }; */

            auctions.GetUserDetails = function (userid, sessionid) {
                let url = domain + 'getuserinfo?userid=' + userid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.GetOtherInfo = function (params) {
                return $http({
                    method: 'GET',
                    url: domain + 'GetOtherInfo?companyId=' + params.companyId + '&value=' + params.value + '&sessionid=' + params.sessionid + '&type=' + params.type,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    var list = {};
                    if (response) {
                        return response;
                    } else {

                    }
                }, function (result) {
                });
            };

            auctions.UpdatePriceCap = function (params) {
                let url = domain + 'updatepricecap';
                return httpServices.post(url, params);
            };


            //auctions.ResetPasswordByOTP = function (phone) {
            //    return $http({
            //        method: 'GET',
            //        url: domain + 'resetpasswordbyotp?phone=' + phone,
            //        encodeURI: true,
            //        headers: { 'Content-Type': 'application/json' }
            //    }).then(function (response) {
            //        var list = {};
            //        if (response && response.data) {
            //            return response.data;
            //        }
            //    }, function (result) {
            //    });
            //};

            auctions.ResetPasswordByOTP = function (params) {
                return httpServices.post(domain + 'resetpasswordbyotp', params);
            };

            auctions.EnableMarginFields = function (params) {
                var url = domain + 'enablemarginfields';
                return httpServices.post(url, params);
            };

            auctions.GetAltUsers = function (clientid, vendorid, sessionid) {
                let url = domain + 'getaltusers?clientid=' + clientid + '&vendorid=' + vendorid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.uploadclientsidequotation = function (params) {
                var url = domain + 'uploadclientsidequotation';
                return httpServices.post(url, params);
            };

            auctions.uploadRequirementItemsSaveExcel = function (params) {
                var url = domain + 'uploadrequirementitemssaveexcel';
                return httpServices.post(url, params);
            };

            auctions.UploadRequirementItemsCeilingSave = function (params) {
                var url = domain + 'uploadrequirementitemsceilingsave';
                return httpServices.post(url, params);
            };

            auctions.getOTP = function (phone) {
                let url = domain + 'getotp?phone=' + phone;
                return httpServices.get(url);
            };

            auctions.UpdatePassword = function (params) {
                var url = domain + 'updatepassword';
                return httpServices.post(url, params);
            };


            auctions.GetItemsList = function (moduleid, module, sessionid) {
                let url = domain + 'getitemslist?moduleid=' + moduleid + '&module=' + module + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };


            auctions.GetCompanyDeptDesigTypes = function (userID, type, sessionid) {
                let url = domain + 'getcompanydeptdesigtypes?userid=' + userID + '&type=' + type + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.GetWorkCategories = function (userid, sessionid) {
                let url = domain + 'getworkcategories?userid=' + userid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.SaveWorkCategory = function (params) {
                var url = domain + 'saveworkcategory';
                return httpServices.post(url, params);
            };

            auctions.GetBudgetCodes = function (userid, sessionid) {

                let url = domain + 'getbudgetcodes?userid=' + userid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };


            auctions.SaveBudgetCode = function (params) {

                var url = domain + 'savebudgetcode';
                return httpServices.post(url, params);
            };

            auctions.GetUserDetailsByDeptDesigTypeID = function (compid, depttypeid, desigtypeid, sessionid) {
                let url = domain + 'getuserdetailsbydeptdesigtypeid?compid=' + compid + '&depttypeid=' + depttypeid + '&desigtypeid=' + desigtypeid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };





            auctions.SaveCBPrices = function (params) {
                var url = domain + 'savecbprices';
                return httpServices.post(url, params);
            };

            auctions.SaveCBPricesCustomer = function (params) {
                var url = domain + 'savecbpricescustomer';
                return httpServices.post(url, params);
            };

            auctions.FreezeCounterBid = function (params) {
                var url = domain + 'freezecounterbid';
                return httpServices.post(url, params);
            };

            auctions.GetCBPricesAudit = function (reqid, vendorid, sessionid) {
                let url = domain + 'getcbpricesaudit?reqid=' + reqid + '&vendorid=' + vendorid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };

            auctions.UpdateCBTime = function (params) {
                var url = domain + 'updatecbtime';
                return httpServices.post(url, params);
            };

            auctions.CBStopQuotations = function (params) {
                var url = domain + 'cbstopquotations';
                return httpServices.post(url, params);
            };

            auctions.CBMarkAsComplete = function (params) {
                var url = domain + 'cbmarkascomplete';
                return httpServices.post(url, params);
            };

            auctions.markAsCompleteREQ = function (params) {
                var url = domain + 'markAsCompleteREQ';
                return httpServices.post(url, params);
            };

            auctions.GetEmailLogs = function (fromDate, toDate, companyID, sessionId) {
                var url = domain + 'getemaillogs?from=' + fromDate + '&to=' + toDate + '&compid=' + companyID + '&sessionid=' + sessionId;
                return httpServices.get(url);
            };

            auctions.GetSMSLogs = function (fromDate, toDate, companyID, sessionId) {
                var url = domain + 'getsmslogs?from=' + fromDate + '&to=' + toDate + '&compid=' + companyID + '&sessionid=' + sessionId;
                return httpServices.get(url);
            };

            auctions.GetCompanyidByVendorid = function (ID) {
                var url = domain + 'getcompanyidbyvendorid?id=' + ID;
                return httpServices.get(url);
            };

            auctions.GetActiveUsers = function (companyID, sessionID) {
                var url = domain + 'getactiveusers?compid=' + companyID + '&sessionid=' + sessionID;
                return httpServices.get(url);
            };

            auctions.GetActiveBuyers = function (userID, sessionID) {
                var url = domain + 'getactivebuyers?userid=' + userID + '&sessionid=' + sessionID;
                return httpServices.get(url);
            };

            auctions.GetUsersLoginStatus = function (userID, fromDate, toDate, sessionID) {
                var url = domain + 'getusersloginstatus?userid=' + userID + '&from=' + fromDate + '&to=' + toDate + '&sessionid=' + sessionID;
                return httpServices.get(url);
            };

            auctions.getcatalogCategories = function (uid) {
                let url = domain + 'getCatalogCategories?uid=' + uid;
                return httpServices.get(url);
            };

            auctions.downloadActiveUserTemplate = function (template, userID, fromDate, toDate, sessionID) {
                let url = domain + 'getactiveuserstemplates?template=' + template + '&userid=' + userID + '&from=' + fromDate + '&to=' + toDate + '&sessionid=' + sessionID;
                let data = httpServices.get(url).then(function (response) {
                    if (response) {
                        response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                        var linkElement = document.createElement('a');
                        try {
                            //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                            var url = $window.URL.createObjectURL(response);

                            linkElement.setAttribute('href', url);
                            linkElement.setAttribute("download", template + ".xlsx");

                            var clickEvent = new MouseEvent("click", {
                                "view": window,
                                "bubbles": true,
                                "cancelable": false
                            });
                            linkElement.dispatchEvent(clickEvent);
                        }
                        catch (ex) { }
                        return response;
                    }
                }, function (result) {
                    //console.log("date error");
                });
            };

            function b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                var blob = new Blob(byteArrays, { type: contentType });
                return blob;
            }

            auctions.saveReductionLevelSetting = function (params) {
                let url = domain + 'saveReductionLevelSetting';
                return httpServices.post(url, params);
            };

            auctions.getReductionSettings = function (params) {
                let url = domain + 'getReductionSettings?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
                return httpServices.get(url);
            };

            return auctions;



        }])


    // =========================================================================
    // Header Messages and Notifications list Data
    // =========================================================================

    .service('messageService', ['$resource', function ($resource) {

    }])

    //==============================================
    // BOOTSTRAP GROWL
    //==============================================

    .service('growlService', function () {
        var gs = {};
        gs.growl = function (message, type, position) {

            var _position = {
                from: 'top',
                align: 'right'
            }

            if (position != undefined) {
                _position = position;
            }

            $.growl({
                message: message
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: _position,
                delay: 2500,
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                },
                offset: {
                    x: 20,
                    y: 85
                }
            });
        }

        return gs;
    })

    .service('authService', ["$http", "store", "$state", "$rootScope", "domain", "version", 'httpServices', function ($http, store, $state, $rootScope, domain, version, httpServices) {
        var authService = this;
        var responseObj = {};
        responseObj.objectID = 0;
        authService.isValidSession = function (sessionid) {
            let url = domain + 'issessionvalid?sessionid=' + sessionid;
            return httpServices.get(url);
        };



        return authService;
    }])

    .service("SettingService", ["$http", "store", "$state", "$rootScope", "settingDomian", "version", 'httpServices', function ($http, store, $state, $rootScope, settingDomian, version, httpServices) {


        var _settingService = this;
        _settingService.getRegistrationFields = function () {
            return httpServices.get(settingDomian + "settings/get_registration_setup")
        }


        _settingService.insertRegistrationField = function (data) {
            return httpServices.post(settingDomian + "settings/insert_reg_field", data)
        }

        _settingService.updateRegistrationField = function (data) {
            return httpServices.post(settingDomian + "settings/update_reg_field", data)
        }

        _settingService.deleteRegistrationField = function (id) {
            return httpServices.post(settingDomian + "settings/delete_reg_field/", { id: id })
        }


        _settingService.getRequirementFields = function () {
            return httpServices.get(settingDomian + "settings/get_requirement_setup")
        }


        _settingService.insertRequirementField = function (data) {
            return httpServices.post(settingDomian + "settings/insert_requirement_field", data)
        }

        _settingService.updateRequirementField = function (data) {
            return httpServices.post(settingDomian + "settings/update_requirement_field", data)
        }

        _settingService.deleteRequirementField = function (id) {
            return httpServices.post(settingDomian + "settings/delete_requirement_field/", { id: id })
        }

    }])
