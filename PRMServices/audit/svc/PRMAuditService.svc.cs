﻿using PRMServices.Models;
using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel.Activation;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMAuditService : IPRMAuditService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        private PRMServices prmServices = new PRMServices();

        public Dictionary<string, List<Audit>> GetRequirementAudit(int reqID, string sessionID)
        {
            Dictionary<string, List<Audit>> audits = new Dictionary<string, List<Audit>>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("audit_GetRequirementAuditData", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    List<Audit> requirementAudits = new List<Audit>();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Audit audit = new Audit();
                        audit.DateCreated = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MinValue;
                        audit.AuditBy = row["FULL_NAME"] != DBNull.Value ? Convert.ToString(row["FULL_NAME"]) : string.Empty;
                        audit.AuditComments = row["AUDIT_COMMENTS"] != DBNull.Value ? Convert.ToString(row["AUDIT_COMMENTS"]) : string.Empty;
                        audit.AuditVersion = row["AUDIT_VERSION"] != DBNull.Value ? Convert.ToString(row["AUDIT_VERSION"]) : string.Empty;

                        requirementAudits.Add(audit);
                    }

                    audits.Add("versions", requirementAudits);
                }

                if (ds != null && ds.Tables.Count > 1)
                {
                    List<Audit> requirementAudits = new List<Audit>();
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        Audit audit = new Audit();

                        audit.DateCreated = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MinValue;
                        audit.AuditBy = row["FULL_NAME"] != DBNull.Value ? Convert.ToString(row["FULL_NAME"]) : string.Empty;
                        audit.ActionType = row["ACTION_TYPE"] != DBNull.Value ? Convert.ToString(row["ACTION_TYPE"]) : string.Empty;
                        audit.MoreDetails = row["ProductName"] != DBNull.Value ? Convert.ToString(row["ProductName"]) : string.Empty;
                        audit.ColumnNameDesc = row["COLUMN_NAME_DESC"] != DBNull.Value ? Convert.ToString(row["COLUMN_NAME_DESC"]) : string.Empty;
                        audit.ColumnName = row["COLUMN_NAME"] != DBNull.Value ? Convert.ToString(row["COLUMN_NAME"]) : string.Empty;
                        audit.OldValue = row["OLD_VALUE"] != DBNull.Value ? Convert.ToString(row["OLD_VALUE"]) : string.Empty;
                        audit.NewValue = row["NEW_VALUE"] != DBNull.Value ? Convert.ToString(row["NEW_VALUE"]) : string.Empty;
                        audit.AuditVersion = row["AUDIT_VERSION"] != DBNull.Value ? Convert.ToString(row["AUDIT_VERSION"]) : string.Empty;

                        requirementAudits.Add(audit);
                    }

                    audits.Add("details", requirementAudits);
                }
            }
            catch (Exception ex)
            {
               
            }

            return audits;
        }
    }
}