﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Models;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMChatService : IPRMChatService
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();

        #region Services

        public List<ChatHistory> GetChatHistory(int fromid, int toid, int moduleid, string module, string sessionid)
        {
            List<ChatHistory> details = new List<ChatHistory>();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $@"SELECT CH.*, concat(U1.U_FNAME, ' ', U1.U_LNAME) AS FromName, concat(U2.U_FNAME, ' ', U2.U_LNAME) AS ToName  
                                FROM ChatHistory CH 
                                INNER JOIN ChatModule CM ON CM.ChatId = CH.ChatID
                                INNER JOIN User U1 ON U1.U_ID = CH.FromId
                                INNER JOIN User U2 ON U2.U_ID = CH.ToId
                                WHERE ((FromId = {fromid} AND ToId = {toid}) || (FromId = {toid} AND ToId = {fromid})) AND ModuleId = 0
                                ORDER BY DateCreated";
                if (moduleid > 0 && !string.IsNullOrEmpty(module))
                {
                    query = $@"SELECT CH.*, concat(U1.U_FNAME, ' ', U1.U_LNAME) AS FromName, concat(U2.U_FNAME, ' ', U2.U_LNAME) AS ToName  
                                FROM ChatHistory CH 
                                INNER JOIN ChatModule CM ON CM.ChatId = CH.ChatID
                                INNER JOIN User U1 ON U1.U_ID = CH.FromId
                                INNER JOIN User U2 ON U2.U_ID = CH.ToId
                                WHERE ((FromId = {fromid} AND ToId = {toid}) || (FromId = {toid} AND ToId = {fromid})) AND ModuleId = {moduleid}
                                AND Module = '{module}'
                                ORDER BY DateCreated";                   
                }

                CORE.DataNamesMapper<ChatHistory> mapper = new CORE.DataNamesMapper<ChatHistory>();
                var dataTable = sqlHelper.SelectQuery(query);
                details = mapper.Map(dataTable).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<ChatHistory> GetModuleChatHistory(int moduleid, string module, int user, string sessionid)
        {
            List<ChatHistory> details = new List<ChatHistory>();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $@"SELECT CH.*, concat(U1.U_FNAME, ' ', U1.U_LNAME) AS FromName, concat(U2.U_FNAME, ' ', U2.U_LNAME) AS ToName  
                                FROM ChatHistory CH 
                                INNER JOIN ChatModule CM ON CM.ChatId = CH.ChatID
                                INNER JOIN User U1 ON U1.U_ID = CH.FromId
                                INNER JOIN User U2 ON U2.U_ID = CH.ToId
                                WHERE (FromId = {user} || ToId = {user}) AND ModuleId = {moduleid}
                                AND Module = '{module}'
                                ORDER BY DateCreated";

                CORE.DataNamesMapper<ChatHistory> mapper = new CORE.DataNamesMapper<ChatHistory>();
                var dataTable = sqlHelper.SelectQuery(query);
                details = mapper.Map(dataTable).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response SaveChatDetails(ChatHistory details)
        {
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(details.SessionID);
                details.Module = (details.ModuleId <= 0 || string.IsNullOrWhiteSpace(details.Module)) ? string.Empty : details.Module;
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_ChatId", details.ChatId);
                sd.Add("P_Message", details.Message);
                sd.Add("P_FromId", details.FromId);
                sd.Add("P_ToId", details.ToId);
                sd.Add("P_ModuleId", details.ModuleId);
                sd.Add("P_Module", details.Module);
                var dataset = sqlHelper.SelectList("chat_SaveChatDetails", sd);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        #endregion Services

    }

}