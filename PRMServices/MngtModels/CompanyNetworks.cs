﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class CompanyNetworks : Entity
    {        
        [DataMember(Name = "userID")]
        public int UserId { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "compNetID")]
        public int CompNetID { get; set; }

        [DataMember(Name = "link")]
        public string Link { get; set; }

        [DataMember(Name = "linkType")]
        public string LinkType { get; set; }

        [DataMember(Name = "userName")]
        public string UserName { get; set; }

    }
}