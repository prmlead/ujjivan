﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;
using PRMServices.models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMCustomFieldService
    {
        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, 
            ResponseFormat = WebMessageFormat.Json, 
            UriTemplate = "getcustomfields?compid={compid}&customfieldid={customfieldid}&fieldname={fieldname}&fieldmodule={fieldmodule}&sessionid={sessionID}")]
        List<PRMCustomField> GetCustomFields(int compid, int customfieldid, string fieldname, string fieldmodule, string sessionid);


        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getcustomfieldsbymoduleid?compid={compid}&fieldmodule={fieldmodule}&moduleid={moduleid}&sessionid={sessionID}")]
        List<PRMCustomField> GetCustomFieldsByModuleId(int compid, string fieldmodule, int moduleid, string sessionid);

        //[WebGet(RequestFormat = WebMessageFormat.Json,
        //    ResponseFormat = WebMessageFormat.Json,
        //    UriTemplate = "getprmfieldmappingdetails?compid={compid}&templatename={templatename}&sessionid={sessionID}")]
        //List<PRMFieldMappingDetails> GetPRMFieldMappingDetails(int compid, string templatename, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getfieldmappingtemplates?compid={compid}&sessionid={sessionID}")]
        string[] GetPRMFieldMappingTemplates(int compid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "templates?compid={compid}&sessionid={sessionID}")]
        PRMTemplate[] GetTemplates(int compid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "templatefields?templateid={templateid}&templatename={templatename}&sessionid={sessionID}")]
        PRMTemplateFields[] GetTemplateFields(int templateid, string templatename, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "gettemplatesubitem?templateid={templateid}&sessionid={sessionid}")]
        List<SubItemTemplate> GetTemplateSubItem(int templateid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savetemplatesubitem")]
        Response SaveTemplateSubItem(SubItemTemplate subitemtemplate, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savetemplatefields")]
        Response SaveTemplateFields(PRMTemplateFields[] fields);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savetemplate")]
        Response SaveTemplate(PRMTemplate template);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savecustomfield")]
        Response SaveCustomField(PRMCustomField details, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savecustomfieldvalue")]
        Response SaveCustomFieldValue(PRMCustomField[] details, string sessionid);
    }
}
