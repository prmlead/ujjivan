﻿prmApp
    .controller('prmTemplateFieldsCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "growlService",
        "PRMCustomFieldService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, growlService, PRMCustomFieldService) {

            $scope.templateId = $stateParams.Id;
            $scope.templateName = $stateParams.name;
            console.log($stateParams);
            $scope.userID = userService.getUserId();
            $scope.compID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();
            $scope.prmTemplateFieldsList = [];
            $scope.selectedTemplateField = {};
            $scope.templateFieldKeys = [];
            $scope.productQuotationTemplateMaster = [];
            $scope.companyItemUnits = [];

            $scope.getPRMTemplateFieldsList = function () {
                var params = {
                    "templateid": $scope.templateId,
                    "templatename": '',
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params)
                    .then(function (response) {
                        $scope.prmTemplateFieldsList = response;
                    });
            };

            auctionsService.GetCompanyConfiguration($scope.compID, "TEMPLATE_FIELD_KEY", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.templateFieldKeys = _.filter(unitResponse, function (field) {
                        return field.configKey === 'TEMPLATE_FIELD_KEY';
                    });
                });

            $scope.getPRMTemplateFieldsList();
            
            $scope.goToTemplate = function (template) {
                var url = $state.href("template", { "Id": template.TEMPLATE_ID });
                window.open(url, '_self');
            };


            $scope.editTemplateField = function (templateField) {
                createTemplateFieldObj();
                $scope.selectedTemplateField = templateField;
            };

            $scope.newTemplateField = function () {
                createTemplateFieldObj();
            };

            $scope.saveTemplateField = function () {
                let values = [];
                $scope.selectedTemplateField.FIELD_READ_ONLY = $scope.selectedTemplateField.FIELD_READ_ONLY ? 1 : 0;
                values.push($scope.selectedTemplateField);
                angular.element('#showTemplatFieldModal').modal('hide');
                let params = {
                    fields: values
                };

                PRMCustomFieldService.SaveTemplateFields(params)
                    .then(function (response) {
                        if (response && +response.objectID) {
                            $scope.getPRMTemplateFieldsList();                            
                            swal("Success!", "Successfully created.", "success");
                        } else {
                            swal("Error!", "Error saving data.", "error");
                        }
                    });
            };

            $scope.isValidForm = function () {
                let isValid = false;

                if ($scope.selectedTemplateField) {
                    if ($scope.selectedTemplateField.FIELD_NAME && $scope.selectedTemplateField.FIELD_LABEL) {
                        isValid = true;
                    }
                }

                if ($scope.duplicateTemplateField()) {
                    isValid = false;
                }

                return isValid;
            };

            $scope.duplicateTemplateField = function () {
                let isDuplicate = false;

                let temp = $scope.prmTemplateFieldsList.filter(function (item) {
                    return item.FIELD_NAME === $scope.selectedTemplateField.FIELD_NAME && item.TEMPLATE_FIELD_ID !== $scope.selectedTemplateField.TEMPLATE_FIELD_ID;
                });

                if (temp && temp.length > 0) {
                    isDuplicate = true;
                }

                return isDuplicate;
            };

            function createTemplateFieldObj() {
                $scope.selectedTemplateField = {
                    TEMPLATE_FIELD_ID: 0,
                    TEMPLATE_ID: $scope.templateId,
                    FIELD_NAME: '',
                    FIELD_LABEL: '',
                    FIELD_PLACEHOLDER: '',
                    FIELD_DATA_TYPE: '',
                    FIELD_DEFAULT_VALUE: '',
                    FIELD_OPTIONS: '',
                    FIELD_READ_ONLY: 0,
                    FIELD_IS_VENDOR: 1,
                    FIELD_IS_CUSTOMER: 1,
                    ModifiedBy: userService.getUserId()
                };
            }

            auctionsService.GetCompanyConfiguration($scope.compID, 'ITEM_UNITS', userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = [];
                    unitResponse.forEach(function (item, index) {
                        if (item.configKey === 'ITEM_UNITS') {
                            $scope.companyItemUnits.push(item);
                        }
                    });
                });


            //Below product sub item
            $scope.SaveProductQuotationTemplate = function (obj) {
                var sameNameError = false;
                $scope.productQuotationTemplateMaster.forEach(function (item, itemIndex) {
                    if (obj.NAME.toUpperCase() === item.NAME.toUpperCase() && obj.T_ID === 0) {
                        sameNameError = true;
                    }
                });

                if (sameNameError) { growlService.growl("Same name Error.", "inverse"); return false; }

                var params = {
                    "subitemtemplate": obj,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.saveTemplateSubItem(params)
                    .then(function (response) {
                        if (response) {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.resetTemplateObj();
                            $scope.GetProductQuotationTemplate();
                            angular.element('#newSubItemAction').modal('hide');
                        }
                    });
            };

            $scope.resetTemplateObj = function () {
                $scope.QuotationTemplateObj = {
                    SUB_ITEM_ID: 0,
                    TEMPLATE_ID: $scope.templateId,
                    NAME: '',
                    DESCRIPTION: '',
                    HAS_SPECIFICATION: 0,
                    HAS_PRICE: 0,
                    HAS_QUANTITY: 0,
                    CONSUMPTION: 1,
                    UOM: '',
                    HAS_TAX: 0,
                    IS_VALID: 1,
                    U_ID: userService.getUserId()
                };
            };

            $scope.resetTemplateObj();

            $scope.GetProductQuotationTemplate = function () {

                var params = {
                    "templateid": $scope.templateId,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.getTemplateSubItem(params)
                    .then(function (response) {
                        if (response) {
                            $scope.productQuotationTemplateMaster = response;
                        }
                    });
            };

            $scope.GetProductQuotationTemplate();

            $scope.editSubItemTemplate = function (obj) {
                $scope.QuotationTemplateObj = obj;
            };

            $scope.deleteSubItemTemplate = function (obj) {
                obj.IS_VALID = 0;
                $scope.QuotationTemplateObj = obj;
                var params = {
                    "subitemtemplate": obj,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.saveTemplateSubItem(params)
                    .then(function (response) {
                        if (response) {
                            growlService.growl("Deleted Successfully.", "success");
                            $scope.resetTemplateObj();
                            $scope.GetProductQuotationTemplate();
                        }
                    });
            };
            //^Above product sub item

        }]);