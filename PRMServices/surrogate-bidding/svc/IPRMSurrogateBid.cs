﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMSurrogateBid
    {

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsurrogateusers?reqid={reqid}&vendorid={vendorid}&deptid={deptid}&onlyvalid={onlyvalid}&sessionid={sessionid}")]
        List<Surrogate> GetSurrogateUsers(int reqid, int vendorid, int deptid, string sessionid, bool onlyvalid = true);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "savesurrogateusers")]
        Response SaveSurrogateUsers(List<Surrogate> surrogates, int reqid, string comments, int user, string sessionid);
    }
}
