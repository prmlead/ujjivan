﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('surrogate-bid', {
                    url: '/surrogate-bid/:Id/:VendorId/:compId',
                    templateUrl: 'surrogate-bidding/views/surrogate-bid.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('surrogate-bid-cb', {
                    url: '/surrogate-bid-cb/:Id/:VendorId/:compId',
                    templateUrl: 'surrogate-bidding/views/surrogate-bid-cb.html',
                    params: {
                        detailsObj: null
                    }
                });
        }]);