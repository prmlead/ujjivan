﻿using PRM.Data;
using PRMServices.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace PRMServices
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

            DbExtension.Invoke();
            AutofacConfiguration.Initialize();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Response.Cookies["ASP.NET_SessionID"].Secure = true;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Headers.Remove("X-Powered-By");
            HttpContext.Current.Response.Headers.Remove("X-AspNet-Version");
            HttpContext.Current.Response.Headers.Remove("Server");
            HttpContext.Current.Response.Headers.Remove("Cache-Control");
            
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Cache.SetNoStore();
            HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);

            HttpContext.Current.Response.Headers.Add("X-Frame-Options", "SAMEORIGIN");
            //HttpContext.Current.Response.Headers.Add("Referrer-Policy", "same-origin");
            HttpContext.Current.Response.Headers.Add("X-Content-Type-Options", "nosniff");
            HttpContext.Current.Response.Headers.Add("X-XSS-Protection", "1; mode=block");
            HttpContext.Current.Response.Headers.Add("Cache-Control", "no-store, no-cache, must-revalidate");
            //HttpContext.Current.Response.Headers.Add("Content-Security-Policy", "default - src https: 'unsafe-eval' 'unsafe-inline'; object-src 'none'");
            Response.Cookies["ASP.NET_SessionID"].Secure = true;
            //HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            //if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            //{
            //    HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST");
            //    HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept");
            //    HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
            //    HttpContext.Current.Response.End();
            //}
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            Response.Cookies["ASP.NET_SessionID"].Secure = true;
        }
    }
}