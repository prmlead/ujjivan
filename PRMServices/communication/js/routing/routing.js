﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('communication', {
                    url: '/communication',
                    templateUrl: 'communication/views/communication.html',
                    params: {
                        detailsObj: null
                    }
                })
        }]);