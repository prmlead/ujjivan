﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRMServices.Models.Catalog;

namespace PRMServices.Models
{
    [DataContract]
    public class SlotBooking : Entity
    {
        [DataMember(Name = "slot_item_id")]
        public int Slot_item_id { get; set; }

        [DataMember(Name = "slot_vendor_id")]
        public int Slot_vendor_id { get; set; }

        [DataMember(Name = "slot_id")]
        public int Slot_id { get; set; }

        [DataMember(Name = "slot_is_valid")]
        public int Slot_is_valid { get; set; }

        [DataMember(Name = "slot_entity_id")]
        public int Slot_entity_id { get; set; }


        [DataMember(Name = "slot_description")]
        public string Slot_description { get; set; }

        [DataMember(Name = "slot_start_time")]
        public DateTime? Slot_Start_Time { get; set; }

        [DataMember(Name = "slot_end_time")]
        public DateTime? Slot_End_Time { get; set; }

        [DataMember(Name = "is_slot_approved")]
        public int Is_slot_approved { get; set; }

        [DataMember(Name = "IS_CUSTOMER")]
        public bool IS_CUSTOMER  { get; set; }

        [DataMember(Name = "vendor")]
        public VendorDetails Vendor { get; set; }


    }
}