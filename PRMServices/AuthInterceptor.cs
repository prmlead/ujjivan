﻿using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using System.Text.RegularExpressions;
using System.Web;

namespace PRMServices
{
    public class AuthInterceptorAttribute : Attribute, IOperationBehavior, IParameterInspector
    {
        public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
        }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
            dispatchOperation.ParameterInspectors.Add(this);
        }

        public void Validate(OperationDescription operationDescription)
        {
        }

        public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState)
        {
            Console.WriteLine("Operation {0} returned: result = {1}", operationName, returnValue);
        }

        public object BeforeCall(string operationName, object[] inputs)
        {
            Console.WriteLine("Calling {0} with the following parameters:", operationName);

            int isValid = 1;
            WebOperationContext ctx = WebOperationContext.Current;

            try
            {
                List<string> keys = new List<string>() { "select", "update", "delete", "truncate", "drop", "grant", "call", "force", "purge", "procedure", "privileges", "revoke", "analyze", "insert", "user_resources", "dataabse", "purge", "right" };
                if (inputs != null && inputs.Length > 0)
                {
                    for (int i = 0; i <= inputs.Length - 1; i++)
                    {
                        if (inputs[i] != null && inputs[i].GetType().Name.Equals("string", StringComparison.InvariantCultureIgnoreCase))
                        {
                            inputs[i] = MySql.Data.MySqlClient.MySqlHelper.EscapeString(inputs[i].ToString());
                            inputs[i] = Regex.Replace(inputs[i].ToString(), @"[^@.,0-9a-zA-Z_\-\s]", "");
                            foreach (var key in keys)
                            {
                                inputs[i] = Regex.Replace(inputs[i].ToString(), key, "", RegexOptions.IgnoreCase);
                            }
                        }
                    }
                }
            }
            catch
            {

            }

            try
            {
                string sessionId = string.Empty;
                string UserId = string.Empty;
                if (ctx != null && ctx.IncomingRequest != null && ctx.IncomingRequest.Headers != null && ctx.IncomingRequest.Headers.Count > 0)
                {
                    bool isValidHost = false;
                    string validHosts = ConfigurationManager.AppSettings["VALID_HOST"];
                    List<string> hosts = validHosts.Split(';').ToList();
                    foreach(var host in hosts)
                    {
                        if (ctx.IncomingRequest.Headers["Host"].ToLower().Contains(host.ToLower()))
                        {
                            isValidHost = true;
                            break;
                        }
                    }


                    if (!isValidHost)
                    {
                        throw new Exception("Invalid Host.");
                    }
                }

                var principal = Utilities.ValidateJWTToken();

                if (principal != null && principal.Claims != null && principal.Claims.Count() > 0)
                {
                    var jwttokenUserId = principal.Claims.ToList().First(c => c.Type == "USER_ID").Value;
                    if (ctx != null && ctx.IncomingRequest.Headers["user_id"] != null)
                    {
                        UserId = ctx.IncomingRequest.Headers["user_id"].ToString();
                    }

                    if (!string.IsNullOrEmpty(UserId) && !UserId.Equals("0") && !string.IsNullOrWhiteSpace(jwttokenUserId) && !UserId.Equals(jwttokenUserId))
                    {
                        ctx.OutgoingResponse.Headers.Add("PRM360-AUTHORIZATION-TOKEN", "UN_AUTHORIZED");
                    }
                    else
                    {
                        MSSQLBizClass sqlHelper = new MSSQLBizClass();
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_SESSION_ID", principal.Claims.ToList().First(c => c.Type == "SESSION_ID").Value);
                        sd.Add("P_U_ID", jwttokenUserId);
                        DataSet ds = sqlHelper.SelectList("cp_ValidateSession", sd);
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            isValid = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                        }

                        if (isValid <= 0)
                        {
                            ctx.OutgoingResponse.Headers.Add("PRM360-AUTHORIZATION-TOKEN", "UN_AUTHORIZED");
                        }
                        else
                        {
                            ctx.OutgoingResponse.Headers.Add("PRM360-AUTHORIZATION-TOKEN", "AUTHORIZED");
                        }
                    }
                }
                else
                {
                    ctx.OutgoingResponse.Headers.Add("PRM360-AUTHORIZATION-TOKEN", "UN_AUTHORIZED");
                }
            }
            catch(Exception ex)
            {
                ctx.OutgoingResponse.Headers.Add("PRM360-AUTHORIZATION-TOKEN", "UN_AUTHORIZED");
            }

            return null;
        }
    }
}