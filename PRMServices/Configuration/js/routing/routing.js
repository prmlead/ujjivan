﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('incoconfiguration', {
                    url: '/incoconfiguration',
                    templateUrl: 'Configuration/views/IncoConfiguration.html'
                });

        }]);