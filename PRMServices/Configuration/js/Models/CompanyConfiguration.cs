﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRMServices.Models;

namespace PRMConfigurationService
{
    [DataContract]
    public class CompanyConfiguration : Entity
    {

        [DataMember(Name = "compConfigID")]
        public int CompConfigID { get; set; }

        [DataMember(Name = "FIELD_PK_ID")]
        public int FIELD_PK_ID { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "configKey")]
        public string ConfigKey { get; set; }

        [DataMember(Name = "configValue")]
        public string ConfigValue { get; set; }

        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "configText")]
        public string ConfigText { get; set; }

        [DataMember(Name = "configType")]
        public string ConfigType { get; set; }

        [DataMember(Name = "createdByName")]
        public string CreatedByName { get; set; }

        [DataMember(Name = "modifiedByName")]
        public string ModifiedByName { get; set; }

        [DataMember(Name = "dateCreate")]
        public DateTime? DateCreate { get; set; }

        [DataMember(Name = "dateModified")]
        public DateTime? DateModified { get; set; }

        [DataMember(Name = "configValue1")]
        public string ConfigValue1 { get; set; }

        [DataMember(Name = "configValue2")]
        public string ConfigValue2 { get; set; }
    }
    [DataContract]
    public class ProductConfiguration : Entity
    {

        [DataMember(Name = "pc_ID")]
        public int PcID { get; set; }

        [DataMember(Name = "config_ID")]
        public int ConfigID { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "prodID")]
        public int ProdID { get; set; }

        [DataMember(Name = "isVendorEdit")]
        public int IsVendorEdit { get; set; }

        [DataMember(Name = "isCustomerEdit")]
        public int IsCustomerEdit { get; set; }

        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "configText")]
        public string ConfigText { get; set; }

        [DataMember(Name = "productName")]
        public string ProductName { get; set; }

         [DataMember(Name = "createdByName")]
        public string CreatedByName { get; set; }

        [DataMember(Name = "modifiedByName")]
        public string ModifiedByName { get; set; }

        [DataMember(Name = "dateCreate")]
        public DateTime? DateCreate { get; set; }

        [DataMember(Name = "dateModified")]
        public DateTime? DateModified { get; set; }

    }

}