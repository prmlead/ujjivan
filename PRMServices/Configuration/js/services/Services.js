prmApp.constant('PRMConfigurationServiceDomain', 'Configuration/svc/PRMConfigurationService.svc/REST/');
prmApp.service('PRMConfigurationService', ["PRMConfigurationServiceDomain", "userService", "httpServices","$window",
    function (PRMConfigurationServiceDomain, userService, httpServices, $window) {

        var PRMServices = this;

        PRMServices.GetCompanyConfiguration = function (compid, configkey, sessionid) {
            let url = PRMConfigurationServiceDomain + 'getcompanyconfiguration?compid=' + compid + '&configkey=' + configkey + '&sessionid=' + sessionid;
            console.log(url);
            return httpServices.get(url);
        };

        PRMServices.SaveCompanyConfiguration = function (params) {
            let url = PRMConfigurationServiceDomain + 'savecompanyconfiguration';
            return httpServices.post(url, params);
        };

        PRMServices.GetNoncoreProductConfiguration = function (compid, configkey, userId, sessionid) {
            let url = PRMConfigurationServiceDomain + 'getcompanyconfiguration?compid=' + compid + '&configkey=' + configkey + '&userId=' + userId + '&sessionid=' + sessionid;
            console.log(url);
            return httpServices.get(url);
        };

        PRMServices.SaveProductConfiguration = function (params) {
            let url = PRMConfigurationServiceDomain + 'saveproductconfiguration';
            return httpServices.post(url, params);
        };

        PRMServices.GetProductConfiguration = function (compid, sessionid) {
            let url = PRMConfigurationServiceDomain + 'getproductconfiguration?compid=' + compid + '&sessionid=' + sessionid;
            console.log(url);
            return httpServices.get(url);
        };

        return PRMServices;

}]);