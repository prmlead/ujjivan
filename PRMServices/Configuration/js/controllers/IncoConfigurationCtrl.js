﻿prmApp
    .controller('IncoConfigurationCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices", "PRMConfigurationService", "catalogService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, PRMConfigurationService, catalogService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();

            $scope.companyConfiguration = [];
            $scope.GetNoncoreProductConfiguration = [];           
            $scope.productConfiguration = [];
            $scope.companyConfiguration2 = [];
            $scope.displayCard = 0;

            $scope.incoterm = {
                compConfigID: 0,
                compID: 0,
                selectedDeptType: {},
                IsEdit: '',
                configKey: '',
                configText: '',
                configValue1: '',
                configValue2: '',
                configType: '',
                configValue: '',
                isValid: true,
            };


            $scope.configuration = {
                compConfigID: 0,
                compID: $scope.compID,
                selectedDeptType: {},
                configKey: '',
                configText: '',
                configValue1: '',
                configValue2: '',
                configType: '',
                configValue: '',
                isValid: true,
            };

            $scope.product = {
                pc_ID: 0,
                config_ID: '',
                compID: $scope.compID,
                prodID: '',
                isVendorEdit: '',
                isCustomerEdit: '',
                isedit: '',
                userID: $scope.userID,
                isValid: true
            };
            $scope.addnewconfigView = false;
            $scope.addnewincoView = false;
            $scope.addnewincoprodView = false;

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };
            $scope.pageChanged = function () {
            };

            /*PAGINATION CODE*/
            $scope.currentPage1 = 1;
            $scope.itemsPerPage1 = 10;
            $scope.maxSize1 = 5;


            $scope.setPage1 = function (pageNoo) {
                $scope.currentPage1 = pageNoo;
            };

            $scope.pageChanged1 = function () {
            };


            $scope.GetCompanyConfiguration = function () {

                PRMConfigurationService.GetCompanyConfiguration(userService.getUserCompanyId(), "INCO", userService.getUserToken())
                    .then(function (response) {
                        $scope.companyConfiguration1 = [];
                        $scope.companyConfiguration = response;
                        $scope.companyConfiguration1 = response;
                        $scope.companyConfiguration1.forEach(function (item) {
                            item.dateCreate = userService.toLocalDateOnly(item.dateCreate);
                            item.dateModified = userService.toLocalDateOnly(item.dateModified);
                        });
                        $scope.totalItems = $scope.companyConfiguration.length;
                    });

            }

            $scope.GetCompanyConfiguration();
            console.log($scope.configuration)

            $scope.SaveCompanyConfiguration = function () {

                console.log($scope.configuration)
                var ts = moment($scope.configuration.dateCreate, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var createdDate = new Date(m);
                var milliseconds = parseInt(createdDate.getTime() / 1000.0);
                $scope.configuration.dateCreate = "/Date(" + milliseconds + "000+0530)/";

                var tss = moment($scope.configuration.dateModified, "DD-MM-YYYY").valueOf();
                var mm = moment(tss);
                var modifiedDate = new Date(mm);
                var milliseconds = parseInt(modifiedDate.getTime() / 1000.0);
                $scope.configuration.dateModified = "/Date(" + milliseconds + "000+0530)/";

                $scope.configurationList = [];

                $scope.configurationList.push($scope.configuration);
                var params = {
                    "listCompanyConfiguration": $scope.configurationList,
                    "sessionID": $scope.sessionID

                };

                PRMConfigurationService.SaveCompanyConfiguration(params)
                    .then(function (response) {
                        if (response) {
                            if (response.errorMessage == '') {
                                if ($scope.configuration.isValid) {
                                    swal("Success", "Saved Successfully.", "success");
                                }
                                else {                                    
                                    swal("Done", "Deleted Successfully.", "success");
                                }
                                $scope.GetCompanyConfiguration();
                                $scope.addnewconfigView = false;
                                $scope.configuration = {
                                    compConfigID: 0,
                                    compID: $scope.compID,
                                    configKey: '',
                                    configText: '',
                                    configValue1: '',
                                    configValue2: '',
                                    configType: '',
                                    configValue: '',
                                    isValid: true,
                                };

                            } else if (response.errorMessage == 'Already Exists') {
                                growlService.growl("Already Exists.", 'inverse');

                            }                            
                            else /*if (response.errorMessage == 'This incoterm already is in used')*/ {
                                growlService.growl("This incoterm already is in used.", 'inverse');
                                $scope.configuration.dateCreate = userService.toLocalDateOnly($scope.configuration.dateCreate);
                                $scope.configuration.dateModified = userService.toLocalDateOnly($scope.configuration.dateModified);
                                $scope.addnewconfigView = false;
                                $scope.GetCompanyConfiguration();
                            }
                            
                        }                        
                        

                    });

            };

            $scope.nonCoreproductsList = [];
            $scope.editConfiguration = function (companyConfiguration, type) {
                $scope.addnewconfigView = true;
                companyConfiguration.configKey = type;
                $scope.configuration = companyConfiguration;
                $scope.configuration.sessionID = $scope.sessionID;
                $scope.deptAdmin = $scope.userID;
                $scope.configuration.userID = $scope.userID;
                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox
            };


            $scope.editIncoConfiguration = function (productConfiguration) {
                $scope.addnewincoprodView = true;
                $scope.product = productConfiguration;
                $scope.product.sessionID = $scope.sessionID;
                $scope.deptAdmin = $scope.userID;
                $scope.product.userID = $scope.userID;                

            };


            $scope.closeEditConfiguration = function () {
                $scope.addnewconfigView = false;
                $scope.configuration = {
                    compConfigID: 0,
                    compID: 0,
                    configKey: '',
                    configText: '',
                    configValue1: '',
                    configValue2: '',
                    configType: '',
                    configValue: '',
                    selectedDeptType: {},
                    isValid: true,
                };
            };

            $scope.closeEditIncoConfiguration = function () {
                $scope.addnewincoprodView = false;
                $scope.product = {
                    pc_ID: 0,
                    compID: 0,
                    config_ID: '',
                    prodID: '',
                    isVendorEdit: '',
                    isCustomerEdit: '',
                    userID: '',
                    configText: '',
                    productName: '',
                    isValid: true,
                };
            };
            
            $scope.deleteIncoConfiguration = function (companyConfiguration, action) {                
                $scope.addnewincoView = true;
                $scope.configuration = companyConfiguration;
                $scope.configuration.userID = $scope.userID;
                
                $scope.configuration.isValid = action == 0 ? false : true;
                swal({
                    title: "Are you sure?",
                    text: 'Do You want to Delete the Item Permanently',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "yes!",
                    closeOnConfirm: true
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            $scope.SaveCompanyConfiguration();                            
                        } else {
                            return;
                        }
                    });                
                $scope.incoterm.sessionID = $scope.sessionID;
                $scope.deptAdmin = $scope.userID;                
                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox
            };

            $scope.getNonCoreProducts = function () {

                catalogService.GetNonCoreProducts($scope.compID, $scope.sessionID)
                    .then(function (unitResponse) {
                        $scope.nonCoreproductsList = unitResponse;
                        console.log($scope.nonCoreproductsList);
                    });
            };

            $scope.getNonCoreProducts();
            $scope.GetNoncoreProductConfiguration = function () {
                PRMConfigurationService.GetNoncoreProductConfiguration(userService.getUserCompanyId(), "INCO", userService.getUserToken())
                    .then(function (response) {
                        $scope.GetNoncoreProductConfiguration = response;
                        $scope.totalItems = $scope.GetNoncoreProductConfiguration.length;
                    });
            }
            $scope.totalItems1 = 0;

            $scope.GetProductConfiguration = function () {

                PRMConfigurationService.GetProductConfiguration(userService.getUserCompanyId(), userService.getUserToken())
                    .then(function (response) {                       
                        $scope.productConfiguration = response;
                        $scope.productConfiguration.forEach(function (item) {
                            item.dateCreate = userService.toLocalDateOnly(item.dateCreate);                           
                        });
                        $scope.totalItems1 = $scope.productConfiguration.length;
                    });
            }
            $scope.GetProductConfiguration();
            
            $scope.SaveProductConfiguration = function () {
             var ts = moment($scope.configuration.dateCreate, "DD-MM-YYYY").valueOf();
                $scope.productList = [];
                $scope.productList.push($scope.product);
                var params = {
                    "listProductConfiguration": $scope.productList,
                    "sessionID": $scope.sessionID

                };
                
                PRMConfigurationService.SaveProductConfiguration(params)
                    .then(function (response) {
                        if (response) {
                            if (response.errorMessage == '') {                                
                                swal("Success", "Saved Successfully.", "success");                               
                                $scope.GetProductConfiguration();
                                $scope.addnewincoprodView = false;
                                $scope.product = {
                                    pc_ID:'',
                                    config_ID: '',
                                    compID: '',
                                    prodID: '',
                                    isVendorEdit: '',
                                    isCustomerEdit: '',
                                    isValid: true,
                                };                               
                            } else {
                                growlService.growl("Already Exists.", 'inverse');

                            }
                        }
                    });
            };

            $scope.isValid = function (product) {

                if (+product.isedit) {
                    product.isVendorEdit = 0;
                    product.isCustomerEdit = 1;
                } else {
                    product.isVendorEdit = 1;
                    product.isCustomerEdit = 0;
                }
            };

        }]);