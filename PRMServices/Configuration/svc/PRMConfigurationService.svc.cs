﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.SQLHelper;
using PRMConfigurationService;
using PRMServices.Models;
using CompanyConfiguration = PRMConfigurationService.CompanyConfiguration;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMConfigurationService : IPRMConfigurationService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        public PRMServices prm = new PRMServices();

        public int ValidateSession(string sessionId, string temp = null)
        {
            return Utilities.ValidateSession(sessionId);
        }
        public Response SaveCompanyConfiguration(List<CompanyConfiguration> listCompanyConfiguration, string sessionID)
        {
            Response response = new Response();
            try
            {
                ValidateSession(sessionID);

                foreach (CompanyConfiguration CC in listCompanyConfiguration)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_C_CONFIG_ID", CC.CompConfigID);
                    sd.Add("P_COMP_ID", CC.CompID);
                    sd.Add("P_CONFIG_KEY", CC.ConfigKey);
                    sd.Add("P_CONFIG_VALUE", CC.ConfigValue);
                    sd.Add("P_CONFIG_TEXT", CC.ConfigText);
                    sd.Add("P_CONFIG_VALUE_1", CC.ConfigValue1);
                    sd.Add("P_CONFIG_VALUE_2", CC.ConfigValue2);
                    sd.Add("P_CONFIG_TYPE", CC.ConfigType);
                    sd.Add("P_IS_VALID", CC.IsValid==true?1:0);
                    sd.Add("P_U_ID", CC.UserID);
                    DataSet ds = sqlHelper.SelectList("cp_SaveCompanyConfiguration", sd);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                        response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        public List<CompanyConfiguration> GetCompanyConfiguration(int compID, string configKey, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<CompanyConfiguration> listCompanyConfiguration = new List<CompanyConfiguration>();
            try
            {
                int isValidSession = ValidateSession(sessionID, null);
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_CONFIG_KEY", configKey);
                DataSet ds = sqlHelper.SelectList("cp_GetCompanyConfiguration", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        CompanyConfiguration companyconfiguration = new CompanyConfiguration();

                        companyconfiguration.CompConfigID = row["C_CONFIG_ID"] != DBNull.Value ? Convert.ToInt16(row["C_CONFIG_ID"]) : 0;
                        companyconfiguration.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt16(row["COMP_ID"]) : 0;
                        companyconfiguration.ConfigKey = row["CONFIG_KEY"] != DBNull.Value ? Convert.ToString(row["CONFIG_KEY"]) : string.Empty;
                        companyconfiguration.ConfigValue = row["CONFIG_VALUE"] != DBNull.Value ? Convert.ToString(row["CONFIG_VALUE"]) : string.Empty;
                        companyconfiguration.ConfigText = row["CONFIG_TEXT"] != DBNull.Value ? Convert.ToString(row["CONFIG_TEXT"]) : string.Empty;
                        companyconfiguration.CreatedByName = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;
                        companyconfiguration.ModifiedByName = row["MODIFIED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["MODIFIED_BY_NAME"]) : string.Empty;
                        companyconfiguration.DateCreate = row["DATE_CREATE"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATE"]) : DateTime.MaxValue;
                        companyconfiguration.DateModified = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]) : DateTime.MaxValue;
                        if (string.IsNullOrEmpty(companyconfiguration.ConfigText))
                        {
                            companyconfiguration.ConfigText = companyconfiguration.ConfigValue;
                        }

                        companyconfiguration.IsValid = row["IS_VALID"] != DBNull.Value ? (Convert.ToInt32(row["IS_VALID"]) == 1 ? true : false) : false;
                        //companyconfiguration.UserID = row["MODIFIED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["MODIFIED_BY_NAME"]) : string.Empty;
                        listCompanyConfiguration.Add(companyconfiguration);
                    }
                }
            }
            catch (Exception ex)
            {
                CompanyConfiguration companyconfiguration = new CompanyConfiguration();
                companyconfiguration.ErrorMessage = ex.Message;
                listCompanyConfiguration.Add(companyconfiguration);
            }

            return listCompanyConfiguration;
        }
        public Response SaveProductConfiguration(List<ProductConfiguration> listProductConfiguration, string sessionID)
        {
            Response response = new Response();
            try
            {
                ValidateSession(sessionID);

                foreach (ProductConfiguration CC in listProductConfiguration)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_PC_ID", CC.PcID);
                    sd.Add("P_C_CONFIG_ID", CC.ConfigID);
                    sd.Add("P_COMP_ID", CC.CompID);
                    sd.Add("P_PROD_ID", CC.ProdID);
                    sd.Add("P_IS_VEND_EDIT", CC.IsVendorEdit);
                    sd.Add("P_IS_CUST_EDIT", CC.IsCustomerEdit);                   
                    sd.Add("P_IS_VALID", CC.IsValid == true ? 1 : 0);
                    sd.Add("P_U_ID", CC.UserID);
                    DataSet ds = sqlHelper.SelectList("cp_SaveProductConfiguration", sd);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                        response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        public List<ProductConfiguration> GetProductConfiguration(int compID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ProductConfiguration> listProductConfiguration = new List<ProductConfiguration>();
            try
            {
                int isValidSession = ValidateSession(sessionID, null);
                sd.Add("P_COMP_ID", compID);                
                DataSet ds = sqlHelper.SelectList("cp_GetProductConfiguration", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ProductConfiguration productconfiguration = new ProductConfiguration();

                        productconfiguration.PcID = row["PC_ID"] != DBNull.Value ? Convert.ToInt32(row["PC_ID"]) : 0;
                        productconfiguration.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                        productconfiguration.ConfigID = row["C_CONFIG_ID"] != DBNull.Value ? Convert.ToInt32(row["C_CONFIG_ID"]) : 0;
                        productconfiguration.ProdID = row["ProductId"] != DBNull.Value ? Convert.ToInt32(row["ProductId"]) : 0;
                        productconfiguration.IsVendorEdit = row["IS_VENDOR_EDIT"] != DBNull.Value ? Convert.ToInt32(row["IS_VENDOR_EDIT"]) : 0;
                        productconfiguration.IsCustomerEdit = row["IS_CUSTOMER_EDIT"] != DBNull.Value ? Convert.ToInt32(row["IS_CUSTOMER_EDIT"]) : 0;
                        productconfiguration.ConfigText = row["CONFIG_TEXT"] != DBNull.Value ? Convert.ToString(row["CONFIG_TEXT"]) : string.Empty;
                        productconfiguration.ProductName = row["ProductName"] != DBNull.Value ? Convert.ToString(row["ProductName"]) : string.Empty;
                        productconfiguration.CreatedByName = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;
                        productconfiguration.DateCreate = row["DATE_CREATE"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATE"]) : DateTime.MaxValue;
                        productconfiguration.DateModified = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]) : DateTime.MaxValue;

                        productconfiguration.IsValid = row["IS_VALID"] != DBNull.Value ? (Convert.ToInt32(row["IS_VALID"]) == 1 ? true : false) : false;
                        //productconfiguration.UserID = row["MODIFIED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["MODIFIED_BY_NAME"]) : string.Empty;
                        listProductConfiguration.Add(productconfiguration);
                    }
                }
            }
            catch (Exception ex)
            {
                ProductConfiguration productconfiguration = new ProductConfiguration();
                productconfiguration.ErrorMessage = ex.Message;
                listProductConfiguration.Add(productconfiguration);
            }

            return listProductConfiguration;
        }


    }

}