﻿using PRM.Core.Models;
using PRMConfigurationService;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using CompanyConfiguration = PRMConfigurationService.CompanyConfiguration;
using Response = PRMServices.Models.Response;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMConfigurationService
    {
        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanyconfiguration")]
        Response SaveCompanyConfiguration(List<CompanyConfiguration> listCompanyConfiguration, string sessionID);



        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyconfiguration?compid={compID}&configkey={configKey}&sessionid={sessionID}")]
        List<CompanyConfiguration> GetCompanyConfiguration(int compID, string configKey, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "SaveProductConfiguration")]
        Response SaveProductConfiguration(List<ProductConfiguration> listProductConfiguration, string sessionID);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getproductconfiguration?compid={compID}&sessionid={sessionID}")]
        List<ProductConfiguration> GetProductConfiguration(int compID, string sessionID);

    }
}
