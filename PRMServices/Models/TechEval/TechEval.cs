﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRMServices.Models;

using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class TechEval : ResponseAudit
    {
        [DataMember(Name = "evalID")]
        public int EvalID { get; set; }

        [DataMember(Name = "reqID")]
        public int ReqID { get; set; }

        [DataMember(Name = "vendor")]
        public UserDetails Vendor { get; set; }

        [DataMember(Name = "isApproved")]
        public int IsApproved { get; set; }

        [DataMember(Name = "totalMarks")]
        public decimal TotalMarks { get; set; }

        string comments = string.Empty;
        [DataMember(Name = "comments")]
        public string Comments
        {
            get
            {
                if (!string.IsNullOrEmpty(comments))
                {
                    return comments;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    comments = value;
                }
            }
        }

        [DataMember(Name = "isAnswered")]
        public int IsAnswered { get; set; }

    }
}