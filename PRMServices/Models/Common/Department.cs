﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class Department : Entity
    {        

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "deptID")]
        public int DeptID { get; set; }

        string _deptCode = string.Empty;
        [DataMember(Name = "deptCode")]
        public string DeptCode {
            get
            {
                return _deptCode;
            }
            set
            {
                _deptCode = value;
            }
        }

        string _deptDesc = string.Empty;
        [DataMember(Name = "deptDesc")]
        public string DeptDesc
        {
            get
            {
                return _deptDesc;
            }
            set
            {
                _deptDesc = value;
            }
        }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "deptAdmin")]
        public int DeptAdmin { get; set; }

        [DataMember(Name = "deptAdminName")]
        public string DeptAdminName { get; set; }

        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "isAssignedToUser")]
        public bool IsAssignedToUser { get; set; }

        [DataMember(Name = "listDesignation")]
        public List<Designation> ListDesignation { get; set; }

        [DataMember(Name = "deptTypeID")]
        public int DeptTypeID { get; set; }        
    }
}