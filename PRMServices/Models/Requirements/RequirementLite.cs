﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class RequirementLite : Entity
    {
        [DataNames("REQ_ID")]
        [DataMember(Name = "requirementId")]
        public int REQ_ID { get; set; }

        [DataNames("REQ_NUMBER")]
        [DataMember(Name = "REQ_NUMBER")]
        public string REQ_NUMBER { get; set; }

        [DataNames("REQ_TITLE")]
        [DataMember(Name = "REQ_TITLE")]
        public string REQ_TITLE { get; set; }

        [DataNames("CUSTOMER_U_ID")]
        [DataMember(Name = "customerId")]
        public int CUSTOMER_U_ID { get; set; }

        [DataNames("SUPER_USER_ID")]
        [DataMember(Name = "superUserId")]
        public int SUPER_USER_ID { get; set; }

        [DataNames("START_TIME")]
        [DataMember(Name = "startTime")]
        public DateTime? START_TIME { get; set; }

        [DataNames("END_TIME")]
        [DataMember(Name = "endTime")]
        public DateTime? END_TIME { get; set; }

        [DataNames("CLOSED")]
        [DataMember(Name = "status")]
        public string CLOSED { get; set; }

        [DataMember(Name = "vendors")]
        public List<VendorDetailsLite> AuctionVendors { get; set; }

        [DataMember(Name = "timeLeft")]
        public long TimeLeft { get; set; }

        [DataNames("SAVINGS")]
        [DataMember(Name = "savings")]
        public decimal Savings { get; set; }

        [DataMember(Name = "requirementItems")]
        public List<RequirementItemsLite> RequirementItems { get; set; }
    }
}