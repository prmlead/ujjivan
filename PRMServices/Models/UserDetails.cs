﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Domain.Vendors;

namespace PRMServices.Models
{
    [DataContract]
    public class UserDetails : User
    {
        DateTime established = DateTime.Now;


        [DataMember(Name = "achievements")]
        public string Achievements { get; set; }

        [DataMember(Name = "assocWithOEM")]
        public bool AssocWithOEM { get; set; }

        [DataMember(Name = "assocWithOEMFile")]
        public byte[] AssocWithOEMFile { get; set; }

        [DataMember(Name = "assocWithOEMFileName")]
        public string AssocWithOEMFileName { get; set; }

        [DataMember(Name = "clients")]
        public string Clients { get; set; }

        [DataMember(Name = "establishedDate")]
        public DateTime EstablishedDate
        {
            get
            {
                return established;
            }
            set
            {
                established = value;
            }
        }

        [DataMember(Name = "products")]
        public string Products { get; set; }

        [DataMember(Name = "strengths")]
        public string Strengths { get; set; }

        [DataMember(Name = "responseTime")]
        public string ResponseTime { get; set; }

        [DataMember(Name = "files")]
        public List<FileUpload> Files { get; set; }

        [DataMember(Name = "aboutUs")]
        public string AboutUs { get; set; }

        [DataMember(Name = "logoURL")]
        public string LogoURL { get; set; }

        [DataMember(Name = "logoFile")]
        public FileUpload LogoFile { get; set; }

        [DataMember(Name = "oemCompanyName")]
        public string OemName { get; set; }

        [DataMember(Name = "oemKnownSince")]
        public string OemKnownSince { get; set; }

        [DataMember(Name = "registrationScore")]
        public int RegistrationScore { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "role")]
        public string Role { get; set; }

        [DataMember(Name = "category")]
        public string Category { get; set; }

        [DataMember(Name = "profileFile")]
        public byte[] ProfileFile { get; set; }

        [DataMember(Name = "profileFileName")]
        public string ProfileFileName { get; set; }

        [DataMember(Name = "profileFileUrl")]
        public string ProfileFileUrl { get; set; }

        [DataMember(Name = "directors")]
        public string Directors { get; set; }

        [DataMember(Name = "address")]
        public string Address { get; set; }

        [DataMember(Name = "workingHours")]
        public string WorkingHours { get; set; }

        [DataMember(Name = "rating")]
        public string Rating { get; set; }

        [DataMember(Name = "subcategories")]
        public List<CategoryObj> Subcategories { get; set; }

        [DataMember(Name = "creditsLeft")]
        public bool CreditsLeft { get; set; }

        [DataMember(Name = "credits")]
        public int Credits { get; set; }

        [DataMember(Name = "currency")]
        public string Currency { get; set; }

        [DataMember(Name = "timeZone")]
        public string TimeZone { get; set; }

        [DataMember(Name = "NegotiationSettings")]
        public NegotiationSettings NegotiationSettings { get; set; }

        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "companyId")]
        public int CompanyId { get; set; }

        [DataMember(Name = "username")]
        public string UserName { get; set; }

        [DataMember(Name = "price")]
        public double Price { get; set; }

        [DataMember(Name = "isPrimary")]
        public int IsPrimary { get; set; }

        [DataMember(Name = "alternateID")]
        public int AlternateID { get; set; }

        [DataMember(Name = "catalogFile")]
        public string MyCatalogFile { get; set; }


        string pan = string.Empty;
        [DataMember(Name = "pan")]
        public string Pan
        {
            get
            {
                if (!string.IsNullOrEmpty(pan))
                { return pan; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { pan = value; }
            }
        }

        string tin = string.Empty;
        [DataMember(Name = "tin")]
        public string Tin
        {
            get
            {
                if (!string.IsNullOrEmpty(tin))
                { return tin; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { tin = value; }
            }
        }

        string stn = string.Empty;
        [DataMember(Name = "stn")]
        public string Stn
        {
            get
            {
                if (!string.IsNullOrEmpty(stn))
                { return stn; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { stn = value; }
            }
        }

        string department = string.Empty;
        [DataMember(Name = "department")]
        public string Department
        {
            get
            {
                if (!string.IsNullOrEmpty(department))
                { return department; }
                else
                { return string.Empty; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { department = value; }
            }
        }

        string designation = string.Empty;
        [DataMember(Name = "designation")]
        public string Designation
        {
            get
            {
                if (!string.IsNullOrEmpty(designation))
                { return designation; }
                else
                { return string.Empty; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { designation = value; }
            }
        }

        [DataMember(Name = "Street1")]
        public string Street1 { get; set; }

        [DataMember(Name = "Street2")]
        public string Street2 { get; set; }

        [DataMember(Name = "Street3")]
        public string Street3 { get; set; }

        [DataMember(Name = "Country")]
        public string Country { get; set; }

        [DataMember(Name = "State")]
        public string State { get; set; }

        [DataMember(Name = "PinCode")]
        public string PinCode { get; set; }

        [DataMember(Name = "City")]
        public string City { get; set; }

        [DataMember(Name = "BankName")]
        public string BankName { get; set; }

        [DataMember(Name = "BankAddress")]
        public string BankAddress { get; set; }

        [DataMember(Name = "accntNumber")]
        public string accntNumber { get; set; }

        [DataMember(Name = "Ifsc")]
        public string Ifsc { get; set; }

        [DataMember(Name = "cancelledCheque")]
        public string cancelledCheque { get; set; }

        [DataMember(Name = "ccAttach")]
        public string CcAttach { get; set; }

        [DataMember(Name = "cancelledchequeattachments")]
        public byte[] Cancelledchequeattachments { get; set; }

        [DataMember(Name = "cancelledchequeattachmentName")]
        public string CancelledchequeattachmentName { get; set; }

        [DataMember(Name = "altMobile")]
        public string altMobile { get; set; }

        [DataMember(Name = "landLine")]
        public string landLine { get; set; }

        [DataMember(Name = "salesPerson")]
        public string salesPerson { get; set; }

        [DataMember(Name = "salesContact")]
        public string salesContact { get; set; }

        [DataMember(Name = "Address1")]
        public string Address1 { get; set; }

        [DataMember(Name = "gstAttach")]
        public string GstAttach { get; set; }

        [DataMember(Name = "gstAttachments")]
        public byte[] GstAttachments { get; set; }

        [DataMember(Name = "gstAttachmentName")]
        public string GstAttachmentName { get; set; }

        [DataMember(Name = "panAttach")]
        public string PanAttach { get; set; }

        [DataMember(Name = "panAttachments")]
        public byte[] PanAttachments { get; set; }

        [DataMember(Name = "panAttachmentName")]
        public string PanAttachmentName { get; set; }

        [DataMember(Name = "BussinessDet")]
        public string BussinessDet { get; set; }

        [DataMember(Name = "businessAttach")]
        public string BussAttach { get; set; }

        [DataMember(Name = "businessAttachments")]
        public byte[] BussAttachments { get; set; }

        [DataMember(Name = "businessAttachmentName")]
        public string BussAttachmentName { get; set; }

        [DataMember(Name = "sapVendorCode")]
        public string SapVendorCode { get; set; }

        [DataMember(Name = "sapUserId")]
        public string SapUserId { get; set; }

        [DataMember(Name = "vendorStatus")]
        public int VendorStatus { get; set; }

        [DataMember(Name = "msmeAttach")]
        public string MsmeAttach { get; set; }

        [DataMember(Name = "msmeAttachments")]
        public byte[] MsmeAttachments { get; set; }

        [DataMember(Name = "msmeAttachmentName")]
        public string MsmeAttachmentName { get; set; }

        [DataMember(Name = "additionalVendorInformation")]
        public string AdditionalVendorInformation { get; set; }

        [DataMember(Name = "customerCopyLetterAttach")]
        public string CustomerCopyLetterAttach { get; set; }

        [DataMember(Name = "customerCopyLetterAttachments")]
        public byte[] CustomerCopyLetterAttachments { get; set; }

        [DataMember(Name = "customerCopyLetterAttachmentName")]
        public string CustomerCopyLetterAttachmentName { get; set; }

        [DataMember(Name = "PERSONAL_INFO_VALID")]
        public int PERSONAL_INFO_VALID { get; set; }

        [DataMember(Name = "CONTACT_INFO_VALID")]
        public int CONTACT_INFO_VALID { get; set; }

        [DataMember(Name = "BANK_INFO_VALID")]
        public int BANK_INFO_VALID { get; set; }

        [DataMember(Name = "BUSINESS_INFO_VALID")]
        public int BUSINESS_INFO_VALID { get; set; }

        [DataMember(Name = "CONFLICT_INFO_VALID")]
        public int CONFLICT_INFO_VALID { get; set; }

        [DataMember(Name = "totalVendors")]
        public int TotalVendors { get; set; }

        [DataMember(Name = "IS_SELECTED")]
        public int IS_SELECTED { get; set; }
        
        [DataMember(Name = "paymentTermCode")]
        public string PaymentTermCode { get; set; }
        [DataMember(Name = "paymentTermDesc")]
        public string PaymentTermDesc { get; set; }
        [DataMember(Name = "incoTerm")]
        public string IncoTerm { get; set; }

        [DataMember(Name = "companyVendorCodes")]
        public string CompanyVendorCodes { get; set; }

        [DataMember(Name = "vendorGSTNumbers")]
        public string VendorGSTNumbers { get; set; }

        [DataMember(Name = "mpnCode")]
        public string MpnCode { get; set; }

        [DataMember(Name = "deliveryDays")]
        public string DeliveryDays { get; set; }
        [DataMember(Name = "vendorPurchaseOrgData")]
        public string VendorPurchaseOrgData { get; set; }
        [DataMember(Name = "manufacturerCode")]
        public string ManufacturerCode { get; set; }
        [DataMember(Name = "manufacturerDivision")]
        public string ManufacturerDivision { get; set; }
        [DataMember(Name = "drugLicenseNo")]
        public string DrugLicenseNo { get; set; }
        [DataMember(Name = "registered")]
        public string Registered { get; set; }
        [DataMember(Name = "vendorCreatedDate")]
        public DateTime? VendorCreatedDate { get; set; }
        [DataMember(Name = "plantcode")]
        public string Plantcode { get; set; }

    }
}