﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class cr_UserDepartments : Entity
    {

        [DataMember] [DataNames("DEPT_ID")] public int DEPT_ID { get; set; }
        [DataMember] [DataNames("DEPT_CODE")] public string DEPT_CODE { get; set; }
        [DataMember] [DataNames("DEPT_DESC")] public string DEPT_DESC { get; set; }

    }
}