﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class VendorDetails : Entity
    {
        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "compID")]
        public int compID { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        string vendorName = string.Empty;
        [DataMember(Name = "vendorName")]
        public string VendorName
        {
            get
            {
                return this.vendorName;
            }
            set
            {
                this.vendorName = value;
            }
        }

        string vendorCode = string.Empty;
        [DataMember(Name = "vendorCode")]
        public string VendorCode
        {
            get
            {
                return this.vendorCode;
            }
            set
            {
                this.vendorCode = value;
            }
        }

        [DataMember(Name = "companyVendorCodes")]
        public string CompanyVendorCodes
        {
            get; set;
        }

        [DataMember(Name = "initialPrice")]
        public double InitialPrice { get; set; }

        [DataMember(Name = "runningPrice")]
        public double RunningPrice { get; set; }

        [DataMember(Name = "totalInitialPrice")]
        public double TotalInitialPrice { get; set; }

        [DataMember(Name = "totalRunningPrice")]
        public double TotalRunningPrice { get; set; }

        [DataMember(Name = "rank")]
        public int Rank { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "taxes")]
        public double Taxes { get; set; }

        [DataMember(Name = "quotationUrl")]
        public string QuotationUrl { get; set; }

        [DataMember(Name = "rating")]
        public double Rating { get; set; }

        string companyName = string.Empty;
        [DataMember(Name = "companyName")]
        public string CompanyName
        {
            get
            {
                return this.companyName;
            }
            set
            {
                this.companyName = value;
            }
        }

        [DataMember(Name = "warranty")]
        public string Warranty { get; set; }

        [DataMember(Name = "payment")]
        public string Payment { get; set; }

        [DataMember(Name = "duration")]
        public string Duration { get; set; }

        [DataMember(Name = "validity")]
        public string Validity { get; set; }

        [DataMember(Name = "REQ_VEND_SPEC_JSON")]
        public string REQ_VEND_SPEC_JSON { get; set; }
        
        [DataMember(Name = "isQuotationRejected")]
        public int IsQuotationRejected { get; set; }

        [DataMember(Name = "quotationRejectedComment")]
        public string QuotationRejectedComment { get; set; }

        [DataMember(Name = "PO")]
        public RequirementPO PO { get; set; }

        [DataMember(Name = "revquotationUrl")]
        public string RevQuotationUrl { get; set; }

        [DataMember(Name = "revPrice")]
        public double RevPrice { get; set; }

        [DataMember(Name = "revVendorTotalPrice")]
        public double RevVendorTotalPrice { get; set; }

        [DataMember(Name = "revVendorTotalPriceNoTax")]
        public double RevVendorTotalPriceNoTax { get; set; }
        [DataMember(Name = "vendorInitTotalPriceNoTax")]
        public double VendorInitTotalPriceNoTax { get; set; }


        [DataMember(Name = "vendorInitTax")]
        public double VendorInitTax { get; set; }
        [DataMember(Name = "revVendorInitTax")]
        public double RevVendorInitTax { get; set; }


        [DataMember(Name = "isRevQuotationRejected")]
        public int IsRevQuotationRejected { get; set; }

        [DataMember(Name = "isFromGlobalSearch")]
        public int IsFromGlobalSearch { get; set; }

        [DataMember(Name = "revQuotationRejectedComment")]
        public string RevQuotationRejectedComment { get; set; }

        [DataMember(Name = "lastActiveTime")]
        public string LastActiveTime { get; set; }

        [DataMember(Name = "discount")]
        public double Discount { get; set; }

        [DataMember(Name = "reductionPercentage")]
        public double ReductionPercentage { get; set; }

        [DataMember(Name = "otherProperties")]
        public string OtherProperties { get; set; }

        [DataMember(Name = "vendor")]
        public User Vendor { get; set; }

        [DataMember(Name = "materialDispachmentLink")]
        public int MaterialDispachmentLink { get; set; }

        [DataMember(Name = "materialReceivedLink")]
        public int MaterialReceivedLink { get; set; }

        [DataMember(Name = "landingPrice")]
        public decimal LandingPrice { get; set; }

        [DataMember(Name = "revLandingPrice")]
        public decimal RevLandingPrice { get; set; }

        [DataMember(Name = "techEvalScore")]
        public decimal TechEvalScore { get; set; }

        [DataMember(Name = "marginRankDiscount")]
        public double marginRankDiscount { get; set; }

        [DataMember(Name = "marginRankRevDiscount")]
        public double marginRankRevDiscount { get; set; }

        [DataMember(Name = "sumOfMargin")]
        public decimal SumOfMargin { get; set; }

        [DataMember(Name = "sumOfInitialMargin")]
        public decimal SumOfInitialMargin { get; set; }

        [DataMember(Name = "listRequirementItems")]
        public List<RequirementItems> ListRequirementItems { get; set; }

        [DataMember(Name = "listFwdRequirementItems")]
        public List<FwdRequirementItems> ListFwdRequirementItems { get; set; }
        
        string multipleAttachments = string.Empty;
        [DataMember(Name = "multipleAttachments")]
        public string MultipleAttachments
        {
            get
            {
                return this.multipleAttachments;
            }
            set
            {
                this.multipleAttachments = value;
            }
        }

        string gstNumber = string.Empty;
        [DataMember(Name = "gstNumber")]
        public string GstNumber
        {
            get
            {
                return this.gstNumber;
            }
            set
            {
                this.gstNumber = value;
            }
        }

        [DataMember(Name = "savingsPercentage")]
        public double savingsPercentage { get; set; }

        [DataMember(Name = "savings")]
        public double savings { get; set; }

        [DataMember(Name = "vendorCurrency")]
        public string VendorCurrency { get; set; }

        [DataMember(Name = "vendorCurrencyFactor")]
        public decimal VendorCurrencyFactor { get; set; }

        [DataMember(Name = "listCurrencies")]
        public List<KeyValuePair> ListCurrencies { get; set; }

        [DataMember(Name = "selectedVendorCurrency")]
        public string SelectedVendorCurrency { get; set; }

        [DataMember(Name = "isCurrency")]
        public bool IsCurrency { get; set; }

        [DataMember(Name = "revPriceCB")]
        public double RevPriceCB { get; set; }

        [DataMember(Name = "revVendorTotalPriceCB")]
        public double RevVendorTotalPriceCB { get; set; }

        [DataMember(Name = "totalRunningPriceCB")]
        public double TotalRunningPriceCB { get; set; }

        [DataMember(Name = "FREEZE_CB")]
        public bool FREEZE_CB { get; set; }

        [DataMember(Name = "VEND_FREEZE_CB")]
        public bool VEND_FREEZE_CB { get; set; }

        [DataMember(Name = "CB_BID_COMMENTS")]
        public string CB_BID_COMMENTS { get; set; }

        [DataMember(Name = "INCO_TERMS")]
        public string INCO_TERMS { get; set; }

        [DataMember(Name = "assignedCatalogueItems")]
        public string AssignedCatalogueItems { get; set; }

        [DataMember(Name = "BOOKED_SLOT_DATE")]
        public DateTime? BOOKED_SLOT_DATE { get; set; }

        [DataMember(Name = "IS_SLOT_APPROVED")]
        public int IS_SLOT_APPROVED { get; set; }

        [DataMember(Name = "SLOT_ID")]
        public int SLOT_ID { get; set; }

        [DataMember(Name = "BASE_PRICE")]
        public double BASE_PRICE { get; set; }

        [DataMember(Name = "SLOT_START_DATE")]
        public DateTime? SLOT_START_DATE { get; set; }

        [DataMember(Name = "SLOT_END_DATE")]
        public DateTime? SLOT_END_DATE { get; set; }

        [DataMember(Name = "SLOT_NAME")]
        public string SLOT_NAME { get; set; }

        [DataMember(Name = "technicalScore")]
        public decimal TechnicalScore { get; set; }

        [DataMember(Name = "WF_ID")]
        public int WF_ID { get; set; }

        [DataMember(Name = "isVendAckChecked")]
        public string IsVendAckChecked { get; set; }

        [DataMember(Name = "auctionModifiedDate")]
        public DateTime? AuctionModifiedDate { get; set; }

        [DataMember(Name = "DIFFERENTIAL_FACTOR")]
        public decimal DIFFERENTIAL_FACTOR { get; set; }

        [DataMember(Name = "DF_REV_VENDOR_TOTAL_PRICE")]
        public decimal DF_REV_VENDOR_TOTAL_PRICE { get; set; }

        [DataMember(Name = "DF_REV_VENDOR_TOTAL_PRICE_NO_TAX")]
        public decimal DF_REV_VENDOR_TOTAL_PRICE_NO_TAX { get; set; }

        [DataMember(Name = "IS_QUOT_LATE")]
        public int IS_QUOT_LATE { get; set; }

        [DataMember(Name = "paymentTermCode")]
        public string PaymentTermCode
        {
            get; set;
        }


        [DataMember(Name = "paymentTermDesc")]
        public string PaymentTermDesc
        {
            get; set;
        }

        [DataMember(Name = "surrogateComments")]
        public string SurrogateComments
        {
            get; set;
        }

        [DataMember(Name = "VENDOR_PLANT_CODE")]
        public string VENDOR_PLANT_CODE
        {
            get; set;
        }


        string firstName = string.Empty;
        [DataMember(Name = "firstName")]
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    firstName = value;
                }
            }
        }

        string lastName = string.Empty;
        [DataMember(Name = "lastName")]
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    lastName = value;
                }
            }
        }

        [DataMember(Name = "regStatus")]
        public int Status { get; set; }
    }
}