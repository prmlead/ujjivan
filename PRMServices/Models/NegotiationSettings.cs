﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class NegotiationSettings : Entity
    {
        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "minReductionAmount")]
        public double MinReductionAmount { get; set; }

        [DataMember(Name = "rankComparision")]
        public double RankComparision { get; set; }

        [DataMember(Name = "negotiationDuration")]
        public string NegotiationDuration { get; set; }

        [DataMember(Name = "compInterest")]
        public Decimal CompInterest { get; set; }

        [DataMember(Name = "fromDate")]
        public DateTime? FromDate { get; set; }

        [DataMember(Name = "toDate")]
        public DateTime? ToDate { get; set; }

        [DataMember(Name = "bestPricefromDate")]
        public DateTime? BestPriceFromDate { get; set; }

        [DataMember(Name = "bestPricetoDate")]
        public DateTime? BestPriceToDate { get; set; }

        [DataMember(Name = "disableLocalGSTFeature")]
        public bool DisableLocalGSTFeature { get; set; }
    }
}