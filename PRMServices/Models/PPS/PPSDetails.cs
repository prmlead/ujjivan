﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class PPSDetails : Entity
    {

        [DataMember] [DataNames("PPS_ID")] public int PPS_ID { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("CATALOGUE_ID")] public int CATALOGUE_ID { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("PPS_FIELDS_JSON")] public string ppsFieldsJson { get; set; }
        [DataMember] [DataNames("PPS_FIELDS_PO_JSON")] public string ppsFieldsPOJson { get; set; }
        [DataMember] [DataNames("ProductNo")] public string PRODUCT_NUM { get; set; }

        [DataMember] [DataNames("DEPT_ID")] public int DEPT_ID { get; set; }

        [DataMember] [DataNames("REQ_TITLE")] public string REQ_TITLE { get; set; }
        [DataMember] [DataNames("ITEM_NAME")] public string ITEM_NAME { get; set; }

        [DataMember] [DataNames("PO_SELECTED_VENDOR_ID")] public int PO_SELECTED_VENDOR_ID { get; set; }
        [DataMember] [DataNames("PO_SELECTED_ITEM_ID")] public int PO_SELECTED_ITEM_ID { get; set; }
        [DataMember] [DataNames("PPS_STATUS")] public string PPS_STATUS { get; set; }

        [DataMember] [DataNames("ACCESSLEVEL_ID")] public int ACCESSLEVEL_ID { get; set; }

        [DataMember] [DataNames("CLONE_ID")] public int CLONE_ID { get; set; }

        [DataMember] [DataNames("ITEM_ID")] public int ITEM_ID { get; set; }

        [DataMember] [DataNames("APPROVER")] public string APPROVER { get; set; }
        [DataMember] [DataNames("CREATOR")] public string CREATOR { get; set; }

        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }

        [DataMember] [DataNames("IS_SELECTED_L1")] public int IS_SELECTED_L1 { get; set; }

    }
}