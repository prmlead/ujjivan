﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Models.RealTimePrice
{
    public class RealTimePriceAllowProductModel
    {
        public string Product { get; set; }

        public bool Active { get; set; }

        public decimal ThresholdValue { get; set; }
    }
}