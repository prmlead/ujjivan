﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models.Vendor
{
    [DataContract]
    public class VendorScoreUpdateModel
    {
        [DataMember]
        public int User_Id { get; set; }

        [DataMember]
        public int Score { get; set; }

        [DataMember]
        public List<VendorScoreModel> Scores { get; set; }
    }
}