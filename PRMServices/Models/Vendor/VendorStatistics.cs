﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRM.Core.Common;

namespace PRMServices.Models.Vendor
{
    [DataContract]
    public class VendorStatistics
    {
        [DataMember]
        [DataNames("PARTICIPATION")]
        public string PARTICIPATION { get; set; }

        [DataMember]
        [DataNames("RESPONSE_TIME")]
        public string RESPONSE_TIME { get; set; }

        [DataMember]
        [DataNames("ACCURACY")]
        public string ACCURACY { get; set; }

        [DataMember]
        [DataNames("STRIKE")]
        public string STRIKE { get; set; }

        [DataMember]
        [DataNames("STRIKE_PO")]
        public string STRIKE_PO { get; set; }

        [DataMember]
        [DataNames("AVG_PRICE_REDUCTION")]
        public string AVG_PRICE_REDUCTION { get; set; }

        [DataMember]
        [DataNames("PARTICIPATION_ALL")]
        public string PARTICIPATION_ALL { get; set; }

        [DataMember]
        [DataNames("RESPONSE_TIME_ALL")]
        public string RESPONSE_TIME_ALL { get; set; }

        [DataMember]
        [DataNames("AVG_PRICE_REDUCTION_ALL")]
        public string AVG_PRICE_REDUCTION_ALL { get; set; }

        [DataMember]
        [DataNames("ACCURACY_ALL")]
        public string ACCURACY_ALL { get; set; }

        [DataMember]
        [DataNames("STRIKE_ALL")]
        public string STRIKE_ALL { get; set; }

        [DataMember]
        [DataNames("STRIKE_PO_ALL")]
        public string STRIKE_PO_ALL { get; set; }

        [DataMember]
        [DataNames("IS_PO_ENABLED")]
        public int IS_PO_ENABLED { get; set; }

        [DataMember]
        [DataNames("TOTAL_REQUIREMENTS")]
        public int TOTAL_REQUIREMENTS { get; set; }

        [DataMember]
        [DataNames("TOTAL_REQUIREMENTS_AMOUNT")]
        public decimal TOTAL_REQUIREMENTS_AMOUNT { get; set; }
    }
}