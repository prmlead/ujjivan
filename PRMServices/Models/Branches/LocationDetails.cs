﻿using PRM.Core.Common;
using System;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class LocationDetails : Entity
    {
        [DataMember] [DataNames("LOCATION_ID")] public int LOCATION_ID { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("LOCATION_CODE")] public string LOCATION_CODE { get; set; }
        [DataMember] [DataNames("LOCATION_NAME")] public string LOCATION_NAME { get; set; }
        [DataMember] [DataNames("LOCATION_ADDR")] public string LOCATION_ADDR { get; set; }
        [DataMember] [DataNames("LOCATION_TYPE")] public string LOCATION_TYPE { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime DATE_CREATED { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime DATE_MODIFIED { get; set; }
    }
}
