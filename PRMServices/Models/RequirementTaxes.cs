﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class RequirementTaxes : Entity
    {
        [DataMember(Name = "taxID")]
        public int TaxID { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        string taxName = string.Empty;
        [DataMember(Name = "taxName")]
        public string TaxName
        {
            get
            {
                if (!string.IsNullOrEmpty(taxName))
                { return taxName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { taxName = value; }
            }
        }

        [DataMember(Name = "taxPercentage")]
        public double TaxPercentage { get; set; }

        [DataMember(Name = "isTaxDeleted")]
        public int IsTaxDeleted { get; set; }

        [DataMember(Name = "taxSNo")]
        public int TaxSNo { get; set; }
    }
}