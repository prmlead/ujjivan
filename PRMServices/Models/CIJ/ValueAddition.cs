﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class ValueAddition : Entity
    {
        [DataMember(Name = "costReduction")]
        public int CostReduction { get; set; }

        [DataMember(Name = "orptEfficiency")]
        public int OrptEfficiency { get; set; }

        [DataMember(Name = "inceraseINEmployee")]
        public int InceraseINEmployee { get; set; }
        
        [DataMember(Name = "reqtByName")]
        public int ReqtByName { get; set; }
        
        [DataMember(Name = "recomByName")]
        public int RecomByName { get; set; }

        [DataMember(Name = "reqtBySign")]
        public int ReqtBySign { get; set; }

        [DataMember(Name = "recomBySign")]
        public int RecomBySign { get; set; }
        
        [DataMember(Name = "reqtByDate")]
        public int ReqtByDate { get; set; }

        [DataMember(Name = "recomByDate")]
        public int RecomByDate { get; set; }

    }
}