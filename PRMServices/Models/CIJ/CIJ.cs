﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class CIJ : Entity
    {

        [DataMember(Name = "atMedical")]
        public int AtMedical { get; set; }

        [DataMember(Name = "atNonMedical")]
        public int AtNonMedical { get; set; }

        [DataMember(Name = "atRevenu")]
        public string AtRevenu { get; set; }

        [DataMember(Name = "atValueAdd")]
        public string AtValueAdd { get; set; }

        [DataMember(Name = "deptName")]
        public string DeptName { get; set; }

        [DataMember(Name = "hodName")]
        public bool HodName { get; set; }

        [DataMember(Name = "purpose")]
        public int Purpose { get; set; }

        [DataMember(Name = "specifications")]
        public int Specifications { get; set; }

        [DataMember(Name = "need")]
        public int Need { get; set; }

        [DataMember(Name = "purposedSuppliers")]
        public int PurposedSuppliers { get; set; }

        [DataMember(Name = "noOfExisting")]
        public int NoOfExisting { get; set; }

        [DataMember(Name = "approxCost")]
        public int ApproxCost { get; set; }        

    }
}