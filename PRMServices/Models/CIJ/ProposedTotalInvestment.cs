﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class ProposedTotalInvestment : Entity
    {

        [DataMember(Name = "basicEquipmentCost")]
        public int BasicEquipmentCost { get; set; }

        [DataMember(Name = "accessoriesCost")]
        public int AccessoriesCost { get; set; }

        [DataMember(Name = "installationCost")]
        public string InstallationCost { get; set; }

        [DataMember(Name = "anyOtherCost")]
        public string AnyOtherCost { get; set; }

        [DataMember(Name = "totalCost")]
        public string TotalCost { get; set; }
        
    }
}