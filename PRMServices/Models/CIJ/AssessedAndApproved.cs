﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class AssessedAndApproved : Entity
    {
        [DataMember(Name = "name")]
        public int Name { get; set; }

        [DataMember(Name = "signature")]
        public int Signature { get; set; }

        [DataMember(Name = "date")]
        public string Date { get; set; }
    }
}