﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models

{
    [DataContract]
    public class OpsVendorDetails : Entity 
    {
       

        [DataMember(Name = "u_ID")]
        public int U_ID { get; set; }

        [DataMember(Name = "isPrimary")]
        public int IsPrimary { get; set; }

        [DataMember(Name = "company_Name")]
        public string COMPANY_NAME { get; set; }

        
        [DataMember(Name = "vendorName")]
        public string VendorName { get; set; }

        [DataMember(Name = "u_PHONE")]
        public string U_PHONE { get; set; }

        [DataMember(Name = "u_EMAIL")]
        public string U_EMAIL { get; set; }

        [DataMember(Name = "u_ALTPHONE")]
        public string U_ALTPHONE { get; set; }

       

        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "vendorPhone")]
        public string VendorPhone { get; set; }

        [DataMember(Name = "vendorAtlMail")]
        public string VendorAtlMail { get; set; }
    }
}