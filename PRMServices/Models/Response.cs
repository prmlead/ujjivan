﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace PRMServices.Models
{
    [DataContract]
    public class Response : Entity
    {
        int objectId = 0;
        [DataMember(Name = "objectID")]
        public int ObjectID
        {
            get
            {
                return this.objectId;
            }
            set
            {
                this.objectId = value;
            }
        }

        string msg = string.Empty;
        [DataMember(Name = "message")]
        public string Message
        {
            get
            {
                return this.msg;
            }
            set
            {
                this.msg = value;
            }
        }

        string category = string.Empty;
        [DataMember(Name = "category")]
        public string Category
        {
            get
            {
                return this.category;
            }
            set
            {
                this.category = value;
            }
        }

        string status = string.Empty;
        [DataMember(Name = "status")]
        public string Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.status = value;
            }
        }


        string purchasedoc = string.Empty;
        [DataMember(Name = "purchasedoc")]
        public string PURCHASEDOC
        {
            get
            {
                return this.purchasedoc;
            }
            set
            {
                this.purchasedoc = value;
            }
        }


        string itemno = string.Empty;
        [DataMember(Name = "itemno")]
        public string ITEMNO
        {
            get
            {
                return this.itemno;
            }
            set
            {
                this.itemno = value;
            }
        }

        string remarksType = string.Empty;
        [DataMember(Name = "remarksType")]
        public string RemarksType
        {
            get
            {
                return this.remarksType;
            }
            set
            {
                this.remarksType = value;
            }
        }

        [DataMember(Name = "userInfo")]
        public UserInfo UserInfo
        {
            get;
            set;
        }

        string prItemNo = string.Empty;
        [DataMember(Name = "prItemNo")]
        public string PR_ITM_NO
        {
            get
            {
                return this.prItemNo;
            }
            set
            {
                this.prItemNo = value;
            }
        }

        string prNo = string.Empty;
        [DataMember(Name = "prNo")]
        public string PR_NUMBER
        {
            get
            {
                return this.prNo;
            }
            set
            {
                this.prNo = value;
            }
        }

        long tLeft = 0;
        [DataMember(Name = "timeLeft")]
        public long TimeLeft
        {
            get
            {
                return this.tLeft;
            }
            set
            {
                this.tLeft = value;
            }
        }



        int roleID = 0;
        [DataMember(Name = "roleID")]
        public int RoleID
        {
            get
            {
                return this.roleID;
            }
            set
            {
                this.roleID = value;
            }
        }

        string userHashToken = string.Empty;
        [DataMember(Name = "userHashToken")]
        public string UserHashToken
        {
            get
            {
                return this.userHashToken;
            }
            set
            {
                this.userHashToken = value;
            }
        }

        string jwtToken = string.Empty;
        [DataMember(Name = "jwtToken")]
        public string JWTToken
        {
            get
            {
                return this.jwtToken;
            }
            set
            {
                this.jwtToken = value;
            }
        }

        int iD = 0;
        [DataMember(Name = "iD")]
        public int ID
        {
            get
            {
                return this.iD;
            }
            set
            {
                this.iD = value;
            }
        }

    }
}