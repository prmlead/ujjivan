﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class SignalRObj : ResponseAudit
    {
        [DataMember(Name = "inv")]
        public InvolvedParties Inv
        {
            get; set;
        }

        [DataMember(Name = "payLoad")]
        public object[] PayLoad { get; set; }
    }
}