﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class APMCAudit : Entity
    {
        [DataMember(Name = "apmcNegotiationID")]
        public int APMCNegotiationID { get; set; }

        [DataMember(Name = "vendorPrice")]
        public double VendorPrice { get; set; }

        [DataMember(Name = "customerPrice")]
        public double CustomerPrice { get; set; }
        
        [DataMember(Name ="created")]
        public DateTime? Created { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; internal set; }

        [DataMember(Name = "vendorID")]
        public int VendorID { get; internal set; }

        [DataMember(Name = "isFrozen")]
        public int IsFrozen { get; internal set; }
    }
}