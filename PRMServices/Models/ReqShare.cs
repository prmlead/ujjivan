﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class ReqShare : Entity
    {
        [DataMember(Name = "requirement")]
        public Requirement Requirement { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "expectedBidding")]
        public string ExpectedBidding { get; set; }

        [DataMember(Name = "minReductionAmount")]
        public double MinReductionAmount { get; set; }

        [DataMember(Name = "vendorRankComparision")]
        public double VendorRankComparision { get; set; }

        [DataMember(Name = "negotiationDuration")]
        public string NegotiationDuration { get; set; }

        [DataMember(Name = "quotationFreezTime")]
        public string QuotationFreezTime { get; set; }

    }
}