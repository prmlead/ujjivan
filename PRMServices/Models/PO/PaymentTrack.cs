﻿
using System.Collections.Generic;
using System.Runtime.Serialization;
using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class PaymentTrack : ResponseAudit
    {
        [DataMember(Name = "vendor")]
        public UserDetails Vendor { get; set; }

        [DataMember(Name = "req")]
        public Requirement Req { get; set; }

        [DataMember(Name = "po")]
        public POItems PO { get; set; }

        [DataMember(Name = "dispatchObject")]
        public List<DispatchTrack> DispatchObject { get; set; }

        [DataMember(Name = "paymentInfoObject")]
        public List<PaymentInfo> PaymentInfoObject { get; set; }
    }
}