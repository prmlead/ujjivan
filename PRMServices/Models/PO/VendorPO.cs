﻿
using System.Collections.Generic;
using System.Runtime.Serialization;
using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class VendorPO : ResponseAudit
    {
        [DataMember(Name = "vendor")]
        public UserDetails Vendor { get; set; }

        [DataMember(Name = "poFile")]
        public FileUpload POFile { get; set; }

        [DataMember(Name = "req")]
        public Requirement Req { get; set; }

        [DataMember(Name = "listPOItems")]
        public List<POItems> ListPOItems { get; set; }

        string poLink = string.Empty;
        [DataMember(Name = "poLink")]
        public string POLink
        {
            get
            {
                if (!string.IsNullOrEmpty(poLink))
                { return poLink; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { poLink = value; }
            }
        }

        [DataMember(Name = "common")]
        public bool Common { get; set; }


        string purchaseOrderID = string.Empty;
        [DataMember(Name = "purchaseOrderID")]
        public string PurchaseOrderID
        {
            get
            {
                if (!string.IsNullOrEmpty(purchaseOrderID))
                { return purchaseOrderID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { purchaseOrderID = value; }
            }
        }
    }


    /*string poStatus = string.Empty;
        [DataMember(Name = "poStatus")]
        public string PoStatus
        {
            get
            {
                if (!string.IsNullOrEmpty(poStatus))
                { return poStatus; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { poStatus = value; }
            }
        }


        [DataMember(Name = "TOTAL_COUNT")]
        public int TOTAL_COUNT { get; set; }
*/
}
 