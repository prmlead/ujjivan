﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class Company : Entity
    {
        [DataMember(Name = "companyID")]
        public int CompanyID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "superUserID")]
        public int SuperUserID { get; set; }

        string companyName = string.Empty;
        [DataMember(Name = "companyName")]
        public string CompanyName
        {
            get
            {
                return companyName;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    companyName = value;
                }
            }
        }

        [DataMember(Name = "sendEmails")]
        public int SendEmails { get; set; }

    }
}