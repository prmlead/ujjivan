﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class Register : Entity
    {
        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "phoneNum")]
        public string PhoneNum { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "category")]
        public string Category { get; set; }

        [DataMember(Name = "panNumber")]
        public string PanNumber { get; set; }

        [DataMember(Name = "vatNumber")]
        public string VatNumber { get; set; }

        [DataMember(Name = "stnNumber")]
        public string StnNumber { get; set; }

        [DataMember(Name = "knownSince")]
        public string KnownSince { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "department")]
        public string Department { get; set; }

        [DataMember(Name = "userType")]
        public string UserType { get; set; }

        [DataMember(Name="referringUserID")]
        public int ReferringUserID { get; set; }

        [DataMember(Name = "subcategories")]
        public int[] Subcategories { get; set; }

        [DataMember(Name = "currency")]
        public int Currency { get; set; }

        [DataMember(Name = "timeZone")]
        public int TimeZone { get; set; }


        string altEmail = string.Empty;
        [DataMember(Name = "altEmail")]
        public string AltEmail
        {
            get
            {
                if (!string.IsNullOrEmpty(altEmail))
                {
                    return altEmail;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    altEmail = value;
                }
            }
        }

        string altPhoneNum = string.Empty;
        [DataMember(Name = "altPhoneNum")]
        public string AltPhoneNum
        {
            get
            {
                if (!string.IsNullOrEmpty(altPhoneNum))
                {
                    return altPhoneNum;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    altPhoneNum = value;
                }
            }
        }


        [DataMember(Name = "additionalData")]
        public string AdditionalData { get; set; }

        [DataMember(Name = "attachments")]
        public string Attachments { get; set; }

        //[DataMember(Name = "vendorCode")]
        //public string VendorCode { get; set; }

        [DataMember(Name = "isAdmin")]
        public int IsAdmin { get; set; }



        //REGISTRATION//
        [DataMember(Name = "Address1")]
        public string Address1 { get; set; }

        [DataMember(Name = "Street1")]
        public string Street1 { get; set; }

        [DataMember(Name = "Street2")]
        public string Street2 { get; set; }

        [DataMember(Name = "Street3")]
        public string Street3 { get; set; }

        [DataMember(Name = "Country")]
        public string Country { get; set; }

        [DataMember(Name = "State")]
        public string State { get; set; }

        [DataMember(Name = "PinCode")]
        public string PinCode { get; set; }

        [DataMember(Name = "City")]
        public string City { get; set; }

        [DataMember(Name = "BankName")]
        public string BankName { get; set; }

        [DataMember(Name = "BankAddress")]
        public string BankAddress { get; set; }

        [DataMember(Name = "accntNumber")]
        public string accntNumber { get; set; }

        [DataMember(Name = "Ifsc")]
        public string Ifsc { get; set; }

        [DataMember(Name = "cancelledCheque")]
        public string cancelledCheque { get; set; }

        [DataMember(Name = "altMobile")]
        public string altMobile { get; set; }

        [DataMember(Name = "landLine")]
        public string landLine { get; set; }

        [DataMember(Name = "salesPerson")]
        public string salesPerson { get; set; }

        [DataMember(Name = "salesContact")]
        public string salesContact { get; set; }
        //salesContact

        [DataMember(Name = "isSelf")]
        public bool IsSelf { get; set; }

        [DataMember(Name = "phoneOrEmail")]
        public string phoneOrEmail { get; set; }

        [DataMember(Name = "gstAttach")]
        public string GstAttach { get; set; }

        [DataMember(Name = "gstAttachments")]
        public byte[] GstAttachments { get; set; }

        [DataMember(Name = "gstAttachmentName")]
        public string GstAttachmentName { get; set; }


        [DataMember(Name = "ccAttach")]
        public string CcAttach { get; set; }

        [DataMember(Name = "cancelledchequeattachments")]
        public byte[] Cancelledchequeattachments { get; set; }

        [DataMember(Name = "cancelledchequeattachmentName")]
        public string CancelledchequeattachmentName { get; set; }


        [DataMember(Name = "panAttach")]
        public string PanAttach { get; set; }

        [DataMember(Name = "panAttachments")]
        public byte[] PanAttachments { get; set; }

        [DataMember(Name = "panAttachmentName")]
        public string PanAttachmentName { get; set; }


        [DataMember(Name = "businessAttach")]
        public string BussAttach { get; set; }

        [DataMember(Name = "businessAttachments")]
        public byte[] BussAttachments { get; set; }

        [DataMember(Name = "businessAttachmentName")]
        public string BussAttachmentName { get; set; }

        [DataMember(Name = "catalogCatId")]
        public string CatalogCatId { get; set; }

        [DataMember(Name = "BussinessDet")]
        public string BussinessDet { get; set; }

        [DataMember(Name = "msmeAttach")]
        public string MsmeAttach { get; set; }

        [DataMember(Name = "msmeAttachments")]
        public byte[] MsmeAttachments { get; set; }

        [DataMember(Name = "msmeAttachmentName")]
        public string MsmeAttachmentName { get; set; }


        string userLoginID = string.Empty;
        [DataMember(Name = "userLoginID")]
        public string UserLoginID
        {
            get
            {
                if (!string.IsNullOrEmpty(userLoginID))
                {
                    return userLoginID;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    userLoginID = value;
                }
            }
        }



        string userPassword = string.Empty;
        [DataMember(Name = "userPassword")]
        public string UserPassword
        {
            get
            {
                if (!string.IsNullOrEmpty(userPassword))
                {
                    return userPassword;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    userPassword = value;
                }
            }
        }


        [DataMember(Name = "isSubUser")]
        public int IsSubUser { get; set; }


        [DataMember(Name = "sapVendorCode")]
        public string SapVendorCode { get; set; }


        string isEditUser = string.Empty;
        [DataMember(Name = "isEditUser")]
        public string IsEditUser
        {
            get
            {
                if (!string.IsNullOrEmpty(isEditUser))
                {
                    return isEditUser;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    isEditUser = value;
                }
            }
        }

        [DataMember(Name = "validityFrom")]
        public DateTime? ValidityFrom { get; set; }

        [DataMember(Name = "validityTo")]
        public DateTime? ValidityTo { get; set; }


        [DataMember(Name = "additionalVendorInformation")]
        public string AdditionalVendorInformation { get; set; }

        [DataMember(Name = "customerCopyLetterAttach")]
        public string CustomerCopyLetterAttach { get; set; }

        [DataMember(Name = "customerCopyLetterAttachments")]
        public byte[] CustomerCopyLetterAttachments { get; set; }

        [DataMember(Name = "customerCopyLetterAttachmentName")]
        public string CustomerCopyLetterAttachmentName { get; set; }

        [DataMember(Name = "vendorStatus")]
        public int VendorStatus { get; set; }

        [DataMember(Name = "sapUserId")]
        public string SAPUserId { get; set; }

        [DataMember(Name = "oldEmail")]
        public string OldEmail { get; set; }

        [DataMember(Name = "oldPhone")]
        public string OldPhone { get; set; }

        [DataMember(Name = "paymentTermCode")]
        public string PaymentTermCode { get; set; }
        [DataMember(Name = "paymentTermDesc")]
        public string PaymentTermDesc { get; set; }
        [DataMember(Name = "incoTerm")]
        public string IncoTerm { get; set; }

        string userLocation = string.Empty;
        [DataMember(Name = "userLocation")]
        public string UserLocation
        {
            get
            {
                if (!string.IsNullOrEmpty(userLocation))
                {
                    return userLocation;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    userLocation = value;
                }
            }
        }

        [DataMember(Name = "mpnCode")]
        public string MpnCode { get; set; }

        [DataMember(Name = "deliveryDays")]
        public string DeliveryDays { get; set; }
        [DataMember(Name = "vendorPurchaseOrgData")]
        public string VendorPurchaseOrgData { get; set; }
        [DataMember(Name = "manufacturerCode")]
        public string ManufacturerCode { get; set; }
        [DataMember(Name = "manufacturerDivision")]
        public string ManufacturerDivision { get; set; }
        [DataMember(Name = "drugLicenseNo")]
        public string DrugLicenseNo { get; set; }
        [DataMember(Name = "registered")]
        public string Registered { get; set; }
        [DataMember(Name = "plantcode")]
        public string Plantcode { get; set; }

        [DataMember(Name = "RH_ID")]
        public int RH_ID { get; set; }

        [DataMember(Name = "LOCATION_ID")]
        public int LOCATION_ID { get; set; }
        //REGISTRATION//
    }
}