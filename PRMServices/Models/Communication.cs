﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class Communication : Entity
    {

        [DataMember(Name = "user")]
        public User User { get; set; }

        [DataMember(Name = "company")]
        public Company Company { get; set; }

        [DataMember(Name = "fromAdd")]
        public string FromAdd { get; set; }

        [DataMember(Name = "displayName")]
        public string DisplayName { get; set; }

        [DataMember(Name = "companyAddress")]
        public string CompanyAddress { get; set; }

        [DataMember(Name = "timeZoneId")]
        public string TimeZoneId { get; set; }
    }
}