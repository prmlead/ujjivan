﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class MACRequirement : Entity
    {
        [DataMember(Name = "reqItems")]
        public MACRequirementItems[] ReqItems { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "reqVendors")]
        public MACVendorDetails[] ReqVendors { get; set; }

        [DataMember(Name = "customerID")]
        public int CustomerID { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "postedByFirstName")]
        public string PostedByFirstName { get; set; }

        [DataMember(Name = "postedByLastName")]
        public string PostedByLastName { get; set; }

        string title = string.Empty;
        [DataMember(Name = "title")]
        public string Title
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                { return title; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { title = value; }
            }
        }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "postedOn")]
        public DateTime? PostedOn { get; set; }

        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }

        [DataMember(Name = "savings")]
        public Double Savings { get; set; }

        [DataMember(Name = "currentDate")]
        public DateTime? CurrentDate { get; set; }

        [DataMember(Name = "preSavings")]
        public Double PreSavings { get; set; }

        [DataMember(Name = "postSavings")]
        public Double PostSavings { get; set; }

        [DataMember(Name = "IS_CB_ENABLED")]
        public int IS_CB_ENABLED { get; set; }

        [DataMember(Name = "IS_CB_COMPLETED")]
        public int IS_CB_COMPLETED { get; set; }

    }
}