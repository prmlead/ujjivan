﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class ReqReportForExcel : ResponseAudit
    {
        [DataMember(Name = "reqItems")]
        public List<ExcelRequirementItems> ReqItems { get; set; }        
    }
}