﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class Credentials : Entity
    {
        [DataMember(Name = "fileLink")]
        public string FileLink { get; set; }

        [DataMember(Name = "fileType")]
        public string FileType { get; set; }

        [DataMember(Name = "isVerified")]
        public int IsVerified { get; set; }

        [DataMember(Name = "credentialID")]
        public string CredentialID { get; set; }
    }
}