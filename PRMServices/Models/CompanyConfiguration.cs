﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class CompanyConfiguration : Entity
    {

        [DataMember(Name = "compConfigID")]
        public int CompConfigID { get; set; }

        [DataMember(Name = "FIELD_PK_ID")]
        public int FIELD_PK_ID { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "configKey")]
        public string ConfigKey { get; set; }

        [DataMember(Name = "configValue")]
        public string ConfigValue { get; set; }

        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "configText")]
        public string ConfigText { get; set; }

        [DataMember(Name = "configType")]
        public string ConfigType { get; set; }

        [DataMember(Name = "configValue1")]
        public string ConfigValue1 { get; set; }

        [DataMember(Name = "configValue2")]
        public string ConfigValue2 { get; set; }

        [DataMember(Name = "regName")]
        public string RegName { get; set; }

        [DataMember(Name = "branchName")]
        public string BranchName { get; set; }

        [DataMember(Name = "locationName")]
        public string LocationName { get; set; }

        [DataMember(Name = "rbType")]
        public string RBType { get; set; }

        [DataMember(Name = "createdBy")]
        public int CreatedBy { get; set; }

        [DataMember(Name = "modifiedBy")]
        public int ModifiedBy { get; set; }

        [DataMember(Name = "dateCreated")]
        public DateTime? DateCreated { get; set; }

        [DataMember(Name = "dateModified")]
        public DateTime? DateModified { get; set; }

        [DataMember(Name = "rbID")]
        public int RbID { get; set; }

        [DataMember(Name = "regionID")]
        public int RegionID { get; set; }

        [DataMember(Name = "branchID")]
        public int BranchID { get; set; }

        [DataMember(Name = "delvLocationID")]
        public int DelvLocationID { get; set; }

        [DataMember(Name = "mrbID")]
        public int MrbID { get; set; }
        
    }
}