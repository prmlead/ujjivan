﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRMServices.Models.Catalog;

namespace PRMServices.Models
{
    [DataContract]
    public class RequirementItems : Entity
    {
        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        string productIDorName = string.Empty;
        [DataMember(Name = "productIDorName")]
        public string ProductIDorName
        {
            get
            {
                if (!string.IsNullOrEmpty(productIDorName))
                { return productIDorName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productIDorName = value; }
            }
        }

        string productIDorNameCustomer = string.Empty;
        [DataMember(Name = "productIDorNameCustomer")]
        public string ProductIDorNameCustomer
        {
            get
            {
                if (!string.IsNullOrEmpty(productIDorNameCustomer))
                { return productIDorNameCustomer; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productIDorNameCustomer = value; }
            }
        }

        string productNo = string.Empty;
        [DataMember(Name = "productNo")]
        public string ProductNo
        {
            get
            {
                if (!string.IsNullOrEmpty(productNo))
                { return productNo; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productNo = value; }
            }
        }

        string productNoCustomer = string.Empty;
        [DataMember(Name = "productNoCustomer")]
        public string ProductNoCustomer
        {
            get
            {
                if (!string.IsNullOrEmpty(productNoCustomer))
                { return productNoCustomer; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productNoCustomer = value; }
            }
        }

        string productDescription = string.Empty;
        [DataMember(Name = "productDescription")]
        public string ProductDescription
        {
            get
            {
                if (!string.IsNullOrEmpty(productDescription))
                { return productDescription; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productDescription = value; }
            }
        }

        string productDescriptionCustomer = string.Empty;
        [DataMember(Name = "productDescriptionCustomer")]
        public string ProductDescriptionCustomer
        {
            get
            {
                if (!string.IsNullOrEmpty(productDescriptionCustomer))
                { return productDescriptionCustomer; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productDescriptionCustomer = value; }
            }
        }

        string productBrand = string.Empty;
        [DataMember(Name = "productBrand")]
        public string ProductBrand
        {
            get
            {
                if (!string.IsNullOrEmpty(productBrand))
                { return productBrand; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productBrand = value; }
            }
        }

        string productBrandCustomer = string.Empty;
        [DataMember(Name = "productBrandCustomer")]
        public string ProductBrandCustomer
        {
            get
            {
                if (!string.IsNullOrEmpty(productBrandCustomer))
                { return productBrandCustomer; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productBrandCustomer = value; }
            }
        }

        string othersBrands = string.Empty;
        [DataMember(Name = "othersBrands")]
        public string OthersBrands
        {
            get
            {
                if (!string.IsNullOrEmpty(othersBrands))
                { return othersBrands; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { othersBrands = value; }
            }
        }

        [DataMember(Name = "isDeleted")]
        public int IsDeleted { get; set; }
       

        [DataMember(Name = "productQuantity")]
        public double ProductQuantity { get; set; }

        string productQuantityIn = string.Empty;
        [DataMember(Name = "productQuantityIn")]
        public string ProductQuantityIn
        {
            get
            {
                if (!string.IsNullOrEmpty(productQuantityIn))
                { return productQuantityIn; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productQuantityIn = value; }
            }
        }

        [DataMember(Name = "productImageID")]
        public int ProductImageID { get; set; }

        [DataMember(Name = "createdDate")]
        public DateTime? CreatedDate { get; set; }

        string attachmentName = string.Empty;
        [DataMember(Name = "attachmentName")]
        public string AttachmentName
        {
            get
            {
                if (!string.IsNullOrEmpty(attachmentName))
                { return attachmentName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { attachmentName = value; }
            }
        }

        string attachmentBase64 = string.Empty;
        [DataMember(Name = "attachmentBase64")]
        public string AttachmentBase64
        {
            get
            {
                if (!string.IsNullOrEmpty(attachmentBase64))
                { return attachmentBase64; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { attachmentBase64 = value; }
            }
        }

        [DataMember(Name = "itemAttachment")]
        public byte[] ItemAttachment { get; set; }

        [DataMember(Name = "quotation")]
        public byte[] Quotation { get; set; }

        [DataMember(Name = "fileType")]
        public string FileType { get; set; }

        [DataMember(Name = "fileId")]
        public int FileId { get; set; }

        [DataMember(Name = "itemPrice")]
        public double ItemPrice { get; set; }

        [DataMember(Name = "ceilingPrice")]
        public double CeilingPrice { get; set; }

        [DataMember(Name = "tax")]
        public double Tax { get; set; }

        [DataMember(Name = "productSNo")]
        public int ProductSNo { get; set; }

        [DataMember(Name = "selectedVendorID")]
        public int SelectedVendorID { get; set; }

        [DataMember(Name = "revisedItemPrice")]
        public double RevisedItemPrice { get; set; }

        [DataMember(Name = "isRevised")]
        public int IsRevised { get; set; }

        [DataMember(Name = "revitemPrice")]
        public double RevItemPrice { get; set; }

        [DataMember(Name = "unitPrice")]
        public double UnitPrice { get; set; }

        [DataMember(Name = "revUnitPrice")]
        public double RevUnitPrice { get; set; }

        [DataMember(Name = "cGst")]
        public double CGst { get; set; }

        [DataMember(Name = "sGst")]
        public double SGst { get; set; }

        [DataMember(Name = "iGst")]
        public double IGst { get; set; }

        [DataMember(Name = "Gst")]
        public double Gst { get; set; }

        [DataMember(Name = "unitMRP")]
        public double UnitMRP { get; set; }

        [DataMember(Name = "unitDiscount")]
        public double UnitDiscount { get; set; }

        [DataMember(Name = "revUnitDiscount")]
        public double RevUnitDiscount { get; set; }

        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "costPrice")]
        public double CostPrice { get; set; }

        [DataMember(Name = "netPrice")]
        public double NetPrice { get; set; }

        [DataMember(Name = "marginAmount")]
        public double MarginAmount { get; set; }

        [DataMember(Name = "itemMinReduction")]
        public double ItemMinReduction { get; set; }

        string vendorRemarks = string.Empty;
        [DataMember(Name = "vendorRemarks")]
        public string VendorRemarks
        {
            get
            {
                if (!string.IsNullOrEmpty(vendorRemarks))
                { return vendorRemarks; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { vendorRemarks = value; }
            }
        }


        string regretComments = string.Empty;
        [DataMember(Name = "regretComments")]
        public string RegretComments
        {
            get
            {
                if (!string.IsNullOrEmpty(regretComments))
                { return regretComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { regretComments = value; }
            }
        }

        [DataMember(Name = "isRegret")]
        public bool IsRegret { get; set; }

        string vendorUnits = string.Empty;
        [DataMember(Name = "vendorUnits")]
        public string VendorUnits
        {
            get
            {
                if (!string.IsNullOrEmpty(vendorUnits))
                { return vendorUnits; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { vendorUnits = value; }
            }
        }

        [DataMember(Name = "itemLastPrice")]
        public double ItemLastPrice { get; set; }

        [DataMember(Name = "itemRank")]
        public int ItemRank { get; set; }

        string itemLevelInitialComments = string.Empty;
        [DataMember(Name = "itemLevelInitialComments")]
        public string ItemLevelInitialComments
        {
            get
            {
                if (!string.IsNullOrEmpty(itemLevelInitialComments))
                { return itemLevelInitialComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { itemLevelInitialComments = value; }
            }
        }


        string itemLevelRevComments = string.Empty;
        [DataMember(Name = "itemLevelRevComments")]
        public string ItemLevelRevComments
        {
            get
            {
                if (!string.IsNullOrEmpty(itemLevelRevComments))
                { return itemLevelRevComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { itemLevelRevComments = value; }
            }
        }


        [DataMember(Name = "savingsPercentage")]
        public double savingsPercentage { get; set; }

        [DataMember(Name = "savings")]
        public double savings { get; set; }

        [DataMember(Name = "revitemPriceCB")]
        public double RevItemPriceCB { get; set; }

        [DataMember(Name = "revUnitPriceCB")]
        public double RevUnitPriceCB { get; set; }

        [DataMember(Name = "revUnitDiscountCB")]
        public double RevUnitDiscountCB { get; set; }


        [DataMember(Name = "ITEM_LEVEL_LEAST_PRICE")]
        public double ITEM_LEVEL_LEAST_PRICE { get; set; }

        [DataMember(Name = "LAST_BID_REV_UNIT_PRICE")]
        public double LAST_BID_REV_UNIT_PRICE { get; set; }

        [DataMember(Name = "LAST_BID_REDUCTION_PRICE")]
        public double LAST_BID_REDUCTION_PRICE { get; set; }

        [DataMember(Name = "I_LLP_DETAILS")]
        public string I_LLP_DETAILS { get; set; }

        [DataMember(Name = "catalogueItemID")]
        public int CatalogueItemID { get; set; }

        [DataMember(Name = "ITEM_PR_NUMBER")]
        public string ITEM_PR_NUMBER { get; set; }
        [DataMember(Name = "IS_GMP_PR_ITEM")]
        public int IS_GMP_PR_ITEM { get; set; }
        [DataMember(Name = "ITEM_PLANTS")]
        public string ITEM_PLANTS { get; set; }

        [DataMember(Name = "PR_WBS_CODE")]
        public string PR_WBS_CODE { get; set; }

        [DataMember(Name = "ITEM_PR_QUANTITY")]
        public decimal ITEM_PR_QUANTITY { get; set; }

        [DataMember(Name = "CATEGORY_ID")]
        public string CATEGORY_ID { get; set; }

        [DataMember(Name = "splitenabled")]
        public bool SplitEnabled { get; set; }

        [DataMember(Name = "fromrange")]
        public double FromRange { get; set; }

        [DataMember(Name = "torange")]
        public double ToRange { get; set; }

        [DataMember(Name = "requiredQuantity")]
        public double RequiredQuantity { get; set; }

        string hsnCode = string.Empty;
        [DataMember(Name = "hsnCode")]
        public string HsnCode
        {
            get
            {
                if (!string.IsNullOrEmpty(hsnCode))
                { return hsnCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { hsnCode = value; }
            }
        }

        string hsnCodeCustomer = string.Empty;
        [DataMember(Name = "hsnCodeCustomer")]
        public string HsnCodeCustomer
        {
            get
            {
                if (!string.IsNullOrEmpty(hsnCodeCustomer))
                { return hsnCodeCustomer; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { hsnCodeCustomer = value; }
            }
        }

        string productCode = string.Empty;
        [DataMember(Name = "productCode")]
        public string ProductCode
        {
            get
            {
                if (!string.IsNullOrEmpty(productCode))
                { return productCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productCode = value; }
            }
        }

        string productQuotationTemplateJson = string.Empty;
        [DataMember(Name = "productQuotationTemplateJson")]
        public string ProductQuotationTemplateJson
        {
            get
            {
                if (!string.IsNullOrEmpty(productQuotationTemplateJson))
                { return productQuotationTemplateJson; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productQuotationTemplateJson = value; }
            }
        }

        [DataMember(Name = "productQuotationTemplateArray")]
        public List<ProductQuotationTemplate> ProductQuotationTemplateArray { get; set; }

        [DataMember(Name = "isCoreProductCategory")]
        public int IsCoreProductCategory { get; set; } = 1;

        [DataMember(Name = "isNewItem")]
        public bool IsNewItem { get; set; }
        [DataMember(Name = "isItemQuotationRejected")]
        public int IsItemQuotationRejected { get; set; }

        string itemQuotationRejectedComment = string.Empty;
        [DataMember(Name = "itemQuotationRejectedComment")]
        public string ItemQuotationRejectedComment
        {
            get
            {
                if (!string.IsNullOrEmpty(itemQuotationRejectedComment))
                { return itemQuotationRejectedComment; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { itemQuotationRejectedComment = value; }
            }
        }

        [DataMember(Name = "productDeliveryDetails")]
        public string ProductDeliveryDetails { get; set; }

        [DataMember(Name = "bidHistory")]
        public List<Comment> BidHistory { get; set; }

        [DataMember(Name = "PR_ID")]
        public string PR_ID { get; set; }

        [DataMember(Name = "PR_ITEM_ID")]
        public string PR_ITEM_ID { get; set; }
    }
}