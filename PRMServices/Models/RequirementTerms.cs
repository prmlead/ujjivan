﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class RequirementTerms : Entity
    {
        [DataMember(Name = "reqTermsID")]
        public int ReqTermsID { get; set; }

        [DataMember(Name = "reqID")]
        public int ReqID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "reqTermsDays")]
        public int ReqTermsDays { get; set; }

        [DataMember(Name = "reqTermsPercent")]
        public int ReqTermsPercent { get; set; }

        string reqTermsType = string.Empty;
        [DataMember(Name = "reqTermsType")]
        public string ReqTermsType
        {
            get
            {
                if (!string.IsNullOrEmpty(reqTermsType))
                {
                    return reqTermsType;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    reqTermsType = value;
                }
            }
        }

        [DataMember(Name = "isRevised")]
        public int IsRevised { get; set; }

        [DataMember(Name = "refReqTermID")]
        public int RefReqTermID { get; set; }

    }
}