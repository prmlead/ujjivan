﻿prmApp
    .controller('savePODetailsCtrl', ["$scope", "$stateParams", "$log", "$state", "$http", "domain", "$window", "userService", "auctionsService",
        "growlService", "PRMPRServices", "$rootScope", "fileReader", "workflowService", "PRMPOServices",
        function ($scope, $stateParams, $log, $state, $http, domain, $window, userService, auctionsService,
            growlService, PRMPRServices, $rootScope, fileReader, workflowService, PRMPOServices) {

            $scope.userId = userService.getUserId();
            $scope.sessionId = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.currentUserName = userService.getFirstname() + ' ' + userService.getLastname();

            $scope.POId = $stateParams.Id;
            $scope.qcsId = $stateParams.qcsId;
            $scope.reqId = $stateParams.reqId;
            $scope.poQuotId = $stateParams.quotId;
            $scope.requirementDetails = $stateParams.requirementDetails;
            $scope.vendorAssignments = $stateParams.detailsObj;
            $scope.poRAWJSON = $stateParams.poRawJSON;
            $scope.splittedItems = $stateParams.splitQtyItems;
            $scope.qcsType = $stateParams.qcsType;
            let poRoundDigits = 3;

            $scope.requirementPRItems = [];
            $scope.requirementPOItems = [];
            $scope.requirementVendorCodeInfo = [];
            $scope.selectedVendors = [];
            $scope.currentUIVendor = {};
            $scope.locationCodes = [];
            $scope.billTolocationCodes = [];
            $scope.shipTolocationCodes = [];
            $scope.deliveryTolocationCodes = [];
            resetPOObject();

            $scope.selectedVendors.push({ 'vendorId': 0, 'vendorCompany': 'Select Vendor', vendorObj: {} });
            if ($scope.vendorAssignments && $scope.vendorAssignments.length > 0) {
                $scope.vendorAssignments.forEach(function (assignment, index) {
                    let temp = _.filter($scope.selectedVendors, function (vendorObj) {
                        return vendorObj.vendorId === assignment.vendorID;
                    });

                    let vendorTemp = _.filter($scope.requirementDetails.auctionVendors, function (vendorObj) {
                        return vendorObj.vendorID === assignment.vendorID;
                    });

                    if (!temp || temp.length <= 0) {
                        $scope.selectedVendors.push({ 'vendorId': assignment.vendorID, 'vendorCompany': assignment.vendorName, vendorObj: vendorTemp[0] });
                    }
                });

                $scope.currentUIVendor = $scope.selectedVendors[0];
            }

            $scope.filterSelectionChange = function () {
                if ($scope.currentUIVendor && $scope.currentUIVendor.vendorId && +$scope.currentUIVendor.vendorId > 0) {
                    resetPOObject();
                    $scope.poDetails.PO_ITEMS = [];
                    $scope.poDetails.VENDOR_COMPANY = $scope.currentUIVendor.vendorCompany;
                    $scope.poDetails.VENDOR_ID = $scope.currentUIVendor.vendorId;
                    $scope.poDetails.VENDOR_QUOTATION = $stateParams.quoteLink ? $stateParams.quoteLink : $scope.reqId + '-' + $scope.currentUIVendor.vendorId;
                    $scope.poDetails.VENDOR_QUOTATION1 = $scope.reqId + '-' + $scope.currentUIVendor.vendorId;
                    $scope.poDetails.VENDOR_NAME = $scope.currentUIVendor.vendorObj.vendorName;
                    let currentVendorObj = _.filter($scope.requirementDetails.auctionVendors, function (vendorObj) {
                        return vendorObj.vendorID === $scope.currentUIVendor.vendorId;
                    });

                    $scope.poDetails.VENDOR_CODE = currentVendorObj[0].selectedVendorCode ? currentVendorObj[0].selectedVendorCode : $scope.currentUIVendor.vendorObj.vendorCode;
                    
                    //Items Populate:
                    let vendorItems = _.filter($scope.vendorAssignments, function (assignment) {
                        return assignment.vendorID === $scope.currentUIVendor.vendorId;
                    });

                    if (vendorItems && vendorItems.length > 0) {
                        totalVendorItems = vendorItems.length;
                        let index = 1;
                        vendorItems.forEach(function (vendorItem, index) {
                            let poItemObj = {
                                PO_ITEM_ID: 0,
                                PO_ID: 0,
                                PO_LINE_ITEM: '',
                                LINE_TYPE: 'ITEM',
                                ORDER_QTY: 0,
                                PENDING_QTY: 0,
                                UOM: '',
                                UNIT_PRICE: 0,
                                TOTAL_ITEM_AMOUNT: 0,
                                TOTAL_VALUE: 0,
                                CGST: 0,
                                SGST: 0,
                                IGST: 0,
                                DELIVERY_DATE: '',
                                PRODUCT_ID: 0,
                                PRODUCT_CODE: '',
                                PRODUCT_NAME: '',
                                CATEGORY_ID: 0,
                                CATEGORY_CODE: '',
                                SHIP_TO_ORGANIZATION_CODE: '',
                                SHIP_TO_LOCATION_ID: '',
                                PR_NUMBER: '',
                                PR_LINE_ITEMS: '',
                                modifiedBy: $scope.userId
                            };

                            var itemdetailsTemp = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                                return reqItem.itemID === vendorItem.itemID;
                            });

                            console.log(itemdetailsTemp);

                            let prItemObj = _.filter($scope.requirementPRItems, function (prItem) {
                                return prItem.PRODUCT_ID === itemdetailsTemp[0].catalogueItemID;
                            });

                            poItemObj.UOM = itemdetailsTemp[0].productQuantityIn;
                            poItemObj.ORDER_QTY = +itemdetailsTemp[0].qtyDistributed;
                            poItemObj.CGST = itemdetailsTemp[0].cGst;
                            poItemObj.SGST = itemdetailsTemp[0].sGst;
                            poItemObj.IGST = itemdetailsTemp[0].iGst;
                            poItemObj.PRODUCT_CODE = itemdetailsTemp[0].productCode.replace(/^0+/, '');
                            poItemObj.PRODUCT_NAME = itemdetailsTemp[0].productIDorName.replace(/^0+/, '');
                            //poItemObj.CATEGORY_CODE = itemdetailsTemp[0].categoryCode.replace(/^0+/, '');
                            poItemObj.PRODUCT_ID = +itemdetailsTemp[0].catalogueItemID;
                            poItemObj.CATEGORY_ID = +itemdetailsTemp[0].CATEGORY_ID;
                            if (itemdetailsTemp && itemdetailsTemp.length > 0) {
                                if ($scope.requirementDetails.isDiscountQuotation === 1) {
                                    poItemObj.DISCOUNT_VALUE = $scope.precisionRound(itemdetailsTemp[0].revUnitDiscount);
                                    poItemObj.UNIT_PRICE = $scope.precisionRound(parseFloat(itemdetailsTemp[0].revUnitPrice));
                                    poItemObj.TOTAL_ITEM_AMOUNT = parseFloat(itemdetailsTemp[0].revUnitPrice) * poItemObj.ORDER_QTY;

                                } else {
                                    poItemObj.DISCOUNT_VALUE = $scope.precisionRound(parseFloat(((itemdetailsTemp[0].unitPrice - itemdetailsTemp[0].revUnitPrice) / itemdetailsTemp[0].unitPrice) * 100));
                                    poItemObj.UNIT_PRICE = parseFloat(itemdetailsTemp[0].revUnitPrice);
                                    poItemObj.TOTAL_ITEM_AMOUNT = parseFloat(itemdetailsTemp[0].revUnitPrice) * poItemObj.ORDER_QTY;
                                }
                                
                                let itemTax = poItemObj.IGST ? poItemObj.IGST : (poItemObj.SGST + poItemObj.CGST);
                                poItemObj.GST = itemTax;
                                let itemTaxAmount = poItemObj.TOTAL_ITEM_AMOUNT * (itemTax/100)
                                $scope.poDetails.TOTAL_VALUE += (poItemObj.TOTAL_ITEM_AMOUNT + itemTaxAmount);
                                poItemObj.TOTAL_ITEM_AMOUNT_WITH_GST = poItemObj.TOTAL_ITEM_AMOUNT + itemTaxAmount;

                                poItemObj.MATERIAL_CODE = itemdetailsTemp[0].productCode.replace(/^0+/, '');
                                poItemObj.PRICE_UNIT = 1;
                                poItemObj.ORDER_PRICE_UNIT = itemdetailsTemp[0].productQuantityIn;
                            }

                            if (prItemObj && prItemObj.length > 0) {
                                let prNumbers = _.map(prItemObj, 'PR_NUMBER').join();
                                poItemObj.PR_NUMBERS = prNumbers;
                                let prItemTotalQuantity = _.sumBy(prItemObj, function (item) {
                                    return item.REQUIRED_QUANTITY;
                                });

                                if (poItemObj.QUANTITY && prItemTotalQuantity && poItemObj.QUANTITY > prItemTotalQuantity) {
                                    poItemObj.QUANTITY = prItemTotalQuantity;
                                }

                                if ($scope.requirementPOItems && $scope.requirementPOItems.length > 0) {
                                    var currentVendorPOItems = _.filter($scope.requirementPOItems, function (reqPOItem) {
                                        return reqPOItem.VENDOR_ID === +$scope.poDetails.VENDOR_ID && reqPOItem.MATNR === prItemObj[0].ITEM_CODE.replace(/^0+/, '');
                                    });

                                    if (poItemObj.QUANTITY && currentVendorPOItems && currentVendorPOItems.length > 0) {
                                        poItemObj.QUANTITY = poItemObj.QUANTITY - currentVendorPOItems.MENGE;
                                    }
                                }

                                let prLineItems = _.map(prItemObj, 'ITEM_NUM').join();
                                poItemObj.PR_LINE_ITEMS = prLineItems;
                            }

                            if (poItemObj.PR_NUMBERS) {
                                poItemObj.PO_LINE_ITEM = "000" + ((index + 1) * 10);
                                $scope.poDetails.PO_ITEMS.push(poItemObj);
                            }
                        });
                    }
                }
            };

            $scope.GetPRItemsByRequirementId = function () {
                $scope.poDetails.PURCHASE_GROUP_NAMES = '';
                $scope.poDetails.PURCHASE_GROUP_CODE = '';

                PRMPRServices.GetPRItemsByReqId({ reqid: $scope.reqId })
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementPRItems = response;
                            if ($scope.requirementPRItems && $scope.requirementPRItems.length > 0) {
                                $scope.poDetails.PURCHASE_GROUP_CODE = $scope.requirementPRItems[0].PURCHASE_GROUP;
                                $scope.poDetails.GMP_TYPE = $scope.requirementPRItems[0].GMP_NGMP === 'GMP' ? true : false;
                                $scope.poDetails.GMP_TYPE_HIDE = $scope.requirementPRItems[0].GMP_NGMP;
                            }
                        }

                        if ($scope.selectedVendors.length === 1) {
                            $scope.currentUIVendor = $scope.selectedVendors[0];
                        }

                        if ($scope.currentUIVendor && $scope.currentUIVendor.vendorId && $scope.currentUIPlant && $scope.currentUIPlant.PLANT_CODE) {
                            $scope.filterSelectionChange();
                        }

                        if ($scope.poRAWJSON) {
                            if ($scope.poRAWJSON && $scope.poRAWJSON.items && $scope.poRAWJSON.items.length > 0) {
                                let selectedPlant = $scope.poRAWJSON.items[0].PLANT;
                                let selectedVendor = $scope.poRAWJSON.VENDOR_ID;
                                if ($scope.selectedVendors && $scope.selectedVendors.length > 1) {
                                    let filteredVendor = _.filter($scope.selectedVendors, function (vendor) {
                                        return vendor.vendorId === selectedVendor;
                                    });

                                    if (filteredVendor && filteredVendor.length > 0) {
                                        $scope.currentUIVendor = filteredVendor[0];
                                    }
                                }
                            }

                            $scope.poDetails = $scope.poRAWJSON;
                        }
                    });
            };

            $scope.GetPRItemsByRequirementId();

            $scope.GetRequirementPO = function () {
                PRMPOServices.getRequirementPO({ compid: $scope.compId, reqid: $scope.reqId, sessionid: $scope.sessionId })
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementPOItems = response;
                        }
                    });
            };

            $scope.GetRequirementPO();

            $scope.GetRequirementVendorCodes = function () {
                auctionsService.getRequirementVendorCodes($scope.reqId, $scope.sessionId)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementVendorCodeInfo = response;
                        }
                    });
            };

            $scope.GetRequirementVendorCodes();
           
            $scope.getLocations = function () {
                $scope.locationCodes = [];
                $http({
                    method: 'get',
                    url: domain + 'getlocations?compid=' + $scope.compId + '&sessionid=' + $scope.sessionId,
                    encodeuri: true,
                    headers: { 'content-type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data && response.data.length > 0) {
                        $scope.locationCodes = response.data;
                        if ($scope.locationCodes && $scope.locationCodes.length > 0) {
                            $scope.billTolocationCodes = $scope.locationCodes.filter(function (location) {
                                return location.LOCATION_TYPE === 'BILLING';
                            });
                            $scope.shipTolocationCodes = $scope.locationCodes.filter(function (location) {
                                return location.LOCATION_TYPE === 'SHIPPING';
                            });
                            $scope.deliveryTolocationCodes = $scope.locationCodes.filter(function (location) {
                                return location.LOCATION_TYPE === 'DELIVERY';
                            });
                        }
                    }
                });
            };

            $scope.getLocations();

            $scope.precisionRound = function (number) {
                var factor = Math.pow(10, poRoundDigits);
                return Math.round(number * factor) / factor;
            };

            $scope.submitPO = function () {
                if ($scope.poDetails.PO_ITEMS && $scope.poDetails.PO_ITEMS.length > 0) {
                    let validationErrors = false;
                    $scope.tableValidation = false;
                    if (!$scope.poDetails.BILL_TO_LOCATION_ID) {
                        validationErrors = true;
                        $scope.poBillToLocationError = true;
                    }

                    if (!$scope.poDetails.SHIP_TO_LOCATION_ID) {
                        validationErrors = true;
                        $scope.poShipToLocationError = true;
                    }

                    $scope.poDetails.BILL_TO_LOCATION_ID = getLocationCode($scope.poDetails.BILL_TO_LOCATION_ID);
                    $scope.poDetails.SHIP_TO_LOCATION_ID = getLocationCode($scope.poDetails.SHIP_TO_LOCATION_ID);

                    let itemsno = 1;
                    $scope.poDetails.PO_ITEMS.forEach(function (item, index) {
                        if (!item.DELIVERY_DATE) {
                            $scope.tableValidation = true;
                            validationErrors = true;
                            $scope.tableValidationMsg = 'Please enter Delivery Date for the item: ' + itemsno;
                            return;
                        }

                        if (!item.SHIP_TO_ORGANIZATION_CODE) {
                            $scope.tableValidation = true;
                            validationErrors = true;
                            $scope.tableValidationMsg = 'Please select Ship to Org. for the item: ' + itemsno;
                            return;
                        }

                        if (!item.SHIP_TO_LOCATION_ID) {
                            $scope.tableValidation = true;
                            validationErrors = true;
                            $scope.tableValidationMsg = 'Please select Ship to location for the item: ' + itemsno;
                            return;
                        }

                        itemsno++;
                    });


                    if (!validationErrors) {
                        $scope.poDetails.PO_ITEMS.forEach(function (item, index) {
                            if (item.DELIVERY_DATE) {
                                let CurrentDateToLocal = userService.toLocalDate(item.DELIVERY_DATE);
                                let ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                                let m = moment(ts);
                                let deliveryDate = new Date(m);
                                let milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                                item.DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";
                            }

                            //item.SHIP_TO_ORGANIZATION_CODE = getLocationCode(item.SHIP_TO_ORGANIZATION_ID);
                            item.SHIP_TO_LOCATION_ID = getLocationCode(item.SHIP_TO_LOCATION_ID);
                        });

                        PRMPOServices.SavePODetails({ details: $scope.poDetails })
                            .then(function (response) {
                                if (response.errorMessage) {
                                    growlService.growl(response.errorMessage, "inverse");
                                } else {
                                    growlService.growl("Saved Successfully.", "success");
                                    //var url = $state.href("cost-comparisions-qcs", { "reqID": $scope.reqId, "qcsID": $scope.qcsId });
                                    $state.go('list-pendingPO');
                                    //window.open(url, '_self');
                                }
                            });
                    }
                }
            };

            function getLocationCode(locationId) {
                let locationCode = locationId;
                //let temp = _.filter($scope.locationCodes, function (location) {
                //    return location.LOCATION_ID === locationId;
                //});

                //if (temp && temp.length > 0) {
                //    locationCode = temp[0].LOCATION_CODE
                //}

                return locationCode;
            }

            function resetPOObject() {
                $scope.poDetails = {
                    COMP_ID: $scope.compId,
                    modifiedBy: $scope.userId,
                    sessionID: $scope.sessionId,
                    PO_NUMBER: '',
                    PRM_PO_NUMBER: '',
                    PO_TYPE: 'Standard',
                    VENDOR_ID: 0,
                    VENDOR_CODE: '',
                    VENDOR_SITE_CODE: '',
                    VENDOR_COMPANY: '',
                    
                    TOTAL_VALUE: 0,
                    BILL_TO_LOCATION_ID: 0,
                    SHIP_TO_LOCATION_ID: 0,
                    ORG_ID: 0,
                    PO_COMMENTS: '',
                    PO_CREATOR: $scope.userId,
                    PO_CREATOR_NAME: '',
                    PO_RELEASE_DATE: null,
                    DEPARTMENT_CODE: '',
                    WF_ID: 0,
                    PO_STATUS: 'PENDING',
                    PURCHASE_GROUP_NAMES: '',
                    PURCHASE_GROUP_CODE: '',
                    QCS_ID: $scope.qcsId,
                    REQ_ID: $scope.reqId,
                    PO_ITEMS: []
                };
            }

        }]);
