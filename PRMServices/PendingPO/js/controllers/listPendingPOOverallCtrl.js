﻿prmApp
    .controller('listPendingPOOverallCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices", "poService",
        "PRMCustomFieldService", "PRMPOService", "workflowService",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, poService, PRMCustomFieldService, PRMPOService,
            workflowService) {
            $scope.poid = $stateParams.poID;
            $scope.fromDate = $stateParams.fromDate;
            $scope.toDate = $stateParams.toDate;
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();

            $scope.poOrderId = $stateParams.poOrderId;
            $scope.dCode = $stateParams.dCode;
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

            $scope.compID = userService.getUserCompanyId();
            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5; //Number of pager buttons to show

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/
            $scope.billedQty = 0;
            $scope.receivedQty = 0;
            $scope.rejectedQty = 0;
            $scope.remainingQty = 0;
            $scope.removeQty = 0;

            $scope.deptIDs = [];
            $scope.desigIDs = [];



            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                        }
                    });
                });
            };

            auctionsService.GetUserDepartmentDesignations($scope.userID, userService.getUserToken())
                .then(function (response) {
                    if (response && response.length > 0) {
                        $scope.ListUserDepartmentDesignations = response;
                        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                                $scope.deptIDs.push(item.deptID);
                                item.listDesignation.forEach(function (item1, index1) {
                                    if (item1.isAssignedToUser && item1.isValid) {
                                        $scope.desigIDs.push(item1.desigID);
                                    }
                                });
                            });
                        }
                    }
                });

            $scope.getPendingPOOverall = function () {
                $scope.billedQty = 0;
                $scope.receivedQty = 0;
                $scope.rejectedQty = 0;
                $scope.remainingQty = 0;
                $scope.removeQty = 0;

                var params = {
                    "compid": $scope.isCustomer ? $scope.compID : 0,
                    "uid": $scope.isCustomer ? 0 : $scope.userID,
                    "poid": $scope.poid,
                    "search": '' ,
                    "categoryid": '',
                    "productid": '',
                    "supplier": '',
                    "postatus": '',
                    "deliverystatus": '',
                    "plant": '',
                    "fromdate": '1970-01-01',
                    "todate": '2100-01-01',
                    "page": 0,
                    "pagesize": 10,
                    "sessionid": userService.getUserToken()

                };

                $scope.pageSizeTemp = (params.page + 1);

                PRMPOService.getPOScheduleList(params)
                    .then(function (response) {
                        $scope.pendingPOList = [];
                        $scope.filteredPendingPOsList = [];
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_CLOSED_DATE = item.PO_CLOSED_DATE ? moment(item.PO_CLOSED_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_RELEASE_DATE = item.PO_RELEASE_DATE ? moment(item.PO_RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_RECEIPT_DATE = item.PO_RECEIPT_DATE ? moment(item.PO_RECEIPT_DATE).format("DD-MM-YYYY") : '-';
                                $scope.pendingPOList.push(item);

                                //item.MODIFIED_DATE = userService.toLocalDate(item.MODIFIED_DATE).split(' ')[0];
                            });
                        }

                        $scope.filteredPendingPOsList = $scope.pendingPOList[0];                        
                    });


                var params1 = {
                    "poid": $scope.poid,
                    "moredetails": 1,
                    "forasn": false
                };

                PRMPOService.getPOScheduleItems(params1)
                    .then(function (response) {
                        $scope.pendingPOItems = response;
                        $scope.pendingPOItems.forEach(function (item, index) {
                            item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            item.PR_RELEASE_DATE = item.PR_RELEASE_DATE ? moment(item.PR_RELEASE_DATE).format("DD-MM-YYYY") : '-';
                            item.PR_DELIVERY_DATE = item.PR_DELIVERY_DATE ? moment(item.PR_DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            item.VALID_FROM = item.VALID_FROM ? moment(item.VALID_FROM).format("DD-MM-YYYY") : '-';
                            item.VALID_TO = item.VALID_TO ? moment(item.VALID_TO).format("DD-MM-YYYY") : '-';
                            let itemPrice = item.UNIT_PRICE * item.ORDER_QTY;
                            item.TOTAL_VAL_WITH_TAX = itemPrice + (itemPrice * ((item.CGST + item.SGST + item.IGST) / 100));
                            $scope.billedQty += item.ORDER_QTY ;
                            $scope.receivedQty += item.RECEIVED_QTY ;
                            $scope.rejectedQty += item.REJECTED_QTY ;
                            $scope.remainingQty += item.REMAINING_QTY;
                            //$scope.removeQty += item.REMOVE_QTY;
                        });

                        //pendingPODetails.pendingPOItems = $scope.pendingPOItems;

                        $scope.getWorkflows();
                        //$scope.getItemWorkflow($scope.workflowList[0].workflowID, $scope.poid, $scope.WorkflowModule, '');
                    });
                
            };

            $scope.getPendingPOOverall();


            $scope.getPOInvoiceDetails = function () {
                $scope.params = {
                    "ponumber": $scope.poNumber,
                    "sessionID": userService.getUserToken()
                };

                PRMPOService.getPOInvoiceDetails($scope.params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.multipleAttachments = [];
                            });
                        }
                        $scope.invoiceList = response;
                        //$scope.invoiceList.forEach(function (item) {
                        //    var attchArray = item.ATTACHMENTS.split(',');
                        //    attchArray.forEach(function (att, index) {

                        //        var fileUpload = {
                        //            fileStream: [],
                        //            fileName: '',
                        //            fileID: parseInt(att)
                        //        };

                        //        item.multipleAttachments.push(fileUpload);
                        //    });

                        //})


                    });
            };

            $scope.getPOInvoiceDetails()


            $scope.viewPO = function (grnNo) {
                var url = $state.href('list-ViewGRN', { "grnNo": grnNo, "isGRN": false });

                $window.open(url, '_blank');
            };

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.obj = {
                auctionVendors: []
            };

            $scope.objNew = {
                auctionVendors: []
            };

            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'PO_V1';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/

            $scope.prStatus = [
                {
                    display: 'PENDING',
                    value: 'PENDING'
                },
                {
                    display: 'COMPLETED',
                    value: 'COMPLETED'
                },
                {
                    display: 'CANCELLED',
                    value: 'CANCELLED'
                },
                {
                    display: 'REJECTED',
                    value: 'REJECTED'
                }
            ];

            $scope.getWorkflows = function () {
                //createDomestic
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                                $scope.workflowListDeptWise.push(item);
                            }
                        });

                        $scope.getItemWorkflow($scope.workflowList[0].workflowID, $scope.poid, $scope.WorkflowModule, '');
                    });
            };

            //$scope.getWorkflows();

            $scope.getItemWorkflow = function (wfID, prID, type, itemID) {
                workflowService.getItemWorkflow(wfID, prID, type)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                //if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                //    $scope.isFormdisabled = true;
                                //}

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                    count = count + 1;
                                    //$scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });
            };

            $scope.updateTrack = function (step, status) {
                $scope.disableAssignPR = true;
                $scope.commentsError = '';
                if (step.comments) {
                    step.comments = validateStringWithoutSpecialCharacters(step.comments);
                }
                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status === 'APPROVED') {
                    $scope.disableAssignPR = false;
                } else {
                    $scope.disableAssignPR = true;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status === 'REJECTED' && !step.comments) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = $scope.userID;
                step.moduleName = $scope.WorkflowModule;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            location.reload();
                        }
                    });
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: $scope.userID, sessionID: $scope.sessionID }))
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            //  $state.go('list-pr');
                        }
                    });
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, $scope.userID)
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {
                let disable = true;
                let previousStep = {};
                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {
                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }

                    previousStep = step;
                });

                return disable;
            };

            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };

            /*region end WORKFLOW*/

            $scope.dateFormat = function (date) {
                return userService.toLocalDate(date);
            };

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }
     
        }]);