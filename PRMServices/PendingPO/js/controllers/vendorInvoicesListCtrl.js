﻿prmApp
    .controller('vendorInvoicesListCtrl', function ($scope, $state, $stateParams, userService, growlService, PRMPOService,
        workflowService,
        auctionsService, fileReader, $log, $window, poService) {


        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        $scope.userID = userService.getUserId();
        $scope.compID = userService.getUserCompanyId();
        $scope.customerCompanyId = userService.getCustomerCompanyId();

        //$scope.payment = {
        //    credits: {
        //        enabled: false
        //    },
        //    chart: {
        //        plotBackgroundColor: null,
        //        plotBorderWidth: null,
        //        plotShadow: false,
        //        type: 'pie',
        //        width: 425,
        //        height:240
        //    },
        //    title: {
        //        text: 'Payment'
        //    },
        //    tooltip: {
        //        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        //    },
        //    accessibility: {
        //        point: {
        //            valueSuffix: '%'
        //        }
        //    },
        //    plotOptions: {
        //        pie: {
        //            allowPointSelect: true,
        //            cursor: 'pointer',
        //            dataLabels: {
        //                enabled: false
        //            },
        //            showInLegend: true
        //        }
        //    },
        //    navigation: {
        //        buttonOptions: {
        //            enabled: false
        //        }
        //    },
        //    legend: {
        //        align: 'right',
        //        layout: 'vertical',
        //        verticalAlign: 'top',
        //        x: 0,
        //        y: 40
        //    },
        //    series: [{
        //        name: 'Brands',
        //        colorByPoint: true,
        //        data: [{
        //            name: 'Received',
        //            y: 60
        //        }, {
        //            name: 'OutStanding',
        //            y: 40
        //        }]
        //    }]
        //}


        //$scope.status = {
        //    credits: {
        //        enabled: false
        //    },
        //    chart: {
        //        plotBackgroundColor: null,
        //        plotBorderWidth: null,
        //        plotShadow: false,
        //        type: 'pie',
        //        width: 425,
        //        height: 240
        //    },
        //    title: {
        //        text: 'Status'
        //    },
        //    tooltip: {
        //        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        //    },
        //    accessibility: {
        //        point: {
        //            valueSuffix: '%'
        //        }
        //    },
        //    legend: {
        //        align: 'right',
        //        layout: 'vertical',
        //        verticalAlign: 'top',
        //        x: 0,
        //        y: 40
        //    },
        //    plotOptions: {
        //        pie: {
        //            allowPointSelect: true,
        //            cursor: 'pointer',
        //            dataLabels: {
        //                enabled: false
        //            },
        //            showInLegend: true
        //        }
        //    },
        //    navigation: {
        //        buttonOptions: {
        //            enabled: false
        //        }
        //    },
        //    series: [{
        //        name: 'Brands',
        //        colorByPoint: true,
        //        data: [{
        //            name: '50 Paid',
        //            y: 50
        //        }, {
        //            name: '0 paid',
        //            y: 0
        //        }, {
        //            name: '7 Unpaid',
        //            y: 7
        //        }, {
        //            name: '42 Overdue',
        //            y: 30
        //        }, {
        //            name: '4 Unconfirmed',
        //            y: 4
        //        }]
        //    }]
        //}
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;
        $scope.invoiceDetails = [];
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
            $scope.getpendingPOlist(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };

        $scope.filters = {
            searchKeyword: '',
            type: '',
            status: '',
            fromDate: moment().format('DD-MM-YYYY'),
            toDate: moment().add(30, "days").format('DD-MM-YYYY')
        }

        //$scope.setFilters = function (recordsFetchFrom, pageSize) {
        //    $scope.params = {
        //        "compid": $scope.compID,
        //        "uid": $scope.userID,
        //        "search": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : '',
        //        "type": $scope.filters.type ? $scope.filters.type : '',
        //        "status": $scope.filters.status ? $scope.status : '',
        //        "fromdate": $scope.filters.fromDate,
        //        "todate": $scope.filters.toDate,
        //        "page": recordsFetchFrom * pageSize,
        //        "pagesize": pageSize,
        //        "sessionid": userService.getUserToken()
        //    };

        //    $scope.pageSizeTemp = ($scope.params.page + 1);

        //    PRMPOService.getInvoiceDetails($scope.params)
        //        .then(function (response) {
        //            $scope.invoiceDetails = response;
        //        });
        //}


        //$scope.setFilters(0,10);

        $scope.getInvoiceList = function () {
            $scope.params = {
                "COMP_ID": +$scope.compID,
                "U_ID": +$scope.userID
            }

            PRMPOService.getInvoiceList($scope.params)
                .then(function (response) {
                    $scope.invoiceDetails = response;
                });
        }
        $scope.getInvoiceList();


        $scope.createInvoice = function (invoice) {
            var url = $state.href("createInvoice", { "poNumber": invoice.PO_NUMBER, "asnCode": invoice.ASN_NUMBER ? invoice.ASN_NUMBER : 0, "invoiceID": invoice.INVOICE_ID });
            $window.open(url, '_blank');
        }

    });