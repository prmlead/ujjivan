﻿prmApp
    .controller('createInvoiceCtrl', function ($scope, $state, $stateParams, userService, growlService, PRMPOService,
        workflowService,
        auctionsService, fileReader, $log, $window, poService) {

        $scope.userID = userService.getUserId();
        $scope.sessionID = userService.getUserToken();
        $scope.poNumber = $stateParams.poNumber;
        $scope.asnCode = $stateParams.asnCode;
        $scope.invoiceID = $stateParams.invoiceID;
        $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
        $scope.compID = userService.getUserCompanyId();
        $scope.customerCompanyId = userService.getCustomerCompanyId();
        $scope.selectedPODetails = [];
        $scope.billedQty = 0;
        $scope.receivedQty = 0;
        $scope.rejectedQty = 0;
        $scope.remainingQty = 0;
        $scope.removeQty = 0;
        $scope.isError = false;
        $scope.ALTERNATIVE_UOM = '';
        $scope.ALTERNATIVE_UOM_QTY = '';
        $scope.SERVICE_CODE = '';
        $scope.SERVICE_DESCRIPTION = '';
        $scope.MISC_CHARGES = '';

        if ($scope.isCustomer) {
            $scope.SelectedDeptId = 0;
            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if (userService.getSelectedUserDepartmentDesignation()) {
                $scope.SelectedUserDepartmentDesignation = userService.getSelectedUserDepartmentDesignation();
            }
            $scope.SelectedDeptId = $scope.SelectedUserDepartmentDesignation.deptID;
            $scope.UserLocation = $scope.SelectedUserDepartmentDesignation.userLocation;
        }


        $scope.checkInvoiceUniqueResult = false;
        $scope.isSaveDisabled = false;

        $scope.getPendingPOOverall = function () {
            $scope.billedQty = 0;
            $scope.receivedQty = 0;
            $scope.rejectedQty = 0;
            $scope.remainingQty = 0;
            $scope.removeQty = 0;


            //if ($scope.asnCode != 0) {
            //    $scope.params = {
            //        "compid": $scope.isCustomer ? $scope.compID : 0,
            //        "asnid": 0,
            //        "ponumber": $scope.poNumber,
            //        "grncode": 0,
            //        "asncode": $scope.asnCode ? $scope.asnCode : 0,
            //        "vendorid": $scope.isCustomer ? 0 : $scope.userID,
            //        "senssionid": userService.getUserToken()
            //    };

            //    PRMPOService.getASNDetails($scope.params)
            //        .then(function (response) {
            //            $scope.selectedPODetails = response;
            //            $scope.selectedPODetails.forEach(function (item) {
            //                item.AMOUNT = 0;
            //                item.INVOICE_QTY = item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY;
            //                item.AMOUNT = item.NET_PRICE * item.INVOICE_QTY;
            //                item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY_TEMP = item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY;

            //            });
            //        });
            //} else {
            //    var params1 = {
            //        "ponumber": $scope.poNumber,
            //        "moredetails": 0,
            //        "forasn": false
            //    };
            //    PRMPOService.getPOScheduleItems(params1)
            //        .then(function (response) {
            //            $scope.selectedPODetails = response;

            //            $scope.selectedPODetails.forEach(function (item) {
            //                item.AMOUNT = 0;
            //                item.INVOICE_QTY = item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY;
            //                item.AMOUNT = item.NET_PRICE * item.INVOICE_QTY;
            //                item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY_TEMP = item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY;
            //            });
            //            $scope.selectedPODetails[0].ASN_NUMBER = $scope.asnCode;
            //        });
            //}
            var params1 = {
                "ponumber": $scope.poNumber,
                "moredetails": 0,
                "forasn": false
            };
            PRMPOService.getPOScheduleItems(params1)
                .then(function (response) {
                    $scope.selectedPODetails = response;
                    var arr = [];
                    $scope.selectedPODetails.forEach(function (item) {
                        if (item.PO_INV && item.PO_INV.length > 0) {
                            if (+$scope.invoiceID > 0) {
                                var foundInviceNumber = _.find(item.PO_INV, { INVOICE_ID: +$scope.invoiceID }) ? _.find(item.PO_INV, { INVOICE_ID: +$scope.invoiceID }).INVOICE_NUMBER : null;

                                item.PO_INV.forEach(function (invoiceItem, invoiceItemIndex) {
                                    if (foundInviceNumber && invoiceItem.INVOICE_NUMBER === foundInviceNumber) {
                                        //item.INVOICE_TYPE = invoiceItem.INVOICE_TYPE;
                                        //item.INVOICE_NUMBER = invoiceItem.INVOICE_NUMBER;
                                        //item.INVOICE_DATE_1 = new moment(invoiceItem.INVOICE_DATE).format("DD-MM-YYYY");
                                        //item.DUE_DATE_1 = new moment(invoiceItem.DUE_DATE).format("DD-MM-YYYY");
                                        //item.TO_ADDRESS = invoiceItem.TO_ADDRESS;
                                        //item.ASN_NUMBER = invoiceItem.ASN_NUMBER ? invoiceItem.ASN_NUMBER : '';
                                        arr.push(invoiceItem);
                                        item.INVOICE_QTY = invoiceItem.INVOICE_QTY;
                                    }
                                });
                            }
                        }
                        item.isError = false;
                        item.AMOUNT = 0;
                        //item.INVOICE_QTY = item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY;
                        item.AMOUNT = item.NET_PRICE * item.INVOICE_QTY;
                        item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY_TEMP = item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY;
                    });
                    console.log(arr);
                    if (arr && arr.length > 0) {
                        arr.forEach(function (invoiceItem) {
                            $scope.selectedPODetails[0].INVOICE_TYPE = invoiceItem.INVOICE_TYPE;
                            $scope.selectedPODetails[0].INVOICE_NUMBER = invoiceItem.INVOICE_NUMBER;
                            $scope.selectedPODetails[0].INVOICE_DATE_1 = new moment(invoiceItem.INVOICE_DATE).format("DD-MM-YYYY");
                            $scope.selectedPODetails[0].DUE_DATE_1 = new moment(invoiceItem.DUE_DATE).format("DD-MM-YYYY");
                            $scope.selectedPODetails[0].TO_ADDRESS = invoiceItem.TO_ADDRESS;
                            $scope.selectedPODetails[0].ASN_NUMBER = invoiceItem.ASN_NUMBER ? invoiceItem.ASN_NUMBER : '';
                            $scope.selectedPODetails[0].STATUS = invoiceItem.STATUS;
                        });
                    }
                    $scope.selectedPODetails[0].ASN_NUMBER = $scope.asnCode;
                    if ($scope.selectedPODetails.length > 0) {
                        var params = {
                            "compid": $scope.isCustomer ? $scope.compID : 0,
                            "uid": $scope.isCustomer ? 0 : +$scope.userID,
                            "search": $scope.poNumber,
                            "categoryid": '',
                            "productid": '',
                            "supplier": '',
                            "postatus": '',
                            "deliverystatus": '',
                            "plant": '',
                            "fromdate": '1970-01-01',
                            "todate": '2100-01-01',
                            "page": 0,
                            "pagesize": 10,
                            "ackStatus": '',
                            "buyer": '',
                            "purchaseGroup": '',
                            "sessionid": userService.getUserToken()

                        };

                        $scope.pageSizeTemp = (params.page + 1);

                        PRMPOService.getPOScheduleList(params)
                            .then(function (response) {
                                $scope.pendingPOList = [];
                                if (response && response.length > 0) {
                                    response.forEach(function (item, index) {
                                        $scope.selectedPODetails[0].PO_NUMBER = $scope.poNumber;
                                        $scope.selectedPODetails[0].CUSTOMER_NAME = item.VENDOR_COMPANY;
                                        $scope.selectedPODetails[0].FROM_ADDRESS = item.ADDRESS;
                                        $scope.selectedPODetails[0].SUB_TOTAL = 0;
                                        $scope.selectedPODetails[0].TAX = 0;
                                        $scope.selectedPODetails[0].INVOICE_AMOUNT = 0;

                                    });
                                    $scope.selectedPODetails.forEach(function (item) {
                                        $scope.selectedPODetails[0].TAX = $scope.selectedPODetails[0].TAX + item.CGST + item.SGST + item.IGST;
                                        $scope.selectedPODetails[0].SUB_TOTAL = $scope.selectedPODetails[0].SUB_TOTAL + item.AMOUNT;
                                    });
                                    $scope.selectedPODetails[0].INVOICE_AMOUNT = $scope.selectedPODetails[0].SUB_TOTAL + $scope.selectedPODetails[0].TAX;

                                }
                            });
                    }
                });



        };

        $scope.getPendingPOOverall();


        $scope.createInvoice = function (poNumber) {
            var url = $state.href('vendorInvoices', { "poNumber": poNumber });
            $window.open(url, '_blank');
        };

        $scope.getSum = function (item, val, indexVal) {
            if (val == 'QTY') {
                $scope.selectedPODetails[0].SUB_TOTAL = 0;
                $scope.selectedPODetails[0].INVOICE_AMOUNT = 0;
                $scope.selectedPODetails.forEach(function (item1, index) {
                    if (index == indexVal) {
                        item1.AMOUNT = 0;
                        item1.AMOUNT = item1.NET_PRICE * item1.INVOICE_QTY;
                        $scope.selectedPODetails[0].SUB_TOTAL = $scope.selectedPODetails[0].SUB_TOTAL + item1.AMOUNT;
                        item1.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY = item1.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY_TEMP - item1.INVOICE_QTY;
                    }

                });
                $scope.selectedPODetails[0].INVOICE_AMOUNT = $scope.selectedPODetails[0].SUB_TOTAL + $scope.selectedPODetails[0].TAX;

            }
        }

        $scope.isQtyError = function () {
            let isValid = false;
            isValid = _.some($scope.selectedPODetails, function (item) {
                return (parseFloat(item.INVOICE_QTY) > parseFloat(item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY_TEMP));
            });
            return isValid;
        };

        $scope.areAllItemsValid = function () {
            let isValid = false;
            isValid = _.some($scope.selectedPODetails, function (item) {
                return item.isError;
            });
            return isValid;
        };


        $scope.saveInvoice = function () {

            if ($scope.checkInvoiceUniqueResult) {
                swal("Error!", "Invoice number already exists.", "error");
                return;
            }


            $scope.selectedPODetails.forEach(function (item) {
                var isError = '';
                item.PO_NUMBER = $scope.selectedPODetails[0].PO_NUMBER;
                item.CUSTOMER_NAME = $scope.selectedPODetails[0].CUSTOMER_NAME;
                item.INVOICE_TYPE = $scope.selectedPODetails[0].INVOICE_TYPE;
                item.INVOICE_ID = $scope.invoiceID ? +$scope.invoiceID : 0;
                item.INVOICE_NUMBER = $scope.selectedPODetails[0].INVOICE_NUMBER;
                item.ASN_NUMBER = $scope.selectedPODetails[0].ASN_NUMBER;
                item.FROM_ADDRESS = $scope.selectedPODetails[0].FROM_ADDRESS;
                item.TO_ADDRESS = $scope.selectedPODetails[0].TO_ADDRESS;
                item.INVOICE_AMOUNT = $scope.selectedPODetails[0].INVOICE_AMOUNT;
                item.C_COMP_ID = +$scope.customerCompanyId;
                item.V_COMP_ID = +$scope.compID;
                item.SessionID = $scope.sessionID;
                item.VENDOR_ID = $scope.userID;
                item.isError = false;

                isError = $scope.isQtyError();
                if (isError) {
                    item.isError = true;
                }

                var ts = moment($scope.selectedPODetails[0].INVOICE_DATE_1, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                item.INVOICE_DATE = "/Date(" + milliseconds + "000+0530)/";

                var ts = moment($scope.selectedPODetails[0].DUE_DATE_1, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                item.DUE_DATE = "/Date(" + milliseconds + "000+0530)/";
            })

            if ($scope.areAllItemsValid()) {
                return;
            }

            var params = {
                poInvDet: $scope.selectedPODetails
            };

            PRMPOService.savePOInvoice(params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Saved Successfully.", "success");
                        $state.go("vendorInvoices", {});
                    }

                });
        }


        $scope.checkUserUniqueResult = function (idtype, inputvalue) {
            if (!$scope.checkInvoiceUniqueResult) {
                $scope.checkInvoiceUniqueResult = false;
            }
            if (inputvalue == "" || inputvalue == undefined) {
                $scope.isSaveDisabled = false;
                return false;
            }

            userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                if (idtype == "INVOICE") {
                    if ($scope.checkInvoiceUniqueResult = !response) {
                        $scope.checkInvoiceUniqueResult = !response;
                        $scope.isSaveDisabled = true;
                    } else {
                        $scope.isSaveDisabled = false;
                    }
                }
            });
        };

        $scope.isFormdisabled = false;

        if ($scope.isCustomer) {
            $scope.isFormdisabled = true;
            $scope.getItemWorkflow = function () {
                workflowService.getItemWorkflow(0, $scope.invoiceID, 'VENDOR_INVOICE')
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });
            };

            $scope.updateTrack = function (step, status) {
                $scope.disableAssignPR = true;
                $scope.commentsError = '';
                if (step.comments != null || step.comments != "" || step.comments != undefined) {
                    step.comments = validateStringWithoutSpecialCharacters(step.comments);
                }
                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status === 'APPROVED') {
                    $scope.disableAssignPR = false;
                } else {
                    $scope.disableAssignPR = true;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = 'VENDOR_INVOICE';

                step.subModuleName = '';
                step.subModuleID = 0;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            location.reload();
                        }
                    });
            };

            $scope.getItemWorkflow();

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                });

                return disable;
            };


            $scope.deptIDs = [];
            $scope.desigIDs = [];
            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                        }
                    });
                });
            }


            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };

            $scope.showApprovedDate = function (date) {
                return userService.toLocalDate(date);
            };
        }

        $scope.completeInvoice = function (item) {
            var params =
            {
                "INVOICE_NUMBER": item[0].INVOICE_NUMBER,
                "INVOICE_ID": +$scope.invoiceID,
                "uId": +$scope.userID,
                "sessionid": userService.getUserToken()
            };

            PRMPOService.completeInvoice(params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Saved Successfully.", "success");
                        location.reload();
                    }
                });
        };

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.filesTemp = $("#" + id)[0].files;
            $scope.filesTemp = Object.values($scope.filesTemp);
            $scope.totalASNSize = 0;
            if ($scope.filesTemp && $scope.filesTemp.length > 0) {
                $scope.filesTemp.forEach(function (item, index) {
                    $scope.totalASNSize = $scope.totalASNSize + item.size;
                });
            }
            if (($scope.totalASNSize + $scope.totalASNItemSize) > $scope.totalAttachmentMaxSize) {
                swal({
                    title: "Attachment size!",
                    text: "Total Attachments size cannot exceed 6MB",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                },
                    function () {
                        return;
                    });
                return;
            }

            $scope.filesTemp.forEach(function (attach, attachIndex) {
                $scope.file = $("#" + id)[0].files[attachIndex];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        var fileUpload = {
                            fileStream: [],
                            fileName: '',
                            fileID: 0,
                            fileSize: 0
                        };
                        var bytearray = new Uint8Array(result);
                        fileUpload.fileSize = result.byteLength;
                        fileUpload.fileStream = $.makeArray(bytearray);
                        fileUpload.fileName = attach.name;
                        if (!$scope.selectedPODetails[0].attachmentsArray) {
                            $scope.selectedPODetails[0].attachmentsArray = [];
                        }

                        var ifExists = _.findIndex($scope.selectedPODetails[0].attachmentsArray, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                        if (ifExists <= -1) {
                            $scope.selectedPODetails[0].attachmentsArray.push(fileUpload);
                        }

                    });
            })
        }

        $scope.removeAttach = function (index) {
            $scope.selectedPODetails[0].attachmentsArray.splice(index, 1);
        }


    });