﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('list-pendingPO', {
                    url: '/list-pendingPO',
                    templateUrl: 'PendingPO/views/list-pendingPO.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('list-pendingPOOverall', {
                    url: '/list-pendingPOOverall/:poID',
                    templateUrl: 'PendingPO/views/list-pendingPOOverall.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('createInvoice', {
                    url: '/createInvoice/:poNumber/:asnCode/:invoiceID',
                    templateUrl: 'PendingPO/views/createInvoice.html'
                })

                .state('paymentTracking', {
                    url: '/paymentTracking',
                    templateUrl: 'PendingPO/views/paymenttracking.html'
                })
                .state('save-po-details', {
                    url: '/save-po-details/:Id',
                    templateUrl: 'PendingPO/views/save-po-details.html',
                    params: {
                        reqId: null,
                        qcsId: null,
                        quotId: null,
                        requirementDetails: null,
                        detailsObj: null,
                        quoteLink: null,
                        poRawJSON: null
                    }
                });               
        }]);