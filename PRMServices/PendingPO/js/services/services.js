﻿prmApp.constant('PRMPOServiceDomain', 'svc/PRMPOService.svc/REST/');
prmApp.service('PRMPOService', ["PRMPOServiceDomain", "SAPIntegrationServicesDomain", "userService", "httpServices",
    function (PRMPOServiceDomain, SAPIntegrationServicesDomain, userService, httpServices) {

        var PRMPOService = this;
       
        PRMPOService.getPOScheduleList = function (params) {
           
            let url = PRMPOServiceDomain + 'getposchedulelist?compid=' + params.compid + '&uid=' + params.uid + '&poid=' + params.poid + '&search=' + params.search + '&categoryid=' + params.categoryid
                + '&productid=' + params.productid + '&supplier=' + params.supplier + '&postatus=' + params.postatus
                + '&deliverystatus=' + params.deliverystatus
                + '&plant=' + params.plant
                + '&fromdate=' + params.fromdate + '&todate=' + params.todate
                + '&page=' + params.page
                + '&pagesize=' + params.pagesize + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOService.getPOScheduleItems = function (params) {
            let url = PRMPOServiceDomain + 'getposcheduleitems?poid=' + params.poid + '&compid=' + +params.compid + '&uid=' + +params.uid + '&moredetails=' + params.moredetails + '&sessionid=' + userService.getUserToken();

            return httpServices.get(url);
        };

        PRMPOService.getPOScheduleFilterValues = function (params) {
            let url = PRMPOServiceDomain + 'getposchedulefilters?compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };


        PRMPOService.getPOInvoiceDetails = function (params) {
            let url = PRMPOServiceDomain + 'getpoinvoicedetails?ponumber=' + params.ponumber + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOService.convertTOPDF = function (params) {
            let url = PRMPOServiceDomain + 'convertTOPDF';
            return httpServices.post(url, params);
        };

        PRMPOService.savePOInvoice = function (params) {
            let url = PRMPOServiceDomain + 'savepoinvoice';
            return httpServices.post(url, params);
        };

        PRMPOService.getASNDetailsList = function (params) {
            let url = PRMPOServiceDomain + 'getasndetailslist?ponumber=' + params.ponumber + '&sessionid=' + userService.getUserToken() + '&vendorid=' + params.vendorid;
            return httpServices.get(url);
        };

        PRMPOService.getASNDetails = function (params) {
            let url = PRMPOServiceDomain + 'getasndetails?compid=' + params.compid + '&asnid=' + params.asnid + '&ponumber=' + params.ponumber + '&grncode=' + params.grncode + '&asncode=' + params.asncode + '&sessionid=' + userService.getUserToken() + '&vendorid=' + params.vendorid;
            return httpServices.get(url);
        };

        PRMPOService.saveASNdetails = function (params) {
            let url = PRMPOServiceDomain + 'saveasndetails';
            return httpServices.post(url, params);
        };

        PRMPOService.deletePOInvoice = function (params) {
            let url = PRMPOServiceDomain + 'deletepoinvoice';
            return httpServices.post(url, params);
        };

        PRMPOService.getInvoiceList = function (params) {
            let url = PRMPOServiceDomain + 'getinvoicelist?compid=' + params.COMP_ID + '&sessionid=' + userService.getUserToken() + '&userid=' + params.U_ID;
            return httpServices.get(url);
        };

        PRMPOService.getPaymentInvoiceDetails = function (params) {
            let url = PRMPOServiceDomain + 'getPaymentInvoiceDetails?sessionid=' + params.sessionID + '&compId=' + params.compId;
            return httpServices.get(url);
        };


        return PRMPOService;

    }]);