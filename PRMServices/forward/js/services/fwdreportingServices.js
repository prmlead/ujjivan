prmApp.constant('PRMFwdReportServiceDomain', 'forward/svc/PRMFwdReportService.svc/REST/');
prmApp
    .service('fwdreportingServices', ["PRMFwdReportServiceDomain", "userService", "httpServices", "$window", function (PRMFwdReportServiceDomain, userService, httpServices, $window) {
        //var domain = 'http://182.18.169.32/services/';
        var reportingService = this;

        reportingService.getLiveBiddingReport = function (reqID) {
            let url = PRMFwdReportServiceDomain + 'getlivebiddingreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getItemWiseReport = function (reqID) {
            let url = PRMFwdReportServiceDomain + 'getitemwisereport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getDeliveryDateReport = function (reqID) {
            let url = PRMFwdReportServiceDomain + 'deliverytimelinereport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getPaymentTermsReport = function (reqID) {
            let url = PRMFwdReportServiceDomain + 'paymenttermsreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetReqDetails = function (reqID) {
            let url = PRMFwdReportServiceDomain + 'getreqdetails?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.downloadTemplate = function (template, userid, reqid) {
            let url = PRMFwdReportServiceDomain + 'gettemplates?template=' + template + '&userid=' + userid + '&reqid=' + reqid + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        reportingService.downloadConsolidatedTemplate = function (template, from, to, userid) {
            let url = PRMFwdReportServiceDomain + 'getconsolidatedtemplates?template=' + template + '&from=' + from + '&to=' + to + '&userid=' + userid + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }


        reportingService.GetReqReportForExcel = function (reqid, sessionid) {
            let url = PRMFwdReportServiceDomain + 'getfwdreqreportforexcel?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        reportingService.getConsolidatedReport = function (fromDate, toDate) {
            let url = PRMFwdReportServiceDomain + 'getconsolidatedreports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };



        reportingService.GetOpenPR = function (compid, plant, purchase, exclusion) {
            let url = PRMFwdReportServiceDomain + 'openpr?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken() + '&exclusion=' + exclusion;
            return httpServices.get(url);
        };

        reportingService.GetOpenPRPivot = function (compid, plant, purchase, exclusion) {
            let url = PRMFwdReportServiceDomain + 'openprpivot?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken() + '&exclusion=' + exclusion;
            return httpServices.get(url);
        };

        reportingService.GetOpenPO = function (compid, pono, plant, purchase, exclude) {
            let url = PRMFwdReportServiceDomain + 'openpo?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&pono=' + pono + '&sessionid=' + userService.getUserToken() + '&exclude='+exclude;
            return httpServices.get(url);
        };

        reportingService.GetOpenPOPivot = function (compid, plant, purchase) {
            let url = PRMFwdReportServiceDomain + 'openpopivot?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetPoComments = function (pono, item, type) {
            let url = PRMFwdReportServiceDomain + 'openpocomments?pono=' + pono + '&itemno=' + item + '&type=' + type + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.UpdatePoStatus = function (params) {
            var url = PRMFwdReportServiceDomain + 'updatepostatus';
            return httpServices.post(url, params);
        };

        reportingService.SavePoComments = function (params) {
            var url = PRMFwdReportServiceDomain + 'savepocomments';
            return httpServices.post(url, params);
        };

        reportingService.GetSapAccess = function (userid) {
            let url = PRMFwdReportServiceDomain + 'sapaccess?userid=' + userid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetLastUpdatedDate = function (table) {
            let url = PRMFwdReportServiceDomain + 'lastupdatedate?table=' + table + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetOpenPOReport = function (compid) {
            let url = PRMFwdReportServiceDomain + 'openporeport?compid=' + compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetOpenPRReport = function (compid) {
            let url = PRMFwdReportServiceDomain + 'openprreport?compid=' + compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };


        reportingService.GetOpenPOShortageReport = function (compid, pono, plant, purchase, exclude) {
            let url = PRMFwdReportServiceDomain + 'getopenposhortagereport?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&pono=' + pono + '&sessionid=' + userService.getUserToken() + '&exclude=' + exclude;
            return httpServices.get(url);
        };

        reportingService.GetOpenPRShortageReport = function (compid, plant, purchase, exclusion) {
            let url = PRMFwdReportServiceDomain + 'getopenprshortagereport?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken() + '&exclusion=' + exclusion;
            return httpServices.get(url);
        };

        return reportingService;
    }]);