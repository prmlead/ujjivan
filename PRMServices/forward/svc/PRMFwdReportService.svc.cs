﻿using System;
using System.Data;
using System.Web;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Common;
using PRMServices.Models;
using PRMServices.SupportingServices1;
using OfficeOpenXml;
using OfficeOpenXml.DataValidation;
using System.IO;
using System.Linq;
using System.Drawing;
using OfficeOpenXml.Style;
using PRMServices.SQLHelper;
using PRM.Core.Models.Reports;
using CORE = PRM.Core.Common;


namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMFwdReportService : IPRMFwdReportService
    {

        PRMServices prm = new PRMServices();

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private MySQLBizClass sqlHelper = new MySQLBizClass();

        #region Get

        public LiveBidding[] GetLiveBiddingReport(int reqID, int count, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            if (count <= 0)
            {
                count = 10;
            }

            List<LiveBidding> details = new List<LiveBidding>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_COUNT", count);
                DataSet ds = sqlHelper.SelectList("rp_GetLiveBiddingReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LiveBidding detail = ReportUtility.GetLiveBiddingReportObject(row, count);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                LiveBidding error = new LiveBidding();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public LiveBidding[] GetLiveBiddingReport2(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<LiveBidding> details = new List<LiveBidding>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                ReportsRequirement reportsrequirement = new ReportsRequirement();
                reportsrequirement = GetReqDetails(reqID, sessionID);
                TimeSpan duration = reportsrequirement.EndTime.Subtract(reportsrequirement.StartTime);
                int interval = 0;
                for (int a = 10; a < duration.Minutes; a = a + 10)
                {
                    interval = duration.Minutes - 10;
                }

                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetLiveBiddingReport2", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LiveBidding detail = ReportUtility.GetLiveBiddingReportObject2(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                LiveBidding error = new LiveBidding();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public ItemWiseReport[] GetItemWiseReport(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ItemWiseReport> details = new List<ItemWiseReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetItemWiseReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ItemWiseReport detail = ReportUtility.GetItemWiseReportObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                ItemWiseReport error = new ItemWiseReport();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public DeliveryTimeLine[] GetDeliveryTimeLineReport(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<DeliveryTimeLine> details = new List<DeliveryTimeLine>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetDeliveryTimeLineReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DeliveryTimeLine detail = ReportUtility.GetIDeliveryTimelineObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                DeliveryTimeLine error = new DeliveryTimeLine();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public DeliveryTimeLine[] GetPaymentTermsReport(int reqID, string sessionID)
        {
            List<DeliveryTimeLine> details = new List<DeliveryTimeLine>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetPaymentTermsReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DeliveryTimeLine detail = ReportUtility.GetIDeliveryTimelineObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                DeliveryTimeLine error = new DeliveryTimeLine();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public ReportsRequirement GetReqDetails(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            ReportsRequirement reportsrequirement = new ReportsRequirement();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetReqDetails", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    reportsrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    reportsrequirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    reportsrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    reportsrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    reportsrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    reportsrequirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    reportsrequirement.NegotiationDuration = row["NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["NEGOTIATION_DURATION"]) : string.Empty;
                    reportsrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    reportsrequirement.IsTabular = row["REQ_IS_TABULAR"] != DBNull.Value ? (Convert.ToInt32(row["REQ_IS_TABULAR"]) == 1 ? true : false) : false;
                    reportsrequirement.IsUnitPriceBidding = row["IS_UNIT_PRICE_BIDDING"] != DBNull.Value ? Convert.ToInt32(row["IS_UNIT_PRICE_BIDDING"]) : 0;
                    reportsrequirement.NoOfVendorsInvited = row["NO_OF_VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_INVITED"]) : 0;
                    reportsrequirement.NoOfVendorsParticipated = row["NO_OF_VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_PARTICIPATED"]) : 0;
                }
            }
            catch (Exception ex)
            {

            }

            return reportsrequirement;
        }

        public string GetTemplates(string template, int compID, int userID, int reqID, string sessionID)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region addvendors
            if (template.ToLower().Contains("addvendors"))
            {
                PRMServices service = new PRMServices();
                List<CategoryObj> categories = service.GetCategories(userID);
                var uniqueCategories = categories.Select(o => o.Category).Distinct().Where(o=>o.Trim()!=string.Empty).ToList();
                List<string> currencies = new List<string>(new string[] { "INR", "USD", "JPY", "GBP", "CAD", "AUD", "HKD", "EUR", "CNY" });
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("AddVendor");
                wsSheet1.Cells["A1"].Value = "FirstName";
                wsSheet1.Cells["B1"].Value = "LasName";
                wsSheet1.Cells["C1"].Value = "Email";
                wsSheet1.Cells["D1"].Value = "PhoneNumber";
                wsSheet1.Cells["E1"].Value = "CompanyName";
                wsSheet1.Cells["F1"].Value = "Category";
                wsSheet1.Cells["G1"].Value = "Currency";
                wsSheet1.Cells["H1"].Value = "KnownSince";
                var validation = wsSheet1.DataValidations.AddListValidation("G2:G100000");
                validation.ShowErrorMessage = true;
                validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                validation.ErrorTitle = "Invalid Currency";
                validation.Error = "Invalid currency selected";
                foreach(var currency in currencies)
                {
                    validation.Formula.Values.Add(currency);
                }

                var catValidation = wsSheet1.DataValidations.AddListValidation("F2:F100000");
                catValidation.ShowErrorMessage = true;
                catValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                catValidation.ErrorTitle = "Invalid Category";
                catValidation.Error = "Invalid category entered";
                int count = 1;
                foreach (var cat in uniqueCategories)
                {
                    wsSheet1.Cells["AA" + count.ToString()].Value = cat;
                    count++;
                }

                catValidation.Formula.ExcelFormula = "AA1:AA" + (count-1).ToString();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }                    
            }
            #endregion

            #region vendorquotation
            if (template.ToUpper().Contains("MARGIN_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("MARGIN_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";                
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";

                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;                
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors) {

                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);                  
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    Requirement req = service.GetRequirementData(entry.Value.RequirementID, entry.Key, sessionID);

                    string vendorRemarks = "";

                    foreach (var vendor in entry.Value.AuctionVendors) {
                        if (vendor.VendorID == entry.Key) {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems) {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["L" + index].Value = item.ItemID;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["M" + index].Value = 0;
                        wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["P" + index].Value = vendorRemarks;
                        wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                        wsSheet1.Cells["R" + index].Value = req.Title;
                        wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString();
                        wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                        var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                        wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, 2);
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region revvendorquotation
            if (template.ToUpper().Contains("MARGIN_REV_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("MARGIN_REV_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";

                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;                
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    Requirement req = service.GetRequirementData(entry.Value.RequirementID, entry.Key, sessionID);
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["L" + index].Value = item.ItemID;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["M" + index].Value = 0;
                        wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["P" + index].Value = vendorRemarks;
                        wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                        wsSheet1.Cells["R" + index].Value = req.Title;
                        wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString(); ;
                        wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                        var costPrice = 100 * item.UnitMRP / (100 + gst) * (1 + (item.UnitDiscount / 100));
                        wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, 2);
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region unitprice
            #region vendorquotation
            if (template.ToUpper().Contains("UNIT_PRICE_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_PRICE_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "PRICE";
                wsSheet1.Cells["D1"].Value = "GST";
                wsSheet1.Cells["E1"].Value = "FREIGHT";
                wsSheet1.Cells["F1"].Value = "TOTAL_PRICE";
                wsSheet1.Cells["G1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["H1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["I1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["J1"].Value = "PRODUCT_NO";
                wsSheet1.Cells["K1"].Value = "BRAND";
                wsSheet1.Cells["L1"].Value = "QUANTITY";
                wsSheet1.Cells["M1"].Value = "UNITS";
                wsSheet1.Cells["N1"].Value = "DESCRIPTION";
                wsSheet1.Cells["O1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["F2:F6000"].Formula = "IF(A2 > 0, L2*C2*(1+(D2/100)), \"\")";
                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["C" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["D" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["E" + index].Value = entry.Value.AuctionVendors[0].RevVendorFreight;
                        //wsSheet1.Cells["F" + index].Value = item.RevItemPrice;
                        wsSheet1.Cells["G" + index].Value = 0;
                        wsSheet1.Cells["H" + index].Value = item.ItemID;
                        wsSheet1.Cells["I" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["J" + index].Value = item.ProductNo;
                        wsSheet1.Cells["K" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["L" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["M" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["N" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["O" + index].Value = item.RequirementID;
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion
            if (template.ToUpper().Contains("UNIT_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "PRICE";
                wsSheet1.Cells["D1"].Value = "GST";
                wsSheet1.Cells["E1"].Value = "FREIGHT";
                wsSheet1.Cells["F1"].Value = "TOTAL_PRICE";
                wsSheet1.Cells["G1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["H1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["I1"].Value = "Main Equipment/Vehicle";
                wsSheet1.Cells["J1"].Value = "Part_No";
                wsSheet1.Cells["K1"].Value = "Make";
                wsSheet1.Cells["L1"].Value = "Specification Of Item";
                wsSheet1.Cells["M1"].Value = "QTY/UOM";
                wsSheet1.Cells["N1"].Value = "Units";
                wsSheet1.Cells["O1"].Value = "Description";
                wsSheet1.Cells["P1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["F2:F6000"].Formula = "IF(A2 > 0, C2*M2*(1+(D2/100)), \"\")";
                wsSheet1.Cells["A:P"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["C:E"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["C:E"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["C" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["D" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["E" + index].Value = entry.Value.AuctionVendors[0].RevVendorFreight;
                        wsSheet1.Cells["G" + index].Value = 0;
                        wsSheet1.Cells["H" + index].Value = item.ItemID;
                        wsSheet1.Cells["I" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["J" + index].Value = item.ProductNo;
                        wsSheet1.Cells["K" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["L" + index].Value = item.OthersBrands;
                        wsSheet1.Cells["M" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["N" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["O" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["P" + index].Value = item.RequirementID;
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            if (template.ToUpper().Contains("UNIT_BIDDING"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_BIDDING");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["C1"].Value = "Main Equipment/Vehicle";
                wsSheet1.Cells["D1"].Value = "Part_No";
                wsSheet1.Cells["E1"].Value = "Make";
                wsSheet1.Cells["F1"].Value = "Specification Of Item";
                wsSheet1.Cells["G1"].Value = "Revised Unit Price";
                wsSheet1.Cells["H1"].Value = "QTY/UOM";
                wsSheet1.Cells["I1"].Value = "GST";
                wsSheet1.Cells["J1"].Value = "Revised Item Price";
                wsSheet1.Cells["K1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["L1"].Value = "Unit Price";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(G2 > 0, G2*H2*(1 + I2/100), \"\")";
                wsSheet1.Cells["A:J"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["G:G"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["G:G"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = item.RequirementID;
                        wsSheet1.Cells["C" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["D" + index].Value = item.ProductNo;
                        wsSheet1.Cells["E" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["F" + index].Value = item.OthersBrands;
                        wsSheet1.Cells["G" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["H" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["I" + index].Value = (item.SGst + item.CGst + item.IGst);
                        wsSheet1.Cells["K" + index].Value = item.ItemID;
                        wsSheet1.Cells["L" + index].Value = item.UnitPrice;
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #region 
            if (template.ToUpper().Contains("UNIT_PRICE_BIDDING_DATA"))
            {
                PRMServices service = new PRMServices();
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_PRICE_BIDDING_DATA");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "PRICE";
                wsSheet1.Cells["D1"].Value = "REVISED_PRICE";
                wsSheet1.Cells["E1"].Value = "GST";
                wsSheet1.Cells["F1"].Value = "FREIGHT";
                wsSheet1.Cells["G1"].Value = "REVISED_FREIGHT";
                wsSheet1.Cells["H1"].Value = "TOTAL_PRICE";
                wsSheet1.Cells["I1"].Value = "REVISED_TOTAL_PRICE";
                wsSheet1.Cells["J1"].Value = "ITEM_ID";
                wsSheet1.Cells["K1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["L1"].Value = "PRODUCT_NO";
                wsSheet1.Cells["M1"].Value = "BRAND";
                wsSheet1.Cells["N1"].Value = "QUANTITY";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "DESCRIPTION";                
                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    double freightcharges = 0;
                    double revfreightcharges = 0;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                            freightcharges = vendor.VendorFreight;
                            revfreightcharges = vendor.RevVendorFreight;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["C" + index].Value = item.UnitPrice;// entry.Value.AuctionVendors[0].RunningPrice;
                        wsSheet1.Cells["D" + index].Value = item.RevUnitPrice; // entry.Value.AuctionVendors[0].RevVendorTotalPrice;
                        wsSheet1.Cells["E" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["F" + index].Value = entry.Value.AuctionVendors[0].VendorFreight;
                        wsSheet1.Cells["G" + index].Value = entry.Value.AuctionVendors[0].RevVendorFreight;
                        wsSheet1.Cells["H" + index].Value = entry.Value.AuctionVendors[0].TotalInitialPrice;
                        wsSheet1.Cells["I" + index].Value = entry.Value.AuctionVendors[0].RevVendorTotalPrice;
                        wsSheet1.Cells["J" + index].Value = item.ItemID;
                        wsSheet1.Cells["K" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["L" + index].Value = item.ProductNo;
                        wsSheet1.Cells["M" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["N" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["P" + index].Value = item.ProductDescription;
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion
            #endregion

            #region bulk margin quotations
            if (template.ToUpper().Contains("BULK_MARGIN_VEND_QUOTATION"))
            {
                PRMServices service = new PRMServices();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("BULK_MARGIN_VEND_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";
                wsSheet1.Cells["U1"].Value = "QUANTITY_ORIG";
                wsSheet1.Cells["V1"].Value = "QUOTATION_STATUS";
                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;
                wsSheet1.Column(21).Hidden = true;
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                int index = 2;
                List<Requirement> requirementList = service.GetVendorRequirements(userID, 14, sessionID);
                foreach (Requirement req in requirementList)
                {
                    if (req.AuctionVendors.Count == 0)
                    {
                        continue;
                    }
                    if (req.PostedOn >= DateTime.Now.AddMonths(-1) && req.IsDiscountQuotation == 2 && (req.StartTime == null || req.StartTime > DateTime.Now))
                    {
                        if(req.RequirementID > 0 && req.PostedOn >= DateTime.Now.AddMonths(-1))
                        {
                            foreach (var item in req.ListRequirementItems)
                            {
                                var gst = item.CGst + item.SGst + item.IGst;
                                wsSheet1.Cells["A" + index].Value = userID;
                                wsSheet1.Cells["B" + index].Value = req.AuctionVendors[0].CompanyName;
                                wsSheet1.Cells["L" + index].Value = item.ItemID;
                                wsSheet1.Cells["C" + index].Value = item.ProductNo;
                                wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                                wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                                wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                                wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                                wsSheet1.Cells["M" + index].Value = 0;
                                wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                                wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                                wsSheet1.Cells["P" + index].Value = req.AuctionVendors[0].OtherProperties;
                                wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                                wsSheet1.Cells["R" + index].Value = req.Title;
                                wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString(); ;
                                wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                                wsSheet1.Cells["U" + index].Value = item.ProductQuantity;
                                wsSheet1.Cells["V" + index].Value = req.AuctionVendors[0].IsQuotationRejected == -1 ? "Pending/Not Uploaded" : (req.AuctionVendors[0].IsQuotationRejected == 0 ? "Approved" : "Rejected");
                                var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                                wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, 2);
                                index++;
                            }
                        }
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region bulk margin bidding
            if (template.ToUpper().Contains("BULK_MARGIN_VEND_BIDDING"))
            {
                PRMServices service = new PRMServices();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("BULK_MARGIN_VEND_BIDDING");
                wsSheet1.Cells["A1"].Value = "REQ_ID";
                wsSheet1.Cells["B1"].Value = "REQ_TITLE";
                wsSheet1.Cells["C1"].Value = "POSTED_DATE";
                wsSheet1.Cells["D1"].Value = "NEGOTIATION_DATE";
                wsSheet1.Cells["E1"].Value = "VENDOR_ID";
                wsSheet1.Cells["F1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["G1"].Value = "HSN_CODE";
                wsSheet1.Cells["H1"].Value = "MANUFACTURER_NAME";
                wsSheet1.Cells["I1"].Value = "BRAND";
                wsSheet1.Cells["J1"].Value = "QUANTITY";
                wsSheet1.Cells["K1"].Value = "COST_PRICE";
                wsSheet1.Cells["L1"].Value = "REV_COST_PRICE";
                wsSheet1.Cells["M1"].Value = "DIFFERENCE";
                wsSheet1.Cells["N1"].Value = "GST";
                wsSheet1.Cells["O1"].Value = "MRP";
                wsSheet1.Cells["P1"].Value = "REV_NET_PRICE";
                wsSheet1.Cells["Q1"].Value = "REV_MARGIN_AMOUNT";
                wsSheet1.Cells["R1"].Value = "REV_MARGIN_PERC";
                wsSheet1.Cells["S1"].Value = "ITEM_ID";
                wsSheet1.Cells["T1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["U1"].Value = "TOTAL_INIT_PRICE";
                wsSheet1.Cells["V1"].Value = "REV_TOTAL_PRICE";
                wsSheet1.Cells["W1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["X1"].Value = "UNITS";
                wsSheet1.Cells["Y1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Z1"].Value = "RANK";
                wsSheet1.Cells["AA1"].Value = "REV_COST_PRICE_ORIG";

                #region excelStyling                

                wsSheet1.Cells["R2:R6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Cells["A:AA"].AutoFitColumns();
                wsSheet1.Column(10).Hidden = true;
                wsSheet1.Column(11).Hidden = true;
                wsSheet1.Column(14).Hidden = true;
                wsSheet1.Column(15).Hidden = true;
                wsSheet1.Column(21).Hidden = true;
                wsSheet1.Column(27).Hidden = true;
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#DAF7A6");
                wsSheet1.Cells["P:P"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["P:P"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["Q:Q"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["Q:Q"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["R:R"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["R:R"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["L:L"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["L:L"].Style.Fill.BackgroundColor.SetColor(colFromHex1);
                var calcColumns = wsSheet1.Cells["P:R"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling
                
                wsSheet1.Cells["M2:M6000"].Formula = "IF(E2 > 0, ABS(K2 - L2), \"\")";
                wsSheet1.Cells["P2:P6000"].Formula = "IF(E2 > 0, L2*(1+(N2/100)), \"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                List<Requirement> requirementList = service.GetVendorRequirements(userID, 1, sessionID);
                int index = 2;
                foreach (Requirement req in requirementList)
                {
                    if(req.AuctionVendors.Count > 0 && (req.AuctionVendors[0].IsQuotationRejected == 1 || req.AuctionVendors[0].IsRevQuotationRejected == 1))
                    {
                        continue;
                    }
                    if (req.PostedOn >= DateTime.Now.AddMonths(-1) && req.IsDiscountQuotation == 2 && (req.StartTime < DateTime.Now && req.EndTime > DateTime.Now))
                    {
                        if(req.AuctionVendors == null)
                        {
                            continue;
                        }
                        foreach (var item in req.ListRequirementItems)
                        {
                            var gst = item.CGst + item.SGst + item.IGst;
                            var revCostPrice = (item.UnitMRP * 100 * 100) / ((item.RevUnitDiscount * 100) + 10000 + (item.RevUnitDiscount * gst) + (gst * 100));
                            wsSheet1.Cells["A" + index].Value = req.RequirementID; // entry.Key;
                            wsSheet1.Cells["B" + index].Value = req.Title;  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["C" + index].Value = req.PostedOn.ToString();  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["D" + index].Value = req.StartTime.ToString();  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["E" + index].Value = userID;  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["F" + index].Value = req.AuctionVendors[0].CompanyName;  // entry.Value.AuctionVendors[0].CompanyName;

                            wsSheet1.Cells["G" + index].Value = item.ProductNo;
                            wsSheet1.Cells["H" + index].Value = item.OthersBrands;
                            wsSheet1.Cells["I" + index].Value = item.ProductBrand;
                            wsSheet1.Cells["J" + index].Value = item.ProductQuantity;
                            wsSheet1.Cells["L" + index].Value = Math.Round(revCostPrice, 2);

                            wsSheet1.Cells["N" + index].Value = item.CGst + item.SGst + item.IGst;
                            wsSheet1.Cells["O" + index].Value = item.UnitMRP;
                            wsSheet1.Cells["S" + index].Value = item.ItemID;
                            wsSheet1.Cells["T" + index].Value = 0;
                            wsSheet1.Cells["U" + index].Value = req.AuctionVendors[0].TotalInitialPrice;
                            wsSheet1.Cells["V" + index].Value = req.AuctionVendors[0].RevVendorTotalPrice;
                            wsSheet1.Cells["W" + index].Value = item.ProductIDorName;
                            wsSheet1.Cells["X" + index].Value = item.ProductQuantityIn;
                            wsSheet1.Cells["Y" + index].Value = req.AuctionVendors[0].OtherProperties;
                            wsSheet1.Cells["Z" + index].Value = req.AuctionVendors[0].Rank > 0 ? req.AuctionVendors[0].Rank.ToString() : "NA";
                            wsSheet1.Cells["AA" + index].Value = Math.Round(revCostPrice, 2);

                            if (req.IsRevUnitDiscountEnable == 0)
                            {
                                wsSheet1.Cells["Q" + index].Value = 0;
                                wsSheet1.Cells["R" + index].Value = 0;
                            }
                            if (req.IsRevUnitDiscountEnable == 1)
                            {
                                wsSheet1.Cells["Q" + index].Formula = "IF(E" + index + " > 0, ABS(O" + index + " - P" + index + "), \"\")";
                                wsSheet1.Cells["R" + index].Formula = "IFERROR(Q" + index + "/P" + index + "*100,\"\")";
                            }

                            var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                            wsSheet1.Cells["K" + index].Value = Math.Round(costPrice, 2);
                            index++;
                        }
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region Comparatives Report

            if (template.ToUpper().Contains("COMPARATIVES_REPORT"))
            {
                ExcelRequirement excelRequirement = Getfwdreqreportforexcel(reqID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("COMPARATIVES_REPORT");
                
                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "COMPARATIVES REPORT";
                wsSheet1.Cells["A" + lineNumber + ":P" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":P" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":P" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":P" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":P" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":P" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":P" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;

               
                wsSheet1.Cells["A" + lineNumber].Value = "INDENT NO:";
                wsSheet1.Cells["A" + lineNumber + ":D" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":D" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["E" + lineNumber].Value = excelRequirement.RequirementID;
                wsSheet1.Cells["E" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                wsSheet1.Cells["E" + lineNumber + ":H" + lineNumber].Merge = true;
              
                //lineNumber++;

                wsSheet1.Cells["I" + lineNumber].Value = "DATE:";
                wsSheet1.Cells["I" + lineNumber + ":L" + lineNumber].Merge = true;
                wsSheet1.Cells["I" + lineNumber + ":L" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["M".ToString() + lineNumber].Value = excelRequirement.CurrentDate.ToString();
                wsSheet1.Cells["M" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                wsSheet1.Cells["M" + lineNumber + ":P" + lineNumber].Merge = true;
             
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "TITLE:";
                wsSheet1.Cells["A" + lineNumber + ":D" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":D" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["E" + lineNumber].Value = excelRequirement.Title;
                wsSheet1.Cells["E" + lineNumber + ":H" + lineNumber].Merge = true;
              
                // lineNumber++;

                wsSheet1.Cells["I" + lineNumber].Value = "CREATED BY:";
                wsSheet1.Cells["I" + lineNumber + ":L" + lineNumber].Merge = true;
                wsSheet1.Cells["I" + lineNumber + ":L" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["M" + lineNumber].Value = excelRequirement.CustFirstName + excelRequirement.CustLastName;
                wsSheet1.Cells["M" + lineNumber + ":P" + lineNumber].Merge = true;
             
                lineNumber++;


                wsSheet1.Cells["A" + lineNumber].Value = "";
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Bold = true;


                wsSheet1.Cells["G" + lineNumber].Value = "Vendor ID";
                wsSheet1.Cells["G" + lineNumber + ":I" + lineNumber].Merge = true;
                wsSheet1.Cells["G" + lineNumber].Style.Font.Bold = true;

               

                lineNumber++;


                wsSheet1.Cells["A" + lineNumber].Value = "Product ID";
                wsSheet1.Cells["A" + lineNumber].Style.Font.Bold = true; 
                wsSheet1.Cells["B" + lineNumber].Value = "HSN Code";
                wsSheet1.Cells["B" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["C" + lineNumber].Value = "Description";
                wsSheet1.Cells["C" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["D" + lineNumber].Value = "Quantity";
                wsSheet1.Cells["D" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["E" + lineNumber].Value = "Units";
                wsSheet1.Cells["E" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["F" + lineNumber].Value = "Brand";
                wsSheet1.Cells["F" + lineNumber].Style.Font.Bold = true;
                lineNumber++;
                lineNumber++;
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;




                foreach (var item in excelRequirement.ReqItems)
                {
                    //var gst = item.CGst + item.SGst + item.IGst;


                    int lineMergeNumber = Convert.ToInt32(lineNumber) + 8;

                    if (!string.IsNullOrEmpty(item.ProductIDorName))
                    {
                        item.ProductIDorName = item.ProductIDorName;
                    }
                    else
                    {
                        item.ProductIDorName = string.Empty;
                    }
                    wsSheet1.Cells["A" + lineNumber].Value = item.ProductIDorName;
                    wsSheet1.Cells["A" + lineNumber + ":A" + lineMergeNumber].Merge = true;

                    if (!string.IsNullOrEmpty(item.ProductNo))
                    {
                        item.ProductNo = item.ProductNo;
                    }
                    else
                    {
                        item.ProductNo = string.Empty;
                    }

                    wsSheet1.Cells["B" + lineNumber].Value = item.ProductNo;
                    wsSheet1.Cells["B" + lineNumber + ":B" + lineMergeNumber].Merge = true;

                    if (!string.IsNullOrEmpty(item.ProductDescription))
                    {
                        item.ProductDescription = item.ProductDescription;
                    }
                    else
                    {
                        item.ProductDescription = string.Empty;
                    }
                    wsSheet1.Cells["C" + lineNumber].Value = item.ProductDescription;
                    wsSheet1.Cells["C" + lineNumber + ":C" + lineMergeNumber].Merge = true;                    
                    wsSheet1.Cells["D" + lineNumber].Value = item.ProductQuantity;
                    wsSheet1.Cells["D" + lineNumber + ":D" + lineMergeNumber].Merge = true;
                    wsSheet1.Cells["E" + lineNumber].Value = item.ProductQuantityIn;
                    wsSheet1.Cells["E" + lineNumber + ":E" + lineMergeNumber].Merge = true;

                    if (!string.IsNullOrEmpty(item.ProductBrand))
                    {
                        item.ProductBrand = item.ProductBrand;
                    }
                    else
                    {
                        item.ProductBrand = string.Empty;
                    }
                    wsSheet1.Cells["F" + lineNumber].Value = item.ProductBrand;
                    wsSheet1.Cells["F" + lineNumber + ":F" + lineMergeNumber].Merge = true;

                    wsSheet1.Cells["G" + lineNumber].Value = "Vendor Units";
                    wsSheet1.Cells["G" + lineNumber + ":I" + lineNumber].Merge = true;
                    wsSheet1.Cells["G" + lineNumber].Style.Font.Bold = true;
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 1)].Value = "Vendor Brand";
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 1) + ":I" + (Convert.ToInt32(lineNumber) + 1)].Merge = true;
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 1)].Style.Font.Bold = true;
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 2)].Value = "Original";
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 2) + ":I" + (Convert.ToInt32(lineNumber) + 2)].Merge = true;
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 2)].Style.Font.Bold = true;
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 3)].Value = "Discounted";
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 3) + ":I" + (Convert.ToInt32(lineNumber) + 3)].Merge = true;
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 3)].Style.Font.Bold = true;
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 4)].Value = "Initial Total GST & GST %";
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 4) + ":I" + (Convert.ToInt32(lineNumber) + 4)].Merge = true;
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 4)].Style.Font.Bold = true;
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 5)].Value = "Initial Total";
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 5) + ":I" + (Convert.ToInt32(lineNumber) + 5)].Merge = true;
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 5)].Style.Font.Bold = true;
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 6)].Value = "Rev Total";
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 6) + ":I" + (Convert.ToInt32(lineNumber) + 6)].Merge = true;
                    wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 6)].Style.Font.Bold = true;
                    //wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 7)].Value = "Comments";
                    //wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 7) + ":I" + (Convert.ToInt32(lineNumber) + 7)].Merge = true;
                    //wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 7)].Style.Font.Bold = true;



                    char cl = 'J';

                    foreach (var vend in item.ReqVendors) {
                        wsSheet1.Cells[cl.ToString() + 4].Value = vend.VendorID;
                        wsSheet1.Cells[cl.ToString() + 4].Style.Font.Bold = true;
                        wsSheet1.Cells[cl.ToString() + 5].Value = vend.CompanyName;
                        wsSheet1.Cells[cl.ToString() + 5].Style.Font.Bold = true;
                        cl++;

                    }
                    foreach (var vend in item.ReqVendors) { 
                    char titleCL = cl--;
                    
                    wsSheet1.Cells["J" + (lineNumber - 1) + ":" + (titleCL) + (lineNumber - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["J" + (lineNumber - 1) + ":" + (titleCL) + (lineNumber - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["J" + (lineNumber - 1) + ":" + (titleCL) + (lineNumber - 1)].Style.Font.Size = 12;

                    wsSheet1.Cells.AutoFitColumns();
                    wsSheet1.Cells["J" + lineNumber + ":" + cl + lineNumber].Style.Font.Bold = false;
                        
                    }
                    
                    foreach (var vend in item.ReqVendors) {

                        //var GST = vend.QuotationPrices.IGst + vend.QuotationPrices.SGst + vend.QuotationPrices.CGst;

                        if (vend.QuotationPrices == null) {
                            vend.QuotationPrices = new ExcelQuotationPrices();
                        }

                        if (!string.IsNullOrEmpty(vend.QuotationPrices.VendorUnits)) {
                            vend.QuotationPrices.VendorUnits = vend.QuotationPrices.VendorUnits;
                        } else {
                            vend.QuotationPrices.VendorUnits = string.Empty;
                        }
                        wsSheet1.Cells[cl.ToString() + lineNumber].Value = vend.QuotationPrices.VendorUnits;
                        wsSheet1.Cells[cl.ToString() + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        if (!string.IsNullOrEmpty(vend.QuotationPrices.VendorBrand))
                        {
                            vend.QuotationPrices.VendorBrand = vend.QuotationPrices.VendorBrand;
                        }
                        else
                        {
                            vend.QuotationPrices.VendorBrand = string.Empty;
                        }
                        wsSheet1.Cells[cl.ToString() + (Convert.ToInt32(lineNumber) + 1)].Value = vend.QuotationPrices.VendorBrand;
                        wsSheet1.Cells[cl.ToString() + (Convert.ToInt32(lineNumber) + 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        wsSheet1.Cells[cl.ToString() + (Convert.ToInt32(lineNumber) + 2)].Value = vend.QuotationPrices.UnitPrice;
                        wsSheet1.Cells[cl.ToString() + (Convert.ToInt32(lineNumber) + 3)].Value = vend.QuotationPrices.RevUnitPrice;
                        wsSheet1.Cells[cl.ToString() + (Convert.ToInt32(lineNumber) + 4)].Value = ((item.ProductQuantity * vend.QuotationPrices.UnitPrice) * (vend.QuotationPrices.IGst + vend.QuotationPrices.SGst + vend.QuotationPrices.CGst) /100) + " ( " +(vend.QuotationPrices.IGst + vend.QuotationPrices.SGst + vend.QuotationPrices.CGst) + '%' + " )";
                        wsSheet1.Cells[cl.ToString() + (Convert.ToInt32(lineNumber) + 4)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        wsSheet1.Cells[cl.ToString() + (Convert.ToInt32(lineNumber) + 5)].Value = vend.QuotationPrices.ItemPrice;
                        wsSheet1.Cells[cl.ToString() + (Convert.ToInt32(lineNumber) + 6)].Value = vend.QuotationPrices.RevItemPrice;
                        //if (!string.IsNullOrEmpty(vend.QuotationPrices.RegretComments))
                        //{
                        //    vend.QuotationPrices.RegretComments = vend.QuotationPrices.RegretComments;
                        //}
                        //else
                        //{
                        //    vend.QuotationPrices.RegretComments = string.Empty;
                        //}
                        //wsSheet1.Cells[cl.ToString() + (Convert.ToInt32(lineNumber) + 7)].Value = vend.QuotationPrices.RegretComments;
                        cl++;
                       
                    }
                    lineNumber++; // 7
                    lineNumber = lineNumber + 8;

                    //index++;
                }
                lineNumber++;
                //wsSheet1.Cells["G" + lineNumber].Value = "Freight";
                //wsSheet1.Cells["G" + lineNumber + ":I" + lineNumber].Merge = true;
                //wsSheet1.Cells["G" + lineNumber].Style.Font.Bold = true;
                //wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 1)].Value = "Rev Freight";
                //wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 1) + ":I" + (Convert.ToInt32(lineNumber) + 1)].Merge = true;
                //wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 1)].Style.Font.Bold = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 1)].Value = "Total Price";
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 1) + ":I" + (Convert.ToInt32(lineNumber) + 1)].Merge = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 1)].Style.Font.Bold = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 2)].Value = "Rev Total Price";
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 2) + ":I" + (Convert.ToInt32(lineNumber) + 2)].Merge = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 2)].Style.Font.Bold = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 3)].Value = "Warranty";
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 3) + ":I" + (Convert.ToInt32(lineNumber) + 3)].Merge = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 3)].Style.Font.Bold = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 4)].Value = "Price validity";
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 4) + ":I" + (Convert.ToInt32(lineNumber) + 4)].Merge = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 4)].Style.Font.Bold = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 5)].Value = "GST Number";
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 5) + ":I" + (Convert.ToInt32(lineNumber) + 5)].Merge = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 5)].Style.Font.Bold = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 6)].Value = "Delivery terms";
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 6) + ":I" + (Convert.ToInt32(lineNumber) + 6)].Merge = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 6)].Style.Font.Bold = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 7)].Value = "Payment terms";
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 7) + ":I" + (Convert.ToInt32(lineNumber) + 7)].Merge = true;
                wsSheet1.Cells["G" + (Convert.ToInt32(lineNumber) + 7)].Style.Font.Bold = true;

                char clm = 'J';
                   foreach (var vend1 in excelRequirement.ReqItems[0].ReqVendors)
                    {
                    
                        //wsSheet1.Cells[clm.ToString() + lineNumber].Value = vend1.VendorFreight;
                        //wsSheet1.Cells[clm.ToString() + (Convert.ToInt32(lineNumber) + 1)].Value = vend1.RevVendorFreight;
                        wsSheet1.Cells[clm.ToString() + (Convert.ToInt32(lineNumber) + 1)].Value = vend1.TotalInitialPrice;
                        wsSheet1.Cells[clm.ToString() + (Convert.ToInt32(lineNumber) + 2)].Value = vend1.RevVendorTotalPrice;
                        wsSheet1.Cells[clm.ToString() + (Convert.ToInt32(lineNumber) + 3)].Value = vend1.Warranty;
                        wsSheet1.Cells[clm.ToString() + (Convert.ToInt32(lineNumber) + 3)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        wsSheet1.Cells[clm.ToString() + (Convert.ToInt32(lineNumber) + 4)].Value = vend1.Validity;
                        wsSheet1.Cells[clm.ToString() + (Convert.ToInt32(lineNumber) + 4)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        wsSheet1.Cells[clm.ToString() + (Convert.ToInt32(lineNumber) + 5)].Value = vend1.GstNumber;
                        wsSheet1.Cells[clm.ToString() + (Convert.ToInt32(lineNumber) + 5)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        wsSheet1.Cells[clm.ToString() + (Convert.ToInt32(lineNumber) + 6)].Value = vend1.Duration;
                        wsSheet1.Cells[clm.ToString() + (Convert.ToInt32(lineNumber) + 6)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        wsSheet1.Cells[clm.ToString() + (Convert.ToInt32(lineNumber) + 7)].Value = vend1.Payment;
                        wsSheet1.Cells[clm.ToString() + (Convert.ToInt32(lineNumber) + 7)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    clm++;
                   }
                lineNumber = lineNumber + 10;

                wsSheet1.Cells["A" + lineNumber].Value = "Requirement ID";
                wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["H" + lineNumber].Value = excelRequirement.RequirementID;
                wsSheet1.Cells["H" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                wsSheet1.Cells["H" + lineNumber + ":J" + lineNumber].Merge = true;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Title";
                wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["H" + lineNumber].Value = excelRequirement.Title;
                wsSheet1.Cells["H" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                wsSheet1.Cells["H" + lineNumber + ":J" + lineNumber].Merge = true;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Posted Date";
                wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["H" + lineNumber].Value = excelRequirement.PostedOn.ToString();
                wsSheet1.Cells["H" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                wsSheet1.Cells["H" + lineNumber + ":J" + lineNumber].Merge = true;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Posted By";
                wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["H" + lineNumber].Value = excelRequirement.CustFirstName + excelRequirement.CustLastName;
                wsSheet1.Cells["H" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                wsSheet1.Cells["H" + lineNumber + ":J" + lineNumber].Merge = true;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Negotiation Date";
                wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["H" + lineNumber].Value = excelRequirement.StartTime.ToString();
                wsSheet1.Cells["H" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                wsSheet1.Cells["H" + lineNumber + ":J" + lineNumber].Merge = true;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Savings";
                wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":G" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["H" + lineNumber].Value = excelRequirement.Savings;
                wsSheet1.Cells["H" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                wsSheet1.Cells["H" + lineNumber + ":J" + lineNumber].Merge = true;
                lineNumber++;

                
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Comparatives Report
            return Convert.ToBase64String(ms.ToArray());

        }

        public ExcelRequirement Getfwdreqreportforexcel(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            ExcelRequirement excelrequirement = new ExcelRequirement();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_rp_GetReqReportForExcel", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[3].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[3].Rows[0];
                    excelrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    excelrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    excelrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.Now;
                    excelrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.Now;
                    excelrequirement.CurrentDate = DateTime.Now;
                    excelrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    excelrequirement.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    excelrequirement.CustFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    excelrequirement.CustLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[4].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[4].Rows[0];
                    excelrequirement.PreSavings = row["PRE_SAVING"] != DBNull.Value ? Convert.ToDouble(row["PRE_SAVING"]) : 0;
                    excelrequirement.PostSavings = row["REV_SAVING"] != DBNull.Value ? Convert.ToDouble(row["REV_SAVING"]) : 0;
                }

                List<ExcelVendorDetails> ListRV = new List<ExcelVendorDetails>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ExcelVendorDetails RV = new ExcelVendorDetails();

                        #region VENDOR DETAILS
                        RV.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        RV.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        RV.VendorFreight = row["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["VEND_FREIGHT"]) : 0;
                        RV.RevVendorFreight = row["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_FREIGHT"]) : 0;
                        RV.TotalInitialPrice = row["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_TOTAL_PRICE"]) : 0;
                        RV.RevVendorTotalPrice = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
                        RV.Warranty = row["WARRANTY"] != DBNull.Value ? Convert.ToString(row["WARRANTY"]) : string.Empty;
                        RV.Validity = row["VALIDITY"] != DBNull.Value ? Convert.ToString(row["VALIDITY"]) : string.Empty;
                        RV.OtherProperties = row["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row["OTHER_PROPERTIES"]) : string.Empty;
                        RV.Duration = row["DURATION"] != DBNull.Value ? Convert.ToString(row["DURATION"]) : string.Empty;
                        RV.Payment = row["PAYMENT"] != DBNull.Value ? Convert.ToString(row["PAYMENT"]) : string.Empty;
                        RV.GstNumber = row["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row["GST_NUMBER"]) : string.Empty;
                        #endregion VENDOR DETAILS

                        ListRV.Add(RV);
                    }
                }

                List<ExcelRequirementItems> ListRITemp = new List<ExcelRequirementItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        ExcelRequirementItems RI = new ExcelRequirementItems();
                        #region ITEM DETAILS
                        RI.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                        RI.ProductIDorName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        RI.ProductNo = row["PROD_NO"] != DBNull.Value ? Convert.ToString(row["PROD_NO"]) : string.Empty;
                        RI.ProductDescription = row["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row["DESCRIPTION"]) : string.Empty;
                        RI.ProductQuantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
                        RI.ProductQuantityIn = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
                        RI.ProductBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                        RI.OthersBrands = row["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row["OTHER_BRAND"]) : string.Empty;
                        RI.ReqVendors = ListRV.Select(item => (ExcelVendorDetails)item.Clone()).ToList().ToArray();
                        #endregion ITEM DETAILS

                        ListRITemp.Add(RI);
                    }
                }

                List<ExcelQuotationPrices> ListQ = new List<ExcelQuotationPrices>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        ExcelQuotationPrices Q = new ExcelQuotationPrices();

                        #region QUOTATION DETAILS
                        Q.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        Q.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                        Q.UnitPrice = row["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["UNIT_PRICE"]) : 0;
                        Q.RevUnitPrice = row["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE"]) : 0;
                        Q.CGst = row["C_GST"] != DBNull.Value ? Convert.ToDouble(row["C_GST"]) : 0;
                        Q.SGst = row["S_GST"] != DBNull.Value ? Convert.ToDouble(row["S_GST"]) : 0;
                        Q.IGst = row["I_GST"] != DBNull.Value ? Convert.ToDouble(row["I_GST"]) : 0;
                        Q.ItemPrice = row["PRICE"] != DBNull.Value ? Convert.ToDouble(row["PRICE"]) : 0;
                        Q.RevItemPrice = row["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE"]) : 0;
                        Q.IsRegret = row["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row["IS_REGRET"]) > 0 ? true : false) : false;
                        Q.RegretComments = row["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REGRET_COMMENTS"]) : string.Empty;
                        Q.VendorUnits = row["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row["VENDOR_UNITS"]) : string.Empty;
                        Q.VendorBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                       // Q.QuotationUrl = row["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row["QUOTATION_URL"]) : string.Empty;
                        #endregion QUOTATION DETAILS

                        ListQ.Add(Q);

                    }
                }

                for (int i = 0; i < ListRITemp.Count; i++)
                {
                    for (int v = 0; v < ListRITemp[i].ReqVendors.Length; v++)
                    {
                        ListRITemp[i].ReqVendors[v].QuotationPrices = new ExcelQuotationPrices();
                        try
                        {
                            ListRITemp[i].ReqVendors[v].QuotationPrices = ListQ.Where(quotation => quotation.VendorID == ListRITemp[i].ReqVendors[v].VendorID && quotation.ItemID == ListRITemp[i].ItemID).FirstOrDefault();
                        }
                        catch
                        {
                            ListRITemp[i].ReqVendors[v].QuotationPrices = new ExcelQuotationPrices();
                        }
                    }
                }

                excelrequirement.ReqItems = ListRITemp.ToArray();


            }
            catch (Exception ex)
            {
                excelrequirement.ErrorMessage = ex.Message;
            }

            return excelrequirement;
        }

        public List<ConsolidatedReport> GetConsolidatedReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ConsolidatedReport> consolidatedReports = new List<ConsolidatedReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.Now.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.Now.ToString();
                }

                DateTime fromDate = Convert.ToDateTime(from);
                DateTime toDate = Convert.ToDateTime(to);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetConsolidatedReports", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ConsolidatedReport report = new ConsolidatedReport();
                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.UID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                        report.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        report.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                        report.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                        report.Closed = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        report.NoOfVendorsInvited = row["NoOfVendorsInvited"] != DBNull.Value ? Convert.ToInt32(row["NoOfVendorsInvited"]) : 0;
                        report.NoOfvendorsParticipated = row["NoOfvendorsParticipated"] != DBNull.Value ? Convert.ToInt32(row["NoOfvendorsParticipated"]) : 0;
                        report.ReqCategory = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]) : string.Empty;
                       // report.L1RevPrice = row["L1_PRICE"] != DBNull.Value ? Convert.ToDouble(row["L1_PRICE"]) : 0;
                        report.L1BasePrice = row["L1_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row["L1_BASE_PRICE"]) : 0;
                        report.L1Name = row["L1_NAME"] != DBNull.Value ? Convert.ToString(row["L1_NAME"]) : string.Empty;
                       // report.L2RevPrice = row["L2_PRICE"] != DBNull.Value ? Convert.ToDouble(row["L2_PRICE"]) : 0;
                        report.L2BasePrice = row["L2_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row["L2_BASE_PRICE"]) : 0;
                        report.L2Name = row["L2_NAME"] != DBNull.Value ? Convert.ToString(row["L2_NAME"]) : string.Empty;
                        report.L1InitialBasePrice = row["L1_INITIAL_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row["L1_INITIAL_BASE_PRICE"]) : 0;
                        report.L2InitialBasePrice = row["L2_INITIAL_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row["L2_INITIAL_BASE_PRICE"]) : 0;
                        report.BasePriceSavings = row["BASE_PRICE_SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["BASE_PRICE_SAVINGS"]) : 0;
                        report.SavingsPercentage = row["SAVINGS_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS_PERCENTAGE"]) : 0;
                        report.InitialL1BasePrice = row["INITIAL_L1_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row["INITIAL_L1_BASE_PRICE"]) : 0;
                        report.ProductQuantity = row["PRODUCT_QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["PRODUCT_QUANTITY"]) : 0;
                        report.ProductQuantityIn = row["PRODUCT_QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["PRODUCT_QUANTITY_IN"]) : string.Empty;
                        report.POSTED_BY_USER = row["POSTED_BY_USER"] != DBNull.Value ? Convert.ToString(row["POSTED_BY_USER"]) : string.Empty;
                        consolidatedReports.Add(report);
                    }

                    consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();
                }
            }
            catch (Exception ex)
            {
                ConsolidatedReport recuirementerror = new ConsolidatedReport();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public string GetConsolidatedTemplates(string template, string from, string to, int userID, string sessionID)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region Consolidated Report

            if (template.ToUpper().Contains("CONSOLIDATED_REPORT"))
            {

                List<ConsolidatedReport> consolidateReport = GetConsolidatedReports(sessionID, from, to, userID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("CONSOLIDATED_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "CONSOLIDATED REPORT";
                wsSheet1.Cells["A" + lineNumber + ":T" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":T" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":T" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":T" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":T" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":T" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":T" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Req ID";
                wsSheet1.Cells["B" + lineNumber].Value = "Title";
                wsSheet1.Cells["C" + lineNumber].Value = "Category";
                wsSheet1.Cells["D" + lineNumber].Value = "Product";
                wsSheet1.Cells["E" + lineNumber].Value = "Quantity";
                wsSheet1.Cells["F" + lineNumber].Value = "Posted On";
                wsSheet1.Cells["G" + lineNumber].Value = "Posted By";
                wsSheet1.Cells["H" + lineNumber].Value = "Freeze Time";
                wsSheet1.Cells["I" + lineNumber].Value = "Quotation Status";
                wsSheet1.Cells["J" + lineNumber].Value = "Scheduled";
                wsSheet1.Cells["K" + lineNumber].Value = "Initial Least Base Price";
                wsSheet1.Cells["L" + lineNumber].Value = "L1 Company Name";
                wsSheet1.Cells["M" + lineNumber].Value = "L1 Initial Base Price";
                wsSheet1.Cells["N" + lineNumber].Value = "L1 Rev Base Price";
                wsSheet1.Cells["O" + lineNumber].Value = "L2 Company Name";
                wsSheet1.Cells["P" + lineNumber].Value = "L2 Initial Base Price";
                wsSheet1.Cells["Q" + lineNumber].Value = "L2 Rev Base Price";
                wsSheet1.Cells["R" + lineNumber].Value = "Base Price Savings";
                wsSheet1.Cells["S" + lineNumber].Value = "Savings %";
                wsSheet1.Cells["T" + lineNumber].Value = "STATUS";

                wsSheet1.Cells["A" + lineNumber + ":T" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":T" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":T" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":T" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;

                wsSheet1.Cells["A:T"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (var item in consolidateReport)
                {
                    

                    wsSheet1.Cells["A" + lineNumber].Value = item.RequirementID;
                    wsSheet1.Cells["B" + lineNumber].Value = item.Title;
                    wsSheet1.Cells["C" + lineNumber].Value = item.ReqCategory;
                    wsSheet1.Cells["D" + lineNumber].Value = item.ProductQuantity;
                    wsSheet1.Cells["E" + lineNumber].Value = item.ProductQuantityIn;
                    wsSheet1.Cells["F" + lineNumber].Value = item.ReqPostedOn.ToString();
                    wsSheet1.Cells["G" + lineNumber].Value = item.POSTED_BY_USER;
                    wsSheet1.Cells["H" + lineNumber].Value = item.QuotationFreezTime.ToString();
                    wsSheet1.Cells["I" + lineNumber].Value = item.NoOfvendorsParticipated + "/" + item.NoOfVendorsInvited;
                    if (item.StartTime.ToString().Contains("9999"))
                    {
                        wsSheet1.Cells["J" + lineNumber].Value = "NOT SCHEDULED";
                    }
                    else {
                        wsSheet1.Cells["J" + lineNumber].Value = item.StartTime.ToString();
                    }
                    wsSheet1.Cells["K" + lineNumber].Value = item.InitialL1BasePrice;
                    wsSheet1.Cells["L" + lineNumber].Value = item.L1Name;
                    wsSheet1.Cells["M" + lineNumber].Value = item.L1InitialBasePrice;
                    wsSheet1.Cells["N" + lineNumber].Value = item.L1BasePrice;
                    wsSheet1.Cells["O" + lineNumber].Value = item.L2Name;
                    wsSheet1.Cells["P" + lineNumber].Value = item.L2InitialBasePrice;
                    wsSheet1.Cells["Q" + lineNumber].Value = item.L2BasePrice;
                    wsSheet1.Cells["R" + lineNumber].Value = item.BasePriceSavings;
                    wsSheet1.Cells["S" + lineNumber].Value = item.SavingsPercentage;
                    wsSheet1.Cells["T" + lineNumber].Value = item.Closed;


                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;





                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Consolidated Report

            return Convert.ToBase64String(ms.ToArray());
        }

        public List<OpenPR> GetOpenPR(int compid, int plant, int purchase, string sessionid, int exclusion)
        {
            List<OpenPR> details = new List<OpenPR>();
            try
            {
                Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_EXCLUDE", exclusion);
                CORE.DataNamesMapper<OpenPR> mapper = new CORE.DataNamesMapper<OpenPR>();
                var dataset = sqlHelper.SelectList("sap_GetOpenPRData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                if (plant > 0)
                {
                    details = details.FindAll(d => d.PLANT == plant.ToString());
                }

                if (purchase > 0)
                {
                    details = details.FindAll(d => d.PURCHASING_GROUP == purchase.ToString());
                }


                DateTime today = DateTime.Now;
                foreach (OpenPR PR in details)
                {
                    if (Convert.ToDateTime(PR.REQUISITION_DATE) >= today.AddDays(-6) && Convert.ToDateTime(PR.REQUISITION_DATE) <= today)
                    {
                        PR.FILTER_STRING = "0 to 7 Days";
                    }
                    else if (Convert.ToDateTime(PR.REQUISITION_DATE) >= today.AddDays(-13) && Convert.ToDateTime(PR.REQUISITION_DATE) < today.AddDays(-6))
                    {
                        PR.FILTER_STRING = "7 to 14 Days";
                    }
                    else if (Convert.ToDateTime(PR.REQUISITION_DATE) >= today.AddDays(-20) && Convert.ToDateTime(PR.REQUISITION_DATE) < today.AddDays(-13))
                    {
                        PR.FILTER_STRING = "14 to 21 Days";
                    }
                    else if (Convert.ToDateTime(PR.REQUISITION_DATE) >= today.AddDays(-34) && Convert.ToDateTime(PR.REQUISITION_DATE) < today.AddDays(-20))
                    {
                        PR.FILTER_STRING = "21 to 35 Days";
                    }
                    else if (Convert.ToDateTime(PR.REQUISITION_DATE) >= today.AddDays(-59) && Convert.ToDateTime(PR.REQUISITION_DATE) < today.AddDays(-34))
                    {
                        PR.FILTER_STRING = "35 to 60 Days";
                    }
                    else if (Convert.ToDateTime(PR.REQUISITION_DATE) >= today.AddDays(-99) && Convert.ToDateTime(PR.REQUISITION_DATE) < today.AddDays(-59))
                    {
                        PR.FILTER_STRING = "60 to 100 Days";
                    }
                    else if (Convert.ToDateTime(PR.REQUISITION_DATE) < today.AddDays(-99))
                    {
                        PR.FILTER_STRING = "> 100";
                    }                    

                }



            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }

        public List<KeyValuePair> GetOpenPRPivot(int compid, int plant, int purchase, string sessionid, int exclusion)
        {

            List<KeyValuePair> privotDetails = new List<KeyValuePair>();
            List<OpenPR> details = new List<OpenPR>();
            try
            {
                Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_EXCLUDE", exclusion);
                CORE.DataNamesMapper<OpenPR> mapper = new CORE.DataNamesMapper<OpenPR>();
                var dataset = sqlHelper.SelectList("sap_GetOpenPRData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                if (plant > 0)
                {
                    details = details.FindAll(d => d.PLANT == plant.ToString());
                }

                if (purchase > 0)
                {
                    details = details.FindAll(d => d.PURCHASING_GROUP == purchase.ToString());
                }

                DateTime today = DateTime.Now;
                KeyValuePair keyVal1 = new KeyValuePair();
                keyVal1.Key1 = "0 to 7 Days";
                keyVal1.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) >= today.AddDays(-6) && Convert.ToDateTime(d.REQUISITION_DATE) <= today)).Count();
                privotDetails.Add(keyVal1);

                KeyValuePair keyVal2 = new KeyValuePair();
                keyVal2.Key1 = "7 to 14 Days";
                keyVal2.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) >= today.AddDays(-13) && Convert.ToDateTime(d.REQUISITION_DATE) < today.AddDays(-6))).Count();
                privotDetails.Add(keyVal2);

                KeyValuePair keyVal3 = new KeyValuePair();
                keyVal3.Key1 = "14 to 21 Days";
                keyVal3.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) >= today.AddDays(-20) && Convert.ToDateTime(d.REQUISITION_DATE) < today.AddDays(-13))).Count();
                privotDetails.Add(keyVal3);

                KeyValuePair keyVal4 = new KeyValuePair();
                keyVal4.Key1 = "21 to 35 Days";
                keyVal4.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) >= today.AddDays(-34) && Convert.ToDateTime(d.REQUISITION_DATE) < today.AddDays(-20))).Count();
                privotDetails.Add(keyVal4);

                KeyValuePair keyVal5 = new KeyValuePair();
                keyVal5.Key1 = "35 to 60 Days";
                keyVal5.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) >= today.AddDays(-59) && Convert.ToDateTime(d.REQUISITION_DATE) < today.AddDays(-34))).Count();
                privotDetails.Add(keyVal5);

                KeyValuePair keyVal6 = new KeyValuePair();
                keyVal6.Key1 = "60 to 100 Days";
                keyVal6.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) >= today.AddDays(-99) && Convert.ToDateTime(d.REQUISITION_DATE) < today.AddDays(-59))).Count();
                privotDetails.Add(keyVal6);

                KeyValuePair keyVal7 = new KeyValuePair();
                keyVal7.Key1 = "> 100";
                keyVal7.Value1 = details.FindAll(d => (Convert.ToDateTime(d.REQUISITION_DATE) < today.AddDays(-99))).Count();
                privotDetails.Add(keyVal7);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return privotDetails;
        }

        public List<OpenPO> GetOpenPO(int compid, string pono, int plant, int purchase, string sessionid, int exclude)
        {
            List<OpenPO> details = new List<OpenPO>();
            int count = 0;
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_PO_NO", pono);
                sd.Add("P_EXCLUDE", exclude);
                CORE.DataNamesMapper<OpenPO> mapper = new CORE.DataNamesMapper<OpenPO>();
                var dataset = sqlHelper.SelectList("sap_GetOpenPOData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
                
                foreach(var detail in details)
                {
                    count++;

                    if (detail.IDEAL_DELIVERY_DATE != null && detail.IDEAL_DELIVERY_DATE.HasValue)
                    {
                        detail.IDEAL_DELIVERY_MONTH = detail.IDEAL_DELIVERY_DATE.Value.ToString("MMMM-yyyy");

                        DateTime CONVERTED_IDEAL_DELIVERY_DATE = getDateFormt(detail.IDEAL_DELIVERY_DATE, "MM/dd/yyyy");

                        DateTime currentDate = getDateFormt(DateTime.Now, "MM/dd/yyyy");

                        if (detail.IMPORT_LOCAL.Equals("INDIA", StringComparison.InvariantCultureIgnoreCase) && CONVERTED_IDEAL_DELIVERY_DATE < currentDate)
                        {
                            detail.STATUS = "Delivery Missed";
                        }
                        else if (detail.IMPORT_LOCAL.Equals("IMPORT", StringComparison.InvariantCultureIgnoreCase) && CONVERTED_IDEAL_DELIVERY_DATE.AddDays(7) < currentDate)
                        {
                            detail.STATUS = "Delivery Missed";
                        }

                        else
                        {
                            if (detail.IMPORT_LOCAL.ToUpper().Equals("INDIA", StringComparison.InvariantCultureIgnoreCase) && currentDate >= CONVERTED_IDEAL_DELIVERY_DATE.AddDays(-15))
                            {
                                detail.STATUS = "Delivery Alert";
                            }
                            else if (detail.IMPORT_LOCAL.ToUpper().Equals("IMPORT", StringComparison.InvariantCultureIgnoreCase) && currentDate >= CONVERTED_IDEAL_DELIVERY_DATE.AddDays(-38))
                            {
                                detail.STATUS = "Delivery Alert";
                            }
                            else
                            {
                                detail.STATUS = "Time Available";
                            }
                        }
                    }
                }

                if (plant > 0)
                {
                    details = details.FindAll(d => d.PLANT == plant.ToString());
                }

                if (purchase > 0)
                {
                    details = details.FindAll(d => d.PURCHASING_GROUP == purchase.ToString());
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            details = details.OrderBy(d => d.IDEAL_DELIVERY_DATE).ToList();
            return details;
        }

        public List<KeyValuePair> GetOpenPOPivot(int compid, int plant, int purchase, string sessionid)
        {
            List<KeyValuePair> privotDetails = new List<KeyValuePair>();
            List<OpenPO> details = new List<OpenPO>();
            try
            {
                Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_PO_NO", string.Empty);
                CORE.DataNamesMapper<OpenPO> mapper = new CORE.DataNamesMapper<OpenPO>();
                var dataset = sqlHelper.SelectList("sap_GetOpenPOData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
                if (plant > 0)
                {
                    details = details.FindAll(d => d.PLANT == plant.ToString());
                }

                if (purchase > 0)
                {
                    details = details.FindAll(d => d.PURCHASING_GROUP == purchase.ToString());
                }

                DateTime today = DateTime.Now;
                KeyValuePair keyVal1 = new KeyValuePair();
                keyVal1.Key1 = "DELIVERY MISSED";
                keyVal1.Value1 = details.FindAll(d => Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) < today).Count();
                privotDetails.Add(keyVal1);

                KeyValuePair keyVal2 = new KeyValuePair();
                keyVal2.Key1 = "WITHIN 0 to 7 Days";
                keyVal2.Value1 = details.FindAll(d => (Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) >= today && Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) < today.AddDays(7))).Count();
                privotDetails.Add(keyVal2);

                KeyValuePair keyVal3 = new KeyValuePair();
                keyVal3.Key1 = "WITHIN 7 to 14 Days";
                keyVal3.Value1 = details.FindAll(d => (Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) >= today.AddDays(7) && Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) < today.AddDays(14))).Count();
                privotDetails.Add(keyVal3);

                KeyValuePair keyVal4 = new KeyValuePair();
                keyVal4.Key1 = "WITHIN 14 to 21 Days";
                keyVal4.Value1 = details.FindAll(d => (Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) >= today.AddDays(14) && Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) < today.AddDays(21))).Count();
                privotDetails.Add(keyVal4);

                KeyValuePair keyVal5 = new KeyValuePair();
                keyVal5.Key1 = "WITHIN 21 to 35 Days";
                keyVal5.Value1 = details.FindAll(d => (Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) >= today.AddDays(21) && Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) < today.AddDays(35))).Count();
                privotDetails.Add(keyVal5);

                KeyValuePair keyVal6 = new KeyValuePair();
                keyVal6.Key1 = "WITHIN 35 to 60 Days";
                keyVal6.Value1 = details.FindAll(d => (Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) >= today.AddDays(35) && Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) < today.AddDays(59))).Count();
                privotDetails.Add(keyVal6);

                KeyValuePair keyVal7 = new KeyValuePair();
                keyVal7.Key1 = "> 60 Days";
                keyVal7.Value1 = details.FindAll(d => (Convert.ToDateTime(d.IDEAL_DELIVERY_DATE) > today.AddDays(60))).Count();
                privotDetails.Add(keyVal7);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return privotDetails;
        }

        public List<Response> GetOpenPOComments(string pono, string itemno, string type, string sessionid)
        {
            List<Response> details = new List<Response>();
            try
            {
                Utilities.ValidateSession(sessionid, null);
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                string query = string.Format("SELECT C.*, CONCAT(U_LNAME, ' ', U.U_FNAME) AS FULL_NAME FROM sap_openpo_comments C INNER JOIN User U ON U.U_ID = C.CREATED_BY " +
                                             "WHERE PURCHASING_DOCUMENT = '{0}' AND ITEM = {1} AND TYPE = '{2}' AND CATEGORY!= '-' ORDER BY CREATED DESC", pono, itemno, type);
                var ds = sqlHelper.ExecuteQuery(query);
                if(ds!= null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Response response = new Response();
                        response.Message = row["COMMENTS"] != DBNull.Value ? Convert.ToString(row["COMMENTS"]) : string.Empty;
                        response.ObjectID = row["QUANTITY"] != DBNull.Value ? Convert.ToInt32(row["QUANTITY"]) : 0;
                        response.ErrorMessage = row["CREATED"] != DBNull.Value ? Convert.ToDateTime(row["CREATED"]).ToString("dd-MM-yyyy") : "";
                        response.CurrentTime = row["NEW_DELIVERY_DATE"] != DBNull.Value ? Convert.ToDateTime(row["NEW_DELIVERY_DATE"]) : DateTime.MaxValue;
                        response.RemarksType = row["TYPE"] != DBNull.Value ? Convert.ToString(row["TYPE"]) : string.Empty;
                        response.Category = row["CATEGORY"] != DBNull.Value ? Convert.ToString(row["CATEGORY"]) : string.Empty;
                        response.Status = row["STATUS"] != DBNull.Value ? Convert.ToString(row["STATUS"]) : string.Empty;
                        response.ITEMNO = row["ITEM"] != DBNull.Value ? Convert.ToString(row["ITEM"]) : string.Empty;
                        response.PURCHASEDOC = row["PURCHASING_DOCUMENT"] != DBNull.Value ? Convert.ToString(row["PURCHASING_DOCUMENT"]) : string.Empty;
                        response.UserInfo = new UserInfo();
                        response.UserInfo.FirstName = row["FULL_NAME"] != DBNull.Value ? Convert.ToString(row["FULL_NAME"]) : "";
                        details.Add(response);
                    }
                }                
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }

        public List<Response> GetSapAccess(int userid, string sessionid)
        {
            List<Response> details = new List<Response>();
            try
            {
                Utilities.ValidateSession(sessionid, null);
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                string query = string.Format("SELECT * FROM SAP_Access WHERE U_ID = {0}", userid);
                var ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string accessString = row["TEAMS"] != DBNull.Value ? Convert.ToString(row["TEAMS"]) : string.Empty;
                        var accessList = accessString.Split(';');
                        foreach(var access in accessList)
                        {
                            Response response = new Response();
                            response.ErrorMessage = row["ACCESS_TYPE"] != DBNull.Value ? Convert.ToString(row["ACCESS_TYPE"]) : string.Empty;
                            if (!string.IsNullOrEmpty(access))
                            {
                                var val = access.Split(':');
                                response.Message = val[0];
                                response.ObjectID = Convert.ToInt16(val[1]);
                                details.Add(response);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving SAP Access");
            }

            return details;
        }

        public Response GetLastUpdatedDate(string table, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid, null);
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                string query = string.Format("SELECT DATE_MODIFIED FROM {0} ORDER BY DATE_MODIFIED DESC LIMIT 1", table);
                var ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    details.Message = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]).ToString("dd MMM, yyyy") : string.Empty;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving last table updated date.");
            }

            return details;
        }

        public List<OpenPR> GetOpenPRReport(int compid, string sessionid)
        {
            List<OpenPR> details = new List<OpenPR>();
            try
            {
                Utilities.ValidateSession(sessionid, null);
                CORE.DataNamesMapper<OpenPR> mapper = new CORE.DataNamesMapper<OpenPR>();
                string query = @"SELECT C.QUANTITY AS C_QUANTITY, C.CREATED AS COMMENT_CREATED,NEW_DELIVERY_DATE, C.COMMENTS, PLANT, PURCHASING_GROUP, PLANT_NAME, PURCHASER, PURCHASE_REQUISITION, ITEM_OF_REQUISITION,
                    REQUISITION_DATE, MATERIAL, SHORT_TEXT, 
                    IFNULL((select API_NAME From SAPAPIName WHERE HANA_CODE = PR.MATERIAL LIMIT 1),'-') AS API_NAME,
                    IFNULL((select LEAD_TIME From SAPMaterialMaster WHERE CODE = PR.MATERIAL LIMIT 1),'-') AS LEAD_TIME,
                    QUANTITY_REQUESTED, UNIT_OF_MEASURE, DELIVERY_DATE AS DELIV_DATE_FROM_TO,
                    GREATEST(DATE_ADD(REQUISITION_DATE, INTERVAL SAPLeadTime(PR.MATERIAL) DAY), DELIVERY_DATE) AS DELIVERY_DATE,
                    '' REMARKS, QUANTITY_ORDERED
                    FROM 
                    (SELECT * FROM (SELECT * FROM sap_openpo_comments WHERE TYPE = 'PR_SHORTAGE' 
                    ORDER BY PURCHASING_DOCUMENT DESC, ITEM DESC, CREATED DESC) AS C1 
                    GROUP BY PURCHASING_DOCUMENT, ITEM) AS C
                    INNER JOIN  sap_openpr PR ON PR.PURCHASE_REQUISITION = C.PURCHASING_DOCUMENT AND PR.ITEM_OF_REQUISITION = C.ITEM
                    INNER JOIN sapplantlookup p ON PR.PLANT = P.PLANT_CODE
                    INNER JOIN sappurchaserlookup PL ON PR.PURCHASING_GROUP = PL.PURCHASER_GRP;";

                var dataset = sqlHelper.ExecuteQuery(query);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }

        public List<OpenPO> GetOpenPOReport(int compid, string sessionid)
        {
            List<OpenPO> details = new List<OpenPO>();
            try
            {
                Utilities.ValidateSession(sessionid, null);
                CORE.DataNamesMapper<OpenPO> mapper = new CORE.DataNamesMapper<OpenPO>();
                string query = @"SELECT C.QUANTITY AS C_QUANTITY,NEW_DELIVERY_DATE, C.COMMENTS, C.CREATED AS COMMENT_CREATED, PO.PLANT, P.PLANT_NAME, PO.PURCHASING_GROUP, PL.PURCHASER, PO.PURCHASING_DOCUMENT, PO.ITEM, 
                concat(PO.PURCHASING_DOCUMENT, PO.ITEM) AS 'CONCAT', DOCUMENT_DATE AS PO_DATE, CONCAT(MONTHNAME(NOW()), '-',YEAR(NOW())) AS PO_MONTH,
                PO.NAME_OF_SUPPLIER, PO.MATERIAL, PO.SHORT_TEXT, API.API_NAME, PO.ORDER_UNIT, PO.ORDER_QUANTITY, PO.STILL_TO_BE_DELIVERED_QTY,
                PR.USER_ID, PR.MULTIPLE_PR_TEXT, PR.PR_NUMBER, PR.PR_ITM_NO, PR.PR_CREATE_DATE, PR.PO_DELV_DATE, 
                IF(IFNULL(PR.VENDORS_COUNTRY, 'India') = 'India', 'India', 'Import') AS IMPORT_LOCAL, MAT.LEAD_TIME, 
                GREATEST(DATE_ADD(IFNULL(PR.PR_CREATE_DATE, DATE('1900-01-01')), INTERVAL SAPLeadTime(PO.MATERIAL) DAY), PR.PR_DELV_DATE) AS IDEAL_DELIVERY_DATE,
                CONCAT(MONTHNAME(GREATEST(DATE_ADD(IFNULL(PR.PR_CREATE_DATE, DATE('1900-01-01')), INTERVAL SAPLeadTime(PO.MATERIAL) DAY), PR.PO_DELV_DATE)), '-', YEAR(GREATEST(DATE_ADD(IFNULL(PR.PR_CREATE_DATE, DATE('1900-01-01')), INTERVAL SAPLeadTime(PO.MATERIAL) DAY), PR.PO_DELV_DATE))) AS DELIVERY_MONTH,
                PR.PR_DELV_DATE
                FROM 
                (SELECT * FROM (SELECT * FROM sap_openpo_comments WHERE TYPE = 'SHORTAGE' 
                ORDER BY PURCHASING_DOCUMENT DESC, ITEM DESC, CREATED DESC) AS C1 
                GROUP BY PURCHASING_DOCUMENT, ITEM) AS C
                INNER JOIN sap_openpo PO ON PO.PURCHASING_DOCUMENT = C.PURCHASING_DOCUMENT AND PO.ITEM = C.ITEM
                LEFT JOIN sapplantlookup p ON PO.PLANT = P.PLANT_CODE
                LEFT JOIN sappurchaserlookup PL ON PO.PURCHASING_GROUP = PL.PURCHASER_GRP
                LEFT JOIN sapapiname API ON API.HANA_CODE = PO.MATERIAL
                LEFT JOIN sapmaterialmaster MAT ON MAT.CODE = PO.MATERIAL
                LEFT JOIN(SELECT* FROM sap_prgrn GROUP BY PO_NO, LN_ITM) PR ON PR.PO_NO = PO.PURCHASING_DOCUMENT AND PR.LN_ITM = PO.ITEM
                WHERE TYPE = 'SHORTAGE'";
                var dataset = sqlHelper.ExecuteQuery(query);
                details = mapper.Map(dataset.Tables[0]).ToList();

                foreach(var detail in details)
                {
                    if (string.IsNullOrEmpty(detail.COMMENTS))
                    {
                        detail.COMMENTS = "";
                    }

                    if (string.IsNullOrEmpty(detail.API_NAME))
                    {
                        detail.API_NAME = "";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }

        public void SendDailyReport(int compid, string type, string sessionid)
        {
            List<OpenPO> details = new List<OpenPO>();
            //Dictionary<string, string> purchasers = new Dictionary<string, string>();
            List<OwnEntity> purchasers = new List<OwnEntity>();
            try
            {
                string dateFormat = "dd/MM/yyyy";
                string subject = string.Empty;
                string days = string.Empty;
                List<OpenPO> list = GetOpenPO(compid, string.Empty, 0, 0, sessionid, 0);
                var dt = sqlHelper.SelectQuery("select * from sappurchaserlookup sapprlp" +
                                               " inner join sap_access sapacc on sapacc.TEAMS like CONCAT('%', sapprlp.PURCHASER, ':', sapprlp.PURCHASER_GRP, '%')" +
                                               " inner join user u on u.U_ID = sapacc.U_ID" +
                                               " where sapacc.TEAMS not like '%ALL%'");
                // var dt = sqlHelper.SelectQuery("select * from sappurchaserlookup");
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach(DataRow row in dt.Rows)
                    {
                        //string purchaser = row["PURCHASER"].ToString();
                        //string email = row["U_EMAIL"].ToString();
                        //purchasers.Add(purchaser, email);
                        OwnEntity oe = new OwnEntity();
                        oe.purchaser = row["PURCHASER"].ToString(); // KEY
                        oe.email = row["U_EMAIL"].ToString(); // VALUE
                        purchasers.Add(oe);

                    }                    
                }
                if (type.Equals("LOCAL", StringComparison.InvariantCultureIgnoreCase))
                {
                    list = list.FindAll(l => l.IDEAL_DELIVERY_DATE.Value.Date > DateTime.Now.Date && (l.IDEAL_DELIVERY_DATE.Value.Date - DateTime.Now.Date).Days == 15);
                    list = list.FindAll(l=>l.IMPORT_LOCAL.Equals("INDIA", StringComparison.InvariantCultureIgnoreCase)).ToList();
                    days = "15";
                    //subject = "Domestic Shipment : <15 days from Ideal Delivery date";
                    subject = "Domestic Shipments due for delivery on Idea Delivery Date";
                }

                if (type.Equals("IMPORT", StringComparison.InvariantCultureIgnoreCase))
                {
                    list = list.FindAll(l => l.IDEAL_DELIVERY_DATE.Value.Date > DateTime.Now.Date && (l.IDEAL_DELIVERY_DATE.Value.Date - DateTime.Now.Date).Days == 45);
                    list = list.FindAll(l => l.IMPORT_LOCAL.Equals("IMPORT", StringComparison.InvariantCultureIgnoreCase)).ToList();
                    days = "45";
                    //subject = "Import Shipment : <45 days from Ideal Delivery Date";
                    subject = "Import Shipments due for delivery on Ideal Delivery Date";
                }


                if(purchasers !=null && purchasers.Count > 0)
                {
                    //foreach (KeyValuePair<string, string> entry in purchasers)
                    foreach (OwnEntity oe in purchasers)
                    {
                        var purchaserItems = list.FindAll(l=>l.PURCHASER.Equals(oe.purchaser, StringComparison.InvariantCultureIgnoreCase)).ToList();
                        if(purchaserItems!=null && purchaserItems.Count > 0)
                        {
                            string emailContent = prm.GenerateEmailBody("SapdailyReportemail");
                            string bodyTemplate = prm.GenerateEmailBody("SapdailyReport_BODY");
                            string body = string.Empty;
                            emailContent = string.Format(emailContent, type, days);
                            foreach (var openpo in purchaserItems)
                            {
                                body = body + string.Format(bodyTemplate, openpo.PLANT_NAME, openpo.PURCHASER,
                                openpo.PURCHASING_DOCUMENT, openpo.ITEM, openpo.USER_ID, openpo.NAME_OF_SUPPLIER, openpo.MATERIAL, openpo.SHORT_TEXT,
                                openpo.API_NAME, openpo.ORDER_UNIT, getStringRoundoff(openpo.ORDER_QUANTITY),
                                getStringRoundoff(openpo.STILL_TO_BE_DELIVERED_QTY), getStringDateFormt(openpo.PR_CREATE_DATE, dateFormat), getStringDateFormt(openpo.PO_DATE, dateFormat),
                                getStringDateFormt(openpo.PR_DELV_DATE, dateFormat), openpo.IMPORT_LOCAL, openpo.LEAD_TIME, getStringDateFormt(openpo.IDEAL_DELIVERY_DATE, dateFormat));
                            }

                            emailContent = emailContent.Replace("%%BODY%%", body);
                            emailContent = emailContent.Replace("%%BODY%%", body).Replace("USER_NAME.", "");

                            string emails = string.Empty;
                            if (!string.IsNullOrEmpty(oe.email))
                            {
                                dt = sqlHelper.SelectQuery("SELECT UD.U_ID, U.U_EMAIL FROM sappurchaserlookup SL INNER JOIN companydepartments CD ON SL.PURCHASER  = CD.DEPT_CODE INNER JOIN userdepartments UD ON UD.DEPT_ID = CD.DEPT_ID INNER JOIN User U ON U.U_ID = UD.U_ID WHERE UD.IS_VALID = 1 AND SL.PURCHASER = '" + oe.email + "'");
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        if(row["U_EMAIL"] != null && !string.IsNullOrEmpty(row["U_EMAIL"].ToString()))
                                        {
                                            emails = emails + row["U_EMAIL"].ToString() + ",";
                                        }
                                    }
                                }

                                emails = emails.Trim(',');
                                emails = emails + "," + oe.email;
                            }

                            emails = emails.Trim(',');
                            if (!string.IsNullOrEmpty(emails))
                            {
                                prm.SendEmail(oe.email, subject, emailContent, null, null, null).ConfigureAwait(false);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }
        }

        #endregion

        #region Post

        public Response SavePOComments(string pono, string itemno, double quantity, string comments, DateTime newdeliverdate, string type, 
            int userid, string materialdescription, string apiname,string sitename, int compid, int plant, int purchase, 
            string sessionid, OpenPR openpr, OpenPO openpo, string category,string status)
        {
            string dateFormat = "dd/MM/yyyy";
            Response response = new Response();
            try
            {

                int isValidSession = Utilities.ValidateSession(sessionid, null);
                string query = string.Format("INSERT INTO sap_openpo_comments (PURCHASING_DOCUMENT, ITEM, QUANTITY, COMMENTS, NEW_DELIVERY_DATE, TYPE, CREATED_BY, CREATED, MODIFIED_BY, MODIFIED,CATEGORY) VALUES " +
                    "('{0}', '{1}', {2}, '{3}', '{4}', '{5}', {6}, NOW(), {6}, NOW(),'{7}');", pono, itemno, quantity, comments, newdeliverdate.ToString("yyyy-MM-dd"), type, userid,category);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                response.ObjectID = 1;
                response.Category = category;
                // response.=status;
                UserInfo user = prm.GetUserNew(userid, sessionid);
                //List<OpenPR> openpr = GetOpenPR(compid, plant, purchase, sessionid);

                string body = string.Empty;
                string subject = string.Empty;
                string purchaserCode = string.Empty;

                // Please Donot Clear The Code Unless U Know What You are Doing



                //if (type == "SHORTAGE") {
                //    purchaserCode = openpo.PURCHASER;
                //    subject = "Shortage of " + materialdescription + " for " + apiname + " - Open PO ";
                //    body = prm.GenerateEmailBody("sap_po_shortage_email");
                //    body = String.Format(body, user.FirstName, user.LastName, DateTime.Now, pono, itemno,
                //        Math.Round(quantity, 0), comments, newdeliverdate.ToString(dateFormat), openpo.PLANT_NAME, openpo.PURCHASER,
                //        openpo.PURCHASING_DOCUMENT, openpo.ITEM, openpo.USER_ID, getStringDateFormt(openpo.PO_DATE, dateFormat), openpo.NAME_OF_SUPPLIER,
                //        openpo.MATERIAL, openpo.SHORT_TEXT, openpo.API_NAME, openpo.ORDER_UNIT, getStringRoundoff(openpo.ORDER_QUANTITY),
                //        getStringRoundoff(openpo.STILL_TO_BE_DELIVERED_QTY), openpo.PR_NUMBER, openpo.PR_ITM_NO, getStringDateFormt(openpo.PR_CREATE_DATE, dateFormat), getStringDateFormt(openpo.PR_DELV_DATE, dateFormat), 
                //        openpo.IMPORT_LOCAL, openpo.LEAD_TIME, getStringDateFormt(openpo.IDEAL_DELIVERY_DATE, dateFormat));
                //}
                //else if (type == "DELIVERY")
                //{
                //    purchaserCode = openpo.PURCHASER;
                //    subject = "Delay in Delivery of " + materialdescription + " for " + apiname + " at " + openpo.PLANT_NAME;
                //    body = prm.GenerateEmailBody("sap_po_delivery_email");
                //    body = String.Format(body, user.FirstName, user.LastName, DateTime.Now, pono, itemno,
                //        Math.Round(quantity, 0), comments, newdeliverdate.ToString(dateFormat), openpo.PLANT_NAME, openpo.PURCHASER, 
                //        openpo.PURCHASING_DOCUMENT, openpo.ITEM, openpo.USER_ID, getStringDateFormt(openpo.PO_DATE, dateFormat), openpo.NAME_OF_SUPPLIER, 
                //        openpo.MATERIAL, openpo.SHORT_TEXT, openpo.API_NAME, openpo.ORDER_UNIT, getStringRoundoff(openpo.ORDER_QUANTITY),
                //        getStringRoundoff(openpo.STILL_TO_BE_DELIVERED_QTY), openpo.PR_NUMBER, openpo.PR_ITM_NO, getStringDateFormt(openpo.PR_CREATE_DATE, dateFormat), getStringDateFormt(openpo.PR_DELV_DATE, dateFormat), 
                //        openpo.IMPORT_LOCAL, openpo.LEAD_TIME, getStringDateFormt(openpo.IDEAL_DELIVERY_DATE, dateFormat));
                //}
                //else if (type == "PR_SHORTAGE") {
                //    purchaserCode = openpr.PURCHASER;
                //    subject = "Shortage of " + materialdescription + " for " + apiname + " - Open PR ";
                //    body = prm.GenerateEmailBody("sap_pr_shortage_email");
                //    body = String.Format(body, user.FirstName, user.LastName, DateTime.Now, pono, itemno,
                //        Math.Round(quantity, 0), comments, newdeliverdate.ToString(dateFormat), openpr.PLANT_NAME, openpr.PURCHASER, 
                //        openpr.PURCHASE_REQUISITION, openpr.ITEM_OF_REQUISITION, getStringDateFromString(openpr.REQUISITION_DATE, dateFormat), openpr.MATERIAL, openpr.SHORT_TEXT, 
                //        openpr.API_NAME, openpr.UNIT_OF_MEASURE, getStringRoundoff(openpr.QUANTITY_REQUESTED), getStringRoundoff(openpr.QUANTITY_ORDERED), openpr.LEAD_TIME,
                //        getStringDateFromString(openpr.DELIV_DATE_FROM_TO, dateFormat), getStringDateFromString(openpr.DELIVERY_DATE, dateFormat));
                //}



                // Up to here
                if (category == "DELAY")
                {
                    purchaserCode = openpo.PURCHASER;
                    subject = "Delay in Delivery of " + materialdescription + " for " + apiname + " at " + openpo.PLANT_NAME;
                    body = prm.GenerateEmailBody("sap_po_delivery_email");
                    body = String.Format(body, user.FirstName, user.LastName, DateTime.Now, pono, itemno,
                        Math.Round(quantity, 0), comments, newdeliverdate.ToString(dateFormat), openpo.PLANT_NAME, openpo.PURCHASER,
                        openpo.PURCHASING_DOCUMENT, openpo.ITEM, openpo.USER_ID, getStringDateFormt(openpo.PO_DATE, dateFormat), openpo.NAME_OF_SUPPLIER,
                        openpo.MATERIAL, openpo.SHORT_TEXT, openpo.API_NAME, openpo.ORDER_UNIT, getStringRoundoff(openpo.ORDER_QUANTITY),
                        getStringRoundoff(openpo.STILL_TO_BE_DELIVERED_QTY), openpo.PR_NUMBER, openpo.PR_ITM_NO, getStringDateFormt(openpo.PR_CREATE_DATE, dateFormat), getStringDateFormt(openpo.PR_DELV_DATE, dateFormat),
                        openpo.IMPORT_LOCAL, openpo.LEAD_TIME, getStringDateFormt(openpo.IDEAL_DELIVERY_DATE, dateFormat), getDaysFromDates(getStringDateFormt(newdeliverdate, dateFormat), getStringDateFormt(openpo.IDEAL_DELIVERY_DATE, dateFormat)),//getStringDateFormt(openpo.IDEAL_DELIVERY_DATE, dateFormat)
                        category.ToLower());
                }
                else if (category == "SHORTAGE")
                {
                    purchaserCode = openpr.PURCHASER;
                    subject = "Shortage/Availability Issue of " + materialdescription + " for " + apiname + " at "+ openpr.PLANT_NAME;
                    body = prm.GenerateEmailBody("sap_pr_shortage_email");
                    body = String.Format(body, user.FirstName, user.LastName, DateTime.Now, pono, itemno,
                        Math.Round(quantity, 0), comments, newdeliverdate.ToString(dateFormat), openpr.PLANT_NAME, openpr.PURCHASER,
                        openpr.PURCHASE_REQUISITION, openpr.ITEM_OF_REQUISITION, getStringDateFromString(openpr.REQUISITION_DATE, dateFormat), openpr.MATERIAL, openpr.SHORT_TEXT,
                        openpr.API_NAME, openpr.UNIT_OF_MEASURE, getStringRoundoff(openpr.QUANTITY_REQUESTED), getStringRoundoff(openpr.QUANTITY_ORDERED), openpr.LEAD_TIME,
                        getStringDateFromString(openpr.DELIV_DATE_FROM_TO, dateFormat), getStringDateFromString(openpr.DELIVERY_DATE, dateFormat), category.ToLower()+ "/Availability Issue");
                }
                //else if (category == "OTHERS")
                //{
                //    purchaserCode = openpr.PURCHASER;
                //    subject = "Shortage of " + materialdescription + " for " + apiname + " - Open PR ";
                //    body = prm.GenerateEmailBody("sap_pr_shortage_email");
                //    body = String.Format(body, user.FirstName, user.LastName, DateTime.Now, pono, itemno,
                //        Math.Round(quantity, 0), comments, newdeliverdate.ToString(dateFormat), openpr.PLANT_NAME, openpr.PURCHASER,
                //        openpr.PURCHASE_REQUISITION, openpr.ITEM_OF_REQUISITION, getStringDateFromString(openpr.REQUISITION_DATE, dateFormat), openpr.MATERIAL, openpr.SHORT_TEXT,
                //        openpr.API_NAME, openpr.UNIT_OF_MEASURE, getStringRoundoff(openpr.QUANTITY_REQUESTED), getStringRoundoff(openpr.QUANTITY_ORDERED), openpr.LEAD_TIME,
                //        getStringDateFromString(openpr.DELIV_DATE_FROM_TO, dateFormat), getStringDateFromString(openpr.DELIVERY_DATE, dateFormat));
                //}

                string emails = user.Email;
                if (!string.IsNullOrEmpty(purchaserCode))
                {
                    string purchaserCodeEmail = string.Empty;
                    DataSet purchaserDetails = sqlHelper.ExecuteQuery(string.Format("SELECT * FROM sappurchaserlookup WHERE PURCHASER = '{0}'", purchaserCode));
                    if (purchaserDetails != null && purchaserDetails.Tables.Count > 0 && purchaserDetails.Tables[0].Rows.Count > 0)
                    {
                        if (purchaserDetails.Tables[0].Rows[0]["EMAIL"] != null && !string.IsNullOrEmpty(purchaserDetails.Tables[0].Rows[0]["EMAIL"].ToString()))
                        {
                            purchaserCodeEmail = purchaserDetails.Tables[0].Rows[0]["EMAIL"].ToString();
                        }
                    }

                    if (!string.IsNullOrEmpty(emails))
                    {
                        emails = emails + "," + purchaserCodeEmail;
                    }
                    else
                    {
                        emails = purchaserCodeEmail;
                    }
                }

                if (category == "SHORTAGE")
                {
                    string shortageEmails = ConfigurationManager.AppSettings["SHORTAGE_EMAILS"].ToString();
                    emails = string.IsNullOrEmpty(emails) ? shortageEmails : emails + "," + shortageEmails;
                }

                if (category == "DELAY")
                {
                    string shortageEmails = ConfigurationManager.AppSettings["DELAY_EMAILS"].ToString();
                    emails = string.IsNullOrEmpty(emails) ? shortageEmails : emails + "," + shortageEmails;
                }


                emails = emails.Trim(',');
                if (!string.IsNullOrEmpty(emails) && (category == "SHORTAGE" || category == "DELAY"))
                {
                    prm.SendEmail(emails, subject, body, sessionid, null, null).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        
        public Response UpdatePoStatus(string pono, string itemno, string sessionid, string status,int userid)
        {
       //     string dateFormat = "dd/MM/yyyy";
            Response response = new Response();
             SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {

                int isValidSession = Utilities.ValidateSession(sessionid, null);

                sd.Add("P_PO_NO", pono);
                sd.Add("P_ITEM_NO", itemno);
                sd.Add("P_STATUS", status);
                sd.Add("P_USER", userid);
                DataSet ds = sqlHelper.SelectList("sap_UpdatePOStatus", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    //  response.ObjectID = 2;
                    //response.ErrorMessage = status;
                    //response.PR_ITM_NO = itemno;
                    //response.PR_NUMBER = pono;
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        // response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                        response.PR_NUMBER = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][0].ToString()) : string.Empty;
                        response.PR_ITM_NO = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                        response.ErrorMessage = ds.Tables[0].Rows[0][2] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][2].ToString()) : string.Empty;

                    }
                }

                //string query = string.Format("SELECT COUNT(*) FROM sap_openpo_comments" +
                //                             " WHERE PURCHASING_DOCUMENT = '{0}' AND ITEM = {1} ORDER BY CREATED DESC limit 1", pono, itemno);
                //var ds = sqlHelper.ExecuteQuery(query);
                //if (ds!=null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{

                //    // foreach (DataRow row in ds.Tables[0].Rows)ds.Tables[0].Rows
                //    //  {
                //    //  LiveBidding detail = ReportUtility.GetLiveBiddingReportObject(row, count);
                //    //details.Add(detail);
                //    //  }
                //    string query1 = string.Format("UPDATE sap_openpo_comments SET STATUS = '{2}',MODIFIED_BY = {3},MODIFIED = {4}" +
                //                             " WHERE PURCHASING_DOCUMENT = '{0}' AND ITEM = '{1}';", pono, itemno, status, userid, "NOW()");
                //    DataSet ds1 = sqlHelper.ExecuteQuery(query1);
                //    response.ObjectID = 1;

                //    response.ErrorMessage = status;
                //    if (ds != null)
                //    {
                //    }
                //    response.PR_ITM_NO = itemno;
                //    response.PR_NUMBER = pono;
                //    //      }

                //} else if (ds.Tables.Count <=0 && ds.Tables[0].Rows.Count <= 0) {

                //    string query2 = string.Format("INSERT INTO sap_openpo_comments (PURCHASING_DOCUMENT, ITEM, QUANTITY, COMMENTS, NEW_DELIVERY_DATE, TYPE, CREATED_BY, CREATED, MODIFIED_BY, MODIFIED,CATEGORY) VALUES " +
                //    "('{0}', '{1}', {2}, '{3}', '{4}', '{5}', {6}, NOW(), {6}, NOW(),'{7}');", pono, itemno,0.0000,"EMPTY COMMENT", "", "", userid, "");
                //    DataSet ds2 = sqlHelper.ExecuteQuery(query2);
                //    response.ObjectID = 2;
                //    response.ErrorMessage = status;
                //    response.PR_ITM_NO = itemno;
                //    response.PR_NUMBER = pono;

                //}

                //string query = string.Format("UPDATE sap_openpo_comments SET STATUS = '{2}',MODIFIED_BY = {3},MODIFIED = {4}" +
                //                             " WHERE PURCHASING_DOCUMENT = '{0}' AND ITEM = '{1}';", pono, itemno, status, userid, "NOW()");
                //DataSet ds = sqlHelper.ExecuteQuery(query);
                //    response.ObjectID = 1;
                //    response.ErrorMessage = status;
                //    response.PR_ITM_NO = itemno;
                //    response.PR_NUMBER = pono;

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public List<OpenPO> GetOpenPOShortageReport(int compid, string pono, int plant, int purchase, string sessionid, int exclude)
        {
            List<OpenPO> details = new List<OpenPO>();
            List<OpenPO> filterdetails = new List<OpenPO>();
            // int count = 0;
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_PO_NO", pono);
                sd.Add("P_EXCLUDE", 0);
                CORE.DataNamesMapper<OpenPO> mapper = new CORE.DataNamesMapper<OpenPO>();
                var dataset = sqlHelper.SelectList("sap_GetOpenPOData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                foreach (var detail in details)
                {
                  //  count++;

                    if (detail.IDEAL_DELIVERY_DATE != null && detail.IDEAL_DELIVERY_DATE.HasValue)
                    {
                        detail.IDEAL_DELIVERY_MONTH = detail.IDEAL_DELIVERY_DATE.Value.ToString("MMMM-yyyy");

                        DateTime CONVERTED_IDEAL_DELIVERY_DATE = getDateFormt(detail.IDEAL_DELIVERY_DATE, "MM/dd/yyyy");

                        DateTime currentDate = getDateFormt(DateTime.Now, "MM/dd/yyyy");

                        if (detail.IMPORT_LOCAL.Equals("INDIA", StringComparison.InvariantCultureIgnoreCase) && CONVERTED_IDEAL_DELIVERY_DATE < currentDate)
                        {
                            detail.STATUS = "Delivery Missed";
                        }
                        else if (detail.IMPORT_LOCAL.Equals("IMPORT", StringComparison.InvariantCultureIgnoreCase) && CONVERTED_IDEAL_DELIVERY_DATE.AddDays(7) < currentDate)
                        {
                            detail.STATUS = "Delivery Missed";
                        }

                        else
                        {
                            if (detail.IMPORT_LOCAL.ToUpper().Equals("INDIA", StringComparison.InvariantCultureIgnoreCase) && currentDate >= CONVERTED_IDEAL_DELIVERY_DATE.AddDays(-15))
                            {
                                detail.STATUS = "Delivery Alert";
                            }
                            else if (detail.IMPORT_LOCAL.ToUpper().Equals("IMPORT", StringComparison.InvariantCultureIgnoreCase) && currentDate >= CONVERTED_IDEAL_DELIVERY_DATE.AddDays(-38))
                            {
                                detail.STATUS = "Delivery Alert";
                            }
                            else
                            {
                                detail.STATUS = "Time Available";
                            }
                        }
                    }

                    if (detail.COMMENTS!="-" && detail.COMMENTS != "" && detail.COMMENTS != null && detail.COMMENTS != "EMPTY COMMENTS")//!= null || detail.COMMENTS != "" || detail.COMMENTS!="-".ToString()
                    {
                        //details.Add(detail);
                        filterdetails.Add(detail);
                    }
                    else
                    {
                       
                    }

                }

                if (plant > 0)
                {
                    details = details.FindAll(d => d.PLANT == plant.ToString());
                }

                if (purchase > 0)
                {
                    details = details.FindAll(d => d.PURCHASING_GROUP == purchase.ToString());
                }



            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            details = details.OrderBy(d => d.IDEAL_DELIVERY_DATE).ToList();
            return filterdetails;
        }

        public List<OpenPR> GetOpenPRShortageReport(int compid, int plant, int purchase, string sessionid, int exclusion)
        {
            List<OpenPR> details = new List<OpenPR>();
            List<OpenPR> filterdetails = new List<OpenPR>();
            // int count = 0;
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionid, null);
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_EXCLUDE", exclusion);
                CORE.DataNamesMapper<OpenPR> mapper = new CORE.DataNamesMapper<OpenPR>();
                var dataset = sqlHelper.SelectList("sap_GetOpenPRData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                foreach (var detail in details)
                {
                    //  count++;

                    if (detail.COMMENTS != "-" && !string.IsNullOrEmpty(detail.COMMENTS) && detail.COMMENTS != "EMPTY COMMENTS")//!= null || detail.COMMENTS != "" || detail.COMMENTS!="-".ToString()
                    {
                        //details.Add(detail);
                        filterdetails.Add(detail);
                    }
                    else
                    {

                    }

                }


            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }
            
            return filterdetails;
        }

        public Response ImportEntity(ImportEntity entity)
        {
            //throw new Exception("You are in right place.");
            string moreInfo = string.Empty;
            DataTable currentData = new DataTable();
            Response response = new Response();
            string sheetName = string.Empty;

            try
            {

                if (entity.Attachment != null)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        ms.Write(entity.Attachment, 0, entity.Attachment.Length);
                        using (ExcelPackage package = new ExcelPackage(ms))
                        {
                            currentData = Utilities.WorksheetToDataTable(package.Workbook.Worksheets["Open PR"]);
                            sheetName = package.Workbook.Worksheets[1].Name;
                        }
                    }
                }

                #region "OPEN PR"
                if (string.Compare(entity.EntityName, "OPEN PR", StringComparison.InvariantCultureIgnoreCase) == 0 && currentData.Rows.Count > 0)
                {
                    string query = "INSERT INTO `sap_openpr` (`COMP_ID`,`PLANT`,`PURCHASING_GROUP`,`PURCHASE_REQUISITION`,`ITEM_OF_REQUISITION`,`REQUISITION_DATE`," +
                        "`MATERIAL`,`SHORT_TEXT`,`UNIT_OF_MEASURE`, `QUANTITY_REQUESTED`,`TOTAL_VALUE`,`DELIV_DATE_FROM_TO`,`VENDOR`,`PURCHASE_ORDER_DATE`," +
                        "`PURCHASE_ORDER`,`DELIVERY_DATE`,`RELEASE_DATE`,`STATUSCODE`, `DELETION_INDICATOR`,`PURCHASE_ORDER_ITEM`,`SUPPLIER`,`BLOCKING_INDICATOR`," +
                        "`CLOSED`,`QUANTITY_ORDERED`,`GOODS_RECEIPT`) VALUES";
                    string queryValues = string.Empty;
                    int totalColumns = currentData.Columns.Count;
                    foreach (DataRow row in currentData.Rows)
                    {
                        queryValues = queryValues + "(";
                        for (int i = 0; i <= totalColumns - 1; i++)
                        {
                            queryValues = queryValues + "'" + row[0] + "',";
                        }

                        queryValues = queryValues.TrimEnd(',');
                        queryValues = queryValues + "),";

                        query = query + queryValues;
                        queryValues = string.Empty;
                    }

                    query = query.TrimEnd(',');

                    sqlHelper.ExecuteQuery(query);
                }
                #endregion

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message + moreInfo;
            }

            return response;
        }

        #endregion

        

        #region Private

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MySQLConnectionString"].ConnectionString;
        }

        private int ValidateSession(string sessionId)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            if (string.IsNullOrEmpty(sessionId))
            {
                throw new Exception("Session ID is null");
            }
            
            int isValid = 1;
            try
            {
                sd.Add("P_SESSION_ID", sessionId);
                DataSet ds = sqlHelper.SelectList("cp_ValidateSession", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    isValid = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            //catch (MySqlException ex)
            //{
            //    HttpContext.Current.Server.Transfer("/prm360.html#/login");
            //}
            catch (Exception ex)
            {
                isValid = 0;
            }

            if (isValid <= 0)
            {
                throw new Exception("Invalid Session ID passed");
            }

            return isValid;
        }

        private void SaveFile(string fileName, byte[] fileContent)
        {
            PTMGenericServicesClient ptmClient = new PTMGenericServicesClient();
            ptmClient.SaveFileBytes(fileContent, fileName);
        }

        private Response SaveAttachment(string path)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {   
                sd.Add("P_PATH", path);
                DataSet ds = sqlHelper.SelectList("cp_SaveAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        private DateTime getDateFormt(DateTime? datetime, string format) {

            DateTime response = Convert.ToDateTime(datetime);

            if (datetime != null) {
                response = Convert.ToDateTime(Convert.ToDateTime(datetime).ToString(format));
            }

            return response;

        }

        private string getStringDateFormt(DateTime? datetime, string format)
        {

            string response = string.Empty;

            if (datetime != null)
            {
                response = Convert.ToDateTime(datetime).ToString(format);
            }

            return response;

        }

        private string getStringDateFromString(string datetime, string format)
        {

            string response = string.Empty;

            if (datetime != null && datetime != "" && !string.IsNullOrEmpty(datetime))
            {
                response = Convert.ToDateTime(datetime).ToString(format);
            }

            return response;

        }

        private string getStringRoundoff(string number)
        {

            string response = number;

            int val = 0;

            if (number != null && number != "" && !string.IsNullOrEmpty(number)){
                if (Int32.TryParse(number, out val))
                {
                    response = Convert.ToInt32(number).ToString();
                }
                else if (number.Contains("."))
                {
                    response = number.Split('.')[0];
                }
            }

            return response;

        }

        private Double getDaysFromDates(string datetime1, string datetime2)
        {

            Double response = 0;

            DateTime d1 = DateTime.ParseExact(datetime1, "dd/MM/yyyy", null);
            DateTime d2 = DateTime.ParseExact(datetime2, "dd/MM/yyyy", null);

            //(d1.Date - d2.Date).Days;

            if (d1 != null && d1 != null)
            {
                response = (d1 - d2).TotalDays;
            }

            return response;

        }


        #endregion
    }
}