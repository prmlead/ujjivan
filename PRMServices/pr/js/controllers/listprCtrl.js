﻿prmApp
    .controller('listprCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices",
        "PRMCustomFieldService", "$location","PRMPOServices",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, PRMCustomFieldService, $location, PRMPOServices) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.getUserName = userService.getUsername();
            $scope.myAuctions1 = [];
            $scope.myAuctionsFiltred = [];
            $scope.selectedPRItems = [];
            $scope.prmTemplates = [];
            $scope.selectedTemplate = {};
            $scope.selectedRFP;
            $scope.selectedPR;
            $scope.filteredRequirements = [];
            $scope.PRStats = {
                totalPRs: 0,
                newPRs: 0,
                partialPRs: 0,
                inProgressPRs: 0,
                totalPRItems: 0,
                totalRFQPosted: 0
            };
            $scope.prExcelReport = [];

            /********  CONSOLIDATE PR ********/
            $scope.prDet = {
                prLevel: true
            };
            /********  CONSOLIDATE PR ********/

            $scope.filtersList = {
                plantList: [],
                wbsCodeList: [],
                projectTypeList: [],
                projectNameList: [],
                sectionHeadList: [],
                purchaseGroupList1: [],
                statusList: [],
                prTypeList: [],
                locationList: [],
                profitCentreList: [],
                requisitionList: []
            };

            $scope.filters = {
                status: '',
                plant: {},
                projectType: {},
                sectionHead: {},
                wbsCode: {},
                projectName: {},
                purcahseGroup: {},
                material: '',
                docType: '',
                purchaseGroup: '',
                searchKeyword: '',
                profitCentre: {},
                requisitioners: {},
                newPrStatus: {},
                requisitionerName: {},
                searchRequirement: '',
                selectedRequirement: null,
                prToDate: '',
                prFromDate: '',
                prType: '',
                location: ''
            };

            $scope.filters.prToDate = moment().format('YYYY-MM-DD');
            $scope.filters.prFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.PlantsList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.materialGroupList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.purchaseGroupList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.docTypeList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.prStatusList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];

            $scope.searchVendors = '';

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getprlist(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.totalItems2 = 0;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize2 = 5;

            $scope.setPage2 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged2 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            /*PAGINATION CODE*/

            $scope.prTypesArray =
                [
                    {
                        id: '',
                        name: 'ALL'
                    },
                    {
                        id: 'REGULAR_PURCHASE',
                        name: 'Regular Purchase'
                    }
                ];



            $scope.PRList = [];
            $scope.filteredPRList = [];
            $scope.PRItems = [];

            $('.selected-items-box').bind('click', function (e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });
            var isFilter = false;
            $scope.setFilters = function (currentPage) {
                $scope.PRStats.totalPRs = 0;
                $scope.PRStats.newPRs = 0;
                $scope.PRStats.partialPRs = 0;
                $scope.PRStats.totalPRItems = 0;
                $scope.PRStats.totalRFQPosted = 0;
                $scope.PRStats.inProgressPRs = 0;

                if ($scope.prDet.prLevel) {
                    $scope.filteredPRList = $scope.PRList;
                    $scope.totalItems = $scope.filteredPRList.length;


                    if ($scope.filters.searchKeyword || !_.isEmpty($scope.filters.newPrStatus) || !_.isEmpty($scope.filters.profitCentre) || !_.isEmpty($scope.filters.plant) ||
                        !_.isEmpty($scope.filters.projectType) || !_.isEmpty($scope.filters.requisitionerName) ||
                        !_.isEmpty($scope.filters.sectionHead) || !_.isEmpty($scope.filters.wbsCode) || !_.isEmpty($scope.filters.purcahseGroup) ||
                        !_.isEmpty($scope.filters.projectName) || !_.isEmpty($scope.filters.prType) || !_.isEmpty($scope.filters.location)) {
                        // $scope.getprlist(currentPage, 10, $scope.filters.searchKeyword);
                        $scope.getprlist(0, 10, $scope.filters.searchKeyword);
                    } else {

                        if ($scope.initialPRPageArray && $scope.initialPRPageArray.length > 0) {
                            $scope.PRList = $scope.initialPRPageArray;
                            if ($scope.PRList && $scope.PRList.length > 0) {
                                $scope.totalItems = $scope.PRList[0].TOTAL_PR_COUNT;
                                $scope.PRStats.totalPRs = $scope.totalItems;
                                $scope.PRStats.newPRs = $scope.PRList[0].newPRs;
                                $scope.PRStats.partialPRs = $scope.PRList[0].partialPRs;
                                $scope.PRStats.totalPRItems = $scope.PRList[0].totalPRItems;
                                $scope.PRStats.totalRFQPosted = $scope.PRList[0].totalRFQPosted;
                                $scope.PRStats.inProgressPRs = $scope.PRList[0].inProgressPRs;
                                $scope.filteredPRList = $scope.PRList;
                            }

                        }
                    }
                } else {
                    $scope.getPRSBasedOnItem();
                }

            };

            $scope.filterByDate = function () {
                $scope.PRStats.totalPRs = 0;
                $scope.PRStats.newPRs = 0;
                $scope.PRStats.partialPRs = 0;
                $scope.PRStats.totalPRItems = 0;
                $scope.PRStats.totalRFQPosted = 0;
                $scope.PRStats.inProgressPRs = 0;

                if ($scope.prDet.prLevel) {
                    $scope.filteredPRList = $scope.PRList;
                    $scope.totalItems = $scope.filteredPRList.length;
                    $scope.getprlist(0, 10, $scope.filters.searchKeyword);
                } else {
                    $scope.getPRSBasedOnItem();
                }
            };

            $scope.totalCount = 0;
            $scope.searchString = '';
            $scope.initialPRPageArray = [];
            $scope.getprlist = function (recordsFetchFrom, pageSize, searchString) {

                var plant, projectType, sectionHead, wbsCode, profitCentre, purchaseCode, creatorName, clientName, prStatus, fromDate, toDate, prType, location;

                if (_.isEmpty($scope.filters.prFromDate)) {
                    fromDate = '';
                } else {
                    fromDate = $scope.filters.prFromDate;
                }

                if (_.isEmpty($scope.filters.prToDate)) {
                    toDate = '';
                } else {
                    toDate = $scope.filters.prToDate;
                }

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }

                if (_.isEmpty($scope.filters.projectType)) {
                    projectType = '';
                } else if ($scope.filters.projectType && $scope.filters.projectType.length > 0) {
                    var projectTypes = _($scope.filters.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters.sectionHead && $scope.filters.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters.wbsCode && $scope.filters.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters.profitCentre && $scope.filters.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }

                if (_.isEmpty($scope.filters.purcahseGroup)) {
                    purchaseCode = '';
                } else if ($scope.filters.purcahseGroup && $scope.filters.purcahseGroup.length > 0) {
                    var purchaseCodes = _($scope.filters.purcahseGroup)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    purchaseCode = purchaseCodes.join(',');
                }

                if (_.isEmpty($scope.filters.requisitionerName)) {
                    creatorName = '';
                } else if ($scope.filters.requisitionerName && $scope.filters.requisitionerName.length > 0) {
                    var creators = _($scope.filters.requisitionerName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    creatorName = creators.join(',');
                }

                if (_.isEmpty($scope.filters.projectName)) {
                    clientName = '';
                } else if ($scope.filters.projectName && $scope.filters.projectName.length > 0) {
                    var clientNames = _($scope.filters.projectName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    clientName = clientNames.join(',');
                }

                if (_.isEmpty($scope.filters.newPrStatus)) {
                    prStatus = '';
                } else if ($scope.filters.newPrStatus && $scope.filters.newPrStatus.length > 0) {
                    var statusFilters = _($scope.filters.newPrStatus)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    prStatus = statusFilters.join(',');
                }
                if (_.isEmpty($scope.filters.prType)) {
                    prType = '';
                } else if ($scope.filters.prType && $scope.filters.prType.length > 0) {
                    //var prTypeFilters = _($scope.filters.prType)
                    //    .filter(item => item.id)
                    //    .map('id')
                    //    .value();
                    // prType = prTypeFilters.join(',');
                    prType = $scope.filters.prType;
                }
                if (_.isEmpty($scope.filters.location)) {
                    location = '';
                } else if ($scope.filters.location && $scope.filters.location.length > 0) {
                    //var locationFilters = _($scope.filters.location)
                    //    .filter(item => item.name)
                    //    .map('name')
                    //    .value();
                    //location = locationFilters.join(',');
                    if ($scope.filters.location === 'ALL_LOCATIONS') {
                        location = '';
                    } else {
                        location = $scope.filters.location;
                    }

                }

                var params = {
                    "userid": userService.getUserId(),
                    "sessionid": userService.getUserToken(),
                    "deptid": userService.getSelectedUserDepartmentDesignation().deptID,
                    "desigid": userService.getSelectedUserDesigID(),
                    "depttypeid": userService.getSelectedUserDepartmentDesignation().deptTypeID,
                    "plant": plant,
                    "projectType": projectType,
                    "sectionHead": sectionHead,
                    "wbsCode": prType,
                    "profitCentre": location,
                    "purchaseCode": purchaseCode,
                    "creatorName": creatorName,
                    "clientName": clientName,
                    "prStatus": prStatus,
                    "searchString": searchString ? searchString : "",
                    "PageSize": recordsFetchFrom * pageSize,
                    'fromDate': fromDate,
                    'toDate': toDate,
                    "NumberOfRecords": pageSize
                };

                $scope.pageSizeTemp = (params.PageSize + 1);
                //$scope.NumberOfRecords = recordsFetchFrom > 0 ? ((recordsFetchFrom + 1) * pageSize) : $scope.PRStats.totalPRs;
                $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);

                PRMPRServices.getprlist(params)
                    .then(function (response) {
                        //$scope.consolidatedReport = response;
                        let tempSelectedPRs = [];
                        if (response && response.length > 0) {                            
                            response.forEach(function (item, index) {
                                var releaseDateTemp = item.RELEASE_DATE ? moment(item.RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.RELEASE_DATE = releaseDateTemp != '-' ? releaseDateTemp.contains("1970") ? '-' : releaseDateTemp : '-';
                                item.MODIFIED_DATE = userService.toLocalDate(item.MODIFIED_DATE).split(' ')[0];                                
                            });
                        }

                        if (!$scope.downloadExcel) {
                            $scope.PRList = [];
                            $scope.filteredPRList = [];
                            if (response && response.length > 0) {
                                if ($scope.selectedPRItems && $scope.selectedPRItems.length > 0) {
                                    tempSelectedPRs = _($scope.selectedPRItems)
                                        .map('PR_ID')
                                        .value();
                                }

                                response.forEach(function (item, index) {
                                    item.selectPRSForRFQ = false;
                                    if (tempSelectedPRs.length > 0 && tempSelectedPRs.includes(item.PR_ID)) {
                                        item.selectPRSForRFQ = true;
                                    }
                                    item.isPOCheckedAll = false;
                                    $scope.PRList.push(item);
                                    if ($scope.initialPRPageArray.length <= 9) { // Push Initial 10 Records When Page is Loaded because needed in SetFilters function it's getting called every time (need to modify directive code)  #Crap Code need to remove(should think of another solution)
                                        $scope.initialPRPageArray.push(item);
                                    }                                   
                                });
                            }

                            if ($scope.PRList && $scope.PRList.length > 0) {
                                $scope.totalItems = $scope.PRList[0].TOTAL_PR_COUNT;
                                $scope.PRStats.totalPRs = $scope.totalItems;
                                $scope.PRStats.newPRs = $scope.PRList[0].newPRs;
                                $scope.PRStats.partialPRs = $scope.PRList[0].partialPRs;
                                $scope.PRStats.totalPRItems = $scope.PRList[0].totalPRItems;
                                $scope.PRStats.totalRFQPosted = $scope.PRList[0].totalRFQPosted;
                                $scope.PRStats.inProgressPRs = $scope.PRList[0].inProgressPRs;
                                $scope.filteredPRList = $scope.PRList;
                            }
                        } else {
                            if (response && response.length > 0) {
                                $scope.prExcelReport = response;
                                downloadPRExcel()
                            } else {
                                swal("Error!", "No records.", "error");
                                $scope.downloadExcel = false;
                            }
                        }

                    });
            };

            $scope.getprlist(0, 10, $scope.searchString);
            $scope.filterValues = [];

            $scope.getFilterValues = function () {
                var params =
                {
                    "compid": $scope.compId
                };

                let plantListTemp = [];
                let wbsCodeTemp = [];
                let projectTypeTemp = [];
                let projectNameTemp = [];
                let sectionHeadTemp = [];
                let purchaseGroupTemp = [];
                let statusTemp = [];
                let profitCentreTemp = [];
                let requisitionListTemp = [];

                PRMPRServices.getFilterValues(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.TYPE === 'WBS_CODE') {
                                        wbsCodeTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROFIT_CENTER') {
                                        profitCentreTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROJECT_TYPE') {
                                        projectTypeTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'SECTION_HEAD') {
                                        sectionHeadTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROJECT_DESCRIPTION') {
                                        projectNameTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PLANTS') {
                                        plantListTemp.push({ id: +item.NAME, name: item.ID + ' - ' + item.NAME });
                                    } else if (item.TYPE === 'PURCHASE_GROUP') {
                                        // purchaseGroupTemp.push({ id: +item.NAME, name: item.ID });
                                        purchaseGroupTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PR_CREATOR_NAME') {
                                        requisitionListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PR_STATUS') {
                                        statusTemp.push({ id: item.ID, name: item.NAME });
                                    }

                                });

                                $scope.filtersList.prTypeList = $scope.prTypesArray;
                                $scope.filtersList.requisitionList = requisitionListTemp;
                                $scope.filtersList.plantList = plantListTemp;
                                $scope.filtersList.wbsCodeList = wbsCodeTemp;
                                $scope.filtersList.projectTypeList = projectTypeTemp;
                                $scope.filtersList.sectionHeadList = sectionHeadTemp;
                                $scope.filtersList.projectNameList = projectNameTemp;
                                $scope.filtersList.purchaseGroupList1 = purchaseGroupTemp;
                                $scope.filtersList.statusList = statusTemp;
                                $scope.filtersList.profitCentreList = profitCentreTemp;
                            }
                        }
                    });

            };
            $scope.getFilterValues();

            $scope.selectedPRItems = [];
            $scope.currentPR = [];
            $scope.getPrItems = function (prDetails, createRequirement, createPO) {
                if (prDetails) {
                    $scope.currentPR = [];
                    var params = {
                        "prid": prDetails.PR_ID
                    };
                    PRMPRServices.getpritemslist(params)
                        .then(function (response) {
                            $scope.PRItems = response;
                            $scope.PRItems.forEach(function (item, index) {
                                //item.CREATED_DATE = userService.toLocalDate(item.CREATED_DATE).split(' ')[0];
                                //item.DELIVERY_DATE = userService.toLocalDate(item.DELIVERY_DATE).split(' ')[0];
                            });
                            $scope.currentPR.push($scope.PRItems[0].PR_ID);
                            prDetails.PRItems = $scope.PRItems;

                            if (createPO) {
                                $scope.selectAllItemsForPO('', 'ALL', prDetails.isPOCheckedAll);
                            }

                            if (createRequirement) {
                                if (prDetails) {
                                    prDetails.isSelected = true;
                                    //$scope.selectedPRItems = [];
                                    if (prDetails.PRItems) {
                                        prDetails.PRItems.forEach(function (prItem) {
                                            prItem.isSelected = true;
                                        });
                                    }
                                    $scope.selectedPRItems.push(prDetails);
                                    //$scope.createRequirementMultiplePR();
                                }
                                $scope.selectedPRItems = _.uniqBy($scope.selectedPRItems, 'PR_ID');
                            }

                            if (prDetails.selectPRSForRFQ) {
                                prDetails.PRItems.forEach(function (item, index) {
                                    item.isCheckedPrItem = true;
                                });
                            }
                        });
                }
            };


            $scope.goToPrEdit = function (id) {
                var url = $state.href("save-pr-details", { "Id": id });
                window.open(url, '_blank');
            };

            $scope.goToPrAction = function (id) {
                var url = $state.href("save-pr-details", { "Id": id });
                //var url = $state.href("pr-actions", { "Id": id });
                window.open(url, '_self');
            };

            $scope.createRequirement = function (prDetails) {
                $scope.getPrItems(prDetails, true);
            };

            $scope.createRequirementMultiplePR = function () {
                //let validSelectedPRs = _.filter($scope.selectedPRItems, function (prObj) {
                //    return prObj.isSelected;
                //});

                ////if (validSelectedPRs && validSelectedPRs.length > 0) {
                ////    $state.go('save-requirementAdv', { 'prDetailsList': validSelectedPRs });
                ////}
            };

            $scope.addToRequirementList = function (prDetails, item, action) {
                if (action === 'ADD') {
                    item.isSelected = true;
                    prDetails.isSelected = true;
                    let index = _.findIndex($scope.selectedPRItems, function (pr) {
                        return pr.PR_ID === prDetails.PR_ID;
                    });

                    if (index < 0) {
                        $scope.selectedPRItems.push(prDetails);
                    }

                } else {
                    item.isSelected = false;
                    let selectedPRItems = _.filter(prDetails.PRItems, function (prItem) {
                        return prItem.isSelected;
                    });

                    if (selectedPRItems && selectedPRItems.length > 0) {
                        prDetails.isSelected = true;
                    } else {
                        prDetails.isSelected = false;
                    }
                }


                console.log($scope.selectedPRItems);
            };

            $scope.showRequirementButton = function () {
                let isvisible = false;
                $scope.selectedPRNumbers = '';

                if ($scope.selectedPRItems && $scope.selectedPRItems.length > 0) {
                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prObj) {
                        return prObj.isSelected;
                    });

                    if (validSelectedPRs && validSelectedPRs.length > 0) {
                        var prNumbers = _(validSelectedPRs)
                            .map('PR_NUMBER')
                            .value();
                        $scope.selectedPRNumbers = prNumbers.join(',');
                        isvisible = true;

                    }
                }

                return isvisible;
            };

            $scope.showRequirementItemsButton = function () {
                let isvisible = false;

                if ($scope.selectedItemsForRFQ && $scope.selectedItemsForRFQ.length > 0) {

                    let selectedPRItems = _.filter($scope.selectedItemsForRFQ, function (prItem) {
                        return prItem.selectForRFQ && prItem.OVERALL_ITEM_QUANTITY != prItem.RFQ_QUANTITY;
                    });

                    if (selectedPRItems && selectedPRItems.length > 0) {
                        isvisible = true;
                    }
                }
                return isvisible;
            };

            $scope.showLinkToRFP = function (prDetails) {
                $scope.selectedPR = prDetails;
                $scope.getAuctions();
            };

            $scope.getAuctions = function () {
                auctionsService.SearchRequirements({ "search": '', "excludeprlinked": true, "compid": $scope.compId, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if (response) {
                            $scope.myAuctions1 = [];
                            $scope.myAuctionsFiltred = [];
                            $scope.myAuctions1 = response;
                            $scope.myAuctionsFiltred = $scope.myAuctions1;
                        }
                    });
            };

            $scope.LinkToRFP = function () {
                if ($scope.selectedPR && $scope.myAuctionsFiltred && $scope.myAuctionsFiltred.length > 0) {
                    let selectedRFPs = $scope.myAuctionsFiltred.filter(function (rfq) {
                        return rfq.isSelected;
                    });

                    if (selectedRFPs && selectedRFPs.length > 0) {
                        selectedRFPs.forEach(function (rfpDetails, itemIndexs) {
                            var params = {
                                "userid": userService.getUserId(),
                                "sessionid": userService.getUserToken(),
                                "reqid": rfpDetails.requirementId,
                                "prid": $scope.selectedPR.PR_ID
                            };
                            PRMPRServices.linkRFPToPR(params)
                                .then(function (response) {
                                    if (response && response.errorMessage === '') {
                                        growlService.growl("Successfully link to RFP", "success");
                                        angular.element('#linkRFP').modal('hide');
                                        $scope.myAuctionsFiltred = [];
                                        console.log(response);
                                        $scope.myAuctions1.forEach(function (rfpTemp, itemIndexs) {
                                            rfpTemp.isSelected = false;
                                        });
                                        $scope.myAuctionsFiltred.forEach(function (rfpTemp, itemIndexs) {
                                            rfpTemp.isSelected = false;
                                        });
                                    } else {
                                        growlService.growl("Error linking PR to RFP, please contact support team.", "inverse");
                                    }
                                });
                        });
                    }
                }
            };

            $scope.isLast = function (last) {
                var a = '';
                if (last) {
                    a = '';
                } else {
                    a = ',';
                }
                return a;
            };

            $scope.searchRFPs = function (str) {
                var filterText = str ? str.toUpperCase() : '';
                if (!filterText) {
                    $scope.myAuctionsFiltred = $scope.myAuctions1;
                }
                else {
                    $scope.myAuctionsFiltred = $scope.myAuctions1.filter(function (req) {
                        return req.REQ_TITLE.toUpperCase().includes(filterText) || String(req.requirementId) === filterText || String(req.REQ_NUMBER) === filterText;
                    });
                }
            };



            /********  CONSOLIDATE PR ********/
            $scope.prDet = {
                prLevel: true
            };

            $scope.totalItems1 = 0;
            $scope.currentPage1 = 1;
            $scope.itemsPerPage1 = 10;
            $scope.maxSize1 = 5;

            $scope.setPage1 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged1 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };


            $scope.itemsList = [];
            $scope.getPRSBasedOnItem = function () {
                $scope.selectedPRNumbers = '';
                var plant, projectType, sectionHead, wbsCode, profitCentre = '', purchaseCode = '';

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }

                if (_.isEmpty($scope.filters.projectType)) {
                    projectType = '';
                } else if ($scope.filters.projectType && $scope.filters.projectType.length > 0) {
                    var projectTypes = _($scope.filters.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters.sectionHead && $scope.filters.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters.wbsCode && $scope.filters.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters.profitCentre && $scope.filters.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }

                if (_.isEmpty($scope.filters.purcahseGroup)) {
                    purchaseCode = '';
                } else if ($scope.filters.purcahseGroup && $scope.filters.purcahseGroup.length > 0) {
                    var purchaseCodes = _($scope.filters.purcahseGroup)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    purchaseCode = purchaseCodes.join(',');
                }
                if (_.isEmpty($scope.filters.requisitionerName)) {
                    creatorName = '';
                } else if ($scope.filters.requisitionerName && $scope.filters.requisitionerName.length > 0) {
                    var creators = _($scope.filters.requisitionerName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    creatorName = creators.join(',');
                }
                if (_.isEmpty($scope.filters.projectName)) {
                    clientName = '';
                } else if ($scope.filters.projectName && $scope.filters.projectName.length > 0) {
                    var clientNames = _($scope.filters.projectName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    clientName = clientNames.join(',');
                }

                var params = {
                    "plant": plant,
                    "projectType": projectType,
                    "sectionHead": sectionHead,
                    "wbsCode": wbsCode,
                    "profitCentre": profitCentre,
                    "purchaseCode": purchaseCode,
                    "creatorName": creatorName,
                    "clientName": clientName,
                    "search": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : ''
                };

                PRMPRServices.GetIndividualItems(params)
                    .then(function (response) {
                        $scope.itemsList = response;
                        $scope.itemsList.forEach(function (item, index) {
                            item.selectForRFQ = false;
                            item.IsDisabled = false;
                            item.SELECTED_QUANTITY = 0;
                            item.RFQ_QUANTITY = 0;
                            item.FILTERED_PR_COUNT = 0;
                        });

                        //addCategoryToFilters(response);
                        if ($scope.itemsList.length > 0) {
                            $scope.totalItems1 = $scope.itemsList.length;
                        } else {
                            $scope.totalItems1 = 0;
                        }

                    });
            };

            $scope.getPRLevelView = function () {
                $scope.selectedPRNumbers = '';
            };

            $scope.ItemPRS = [];
            $scope.ItemPRSTemp = [];
            $scope.ItemPRSPopUp = [];
            $scope.ItemPRSPopUpTemp = [];

            $scope.GetPRSbyItem = function (prItem, type, productid) {

                var plant, projectType, sectionHead, wbsCode, profitCentre = '';

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }

                if (_.isEmpty($scope.filters.projectType)) {
                    projectType = '';
                } else if ($scope.filters.projectType && $scope.filters.projectType.length > 0) {
                    var projectTypes = _($scope.filters.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters.sectionHead && $scope.filters.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters.wbsCode && $scope.filters.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters.profitCentre && $scope.filters.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }



                var params = {
                    "PRODUCT_ID": prItem.PRODUCT_ID,
                    "plant": plant,
                    "projectType": projectType,
                    "sectionHead": sectionHead,
                    "wbsCode": wbsCode,
                    "profitCentre": profitCentre,
                    "search": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : ''
                };

                if (!prItem.ItemPRS) {
                    PRMPRServices.GetPRSbyItem(params)
                        .then(function (response) {
                            $scope.ItemPRS = response;
                            $scope.ItemPRSPopUp = response;
                            $scope.ItemPRSPopUpTemp = response;
                            $scope.ItemPRSTemp = response;
                            //prItem.FILTERED_PR_COUNT = $scope.ItemPRS.length;
                            var prids = _.uniqBy($scope.ItemPRS, 'PR_NUMBER');
                            prItem.FILTERED_PR_COUNT = prids.length;
                            /**** Disabling For All Items ****/
                            $scope.itemsList.forEach(function (item, index) {
                                item.IsDisabled = false;
                            });
                            /**** Disabling For All Items ****/

                            $scope.ItemPRS.forEach(function (item, index) {
                                //item.CREATED_DATE = moment(item.CREATED_DATE).format("YYYY-MM-DD");

                                var releaseDateTemp = item.RELEASE_DATE ? moment(item.RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.RELEASE_DATE = releaseDateTemp != '-' ? releaseDateTemp.contains("1970") ? '-' : releaseDateTemp : '-';

                                //if (item.COMPLETED_ITEMS === item.TOTAL_ITEMS) {
                                //    
                                //    item.NEW_PR_STATUS = "In Progess"
                                //} else if (item.COMPLETED_ITEMS > 0 && item.COMPLETED_ITEMS < item.TOTAL_ITEMS) {
                                //    item.NEW_PR_STATUS = "Partial"
                                //} else {
                                //    item.NEW_PR_STATUS = "New"
                                //}
                                prItem.isExpanded1 = true;
                                //if (item.PR_APPROVAL_STATUS == 'APPROVED') {
                                //    prItem.isExpanded1 = true;
                                //    prItem.isDisable = false;
                                //}
                                //else {
                                //    prItem.isExpanded1 = false;
                                //    prItem.isDisable = true;
                                //}

                                item.isChecked = prItem.isExpanded1;


                            });



                            $scope.ItemPRSPopUp = $scope.ItemPRS;
                            $scope.ItemPRSPopUpTemp = $scope.ItemPRS;
                            $scope.ItemPRSTemp = $scope.ItemPRS;

                            //$scope.ItemPRSPopUp.forEach(function (itemPopup, indexPopup) {
                            //    itemPopup.CREATED_DATE = userService.toLocalDate(itemPopup.CREATED_DATE).split(' ')[0];
                            //    itemPopup.RELEASE_DATE = userService.toLocalDate(itemPopup.RELEASE_DATE).split(' ')[0];
                            //    prItem.isExpanded1 = true;
                            //    itemPopup.isChecked = prItem.isExpanded1;
                            //});

                            //$scope.ItemPRSPopUpTemp.forEach(function (itemPopupTemp, indexPopup) {
                            //    itemPopupTemp.CREATED_DATE = userService.toLocalDate(itemPopupTemp.CREATED_DATE).split(' ')[0];
                            //    itemPopupTemp.RELEASE_DATE = userService.toLocalDate(itemPopupTemp.RELEASE_DATE).split(' ')[0];
                            //    prItem.isExpanded1 = true;
                            //    itemPopupTemp.isChecked = prItem.isExpanded1;
                            //});

                            $scope.totalItems2 = $scope.ItemPRSPopUp.length;


                            calculateQuantity($scope.ItemPRS, prItem);
                            $scope.ItemPRSTemp.forEach(function (item, index) {
                                //item.CREATED_DATE = userService.toLocalDate(item.CREATED_DATE);
                                prItem.isExpanded1 = true;
                                //if (item.PR_APPROVAL_STATUS == 'APPROVED') {
                                //    prItem.isExpanded1 = true;
                                //    prItem.isDisable = false;
                                //}
                                //else {
                                //    prItem.isExpanded1 = false;
                                //    prItem.isDisable = true;
                                //}
                                item.isChecked = prItem.isExpanded1;
                            });
                            prItem.ItemPRS = $scope.ItemPRS;

                            prItem.ItemPRSTemp = prItem.ItemPRS;

                            /********** RFQ Posting With Selected Items ***********/
                            if (prItem.selectForRFQ) {
                                $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                            }
                            /********** RFQ Posting With Selected Items ***********/

                            if (type && type === 'ITEM_DISPLAY') {
                                $scope.displayItems(productid);
                            }


                        });
                } else {
                    /**** Disabling For All Items ****/
                    //calculateQuantity($scope.ItemPRS, prItem);
                    $scope.itemsList.forEach(function (item, index) {
                        item.IsDisabled = false;
                    });
                    /**** Disabling For All Items ****/


                    /********** RFQ Posting With Selected Items ***********/
                    if (prItem.selectForRFQ) {
                        $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                    }
                    /********** RFQ Posting With Selected Items ***********/

                    if (type && type === 'ITEM_DISPLAY') {
                        $scope.displayItems(productid);
                    }
                }

            };

            $scope.searchPR = function (searchText, prItem) {
                var filterText = angular.lowercase(searchText);
                if (!filterText || filterText == '' || filterText == undefined || filterText == null) {
                    prItem.ItemPRS = prItem.ItemPRSTemp;
                    $scope.consolidate(prItem, filterText);
                }
                else {
                    prItem.ItemPRS = prItem.ItemPRSTemp.filter(function (pr) {
                        return (String(angular.lowercase(pr.PR_NUMBER)).includes(filterText) == true);
                    });
                }

                $scope.totalItems1 = prItem.ItemPRS.length;

            };


            $scope.unSelectAll = function (prItem, searchedPR) {

                if (!prItem.isExpanded1) {
                    prItem.ItemPRS.forEach(function (item, index) {
                        if (item.REQ_ID <= 0) {
                            item.isChecked = false;
                        }
                    });
                    $scope.consolidate(prItem, searchedPR, prItem.isExpanded1, 'OVERALL');
                } else {
                    prItem.ItemPRS.forEach(function (item, index) {
                        item.isChecked = true;
                    });
                    $scope.consolidate(prItem, searchedPR, prItem.isExpanded1, 'OVERALL');
                }

            };

            $scope.consolidate = function (pr, searchedPR, isChecked, type) {
                if (searchedPR == null || searchedPR == "undefined" || searchedPR == "") {
                    if (pr && pr.ItemPRS.length > 0) {
                        var consolidateQuantity = pr.ItemPRS.filter(function (items) { return items.isChecked && items.REQ_ID <= 0; });
                        var getRFQQuantity = pr.ItemPRS.filter(function (items) { return items.isChecked && items.REQ_ID > 0; });
                        if (consolidateQuantity && consolidateQuantity.length > 0) {
                            pr.SELECTED_QUANTITY = _.sumBy(consolidateQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                            pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                        } else {
                            pr.SELECTED_QUANTITY = 0;
                        }
                    }
                }
                else {
                    searchedPR = angular.lowercase(searchedPR);
                    var searchedFilteredPRS = [];
                    if (pr.isExpanded1) {
                        if (type === 'OVERALL') {
                            searchedFilteredPRS = $scope.ItemPRSTemp;
                            if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                calculateQuantity(searchedFilteredPRS, pr);
                            }
                        } else {
                            if (!isChecked) {
                                var unCheckedPRID = _.result(_.find(pr.ItemPRS, function (items) {
                                    return !items.isChecked;
                                }), 'PR_ID'); // get Unchecked PR IR's

                                searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return item.PR_ID != unCheckedPRID; }); // remove the unchecked PR and calculate

                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }

                            } else {
                                searchedFilteredPRS = $scope.ItemPRSTemp; // Assign All The PR's from DB
                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }
                            }
                        }
                    } else {
                        if (type === 'OVERALL') {

                            var prOverallIds = _(pr.ItemPRS)
                                .filter(item => !item.isChecked)
                                .map('PR_ID')
                                .value();

                            searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return !prOverallIds.includes(item.PR_ID); }); // remove the unchecked PR and calculate

                            if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                calculateQuantity(searchedFilteredPRS, pr);
                            }

                        } else {
                            if (!isChecked) {
                                var itemUnCheckedPRID = _.result(_.find(pr.ItemPRS, function (items) {
                                    return !items.isChecked;
                                }), 'PR_ID'); // get Unchecked PR IR's

                                searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return item.PR_ID != itemUnCheckedPRID; }); // remove the unchecked PR and calculate

                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }

                            } else {
                                searchedFilteredPRS = $scope.ItemPRSTemp; // Assign All The PR's from DB
                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }
                            }
                        }
                    }

                }
            };


            function calculateQuantity(searchedFilteredPRS, pr) {
                var searchConsolidatedQuantity = searchedFilteredPRS.filter(function (items) { return items.isChecked && items.REQ_ID <= 0; }); // calculate all the checked PR's Quantity
                var getRFQQuantity = searchedFilteredPRS.filter(function (items) { return items.isChecked && items.REQ_ID > 0; });
                if (searchConsolidatedQuantity && searchConsolidatedQuantity.length > 0) {
                    pr.SELECTED_QUANTITY = _.sumBy(searchConsolidatedQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                    pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                } else if (getRFQQuantity && getRFQQuantity.length > 0) {
                    pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                } else {
                    pr.SELECTED_QUANTITY = 0;
                }
            }

            function addCategoryToFilters(catArray) {
                var categoryListresp = catArray;
                categoryListresp = _.uniqBy(categoryListresp, 'CATEGORY_ID');
                categoryListresp.forEach(function (item, index) {
                    var catObj = {
                        FIELD_NAME: '',
                        FIELD_VALUE: ''
                    };
                    catObj.FIELD_NAME = item.CategoryName;
                    catObj.FIELD_VALUE = item.CATEGORY_ID;
                    $scope.categoryList.push(catObj);
                });

                $scope.categoryList = _.uniqBy($scope.categoryList, 'FIELD_VALUE');

            }


            $scope.PostRequirement = function (prItem) {
                $scope.itemsList.forEach(function (item, index) {
                    item.IsDisabled = true;
                });
                if (prItem.selectForRFQ) {
                    $scope.GetPRSbyItem(prItem);
                } else {
                    $scope.itemsList.forEach(function (item, index) {
                        item.IsDisabled = false;
                    });

                    $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                }
            };

            $scope.selectedItemsForRFQ = [];
            $scope.AddItemToRequirement = function (prItem, type) {

                if (type) {
                    var checkedPRIDs = _(prItem.ItemPRS)
                        .filter(item => item.isChecked)
                        .map('PR_ID')
                        .value();

                    prItem.PR_ID = checkedPRIDs.join(',');
                    $scope.selectedItemsForRFQ.push(prItem);
                } else {
                    if ($scope.selectedItemsForRFQ && $scope.selectedItemsForRFQ.length > 0) {
                        var itemIndex = _.findIndex($scope.selectedItemsForRFQ, function (item) {
                            return item.PRODUCT_ID === prItem.PRODUCT_ID;
                        });

                        if (itemIndex >= 0) {
                            $scope.selectedItemsForRFQ.splice(itemIndex, 1);
                        }
                    }
                }

                $scope.selectedItemsForRFQ = _.uniqBy($scope.selectedItemsForRFQ, 'PRODUCT_ID');

            };

            $scope.createRFQWithConsolidatedItems = function () {
                let prmFieldMappingDetails = {};
                let isServiceRelatedITems = false;
                let isEmptyItemCode = false;
                var template = $scope.selectedTemplate && $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : 'PRM-DEFAULT';
                var params = {
                    "templateid": 0,
                    "templatename": template,
                    "sessionid": userService.getUserToken()
                };

                var rfqPRSList = $scope.selectedItemsForRFQ.filter(function (item) {
                    return item.selectForRFQ && item.RFQ_QUANTITY != item.OVERALL_ITEM_QUANTITY;
                });

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });

                    if (prmFieldMappingDetails) {
                        prmFieldMappingDetails.IS_SEARCH_FOR_CAS = prmFieldMappingDetails.PRODUCT_CODE.FIELD_LABEL.toLowerCase().contains("cas number");
                        prmFieldMappingDetails.IS_SEARCH_FOR_MFCD = prmFieldMappingDetails.PRODUCT_NUMBER.FIELD_LABEL.toLowerCase().contains("mfcd code");
                        if (!isServiceRelatedITems && prmFieldMappingDetails.IS_SEARCH_FOR_CAS) {
                            isServiceRelatedITems = true;
                        }
                    }

                    rfqPRSList.forEach(function (item, index) {
                        if (!isEmptyItemCode && !(item.ITEM_CODE_CAS || item.CASNR) && !item.REQ_ID) {
                            isEmptyItemCode = true;
                        }
                    });

                    if (isServiceRelatedITems && isEmptyItemCode && false) {
                        swal("Error!", 'Selected template is not Valid as empty CAS number');
                    }
                    else if (rfqPRSList && rfqPRSList.length > 0) {
                        if ($scope.filters.selectedRequirement && $scope.filters.selectedRequirement.requirementId) {
                            $state.go('save-requirementAdv', { 'Id': $scope.filters.selectedRequirement.requirementId, 'prItemsList': rfqPRSList, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        } else {
                            $state.go('save-requirementAdv', { 'prItemsList': rfqPRSList, 'selectedTemplate': $scope.selectedTemplate, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        }
                    }
                });
            };

            /********  CONSOLIDATE PR ********/

            $scope.navigateToRequirement = function () {
                angular.element('#templateSelection').modal('hide');
                if ($scope.prDet.prLevel) {
                    $scope.GetPRMTemplateFields();
                } else {
                    $scope.createRFQWithConsolidatedItems();
                }
            };

            $scope.GetPRMTemplates = function () {
                PRMCustomFieldService.GetTemplates().then(function (response) {
                    $scope.prmTemplates = response;
                    if ($scope.prmTemplates && $scope.prmTemplates.length > 0) {
                        $scope.selectedTemplate = $scope.prmTemplates[0];
                    }
                });
            };

            $scope.GetPRMTemplates();


            $scope.GetPRMTemplateFields = function () {
                let prmFieldMappingDetails = {};
                let isServiceRelatedITems = false;
                let isEmptyItemCode = false;
                var template = $scope.selectedTemplate && $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : 'PRM-DEFAULT';
                var params = {
                    "templateid": 0,
                    "templatename": template,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });

                    if (prmFieldMappingDetails) {
                        prmFieldMappingDetails.IS_SEARCH_FOR_CAS = prmFieldMappingDetails.PRODUCT_CODE.FIELD_LABEL.toLowerCase().contains("cas number");
                        prmFieldMappingDetails.IS_SEARCH_FOR_MFCD = prmFieldMappingDetails.PRODUCT_NUMBER.FIELD_LABEL.toLowerCase().contains("mfcd code");
                        if (!isServiceRelatedITems && prmFieldMappingDetails.IS_SEARCH_FOR_CAS) {
                            isServiceRelatedITems = true;
                        }
                    }

                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                        return prItemObj.isSelected;
                    });

                    validSelectedPRs.forEach(function (prObj, index) {
                        prObj.PRItems.forEach(function (item, index1) {
                            if (prmFieldMappingDetails.IS_SEARCH_FOR_CAS && (item.ITEM_CODE_CAS || item.CASNR)) {
                                item.ITEM_CODE = item.ITEM_CODE_CAS || item.CASNR;
                            }

                            if (prmFieldMappingDetails.IS_SEARCH_FOR_MFCD && (item.ITEM_CODE_MFCD || item.MFCD_NUMBER)) {
                                item.ITEM_NUM = item.ITEM_CODE_MFCD || item.MFCD_NUMBER;
                            }

                            if (!isEmptyItemCode && !(item.ITEM_CODE_CAS || item.CASNR) && !item.REQ_ID && item.isCheckedPrItem) {
                                isEmptyItemCode = true;
                            }
                        });
                    });

                    if (isServiceRelatedITems && isEmptyItemCode && false) {
                        swal("Error!", 'Selected template is not Valid as empty CAS number');
                    }
                    else if (validSelectedPRs && validSelectedPRs.length > 0) {
                        if ($scope.filters.selectedRequirement && $scope.filters.selectedRequirement.requirementId) {
                            $state.go('save-requirementAdv', { 'Id': $scope.filters.selectedRequirement.requirementId, 'prDetailsList': validSelectedPRs, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        } else {
                            $state.go('save-requirementAdv', { 'prDetailsList': validSelectedPRs, 'selectedTemplate': $scope.selectedTemplate, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        }
                    }
                });
            };


            $scope.convertDate = function (date) {
                if (date) {
                    var convertedDateTemp = moment(date).format("DD-MM-YYYY");
                    return convertedDateTemp.contains("1970") ? '-' : convertedDateTemp;
                } else {
                    return '-';
                }
            };


            $scope.ItemsListPopup = [];

            $scope.showPRItemlevelDetails = function (prItem) {
                $scope.GetPRSbyItem(prItem, 'ITEM_DISPLAY', prItem.PRODUCT_ID);
            };

            $scope.displayItems = function (productId) {

                var PR_IDS = '';

                var pridspop = _($scope.ItemPRSPopUp)
                    .filter(item => item.PR_ID)
                    .map('PR_ID')
                    .value();
                PR_IDS = pridspop.join(',');

                var params = {
                    "prIds": PR_IDS
                };

                PRMPRServices.GetItemDetails(params)
                    .then(function (response) {
                        $scope.ItemsListPopup = response;

                        $scope.ItemsListPopup = $scope.ItemsListPopup.filter(function (item) {
                            return item.PRODUCT_ID === productId;
                        });

                    });
            };

            $scope.filterPRsPopup = function (searchText) {

                var filterText = angular.lowercase(searchText);

                if (filterText) {
                    $scope.ItemPRSPopUp = $scope.ItemPRSPopUpTemp.filter(function (pr) {
                        return (String(angular.lowercase(pr.PR_NUMBER)).includes(filterText) == true);
                    });
                } else {
                    $scope.ItemPRSPopUp = $scope.ItemPRSPopUpTemp;
                }

                $scope.totalItems2 = $scope.ItemPRSPopUp.length;
            };

            $scope.selectMultiplePRS = [];

            $scope.createRFQWithMultiplePR = function (prDet) {
                if (prDet.selectPRSForRFQ && $scope.selectedPRItems && $scope.selectedPRItems.length > 0) {
                    prDet.selectPRSForRFQ = false;
                    swal("Only one PR can be selected to post an requirement.", '', "warning");
                } else {
                    if (prDet.selectPRSForRFQ) {
                        var getCheckedPRIDs = _($scope.filteredPRList)
                            .filter(pr => pr.selectPRSForRFQ)
                            .map('PR_ID')
                            .value(); // GET checked PR_ID's

                        if (getCheckedPRIDs && getCheckedPRIDs.length > 0) {

                            var checkedPRObjects = $scope.filteredPRList.filter(function (prItem, prIndex) { return getCheckedPRIDs[0] === prItem.PR_ID; });

                            if (checkedPRObjects && checkedPRObjects.length > 0) {
                                $scope.createRequirement(prDet, true);
                            }
                        }
                    } else {

                        //var getUnCheckedPRIDs = _($scope.selectedPRItems)
                        //    .filter(pr => !pr.selectPRSForRFQ)
                        //    .map('PR_ID')
                        //    .value(); // GET un checked PR_ID's 

                        if (prDet.PRItems && prDet.PRItems.length > 0) {
                            prDet.PRItems.forEach(function (prItem, prIndex) {
                                prItem.isCheckedPrItem = false;
                            });
                        }

                        //var prDetIndex = _.findIndex($scope.selectedPRItems, function (item) {
                        //    return item.PR_ID === getUnCheckedPRIDs[0];
                        //}); // filter the PR in the RFQ posting List and get the Index

                        $scope.selectedPRItems = $scope.selectedPRItems.filter(function (prObj, prIndex) { return prObj.PR_ID !== prDet.PR_ID; });
                        //if (prDetIndex >= 0) { // if PR Found
                        //    $scope.selectedPRItems.splice(prDetIndex, 1); //remove the PR  and PR Items
                        //}
                    }
                }
            };

            $scope.downloadExcel = false;
            $scope.GetReport = function () {
                $scope.PRList = [];
                $scope.downloadExcel = true;
                $scope.getprlist(0, 0, $scope.searchString);
            };

            $scope.searchRequirement = function () {
                $scope.filteredRequirements = [];
                auctionsService.SearchRequirements({ "search": $scope.filters.searchRequirement, "compid": $scope.compId, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if (response) {
                            $scope.filteredRequirements = response;
                        }
                    });
            };

            $scope.selectRequirement = function () {
                //console.log($scope.filters.selectedRequirement);
            };

            function downloadPRExcel() {
                $scope.prExcelReport.forEach(function (item) {
                    item.ITEMS = item.COMPLETED_ITEMS + "/" + item.TOTAL_ITEMS;
                    item.PENDINGBY = item.PR_STATUS + " " + (item.CurrentApproverName == '' || item.CurrentApproverName == null || item.CurrentApproverName == undefined ? '' : 'By') + " " + item.CurrentApproverName;
                })
                alasql('SELECT PR_NUMBER as [PR Number],PR_TYPE as [PR Type],USER_LOCATION1 as [Company Name], ' +
                    'CREATED_BY_NAME as [PR Creator Name],ITEMS as [Items], NEW_PR_STATUS as [Status],RELEASE_DATE as [Created Date], ' +
                    'DEPARTMENT_NAME as [Purchase Group Name],PENDINGBY as [Approval Status]' +

                    'INTO XLSX(?, { headers: true, sheetid: "PR_DETAILS", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["PR Details.xlsx", $scope.prExcelReport]);
                $scope.downloadExcel = false;
            }

            $scope.goToPrView = function (id) {
                //var url = $state.href("view-pr", { "Id": id });
                //window.open(url, '_blank');
                var url = $state.href("save-pr-details", { "Id": id, "isView": true });
                //var url = $state.href("pr-actions", { "Id": id });
                window.open(url, '_self');
            };

            $scope.vendor = {}
            $scope.page = 0;
            $scope.PageSize = 10;
            $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
            $scope.totalCount = 0;

            $scope.getcompanyVendors = function (IsPaging, searchString) {

                $scope.params =
                {
                    "userID": userService.getUserId(),
                    "sessionID": userService.getUserToken(),
                    "PageSize": $scope.fetchRecordsFrom,
                    "NumberOfRecords": $scope.PageSize,
                    "searchString": searchString ? searchString : ""
                };

                if (IsPaging === 1) {
                    if ($scope.page > -1) {
                        $scope.page++;
                        $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
                        $scope.loaderMore = true;
                        $scope.LoadMessage = ' Loading page ' + $scope.fetchRecordsFrom + ' of ' + $scope.PageSize + ' data...';
                        $scope.result = "color-green";
                        $scope.params.PageSize = $scope.fetchRecordsFrom;
                        $scope.params.searchString = searchString ? searchString : "";

                        if ($scope.totalCount != $scope.vendorsList.length) {
                            userService.GetCompanyVendors($scope.params)
                                .then(function (response) {
                                    //$scope.vendorsList = response;
                                    if (response) {
                                        //response.forEach(function (item) {
                                        //    item.companyVendorCodes = _.uniq(item.companyVendorCodes.split(',').map(function (item) { return item.trim() })).join(',');
                                        //})
                                        for (var a in response) {
                                            $scope.vendorsList.push(response[+a]);
                                        }
                                        $scope.vendorDisplayCollecton = [].concat($scope.vendorsList);
                                        $scope.totalCount = $scope.vendorsList[0].totalVendors;
                                    }
                                });
                        }
                    }
                } else {
                    userService.GetCompanyVendors($scope.params)
                        .then(function (response) {
                            response.forEach(function (item) {
                                item.companyVendorCodes = _.uniq(item.companyVendorCodes.split(',').map(function (item) { return item.trim() })).join(',');
                            })
                            $scope.vendorsList = response;

                            if ($scope.vendorsList.length > 0 && $scope.vendorsList) {
                                $scope.vendorDisplayCollecton = [].concat($scope.vendorsList);
                                $scope.totalCount = $scope.vendorsList[0].totalVendors;
                            }
                        });
                }

            };

            $scope.vendorDisplay1 = '';
            $scope.PAYMENT_TERMS = '';
            $scope.SHIPPED_THROUGH = '';
            $scope.displayPRNumber = function (prDet) {
                $scope.prnum = '';
                $scope.BRANCH_LOCATION = '';
                $scope.PAYMENT_TERMS = '';
                $scope.SHIPPED_THROUGH = '';
                $scope.prnum = prDet.PR_NUMBER;
                $scope.BRANCH_LOCATION = prDet.USER_LOCATION1;
                $scope.getPrItems(prDet, '');
            };

            $scope.displayVendorNames = function (vendor) {
                $scope.vendorDisplay1 = vendor.companyName;
                $scope.vendorDisplay = vendor;
                $scope.selectedObjForPO.VENDOR_NAME = vendor.companyName;
                $scope.selectedObjForPO.VENDOR_ID = vendor.userID;
                $scope.selectedObjForPO.VENDOR_CODE = vendor.sapVendorCode;
                $scope.selectedObjForPO.VENDOR_PHONE = vendor.phoneNum;
                $scope.selectedObjForPO.VENDOR_EMAIL = vendor.email;
            };

            $scope.GeneratePO = function () {
                $scope.paymentTermErr = '';
                if (!$scope.PAYMENT_TERMS) {
                    $scope.paymentTermErr = 'Please fill payment terms before generating a PO.';
                    return
                }



                if ($scope.convertToPOArr && $scope.convertToPOArr.length > 0) {
                    /*** For Header Level ***/

                    /*** For Header Level ***/

                    $scope.poItems = [];

                    $scope.convertToPOArr.forEach(function (prItem, prItemIndex) {
                        /*** For Item userseLevel ***/
                        var lineItem = lineItemNum((prItemIndex + 1) * 10);

                        prItem.PAYMENT_TERMS = $scope.PAYMENT_TERMS;
                        prItem.SHIPPED_THROUGH = $scope.SHIPPED_THROUGH;

                        $scope.poObj =
                        {
                            COMP_ID: +$scope.compId,
                            VENDOR_CODE: prItem.VENDOR_CODE,
                            DOC_TYPE: 'PO',
                            VENDOR_COMPANY: prItem.VENDOR_NAME,
                            VENDOR_PHONE: prItem.VENDOR_PHONE,
                            VENDOR_EMAIL: prItem.VENDOR_EMAIL,
                            BRANCH_LOCATION: prItem.BRANCH_LOCATION,
                            PR_NUMBER: prItem.PR_NUMBER,
                            VENDOR_ID: prItem.VENDOR_ID,
                            PO_CREATOR: $scope.getUserName,
                            PRODUCT_ID: prItem.PRODUCT_ID,
                            CATEGORY_ID: prItem.CATEGORY_ID,
                            DELIVERY_DATE: prItem.MATERIAL_DELIVERY_DATE,//userService.convertDateTime(),
                            VENDOR_EXPECTED_DELIVERY_DATE_STRING: moment(prItem.MATERIAL_DELIVERY_DATE).add(330, 'minutes').format("YYYY-MM-DD"),//userService.convertDateTime(),
                            ORDER_QTY: prItem.ASSIGN_QTY,
                            UOM: prItem.UOM,
                            NET_PRICE: (prItem.UNIT_PRICE * prItem.ASSIGN_QTY),
                            PR_ITEM_ID: prItem.PR_ITEM_ID,
                            PR_QTY: prItem.TOTAL_QTY,
                            PR_LINE_ITEM: lineItem,
                            PO_LINE_ITEM: lineItem,
                            DESCRIPTION: prItem.ITEM_NAME,
                            CREATED_BY: +$scope.userID,
                            MODIFIED_BY: +$scope.userID,
                            PAYMENT_TERMS: prItem.PAYMENT_TERMS,
                            SHIPPED_THROUGH: prItem.SHIPPED_THROUGH

                        };
                        $scope.poItems.push($scope.poObj);

                        /*** For Item Level ***/
                    });

                    var params = {
                        "poItems": $scope.poItems,
                        "sessionID": $scope.sessionID
                    };

                    PRMPOServices.GeneratePRToPO(params)
                        .then(function (response) {
                            angular.element('#poConversion').modal('hide');
                            swal("PO Generated Successfully.", '', "success");
                            location.reload();
                        });
                }

            };

            function lineItemNum(num) {
                let maxNum = 5;
                return num.toString().padStart(maxNum, "0");
            };

            $scope.POArrSelection = [];
            $scope.selectedObjForPO = {};
            $scope.selectAllItemsForPO = function (pritems, type, isChecked) {
                if (isChecked) {
                    if (type === 'ALL') {
                        $scope.PRItems.forEach(function (item, index) {
                            var obj =
                            {
                                PR_NUMBER: _.find($scope.filteredPRList, { PR_ID: item.PR_ID }).PR_NUMBER,
                                ITEM_NAME: item.ITEM_NAME,
                                TOTAL_QTY: item.REQUIRED_QUANTITY,
                                PR_ITEM_ID: item.ITEM_ID,
                                PR_ID: item.PR_ID,
                                PRODUCT_ID: item.PRODUCT_ID,
                                CATEGORY_ID: item.CATEGORY_ID,
                                MATERIAL_DELIVERY_DATE: item.MATERIAL_DELIVERY_DATE,
                                TOTAL_PRICE: item.TOTAL_PRICE,
                                UNIT_PRICE: item.UNIT_PRICE,
                                UOM: item.UOM,
                                BRANCH_LOCATION: _.find($scope.filteredPRList, { PR_ID: item.PR_ID }).USER_LOCATION1
                            };
                            item.SelectItemForPO = true;
                            if (item.USED_IN_PO_QTY !== item.REQUIRED_QUANTITY) {
                                $scope.POArrSelection.push(obj);
                            }
                        });

                    } else if (type === 'ITEM') {
                        var obj =
                        {
                            PR_NUMBER: _.find($scope.filteredPRList, { PR_ID: pritems.PR_ID }).PR_NUMBER,
                            ITEM_NAME: pritems.ITEM_NAME,
                            TOTAL_QTY: pritems.REQUIRED_QUANTITY,
                            PR_ITEM_ID: pritems.ITEM_ID,
                            PR_ID: pritems.PR_ID,
                            PRODUCT_ID: pritems.PRODUCT_ID,
                            CATEGORY_ID: pritems.CATEGORY_ID,
                            MATERIAL_DELIVERY_DATE: pritems.MATERIAL_DELIVERY_DATE,
                            TOTAL_PRICE: pritems.TOTAL_PRICE,
                            UNIT_PRICE: pritems.UNIT_PRICE,
                            UOM: pritems.UOM,
                            BRANCH_LOCATION: _.find($scope.filteredPRList, { PR_ID: pritems.PR_ID }).USER_LOCATION1
                        };
                        pritems.SelectItemForPO = true;
                        if (pritems.USED_IN_PO_QTY !== pritems.REQUIRED_QUANTITY) {
                            $scope.POArrSelection.push(obj);
                        }
                        /**** For checking PR HEader level check box */
                        var totalPRItemsCount = _.find($scope.filteredPRList, { PR_ID: pritems.PR_ID }).TOTAL_ITEMS;
                        var alreadyAssignedQty = _.size($scope.PRItems.filter(s => s.USED_IN_PO_QTY === pritems.REQUIRED_QUANTITY));
                        var checkedQty = _.size($scope.POArrSelection);
                        if (+totalPRItemsCount === (+alreadyAssignedQty + +checkedQty)) {
                            var obj = _.find($scope.filteredPRList, { PR_ID: pritems.PR_ID });
                            obj.isPOCheckedAll = true;
                        }
                        /**** For checking PR HEader level check box */
                    }
                } else {
                    if (type === 'ALL') {
                        $scope.POArrSelection = $scope.POArrSelection.filter(s => s.PR_ID !== $scope.PRItems[0].PR_ID);
                        $scope.PRItems.forEach(function (item, index) {
                            item.SelectItemForPO = false;
                        });
                    } else {
                        var obj = _.find($scope.filteredPRList, { PR_ID: pritems.PR_ID });
                        obj.isPOCheckedAll = false;
                        var index = _.findIndex($scope.POArrSelection, function (po) { return po.PR_ID === pritems.PR_ID && po.PR_ITEM_ID === pritems.ITEM_ID });
                        $scope.POArrSelection.splice(index, 1);
                    }
                }
            };

            $scope.validateQty = function (poObj) {
                $scope.selectedObjForPO.ASSIGN_QTY = +$scope.selectedObjForPO.ASSIGN_QTY;
                $scope.validateQtyErr = '';
                if ($scope.selectedObjForPO.ASSIGN_QTY > $scope.selectedObjForPO.ASSIGN_QTY_TEMP) {
                    $scope.validateQtyErr = 'The given quantity Can\'t be more than item quantity';
                    $scope.selectedObjForPO.ASSIGN_QTY = $scope.selectedObjForPO.ASSIGN_QTY_TEMP;
                }
            };

            $scope.autoPopulateItemQty = function (poObj) {
                if (poObj && poObj.PR_ITEM_ID) {
                    var qty = _.find($scope.POArrSelection, { PR_ITEM_ID: poObj.PR_ITEM_ID }).TOTAL_QTY;
                    var checkAlreadyAssignedQty = _.find($scope.convertToPOArr, { PR_ITEM_ID: poObj.PR_ITEM_ID }) ?
                        _.sumBy($scope.convertToPOArr.filter(s => s.PR_ITEM_ID === poObj.PR_ITEM_ID), 'ASSIGN_QTY') :
                        $scope.selectedObjForPO.USED_IN_PO_QTY;
                    $scope.selectedObjForPO.ASSIGN_QTY = +qty - +checkAlreadyAssignedQty;
                    $scope.selectedObjForPO.ASSIGN_QTY_TEMP = +qty - +checkAlreadyAssignedQty;
                }
            };
            $scope.convertToPOArr = [];
            $scope.AddtoPO = function () {
                $scope.addItemErr = '';
                if ($scope.selectedObjForPO.PR_ITEM_ID > 0 && $scope.selectedObjForPO.ASSIGN_QTY > 0 && $scope.selectedObjForPO.VENDOR_ID > 0) {
                    $scope.vendSameItemErr = '';
                    let index = _.findIndex($scope.convertToPOArr, function (po) { return po.PR_ITEM_ID === $scope.selectedObjForPO.PR_ITEM_ID && po.VENDOR_ID === $scope.selectedObjForPO.VENDOR_ID });
                    let assignedTotalQty = _.sumBy($scope.convertToPOArr.filter(s => s.PR_ITEM_ID === $scope.selectedObjForPO.PR_ITEM_ID), 'ASSIGN_QTY');
                    let totalItemQty = _.find($scope.POArrSelection, { PR_ITEM_ID: $scope.selectedObjForPO.PR_ITEM_ID }).TOTAL_QTY;
                    if (index >= 0) {
                        $scope.vendSameItemErr = 'Already Quantity has been assigned to the same vendor.';
                    } else if (assignedTotalQty >= totalItemQty) {
                        $scope.vendSameItemErr = 'All the Quantity for this item is already used.';
                    } else {
                        $scope.selectedObjForPO.PR_NUMBER = _.find($scope.POArrSelection, { PR_ITEM_ID: $scope.selectedObjForPO.PR_ITEM_ID }).PR_NUMBER;
                        $scope.selectedObjForPO.PR_ID = _.find($scope.POArrSelection, { PR_ITEM_ID: $scope.selectedObjForPO.PR_ITEM_ID }).PR_ID;
                        $scope.selectedObjForPO.ITEM_NAME = _.find($scope.POArrSelection, { PR_ITEM_ID: $scope.selectedObjForPO.PR_ITEM_ID }).ITEM_NAME;
                        $scope.selectedObjForPO.PRODUCT_ID = _.find($scope.POArrSelection, { PR_ITEM_ID: $scope.selectedObjForPO.PR_ITEM_ID }).PRODUCT_ID;
                        $scope.selectedObjForPO.CATEGORY_ID = _.find($scope.POArrSelection, { PR_ITEM_ID: $scope.selectedObjForPO.PR_ITEM_ID }).CATEGORY_ID;
                        $scope.selectedObjForPO.TOTAL_PRICE = _.find($scope.POArrSelection, { PR_ITEM_ID: $scope.selectedObjForPO.PR_ITEM_ID }).TOTAL_PRICE;
                        $scope.selectedObjForPO.TOTAL_QTY = _.find($scope.POArrSelection, { PR_ITEM_ID: $scope.selectedObjForPO.PR_ITEM_ID }).TOTAL_QTY;
                        $scope.selectedObjForPO.UOM = _.find($scope.POArrSelection, { PR_ITEM_ID: $scope.selectedObjForPO.PR_ITEM_ID }).UOM;
                        $scope.selectedObjForPO.BRANCH_LOCATION = _.find($scope.POArrSelection, { PR_ITEM_ID: $scope.selectedObjForPO.PR_ITEM_ID }).BRANCH_LOCATION;
                        $scope.selectedObjForPO.MATERIAL_DELIVERY_DATE = _.find($scope.POArrSelection, { PR_ITEM_ID: $scope.selectedObjForPO.PR_ITEM_ID }).MATERIAL_DELIVERY_DATE;
                        $scope.selectedObjForPO.UNIT_PRICE = _.find($scope.POArrSelection, { PR_ITEM_ID: $scope.selectedObjForPO.PR_ITEM_ID }).UNIT_PRICE;
                        $scope.selectedObjForPOTemp = angular.copy($scope.selectedObjForPO);
                        $scope.convertToPOArr.push($scope.selectedObjForPOTemp);
                        $scope.selectedObjForPO = {};
                    }
                } else {
                    $scope.addItemErr = 'Please fill all mandatory fields.';
                }
            };
            $scope.showEdit = false;
            $scope.EditPOItem = function (poItem, index) {
                if (poItem) {
                    $scope.showEdit = true;
                    $scope.validateQtyErr = ''
                    poItem.INDEX = index;
                    $scope.selectedObjForPO = angular.copy(poItem);
                }
            };

            $scope.EditPO = function () {
                var index = +$scope.selectedObjForPO.INDEX;
                $scope.convertToPOArr.splice(index, 1);
                $scope.AddtoPO();
                $scope.showEdit = false;
            };

            $scope.DeletePOItem = function (index) {
                $scope.convertToPOArr.splice(index, 1);
                $scope.showEdit = false;
            };

            $scope.getCurrentPRItemQty = function (prItemID) {
                PRMPRServices.GetCurrentPRItemQty(prItemID)
                    .then(function (response) {
                        $scope.selectedObjForPO.USED_IN_PO_QTY = response;
                        $scope.autoPopulateItemQty($scope.selectedObjForPO);
                    });
            };

            $scope.UpdateSearch = function (val) {
                $scope.fetchRecordsFrom = 0;
                $scope.PageSize = 10;
                $scope.getcompanyVendors(0, val)
            };

            $scope.clearSearch = function () {
                $scope.searchVendors = '';
            };

        }]);