﻿prmApp
    .controller('viewPRCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "growlService", "PRMPRServices", "workflowService", "$rootScope", "catalogService",
        "fileReader",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            growlService, PRMPRServices, workflowService, $rootScope, catalogService,
            fileReader) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.minDateMoment = moment();
            $scope.prID = $stateParams.Id;
            $scope.userName = userService.getFirstname() + ' ' + userService.getLastname();
          //  $scope.userEmail = userService.getEmail();
          //  $scope.userExt = userService.getUserExtension();
          //  $scope.userPhoneNumber = userService.getPhoneNumber();
            $scope.typesOfExpenditure = [
                {
                    display: '------------- Please Select Expenditure Type --------------',
                    value: ''
                },
                {
                    display: 'Equipment',
                    value: '0'
                },
                {
                    display: 'Consumables',
                    value: '1'
                },
                {
                    display: 'Consumables/Services',
                    value: '2'
                }
                //,
                //{
                //    display: '4 Wheeler Petrol',
                //    value: '3'
                //}
                //,
                //{
                //    display: 'Bill Submission',
                //    value: '4'
                //}
            ];


            $scope.natureOfRequirement = [
                {
                    display: '------------- Please Select Nature Of Requirement --------------',
                    value: ''
                },
                {
                    display: 'Product',
                    value: '0'
                },
                {
                    display: 'Service',
                    value: '1'
                },
                {
                    display: 'Product & Service',
                    value: '2'
                }
                //,
                //{
                //    display: '4 Wheeler Petrol',
                //    value: '3'
                //}
                //,
                //{
                //    display: 'Bill Submission',
                //    value: '4'
                //}
            ];



            $scope.companyItemUnits = [];



            $scope.companyDepartments = [];

            $scope.PRDetails = {
                isTabular: true,
                PR_ID: 0,
                U_ID: userService.getUserId(),
                COMP_ID: 0,
                PR_NUMBER: '',
                PR_TYPE: 0,
                ASSET_TYPE: '',
                PRIORITY: '',
                PRIORITY_COMMENTS: '',
                DEPARTMENT: 0,
                REQUEST_DATE: '',
                PROJECT: '',
                ATTACHMENTS: '',
                TOTAL_BASE_PRICE: 0,
                TOTAL_GST_PRICE: 0,
                TOTAL_PRICE: 0,
                SUB_TOTAL: 0,
                GST_PRICE: 0,
                WF_ID: 0,
                //CREATED_BY: 0,
                //CREATED_DATE: '',
                //MODIFIED_BY: 0,
                //MODIFIED_DATE: '',
                PHONE: $scope.userPhoneNumber,
                EMAIL: $scope.userEmail,
                WORK_ORDER_DURATION: '',
                PURPOSE_IN_BRIEF: '',

                PRItemsList: [],
                companyLocation: [],
                SelectedLocationArray: [],
                selectedLocations: '',
                PURCHASE: true,
                PRSHOW: true,
                URGENCY: '',
                PHONE_EXT: $scope.userExt
            };

            $scope.PRItem = {

                ITEM_ID: 0,
                PR_ID: 0,
                ITEM_NAME: '',
                ITEM_CODE: '',
                ITEM_DESCRIPTION: '',
                BRAND: '',
                UNITS: '',
                EXIST_QUANTITY: 0,
                REQUIRED_QUANTITY: 0,
                UNIT_PRICE: 0,
                C_GST_PERCENTAGE: 0,
                S_GST_PERCENTAGE: 0,
                I_GST_PERCENTAGE: 0,
                TOTAL_PRICE: 0,
                COMMENTS: '',
                ATTACHMENTS: '',
                CREATED_BY: 0,
                //CREATED_DATE: '',
                MODIFIED_BY: 0,
                //MODIFIED_DATE: '',
                CATALOGUE_ID: 0,
                U_ID: $scope.userID,
                sessionID: $scope.sessionID,
                itemAttachment: [],
                attachmentName: '',
                BUDGET_ID: 0
            };

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'PR';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/


            $scope.GetSeries = function (series, deptID) {

                $scope.PRDetails.PR_NUMBER = 'AGH/PR/' + userService.getSelectedUserDepartmentDesignation().deptCode;

                //PRMPurchaseRequestServices.GetSeries(series, 'PR', deptID)
                //    .then(function (response) {

                //        if ($scope.PRDetails.PURCHASE == true) {
                //            type = "PR";
                //        } else {
                //            type = "WO"
                //        }
                //        $scope.seriesDB = response.objectID;
                //        $scope.seriesDBDeptCode = response.message;

                //        if ($scope.seriesDB <= 0) {
                //            $scope.seriesDB = 1;
                //        }

                //        if ($scope.PRDetails.PR_NUMBER == '' || $scope.PRDetails.PR_NUMBER == null || $scope.PRDetails.PR_NUMBER == undefined) {

                //            if ($scope.seriesDB < 10) {
                //                $scope.PRDetails.PR_NUMBER = "PR" + '/' + type + '/' + $scope.seriesDBDeptCode + '/000000000' + $scope.seriesDB;
                //            }
                //            if ($scope.seriesDB > 9 && $scope.seriesDB < 100) {
                //                $scope.PRDetails.PR_NUMBER = "PR" + '/' + type + '/' + $scope.seriesDBDeptCode + '/00000000' + $scope.seriesDB;
                //            }
                //            if ($scope.seriesDB > 99 && $scope.seriesDB < 1000) {
                //                $scope.PRDetails.PR_NUMBER = "PR" + '/' + type + '/' + $scope.seriesDBDeptCode + '/0000000' + $scope.seriesDB;
                //            }
                //            if ($scope.seriesDB > 999 && $scope.seriesDB < 10000) {
                //                $scope.PRDetails.PR_NUMBER = "PR" + '/' + type + '/' + $scope.seriesDBDeptCode + '/000000' + $scope.seriesDB;
                //            }

                //            if ($scope.seriesDB > 9999 && $scope.seriesDB < 100000) {
                //                $scope.PRDetails.PR_NUMBER = "PR" + '/' + type + '/' + $scope.seriesDBDeptCode + '/00000' + $scope.seriesDB;
                //            }
                //            if ($scope.seriesDB > 99999 && $scope.seriesDB < 1000000) {
                //                $scope.PRDetails.PR_NUMBER = "PR" + '/' + type + '/' + $scope.seriesDBDeptCode + '/0000' + $scope.seriesDB;
                //            }

                //            if ($scope.seriesDB > 999999 && $scope.seriesDB < 10000000) {
                //                $scope.PRDetails.PR_NUMBER = "PR" + '/' + type + '/' + $scope.seriesDBDeptCode + '/000' + $scope.seriesDB;
                //            }
                //            if ($scope.seriesDB > 9999999 && $scope.seriesDB < 100000000) {
                //                $scope.PRDetails.PR_NUMBER = "PR" + '/' + type + '/' + $scope.seriesDBDeptCode + '/00' + $scope.seriesDB;
                //            }
                //            if ($scope.seriesDB > 99999999 && $scope.seriesDB < 100000000) {
                //                $scope.PRDetails.PR_NUMBER = "PR" + '/' + type + '/' + $scope.seriesDBDeptCode + '/0' + $scope.seriesDB;
                //            }
                //        }

                //    })
            }


            $scope.deptIDs = [];
            $scope.desigIDs = [];
            $scope.getServerTime = function () {
                auctionsService.getdate()
                    .then(function (responseFromServer) {
                        //var serverTime = moment(responseFromServer, "YYYY-MM-DD HH:mm").valueOf();
                        //$scope.PRDetails.REQUEST_DATE = serverTime;


                        //var serverTime = moment(responseFromServer, "YYYY-MM-DD HH:mm").valueOf();
                        //var m1 = moment(serverTime);
                        //REQUEST_DATE = new Date(m1);
                        //REQUEST_DATE = parseInt(serverTime.getTime() / 1000.0);
                        //$scope.PRDetails.REQUEST_DATE = new moment(responseFromServer).format("YYYY-MM-DD HH:mm");

                        var requestDate = userService.toLocalDate(responseFromServer);
                        $scope.PRDetails.REQUEST_DATE = requestDate;
                    });

            }
            if ($scope.prID == 0) {
                $scope.getServerTime();
            }

            auctionsService.GetCompanyDepartments($scope.userID, $scope.sessionID)
                .then(function (response) {
                    $scope.companyDepartments = response;
                })
            $scope.compID = userService.getUserCompanyId();

            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                        }
                    });
                });
            };


            //auctionsService.GetCompanyConfiguration($scope.compID, "ITEM_UNITS", userService.getUserToken())
            //    .then(function (unitResponse) {
            //        $scope.companyItemUnits = unitResponse;
            //        //$scope.companyItemUnitsTemp = $scope.companyItemUnits;
            //        //$scope.companyItemUnits = $scope.companyItemUnitsTemp.forEach(function (item, index) {
            //        //})

            //        $scope.companyItemUnits = $scope.companyItemUnits.filter(function (item) {
            //            if (item.isValid) {
            //                return item;
            //            }
            //        })


            //    });
            $scope.companyLocation = [];
            $scope.companyLocationTemp = [];
            auctionsService.GetCompanyConfiguration($scope.compID, "ITEM_UNITS,USER_LOCATION", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = unitResponse;


                    $scope.companyConfigList = unitResponse;
                    var countriesTemp = [];
                    $scope.companyConfigList.forEach(function (item, index) {
                        if (item.configKey == 'ITEM_UNITS') {
                            $scope.companyItemUnits.push(item);
                        }
                        if (item.configKey == 'USER_LOCATION') {
                            $scope.companyLocationTemp.push(item);
                        }
                    });
                    $scope.companyItemUnits = $scope.companyItemUnits.filter(function (item) {
                        if (item.isValid) {
                            return item;
                        }
                    })
                   
                    $scope.getprdetails();

                });

            $scope.companyLocationSearch = [];
            $scope.getuserLocations = function () {
                $scope.companyLocation = [];
                $scope.companyLocation1 = [];
                $scope.ListUserDepartmentDesignations.forEach(function (deptLoc, deptIdx) {
                    deptLoc.userLocationTemp = deptLoc.userLocation.split(',');
                    deptLoc.userLocationTemp.forEach(function (compLoc, locIdx) {
                        $scope.companyLocation1 = $scope.companyLocationTemp.filter(function (udd) {
                            if (udd.configValue == compLoc) {
                                return udd;
                            };
                        });
                        $scope.companyLocation.push($scope.companyLocation1[0]);
                    })
                })
                $scope.companyLocationSearch = angular.copy($scope.companyLocation);
                $scope.PRDetails.companyLocation = angular.copy($scope.companyLocation);
            }

            $scope.PRDetails.PRItemsList.push($scope.PRItem);

            $scope.addPrItem = function () {
                var PRItem = {
                    ITEM_ID: 0,
                    PR_ID: 0,
                    ITEM_NAME: '',
                    ITEM_CODE: '',
                    ITEM_DESCRIPTION: '',
                    BRAND: '',
                    UNITS: '',
                    EXIST_QUANTITY: 0,
                    REQUIRED_QUANTITY: 0,
                    UNIT_PRICE: 0,
                    C_GST_PERCENTAGE: 0,
                    S_GST_PERCENTAGE: 0,
                    I_GST_PERCENTAGE: 0,
                    TOTAL_PRICE: 0,
                    COMMENTS: '',
                    ATTACHMENTS: '',
                    CREATED_BY: 0,
                    //CREATED_DATE: '',
                    MODIFIED_BY: 0,
                    //MODIFIED_DATE: '',
                    CATALOGUE_ID: 0,
                    U_ID: $scope.userID,
                    sessionID: $scope.sessionID,
                    BUDGET_ID: 0
                };

                $scope.PRDetails.PRItemsList.push(PRItem);

            };

            $scope.checkIsFormDisable = function () {
                $scope.isFormdisabled = true;
                if ($scope.PRDetails.CREATED_BY == $scope.userID) {
                    $scope.isFormdisabled = false;
                }
            };



            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                            }
                        });

                        if (userService.getUserObj().isSuperUser) {
                            $scope.workflowList = $scope.workflowList;
                        }
                        else {
                            $scope.workflowList = $scope.workflowList.filter(function (item) {
                                return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                            });
                        }



                    });
            };

            $scope.getWorkflows();

            $scope.getItemWorkflow = function (paramId, module, subModuleId) {
                workflowService.getItemWorkflow(paramId, module, subModuleId)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

                                if (track.status == 'APPROVED' || track.status == 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status == 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }



                                if (track.status == 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });

            };

            $scope.updateTrack = function (step, status) {

                $scope.commentsError = '';

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = $scope.WorkflowModule;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            //location.reload();
                            $state.go('listPr');
                        }
                    })
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            $state.go('listPr');
                        }
                    })
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if (userService.getSelectedUserDeptID() == step.department.deptID && userService.getSelectedUserDesigID() == step.approver.desigID &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
                                disable = true;
                            }
                            else if (userService.getSelectedUserDeptID() == step.department.deptID && userService.getSelectedUserDesigID() == step.approver.desigID &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                })

                return disable;
            };

            /*region end WORKFLOW*/
            
            $scope.deletePrItem = function (index) {
                if ($scope.PRDetails.PRItemsList.length > 1) {
                    $scope.PRDetails.PRItemsList.splice(index, 1);
                    $scope.prUnitPriceCalculation();
                } else {
                    growlService.growl("Can't Delete this Item", "inverse");
                }
            };
            

            $scope.getprdetails = function () {
                var params = {
                    "prid": $stateParams.Id,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.getprdetails(params)
                    .then(function (response) {

                        $scope.PRDetails = response;
                        if ($scope.PRDetails.TYPE == "PR") {
                            $scope.PRDetails.PURCHASE = true;
                            $scope.PRDetails.PRSHOW = true;
                            //$scope.display($scope.PRDetails.TYPE);
                            $scope.PRDetails.DEPARTMENT_CODE = $scope.PRDetails.DEPARTMENT_NAME;
                        } else {
                            $scope.PRDetails.PURCHASE = false;
                            $scope.PRDetails.PRSHOW = false;
                            //$scope.display($scope.PRDetails.TYPE);
                        }
                       // $scope.PRDetails.REQUEST_DATE = userService.toLocalDate($scope.PRDetails.REQUEST_DATE);
                        $scope.workflowObj.workflowID = $scope.PRDetails.WF_ID;
                        if ($stateParams.Id > 0) {
                            $scope.checkIsFormDisable();
                          //  $scope.getItemWorkflow();
                            $scope.getItemWorkflow($scope.PRDetails.WF_ID, $scope.PRDetails.PR_ID, 'PR', '');
                        }

                        $scope.PRDetails.PRItemsList.forEach(function (item, itemIndex) {
                            if (item.MATERIAL_DELIVERY_DATE) {
                                item.MATERIAL_DELIVERY_DATE = userService.toLocalDate(item.MATERIAL_DELIVERY_DATE);
                            }
                        });
                        if ($scope.isSuperUser) {

                            $scope.PRDetails.companyLocation = $scope.companyLocationTemp;
                        } else {
                            $scope.getuserLocations();
                        }
                    });
            };


            $scope.showPendingApprovals = function (prObj) {
                $scope.getItemWorkflow(prObj.PR_ID, 'PR', prObj.ITEM_ID);
            };

            if ($stateParams.Id > 0) {
                //$scope.getprdetails();
            } else {
                $scope.GetSeries('', userService.getSelectedUserDeptID());

            }


            //$scope.BudgetDetailsList = [];
            //$scope.GetComapanyBudgetDetails = function () {
            //    auctionsService.GetComapanyBudgetDetails($scope.userID, $scope.sessionID)
            //        .then(function (response) {
            //            $scope.BudgetDetailsList = response;
            //            $scope.BudgetDetailsList.forEach(function (budget, budgetIndex) {
            //                budget.display = budget.BUDGET_CODE + ' (' + budget.BUDGET_NAME + ')';
            //            })
            //            if ($stateParams.Id > 0) {
            //                $scope.getprdetails();
            //            }
            //        })
            //};
            //$scope.GetComapanyBudgetDetails();



            $scope.prUnitPriceCalculation = function () {
                $scope.PRDetails.TOTAL_PRICE = 0;
                $scope.PRDetails.TOTAL_BASE_PRICE = 0;
                $scope.PRDetails.TOTAL_GST_PRICE = 0;

                $scope.PRDetails.PRItemsList.forEach(function (item, index) {
                    if (item.UNIT_PRICE == undefined || item.UNIT_PRICE <= 0) {
                        item.UNIT_PRICE = 0;
                    };


                    item.TOTAL_BASE_PRICE = item.UNIT_PRICE * item.REQUIRED_QUANTITY;
                    item.TOTAL_PRICE = item.TOTAL_BASE_PRICE + ((item.TOTAL_BASE_PRICE / 100) * (item.C_GST_PERCENTAGE));

                    $scope.PRDetails.TOTAL_GST_PRICE += (item.TOTAL_BASE_PRICE / 100) * (item.C_GST_PERCENTAGE);
                    $scope.PRDetails.TOTAL_BASE_PRICE += item.TOTAL_BASE_PRICE;
                    $scope.PRDetails.TOTAL_PRICE += item.TOTAL_PRICE;

                })

            }

            //$scope.loadUserDepartments = function () {
            //    auctionsService.GetUserDepartments(userService.getUserId(), userService.getUserToken())
            //        .then(function (response) {
            //            if (response && response.length > 0) {
            //                $scope.userDepartments = response;
            //           }
            //        })
            //};

            //$scope.loadUserDepartments();

            //$scope.userDepartments = userService.getListUserDepartmentDesignations();
            $scope.userDepartments = [];
            $scope.userDepartments.push(userService.getSelectedUserDepartmentDesignation());
            if ($scope.PRDetails.PR_ID == 0) {
                $scope.PRDetails.DEPARTMENT = $scope.userDepartments[0].deptID
                $scope.PRDetails.DEPARTMENT_CODE = $scope.userDepartments[0].deptCode
            }
            
            $scope.goToPrList = function (id) {
                var url = $state.href("listPr");
                window.open(url, '_self');
            };

            $scope.deptChanged = function () {
                $scope.userDepartments.filter(function (udd) {
                    if (udd.deptID == $scope.PRDetails.DEPARTMENT) {
                        $scope.PRDetails.DEPARTMENT_CODE = udd.deptCode;
                    }
                    //return deptCode
                });
            };


            $scope.productsList = [];
            $scope.getProducts = function () {
                catalogService.getProducts($scope.compID)
                    .then(function (response) {
                        $scope.productsList = response;
                    });
            }
            $scope.getProducts();
            $scope.autofillProduct = function (prodName, index) {
                $scope['ItemSelected_' + index] = false;
                var output = [];
                if (prodName && prodName.trim() != '') {
                    angular.forEach($scope.productsList, function (prod) {
                        if (prod.prodName.toLowerCase().indexOf(prodName.trim().toLowerCase()) >= 0) {
                            output.push(prod);
                        }
                    });
                }
                $scope["filterProducts_" + index] = output;
            }
            $scope.fillTextbox = function (selProd, index) {
                $scope['ItemSelected_' + index] = true;
                $scope.PRDetails.PRItemsList[index].ITEM_NAME = selProd.prodName;
                $scope.PRDetails.PRItemsList[index].PRODUCT_ID = selProd.prodId;
                $scope.PRDetails.PRItemsList[index].ITEM_DESCRIPTION = selProd.prodDesc;
                $scope.PRDetails.PRItemsList[index].UNITS = selProd.prodQty;
                $scope['filterProducts_' + index] = null;


            }

            $scope.onBlurProduct = function (index) {
                if ($scope['ItemSelected_' + index] == false) {
                    $scope.PRDetails.PRItemsList[index].ITEM_NAME = "";
                }
            }


           

            $scope.goToPrList = function (id) {
                var url = $state.href("list-pr");
                window.open(url, '_self');
            };


            $scope.dateFormat = function (date) {
                return userService.toLocalDate(date);
            };

        }]);