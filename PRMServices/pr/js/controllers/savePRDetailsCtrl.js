﻿prmApp
    .controller('savePRDetailsCtrl', ["$scope", "$stateParams", "$log", "$state", "$http", "domain", "$window", "userService", "auctionsService",
        "growlService", "PRMPRServices", "$rootScope", "catalogService",
        "fileReader","workflowService",
        function ($scope, $stateParams, $log, $state, $http, domain, $window, userService, auctionsService,
            growlService, PRMPRServices, $rootScope, catalogService,
            fileReader, workflowService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.prID = $stateParams.Id;
            let isFormdisabled = $stateParams.isView;
            $scope.isFormdisabled = isFormdisabled ? true : false;
            $scope.branchCodes = [];
            $scope.PRDetails = {
                isTabular: true,
                PR_ID: $stateParams.Id ? $stateParams.Id : 0,
                U_ID: $scope.userID,
                COMP_ID: 0,
                PR_NUMBER: 'PR/',
                PR_TYPE: 'REGULAR_PURCHASE',
                PRIORITY: '',
                COMMENTS: '',
                DEPARTMENT: 0,
                ATTACHMENTS: '',
                IS_PR_SUBMITTED: 0,
                WF_ID: 0,
                STATUS: 'PENDING',
                PRItemsList: [],
                URGENCY: ''
            };

            $scope.deptIDs = [];
            $scope.desigIDs = [];
            $scope.ListUserDepartmentDesignationsTemp = [];
           

            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    if (!item.deptCode.includes('_TEMP')) {
                        $scope.ListUserDepartmentDesignationsTemp.push(item);
                    }
                    
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                        }
                    });
                });
            };

            auctionsService.GetUserDepartmentDesignations($scope.userID, userService.getUserToken(), 1)
                .then(function (response) {
                    if (response && response.length > 0) {
                        $scope.ListUserDepartmentDesignationsTemp = [];
                        $scope.ListUserDepartmentDesignations = response;
                        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                                if (!item.deptCode.includes('_TEMP')) {
                                    $scope.ListUserDepartmentDesignationsTemp.push(item);
                                }

                                $scope.deptIDs.push(item.deptID);
                                item.listDesignation.forEach(function (item1, index1) {
                                    if (item1.isAssignedToUser && item1.isValid) {
                                        $scope.desigIDs.push(item1.desigID);
                                    }
                                });
                            });
                        }
                    }
                });

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.obj = {
                auctionVendors: []
            };

            $scope.objNew = {
                auctionVendors: []
            };

            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'PR_SELF';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/

            $scope.prStatus = [
                {
                    display: 'PENDING',
                    value: 'PENDING'
                },
                {
                    display: 'COMPLETED',
                    value: 'COMPLETED'
                },
                {
                    display: 'CANCELLED',
                    value: 'CANCELLED'
                },
                {
                    display: 'REJECTED',
                    value: 'REJECTED'
                }
            ];

            $scope.companyDepartments = [];
            $scope.PRItem = {
                ITEM_ID: 0,
                PR_ID: 0,
                ITEM_NAME: '',
                ITEM_CODE: '',
                REQUIRED_QUANTITY: 0,
                COMMENTS: '',
                ATTACHMENTS: '',
                ATTACHMENTSARR: [],
                PRODUCT_ID: 0,
                CATEGORY_ID: 0,
                U_ID: $scope.userID,
                sessionID: $scope.sessionID,
                itemAttachment: [],
                attachmentName: '',
                ITEM_NUM: '',
                REQUISITIONER_NAME: '',
                REQUISITIONER_EMAIL: '',
                IS_VALID: 1,
                TEMP_ID: 1
            };

            auctionsService.GetCompanyDepartments($scope.userID, $scope.sessionID)
                .then(function (response) {
                    $scope.companyDepartments = response;
                });


            $scope.PRDetails.PRItemsList.push($scope.PRItem);

            $scope.assignToItems = function (date) {
                $scope.PRDetails.PRItemsList.forEach(function (item) {
                    item.MATERIAL_DELIVERY_DATE = date;
                });
            }

            $scope.addPrItem = function () {
                let materialDeliveryDate = null;
                if ($scope.PRDetails && $scope.PRDetails.PRItemsList && $scope.PRDetails.PRItemsList.length > 0 &&
                    $scope.PRDetails.PRItemsList[0].MATERIAL_DELIVERY_DATE) {
                    materialDeliveryDate = $scope.PRDetails.PRItemsList[0].MATERIAL_DELIVERY_DATE
                } else if ($scope.MATERIAL_DELIVERY_DATE) {
                    materialDeliveryDate = $scope.MATERIAL_DELIVERY_DATE;
                } else {
                    materialDeliveryDate = moment().format('DD-MM-YYYY');
                }

                var PRItem = {
                    ITEM_ID: 0,
                    PR_ID: 0,
                    ITEM_NAME: '',
                    ITEM_CODE: '',
                    ITEM_DESCRIPTION: '',
                    REQUIRED_QUANTITY: 0,
                    COMMENTS: '',
                    ATTACHMENTS: '',
                    ATTACHMENTSARR: [],
                    PRODUCT_ID: 0,
                    U_ID: $scope.userID,
                    sessionID: $scope.sessionID,
                    ITEM_NUM: '',
                    CATEGORY_ID: 0,
                    MATERIAL_DESCRIPTION: '',
                    REQUISITIONER_NAME: '',
                    REQUISITIONER_EMAIL: '',
                    IS_VALID: 1,
                    MATERIAL_DELIVERY_DATE: materialDeliveryDate,
                    COST_CENTER: '',
                    TEMP_ID: 1
                };

                if ($scope.PRDetails.PRItemsList.length > 0) {
                    PRItem.TEMP_ID = $scope.PRDetails.PRItemsList[$scope.PRDetails.PRItemsList.length - 1].TEMP_ID + 1;
                }
                

                $scope.PRDetails.PRItemsList.push(PRItem);
            };

            $scope.checkIsFormDisable = function () {
                $scope.isFormdisabled = false;
                if (isFormdisabled) {
                    $scope.isFormdisabled = true;
                }
                if ($scope.PRDetails.CREATED_BY === +$scope.userID && $scope.itemWorkflow.length > 0 &&
                    $scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                    $scope.itemWorkflow[0].WorkflowTracks[0].status !== "APPROVED" &&
                    $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 &&
                    $scope.itemWorkflow[0].workflowID > 0
                ) {
                    $scope.isFormdisabled = false;
                    if (isFormdisabled) {
                        $scope.isFormdisabled = true;
                    }
                } else {
                    if ($scope.PRDetails.CREATED_BY === +$scope.userID && ($scope.itemWorkflow.length <= 0 ||
                        $scope.itemWorkflow[0].WorkflowTracks.length <= 0)) {
                        $scope.isFormdisabled = false;
                    } else {
                        $scope.isFormdisabled = true;
                    }                    
                }
            };

            $scope.validateMandateItems = function () {
                $scope.PRDetails.PRItemsList.forEach(function (item, index) {
                    if ($scope.tableValidation) {
                        return false;
                    }

                    var itemsno = parseInt(index) + 1;
                    if (!item.MATERIAL_DESCRIPTION) {
                        $scope.tableValidation = true;
                        $scope.tableValidationMsg = 'Please enter NAME(From Catalogue) for item: ' + itemsno;
                        return;
                    }
                    if (!item.MATERIAL_DELIVERY_DATE) {
                        $scope.tableValidation = true;
                        $scope.tableValidationMsg = 'Please enter Item Delivery Date for item: ' + itemsno;
                        return;
                    }

                    if (!item.REQUIRED_QUANTITY) {
                        $scope.tableValidation = true;
                        $scope.tableValidationMsg = 'Please enter QUANTITY for item: ' + itemsno;
                        return;
                    }

                    if (!item.REQUISITIONER_NAME) {
                        $scope.tableValidation = true;
                        $scope.tableValidationMsg = 'Please enter Requisitioner Name for item: ' + itemsno;
                        return;
                    }

                    if (item.REQUISITIONER_NAME && !(/^[a-zA-Z]+$/.test(item.REQUISITIONER_NAME))) {
                        $scope.tableValidation = true;
                        $scope.tableValidationMsg = 'Please enter valid Requisitioner Name (only a-z are allowed) for item: ' + itemsno;
                        return;
                    }

                    if (!item.REQUISITIONER_EMAIL) {
                        $scope.tableValidation = true;
                        $scope.tableValidationMsg = 'Please enter Requisitioner Email for item: ' + itemsno;
                        return;
                    }

                    if (item.REQUISITIONER_EMAIL && !(/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(item.REQUISITIONER_EMAIL))) {
                        $scope.tableValidation = true;
                        $scope.tableValidationMsg = 'Please enter valid Requisitioner Email for item: ' + itemsno;
                        return;
                    }

                    //if (!item.COST_CENTER) {
                    //    $scope.tableValidation = true;
                    //    $scope.tableValidationMsg = 'Please enter cost-center for item: ' + itemsno;
                    //    return;
                    //}
                });
            };

            $scope.handleBudgetAvailable = function () {

                if (+$scope.PRDetails.BUDGET_USED > +$scope.PRDetails.BUDGET_ALLOCATED) {
                    $scope.PRDetails.BUDGET_USED = 0;
                    $scope.PRDetails.BUDGET_AVAILABLE = $scope.PRDetails.BUDGET_ALLOCATED;
                } else {
                    let BUDGET_ALLOCATED = $scope.PRDetails.BUDGET_ALLOCATED && +$scope.PRDetails.BUDGET_ALLOCATED > 0 ? +$scope.PRDetails.BUDGET_ALLOCATED : 0;
                    let BUDGET_USED = $scope.PRDetails.BUDGET_USED && $scope.PRDetails.BUDGET_USED > 0 ? $scope.PRDetails.BUDGET_USED : 0;

                    $scope.PRDetails.BUDGET_AVAILABLE = BUDGET_ALLOCATED - BUDGET_USED;
                }
            };

            $scope.submitPr = function (isSubmit) {
                $scope.tableValidation = false;
                $scope.isBudgetedValidation = false;
                $scope.budgetAllocatedValidation = false;
                $scope.budgetUsedValidation = false;
                $scope.tentativeValueValidation = false;
                if (!$scope.PRDetails.PR_NUMBER) {
                    $scope.isValidPRNO = true;
                    return;
                }


                if (!$scope.PRDetails.PURCHASE_GROUP_CODE) {
                    $scope.isValidIndentDept = true;
                    return;
                }

                if (!$scope.PRDetails.URGENCY) {
                    $scope.urgencyValidation = true;
                    return;
                }

                if (!$scope.PRDetails.PR_TYPE) {
                    $scope.prTypeValidation = true;
                    return;
                }

                if (!$scope.PRDetails.RH_ID) {
                    $scope.prRHIDValidation = true;
                    return;
                }

                if (!$scope.PRDetails.IS_BUDGETED) {
                    $scope.isBudgetedValidation = true;
                    return;
                }

                if ($scope.PRDetails.IS_BUDGETED && $scope.PRDetails.IS_BUDGETED === 'YES') {
                    if (!$scope.PRDetails.BUDGET_ALLOCATED || +$scope.PRDetails.BUDGET_ALLOCATED <= 0) {
                        $scope.budgetAllocatedValidation = true;
                        return;
                    }

                    if (!$scope.PRDetails.BUDGET_USED || +$scope.PRDetails.BUDGET_USED <= 0) {
                        $scope.budgetUsedValidation = true;
                        return;
                    }
                }
                

                //if (!$scope.PRDetails.TENTATIVE_VALUE || +$scope.PRDetails.TENTATIVE_VALUE <= 0) {
                //    $scope.tentativeValueValidation = true;
                //    return;
                //}

                if ($scope.PRDetails.PRItemsList && $scope.PRDetails.PRItemsList.length > 0) {
                    $scope.PRDetails.PRItemsList = _.filter($scope.PRDetails.PRItemsList, function (o) {
                        return (o.ITEM_ID || o.IS_VALID);
                    });
                }

                $scope.validateMandateItems();

                if (!$scope.PRDetails.PRItemsList || $scope.PRDetails.PRItemsList.length <= 0) {
                    $scope.tableValidation = true;
                    swal("Warning!", "PR Items contains in-valid items.", "warning");
                }

                if ($scope.PRDetails.PRItemsList && $scope.PRDetails.PRItemsList.length > 0) {
                    var validItems = _.filter($scope.PRDetails.PRItemsList, function (o) {
                        return o.IS_VALID;
                    });
                    if (!validItems || validItems.length <= 0) {
                        swal("Warning!", "PR Items contains in-valid items.", "warning");
                        $scope.tableValidation = true;
                    }
                }

                if ($scope.tableValidation) {
                    return;
                }

                $scope.PRDetails.U_ID = $scope.userID;
                var params = {
                    "prdetails": $scope.PRDetails,
                    "sessionid": $scope.sessionID
                };

                params.prdetails.WF_ID = $scope.workflowObj.workflowID;
                params.prdetails.DESIG_ID = userService.getSelectedUserDesigID();
                params.prdetails.COMP_ID = $scope.compID;
                params.prdetails.CREATED_BY = $scope.userID;
                params.prdetails.IS_PR_SUBMITTED = params.prdetails.WF_ID && isSubmit ? 1 : 0;
                if (params.prdetails.PRItemsList && params.prdetails.PRItemsList.length > 0) {
                    params.prdetails.PRItemsList.forEach(function (item, index) {
                        if (item.MATERIAL_DELIVERY_DATE) {

                            let CurrentDateToLocal = userService.toLocalDate(item.MATERIAL_DELIVERY_DATE);
                            let ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            let m = moment(ts);
                            let deliveryDate = new Date(m);
                            let milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            item.MATERIAL_DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";
                        }
                    });
                }

                if (isSubmit) {
                    params.prdetails.IS_PR_SUBMITTED = 1;
                    params.prdetails.WF_ID = $scope.workflowList[0].workflowID;
                    params.prdetails.PR_APPROVER = params.prdetails.PR_APPROVER ? params.prdetails.PR_APPROVER : '';

                }

                PRMPRServices.savePrDetails(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Saved Successfully.", "success");
                            $state.go('list-pr');
                        }
                    });
            };

            $scope.getprdetails = function () {
                var params = {
                    "prid": $stateParams.Id,
                    "sessionid": $scope.sessionID
                };
                PRMPRServices.getprdetails(params)
                    .then(function (response) {

                        $scope.PRDetails = response;
                        if ($scope.PRDetails.TYPE === "PR") {
                            $scope.PRDetails.PURCHASE = true;
                            $scope.PRDetails.PRSHOW = true;
                            $scope.PRDetails.DEPARTMENT_CODE = $scope.PRDetails.DEPARTMENT_NAME;
                        } else {
                            $scope.PRDetails.PURCHASE = false;
                            $scope.PRDetails.PRSHOW = false;
                        }
                        //   $scope.PRDetails.REQUEST_DATE = userService.toLocalDate($scope.PRDetails.REQUEST_DATE);//new moment($scope.PRDetails.REQUEST_DATE).format("YYYY-MM-DD HH:mm");
                        //   $scope.PRDetails.REQUIRED_DATE = userService.toLocalDate($scope.PRDetails.REQUIRED_DATE);//new moment($scope.PRDetails.REQUIRED_DATE).format("YYYY-MM-DD HH:mm");

                        if ($scope.PRDetails.PRItemsList.length > 0) {
                            let itemCount = 1
                            $scope.PRDetails.PRItemsList.forEach(function (item, Idx) {
                                item.CURRENCY = _.filter($scope.currencies, function (x) { return x.value == item.CURRENCY; })[0];
                                item.MATERIAL_DESCRIPTION = item.ITEM_NAME;
                                item.CATEGORY_CODE = item.CategoryCode;
                                item.CATEGORY_NAME = item.CategoryName;
                                if (item.MATERIAL_DELIVERY_DATE) {
                                    item.MATERIAL_DELIVERY_DATE = userService.toLocalDate(item.MATERIAL_DELIVERY_DATE);
                                } else {
                                    item.MATERIAL_DELIVERY_DATE = moment().format('DD-MM-YYYY');
                                }

                                item.TEMP_ID = itemCount;
                                itemCount++;
                            })
                        }

                        if ($scope.PRDetails.WF_ID > 0) {
                            $scope.workflowObj.workflowID = $scope.PRDetails.WF_ID;
                        }

                        if ($stateParams.Id > 0) {
                            $scope.getItemWorkflow($scope.PRDetails.WF_ID, $scope.PRDetails.PR_ID, $scope.WorkflowModule, '');
                        }

                        $scope.handleBranch();
                    });
            };

            if ($stateParams.Id > 0) {
                $scope.getprdetails();
            }

            $scope.deletePrItem = function (prItem) {
                prItem.IS_VALID = 0;
            };

            $scope.userDepartments = [];
            $scope.userDepartments.push(userService.getSelectedUserDepartmentDesignation());
            if ($scope.PRDetails.PR_ID === 0) {
                $scope.PRDetails.DEPARTMENT = $scope.userDepartments[0].deptID;
                $scope.PRDetails.DEPT_CODE = $scope.userDepartments[0].deptCode;
                $scope.PRDetails.DEPARTMENT_CODE = $scope.userDepartments[0].deptCode;
            }
            $scope.goToPrList = function (id) {
                var url = $state.href("list-pr");
                window.open(url, '_self');
            };

            $scope.deptChanged = function () {
                $scope.userDepartments.filter(function (udd) {
                    if (udd.deptID == $scope.PRDetails.DEPARTMENT) {
                        $scope.PRDetails.DEPT_CODE = udd.deptCode;
                    }
                });
            };

            //#region Catalog
            $scope.productsList = [];
            $scope.getProducts = function (recordsFetchFrom, pageSize, searchString, index) {
                catalogService.getProducts($scope.compID, recordsFetchFrom * pageSize, pageSize, searchString ? searchString : "")
                    .then(function (response) {
                        $scope.productsList = [];
                        $scope.productsListTemp = [];
                        if (response) {
                            response = response.filter(function (prodItem, prodIndex) {
                                if (prodItem.categoryId > 0 && +prodItem.isValid === 1) {
                                    return prodItem;
                                }
                            })
                            $scope.productsList = response;
                            $scope.productsListTemp = response;
                            $scope.fillProduct($scope.productsList, index);
                        }
                    });
            };

            $scope.autofillProduct = function (prodName, index) {
                if ($scope.PRDetails.PR_TYPE === undefined) {
                    swal("Error!", "Please Select Medical/Non-Medical PR Type to search for Products", "warning");
                    $scope.PRDetails.PRItemsList[index].ITEM_NAME = "";
                    return;
                }
                $scope['filterProducts_' + index] = null;
                //$scope['ItemSelected_' + index] = false;
                $scope.getProducts(0, 10, prodName, index);
            };

            $scope.fillTextbox = function (selProd, index) {
                $scope['ItemSelected_' + index] = true;
                $scope.PRDetails.PRItemsList[index].MATERIAL_DESCRIPTION = selProd.prodName;
                $scope.PRDetails.PRItemsList[index].ITEM_CODE = selProd.prodCode;
                $scope.PRDetails.PRItemsList[index].ITEM_NUM = "000" + ((index + 1) * 10);
                $scope.PRDetails.PRItemsList[index].PRODUCT_ID = selProd.prodId;
                $scope.PRDetails.PRItemsList[index].CATEGORY_ID = selProd.categoryId;
                $scope.PRDetails.PRItemsList[index].CATEGORY_CODE = selProd.categoryCode;
                $scope.PRDetails.PRItemsList[index].CATEGORY_NAME = selProd.categoryName;
                $scope.PRDetails.PRItemsList[index].ITEM_DESCRIPTION = selProd.prodDesc;
                $scope.PRDetails.PRItemsList[index].UOM = selProd.prodQty;
                $scope.PRDetails.PRItemsList[index].HSN_CODE = selProd.prodHSNCode;
                
                $scope['filterProducts_' + index] = null;
            }

            $scope.onBlurProduct = function (index) {
                if ($scope['ItemSelected_' + index] == false) {
                    $scope.PRDetails.PRItemsList[index].ITEM_NAME = "";
                }
            }
            //#endregion Catalog

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (id != "itemsAttachment") {
                            let bytearray = new Uint8Array(result);
                            let arrayByte = $.makeArray(bytearray);
                            let ItemFileName = $scope.file.name;

                            let currentPRItem = $scope.PRDetails.PRItemsList.filter(function (item) {
                                return +item.TEMP_ID === +id;
                            });

                            if (currentPRItem && currentPRItem.length > 0) {
                                if (!currentPRItem[0].ATTACHMENTSARR) {
                                    currentPRItem[0].ATTACHMENTSARR = [];                                   
                                }

                                let attachmentObj = {
                                    fileID: 0,
                                    fileName: ItemFileName,
                                    fileStream: arrayByte
                                }

                                currentPRItem[0].ATTACHMENTSARR.push(attachmentObj);

                                //currentPRItem[0].itemAttachment = arrayByte;
                                //currentPRItem[0].attachmentName = ItemFileName;
                            }
                        }
                    });
            };


            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                //createDomestic
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                                $scope.workflowListDeptWise.push(item);
                            }
                        });

                        if (userService.getUserObj().isSuperUser) {
                            $scope.workflowList = $scope.workflowList;
                        }
                        else {
                            $scope.workflowList = $scope.workflowList;
                            //$scope.workflowList = [];
                            //$scope.workflowListDeptWise.forEach(function (wf, idx) {
                            //    $scope.deptIDs.forEach(function (dep) {
                            //        if (dep == wf.deptID) {
                            //            $scope.workflowList.push(wf);
                            //        }
                            //    });
                            //});

                            //$scope.workflowList = $scope.workflowList.filter(function (item) {
                            //    return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                            //});
                        }
                    });
            };

            $scope.getWorkflows();

            $scope.getItemWorkflow = function (wfID, prID, type, itemID) {
                workflowService.getItemWorkflow(wfID, prID, type)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        $scope.checkIsFormDisable();
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                    count = count + 1;
                                    //$scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });
            };

            $scope.updateTrack = function (step, status) {
                $scope.disableAssignPR = true;
                $scope.commentsError = '';
                if (step.comments) {
                    step.comments = validateStringWithoutSpecialCharacters(step.comments);
                }
                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status === 'APPROVED') {
                    $scope.disableAssignPR = false;
                } else {
                    $scope.disableAssignPR = true;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status === 'REJECTED' && !step.comments) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = $scope.userID;

                step.moduleName = $scope.WorkflowModule;
                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            location.reload();
                            //$state.go('approval-qcs-list');
                        }
                    });
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: $scope.userID, sessionID: $scope.sessionID }))
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            //  $state.go('list-pr');
                        }
                    });
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, $scope.userID)
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {
                let disable = true;
                let previousStep = {};
                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {
                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }

                    previousStep = step;
                });

                return disable;
            };

            /*region end WORKFLOW*/

            $scope.fillProduct = function (output, index) {
                $scope['filterProducts_' + index] = output;
            };

            $scope.dateFormat = function (date) {
                return userService.toLocalDate(date);
            };


            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }


            $scope.PRDetails.SelectedLocationArray = [];
            $scope.PRDetails.selectedLocations = '';
            $scope.fillValue = function (locationObj, location, isChecked) {
                $scope.locationArray = [];
                if (isChecked) {
                    $scope.PRDetails.SelectedLocationArray.push(location);

                    locationObj.forEach(function (obj, objIdx) {
                        if (obj.isChecked == true) {
                            $scope.locationArray.push(obj.configValue);
                        }
                    })
                    $scope.PRDetails.selectedLocations = $scope.locationArray.join(',');
                    if ($scope.PRDetails.selectedLocations.length > 0) {
                        $scope.PRDetails.selectedLocationsError = '';
                        $scope.searchKeyword = '';
                    }

                } else {
                    var tempindex = $scope.PRDetails.SelectedLocationArray.indexOf(location);
                    if (tempindex > -1) {
                        $scope.PRDetails.SelectedLocationArray.splice(tempindex, 1);
                        $scope.PRDetails.SelectedLocationArray.forEach(function (obj, objIdx) {
                            if (obj.isChecked == true) {
                                $scope.locationArray.push(obj.configValue);
                            }
                        })
                        $scope.PRDetails.selectedLocations = $scope.locationArray.join(',');
                    }
                }
            };

            $scope.getBranches = function () {
                $scope.branchCodes = [];
                $http({
                    method: 'get',
                    url: domain + 'getregionhierarchy?compid=' + $scope.compID + '&sessionid=' + $scope.sessionID,
                    encodeuri: true,
                    headers: { 'content-type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data && response.data.length > 0) {
                        $scope.branchCodes = response.data;
                        $scope.handleBranch();
                    }
                });
            };

            $scope.getBranches();

            $scope.handleBranch = function () {
                let location = '';
                if ($scope.branchCodes && $scope.branchCodes.length > 0) {
                    let selectedBranch = _.filter($scope.branchCodes, function (o) {
                        return o.RH_ID === $scope.PRDetails.RH_ID
                    });

                    if (selectedBranch && selectedBranch.length > 0) {
                        $scope.PRDetails.LOCATION_CODE = selectedBranch[0].LOCATION_CODE;
                        $scope.PRDetails.REGION_CODE = selectedBranch[0].REGION_CODE;
                    }
                }   
            };

            $scope.handleBudgeted = function () {
                if ($scope.PRDetails.IS_BUDGETED && $scope.PRDetails.IS_BUDGETED === 'NO') {
                    $scope.PRDetails.BUDGET_ALLOCATED = 0;
                    $scope.PRDetails.BUDGET_USED = 0;
                    $scope.PRDetails.BUDGET_AVAILABLE = 0;
                }
            };

            $scope.removeAttach = function (prItem) {
                prItem.ATTACHMENTSARR = [];
            };

        }]);
