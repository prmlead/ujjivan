﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('save-pr-details', {
                    url: '/save-pr-details/:Id/:isView',
                    templateUrl: 'pr/views/save-pr-details.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('pr-actions', {
                    url: '/pr-actions/:Id',
                    templateUrl: 'pr/views/pr-actions.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('list-pr', {
                    url: '/list-pr',
                    templateUrl: 'pr/views/list-pr.html',
                    params: {
                        detailsObj: null
                    }
                })

                .state('view-pr', {
                    url: '/view-pr/:Id',
                    templateUrl: 'pr/views/view-pr.html',
                    params: {
                        detailsObj: null
                    }
                })
        }]);