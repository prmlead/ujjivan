﻿angular = require('angular');

angular.module('storeModule')
    .controller('addnewstoreitemCtrl', ['$scope', '$stateParams', '$window', 'loginService', 'storeService', 'growlService',  function ($scope, $stateParams, $window, loginService, storeService, growlService) {
        $scope.storeID = $stateParams.storeID;
        $scope.itemID = $stateParams.itemID;

        $scope.companyID = loginService.getUserCompanyId();
        $scope.sessionID = loginService.getUserToken();

        $scope.storeObj = {
            storeID: $scope.storeID,
            companyID: $scope.companyID,
            //isMainBranch: 0,
            //mainStoreID: 0,
            //storeCode: '',
            //storeDescription: '',
            //storeInCharge: '',
            //storeDetails: '',
            //isValid: 0,
            //storeAddress: '',
            //storeBranches: 0,           
            sessionID: $scope.sessionID
        };

        $scope.storeItemsObj = {
            itemID: $scope.itemID,
            storeDetails: $scope.storeObj,
            //itemCode: '',
            //itemType: '',
            //itemDescription: '',
            //totalStock: 0,
            //inStock: 0,
            //itemPrice: 0,
            //isValid: 0,
            //doAlert: 0,
            //threshold: 0,
            //iGST: 0,
            //cGST: 0,
            //sGST: 0,
            //itemUnits: '',
            sessionID: $scope.sessionID
        };

        $scope.typesOfMeterial =
        [
            'RAW MATERIALS', 'NON-VALUATED MATERIALS', 'PACKAGING MATERIALS',
            'COMPETITIVE PRODUCTS','SERVICES','FINISHED PRODUCTS','PRODUCTION RESOURCES/TOOLS',
            'SEMI-FINISHED PRODUCTS', 'TRADING GOODS', 'OPERATING SUPPLIES', 'NON-STOCK MATERIAL'
        ];


        $scope.itemUnitValues1 =
            [
            'Length', 'inch', 'litres', 'foot',
            'yard', 'mile', 'meter', 'kilograms', 'cup',
            'US quart', 'US gallon', 'ounces', 'pounds',
            'stone', 'tonnes', 'Joules', 'Number', 'Watt',
            'Duration', 'Kilowatt', 'Calorie', 'Horsepower', 'Workdays'
            ];


        $scope.itemUnitValues = [
             { mName: 'inch', mValue: 'in' },
             { mName: 'meter', mValue: 'mt' },
             { mName: 'foot', mValue: 'ft' },
             { mName: 'yard', mValue: 'yd' },
             { mName: 'mile', mValue: 'ml' },
             { mName: 'Centimeter', mValue: 'cm' },
             { mName: 'Millimeter', mValue: 'mm' },
             { mName: 'litre', mValue: 'l' },
             { mName: 'kilograms', mValue: 'kg' },
             { mName: 'cup', mValue: 'cp' },
             { mName: ' US quart', mValue: 'qt' },
             { mName: 'US gallon', mValue: 'gal' },
             { mName: 'ounces', mValue: 'oz' },
             { mName: 'pounds', mValue: 'lb' },
             { mName: 'stone', mValue: 'st' },
             { mName: 'Carats', mValue: 'ca' },
             { mName: 'tonnes', mValue: 'ton' },
             { mName: 'Number', mValue: 'nos' },
             { mName: 'Watt', mValue: 'w' },
             { mName: 'Kilowatt', mValue: 'kw' },
             { mName: 'Calorie', mValue: 'cal' },
             { mName: 'Horsepower', mValue: 'hp' },
             { mName: 'Joules', mValue: 'jl' },
             { mName: 'Duration', mValue: 'dur' },
             { mName: 'Workdays', mValue: 'wd' },
             { mName: 'Days', mValue: 'dy' },
             { mName: 'Degree', mValue: 'dg' },
             { mName: 'Newtons', mValue: 'nw' },
             { mName: 'Barrels', mValue: 'bl' },
        ];




        $scope.taxClassification = [
                   { name: 'Cement - 28 %', tax: 28 },
                   { name: 'Steel - 18 %', tax: 18 },
                   { name: 'River Sand - 5 %', tax: 5 },
                   { name: 'Crush Sandg - 5 %', tax: 5 },
                   { name: 'Aggregate - 5 %', tax: 5 },
                   { name: 'Marble and Granite- 12 - 28 %', tax: 28 },
                   { name: 'Stone - 5 %', tax: 5 },
                   { name: 'Red Bricks- 5 %', tax: 5 },
                   { name: 'Fly Ash Bricks- 12 %', tax: 12 },
                   { name: 'Cement Block - 18 %', tax: 18 },
                   { name: 'Glass - 28 %', tax: 28 },
                   { name: 'Shahabad - 5 %', tax: 5 },
                   { name: 'Tiles - 18 - 28 %', tax: 28 },
                   { name: 'Wire and Cable - 28 %', tax: 28 },
                   { name: 'Paint and Varnish - 28 %', tax: 28 },
                   { name: 'Sanatary Fittings - 28 %', tax: 28 },
                   { name: 'GI Fittings - 18 %', tax: 18 },
                   { name: 'CP Fittings - 18 - 28 % (Depends on Base Material)', tax: 28 },
                   { name: 'Wallpapers - 28 %', tax: 28 },
                   { name: 'Key Locks - 18 %', tax: 18 },
                   { name: 'Wooden Door and window - 28 %', tax: 28 },
                   { name: 'Aluminium Windows - 18 - 28 %', tax: 28 },
        ];





        if ($scope.itemID > 0)
        {
            if (!$stateParams.itemObj) {
                storeService.getcompanystoreitems($scope.storeID, $scope.itemID)
              .then(function (response) {
                  $scope.storeItemsObj = response[0];
                  $scope.storeItemsObj.sessionID = $scope.sessionID;
              });
            }
            else {
                $scope.storeItemsObj = $stateParams.itemObj;
                $scope.storeItemsObj.sessionID = $scope.sessionID;
            }
        }
        


        $scope.saveStoreItem = function () {

            $scope.storeItemsObj.isValid = 1;

            var params = {
                "item": $scope.storeItemsObj
            };

            storeService.savestoreitem(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Item Details Saved Successfully.", "success");
                       $window.history.back();
                   }
               })
        }

        $scope.itemTax = 0;

        $scope.getTaxes = function(tax)
        {            
            $scope.storeItemsObj.cGST = tax / 2;
            $scope.storeItemsObj.sGST = tax / 2
        }


    }]);