﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;
using PRMServices.models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMPOModuleService
    {
        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, 
            ResponseFormat = WebMessageFormat.Json, 
            UriTemplate = "getcustomfields?compid={compid}&customfieldid={customfieldid}&fieldname={fieldname}&fieldmodule={fieldmodule}&sessionid={sessionID}")]
        List<PRMCustomField> GetCustomFields(int compid, int customfieldid, string fieldname, string fieldmodule, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savetemplatesubitem")]
        Response SaveTemplateSubItem(SubItemTemplate subitemtemplate, string sessionid);
    }
}
