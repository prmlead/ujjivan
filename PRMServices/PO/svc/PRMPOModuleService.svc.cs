﻿using PRM.Core.Common;
using PRMServices.models;
using PRMServices.Models;
using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel.Activation;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMPOModuleService : IPRMPOModuleService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        private PRMServices prmServices = new PRMServices();

        #region GetFunctions
        public List<PRMCustomField> GetCustomFields(int compid, int customfieldid, string fieldname, string fieldmodule, string sessionid)
        {
            List<PRMCustomField> details = new List<PRMCustomField>();
            try
            {
                fieldname = fieldname ?? string.Empty;
                fieldmodule = fieldmodule ?? string.Empty;

                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_CUST_FIELD_ID", customfieldid);
                sd.Add("P_FIELD_NAME", fieldname);
                sd.Add("P_FIELD_MODULE", fieldmodule);
                DataSet dataset = sqlHelper.SelectList("cf_GetCustomFields", sd);
                DataNamesMapper<PRMCustomField> mapper = new DataNamesMapper<PRMCustomField>();
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    details = mapper.Map(dataset.Tables[0]).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }

        #endregion

        #region SaveFunctions
        public Response SaveTemplateSubItem(SubItemTemplate subitemtemplate, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_SUB_ITEM_ID", subitemtemplate.SUB_ITEM_ID);
                sd.Add("P_TEMPLATE_ID", subitemtemplate.TEMPLATE_ID);
                sd.Add("P_NAME", subitemtemplate.NAME);
                sd.Add("P_DESCRIPTION", subitemtemplate.DESCRIPTION);
                sd.Add("P_HAS_SPECIFICATION", subitemtemplate.HAS_SPECIFICATION);
                sd.Add("P_HAS_PRICE", subitemtemplate.HAS_PRICE);
                sd.Add("P_HAS_QUANTITY", subitemtemplate.HAS_QUANTITY);
                sd.Add("P_CONSUMPTION", subitemtemplate.CONSUMPTION);
                sd.Add("P_UOM", subitemtemplate.UOM);
                sd.Add("P_HAS_TAX", subitemtemplate.HAS_TAX);
                sd.Add("P_IS_VALID", subitemtemplate.IS_VALID);
                sd.Add("P_U_ID", subitemtemplate.U_ID);
                DataNamesMapper<Response> mapper = new DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("cm_SaveTemplateSubItem", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        #endregion
    }
}