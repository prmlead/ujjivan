﻿prmApp
    .controller('poDomesticZSDMCtrl', ["$state", "$stateParams", "$scope", "$rootScope", "auctionsService", "userService", "$http", "$window", "domain",
        "fileReader", "growlService", "$log", "$filter", "ngDialog", "catalogService", "PRMPRServices", "PRMCustomFieldService", "$modal", "PRMPOServices",
        function ($state, $stateParams, $scope, $rootScope, auctionsService, userService, $http, $window, domain,
            fileReader, growlService, $log, $filter, ngDialog, catalogService, PRMPRServices, PRMCustomFieldService, $modal, PRMPOServices) {

            $scope.currentUserId = +userService.getUserId();
            $scope.isCustomer = $scope.customerType === "CUSTOMER" ? true : false;
            $scope.currentUserSessionId = userService.getUserToken();
            $scope.currentUsercompanyId = userService.getUserCompanyId();
            $scope.currentUserName = userService.getFirstname() + ' ' + userService.getLastname();
            $scope.currentUserSapId = userService.getSAPUserId();
            $scope.qcsId = $stateParams.qcsId;
            $scope.reqId = $stateParams.reqId;
            $scope.poQuotId = $stateParams.quotId;
            $scope.requirementDetails = $stateParams.requirementDetails;
            $scope.vendorAssignments = $stateParams.detailsObj;
            $scope.poRAWJSON = $stateParams.poRawJSON;
            $scope.splittedItems = $stateParams.splitQtyItems;
            let poRoundDigits = 2;
            $scope.POServiceStatusText = '';
            $scope.documentType = $stateParams.templateName;
            $scope.selectedVendors = [];
            $scope.currentUIVendor = {};
            $scope.currentUIPlant = {};
            $scope.formDetails = {};
            $scope.formDetails.items = [];
            $scope.reqDepartments = [];
            $scope.requirementPRItems = [];
            $scope.requirementPOItems = [];
            $scope.requirementVendorCodeInfo = [];
            $scope.qcsType = $stateParams.qcsType;

            $scope.formDetails.MEMORY_TYPE = 'X';
            $scope.formDetails.PR_NUMBER = $scope.requirementDetails && $scope.requirementDetails.prNumbers ? $scope.requirementDetails.prNumbers : '';
            $scope.formDetails.PO_CREATOR = $scope.currentUserSapId;//$scope.requirementDetails && $scope.requirementDetails.prCreator ? $scope.requirementDetails.prCreator : '';
            //$scope.formDetails.INCO_TERMS1 = $scope.requirementDetails && $scope.requirementDetails.generalTC ? $scope.requirementDetails.generalTC : '';
            $scope.formDetails.INCO_TERMS1 = '';
            $scope.formDetails.INCO_TERMS2 = '';
            //$scope.formDetails.PLANT = $scope.requirementDetails && $scope.requirementDetails.PLANTS ? $scope.requirementDetails.PLANTS : '';
            //$scope.formDetails.PO_CURRENCY = $scope.requirementDetails && $scope.requirementDetails.currency ? $scope.requirementDetails.currency : '';
            //$scope.formDetails.PAYMENT_TERMS = $scope.requirementDetails && $scope.requirementDetails.paymentTerms ? $scope.requirementDetails.paymentTerms : '';
            $scope.formDetails.DOC_TYPE = $scope.documentType;
            //$scope.formDetails.GMP_TYPE = $scope.requirementDetails && $scope.requirementDetails.GMP_NGMP === 'GMP' ? true : false;
            $scope.requirementPlants = [];
            let requirementPlantsCodes = _.uniq($scope.requirementDetails.PLANT_CODES.split(","));
            let requirementPlantsNames = _.uniq($scope.requirementDetails.PLANTS.split(","));

            if (requirementPlantsCodes && requirementPlantsCodes.length > 0) {
                let temp = 0;
                requirementPlantsCodes.forEach(function (code, index) {
                    $scope.requirementPlants.push({
                        PLANT_CODE: code,
                        PLANT_NAME: requirementPlantsNames[temp]
                    });

                    temp++;
                });
            }

            $scope.quantityError = false;
            var SelectedVendorDetails;

            $scope.companyCurrencies = [];

            auctionsService.GetCompanyCurrencies($scope.currentUsercompanyId, userService.getUserToken())
                .then(function (Response) {

                    $scope.companyCurrencies = Response;

                });
            
            $scope.companyConfigList = [];
            $scope.COMPANY_INCO_TERMS = [];
            auctionsService.GetCompanyConfiguration($scope.currentUsercompanyId, 'INCO', userService.getUserToken())
                .then(function (unitResponse) {

                    $scope.companyConfigList = unitResponse;
                    $scope.companyConfigList.forEach(function (item, index) {
                        if (item.configKey === 'INCO') {
                            $scope.COMPANY_INCO_TERMS.push(item);
                        } 
                    });

                });

            if ($scope.vendorAssignments && $scope.vendorAssignments.length > 0) {
                $scope.vendorAssignments.forEach(function (assignment, index) {
                    let temp = _.filter($scope.selectedVendors, function (vendorObj) {
                        return vendorObj.vendorId === assignment.vendorID;
                    });

                    let vendorTemp = _.filter($scope.requirementDetails.auctionVendors, function (vendorObj) {
                        return vendorObj.vendorID === assignment.vendorID;
                    });

                    //if (!temp || temp.length <= 0) {
                    //    $scope.selectedVendors.push({ 'vendorId': assignment.vendorID, 'vendorCompany': assignment.vendorName, vendorObj: vendorTemp[0] });
                    //}
                });
            }

            //$scope.selectedVendors = _.uniq(_.map($scope.vendorAssignments, 'vendorName'));

            $scope.precisionRound = function (number) {
                var factor = Math.pow(10, poRoundDigits);
                return Math.round(number * factor) / factor;
            };
            $scope.addItem = function () {
                $scope.formDetails.items.push({
                    ITEM_OF_REQUESITION: '',
                    isDeleted: false
                });
            };
            $scope.deleteItem = function (item) {
                item.isDeleted = true;
            };

            $scope.pageNo = 1;
            $scope.nextpage = function (pageNo) {
                $scope.pageNo = $scope.pageNo + 1;
            };
            $scope.previouspage = function () {
                $scope.pageNo = $scope.pageNo - 1;

            };
            $scope.cancel = function () {
                $window.history.back();
            };

            $scope.filterSelectionChange = function () {
                if ($scope.currentUIVendor && $scope.currentUIVendor.vendorId) {
                    $scope.formDetails.items = [];
                    $scope.formDetails.VENDOR_COMPANY = $scope.currentUIVendor.vendorCompany;
                    $scope.formDetails.VENDOR_ID = $scope.currentUIVendor.vendorId;
                    $scope.formDetails.VENDOR_QUOTATION = $stateParams.quoteLink ? $stateParams.quoteLink : $scope.reqId + '-' + $scope.currentUIVendor.vendorId;
                    $scope.formDetails.VENDOR_QUOTATION1 = $scope.reqId + '-' + $scope.currentUIVendor.vendorId;
                    $scope.formDetails.INCO_TERMS1 = '';
                    $scope.formDetails.INCO_TERMS2 = '';
                    let currentVendorObj = _.filter($scope.requirementDetails.auctionVendors, function (vendorObj) {
                        return vendorObj.vendorID === $scope.currentUIVendor.vendorId;
                    });

                    let totalVendorItems = 1;
                    let packageTax = 0;
                    let packageValWithTax = 0;
                    let packageValWithOutTax = 0;

                    let freightTax = 0;
                    let freightValWithTax = 0;
                    let freightValWithOutTax = 0;

                    let miscTax = 0;
                    let miscValWithTax = 0;
                    let miscValWithOutTax = 0;


                    //Packing & Forwarding charges
                    var packaging = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                        return reqItem.productCode === 'Packing & Forwarding charges';
                    });

                    if (packaging && packaging.length > 0) {
                        packageTax = packaging[0].iGst ? packaging[0].iGst : (packaging[0].sGst + packaging[0].cGst);
                        packageValWithTax = packaging[0].itemPrice;
                        packageValWithOutTax = packaging[0].unitPrice;
                    }

                    //Freight charges
                    var freightCharges = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                        return reqItem.productCode === 'Freight charges';
                    });

                    if (freightCharges && freightCharges.length > 0) {
                        freightTax = freightCharges[0].iGst ? freightCharges[0].iGst : (freightCharges[0].sGst + freightCharges[0].cGst);
                        freightValWithTax = freightCharges[0].itemPrice;
                        freightValWithOutTax = freightCharges[0].unitPrice;
                    }

                    //Miscellenous
                    var miscellenous = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                        return reqItem.productCode === 'Miscellenous';
                    });

                    if (miscellenous && miscellenous.length > 0) {
                        miscTax = miscellenous[0].iGst ? miscellenous[0].iGst : (miscellenous[0].sGst + miscellenous[0].cGst);
                        miscValWithTax = miscellenous[0].itemPrice;
                        miscValWithOutTax = miscellenous[0].unitPrice;
                    }

                    //$scope.formDetails.PO_CURRENCY = currentVendorObj[0].vendorCurrency;
                    //$scope.formDetails.VENDOR_NAME = $scope.currentUIVendor.vendorObj.vendorName;
                    //$scope.formDetails.VENDOR_CODE = $scope.currentUIVendor.vendorObj.vendorCode;
                    if ($scope.currentUIVendor.vendorObj.gstNumber && $scope.requirementVendorCodeInfo && $scope.requirementVendorCodeInfo.length > 0) {
                        let vendorGstDetails = _.filter($scope.requirementVendorCodeInfo, function (vendorInfo) {
                            return vendorInfo.gstNumber && vendorInfo.vendorCode && vendorInfo.vendorID === $scope.currentUIVendor.vendorObj.vendorID &&
                                vendorInfo.gstNumber === $scope.currentUIVendor.vendorObj.gstNumber;
                        });

                        if (vendorGstDetails && vendorGstDetails.length > 0) {
                            $scope.formDetails.VENDOR_CODE = vendorGstDetails[0].vendorCode;
                        }
                    }
                    //Items Populate:
                    let vendorItems = _.filter($scope.splittedItems, function (assignment) {
                        return assignment.VENDOR_ID === $scope.currentUIVendor.vendorId;
                    });

                    if (vendorItems && vendorItems.length > 0) {
                        totalVendorItems = vendorItems.length;
                        vendorItems.forEach(function (vendorItem, index) {
                            let poItemObj = {
                                MPN_CODE: '',
                                Deliver_SCHEDULE: ''
                            };
                            var itemdetailsTemp = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                                return reqItem.itemID === vendorItem.itemID;
                            });

                            let prItemObj = _.filter($scope.requirementPRItems, function (prItem) {
                                return prItem.PRODUCT_ID === itemdetailsTemp[0].catalogueItemID;
                            });

                            //poItemObj.TAX_CODE = currentVendorObj[0].gstNumber;
                            if (itemdetailsTemp && itemdetailsTemp.length > 0) {
                                if ($scope.requirementDetails.isDiscountQuotation === 1) {
                                    poItemObj.DISCOUNT_VALUE = $scope.precisionRound(itemdetailsTemp[0].revUnitDiscount, 2);

                                    poItemObj.PO_NETPRICE = parseFloat(itemdetailsTemp[0].unitMRP);
                                    poItemObj.PO_PRICE = parseFloat(itemdetailsTemp[0].unitMRP);
                                    poItemObj.NET_PRICE_PUR_DOC = $scope.precisionRound(parseFloat(itemdetailsTemp[0].unitMRP));

                                } else {
                                    poItemObj.DISCOUNT_VALUE = $scope.precisionRound(parseFloat(((itemdetailsTemp[0].unitPrice - itemdetailsTemp[0].revUnitPrice) / itemdetailsTemp[0].unitPrice) * 100), 2);

                                    poItemObj.PO_NETPRICE = parseFloat(vendorItem.assignedPrice);
                                    poItemObj.PO_PRICE = parseFloat(vendorItem.assignedPrice);
                                    poItemObj.NET_PRICE_PUR_DOC = $scope.precisionRound(parseFloat(vendorItem.assignedPrice));
                                }

                                poItemObj.MATERIAL_CODE = itemdetailsTemp[0].productCode.replace(/^0+/, ''); // to Remove leading zeros
                                //poItemObj.PRICE_UNIT = parseFloat(vendorItem.assignedPrice);
                                poItemObj.PRICE_UNIT = 1;
                                poItemObj.PO_UOM = itemdetailsTemp[0].productQuantityIn;
                                poItemObj.ORDER_PRICE_UNIT = itemdetailsTemp[0].productQuantityIn;



                                


                                poItemObj.QUANTITY = +itemdetailsTemp[0].qtyDistributed;
                                if (!poItemObj.QUANTITY) {
                                    poItemObj.quantityError = true;
                                }
                            }

                            if (prItemObj && prItemObj.length > 0) {
                                let prNumbers = _.map(prItemObj, 'PR_NUMBER').join(); //_.uniq(_.map(prItemObj, 'PR_NUMBER')).join();
                                poItemObj.PR_NUMBER = prNumbers;
                                let prItemTotalQuantity = _.sumBy(prItemObj, function (item) {
                                    return item.REQUIRED_QUANTITY;
                                });

                                if (poItemObj.QUANTITY && prItemTotalQuantity && poItemObj.QUANTITY > prItemTotalQuantity) {
                                    poItemObj.QUANTITY = prItemTotalQuantity;
                                }

                                if ($scope.requirementPOItems && $scope.requirementPOItems.length > 0) {
                                    var currentVendorPOItems = _.filter($scope.requirementPOItems, function (reqPOItem) {
                                        return reqPOItem.VENDOR_ID === +$scope.formDetails.VENDOR_ID && reqPOItem.MATNR === prItemObj[0].ITEM_CODE.replace(/^0+/, '');
                                    });

                                    if (poItemObj.QUANTITY && currentVendorPOItems && currentVendorPOItems.length > 0) {
                                        poItemObj.QUANTITY = poItemObj.QUANTITY - currentVendorPOItems.MENGE;
                                    }
                                }

                                let prLineItems = _.map(prItemObj, 'ITEM_NUM').join(); //_.uniq(_.map(prItemObj, 'ITEM_NUM')).join();
                                poItemObj.ITEM_OF_REQUESITION = prLineItems;
                                poItemObj.MATERIAL_TYPE = prItemObj[0].MATERIAL_TYPE;
                                poItemObj.SHORT_TEXT = prItemObj[0].SHORT_TEXT;
                                poItemObj.PLANT = prItemObj[0].PLANT;
                                //poItemObj.PO_NETPRICE = vendorItem.assignedPrice * vendorItem.assignedQty;
                                poItemObj.MATERIAL_CODE = prItemObj[0].ITEM_CODE.replace(/^0+/, ''); // to Remove leading zeros
                            }

                            //Packing & Forwarding charges
                            var packaging = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                                return reqItem.productCode === 'Packing & Forwarding charges';
                            });

                            if (packaging && packaging.length > 0) {
                                poItemObj.PACK_FORWARD_PER = packageTax; //parseFloat((packageTax / totalVendorItems).toFixed(2));
                                poItemObj.PACK_VALUE = parseFloat((packageValWithTax / totalVendorItems).toFixed(2));
                            }

                            //Freight charges
                            var freightCharges = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                                return reqItem.productCode === 'Freight charges';
                            });

                            if (freightCharges && freightCharges.length > 0) {
                                poItemObj.FREIGHT_TAXABLE_PER = freightTax;//parseFloat((freightTax / totalVendorItems).toFixed(2));
                                poItemObj.FREIGHT_VALUE = parseFloat((freightValWithTax / totalVendorItems).toFixed(2));
                                poItemObj.FREIGHT_NON_TAX_PER = parseFloat((freightValWithOutTax / totalVendorItems).toFixed(2));
                            }

                            //Miscellenous
                            var miscellenous = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                                return reqItem.productCode === 'Miscellenous';
                            });

                            if (miscellenous && miscellenous.length > 0) {
                                poItemObj.MISC_TAXABLE_PER = miscTax; //parseFloat(( miscTax / totalVendorItems).toFixed(2));
                                poItemObj.MISC_TAXABLE_VAL = parseFloat((miscValWithTax / totalVendorItems).toFixed(2));
                                poItemObj.MISC_VALUE_NONTAX = parseFloat((miscValWithOutTax / totalVendorItems).toFixed(2));
                            }


                            userService.getProfileDetails({ "userid": $scope.formDetails.VENDOR_ID, "sessionid": $scope.currentUserSessionId })
                                .then(function (response) {
                                    if (response) {
                                        SelectedVendorDetails = response;
                                        if (SelectedVendorDetails) {
                                            $scope.formDetails.PAYMENT_TERMS = SelectedVendorDetails.paymentTermDesc;
                                            $scope.formDetails.PAYMENT_TERMS_HIDE = SelectedVendorDetails.paymentTermCode;
                                            $scope.formDetails.INCO_TERMS1 = SelectedVendorDetails.incoTerm;

                                            $scope.COMPANY_INCO_TERMS.forEach(function (term, idx) {
                                                if (term.configValue == $scope.formDetails.INCO_TERMS1) {
                                                    $scope.formDetails.INCO_TERMS2 = term.configText;
                                                }
                                            });
                                        }
                                    }
                                });


                            if (poItemObj.PR_NUMBER) {
                                $scope.formDetails.items.push(poItemObj);
                            }
                        });
                    }
                }
            };

            $scope.GetPRItemsByRequirementId = function () {
                $scope.formDetails.PURCHASE_GROUP_NAMES = '';
                $scope.formDetails.PURCHASE_GROUP_CODE = '';

                PRMPRServices.GetPRItemsByReqId({ reqid: $scope.reqId })
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementPRItems = response;

                            $scope.splitItemsOnVendorAssndQty();

                            //if ($scope.requirementPRItems && $scope.requirementPRItems.length > 0) {
                            //    $scope.formDetails.PURCHASE_GROUP_CODE = $scope.requirementPRItems[0].PURCHASE_GROUP;
                            //    $scope.formDetails.GMP_TYPE = $scope.requirementPRItems[0].GMP_NGMP === 'GMP' ? true : false;
                            //    $scope.formDetails.GMP_TYPE_HIDE = $scope.requirementPRItems[0].GMP_NGMP;
                            //}
                        }

                        //if ($scope.requirementPlants.length === 1) {
                        //    $scope.currentUIPlant = $scope.requirementPlants[0];
                        //}
                        //if ($scope.selectedVendors.length === 1) {
                        //    $scope.currentUIVendor = $scope.selectedVendors[0];
                        //}

                        //if ($scope.currentUIVendor && $scope.currentUIVendor.vendorId && $scope.currentUIPlant && $scope.currentUIPlant.PLANT_CODE) {
                        //    $scope.filterSelectionChange();                            
                        //}

                        if ($scope.poRAWJSON) {
                            if ($scope.poRAWJSON && $scope.poRAWJSON.items && $scope.poRAWJSON.items.length > 0) {
                                let selectedPlant = $scope.poRAWJSON.items[0].PLANT;
                                let selectedVendor = $scope.poRAWJSON.VENDOR_ID;
                                if ($scope.requirementPlants && $scope.requirementPlants.length > 1) {
                                    let filteredPlant = _.filter($scope.requirementPlants, function (plant) {
                                        return plant.PLANT_CODE === selectedPlant;
                                    });

                                    if (filteredPlant && filteredPlant.length > 0) {
                                        $scope.currentUIPlant = filteredPlant[0];
                                    }
                                }

                                if ($scope.selectedVendors && $scope.selectedVendors.length > 1) {
                                    let filteredVendor = _.filter($scope.selectedVendors, function (vendor) {
                                        return vendor.vendorId === selectedVendor;
                                    });

                                    if (filteredVendor && filteredVendor.length > 0) {
                                        $scope.currentUIVendor = filteredVendor[0];
                                    }
                                }
                            }

                            $scope.formDetails = $scope.poRAWJSON;
                        }
                    });
            };

            $scope.GetPRItemsByRequirementId();

            $scope.GetRequirementPO = function () {
                PRMPOServices.getRequirementPO({ compid: $scope.currentUsercompanyId, reqid: $scope.reqId })
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementPOItems = response;
                        }
                    });
            };

            $scope.GetRequirementPO();

            $scope.GetRequirementVendorCodes = function () {
                auctionsService.getRequirementVendorCodes($scope.reqId, $scope.currentUserSessionId)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementVendorCodeInfo = response;
                        }
                    });
            };

            $scope.GetRequirementVendorCodes();

            $scope.submitPOToSAP = function (isDraft) {
                let sapDataList = [];
                $scope.POServiceStatusText = '';
                if ($scope.formDetails && $scope.formDetails.items && $scope.formDetails.items.length > 0) {
                    $scope.formDetails.items.forEach(function (poItem, index) {
                        if (!poItem.isDeleted) {
                            let PRNumberList = [];
                            let PRItemListList = [];
                            let counter = 0;
                            if (poItem.PR_NUMBER.includes(',')) {
                                PRNumberList = poItem.PR_NUMBER.split(',');
                                PRItemListList = poItem.ITEM_OF_REQUESITION.split(',');
                            }
                            else {
                                PRNumberList.push(poItem.PR_NUMBER);
                                PRItemListList.push(poItem.ITEM_OF_REQUESITION);
                            }

                            let currentProductObj = _.filter($scope.requirementDetails.listRequirementItems, function (item) {
                                return item.catalogueItemID === poItem.PRODUCT_ID;
                            });

                            let productCategoryID = currentProductObj[0].CATEGORY_ID;
                            
                            PRNumberList.forEach(function (prNumber, index) {
                                if (!poItem.QUANTITY) {
                                    poItem.quantityError = true;
                                    return;
                                } else {
                                    poItem.quantityError = false;
                                }

                                sapDataList.push({
                                    COMP_ID: $scope.currentUsercompanyId,
                                    REQ_ID: $scope.reqId,
                                    QCS_ID: $scope.qcsId,
                                    PO_TEMPLATE: $scope.documentType,
                                    LIFNR: $scope.formDetails.VENDOR_CODE,
                                    BSART: $scope.formDetails.DOC_TYPE,
                                    BUKRS: '',
                                    EKORG: '',
                                    ZTERM: $scope.formDetails.PAYMENT_TERMS_HIDE,
                                    ERNAM: $scope.formDetails.PO_CREATOR,
                                    INCO1: $scope.formDetails.INCO_TERMS1,
                                    INCO2: $scope.formDetails.INCO_TERMS2,
                                    EKGRP: $scope.formDetails.PURCHASE_GROUP_CODE,
                                    WAERS: $scope.formDetails.PO_CURRENCY,
                                    CATEGORY_ID: productCategoryID,
                                    MEMORYTYPE: $scope.formDetails.MEMORY_TYPE,
                                    DELIVERY_MODE: $scope.formDetails.Delivery_MODE, //PENDING
                                    ITM_SP_INST: $scope.formDetails.ITEM_SPECIAL_INST,
                                    ZZQANO: $scope.detectLinks($stateParams.quoteLink),
                                    ZZBONGING: $scope.formDetails.ZZBONGING,
                                    ZZMAP_NON_GMP: $scope.formDetails.GMP_TYPE_HIDE,
                                    VENDOR_ID: poItem.VENDOR_ID,
                                    VENDOR_NAME: poItem.VENDOR_NAME,
                                    VENDOR_COMPANY: poItem.VENDOR_COMPANY,
                                    VENDOR_PHONE: poItem.VENDOR_PHONE,
                                    VENDOR_EMAIL: poItem.VENDOR_EMAIL,
                                    BRANCH_LOCATION: poItem.BRANCH_LOCATION,
                                    PRODUCT_ID: poItem.PRODUCT_ID,
                                    SHORT_TEXT: poItem.SHORT_TEXT,
                                    INSURANCE_NUMBER: $scope.formDetails.INSURANCE_NUMBER,
                                    PENALITY_PER_WEEK: $scope.formDetails.PENALITY_PER_WEEK,
                                    NUMBER_WEEKS: $scope.formDetails.NUMBER_WEEKS,
                                    MAX_PENALITY: $scope.formDetails.MAX_PENALITY,
                                    MAX_WEEKS: $scope.formDetails.MAX_WEEKS,
                                    EINDT: poItem.Deliver_SCHEDULE,
                                    //ITM_SP_INST: poItem.ITEM_SPECIAL_INST,
                                    WERKS: poItem.PLANT,
                                    MTART: poItem.MATERIAL_TYPE,
                                    MATNR: poItem.MATERIAL_CODE,
                                    EMATN: poItem.MPN_CODE,
                                    BANFN: prNumber, //poItem.PR_NUMBER,
                                    BNFPO: PRItemListList[counter], //poItem.ITEM_OF_REQUESITION,
                                    MEINS: poItem.PO_UOM,
                                    BPRME: poItem.ORDER_PRICE_UNIT, //PENDING/DOUBT
                                    PEINH: poItem.PRICE_UNIT,
                                    PEINHFieldSpecified: (poItem.PACK_SIZE ? parseFloat(poItem.PACK_SIZE) : 0) ? 1 : 0,
                                    IDNLF: poItem.Vendor_CATALOG, //PENDING
                                    KONNR: '', //PENDING
                                    KTPNR: '', //PENDING
                                    MWSKZ: poItem.TAX_CODE,
                                    PBXX: poItem.PO_PRICE ? $scope.precisionRound(parseFloat(poItem.PO_PRICE)) : 0,
                                    PBXXFieldSpecified: (poItem.PO_PRICE ? parseFloat(poItem.PO_PRICE) : 0) ? 1 : 0,
                                    RB00: poItem.DISCOUNT_VALUE ? $scope.precisionRound(poItem.DISCOUNT_VALUE) : 0,
                                    RB00FieldSpecified: (poItem.DISCOUNT_VALUE ? parseFloat(poItem.DISCOUNT_VALUE) : 0) ? 1 : 0,
                                    P_26F1: poItem.PACK_FORWARD_PER ? parseFloat(poItem.PACK_FORWARD_PER) : 0,
                                    P_26F1FieldSpecified: (poItem.PACK_FORWARD_PER ? parseFloat(poItem.PACK_FORWARD_PER) : 0) ? 1 : 0,
                                    P_26F2: poItem.PACK_VALUE ? parseFloat(poItem.PACK_VALUE) : 0,
                                    P_26F2FieldSpecified: (poItem.PACK_VALUE ? parseFloat(poItem.PACK_VALUE) : 0) ? 1 : 0,
                                    FRA2: poItem.FRIEGHT_NON_TAX_PER ? parseFloat(poItem.FRIEGHT_NON_TAX_PER) : 0,
                                    FRA2FieldSpecified: (poItem.FRIEGHT_NON_TAX_PER ? parseFloat(poItem.FRIEGHT_NON_TAX_PER) : 0) ? 1 : 0,
                                    FRA1: poItem.FREIGHT_TAXABLE_PER ? parseFloat(poItem.FREIGHT_TAXABLE_PER) : 0,
                                    FRA1FieldSpecified: (poItem.FREIGHT_TAXABLE_PER ? parseFloat(poItem.FREIGHT_TAXABLE_PER) : 0) ? 1 : 0,
                                    FRB1: poItem.FREIGHT_VALUE ? parseFloat(poItem.FREIGHT_VALUE) : 0,
                                    FRB1FieldSpecified: (poItem.FREIGHT_VALUE ? parseFloat(poItem.FREIGHT_VALUE) : 0) ? 1 : 0,
                                    FRB2: poItem.FREIGHT_NON_TAX_PER ? parseFloat(poItem.FREIGHT_NON_TAX_PER) : 0,
                                    FRB2FieldSpecified: (poItem.FREIGHT_NON_TAX_PER ? parseFloat(poItem.FREIGHT_NON_TAX_PER) : 0) ? 1 : 0,
                                    ZMS1: poItem.MISC_TAXABLE_PER ? parseFloat(poItem.MISC_TAXABLE_PER) : 0,
                                    ZMS1FieldSpecified: (poItem.MISC_TAXABLE_PER ? parseFloat(poItem.MISC_TAXABLE_PER) : 0) ? 1 : 0,
                                    ZMS2: poItem.MISC_TAXABLE_VAL ? parseFloat(poItem.MISC_TAXABLE_VAL) : 0,
                                    ZMS2FieldSpecified: (poItem.MISC_TAXABLE_VAL ? parseFloat(poItem.MISC_TAXABLE_VAL) : 0) ? 1 : 0,
                                    ZMS3: poItem.MISC_NONTAX_PER ? parseFloat(poItem.MISC_NONTAX_PER) : 0,
                                    ZMS3FieldSpecified: (poItem.MISC_NONTAX_PER ? parseFloat(poItem.MISC_NONTAX_PER) : 0) ? 1 : 0,
                                    ZMS4: poItem.MISC_VALUE_NONTAX ? parseFloat(poItem.MISC_VALUE_NONTAX) : 0,
                                    ZMS4FieldSpecified: (poItem.MISC_VALUE_NONTAX ? parseFloat(poItem.MISC_VALUE_NONTAX) : 0) ? 1 : 0,
                                    MENGE: poItem.QUANTITY ? $scope.precisionRound(parseFloat(poItem.QUANTITY)) : 0,
                                    MENGEFieldSpecified: (poItem.QUANTITY ? parseFloat(poItem.QUANTITY) : 0) ? 1 : 0,
                                    NETPR: $scope.precisionRound(poItem.PO_NETPRICE),
                                    QUOT_NO: $scope.poQuotId ? $scope.poQuotId : '',
                                    USER: $scope.currentUserId
                                });

                                counter++;
                            });
                        }
                    });

                    //if ($scope.taxCodeErrorItems.length > 0) {
                    //    $scope.taxCodeError = 'Please select Tax Code.';
                    //    swal("Error!", $scope.taxCodeError, "error");
                    //    return;
                    //}
                    //if ($scope.docTypeErrorItems.length > 0) {
                    //    $scope.docTypeError = 'Please select Document Type.';
                    //    swal("Error!", $scope.docTypeError, "error");
                    //    return;
                    //}

                    var params = {
                        "data": sapDataList,
                        "rawJSON": JSON.stringify($scope.formDetails),
                        "qcsVendorAssignmentJSON": JSON.stringify($scope.vendorAssignments),
                        "qcsRequirementJSON": JSON.stringify($scope.requirementDetails),
                        "isDraft": isDraft ? true : false
                    };

                    PRMPOServices.SavePODetailsToFTP(params)
                        .then(function (response) {
                            $scope.GetRequirementPO();
                            if (!response.errorMessage) {
                                swal("Thanks !", response.message, "success");
                                $scope.POServiceStatusText = response.message;
                                if ($scope.qcsType === 'DOMESTIC') {
                                    $state.go("cost-comparisions-qcs", { "reqID": $scope.reqId, "qcsID": $scope.qcsId });
                                } else if ($scope.qcsType === 'IMPORT') {
                                    $state.go("import-qcs", { "reqID": $scope.reqId, "qcsID": $scope.qcsId });
                                }
                                //$state.go("cost-comparisions-qcs", { "reqID": $scope.reqId, "qcsID": $scope.qcsId });
                            } else {
                                swal("Error!", response.errorMessage, "error");
                                $scope.POServiceStatusText = response.errorMessage;
                            }
                        });
                }
            };

            $scope.detectLinks = function urlify(text) {
                if (text) {
                    var urlRegex = /(https?:\/\/[^\s]+)/g;
                    return text.replace(urlRegex, function (url) {
                        return url;//'<a target="_blank" href="' + url + '">' + url + '</a>';
                    });
                }
            };


            $scope.splitItemsOnVendorAssndQty = function ()
            {
                $scope.formDetails.items = [];
                $scope.TempSplitArray = [];
                $scope.splittedItems.forEach(function (splitItem,splitItemIndex) {
                    $scope.selectedVendors = [];
                    let vendorTemp = _.filter($scope.requirementDetails.auctionVendors, function (vendorObj) {
                        return vendorObj.vendorID === splitItem.VENDOR_ID;
                    });

                    if (vendorTemp && vendorTemp.length > 0) {
                        $scope.selectedVendors.push({ 'vendorId': splitItem.VENDOR_ID, 'vendorCompany': splitItem.VENDOR_NAME, vendorObj: vendorTemp[0] });
                    }

                    splitItem.itemIndex = splitItemIndex;
                    $scope.TempSplitArray.push(splitItem);
                    $scope.currentUIVendor = $scope.selectedVendors[0];
                    $scope.populateItems(splitItem);

                });
                $scope.splittedItems = $scope.TempSplitArray;
            };



            $scope.populateItems = function (splitItem) {
                if ($scope.currentUIVendor && $scope.currentUIVendor.vendorId) {
                    $scope.formDetails.VENDOR_COMPANY = $scope.currentUIVendor.vendorCompany;
                    $scope.formDetails.VENDOR_ID = $scope.currentUIVendor.vendorId;
                    $scope.formDetails.VENDOR_QUOTATION = $stateParams.quoteLink ? $stateParams.quoteLink : $scope.reqId + '-' + $scope.currentUIVendor.vendorId;
                    $scope.formDetails.VENDOR_QUOTATION1 = $scope.reqId + '-' + $scope.currentUIVendor.vendorId;
                    $scope.formDetails.INCO_TERMS1 = '';
                    $scope.formDetails.INCO_TERMS2 = '';
                    let currentVendorObj = _.filter($scope.requirementDetails.auctionVendors, function (vendorObj) {
                        return vendorObj.vendorID === $scope.currentUIVendor.vendorId;
                    });

                    let totalVendorItems = 1;
                    let packageTax = 0;
                    let packageValWithTax = 0;
                    let packageValWithOutTax = 0;

                    let freightTax = 0;
                    let freightValWithTax = 0;
                    let freightValWithOutTax = 0;

                    let miscTax = 0;
                    let miscValWithTax = 0;
                    let miscValWithOutTax = 0;


                    //Packing & Forwarding charges
                    var packaging = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                        return reqItem.productCode === 'Packing & Forwarding charges';
                    });

                    if (packaging && packaging.length > 0) {
                        packageTax = packaging[0].iGst ? packaging[0].iGst : (packaging[0].sGst + packaging[0].cGst);
                        packageValWithTax = packaging[0].itemPrice;
                        packageValWithOutTax = packaging[0].unitPrice;
                    }

                    //Freight charges
                    var freightCharges = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                        return reqItem.productCode === 'Freight charges';
                    });

                    if (freightCharges && freightCharges.length > 0) {
                        freightTax = freightCharges[0].iGst ? freightCharges[0].iGst : (freightCharges[0].sGst + freightCharges[0].cGst);
                        freightValWithTax = freightCharges[0].itemPrice;
                        freightValWithOutTax = freightCharges[0].unitPrice;
                    }

                    //Miscellenous
                    var miscellenous = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                        return reqItem.productCode === 'Miscellenous';
                    });

                    if (miscellenous && miscellenous.length > 0) {
                        miscTax = miscellenous[0].iGst ? miscellenous[0].iGst : (miscellenous[0].sGst + miscellenous[0].cGst);
                        miscValWithTax = miscellenous[0].itemPrice;
                        miscValWithOutTax = miscellenous[0].unitPrice;
                    }

                    if ($scope.currentUIVendor.vendorObj.gstNumber && $scope.requirementVendorCodeInfo && $scope.requirementVendorCodeInfo.length > 0) {
                        let vendorGstDetails = _.filter($scope.requirementVendorCodeInfo, function (vendorInfo) {
                            return vendorInfo.gstNumber && vendorInfo.vendorCode && vendorInfo.vendorID === $scope.currentUIVendor.vendorObj.vendorID &&
                                vendorInfo.gstNumber === $scope.currentUIVendor.vendorObj.gstNumber;
                        });

                        if (vendorGstDetails && vendorGstDetails.length > 0) {
                            $scope.formDetails.VENDOR_CODE = vendorGstDetails[0].vendorCode;
                        }
                    }

                    let vendorItems = _.filter($scope.splittedItems, function (assignment,index) {
                        if (index === splitItem.itemIndex) {
                            return assignment;
                        }
                    });

                    if (vendorItems && vendorItems.length > 0) {
                        totalVendorItems = vendorItems.length;

                        vendorItems.forEach(function (vendorItem, index) {
                            vendorItem.itemID = vendorItem.ITEM_ID;
                            vendorItem.assignedPrice = _.find($scope.vendorAssignments, { itemID: vendorItem.itemID }).assignedPrice;
                            let poItemObj = {
                                MPN_CODE: '',
                                Deliver_SCHEDULE: ''
                            };
                            var itemdetailsTemp = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                                return reqItem.itemID === vendorItem.itemID;
                            });

                            let prItemObj = _.filter($scope.requirementPRItems, function (prItem) {
                                return prItem.PR_NUMBER === vendorItem.PR_NUMBER && prItem.ITEM_ID === vendorItem.PR_ITEM_ID;
                            });

                            if (itemdetailsTemp && itemdetailsTemp.length > 0) {
                                if ($scope.requirementDetails.isDiscountQuotation === 1) {
                                    poItemObj.DISCOUNT_VALUE = $scope.precisionRound(itemdetailsTemp[0].revUnitDiscount, 2);

                                    poItemObj.PO_NETPRICE = parseFloat(itemdetailsTemp[0].unitMRP);
                                    poItemObj.PO_PRICE = parseFloat(itemdetailsTemp[0].unitMRP);
                                    poItemObj.NET_PRICE_PUR_DOC = $scope.precisionRound(parseFloat(itemdetailsTemp[0].unitMRP));

                                } else {
                                    poItemObj.DISCOUNT_VALUE = $scope.precisionRound(parseFloat(((itemdetailsTemp[0].unitPrice - itemdetailsTemp[0].revUnitPrice) / itemdetailsTemp[0].unitPrice) * 100), 2);

                                    poItemObj.PO_NETPRICE = parseFloat(itemdetailsTemp[0].revUnitPrice);
                                    poItemObj.PO_PRICE = parseFloat(itemdetailsTemp[0].revUnitPrice);
                                    poItemObj.NET_PRICE_PUR_DOC = $scope.precisionRound(parseFloat(itemdetailsTemp[0].revUnitPrice));


                                    //poItemObj.PO_NETPRICE = parseFloat(vendorItem.assignedPrice);
                                    //poItemObj.PO_PRICE = parseFloat(vendorItem.assignedPrice);
                                    //poItemObj.NET_PRICE_PUR_DOC = $scope.precisionRound(parseFloat(vendorItem.assignedPrice));
                                }
                                poItemObj.MATERIAL_CODE = itemdetailsTemp[0].productCode.replace(/^0+/, ''); // to Remove leading zeros
                                poItemObj.PRICE_UNIT = 1;
                                poItemObj.PO_UOM = itemdetailsTemp[0].productQuantityIn;
                                poItemObj.ORDER_PRICE_UNIT = itemdetailsTemp[0].productQuantityIn;
                                poItemObj.QUANTITY = +vendorItem.QTY;
                                if (!poItemObj.QUANTITY) {
                                    poItemObj.quantityError = true;
                                }
                            }

                            if (prItemObj && prItemObj.length > 0) {
                                let prNumbers = _.map(prItemObj, 'PR_NUMBER').join(); //_.uniq(_.map(prItemObj, 'PR_NUMBER')).join();
                                poItemObj.PR_NUMBER = prNumbers;
                                let prItemTotalQuantity = _.sumBy(prItemObj, function (item) {
                                    return item.REQUIRED_QUANTITY;
                                });

                                if (poItemObj.QUANTITY && prItemTotalQuantity && poItemObj.QUANTITY > prItemTotalQuantity) {
                                    poItemObj.QUANTITY = prItemTotalQuantity;
                                }

                                if ($scope.requirementPOItems && $scope.requirementPOItems.length > 0) {
                                    var currentVendorPOItems = _.filter($scope.requirementPOItems, function (reqPOItem) {
                                        return reqPOItem.VENDOR_ID === +$scope.formDetails.VENDOR_ID && reqPOItem.MATNR === prItemObj[0].ITEM_CODE.replace(/^0+/, '');
                                    });

                                    if (poItemObj.QUANTITY && currentVendorPOItems && currentVendorPOItems.length > 0) {
                                        poItemObj.QUANTITY = poItemObj.QUANTITY - currentVendorPOItems.MENGE;
                                    }
                                }

                                let prLineItems = _.map(prItemObj, 'ITEM_NUM').join(); //_.uniq(_.map(prItemObj, 'ITEM_NUM')).join();
                                poItemObj.ITEM_OF_REQUESITION = prLineItems;
                                poItemObj.MATERIAL_TYPE = prItemObj[0].MATERIAL_TYPE;
                                poItemObj.SHORT_TEXT = prItemObj[0].SHORT_TEXT;
                                poItemObj.PLANT = prItemObj[0].PLANT;
                                //poItemObj.PO_NETPRICE = vendorItem.assignedPrice * vendorItem.assignedQty;
                                poItemObj.MATERIAL_CODE = prItemObj[0].ITEM_CODE.replace(/^0+/, ''); // to Remove leading zeros

                            }

                            poItemObj.VENDOR_ID = currentVendorObj[0].vendorID;
                            poItemObj.VENDOR_CODE = currentVendorObj[0].vendorCode;
                            poItemObj.VENDOR_NAME = currentVendorObj[0].vendorName;
                            poItemObj.VENDOR_COMPANY = currentVendorObj[0].companyName;
                            poItemObj.VENDOR_PHONE = currentVendorObj[0].vendor.phoneNum;
                            poItemObj.VENDOR_EMAIL = currentVendorObj[0].vendor.email;
                            poItemObj.BRANCH_LOCATION = prItemObj[0].BRANCH_LOCATION;

                            //if (vendorItems[0].PR_NUMBER === prItemObj[0].PR_NUMBER) {
                            //    poItemObj.VERTICAL_TYPE = prItemObj[0].VERTICAL_TYPE;
                            //}
                            //poItemObj.docTypes = [];
                            //poItemObj.taxCodes = [];
                            //if (poItemObj.VERTICAL_TYPE == 'SEEDS') {
                            //    poItemObj.docTypes = $scope.SeedsDocTypes;
                            //    poItemObj.taxCodes = $scope.SeedsTaxCodes;
                            //}
                            //if (poItemObj.VERTICAL_TYPE == 'SUGARS') {
                            //    poItemObj.docTypes = $scope.SugarsDocTypes;
                            //    poItemObj.taxCodes = $scope.SugarsTaxCodes;
                            //}
                            //if (poItemObj.VERTICAL_TYPE == 'TEXTILES') {
                            //    poItemObj.docTypes = $scope.TextilesDocTypes;
                            //    poItemObj.taxCodes = $scope.TextilesTaxCodes;
                            //}

                            //Packing & Forwarding charges
                            var packaging = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                                return reqItem.productCode === 'Packing & Forwarding charges';
                            });

                            if (packaging && packaging.length > 0) {
                                poItemObj.PACK_FORWARD_PER = packageTax; //parseFloat((packageTax / totalVendorItems).toFixed(2));
                                poItemObj.PACK_VALUE = parseFloat((packageValWithTax / totalVendorItems).toFixed(2));
                            }

                            //Freight charges
                            var freightCharges = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                                return reqItem.productCode === 'Freight charges';
                            });

                            if (freightCharges && freightCharges.length > 0) {
                                poItemObj.FREIGHT_TAXABLE_PER = freightTax;//parseFloat((freightTax / totalVendorItems).toFixed(2));
                                poItemObj.FREIGHT_VALUE = parseFloat((freightValWithTax / totalVendorItems).toFixed(2));
                                poItemObj.FREIGHT_NON_TAX_PER = parseFloat((freightValWithOutTax / totalVendorItems).toFixed(2));
                            }

                            //Miscellenous
                            var miscellenous = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                                return reqItem.productCode === 'Miscellenous';
                            });

                            if (miscellenous && miscellenous.length > 0) {
                                poItemObj.MISC_TAXABLE_PER = miscTax; //parseFloat(( miscTax / totalVendorItems).toFixed(2));
                                poItemObj.MISC_TAXABLE_VAL = parseFloat((miscValWithTax / totalVendorItems).toFixed(2));
                                poItemObj.MISC_VALUE_NONTAX = parseFloat((miscValWithOutTax / totalVendorItems).toFixed(2));
                            }
                            poItemObj.PRODUCT_ID = prItemObj[0].PRODUCT_ID;
                            userService.getProfileDetails({ "userid": $scope.formDetails.VENDOR_ID, "sessionid": $scope.currentUserSessionId })
                                .then(function (response) {
                                    if (response) {
                                        SelectedVendorDetails = response;
                                        if (SelectedVendorDetails) {
                                            $scope.formDetails.PAYMENT_TERMS = SelectedVendorDetails.paymentTermDesc;
                                            $scope.formDetails.PAYMENT_TERMS_HIDE = SelectedVendorDetails.paymentTermCode;
                                            $scope.formDetails.INCO_TERMS1 = SelectedVendorDetails.incoTerm;

                                            $scope.COMPANY_INCO_TERMS.forEach(function (term, idx) {
                                                if (term.configValue == $scope.formDetails.INCO_TERMS1) {
                                                    $scope.formDetails.INCO_TERMS2 = term.configText;
                                                }
                                            });
                                        }
                                    }
                                });


                            if (poItemObj.PR_NUMBER) {
                                $scope.formDetails.items.push(poItemObj);
                            }

                        });

                    }

                }

            };

        }]);