﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class POScheduleDetails : ResponseAudit
    {
        [DataMember] [DataNames("PO_ID")] public int PO_ID { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PRM_PO_NUMBER")] public string PRM_PO_NUMBER { get; set; }
        [DataMember] [DataNames("PO_TYPE")] public string PO_TYPE { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string VENDOR_SITE_CODE { get; set; }
        [DataMember] [DataNames("VENDOR_PRIMARY_EMAIL")] public string VENDOR_PRIMARY_EMAIL { get; set; }
        [DataMember] [DataNames("VENDOR_COMPANY")] public string VENDOR_COMPANY { get; set; }        
        [DataMember] [DataNames("GST_NUMBER")] public string GST_NUMBER { get; set; }
        [DataMember] [DataNames("GST_ADDR")] public string GST_ADDR { get; set; }
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("TOTAL_VALUE")] public decimal TOTAL_VALUE { get; set; }
        [DataMember] [DataNames("BILL_TO_LOCATION_ID")] public string BILL_TO_LOCATION_ID { get; set; }
        [DataMember] [DataNames("SHIP_TO_LOCATION_ID")] public string SHIP_TO_LOCATION_ID { get; set; }
        [DataMember] [DataNames("BILL_TO_LOCATION_CODE")] public string BILL_TO_LOCATION_CODE { get; set; }
        [DataMember] [DataNames("SHIP_TO_LOCATION_CODE")] public string SHIP_TO_LOCATION_CODE { get; set; }
        [DataMember] [DataNames("ORG_ID")] public int ORG_ID { get; set; }
        [DataMember] [DataNames("PO_COMMENTS")] public string PO_COMMENTS { get; set; }
        [DataMember] [DataNames("PO_CREATOR")] public string PO_CREATOR { get; set; }
        [DataMember] [DataNames("PO_CREATOR_NAME")] public string PO_CREATOR_NAME { get; set; }
        [DataMember] [DataNames("PO_RELEASE_DATE")] public DateTime? PO_RELEASE_DATE { get; set; }
        [DataMember] [DataNames("DEPARTMENT_CODE")] public string DEPARTMENT_CODE { get; set; }
        [DataMember] [DataNames("WF_ID")] public int WF_ID { get; set; }
        [DataMember] [DataNames("QCS_ID")] public int QCS_ID { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("PO_STATUS")] public string PO_STATUS { get; set; }
        [DataMember(Name = "attachmentsArray")]
        public List<FileUpload> AttachmentsArray { get; set; }

        [DataMember(Name = "vendorAttachmentsArray")]
        public List<FileUpload> VendorAttachmentsArray { get; set; }
        [DataMember] [DataNames("ATTACHMENTS")] public string ATTACHMENTS { get; set; }
        [DataMember] [DataNames("VENDOR_ATTACHEMNTS")] public string VENDOR_ATTACHEMNTS { get; set; }
        [DataMember] [DataNames("EMAIL_SENT_DATE")] public DateTime? EMAIL_SENT_DATE { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }
        [DataMember] [DataNames("TOTAL_COUNT")] public int TOTAL_COUNT { get; set; }
        [DataMember] [DataNames("BUDGET_ALLOCATED")] public decimal BUDGET_ALLOCATED { get; set; }
        [DataMember] [DataNames("BUDGET_AVAILABLE")] public decimal BUDGET_AVAILABLE { get; set; }
        [DataMember] [DataNames("BUDGET_USED")] public decimal BUDGET_USED { get; set; }
        [DataMember] [DataNames("TENTATIVE_VALUE")] public decimal TENTATIVE_VALUE { get; set; }
        [DataMember] [DataNames("PR_NUMBER")] public string PR_NUMBER { get; set; }
        [DataMember] [DataNames("IS_BUDGETED")] public string IS_BUDGETED { get; set; }
        
        [DataMember] [DataNames("PO_ITEMS")] public List<POScheduleDetailsItems> PO_ITEMS { get; set; }
    }

    public class POScheduleDetailsItems : ResponseAudit
    {
        [DataMember] [DataNames("PO_ITEM_ID")] public int PO_ITEM_ID { get; set; }
        [DataMember] [DataNames("PO_ID")] public int PO_ID { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
        [DataMember] [DataNames("LINE_TYPE")] public string LINE_TYPE { get; set; }
        [DataMember] [DataNames("ORDER_QTY")] public decimal ORDER_QTY { get; set; }
        [DataMember] [DataNames("UOM")] public string UOM { get; set; }
        [DataMember] [DataNames("UNIT_PRICE")] public decimal UNIT_PRICE { get; set; }
        [DataMember] [DataNames("TOTAL_ITEM_AMOUNT")] public decimal TOTAL_ITEM_AMOUNT { get; set; }
        [DataMember] [DataNames("TOTAL_VALUE")] public decimal TOTAL_VALUE { get; set; }
        [DataMember] [DataNames("CGST")] public decimal CGST { get; set; }
        [DataMember] [DataNames("SGST")] public decimal SGST { get; set; }
        [DataMember] [DataNames("IGST")] public decimal IGST { get; set; }
        [DataMember] [DataNames("DELIVERY_DATE")] public DateTime? DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
        [DataMember] [DataNames("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
        [DataMember] [DataNames("CATEGORY_CODE")] public string CATEGORY_CODE { get; set; }
        [DataMember] [DataNames("SHIP_TO_ORGANIZATION_CODE")] public string SHIP_TO_ORGANIZATION_CODE { get; set; }
        [DataMember] [DataNames("SHIP_TO_LOCATION_ID")] public string SHIP_TO_LOCATION_ID { get; set; }
        //[DataMember] [DataNames("SHIP_TO_ORGANIZATION_CODE")] public string SHIP_TO_ORGANIZATION_CODE { get; set; }
        [DataMember] [DataNames("SHIP_TO_LOCATION_CODE")] public string SHIP_TO_LOCATION_CODE { get; set; }
        [DataMember] [DataNames("PR_NUMBERS")] public string PR_NUMBERS { get; set; }
        [DataMember] [DataNames("PR_LINE_ITEMS")] public string PR_LINE_ITEMS { get; set; }
        [DataMember] [DataNames("PR_ITEM_IDS")] public string PR_ITEM_IDS { get; set; }        
        [DataMember] [DataNames("PRODUCT_ID")] public int PRODUCT_ID { get; set; }
        [DataMember] [DataNames("CATEGORY_ID")] public int CATEGORY_ID { get; set; }
        




        [DataMember] [DataNames("ACK_QTY")] public decimal ACK_QTY { get; set; }
        [DataMember] [DataNames("ACK_DATE")] public DateTime? ACK_DATE { get; set; }        
        [DataMember] [DataNames("RECEIVED_QTY")] public decimal RECEIVED_QTY { get; set; }
        [DataMember] [DataNames("LAST_RECEIVED_DATE")] public DateTime? LAST_RECEIVED_DATE { get; set; }
        [DataMember] [DataNames("REJECTED_QTY")] public decimal REJECTED_QTY { get; set; }
        [DataMember] [DataNames("PENDING_QTY")] public decimal PENDING_QTY { get; set; }
        [DataMember] [DataNames("GRN_NUMBER")] public string GRN_NUMBER { get; set; }
        [DataMember] [DataNames("GRN_RECEIVED_QTY")] public decimal GRN_RECEIVED_QTY { get; set; }
        [DataMember] [DataNames("GRN_DELIVERY_DATE")] public DateTime? GRN_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("GRN_LINE_ITEM")] public string GRN_LINE_ITEM { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_STATUS")] public string VENDOR_ACK_STATUS { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_COMMENTS")] public string VENDOR_ACK_COMMENTS { get; set; }
        [DataMember] [DataNames("VENDOR_EXPECTED_DELIVERY_DATE")] public DateTime? VENDOR_EXPECTED_DELIVERY_DATE { get; set; }       
        [DataMember] [DataNames("GRNItem")] public List<GRNItem> GRNItems { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_REJECT_COMMENTS")] public string VENDOR_ACK_REJECT_COMMENTS { get; set; }
    }

    [DataContract]
    public class GRNItem : ResponseAudit
    {
        [DataMember] [DataNames("GRN_NUMBER")] public string GRN_NUMBER { get; set; }
        [DataMember] [DataNames("GRN_LINE_ITEM")] public string GRN_LINE_ITEM { get; set; }
        [DataMember] [DataNames("GRN_RECEIVED_QTY")] public decimal GRN_RECEIVED_QTY { get; set; }
        [DataMember] [DataNames("GRN_REJECTED_QTY")] public decimal GRN_REJECTED_QTY { get; set; }
        [DataMember] [DataNames("GRN_DELIVERY_DATE")] public DateTime? GRN_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
    }

    [DataContract]
    public class POInvoice : ResponseAudit
    {
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("INVOICE_NUMBER")] public string INVOICE_NUMBER { get; set; }
        [DataMember] [DataNames("INVOICE_AMOUNT")] public decimal INVOICE_AMOUNT { get; set; }
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("COMMENTS")] public string COMMENTS { get; set; }
        [DataMember] [DataNames("STATUS")] public string STATUS { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }

        [DataMember(Name = "attachmentsArray")]
        public List<FileUpload> AttachmentsArray { get; set; }
        [DataMember] [DataNames("ATTACHMENTS")] public string ATTACHMENTS { get; set; }
    }
}