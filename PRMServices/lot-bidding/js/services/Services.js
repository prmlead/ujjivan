prmApp.constant('PRMLotReqServiceDomain', 'lot-bidding/svc/PRMLotReqService.svc/REST/');
prmApp.constant('signalRFwdHubName', 'fwdRequirementHub');
prmApp.service('PRMLotReqService', ["PRMLotReqServiceDomain", "userService", "httpServices",
    function (PRMLotReqServiceDomain, userService, httpServices) {
        var PRMLotReqService = this;

        PRMLotReqService.lotdetails = function (lotid, reqid) {
            let url = PRMLotReqServiceDomain + 'lotdetails?reqid=' + reqid + '&lotid=' + lotid + '&user=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMLotReqService.lotRequirements = function (lotid, reqid, vendorid) {
            let url = PRMLotReqServiceDomain + 'lotrequirements?reqid=' + reqid + '&lotid=' + lotid + '&vendorid=' + vendorid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMLotReqService.getCompanyLots = function (compid, vendorid) {
            let url = PRMLotReqServiceDomain + 'companylots?compid=' + compid + '&vendorid=' + vendorid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMLotReqService.lotRequirementItems = function (id) {
            let url = PRMLotReqServiceDomain + 'lotrequirementitems?id=' + id + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMLotReqService.markLotAsComplete = function (lotid) {
            let url = PRMLotReqServiceDomain + 'marklotascomplete?lotid=' + lotid + '&user=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };
        
        PRMLotReqService.savelotdetails = function (params) {
            let url = PRMLotReqServiceDomain + 'savelotdetails';
            return httpServices.post(url, params);
        };

        PRMLotReqService.SaveLotRequirement = function (params) {
            let url = PRMLotReqServiceDomain + 'savelotrequirement';
            return httpServices.post(url, params);
        };

        PRMLotReqService.updateAuctionStart = function (params) {
            let url = PRMLotReqServiceDomain + 'updateAuctionStart';
            return httpServices.post(url, params);
        };
        

        return PRMLotReqService;

}]);