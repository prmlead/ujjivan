﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;
using PRMServices.models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMLotReqService
    {
        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "lotdetails?reqid={reqid}&lotid={lotid}&user={user}&sessionid={sessionid}")]
        LotDetails GetLotDetails(int reqid, int lotid, int user, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "companylots?compid={compid}&vendorid={vendorid}&sessionid={sessionid}")]
        List<LotDetails> GetCompanyLots(int compid, int vendorid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "lotrequirements?reqid={reqid}&lotid={lotid}&vendorid={vendorid}&sessionid={sessionid}")]
        Requirement[] GetLotRequirements(int reqid, int lotid, int vendorid, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "lotrequirementitems?id={id}&sessionid={sessionid}")]
        Requirement[] GetLotRequirementItems(int id, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "marklotascomplete?lotid={lotid}&user={user}&sessionid={sessionid}")]
        Response MarkLotAsComplete(int lotid, int user, string sessionid);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savelotdetails")]
        Response SaveLotDetails(LotDetails details);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updateAuctionStart")]
        Response UpdateAuctionStart(List<Requirement> itemids, string sessionid, int user);

        [AuthInterceptor][OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savelotrequirement")]
        Response SaveLotRequirement(string itemids, int lotid, string title, string desc, DateTime start,
            DateTime end, string sessionid, int user);

    }
}
