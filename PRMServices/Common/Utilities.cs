﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using OfficeOpenXml;
using PRMServices.Common;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using PRMServices.Models;
using System.Net;
using PRMServices.SQLHelper;
using System.ComponentModel;
using System.Threading.Tasks;
using GRID = SendGrid;
using GRID_EMAIL = SendGrid.Helpers.Mail;
using System.Net.Mail;
using System.Net.Http;
using System.ServiceModel.Web;
using System.ServiceModel.Channels;
using System.ServiceModel;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Runtime.Serialization;

namespace PRMServices
{
    public class Utilities
    {
        private const string Secret = "db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==";
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public const string SESSION_ERRORMSG = "The user session has timed out. Please log in again.";
        public const string ERRORMSG = "Unexpected Error Occured.";
        public const string ERRORMSG_SAME_USER_PASSWORD = "Username(Phone number) and Password are same, kindly please change password.";
        public const string USERERRORMSG = "User already exists.";
        public static string FILE_URL = ConfigurationManager.AppSettings["FILE_PATH"].ToString();
        public static string WEBSITE_URL = ConfigurationManager.AppSettings["WEBSITE_URL"].ToString();
        public const string OTP_MSG = "One Time Password(OTP) for PRM360 is $$$$.";
        public const string PRM_FROM = "PRMAPP";
        public const string WELCOME_MSG = "Congratulations and thank you for registering with PRM360 application. Please upload your credentials for verifications if you haven't already to use PRM360 to the fullest.\n Team PRM360";
        public const string GroupName = "requirementGroup";
        public const string FwdGroupName = "fwdRequirementGroup";
        public const string APMCGroupName = "apmcHub";
        public const string LogisticsGroupName = "logisticsHub";
        public const string PRMChatGroupName = "chatGroup";
        public const string UserIDComponent = "userIDComponent";
        public static string SITE_LINK = ConfigurationManager.AppSettings["SITE_LINK"].ToString();
        public static string MEDICAL_CENTRAL_PHARMACY = ConfigurationManager.AppSettings["MEDICAL_CENTRAL_PHARMACY"].ToString();
        public static string NON_MEDICAL_GENERAL_PHARMACY = ConfigurationManager.AppSettings["NON_MEDICAL_GENERAL_PHARMACY"].ToString();
        public const string ED_KEY = "Acads360";
        public const string USER_VALIDITY_ERRORMSG = "Your Validity has Ended.";
        public const string USER_VALIDITY_FROM_ERRORMSG = "Your Validity did not start yet";
        public const string USER_INVALID_ERRORMSG = "User is in in-active state";
        public const string USER_MULTIPLE_LOGINS = "Cannot have multiple active sessions. Please contact support.";
        public const string USER_OTP_ERROR = "OTP expired. Please generate new one.";
        public const string USER_OTP_INVALID_ERROR = "OTP invalid. Please enter correct one.";
        public const string PRM_APP_SESSIONS = "PRM_APP_SESSIONS";

        public static double RoundValue(double input)
        {
            return Math.Round(input, Convert.ToInt16(ConfigurationManager.AppSettings["ROUNDING_DECIMALS"]));
        }

        public static decimal RoundDecValue(decimal input)
        {
            return Math.Round(input, Convert.ToInt16(ConfigurationManager.AppSettings["ROUNDING_DECIMALS"]));
        }

        public static List<Currencies> GetCurrencyFactors(int compid, int reqid = 0, int userid = 0)
        {
            List<Currencies> currencies = new List<Currencies>();
            string query = $"SELECT * FROM CurrencyFactor WHERE COMP_ID = {compid}";
            if (reqid > 0)
            {
                query = $"SELECT * FROM CurrencyFactor WHERE COMP_ID = (SELECT GetCompanyID(U_ID) FROM requirementdetails WHERE REQ_ID = {reqid})";
            }

            if (userid > 0)
            {
                query = $"SELECT * FROM CurrencyFactor WHERE COMP_ID = GetCompanyID({userid})";
            }

            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            DataSet ds = sqlHelper.ExecuteQuery(query);
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (var row in ds.Tables[0].AsEnumerable())
                {
                    var currency = new Currencies();

                    currency.Currency = row["CURRENCY_CODE"] != null && row["CURRENCY_CODE"] != DBNull.Value ? row["CURRENCY_CODE"].ToString() : string.Empty;
                    currency.Factor = row["CURRENCY_FACTOR"] != null && row["CURRENCY_FACTOR"] != DBNull.Value ? Convert.ToDouble(row["CURRENCY_FACTOR"]) : 1;
                    currency.StartTime = row["START_DATE"] != null && row["START_DATE"] != DBNull.Value ? Convert.ToDateTime(row["START_DATE"]) : DateTime.MinValue;
                    currency.EndTime = row["END_DATE"] != null && row["END_DATE"] != DBNull.Value ? Convert.ToDateTime(row["END_DATE"]) : DateTime.MaxValue;
                    currencies.Add(currency);
                }
            }

            return currencies;
        }

        public static int ValidateSession(string sessionId,string AccessApi = null,string UserId = null)
        {
            int isValid = 1;
            bool isAccessDenied = false;
            try
            {
                WebOperationContext ctx = WebOperationContext.Current;
                if (ctx != null)
                {
                    sessionId = string.Empty;
                    UserId = string.Empty;

                    if (ctx != null && ctx.IncomingRequest.Headers["SESSION_ID"] != null)
                    {
                        sessionId = ctx.IncomingRequest.Headers["SESSION_ID"].ToString();
                    }

                    if (ctx != null && ctx.IncomingRequest.Headers["USER_ID"] != null)
                    {
                        UserId = ctx.IncomingRequest.Headers["USER_ID"].ToString();
                        //if (AccessApi != null)
                        //{
                        //    int isSuperuser = 0;
                        //    string query = $@"SELECT IS_SUPER_USER FROM userdata WHERE u_id = {ctx.IncomingRequest.Headers["user_id"]}";
                        //    IDatabaseHelper sqlHelper1 = DatabaseProvider.SGetDatabaseProvider();
                        //    DataSet dataSet = sqlHelper1.ExecuteQuery(query);
                        //    if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0 && dataSet.Tables[0].Rows[0][0] != null)
                        //    {
                        //        isSuperuser = dataSet.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[0][0].ToString()) : -1;
                        //        var apis = ConfigurationManager.AppSettings["ACCESS_APIS"].ToString().Split(',').ToList();
                        //        if (isSuperuser <= 0 && apis.Contains(AccessApi))
                        //        {
                        //            //throw new Exception("Access Denied");
                        //            throw new AccessDeniedException("Access Denied");
                        //        }
                        //    }
                        //}


                    }

                    if (string.IsNullOrEmpty(sessionId) || string.IsNullOrEmpty(UserId))
                    {
                        MessageHeaders messageHeaderInfos = OperationContext.Current.IncomingMessageHeaders;
                        sessionId = messageHeaderInfos.GetHeader<string>("SESSION_ID", "");
                        UserId = messageHeaderInfos.GetHeader<string>("USER_ID", "");
                    }

                }
                if (string.IsNullOrEmpty(sessionId))
                {
                    throw new Exception("Session ID is null");
                }

                if (string.IsNullOrEmpty(sessionId) || string.IsNullOrEmpty(UserId) || UserId.Equals("0"))
                {
                    throw new Exception("Un-authorized Exeception.");
                }


                #region validate from cache
                List<Entity> sessions = (List<Entity>)getCacheObject("PRM_APP_SESSIONS");
                if (sessions != null && sessions.Count > 0)
                {
                    int sessionTime = Convert.ToInt16(ConfigurationManager.AppSettings["SESSION_TIME"]);
                    Entity entity = sessions.Where(v => v.SessionID == sessionId).FirstOrDefault<Entity>();
                    if (entity != null)
                    {
                        if ((DateTime.UtcNow - entity.CurrentTime).Minutes <= sessionTime)
                        {
                            entity.CurrentTime = DateTime.UtcNow;
                            sessions = sessions.Where(v => v.SessionID != sessionId).ToList();
                            sessions.Add(entity);
                            setCacheObject(PRM_APP_SESSIONS, sessions);
                            return isValid;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }
                #endregion validate from cache

                IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_SESSION_ID", sessionId);
                sd.Add("P_U_ID", UserId);
                DataSet ds = sqlHelper.SelectList("cp_ValidateSession", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    isValid = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (AccessDeniedException ex) 
            {
                //throw ex;
                logger.Error("error is>>>"+ex);
                isAccessDenied = true;
                isValid = 0;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Server.Transfer("/prm360.html#/login");
                isValid = 0;
            }

            if (isValid <= 0)
            {
                if (isAccessDenied)
                {
                    throw new Exception("Access Denied.");
                }
                else { 
                    throw new Exception("Invalid Session ID passed");
                }
            }

            return isValid;
        }

        public static string CreateSession(string sessionId, int uid, string source)
        {
            bool rBool = true;
            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                sd.Add("P_U_ID", uid);
                sd.Add("P_SESSION_ID", sessionId);
                sd.Add("P_SOURCE", source);
                DataSet ds = sqlHelper.SelectList("cp_CreateSession", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    string ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][0].ToString()) : string.Empty;
                    if (ObjectID == "OTP not verified")
                    {
                        return ObjectID;
                    }
                    else if (string.IsNullOrEmpty(ObjectID))
                    {
                        rBool = false;
                        return Utilities.SESSION_ERRORMSG;
                    }
                    else
                    {
                        rBool = true;

                        Dictionary<string, DateTime> sessions = (Dictionary<string, DateTime>)getCacheObject(PRM_APP_SESSIONS);
                        if (sessions != null && sessions.Count > 0)
                        {
                            sessions.Add(sessionId, DateTime.Now);
                        }
                        else
                        {
                            sessions = new Dictionary<string, DateTime>();
                            sessions.Add(sessionId, DateTime.Now);
                        }

                        setCacheObject(PRM_APP_SESSIONS, sessions);

                        return ObjectID;
                    }
                }
            }
            catch (Exception ex)
            {
                rBool = false;
            }

            return Utilities.SESSION_ERRORMSG;
        }
        
        public static string GenerateEmailBody(string TemplateName)
        {
            PRMNotifications notifications = new PRMNotifications();
            return notifications.GenerateEmailBody(TemplateName);
        }

        public static string GetFromAddress(string FromAdd = null)
        {
            string from = ConfigurationManager.AppSettings["FROMADDRESS"].ToString();

            if (!string.IsNullOrEmpty(FromAdd))
            {
                from = FromAdd;
            };

            if (from.ToLower().Contains("@yahoo"))
            {
                from = ConfigurationManager.AppSettings["FROMADDRESS"].ToString();
            }


            return from;
        }

        public static string GetFromDisplayName(string DisplayName = null)
        {
            string from = ConfigurationManager.AppSettings["FROMDISPLAYNAME"].ToString();

            if (!string.IsNullOrEmpty(DisplayName))
            {
                from = DisplayName;
            };

            return from;
        }

        private static IDictionary<string, string> _mappings = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase) {
            #region Big list of mime types
        {".323", "text/h323"},
        {".3g2", "video/3gpp2"},
        {".3gp", "video/3gpp"},
        {".3gp2", "video/3gpp2"},
        {".3gpp", "video/3gpp"},
        {".7z", "application/x-7z-compressed"},
        {".aa", "audio/audible"},
        {".AAC", "audio/aac"},
        {".aaf", "application/octet-stream"},
        {".aax", "audio/vnd.audible.aax"},
        {".ac3", "audio/ac3"},
        {".aca", "application/octet-stream"},
        {".accda", "application/msaccess.addin"},
        {".accdb", "application/msaccess"},
        {".accdc", "application/msaccess.cab"},
        {".accde", "application/msaccess"},
        {".accdr", "application/msaccess.runtime"},
        {".accdt", "application/msaccess"},
        {".accdw", "application/msaccess.webapplication"},
        {".accft", "application/msaccess.ftemplate"},
        {".acx", "application/internet-property-stream"},
        {".AddIn", "text/xml"},
        {".ade", "application/msaccess"},
        {".adobebridge", "application/x-bridge-url"},
        {".adp", "application/msaccess"},
        {".ADT", "audio/vnd.dlna.adts"},
        {".ADTS", "audio/aac"},
        {".afm", "application/octet-stream"},
        {".ai", "application/postscript"},
        {".aif", "audio/x-aiff"},
        {".aifc", "audio/aiff"},
        {".aiff", "audio/aiff"},
        {".air", "application/vnd.adobe.air-application-installer-package+zip"},
        {".amc", "application/x-mpeg"},
        {".application", "application/x-ms-application"},
        {".art", "image/x-jg"},
        {".asa", "application/xml"},
        {".asax", "application/xml"},
        {".ascx", "application/xml"},
        {".asd", "application/octet-stream"},
        {".asf", "video/x-ms-asf"},
        {".ashx", "application/xml"},
        {".asi", "application/octet-stream"},
        {".asm", "text/plain"},
        {".asmx", "application/xml"},
        {".aspx", "application/xml"},
        {".asr", "video/x-ms-asf"},
        {".asx", "video/x-ms-asf"},
        {".atom", "application/atom+xml"},
        {".au", "audio/basic"},
        {".avi", "video/x-msvideo"},
        {".axs", "application/olescript"},
        {".bas", "text/plain"},
        {".bcpio", "application/x-bcpio"},
        {".bin", "application/octet-stream"},
        {".bmp", "image/bmp"},
        {".c", "text/plain"},
        {".cab", "application/octet-stream"},
        {".caf", "audio/x-caf"},
        {".calx", "application/vnd.ms-office.calx"},
        {".cat", "application/vnd.ms-pki.seccat"},
        {".cc", "text/plain"},
        {".cd", "text/plain"},
        {".cdda", "audio/aiff"},
        {".cdf", "application/x-cdf"},
        {".cer", "application/x-x509-ca-cert"},
        {".chm", "application/octet-stream"},
        {".class", "application/x-java-applet"},
        {".clp", "application/x-msclip"},
        {".cmx", "image/x-cmx"},
        {".cnf", "text/plain"},
        {".cod", "image/cis-cod"},
        {".config", "application/xml"},
        {".contact", "text/x-ms-contact"},
        {".coverage", "application/xml"},
        {".cpio", "application/x-cpio"},
        {".cpp", "text/plain"},
        {".crd", "application/x-mscardfile"},
        {".crl", "application/pkix-crl"},
        {".crt", "application/x-x509-ca-cert"},
        {".cs", "text/plain"},
        {".csdproj", "text/plain"},
        {".csh", "application/x-csh"},
        {".csproj", "text/plain"},
        {".css", "text/css"},
        {".csv", "text/csv"},
        {".cur", "application/octet-stream"},
        {".cxx", "text/plain"},
        {".dat", "application/octet-stream"},
        {".datasource", "application/xml"},
        {".dbproj", "text/plain"},
        {".dcr", "application/x-director"},
        {".def", "text/plain"},
        {".deploy", "application/octet-stream"},
        {".der", "application/x-x509-ca-cert"},
        {".dgml", "application/xml"},
        {".dib", "image/bmp"},
        {".dif", "video/x-dv"},
        {".dir", "application/x-director"},
        {".disco", "text/xml"},
        {".dll", "application/x-msdownload"},
        {".dll.config", "text/xml"},
        {".dlm", "text/dlm"},
        {".doc", "application/msword"},
        {".docm", "application/vnd.ms-word.document.macroEnabled.12"},
        {".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
        {".dot", "application/msword"},
        {".dotm", "application/vnd.ms-word.template.macroEnabled.12"},
        {".dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template"},
        {".dsp", "application/octet-stream"},
        {".dsw", "text/plain"},
        {".dtd", "text/xml"},
        {".dtsConfig", "text/xml"},
        {".dv", "video/x-dv"},
        {".dvi", "application/x-dvi"},
        {".dwf", "drawing/x-dwf"},
        {".dwp", "application/octet-stream"},
        {".dxr", "application/x-director"},
        {".eml", "message/rfc822"},
        {".emz", "application/octet-stream"},
        {".eot", "application/octet-stream"},
        {".eps", "application/postscript"},
        {".etl", "application/etl"},
        {".etx", "text/x-setext"},
        {".evy", "application/envoy"},
        {".exe", "application/octet-stream"},
        {".exe.config", "text/xml"},
        {".fdf", "application/vnd.fdf"},
        {".fif", "application/fractals"},
        {".filters", "Application/xml"},
        {".fla", "application/octet-stream"},
        {".flr", "x-world/x-vrml"},
        {".flv", "video/x-flv"},
        {".fsscript", "application/fsharp-script"},
        {".fsx", "application/fsharp-script"},
        {".generictest", "application/xml"},
        {".gif", "image/gif"},
        {".group", "text/x-ms-group"},
        {".gsm", "audio/x-gsm"},
        {".gtar", "application/x-gtar"},
        {".gz", "application/x-gzip"},
        {".h", "text/plain"},
        {".hdf", "application/x-hdf"},
        {".hdml", "text/x-hdml"},
        {".hhc", "application/x-oleobject"},
        {".hhk", "application/octet-stream"},
        {".hhp", "application/octet-stream"},
        {".hlp", "application/winhlp"},
        {".hpp", "text/plain"},
        {".hqx", "application/mac-binhex40"},
        {".hta", "application/hta"},
        {".htc", "text/x-component"},
        {".htm", "text/html"},
        {".html", "text/html"},
        {".htt", "text/webviewhtml"},
        {".hxa", "application/xml"},
        {".hxc", "application/xml"},
        {".hxd", "application/octet-stream"},
        {".hxe", "application/xml"},
        {".hxf", "application/xml"},
        {".hxh", "application/octet-stream"},
        {".hxi", "application/octet-stream"},
        {".hxk", "application/xml"},
        {".hxq", "application/octet-stream"},
        {".hxr", "application/octet-stream"},
        {".hxs", "application/octet-stream"},
        {".hxt", "text/html"},
        {".hxv", "application/xml"},
        {".hxw", "application/octet-stream"},
        {".hxx", "text/plain"},
        {".i", "text/plain"},
        {".ico", "image/x-icon"},
        {".ics", "application/octet-stream"},
        {".idl", "text/plain"},
        {".ief", "image/ief"},
        {".iii", "application/x-iphone"},
        {".inc", "text/plain"},
        {".inf", "application/octet-stream"},
        {".inl", "text/plain"},
        {".ins", "application/x-internet-signup"},
        {".ipa", "application/x-itunes-ipa"},
        {".ipg", "application/x-itunes-ipg"},
        {".ipproj", "text/plain"},
        {".ipsw", "application/x-itunes-ipsw"},
        {".iqy", "text/x-ms-iqy"},
        {".isp", "application/x-internet-signup"},
        {".ite", "application/x-itunes-ite"},
        {".itlp", "application/x-itunes-itlp"},
        {".itms", "application/x-itunes-itms"},
        {".itpc", "application/x-itunes-itpc"},
        {".IVF", "video/x-ivf"},
        {".jar", "application/java-archive"},
        {".java", "application/octet-stream"},
        {".jck", "application/liquidmotion"},
        {".jcz", "application/liquidmotion"},
        {".jfif", "image/pjpeg"},
        {".jnlp", "application/x-java-jnlp-file"},
        {".jpb", "application/octet-stream"},
        {".jpe", "image/jpeg"},
        {".jpeg", "image/jpeg"},
        {".jpg", "image/jpeg"},
        {".js", "application/x-javascript"},
        {".json", "application/json"},
        {".jsx", "text/jscript"},
        {".jsxbin", "text/plain"},
        {".latex", "application/x-latex"},
        {".library-ms", "application/windows-library+xml"},
        {".lit", "application/x-ms-reader"},
        {".loadtest", "application/xml"},
        {".lpk", "application/octet-stream"},
        {".lsf", "video/x-la-asf"},
        {".lst", "text/plain"},
        {".lsx", "video/x-la-asf"},
        {".lzh", "application/octet-stream"},
        {".m13", "application/x-msmediaview"},
        {".m14", "application/x-msmediaview"},
        {".m1v", "video/mpeg"},
        {".m2t", "video/vnd.dlna.mpeg-tts"},
        {".m2ts", "video/vnd.dlna.mpeg-tts"},
        {".m2v", "video/mpeg"},
        {".m3u", "audio/x-mpegurl"},
        {".m3u8", "audio/x-mpegurl"},
        {".m4a", "audio/m4a"},
        {".m4b", "audio/m4b"},
        {".m4p", "audio/m4p"},
        {".m4r", "audio/x-m4r"},
        {".m4v", "video/x-m4v"},
        {".mac", "image/x-macpaint"},
        {".mak", "text/plain"},
        {".man", "application/x-troff-man"},
        {".manifest", "application/x-ms-manifest"},
        {".map", "text/plain"},
        {".master", "application/xml"},
        {".mda", "application/msaccess"},
        {".mdb", "application/x-msaccess"},
        {".mde", "application/msaccess"},
        {".mdp", "application/octet-stream"},
        {".me", "application/x-troff-me"},
        {".mfp", "application/x-shockwave-flash"},
        {".mht", "message/rfc822"},
        {".mhtml", "message/rfc822"},
        {".mid", "audio/mid"},
        {".midi", "audio/mid"},
        {".mix", "application/octet-stream"},
        {".mk", "text/plain"},
        {".mmf", "application/x-smaf"},
        {".mno", "text/xml"},
        {".mny", "application/x-msmoney"},
        {".mod", "video/mpeg"},
        {".mov", "video/quicktime"},
        {".movie", "video/x-sgi-movie"},
        {".mp2", "video/mpeg"},
        {".mp2v", "video/mpeg"},
        {".mp3", "audio/mpeg"},
        {".mp4", "video/mp4"},
        {".mp4v", "video/mp4"},
        {".mpa", "video/mpeg"},
        {".mpe", "video/mpeg"},
        {".mpeg", "video/mpeg"},
        {".mpf", "application/vnd.ms-mediapackage"},
        {".mpg", "video/mpeg"},
        {".mpp", "application/vnd.ms-project"},
        {".mpv2", "video/mpeg"},
        {".mqv", "video/quicktime"},
        {".ms", "application/x-troff-ms"},
        {".msi", "application/octet-stream"},
        {".mso", "application/octet-stream"},
        {".mts", "video/vnd.dlna.mpeg-tts"},
        {".mtx", "application/xml"},
        {".mvb", "application/x-msmediaview"},
        {".mvc", "application/x-miva-compiled"},
        {".mxp", "application/x-mmxp"},
        {".nc", "application/x-netcdf"},
        {".nsc", "video/x-ms-asf"},
        {".nws", "message/rfc822"},
        {".ocx", "application/octet-stream"},
        {".oda", "application/oda"},
        {".odc", "text/x-ms-odc"},
        {".odh", "text/plain"},
        {".odl", "text/plain"},
        {".odp", "application/vnd.oasis.opendocument.presentation"},
        {".ods", "application/oleobject"},
        {".odt", "application/vnd.oasis.opendocument.text"},
        {".one", "application/onenote"},
        {".onea", "application/onenote"},
        {".onepkg", "application/onenote"},
        {".onetmp", "application/onenote"},
        {".onetoc", "application/onenote"},
        {".onetoc2", "application/onenote"},
        {".orderedtest", "application/xml"},
        {".osdx", "application/opensearchdescription+xml"},
        {".p10", "application/pkcs10"},
        {".p12", "application/x-pkcs12"},
        {".p7b", "application/x-pkcs7-certificates"},
        {".p7c", "application/pkcs7-mime"},
        {".p7m", "application/pkcs7-mime"},
        {".p7r", "application/x-pkcs7-certreqresp"},
        {".p7s", "application/pkcs7-signature"},
        {".pbm", "image/x-portable-bitmap"},
        {".pcast", "application/x-podcast"},
        {".pct", "image/pict"},
        {".pcx", "application/octet-stream"},
        {".pcz", "application/octet-stream"},
        {".pdf", "application/pdf"},
        {".pfb", "application/octet-stream"},
        {".pfm", "application/octet-stream"},
        {".pfx", "application/x-pkcs12"},
        {".pgm", "image/x-portable-graymap"},
        {".pic", "image/pict"},
        {".pict", "image/pict"},
        {".pkgdef", "text/plain"},
        {".pkgundef", "text/plain"},
        {".pko", "application/vnd.ms-pki.pko"},
        {".pls", "audio/scpls"},
        {".pma", "application/x-perfmon"},
        {".pmc", "application/x-perfmon"},
        {".pml", "application/x-perfmon"},
        {".pmr", "application/x-perfmon"},
        {".pmw", "application/x-perfmon"},
        {".png", "image/png"},
        {".pnm", "image/x-portable-anymap"},
        {".pnt", "image/x-macpaint"},
        {".pntg", "image/x-macpaint"},
        {".pnz", "image/png"},
        {".pot", "application/vnd.ms-powerpoint"},
        {".potm", "application/vnd.ms-powerpoint.template.macroEnabled.12"},
        {".potx", "application/vnd.openxmlformats-officedocument.presentationml.template"},
        {".ppa", "application/vnd.ms-powerpoint"},
        {".ppam", "application/vnd.ms-powerpoint.addin.macroEnabled.12"},
        {".ppm", "image/x-portable-pixmap"},
        {".pps", "application/vnd.ms-powerpoint"},
        {".ppsm", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"},
        {".ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
        {".ppt", "application/vnd.ms-powerpoint"},
        {".pptm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12"},
        {".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
        {".prf", "application/pics-rules"},
        {".prm", "application/octet-stream"},
        {".prx", "application/octet-stream"},
        {".ps", "application/postscript"},
        {".psc1", "application/PowerShell"},
        {".psd", "application/octet-stream"},
        {".psess", "application/xml"},
        {".psm", "application/octet-stream"},
        {".psp", "application/octet-stream"},
        {".pub", "application/x-mspublisher"},
        {".pwz", "application/vnd.ms-powerpoint"},
        {".qht", "text/x-html-insertion"},
        {".qhtm", "text/x-html-insertion"},
        {".qt", "video/quicktime"},
        {".qti", "image/x-quicktime"},
        {".qtif", "image/x-quicktime"},
        {".qtl", "application/x-quicktimeplayer"},
        {".qxd", "application/octet-stream"},
        {".ra", "audio/x-pn-realaudio"},
        {".ram", "audio/x-pn-realaudio"},
        {".rar", "application/octet-stream"},
        {".ras", "image/x-cmu-raster"},
        {".rat", "application/rat-file"},
        {".rc", "text/plain"},
        {".rc2", "text/plain"},
        {".rct", "text/plain"},
        {".rdlc", "application/xml"},
        {".resx", "application/xml"},
        {".rf", "image/vnd.rn-realflash"},
        {".rgb", "image/x-rgb"},
        {".rgs", "text/plain"},
        {".rm", "application/vnd.rn-realmedia"},
        {".rmi", "audio/mid"},
        {".rmp", "application/vnd.rn-rn_music_package"},
        {".roff", "application/x-troff"},
        {".rpm", "audio/x-pn-realaudio-plugin"},
        {".rqy", "text/x-ms-rqy"},
        {".rtf", "application/rtf"},
        {".rtx", "text/richtext"},
        {".ruleset", "application/xml"},
        {".s", "text/plain"},
        {".safariextz", "application/x-safari-safariextz"},
        {".scd", "application/x-msschedule"},
        {".sct", "text/scriptlet"},
        {".sd2", "audio/x-sd2"},
        {".sdp", "application/sdp"},
        {".sea", "application/octet-stream"},
        {".searchConnector-ms", "application/windows-search-connector+xml"},
        {".setpay", "application/set-payment-initiation"},
        {".setreg", "application/set-registration-initiation"},
        {".settings", "application/xml"},
        {".sgimb", "application/x-sgimb"},
        {".sgml", "text/sgml"},
        {".sh", "application/x-sh"},
        {".shar", "application/x-shar"},
        {".shtml", "text/html"},
        {".sit", "application/x-stuffit"},
        {".sitemap", "application/xml"},
        {".skin", "application/xml"},
        {".sldm", "application/vnd.ms-powerpoint.slide.macroEnabled.12"},
        {".sldx", "application/vnd.openxmlformats-officedocument.presentationml.slide"},
        {".slk", "application/vnd.ms-excel"},
        {".sln", "text/plain"},
        {".slupkg-ms", "application/x-ms-license"},
        {".smd", "audio/x-smd"},
        {".smi", "application/octet-stream"},
        {".smx", "audio/x-smd"},
        {".smz", "audio/x-smd"},
        {".snd", "audio/basic"},
        {".snippet", "application/xml"},
        {".snp", "application/octet-stream"},
        {".sol", "text/plain"},
        {".sor", "text/plain"},
        {".spc", "application/x-pkcs7-certificates"},
        {".spl", "application/futuresplash"},
        {".src", "application/x-wais-source"},
        {".srf", "text/plain"},
        {".SSISDeploymentManifest", "text/xml"},
        {".ssm", "application/streamingmedia"},
        {".sst", "application/vnd.ms-pki.certstore"},
        {".stl", "application/vnd.ms-pki.stl"},
        {".sv4cpio", "application/x-sv4cpio"},
        {".sv4crc", "application/x-sv4crc"},
        {".svc", "application/xml"},
        {".swf", "application/x-shockwave-flash"},
        {".t", "application/x-troff"},
        {".tar", "application/x-tar"},
        {".tcl", "application/x-tcl"},
        {".testrunconfig", "application/xml"},
        {".testsettings", "application/xml"},
        {".tex", "application/x-tex"},
        {".texi", "application/x-texinfo"},
        {".texinfo", "application/x-texinfo"},
        {".tgz", "application/x-compressed"},
        {".thmx", "application/vnd.ms-officetheme"},
        {".thn", "application/octet-stream"},
        {".tif", "image/tiff"},
        {".tiff", "image/tiff"},
        {".tlh", "text/plain"},
        {".tli", "text/plain"},
        {".toc", "application/octet-stream"},
        {".tr", "application/x-troff"},
        {".trm", "application/x-msterminal"},
        {".trx", "application/xml"},
        {".ts", "video/vnd.dlna.mpeg-tts"},
        {".tsv", "text/tab-separated-values"},
        {".ttf", "application/octet-stream"},
        {".tts", "video/vnd.dlna.mpeg-tts"},
        {".txt", "text/plain"},
        {".u32", "application/octet-stream"},
        {".uls", "text/iuls"},
        {".user", "text/plain"},
        {".ustar", "application/x-ustar"},
        {".vb", "text/plain"},
        {".vbdproj", "text/plain"},
        {".vbk", "video/mpeg"},
        {".vbproj", "text/plain"},
        {".vbs", "text/vbscript"},
        {".vcf", "text/x-vcard"},
        {".vcproj", "Application/xml"},
        {".vcs", "text/plain"},
        {".vcxproj", "Application/xml"},
        {".vddproj", "text/plain"},
        {".vdp", "text/plain"},
        {".vdproj", "text/plain"},
        {".vdx", "application/vnd.ms-visio.viewer"},
        {".vml", "text/xml"},
        {".vscontent", "application/xml"},
        {".vsct", "text/xml"},
        {".vsd", "application/vnd.visio"},
        {".vsi", "application/ms-vsi"},
        {".vsix", "application/vsix"},
        {".vsixlangpack", "text/xml"},
        {".vsixmanifest", "text/xml"},
        {".vsmdi", "application/xml"},
        {".vspscc", "text/plain"},
        {".vss", "application/vnd.visio"},
        {".vsscc", "text/plain"},
        {".vssettings", "text/xml"},
        {".vssscc", "text/plain"},
        {".vst", "application/vnd.visio"},
        {".vstemplate", "text/xml"},
        {".vsto", "application/x-ms-vsto"},
        {".vsw", "application/vnd.visio"},
        {".vsx", "application/vnd.visio"},
        {".vtx", "application/vnd.visio"},
        {".wav", "audio/wav"},
        {".wave", "audio/wav"},
        {".wax", "audio/x-ms-wax"},
        {".wbk", "application/msword"},
        {".wbmp", "image/vnd.wap.wbmp"},
        {".wcm", "application/vnd.ms-works"},
        {".wdb", "application/vnd.ms-works"},
        {".wdp", "image/vnd.ms-photo"},
        {".webarchive", "application/x-safari-webarchive"},
        {".webtest", "application/xml"},
        {".wiq", "application/xml"},
        {".wiz", "application/msword"},
        {".wks", "application/vnd.ms-works"},
        {".WLMP", "application/wlmoviemaker"},
        {".wlpginstall", "application/x-wlpg-detect"},
        {".wlpginstall3", "application/x-wlpg3-detect"},
        {".wm", "video/x-ms-wm"},
        {".wma", "audio/x-ms-wma"},
        {".wmd", "application/x-ms-wmd"},
        {".wmf", "application/x-msmetafile"},
        {".wml", "text/vnd.wap.wml"},
        {".wmlc", "application/vnd.wap.wmlc"},
        {".wmls", "text/vnd.wap.wmlscript"},
        {".wmlsc", "application/vnd.wap.wmlscriptc"},
        {".wmp", "video/x-ms-wmp"},
        {".wmv", "video/x-ms-wmv"},
        {".wmx", "video/x-ms-wmx"},
        {".wmz", "application/x-ms-wmz"},
        {".wpl", "application/vnd.ms-wpl"},
        {".wps", "application/vnd.ms-works"},
        {".wri", "application/x-mswrite"},
        {".wrl", "x-world/x-vrml"},
        {".wrz", "x-world/x-vrml"},
        {".wsc", "text/scriptlet"},
        {".wsdl", "text/xml"},
        {".wvx", "video/x-ms-wvx"},
        {".x", "application/directx"},
        {".xaf", "x-world/x-vrml"},
        {".xaml", "application/xaml+xml"},
        {".xap", "application/x-silverlight-app"},
        {".xbap", "application/x-ms-xbap"},
        {".xbm", "image/x-xbitmap"},
        {".xdr", "text/plain"},
        {".xht", "application/xhtml+xml"},
        {".xhtml", "application/xhtml+xml"},
        {".xla", "application/vnd.ms-excel"},
        {".xlam", "application/vnd.ms-excel.addin.macroEnabled.12"},
        {".xlc", "application/vnd.ms-excel"},
        {".xld", "application/vnd.ms-excel"},
        {".xlk", "application/vnd.ms-excel"},
        {".xll", "application/vnd.ms-excel"},
        {".xlm", "application/vnd.ms-excel"},
        {".xls", "application/vnd.ms-excel"},
        {".xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12"},
        {".xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12"},
        {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
        {".xlt", "application/vnd.ms-excel"},
        {".xltm", "application/vnd.ms-excel.template.macroEnabled.12"},
        {".xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template"},
        {".xlw", "application/vnd.ms-excel"},
        {".xml", "text/xml"},
        {".xmta", "application/xml"},
        {".xof", "x-world/x-vrml"},
        {".XOML", "text/plain"},
        {".xpm", "image/x-xpixmap"},
        {".xps", "application/vnd.ms-xpsdocument"},
        {".xrm-ms", "text/xml"},
        {".xsc", "application/xml"},
        {".xsd", "text/xml"},
        {".xsf", "text/xml"},
        {".xsl", "text/xml"},
        {".xslt", "text/xml"},
        {".xsn", "application/octet-stream"},
        {".xss", "application/xml"},
        {".xtp", "application/octet-stream"},
        {".xwd", "image/x-xwindowdump"},
        {".z", "application/x-compress"},
        {".zip", "application/x-zip-compressed"},
        #endregion
        };

        public static DataTable WorksheetToDataTable(ExcelWorksheet oSheet)
        {
            int totalRows = oSheet.Dimension.End.Row;
            int totalCols = oSheet.Dimension.End.Column;
            DataTable dt = new DataTable(oSheet.Name);
            DataRow dr = null;
            for (int i = 1; i <= totalRows; i++)
            {
                if (i > 1) dr = dt.Rows.Add();
                for (int j = 1; j <= totalCols; j++)
                {
                    if (i == 1)
                    {
                        dt.Columns.Add(oSheet.Cells[i, j].Value.ToString());
                    }
                    else
                    {
                        if (oSheet.Cells[i, j] != null && oSheet.Cells[i, j].Value != null)
                        {
                            dr[j - 1] = oSheet.Cells[i, j].Value.ToString();
                        }
                        else
                        {
                            dr[j - 1] = string.Empty;
                        }
                    }
                }
            }
            return dt;
        }

        public static object getCacheObject(string cacheKey)
        {
            //if (ServerConfig.CachingEnabled)
            //{
            //    return WCFCache.Current[cacheKey];
            //}

            return null;

        }

        public static void setCacheObject(string cacheKey, object details)
        {
            //if (ServerConfig.CachingEnabled)
            //{
            //    if (ServerConfig.SlidingExpirationTime <= 0 || ServerConfig.SlidingExpirationTime == int.MaxValue)
            //    {
            //        WCFCache.Current[cacheKey] = details;
            //    }
            //    else
            //    {
            //        WCFCache.Current.Insert(cacheKey, details, new TimeSpan(0, 0, ServerConfig.SlidingExpirationTime), false);
            //    }
            //}            

        }

        public static string replaceDomain(string stringToReplace)
        {
            stringToReplace.Replace("https://www.prm360.com", Utilities.WEBSITE_URL);
            return stringToReplace;
        }


        // This constant is used to determine the keysize of the encryption algorithm in bits.
        // We divide this by 8 within the code below to get the equivalent number of bytes.
        private const int Keysize = 256;

        // This constant determines the number of iterations for the password bytes generation function.
        private const int DerivationIterations = 1000;

        public static string Encrypt(string plainText, string passPhrase)
        {
            // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
            // so that the same Salt and IV values can be used when decrypting.  
            var saltStringBytes = Generate256BitsOfRandomEntropy();
            var ivStringBytes = Generate256BitsOfRandomEntropy();
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                var cipherTextBytes = saltStringBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        public static string Decrypt(string cipherText, string passPhrase)
        {
            // Get the complete stream of bytes that represent:
            // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
            var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
            // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
            var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
            // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
            var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
            // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
            var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

            using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                var plainTextBytes = new byte[cipherTextBytes.Length];
                                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }

        private static byte[] Generate256BitsOfRandomEntropy()
        {
            var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                // Fill the array with cryptographically secure random bytes.
                rngCsp.GetBytes(randomBytes);
            }
            return randomBytes;
        }

        public static Response GetAttachment(string attachmentID, string sessionID)
        {
            Response response = new Response();
            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                ValidateSession(sessionID);
                sd.Add("P_ATT_ID", attachmentID);
                DataSet ds = sqlHelper.SelectList("cp_GetAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.Message = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][0].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public static void SaveFile(string fileName, byte[] fileContent)
        {
            var allowedExtns = ConfigurationManager.AppSettings["SUPPORTED.FILE.EXT"].ToString().Split(',').ToList();
            var isValid = allowedExtns.Any(e => fileName.ToLower().EndsWith(e));

            if (isValid)
            {
                File.WriteAllBytes(fileName, fileContent);
            }
            else
            {
                logger.Warn("Unsupported file uploaded: " + fileName);
            }
        }

        public static Response SaveAttachment(string path)
        {
            Response response = new Response();
            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PATH", path);
                DataSet ds = sqlHelper.SelectList("cp_SaveAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public static string GetEnumDesc<T>(string value) where T : struct, IConvertible
        {
            var type = typeof(T);
            var memInfo = type.GetMember(value);
            var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            var description = ((DescriptionAttribute)attributes[0]).Description;
            return description;
        }


        public static string SaveFileBytes(byte[] content, string fileName)
        {
            string message = string.Empty;

            try
            {
                var allowedExtns = ConfigurationManager.AppSettings["SUPPORTED.FILE.EXT"].ToString().Split(',').ToList();
                var isValid = allowedExtns.Any(e => fileName.ToLower().EndsWith(e));

                if (isValid)
                {
                    File.WriteAllBytes(fileName, content);
                }
                else
                {
                    logger.Warn("Unsupported file uploaded: " + fileName);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            //logger.Debug("message-shfdkj-PTMG:{0}", message);

            return message;
        }

        public static string MeetingRequestString(string from, List<string> toUsers, string subject, string desc, string location, DateTime startTime, DateTime endTime, TimeZoneInfo timezone, int? eventID = null, bool isCancel = false)
        {
            desc = desc.Replace("\r\n", "");
            desc = desc.Replace("&", "and").Replace("<br/>", "").Replace(">", string.Empty).Replace("<", string.Empty).Replace("a href=\"", " ").Replace("\"", " ");
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("BEGIN:VCALENDAR");
            sb.AppendLine("PRODID:-//Microsoft Corporation//Outlook 16.0 MIMEDIR//EN"); // new
            sb.AppendLine("VERSION:2.0");
            sb.AppendLine("METHOD:REQUEST");
            sb.AppendLine("X-MS-OLK-FORCEINSPECTOROPEN:TRUE"); // new
            //sb.AppendLine("BEGIN:VTIMEZONE");
            //sb.AppendLine("TZID:" + timezone.Id);
            //sb.AppendLine("BEGIN:STANDARD");
            //sb.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmss}", startTime));
            //sb.AppendLine("RRULE:FREQ=YEARLY;BYDAY=1SU;BYHOUR=2;BYMINUTE=0;BYMONTH=11");
            //sb.AppendLine("TZNAME:" + timezone.Id);
            //sb.AppendLine("TZOFFSETFROM:+0530");
            //sb.AppendLine("TZOFFSETTO:+0000");
            //sb.AppendLine("END:STANDARD");
            //sb.AppendLine("BEGIN:DAYLIGHT");
            //sb.AppendLine("DTSTART:20070301T020000");
            //sb.AppendLine("RRULE:FREQ=YEARLY;BYDAY=2SU;BYHOUR=2;BYMINUTE=0;BYMONTH=3");
            //sb.AppendLine("TZNAME:" + timezone.Id);
            //sb.AppendLine("TZOFFSETFROM:+0530");
            //sb.AppendLine("TZOFFSETTO:+0000");
            //sb.AppendLine("END:DAYLIGHT");
            //sb.AppendLine("END:VTIMEZONE");
            sb.AppendLine("BEGIN:VTIMEZONE");
            sb.AppendLine("TZID:Asia/Kolkata");
            sb.AppendLine("BEGIN:STANDARD");
            sb.AppendLine("TZOFFSETFROM:+0530");
            sb.AppendLine("TZOFFSETTO:+0530");
            sb.AppendLine("DTSTART:19451015T000000");
            sb.AppendLine("TZNAME:IST");
            sb.AppendLine("END:STANDARD");
            sb.AppendLine("END:VTIMEZONE");
            sb.AppendLine("BEGIN:VEVENT");
            sb.AppendLine("CLASS:PUBLIC"); // new
            sb.AppendLine(string.Format("CREATED:{0:yyyyMMddTHHmmss}", startTime)); // new
            sb.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", string.Join(",", toUsers), string.Join(",", toUsers)));
            sb.AppendLine(string.Format("DESCRIPTION:{0}", desc));
            sb.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:<html><body>{0}</body></html>", desc));
            sb.AppendLine("FMTTYPE:text/html");
            sb.AppendLine(string.Format("DTEND;TZID=Asia/Kolkata:{0:yyyyMMddTHHmmss}", endTime));
            sb.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmss}", DateTime.Now));
            sb.AppendLine(string.Format("DTSTART;TZID=Asia/Kolkata:{0:yyyyMMddTHHmmss}", startTime));
            sb.AppendLine(string.Format("LAST-MODIFIED:{0:yyyyMMddTHHmmss}", startTime));
            sb.AppendLine(string.Format("LOCATION: {0}", location));
            sb.AppendLine(string.Format("ORGANIZER;CN=\"{0}\":MAILTO:{1}", from, from));
            sb.AppendLine("PRIORITY: 5"); // new
            sb.AppendLine("SEQUENCE:0");
            sb.AppendLine("ACTION:DISPLAY");
            //sb.AppendLine("SUMMARY;LANGUAGE=en-ca:Meeting Request"); // changed
            sb.AppendLine(string.Format("SUMMARY:{0}", subject));
            sb.AppendLine("TRANSP:OPAQUE"); // new
            sb.AppendLine(string.Format("UID:{0}", (eventID.HasValue ? "PRM360" + eventID : Guid.NewGuid().ToString())));
            sb.AppendLine("X-MICROSOFT-CDO-BUSYSTATUS:TENTATIVE"); // new
            sb.AppendLine("X-MICROSOFT-CDO-IMPORTANCE:1"); // new
            sb.AppendLine("X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY"); // new
            sb.AppendLine("BEGIN:VALARM"); // new
            sb.AppendLine("TRIGGER:-PT15M"); // new
            sb.AppendLine("ACTION:DISPLAY"); // new
            sb.AppendLine("DESCRIPTION:Reminder"); // new
            sb.AppendLine("END:VALARM"); // new
            sb.AppendLine("END:VEVENT");
            sb.AppendLine("END:VCALENDAR");

            return sb.ToString();
        }

        public static FileData DownloadFile(string path, string sessioID, byte[] attachment = null, string fileName = "")
        {
            FileData fd = new FileData();
            string logURL = string.Empty;
            try
            {
                if (attachment == null)
                {
                    byte[] fileContent = null;
                    using (WebClient client = new WebClient())
                    {
                        Response response = GetAttachment(path, sessioID);
                        fd.FileName = response.Message;
                        logURL = Utilities.WEBSITE_URL + Utilities.FILE_URL + response.Message + "?sessionid=" + sessioID;
                        logger.Info(logURL);
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                        fileContent = client.DownloadData(Utilities.WEBSITE_URL + Utilities.FILE_URL + response.Message + "?sessionid=" + sessioID);
                    }

                    fd.FileStream = new MemoryStream(fileContent);
                }
                else
                {
                    fd.FileName = fileName;
                    fd.FileStream = new MemoryStream(attachment);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data DownloadFile");
                FileData fd1 = new FileData();
                return fd1;
            }

            return fd;
        }

        public static Response GetSessionId(int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_GetSessionId", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    response.ObjectID = row["P_U_ID"] != DBNull.Value ? Convert.ToInt32(row["P_U_ID"]) : 0;
                    response.Message = row["PARAM_SESSION_ID"] != DBNull.Value ? Convert.ToString(row["PARAM_SESSION_ID"]) : string.Empty;
                    response.ErrorMessage = row["ERROR_MESSAGE"] != DBNull.Value ? Convert.ToString(row["ERROR_MESSAGE"]) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            return response;
        }

        public static string SaveAttachments(int reqID, string sessionID, int userID, string attachmentID, int revised, string fileType)
        {
            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                int isValidSession = ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_ATTACHMENT_ID", attachmentID);
                sd.Add("P_IS_REVISED", revised);
                sd.Add("P_FILE_TYPE", fileType);
                DataSet ds = sqlHelper.SelectList("cp_SaveQuotationAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response.ErrorMessage;
        }

        public static async Task SendEmail(string To, string Subject, string Body, int reqID, int vendorID, string module, string sessionID = null, Attachment attachment = null, List<Attachment> ListAttachment = null, DateTime? startTime = null, long duration = 0, string reminderType = "")
        {
            GRID.SendGridClient sendGridClient = new GRID.SendGridClient(ConfigurationManager.AppSettings["SEND_GRID_KEY"].ToString());
            GRID_EMAIL.SendGridMessage sendGridMessage = new GRID_EMAIL.SendGridMessage();
            List<SendGrid.Helpers.Mail.Attachment> sendGridAttachements = new List<SendGrid.Helpers.Mail.Attachment>();
            try
            {
                int compID1;
                Communication communication = new Communication();
                communication = GetCommunicationData("EMAIL_FROM", sessionID, reqID: reqID);
                if (communication.Company.SendEmails != 0 || string.IsNullOrEmpty(sessionID))
                {

                    //Body = Body.Replace("COMPANY_LOGO_ID", communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
                    Body = Body.Replace("COMPANY_NAME", string.Equals(module, "END_NEGOTIATION", StringComparison.InvariantCultureIgnoreCase) ? String.Empty : !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : "PRM360");
                    Body = Body.Replace("USER_NAME", string.Equals(module, "END_NEGOTIATION", StringComparison.InvariantCultureIgnoreCase) ? String.Empty : !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");
                    Body = Body.Replace("COMPANY_ADDRESS", string.Equals(module, "END_NEGOTIATION", StringComparison.InvariantCultureIgnoreCase) ? String.Empty : !string.IsNullOrEmpty(communication.CompanyAddress) ? communication.CompanyAddress : "");
                    Body = Utilities.replaceDomain(Body);
                    Subject = Utilities.replaceDomain(Subject);

                    SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString());
                    System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
                    smtpClient.Credentials = credentials;
                    smtpClient.EnableSsl = false;
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(GetFromAddress(communication.User.Email), GetFromDisplayName(communication.User.Email));
                    mail.From = new MailAddress("no-reply@prm360.com", "no-reply@prm360.com");
                    sendGridMessage.From = new GRID_EMAIL.EmailAddress("no-reply@prm360.com", "no-reply@prm360.com");
                    mail.BodyEncoding = System.Text.Encoding.ASCII;
                    if (startTime != null && startTime.HasValue && !string.IsNullOrEmpty(reminderType))
                    {
                        List<string> emails = new List<string>();
                        emails = To.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList<string>().Distinct().ToList();
                        string fileName = "";
                        if (reminderType.Equals("QUOTATION_FREEZE"))
                        {
                            Subject = "Quotation upload reminder " + Subject;
                            fileName = "Quotation Reminder";
                        }
                        else if (reminderType.Equals("NEGOTATION_START"))
                        {
                            Subject = "Negotiation reminder " + Subject;
                            fileName = "Negotiation Reminder";
                        }

                        TimeZoneInfo timeInfo = TimeZoneInfo.Utc;
                        if (!string.IsNullOrEmpty(communication.TimeZoneId))
                        {
                            try
                            {
                                timeInfo = TimeZoneInfo.FindSystemTimeZoneById(communication.TimeZoneId);
                                startTime = TimeZoneInfo.ConvertTimeFromUtc(startTime.Value, timeInfo);
                            }
                            catch { }
                        }

                        string icsContent = Utilities.MeetingRequestString("no-reply@prm360.com", emails, Subject, Body, "On-Line", startTime.Value, startTime.Value.AddMinutes(duration), timeInfo);
                        byte[] calendarBytes = System.Text.Encoding.UTF8.GetBytes(icsContent.ToString());
                        GRID_EMAIL.Attachment calendarAttachment = new GRID_EMAIL.Attachment();
                        calendarAttachment.Filename = fileName + ".ics";
                        calendarAttachment.Content = Convert.ToBase64String(calendarBytes);
                        calendarAttachment.Type = "text/calendar";
                        sendGridAttachements.Add(calendarAttachment);
                    }

                    if (attachment != null)
                    {
                        mail.Attachments.Add(attachment);
                        GRID_EMAIL.Attachment attachmentTemp = new GRID_EMAIL.Attachment();
                        byte[] bytes;
                        using (var memoryStream = new MemoryStream())
                        {
                            attachment.ContentStream.Seek(0, SeekOrigin.Begin);
                            attachment.ContentStream.CopyTo(memoryStream);
                            bytes = memoryStream.ToArray();
                        }

                        attachmentTemp.Content = Convert.ToBase64String(bytes);
                        attachmentTemp.Filename = attachment.Name;
                        //attachmentTemp.Type = attachment.ContentType.Name;
                        attachmentTemp.ContentId = attachment.ContentId;
                        sendGridAttachements.Add(attachmentTemp);
                    }

                    if (ListAttachment != null)
                    {
                        foreach (Attachment singleAttachment in ListAttachment)
                        {
                            if (singleAttachment != null)
                            {
                                mail.Attachments.Add(singleAttachment);


                                GRID_EMAIL.Attachment attachmentTemp = new GRID_EMAIL.Attachment();
                                byte[] bytes;
                                using (var memoryStream = new MemoryStream())
                                {
                                    singleAttachment.ContentStream.Seek(0, SeekOrigin.Begin);
                                    singleAttachment.ContentStream.CopyTo(memoryStream);
                                    bytes = memoryStream.ToArray();
                                }

                                attachmentTemp.Content = Convert.ToBase64String(bytes);
                                attachmentTemp.Filename = singleAttachment.Name;
                                //attachmentTemp.Type = singleAttachment.ContentType.Name;
                                attachmentTemp.ContentId = singleAttachment.ContentId;
                                sendGridAttachements.Add(attachmentTemp);
                            }
                        }
                    }

                    List<string> ToAddresses = To.Split(',').ToList<string>();
                    foreach (string address in ToAddresses)
                    {
                        if (!string.IsNullOrEmpty(address))
                        {
                            mail.To.Add(new MailAddress(address));
                        }
                    }

                    DateTime currentdate = DateTime.UtcNow;
                    var current_date = currentdate.ToString("yyyy/MM/dd HH:mm:ss");
                    string message_body = Body;
                    EmailLogs compID = GetCompanyidByVendorid(vendorID);
                    compID1 = compID.CompID;
                    string emailLogQuery = string.Empty;
                    foreach (var item in mail.To)
                    {
                        message_body = message_body.Replace("<br/>", "");
                        message_body = message_body.Replace("<b>", "");
                        message_body = message_body.Replace("</b>", "");
                        emailLogQuery += string.Format("INSERT INTO emaillogs" +
                            "(COMP_ID,V_USER_ID, REQ_ID, USER_NAME,USER_EMAIL, MESSAGE, SUBJECT,REQUIREMENT,DATE_CREATED,DATE_MODIFIED)" +
                            "values('{4}','{6}','{5}','{0}','{1}','{2}','{3}','{7}','{8}','{9}');",
                            item.User, item.Address, message_body, Subject, compID1, reqID, vendorID, module, current_date, current_date);
                        mail.Subject = Subject;
                        mail.IsBodyHtml = true;
                        mail.Body = Body;
                        sendGridMessage.Subject = Subject;
                        sendGridMessage.HtmlContent = Body;
                        sendGridMessage.AddTos(mail.To.Distinct().ToList().Select(m => new GRID_EMAIL.EmailAddress(m.Address)).ToList());
                        if (sendGridAttachements != null && sendGridAttachements.Count > 0)
                        {
                            sendGridMessage.Attachments = sendGridAttachements;
                        }
                        logger.Info("Send email Before");
                        var sendGridResponse = await sendGridClient.SendEmailAsync(sendGridMessage);
                        logger.Info(sendGridResponse.StatusCode);
                        logger.Info(sendGridResponse.Body.ReadAsStringAsync().Result);
                        logger.Info(sendGridResponse.Headers.ToString());
                        logger.Info("Send email after");

                        Response response = new Response();
                        response.ObjectID = 1;
                    }

                    IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
                    sqlHelper.ExecuteNonQuery_IUD(emailLogQuery);
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                Response response = new Response();
                response.ErrorMessage = ex.Message;
            }
        }

        public static async Task<string> SendSMS(string from, string to, string message, int reqID, int vendorID, string module, string sessionID = null, bool isOTP = false)
        {
            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            try
            {
                int compID1;
                var mobileList = (from string phone in to.Split(',').ToList()
                                  where !string.IsNullOrEmpty(phone) && phone.Length == 10
                                  && (phone.StartsWith("6") || phone.StartsWith("7") || phone.StartsWith("8") || phone.StartsWith("9"))
                                  select phone).Distinct().ToList();
                to = string.Join(",", mobileList);
                Communication communication = new Communication();
                communication = GetCommunicationData("SMS_FROM", sessionID, reqID: reqID);
                if (communication.Company.SendEmails != 0 || string.IsNullOrEmpty(sessionID))
                {
                    message = message.Replace("COMPANY_LOGO_ID", string.Equals(module, "END_NEGOTIATION", StringComparison.InvariantCultureIgnoreCase) ? String.Empty : communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
                    message = message.Replace("COMPANY_NAME", string.Equals(module, "END_NEGOTIATION", StringComparison.InvariantCultureIgnoreCase) ? String.Empty : !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : "PRM360");
                    message = message.Replace("USER_NAME", string.Equals(module, "END_NEGOTIATION", StringComparison.InvariantCultureIgnoreCase) ? String.Empty : !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");
                    message = Utilities.replaceDomain(message);
                    if (string.IsNullOrEmpty(from))
                    {
                        from = ConfigurationManager.AppSettings["SMSFROM"].ToString();
                        if (!string.IsNullOrEmpty(communication.FromAdd))
                        {
                            from = communication.FromAdd;
                        }
                    }

                    message = message.Replace("&", "and").Replace("<br/>", Environment.NewLine);
                    string strUrl = string.Format(ConfigurationManager.AppSettings["SMSURL"].ToString(), from, to, message);
                    if (isOTP)
                    {
                        strUrl = string.Format(ConfigurationManager.AppSettings["SMSURL_OTP"].ToString(), from, to, message);
                    }


                    string decodedUrl = HttpUtility.UrlDecode(message);

                    DateTime currentdate = DateTime.UtcNow;
                    var current_date = currentdate.ToString("yyyy/MM/dd HH:mm:ss");

                    EmailLogs compID = GetCompanyidByVendorid(vendorID);
                    compID1 = compID.CompID;

                    string smslLogQuery = string.Format("INSERT INTO smslogs" +
                        " (SMS_PH_NUMBER, SMS_MESSAGE, COMP_ID, V_USER_ID, REQ_ID,REQUIREMENT,DATE_CREATED,DATE_MODIFIED) " +
                        " values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}'); ",
                        to, decodedUrl, compID1, vendorID, reqID, module, current_date, current_date);
                    sqlHelper.ExecuteNonQuery_IUD(smslLogQuery);
                    using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                    {
                        HttpResponseMessage httpResponse = client.GetAsync(strUrl).Result;
                        httpResponse.EnsureSuccessStatusCode();
                        string result = httpResponse.Content.ReadAsStringAsync().Result;
                    }

                    return string.Empty;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        public static TelegramMsg SendTelegramMsg(TelegramMsg telegrammsg)
        {

            string returnValue = string.Empty;

            try
            {
                //telegrammsg.Message = Utilities.replaceDomain(telegrammsg.Message);
                //string group = string.IsNullOrEmpty(telegrammsg.Group) ? ConfigurationManager.AppSettings["TELEGRAM_MSG"] : telegrammsg.Group;
                //PTMGenericServicesClient client = new PTMGenericServicesClient();
                //client.SendTelegramMessage(telegrammsg.Message, group);
                //telegrammsg.CurrentTime = DateTime.MaxValue;
            }
            catch (Exception ex)
            {
                telegrammsg.ErrorMessage = ex.Message;
            }

            return telegrammsg;
        }

        public static Communication GetCommunicationData(string communicationType, string sessionID = null, int reqID = 0)
        {
            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Communication communication = new Communication();
            communication.User = new User();
            communication.Company = new Company();
            try
            {
                ValidateSession(sessionID);
                sd.Add("P_SESSION", sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_C_TYPE", communicationType); // (SMS or EMAIL or TELEGRAM)
                DataSet ds = sqlHelper.SelectList("cp_GetCommunicationData", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    communication.User.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    communication.User.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    communication.User.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    communication.User.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    communication.User.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    communication.Company.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    communication.Company.CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                    communication.Company.SendEmails = row["SEND_EMAILS"] != DBNull.Value ? Convert.ToInt32(row["SEND_EMAILS"]) : 0;
                    communication.CompanyAddress = row["ADDRESS"] != DBNull.Value ? Convert.ToString(row["ADDRESS"]) : string.Empty;
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                    {
                        DataRow row1 = ds.Tables[1].Rows[0];
                        communication.FromAdd = row1["CONFIG_VALUE"] != DBNull.Value ? Convert.ToString(row1["CONFIG_VALUE"]) : string.Empty;
                        communication.DisplayName = row1["CONFIG_TEXT"] != DBNull.Value ? Convert.ToString(row1["CONFIG_TEXT"]) : string.Empty;
                    }
                }

                if (ds != null && ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0 && ds.Tables[2].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[2].Rows[0];
                    communication.TimeZoneId = row["ZONE_NAME"] != DBNull.Value ? Convert.ToString(row["ZONE_NAME"]) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                communication.ErrorMessage = ex.Message;
            }

            return communication;
        }

        public static EmailLogs GetCompanyidByVendorid(int ID)
        {
            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            EmailLogs details = new EmailLogs();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_ID", ID);
                DataSet ds = sqlHelper.SelectList("el_GetCompanyidByVendorid", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        EmailLogs user = new EmailLogs();
                        user.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                        details = user;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public static string GenerateForwardEmailBody(string TemplateName)
        {
            PRMNotifications notifications = new PRMNotifications();

            return notifications.GenerateForwardEmailBody(TemplateName);
            //string body = string.Empty;
            ////body = Engine.Razor.RunCompile(File.ReadAllText(templateFolderPath + "/EmailTemplates/" + TemplateName), "Email", null, model);
            //XmlDocument doc = new XmlDocument();
            //doc.Load(templateFolderPath + "/EmailTemplates/EmailFormats.xml");
            //XmlNode node = doc.DocumentElement.SelectSingleNode(TemplateName);
            //body = node.InnerText;
            //string footerName = "";
            //if (TemplateName.ToLower().Contains("email"))
            //{
            //    footerName = "EmailFooter";
            //}
            //else if (TemplateName.ToLower().Contains("sms"))
            //{
            //    footerName = "SMSFooter";
            //}
            //else
            //{
            //    footerName = "FooterXML";
            //}


            //if(footerName == "FooterXML")
            //{

            //}
            //else
            //{
            //    XmlNode footernode = doc.DocumentElement.SelectSingleNode(footerName);
            //    body += footernode.InnerText;
            //}


            //return body;
        }

        public static Response GetCompanyId(int userid, string sessionid)
        {
            Response response = new Response();
            int isValidSession = ValidateSession(sessionid);
            string query = string.Format("SELECT COMP_ID  from company where CREATED_BY = (SELECT SUPER_U_ID from userdata where U_ID = {0});", userid);
            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            DataSet ds = sqlHelper.ExecuteQuery(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
            }

            return response;
        }

        public static double GetRequirementCurrency(int requirementId, string currency)
        {
            double factor = 1;
            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            DataSet ds = sqlHelper.ExecuteQuery($"SELECT * From RequirementSettings WHERE REQ_ID = {requirementId} AND REQ_SETTING = 'CURRENCY_{requirementId}_{currency.ToUpper()}'");
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                factor = ds.Tables[0].Rows[0]["REQ_SETTING_VALUE"] != DBNull.Value ? Convert.ToDouble(ds.Tables[0].Rows[0]["REQ_SETTING_VALUE"].ToString()) : 0;
            }

            return factor;
        }

        public async Task SendSMTPEmail(string To, string Subject, string Body,  List<Attachment> ListAttachment = null, string cc = "")
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(RemoteServerCertificateValidationCallback);
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
            SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString(), Convert.ToInt16(ConfigurationManager.AppSettings["MAILHOST_PORT"]));
            MailMessage mail = new MailMessage();

            try
            {
                Body = Utilities.replaceDomain(Body);
                Subject = Utilities.replaceDomain(Subject);
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["MAILHOST_SSL_ENABLED"].ToString());
                mail.From = new MailAddress(GetFromAddress(""), GetFromDisplayName(""));
                mail.BodyEncoding = System.Text.Encoding.ASCII;

                if (ListAttachment != null)
                {
                    foreach (Attachment singleAttachment in ListAttachment)
                    {
                        if (singleAttachment != null)
                        {
                            mail.Attachments.Add(singleAttachment);
                        }
                    }
                }

                List<string> ToAddresses = To.Split(',').ToList<string>();
                foreach (string address in ToAddresses)
                {
                    if (!string.IsNullOrEmpty(address))
                    {
                        mail.To.Add(new MailAddress(address));
                    }
                }

                if (!string.IsNullOrEmpty(cc))
                {
                    List<string> ccAddresses = cc.Split(';').ToList<string>();
                    foreach (string address in ccAddresses)
                    {
                        if (!string.IsNullOrEmpty(address))
                        {
                            mail.CC.Add(new MailAddress(address));
                        }
                    }
                }

                string message_body = Body;
                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                mail.Body = Body;
                mail.Subject = Subject;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                logger.Info("Send email Before");
                smtpClient.Send(mail);
                logger.Info("Send email after");
                Response response = new Response();
                response.ObjectID = 1;
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                Response response = new Response();
                response.ErrorMessage = ex.Message;
            }
            finally
            {
                mail = null;
                smtpClient = null;
            }
        }

        public static string GetJWTToken(UserInfo userInfo, string sessionid)
        {
            string JWTtoken = string.Empty;
            try
            {
                var symmetricKey = Convert.FromBase64String(Secret);
                var tokenHandler = new JwtSecurityTokenHandler();

                var now = DateTime.UtcNow;
                var permClaims = new List<Claim>();
                permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
                permClaims.Add(new Claim("SESSION_ID", sessionid));
                permClaims.Add(new Claim("USER_ID", userInfo.UserID));
                permClaims.Add(new Claim("USER_TYPE", userInfo.UserType));
                permClaims.Add(new Claim("COMPANY", userInfo.Institution));

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(permClaims),

                    Expires = now.AddDays(1),

                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature)
                };

                var stoken = tokenHandler.CreateToken(tokenDescriptor);
                JWTtoken = tokenHandler.WriteToken(stoken);
            }
            catch
            {

            }

            return JWTtoken;
        }

        public static ClaimsPrincipal ValidateJWTToken(string token = "")
        {
            try
            {
                WebOperationContext ctx = WebOperationContext.Current;
                if (string.IsNullOrEmpty(token) && ctx != null && ctx.IncomingRequest.Headers["PRM360-AUTHORIZATION-TOKEN"] != null)
                {
                    token = Convert.ToString(ctx.IncomingRequest.Headers["PRM360-AUTHORIZATION-TOKEN"].ToString());
                }

                if (!string.IsNullOrWhiteSpace(token))
                {
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                    if (jwtToken == null) { }
                    //return null;

                    var symmetricKey = Convert.FromBase64String(Secret);

                    var validationParameters = new TokenValidationParameters()
                    {
                        RequireExpirationTime = true,
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                    };

                    SecurityToken securityToken;
                    var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

                    return principal;
                }
            }
            catch (Exception ex)
            {
                
            }

            return null;
        }

        public static string GetWTTokenKeyValue(string key, string token = "")
        {
            string value = "";
            try
            {
                var principal = ValidateJWTToken(token);
                if (principal != null && principal.Claims != null && principal.Claims.Count() > 0 && principal.Claims.ToList().Any(c => c.Type == key))
                {
                    value = principal.Claims.ToList().First(c => c.Type == key).Value;
                }
            }
            catch (Exception ex)
            {

            }

            return value;
        }

        private bool RemoteServerCertificateValidationCallback(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

    }

    [Serializable]
    internal class AccessDeniedException : Exception
    {
        public AccessDeniedException()
        {
        }

        public AccessDeniedException(string message) : base(message)
        {
            
        }

        public AccessDeniedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AccessDeniedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}