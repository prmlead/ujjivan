﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Configuration;
using PRMServices.Common;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using PRMServices.Models;
using iTextSharp.text.pdf.draw;
using PRMServices.models;

namespace PRMServices
{
    public class PdfUtilities
    {
        private static List<PRMTemplateFields> templateFields = new List<PRMTemplateFields>();
        public static int decimal_round = Convert.ToInt32(ConfigurationManager.AppSettings["ROUNDING_DECIMALS"]);
        public static List<KeyValuePair<string, DataTable>> MakeDataTable(int reqID, Requirement requirement, string quotationItems, UserDetails customer, string folderPath, int flag, string biddingType)
        {
            GetTemplateDetails(reqID, requirement.TemplateId);
            List<KeyValuePair<string, DataTable>> requirementItems = new List<KeyValuePair<string, DataTable>>();

            #region Item Table
            //Define columns
            DataTable itemTable = new DataTable();

            var otherBrand = 0;

            foreach (RequirementItems Item in requirement.ListRequirementItems)
            {
                if (!string.IsNullOrEmpty(Item.OthersBrands))
                {
                    otherBrand++;
                }
            }

 
            itemTable.Columns.Add(GetLabel("PRODUCT_NAME"));
            itemTable.Columns.Add(GetLabel("PRODUCT_NUMBER"));
            //itemTable.Columns.Add("DESCRIPTION");
            itemTable.Columns.Add(GetLabel("PRODUCT_QUANTITY"));
            itemTable.Columns.Add(GetLabel("PRODUCT_ITEMDETAILS"));
            itemTable.Columns.Add(GetLabel("PRODUCT_DELIVERY_DETAILS"));
            if (otherBrand > 0)
            {
                //itemTable.Columns.Add("OTHER BRANDS");
                foreach (RequirementItems Item in requirement.ListRequirementItems)
                {
                    if (requirement.IsTabular && Item.IsDeleted <= 0)
                    {
                        string units = Item.IsCoreProductCategory > 0 ? Item.ProductQuantity + " " + Item.ProductQuantityIn : string.Empty;
                        //xml = String.Format(xml, Item.ProductIDorName, Item.ProductNo, Item.ProductDescription, Item.ProductQuantity, Item.ProductBrand, Item.OthersBrands);
                        itemTable.Rows.Add(Item.IsCoreProductCategory > 0 ? Item.ProductIDorName : "", Item.IsCoreProductCategory > 0 ? Item.ProductNo : "", units, Item.IsCoreProductCategory > 0 ? Item.ProductDescription : "", Item.IsCoreProductCategory > 0 ? Item.ProductDeliveryDetails : "");
                        //Item.ProductDescription
                        //Item.OthersBrands
                    }
                }
            }
            else
            {
                foreach (RequirementItems Item in requirement.ListRequirementItems)
                {
                    if (requirement.IsTabular && Item.IsDeleted <= 0)
                    {
                        string units = Item.IsCoreProductCategory > 0 ? Item.ProductQuantity + " " + Item.ProductQuantityIn : string.Empty;
                        //xml = String.Format(xml, Item.ProductIDorName, Item.ProductNo, Item.ProductDescription, Item.ProductQuantity, Item.ProductBrand, Item.OthersBrands);
                        itemTable.Rows.Add(Item.IsCoreProductCategory > 0 ? Item.ProductIDorName : "", Item.IsCoreProductCategory > 0 ? Item.ProductNo : "", units, Item.IsCoreProductCategory > 0 ? Item.ProductDescription : "", Item.IsCoreProductCategory > 0 ? Item.ProductDeliveryDetails : "");
                        //Item.ProductDescription
                    }
                }
            }



            requirementItems.Add(new KeyValuePair<string, DataTable>("ITEMS_TABLE", itemTable));

            #endregion Item Table

            #region Rfq Table

            DataTable rfqsTable = new DataTable();
            //Define columns
            rfqsTable.Columns.Add("Company Name");
            rfqsTable.Columns.Add(ExceptionHandler(customer.CompanyName.ToString()));


            rfqsTable.Rows.Add("RFQ # Reference Number", ExceptionHandler(requirement.RequirementNumber));
            rfqsTable.Rows.Add(GetLabel("REQ_TITLE"), ExceptionHandler(requirement.Title.ToString()));
            rfqsTable.Rows.Add("RFQ Issue Date", dateFormatChange(requirement.PostedOn));
            rfqsTable.Rows.Add("Quotation Freeze Time", dateFormatChange(requirement.QuotationFreezTime));
            if (!string.IsNullOrEmpty(biddingType) && biddingType.Equals("TENDER", StringComparison.InvariantCultureIgnoreCase))
            {
                rfqsTable.Rows.Add("", string.Empty);
            }
            else
            {
                rfqsTable.Rows.Add("Expected Negotiation Time", dateFormatChange(requirement.ExpStartTime));
            }

            rfqsTable.Rows.Add("Contact Person Name", ExceptionHandler(requirement.CustFirstName.ToString() + " (" + customer.Email.ToString() + ")"));
            rfqsTable.Rows.Add(GetLabel("CONTACT_DETAILS"), ExceptionHandler(requirement.ContactDetails.ToString()));

            //Populate with friends :)

            requirementItems.Add(new KeyValuePair<string, DataTable>("RFQS_TABLE", rfqsTable));

            #endregion Rfqs Table

            #region terms Table

            DataTable termsTable = new DataTable();

            //Define columns
            termsTable.Columns.Add(GetLabel("PAYMENT_TERMS"));

            if (requirement.DeliveryLocation == "Payment Terms")
            {
                termsTable.Columns.Add(ExceptionHandler(requirement.PaymentTerms) + ".");
            }
            else
            {
                termsTable.Columns.Add(ExceptionHandler(requirement.PaymentTerms));
            }

           // termsTable.Rows.Add(GetLabel("PAYMENT_TERMS"), ExceptionHandler(requirement.PaymentTerms.ToString()));
            termsTable.Rows.Add(GetLabel("DELIVERY_TERMS"), ExceptionHandler(requirement.DeliveryTime.ToString()));
            //termsTable.Rows.Add("Contact Details", ExceptionHandler(requirement.ContactDetails.ToString()));
            termsTable.Rows.Add(GetLabel("GENERAL_TERMS"), ExceptionHandler(requirement.GeneralTC.ToString()));
            

            requirementItems.Add(new KeyValuePair<string, DataTable>("TERMS_TABLE", termsTable));

            #endregion terms table

            #region Contract Table

            if (requirement.IsContract == true)
            {
                DataTable cntrctTable = new DataTable();

                //Define columns
                cntrctTable.Columns.Add("Start Time");
                cntrctTable.Columns.Add("End Time");

                cntrctTable.Rows.Add(ExceptionHandler(requirement.ContractStartTime.ToString()), ExceptionHandler(requirement.ContractEndTime.ToString()));

                requirementItems.Add(new KeyValuePair<string, DataTable>("CNTRCT_TABLE", cntrctTable));
            }

            #endregion Contract Table

            #region trans Table

            DataTable transTable = new DataTable();

            //Define columns
            transTable.Columns.Add("Product Urgency");
            transTable.Columns.Add(requirement.Urgency.ToString());


            transTable.Rows.Add("Currency", requirement.Currency.ToString());

            //Populate with friends :)

            requirementItems.Add(new KeyValuePair<string, DataTable>("TRANS_TABLE", transTable));

            #endregion trans Table

            #region vendor Table

            DataTable vendorsTable = new DataTable();

            //Define columns
            vendorsTable.Columns.Add("Name");
            vendorsTable.Columns.Add("Company");
            vendorsTable.Columns.Add("Email");
            vendorsTable.Columns.Add("Phone");

            foreach (VendorDetails vendor in requirement.AuctionVendors)
            {
                vendorsTable.Rows.Add(ExceptionHandler(vendor.VendorName),
                ExceptionHandler(vendor.CompanyName),
                ExceptionHandler(vendor.Vendor.Email),
                ExceptionHandler(vendor.Vendor.PhoneNum));

            }



            requirementItems.Add(new KeyValuePair<string, DataTable>("VENDORS_TABLE", vendorsTable));

            #endregion vendor Table


            //Populate with requirementItems :)
            return requirementItems;
        }

        public static byte[] ExportDataTableToPdf(List<KeyValuePair<string, DataTable>> dtblTable, String strPdfPath, int reqId, Requirement requirement, 
            int flag)
        {
            string watermark = "";
            using (MemoryStream ms = new MemoryStream())
            {

                System.IO.FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
                Document document = new Document();
                document.SetPageSize(iTextSharp.text.PageSize.A4);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                
                document.Open();

                PdfWriterEvents writerEvent = new PdfWriterEvents(ConfigurationManager.AppSettings["COMPANY_NAME"].ToString());
                writer.PageEvent = writerEvent;
                writerEvent.OnOpenDocument(writer, document);

                BaseFont bfntHead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont btnColumnHeader = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont sideHeaders = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                var FontColour = new BaseColor(128, 128, 128);
                var tabbleHeadColor = new BaseColor(17, 89, 128);
                var tabbleEvenColor = new BaseColor(242, 242, 242);
                var tableOddColor = new BaseColor(255, 255, 255);
                var tabbleFontBodColor = new BaseColor(117, 125, 138);
                var LineColour = new BaseColor(169, 169, 169);
                var BlackColour = new BaseColor(0, 0, 0);

                iTextSharp.text.Font myFont = new iTextSharp.text.Font(bfntHead, 10, iTextSharp.text.Font.NORMAL);

                iTextSharp.text.Font myComments = new iTextSharp.text.Font(btnColumnHeader, 12, iTextSharp.text.Font.NORMAL);

                Font fntQutBody = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, BlackColour);
                Font fntHead = new Font(bfntHead, 20, iTextSharp.text.Font.NORMAL, FontColour);
                Font sideHead = new Font(sideHeaders, 12, iTextSharp.text.Font.NORMAL, BlackColour);
                Font commentHead = new Font(sideHeaders, 10, iTextSharp.text.Font.NORMAL, BlackColour);
                Font fntColumnHeader = new Font(bfntHead, 10, iTextSharp.text.Font.NORMAL, tableOddColor);
                Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, LineColour, Element.ALIGN_LEFT, 1)));
                var spacerParagraph = new Paragraph();
                spacerParagraph.SpacingBefore = 4f;
                spacerParagraph.SpacingAfter = 0f;

                //ColumnText.ShowTextAligned(writer.DirectContent, Element.ALIGN_CENTER, new Phrase(watermark, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 50, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.LIGHT_GRAY)), 300, 400, 45);

                Paragraph prgHeading = new Paragraph();
                prgHeading.Alignment = Element.ALIGN_LEFT;
                prgHeading.Add(new Chunk("Request for Quotation (RFQ)", fntHead));
                document.Add(prgHeading);

                //Add a line seperation

                document.Add(p);
                document.Add(spacerParagraph);
                document.Add(spacerParagraph);

                //PdfWriterEvents writerEvent = new PdfWriterEvents(watermark);
                //writer.PageEvent = writerEvent;

                KeyValuePair<string, DataTable> rfqs_data = dtblTable.Where(item => item.Key == "RFQS_TABLE").FirstOrDefault();

                //Write the table
                //Table RFQ
                PdfPTable tableRFQ = new PdfPTable(rfqs_data.Value.Columns.Count);
                tableRFQ.DefaultCell.Padding = 3;
                tableRFQ.WidthPercentage = 100;
                tableRFQ.DefaultCell.Border = 0;
                tableRFQ.HorizontalAlignment = Element.ALIGN_CENTER;
                tableRFQ.DefaultCell.BorderColor = tableOddColor;

                for (int i = 0; i < rfqs_data.Value.Columns.Count; i++)
                {
                    PdfPCell cell = new PdfPCell();
                    //cell.BackgroundColor = tabbleEvenColor;
                    cell.PaddingBottom = 8;
                    cell.Border = 0;
                    cell.VerticalAlignment = Element.ALIGN_CENTER;
                    cell.AddElement(new Chunk(rfqs_data.Value.Columns[i].ColumnName.ToString(), myFont));
                    tableRFQ.AddCell(cell);

                }

                var oddEvenOne = 1;
                for (int i = 0; i < rfqs_data.Value.Rows.Count; i++)
                {
                    if (oddEvenOne % 2 == 0)
                    {
                        for (int j = 0; j < rfqs_data.Value.Columns.Count; j++)
                        {
                            PdfPCell tblcell = new PdfPCell();
                            //tblcell.BackgroundColor = tabbleEvenColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.Border = 0;
                            tblcell.AddElement(new Chunk(rfqs_data.Value.Rows[i][j].ToString(), myFont));
                            tableRFQ.AddCell(tblcell);
                        }
                    }
                    else
                    {
                        for (int j = 0; j < rfqs_data.Value.Columns.Count; j++)
                        {
                            PdfPCell tblcell = new PdfPCell();
                            //tblcell.BackgroundColor = tableOddColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.Border = 0;
                            tblcell.AddElement(new Chunk(rfqs_data.Value.Rows[i][j].ToString(), myFont));
                            tableRFQ.AddCell(tblcell);
                        }
                    }

                    oddEvenOne++;
                }

                document.Add(tableRFQ);

                document.Add(p);

                document.Add(new Chunk("\n"));

                Paragraph itemsHeading = new Paragraph();
                itemsHeading.Alignment = Element.ALIGN_LEFT;
                itemsHeading.Add(new Chunk("Product Details : ", sideHead));
                document.Add(itemsHeading);

                document.Add(spacerParagraph);

                KeyValuePair<string, DataTable> items_data = dtblTable.Where(item => item.Key == "ITEMS_TABLE").FirstOrDefault();

                //Table main

                PdfPTable table = new PdfPTable(items_data.Value.Columns.Count);
                float[] widths = new float[] { 40, 15, 15, 15, 15 };
                if (items_data.Value.Columns.Count == 5)
                {
                    widths = new float[] { 30, 20, 10, 20, 20 };
                }
                if (items_data.Value.Columns.Count == 6)
                {
                    widths = new float[] { 30, 30, 20, 10, 20, 20 };
                }
                table.SetWidths(widths);
                table.DefaultCell.Padding = 8;
                table.WidthPercentage = 100;
                table.HorizontalAlignment = Element.ALIGN_CENTER;
                table.DefaultCell.BorderColor = tabbleFontBodColor;

                for (int i = 0; i < items_data.Value.Columns.Count; i++)
                {
                    PdfPCell cell = new PdfPCell();
                    cell.BackgroundColor = tabbleHeadColor;
                    cell.PaddingBottom = 8;
                    cell.VerticalAlignment = Element.ALIGN_CENTER;
                    cell.AddElement(new Chunk(items_data.Value.Columns[i].ColumnName.ToUpper(), fntColumnHeader));
                    table.AddCell(cell);

                }
                //table Data
                var oddEvenTwo = 1;
                for (int i = 0; i < items_data.Value.Rows.Count; i++)
                {
                    if (oddEvenTwo % 2 == 0)
                    {
                        for (int j = 0; j < items_data.Value.Columns.Count; j++)
                        {
                            PdfPCell tblcell = new PdfPCell();
                            //tblcell.BackgroundColor = tabbleEvenColor;
                            tblcell.PaddingTop = 0;
                            tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), myFont));
                            table.AddCell(tblcell);
                        }
                    }
                    else
                    {
                        for (int j = 0; j < items_data.Value.Columns.Count; j++)
                        {
                            PdfPCell tblcell = new PdfPCell();
                        //    tblcell.BackgroundColor = tableOddColor;
                            tblcell.PaddingTop = 0;
                            tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), myFont));
                            table.AddCell(tblcell);
                        }
                    }

                    oddEvenTwo++;
                }

                document.Add(table);
                document.Add(new Chunk("\n"));

                Chunk glue = new Chunk(new VerticalPositionMark());
                if (requirement.ReqComments.ToString() != String.Empty)
                {
                    Paragraph mainComments = new Paragraph();
                    mainComments.Alignment = Element.ALIGN_LEFT;
                    mainComments.Add(new Chunk("Comments & Link: " + requirement.ReqComments.ToString(), commentHead));
                    document.Add(mainComments);

                    document.Add(new Chunk("\n"));
                }

                Paragraph termsHeading = new Paragraph();
                termsHeading.Alignment = Element.ALIGN_LEFT;
                termsHeading.Add(new Chunk("Delivery & Payment Terms : ", sideHead));
                document.Add(termsHeading);

                document.Add(spacerParagraph);

                KeyValuePair<string, DataTable> terms_data = dtblTable.Where(item => item.Key == "TERMS_TABLE").FirstOrDefault();

                //Table Terms & Condition

                PdfPTable tableTerms = new PdfPTable(terms_data.Value.Columns.Count);
                float[] termsWidths = new float[] { 30, 70 };
                tableTerms.SetWidths(termsWidths);
                tableTerms.DefaultCell.Padding = 3;
                tableTerms.WidthPercentage = 100;
                tableTerms.DefaultCell.Border = 0;
                tableTerms.HorizontalAlignment = Element.ALIGN_CENTER;
                tableTerms.DefaultCell.BorderColor = tableOddColor;

                for (int i = 0; i < terms_data.Value.Columns.Count; i++)
                {
                    PdfPCell cell = new PdfPCell();
                    //cell.BackgroundColor = tabbleEvenColor;
                    cell.PaddingBottom = 8;
                    cell.VerticalAlignment = Element.ALIGN_CENTER;
                    cell.AddElement(new Chunk(terms_data.Value.Columns[i].ColumnName.ToString(), myFont));
                    tableTerms.AddCell(cell);

                }

                var oddEvenThree = 1;
                for (int i = 0; i < terms_data.Value.Rows.Count; i++)
                {
                    if (oddEvenThree % 2 == 0)
                    {
                        for (int j = 0; j < terms_data.Value.Columns.Count; j++)
                        {
                            PdfPCell tblcell = new PdfPCell();
                            //tblcell.BackgroundColor = tabbleEvenColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.AddElement(new Chunk(terms_data.Value.Rows[i][j].ToString(), myFont));
                            tableTerms.AddCell(tblcell);
                        }
                    }
                    else
                    {
                        for (int j = 0; j < terms_data.Value.Columns.Count; j++)
                        {
                            PdfPCell tblcell = new PdfPCell();
                            //tblcell.BackgroundColor = tableOddColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.AddElement(new Chunk(terms_data.Value.Rows[i][j].ToString(), myFont));
                            tableTerms.AddCell(tblcell);
                        }
                    }

                    oddEvenThree++;
                }

                document.Add(tableTerms);

                document.Add(new Chunk("\n"));


                //Contract Details start

                if (requirement.IsContract == true)
                {

                    Paragraph cntrctHeading = new Paragraph();
                    cntrctHeading.Alignment = Element.ALIGN_LEFT;
                    cntrctHeading.Add(new Chunk("Contract Details : ", sideHead));
                    document.Add(cntrctHeading);

                    document.Add(spacerParagraph);

                    KeyValuePair<string, DataTable> cntrct_data = dtblTable.Where(item => item.Key == "CNTRCT_TABLE").FirstOrDefault();

                    //Table Terms & Condition

                    PdfPTable tableCntrct = new PdfPTable(cntrct_data.Value.Columns.Count);
                    float[] cntrctWidths = new float[] { 50, 50 };
                    tableCntrct.SetWidths(cntrctWidths);
                    tableCntrct.DefaultCell.Padding = 3;
                    tableCntrct.WidthPercentage = 100;
                    tableCntrct.DefaultCell.Border = 0;
                    tableCntrct.HorizontalAlignment = Element.ALIGN_CENTER;
                    tableCntrct.DefaultCell.BorderColor = tableOddColor;

                    for (int i = 0; i < cntrct_data.Value.Columns.Count; i++)
                    {
                        PdfPCell cell = new PdfPCell();
                        cell.BackgroundColor = tabbleHeadColor;
                        cell.PaddingBottom = 8;
                        cell.VerticalAlignment = Element.ALIGN_CENTER;
                        cell.AddElement(new Chunk(cntrct_data.Value.Columns[i].ColumnName.ToString(), fntColumnHeader));
                        tableCntrct.AddCell(cell);

                    }

                    for (int i = 0; i < cntrct_data.Value.Rows.Count; i++)
                    {
                        for (int j = 0; j < cntrct_data.Value.Columns.Count; j++)
                        {
                            PdfPCell tblcell = new PdfPCell();
                            //tblcell.BackgroundColor = tabbleEvenColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.AddElement(new Chunk(cntrct_data.Value.Rows[i][j].ToString(), myFont));
                            tableCntrct.AddCell(tblcell);
                        }
                    }

                    document.Add(tableCntrct);

                    document.Add(new Chunk("\n"));
                }

                //Contract details end


                Paragraph transHeading = new Paragraph();
                transHeading.Alignment = Element.ALIGN_LEFT;
                transHeading.Add(new Chunk("Transaction Configuration : ", sideHead));
                document.Add(transHeading);

                document.Add(spacerParagraph);

                KeyValuePair<string, DataTable> trans_data = dtblTable.Where(item => item.Key == "TRANS_TABLE").FirstOrDefault();

                PdfPTable tableTrans = new PdfPTable(trans_data.Value.Columns.Count);
                tableTrans.SetWidths(termsWidths);
                tableTrans.DefaultCell.Padding = 3;
                tableTrans.WidthPercentage = 100;
                tableTrans.HorizontalAlignment = Element.ALIGN_CENTER;
                tableTrans.DefaultCell.BorderColor = tableOddColor;

                for (int i = 0; i < trans_data.Value.Columns.Count; i++)
                {
                    PdfPCell cell = new PdfPCell();
                    //cell.BackgroundColor = tabbleEvenColor;
                    cell.PaddingBottom = 8;
                    cell.VerticalAlignment = Element.ALIGN_CENTER;
                    cell.AddElement(new Chunk(trans_data.Value.Columns[i].ColumnName.ToString(), myFont));
                    tableTrans.AddCell(cell);

                }

                var oddEvenFour = 1;
                for (int i = 0; i < trans_data.Value.Rows.Count; i++)
                {
                    if (oddEvenFour % 2 == 0)
                    {
                        for (int j = 0; j < trans_data.Value.Columns.Count; j++)
                        {
                            PdfPCell tblcell = new PdfPCell();
                            //tblcell.BackgroundColor = tabbleEvenColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.AddElement(new Chunk(trans_data.Value.Rows[i][j].ToString(), myFont));
                            tableTrans.AddCell(tblcell);
                        }
                    }
                    else
                    {
                        for (int j = 0; j < trans_data.Value.Columns.Count; j++)
                        {
                            PdfPCell tblcell = new PdfPCell();
                            //tblcell.BackgroundColor = tableOddColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.AddElement(new Chunk(trans_data.Value.Rows[i][j].ToString(), myFont));
                            tableTrans.AddCell(tblcell);
                        }
                    }

                    oddEvenFour++;
                }

                document.Add(tableTrans);

                KeyValuePair<string, DataTable> vendors_data = dtblTable.Where(item => item.Key == "VENDORS_TABLE").FirstOrDefault();

                if (flag == 1)
                {

                    document.Add(new Chunk("\n"));

                    Paragraph vendorHeading = new Paragraph();
                    vendorHeading.Alignment = Element.ALIGN_LEFT;
                    vendorHeading.Add(new Chunk("Vendors Details : ", sideHead));
                    document.Add(vendorHeading);

                    document.Add(spacerParagraph);

                    PdfPTable tableVendors = new PdfPTable(vendors_data.Value.Columns.Count);

                    tableVendors.DefaultCell.Padding = 3;
                    tableVendors.WidthPercentage = 100;
                    tableVendors.HorizontalAlignment = Element.ALIGN_CENTER;
                    tableVendors.DefaultCell.BorderColor = tableOddColor;

                    for (int i = 0; i < vendors_data.Value.Columns.Count; i++)
                    {
                        PdfPCell cell = new PdfPCell();
                        cell.BackgroundColor = tabbleHeadColor;
                        cell.PaddingBottom = 8;
                        cell.VerticalAlignment = Element.ALIGN_CENTER;
                        cell.AddElement(new Chunk(vendors_data.Value.Columns[i].ColumnName.ToString(), fntColumnHeader));
                        tableVendors.AddCell(cell);

                    }

                    var oddEvenFive = 1;
                    for (int i = 0; i < vendors_data.Value.Rows.Count; i++)
                    {
                        if (oddEvenFive % 2 == 0)
                        {
                            for (int j = 0; j < vendors_data.Value.Columns.Count; j++)
                            {
                                PdfPCell tblcell = new PdfPCell();
                                //tblcell.BackgroundColor = tabbleEvenColor;
                                tblcell.PaddingTop = 0;
                                tblcell.PaddingBottom = 8;
                                tblcell.AddElement(new Chunk(vendors_data.Value.Rows[i][j].ToString(), myFont));
                                tableVendors.AddCell(tblcell);
                            }
                        }
                        else
                        {
                            for (int j = 0; j < vendors_data.Value.Columns.Count; j++)
                            {
                                PdfPCell tblcell = new PdfPCell();
                                //tblcell.BackgroundColor = tableOddColor;
                                tblcell.PaddingTop = 0;
                                tblcell.PaddingBottom = 8;
                                tblcell.AddElement(new Chunk(vendors_data.Value.Rows[i][j].ToString(), myFont));
                                tableVendors.AddCell(tblcell);
                            }
                        }

                        oddEvenFive++;
                    }

                    document.Add(tableVendors);
                }

                document.Add(spacerParagraph);
                document.Add(spacerParagraph);

                Paragraph prgNote = new Paragraph();
                prgNote.Alignment = Element.ALIGN_LEFT;
                prgNote.Add(new Chunk("NOTE : This Document is Auto Generated from PRM360 System. ", fntQutBody));
                document.Add(prgNote);
                // var a = document.PageNumber;


                PdfWriterEvents writerEvent1 = new PdfWriterEvents(ConfigurationManager.AppSettings["COMPANY_NAME"].ToString());
                writer.PageEvent = writerEvent1;
                writerEvent.OnStartPage(writer, document);
                writerEvent.OnCloseDocument(writer, document);

                document.Close();
                writer.Close();
                fs.Close();

                return ms.ToArray();
            }
        }

        public static List<KeyValuePair<string, DataTable>> MakeDataTableQuotation(List<RequirementItems> quotationObject, int reqID, int customerID, int vendorID, string sessionID,
            string warranty, string payment, string duration, string validity, double price, double tax, double freightcharges,
            double vendorBidPrice, bool isTabular, int revised, string quotationTaxes, double discountAmount, string otherProperties,
            string deliveryTermsDays, string deliveryTermsPercent, string paymentTermsDays, string paymentTermsPercent,
            string paymentTermsType, string folderPath, string gstNumber, int isRegret,
            string selectedVendorCurrency, double packingCharges, double packingChargesTaxPercentage, double packingChargesWithTax,
            double revpackingCharges, double revpackingChargesWithTax, double installationCharges, double installationChargesTaxPercentage,
            double installationChargesWithTax, double revinstallationCharges, double revinstallationChargesWithTax, double freightCharges,
            double freightChargesTaxPercentage, double freightChargesWithTax, double revfreightCharges, double revfreightChargesWithTax,
            string INCO_TERMS, int TaxFiledValidation, string deliverLocation = "", Requirement requirementDetails = null, string localGst = "36")
        {            
            bool hasTaxes = false;
            bool isIGSTField = !string.IsNullOrEmpty(gstNumber) && gstNumber.StartsWith(localGst) ? false : true;
            if (requirementDetails != null && requirementDetails.NegotiationSettings != null && requirementDetails.NegotiationSettings.DisableLocalGSTFeature)
            {
                if (requirementDetails.AuctionVendors != null && requirementDetails.AuctionVendors.Count > 0 && requirementDetails.AuctionVendors[0].ListRequirementItems != null
                    && requirementDetails.AuctionVendors[0].ListRequirementItems.Count > 0
                    && requirementDetails.AuctionVendors[0].ListRequirementItems.Any(i => i.IGst > 0))
                {
                    isIGSTField = true;
                }
                else
                {
                    isIGSTField = false;
                }
            }

            GetTemplateDetails(reqID);
            List<KeyValuePair<string, DataTable>> requirementItems = new List<KeyValuePair<string, DataTable>>();

            PRMServices prmservices = new PRMServices();

            Requirement requirement = requirementDetails !=  null ? requirementDetails : prmservices.GetRequirementDataOfflinePrivate(reqID, customerID, sessionID);
            #region Item Table
            //Define columns

            List<string> columns = new List<string>() { GetLabel("PRODUCT_NAME"), GetLabel("PRODUCT_NUMBER"), GetLabel("PRODUCT_HSNCODE"), GetLabel("PRODUCT_QUANTITY"), " COMMENTS " };
            DataTable itemTable = new DataTable();           
            if (isRegret > 0)
            {
                columns.Add(" REGRET COMMENTS ");
            }

            columns.Add(" UNIT PRICE ");
            if (!string.IsNullOrEmpty(selectedVendorCurrency) && selectedVendorCurrency.Equals("INR"))
            {
                hasTaxes = true;
                if (!string.IsNullOrEmpty(gstNumber))
                {
                    if (TaxFiledValidation == 1 && !isIGSTField)
                    {
                        columns.Add(" CGST (%)");
                        columns.Add(" SGST (%)");
                    }
                    else
                    {
                        columns.Add(" IGST (%)");
                    }
                }
                else
                {
                    if (TaxFiledValidation == 1)
                    {
                        columns.Add(" CGST (%)");
                        columns.Add(" SGST (%)");
                    }
                    else
                    {
                        columns.Add(" IGST (%)");
                    }
                }
            }

            columns.Add(" ITEM PRICE ");

            foreach(var column in columns)
            {
                itemTable.Columns.Add(column);
            }

            bool isUOMDiffertent = false;
            foreach (RequirementItems quotation in quotationObject)
            {
                var priceRow = itemTable.NewRow();
                double itemPrice = 0;
                double unitPrice = 0;

                if (revised == 0)
                {
                    itemPrice = quotation.ItemPrice;
                    unitPrice = quotation.UnitPrice;
                }
                else if (revised == 1)
                {
                    itemPrice = quotation.RevItemPrice;
                    unitPrice = quotation.RevUnitPrice;
                }


                double itemUnitDiscount = 0;
                double itemCostPrice = 0;

                if (revised == 1)
                {
                    quotation.RevItemPrice = quotation.RevisedItemPrice;
                    itemUnitDiscount = quotation.RevUnitDiscount;

                }
                else if (revised == 0)
                {
                    itemUnitDiscount = quotation.UnitDiscount;
                }

                itemCostPrice = (quotation.UnitMRP * 100 * 100) / ((itemUnitDiscount * 100) + 10000 + (itemUnitDiscount * (quotation.CGst + quotation.SGst + quotation.IGst)) + ((quotation.CGst + quotation.SGst + quotation.IGst) * 100));

                double Gst = (quotation.CGst + quotation.SGst + quotation.IGst);

                if (quotation.IsRegret == true)
                {
                    itemPrice = 0;

                    unitPrice = 0;
                    quotation.CGst = 0;
                    quotation.SGst = 0;
                    quotation.IGst = 0;
                    Gst = 0;
                    quotation.UnitMRP = 0;
                    itemUnitDiscount = 0;
                }



                if (quotation.ProductQuantityIn != quotation.VendorUnits)
                {
                    itemPrice = 0;
                    isUOMDiffertent = true;
                }

                //quotationObject


                string itemComments = string.Empty;

                if (revised == 1)
                {
                    itemComments = quotation.ItemLevelRevComments;
                }
                else if (revised == 0)
                {
                    itemComments = quotation.ItemLevelInitialComments;
                }

                double freight = 0;
                if (revised == 1)
                {
                    freight = 0;// quotation.ItemRevFreightCharges + ((quotation.ItemRevFreightCharges / 100) * quotation.ItemFreightTAX);
                }
                else
                {
                    freight = 0;// quotation.ItemFreightCharges + ((quotation.ItemFreightCharges / 100) * quotation.ItemFreightTAX);
                }

                if (isUOMDiffertent)
                {
                    vendorBidPrice = 0;
                    price = 0;
                }

                if (revised == 1)
                {
                    packingCharges = revpackingCharges;
                    packingChargesWithTax = revpackingChargesWithTax;
                    installationCharges = revinstallationCharges;
                    installationChargesWithTax = revinstallationChargesWithTax;
                    freightCharges = revfreightCharges;
                    freightChargesWithTax = revfreightChargesWithTax;

                }

                List<object> priceRowData = new List<object>();
                if (isRegret == 0)
                {
                    string units = quotation.IsCoreProductCategory > 0 ? quotation.ProductQuantity + " " + quotation.ProductQuantityIn : string.Empty;
                    priceRowData.Add(quotation.ProductIDorName);
                    priceRowData.Add(quotation.IsCoreProductCategory > 0 ? quotation.ProductNo : "");
                    priceRowData.Add(quotation.IsCoreProductCategory > 0 ? quotation.HsnCode : "");
                    priceRowData.Add(units);
                    priceRowData.Add(itemComments);
                    priceRowData.Add(Utilities.RoundValue(unitPrice));

                    if (hasTaxes)
                    {
                        if (!string.IsNullOrEmpty(gstNumber))
                        {
                            if (TaxFiledValidation == 1 && !isIGSTField)
                            {
                                priceRowData.Add(Utilities.RoundValue(quotation.CGst));
                                priceRowData.Add(Utilities.RoundValue(quotation.SGst));
                                priceRowData.Add(Utilities.RoundValue(itemPrice));
                            }
                            else
                            {
                                priceRowData.Add(Utilities.RoundValue(quotation.IGst));
                                priceRowData.Add(Utilities.RoundValue(itemPrice));
                            }
                        }
                        else
                        {
                            if (TaxFiledValidation == 1)
                            {
                                priceRowData.Add(Utilities.RoundValue(quotation.CGst));
                                priceRowData.Add(Utilities.RoundValue(quotation.SGst));
                                priceRowData.Add(Utilities.RoundValue(itemPrice));
                            }
                            else
                            {
                                priceRowData.Add(Utilities.RoundValue(quotation.IGst));
                                priceRowData.Add(Utilities.RoundValue(itemPrice));
                            }
                        }   
                    }
                    else
                    {
                        priceRowData.Add(Utilities.RoundValue(itemPrice));
                    }
                }
                else if (isRegret > 0)
                {
                    string units = quotation.IsCoreProductCategory > 0 ? quotation.ProductQuantity + " " + quotation.ProductQuantityIn : string.Empty;
                    priceRowData.Add(quotation.ProductIDorName);
                    priceRowData.Add(quotation.IsCoreProductCategory > 0 ? quotation.ProductNo: string.Empty);
                    priceRowData.Add(quotation.IsCoreProductCategory > 0 ? quotation.HsnCode :  string.Empty);
                    priceRowData.Add(units);
                    priceRowData.Add(itemComments);
                    priceRowData.Add(quotation.RegretComments);
                    priceRowData.Add(Utilities.RoundValue(unitPrice));

                    if (hasTaxes)
                    {
                        if (!string.IsNullOrEmpty(gstNumber))
                        {
                            if (TaxFiledValidation == 1 && !isIGSTField)
                            {
                                priceRowData.Add(Utilities.RoundValue(quotation.CGst));
                                priceRowData.Add(Utilities.RoundValue(quotation.SGst));
                                priceRowData.Add(Utilities.RoundValue(itemPrice));
                            }
                            else
                            {
                                priceRowData.Add(Utilities.RoundValue(quotation.IGst));
                                priceRowData.Add(Utilities.RoundValue(itemPrice));
                            }
                        }
                        else
                        {
                            if (TaxFiledValidation == 1)
                            {
                                priceRowData.Add(Utilities.RoundValue(quotation.CGst));
                                priceRowData.Add(Utilities.RoundValue(quotation.SGst));
                                priceRowData.Add(Utilities.RoundValue(itemPrice));
                            }
                            else
                            {
                                priceRowData.Add(Utilities.RoundValue(quotation.IGst));
                                priceRowData.Add(Utilities.RoundValue(itemPrice));
                            }
                        }
                    }
                    else
                    {
                        priceRowData.Add(Utilities.RoundValue(itemPrice));
                    }
                }

                int count = 0;
                foreach (var data in priceRowData)
                {
                    priceRow[count] = data;
                    count++;
                }

                itemTable.Rows.Add(priceRow);
            }

            DataRow totalPriceRow = itemTable.NewRow();
            for (int i = 0; i < itemTable.Columns.Count - 2; i++)
            {
                totalPriceRow[i] = "/*//*/";
            }

            totalPriceRow[itemTable.Columns.Count - 2] = "Total Item Price";
            totalPriceRow[itemTable.Columns.Count - 1] = Utilities.RoundValue(price);
            itemTable.Rows.Add(totalPriceRow);

            DataRow grandTotalRow = itemTable.NewRow();
            for (int i = 0; i < itemTable.Columns.Count - 2; i++)
            {
                grandTotalRow[i] = "/*//*/";
            }

            grandTotalRow[itemTable.Columns.Count - 2] = "Grand Total";
            grandTotalRow[itemTable.Columns.Count - 1] = Utilities.RoundValue(vendorBidPrice);
            itemTable.Rows.Add(grandTotalRow);

            requirementItems.Add(new KeyValuePair<string, DataTable>("ITEMS_TABLE", itemTable));

            #endregion Item Table

            #region terms & condition

            DataTable termsTable = new DataTable();

            //Define columns
            termsTable.Columns.Add(" Warranty ");
            if (string.IsNullOrEmpty(warranty))
            {
                termsTable.Columns.Add(" N ");
            }
            else
            {
                if (warranty == "Warranty")
                {
                    termsTable.Columns.Add(ExceptionHandler(warranty) + ".");
                }
                else
                {
                    termsTable.Columns.Add(ExceptionHandler(warranty));
                }
            }

            if (!string.IsNullOrEmpty(otherProperties))
            {
                //termsTable.Rows.Add(GetLabel("REQ_COMMENTS"), ExceptionHandler(otherProperties.ToString()));
                termsTable.Rows.Add("Comments", ExceptionHandler(otherProperties.ToString()));
            }
            if (!string.IsNullOrEmpty(payment))
            {
                termsTable.Rows.Add(GetLabel("PAYMENT_TERMS"), ExceptionHandler(payment.ToString()));
            }
            if (!string.IsNullOrEmpty(payment))
            {
                termsTable.Rows.Add("Validity", ExceptionHandler(validity.ToString()));
            }
            if (!string.IsNullOrEmpty(duration))
            {
                termsTable.Rows.Add(GetLabel("DELIVERY_TERMS"), ExceptionHandler(duration.ToString()));
            }
            if (!string.IsNullOrEmpty(deliverLocation))
            {
                termsTable.Rows.Add(GetLabel("DELIVERY_LOCATION"), deliverLocation.ToString());
            }
            if (!string.IsNullOrEmpty(INCO_TERMS))
            {
                termsTable.Rows.Add(GetLabel("GENERAL_TERMS"), ExceptionHandler(INCO_TERMS.ToString()));
            }

            requirementItems.Add(new KeyValuePair<string, DataTable>("TERMS_TABLE", termsTable));

            #endregion terms & condition


            return requirementItems;
        }

        public static List<KeyValuePair<string, DataTable>> MakeDataTableDiscountQuotation(List<RequirementItems> quotationObject, int reqID, int customerID, int vendorID, string sessionID,
                    string warranty, string payment, string duration, string validity, double price, double tax, double freightcharges,
                    double vendorBidPrice, bool isTabular, int revised, string quotationTaxes, double discountAmount, string otherProperties,
                    string deliveryTermsDays, string deliveryTermsPercent, string paymentTermsDays, string paymentTermsPercent,
                    string paymentTermsType, string folderPath, string gstNumber, int isRegret, string selectedVendorCurrency, double packingCharges, double packingChargesTaxPercentage, 
                    double packingChargesWithTax,
                    double revpackingCharges, double revpackingChargesWithTax, double installationCharges, double installationChargesTaxPercentage,
                    double installationChargesWithTax, double revinstallationCharges, double revinstallationChargesWithTax, double freightCharges,
                    double freightChargesTaxPercentage, double freightChargesWithTax, double revfreightCharges, double revfreightChargesWithTax,
                    string INCO_TERMS, int TaxFiledValidation)
        {
            List<KeyValuePair<string, DataTable>> requirementItems = new List<KeyValuePair<string, DataTable>>();
            PRMServices prmservices = new PRMServices();
            Requirement requirement = prmservices.GetRequirementDataOfflinePrivate(reqID, customerID, sessionID);
            UserDetails customer = prmservices.GetUserDetails(customerID, sessionID);
            UserDetails vendor = prmservices.GetUserDetails(vendorID, sessionID);

            #region Item Table
            //Define columns
            DataTable itemTable = new DataTable();

            itemTable.Columns.Add(" NAME ");
            itemTable.Columns.Add(" PRODUCT NO. ");
            itemTable.Columns.Add(" DESCRIPTION ");
            itemTable.Columns.Add(" MAKE ");
            itemTable.Columns.Add(" QTY ");
            itemTable.Columns.Add(" UNIT PRICE ");
            itemTable.Columns.Add(" UNIT DISC (%)");
            itemTable.Columns.Add(" GST (%)");
            itemTable.Columns.Add(" REDUCTION PRICE ");
            itemTable.Columns.Add(" COMMENTS ");
            if (isRegret > 0)
            {
                itemTable.Columns.Add(" REGRET COMMENTS ");
            }
            itemTable.Columns.Add(" ITEM PRICE ");

            bool isUOMDiffertent = false;

            foreach (RequirementItems quotation in quotationObject)
            {
                double itemPrice = 0;
                double unitPrice = 0;

                if (revised == 0)
                {
                    itemPrice = quotation.ItemPrice;
                    unitPrice = quotation.UnitPrice;
                }
                else if (revised == 1)
                {
                    itemPrice = quotation.RevItemPrice;
                    unitPrice = quotation.RevUnitPrice;
                }



                double itemUnitDiscount = 0;
                double itemCostPrice = 0;

                if (revised == 1)
                {
                    quotation.RevItemPrice = quotation.RevisedItemPrice;
                    itemUnitDiscount = quotation.RevUnitDiscount;

                }
                else if (revised == 0)
                {
                    itemUnitDiscount = quotation.UnitDiscount;
                }

                itemCostPrice = (quotation.UnitMRP * 100 * 100) / ((itemUnitDiscount * 100) + 10000 + (itemUnitDiscount * (quotation.CGst + quotation.SGst + quotation.IGst)) + ((quotation.CGst + quotation.SGst + quotation.IGst) * 100));

                double reductionPrice = 0;
                if (revised == 1)
                {
                    reductionPrice = (quotation.UnitMRP - quotation.RevUnitPrice) * quotation.ProductQuantity;
                }
                else
                {
                    reductionPrice = (quotation.UnitMRP - quotation.UnitPrice) * quotation.ProductQuantity;
                }

                var gst = quotation.CGst + quotation.SGst + quotation.IGst;

                string xml = string.Empty;

                string itemComments = string.Empty;

                if (revised == 1)
                {
                    itemComments = quotation.ItemLevelRevComments;
                }
                else if (revised == 0)
                {
                    itemComments = quotation.ItemLevelInitialComments;
                }

                if (isRegret > 0)
                {
                    string units = quotation.IsCoreProductCategory > 0 ? quotation.ProductQuantity + " " + quotation.ProductQuantityIn : string.Empty;
                    itemTable.Rows.Add(quotation.ProductIDorName, quotation.IsCoreProductCategory > 0 ? quotation.ProductNo : "", quotation.IsCoreProductCategory > 0 ? quotation.ProductDescription : "",
                                            quotation.IsCoreProductCategory > 0 ? quotation.ProductBrand : "", units, quotation.UnitMRP,
                                            itemUnitDiscount, gst, Math.Round(Convert.ToDouble(reductionPrice), decimal_round), itemComments, quotation.RegretComments, itemPrice);
                }
                else
                {
                    string units = quotation.IsCoreProductCategory > 0 ? quotation.ProductQuantity + " " + quotation.ProductQuantityIn : string.Empty;
                    itemTable.Rows.Add(quotation.ProductIDorName, quotation.IsCoreProductCategory > 0 ? quotation.ProductNo : "", quotation.IsCoreProductCategory > 0 ? quotation.ProductDescription : "",
                                            quotation.IsCoreProductCategory > 0 ? quotation.ProductBrand : "", units, quotation.UnitMRP,
                                            itemUnitDiscount, gst, Math.Round(Convert.ToDouble(reductionPrice), decimal_round), itemComments, itemPrice);

                }

            }

            if (isRegret == 0)
            {
                itemTable.Rows.Add("/*//*/",
                    "/*//*/",
                    "/*//*/",
                    "/*//*/",
                    "/*//*/",
                    "/*//*/",
                    "/*//*/",
                    "/*//*/",
                    "/*//*/",
                    "Total Item Price",
                    price);
                if (packingCharges != 0)
                {
                    itemTable.Rows.Add("/*//*/",
                        "/*//*/",
                        "/*//*/", 
                        "/*//*/", 
                        "/*//*/",
                        "/*//*/",
                        "/*//*/",
                        "Packing & Forward Charges", 
                        packingCharges, 
                        packingChargesTaxPercentage, 
                        packingChargesWithTax);
                }
                if (installationCharges != 0)
                {
                    itemTable.Rows.Add("/*//*/", 
                        "/*//*/",
                        "/*//*/", 
                        "/*//*/", 
                        "/*//*/",
                        "/*//*/",
                        "/*//*/",
                        "Installation Charges", 
                        installationCharges, 
                        installationChargesTaxPercentage, 
                        installationChargesWithTax);
                }
                if (freightCharges != 0)
                {
                    itemTable.Rows.Add("/*//*/", 
                        "/*//*/",
                        "/*//*/", 
                        "/*//*/", 
                        "/*//*/",
                        "/*//*/",
                        "/*//*/",
                        "Freight Charges", 
                        freightCharges, 
                        freightChargesTaxPercentage, 
                        freightChargesWithTax);
                }
                itemTable.Rows.Add("/*//*/", 
                    "/*//*/",
                    "/*//*/", 
                    "/*//*/", 
                    "/*//*/", 
                    "/*//*/", 
                    "/*//*/",
                    "/*//*/",
                    "/*//*/",
                    //"Grand Total in " + selectedVendorCurrency,
                    "Grand Total",
                    vendorBidPrice);
            }
            else if (isRegret > 0) {
                itemTable.Rows.Add("/*//*/", 
                    "/*//*/",
                    "/*//*/", 
                    "/*//*/", 
                    "/*//*/", 
                    "/*//*/", 
                    "/*//*/", 
                    "/*//*/",
                    "/*//*/",
                    "/*//*/",
                    "Total Item Price", 
                    price);
                if (packingCharges != 0)
                {
                    itemTable.Rows.Add("/*//*/", 
                        "/*//*/",
                        "/*//*/", 
                        "/*//*/", 
                        "/*//*/", 
                        "/*//*/",
                        "/*//*/",
                        "/*//*/",
                        "Packing & Forward Charges", 
                        packingCharges, 
                        packingChargesTaxPercentage, 
                        packingChargesWithTax);
                }
                if (installationCharges != 0)
                {
                    itemTable.Rows.Add("/*//*/", 
                        "/*//*/",
                        "/*//*/", 
                        "/*//*/", 
                        "/*//*/", 
                        "/*//*/",
                        "/*//*/",
                        "/*//*/",
                        "Installation Charges", 
                        installationCharges, 
                        installationChargesTaxPercentage, 
                        installationChargesWithTax);
                }
                if (freightCharges != 0)
                {
                    itemTable.Rows.Add("/*//*/", 
                        "/*//*/",
                        "/*//*/", 
                        "/*//*/", 
                        "/*//*/", 
                        "/*//*/",
                        "/*//*/",
                        "/*//*/",
                        "Freight Charges", 
                        freightCharges, 
                        freightChargesTaxPercentage, 
                        freightChargesWithTax);
                }
                itemTable.Rows.Add("/*//*/", 
                    "/*//*/",
                    "/*//*/", 
                    "/*//*/", 
                    "/*//*/", 
                    "/*//*/", 
                    "/*//*/", 
                    "/*//*/",
                    "/*//*/",
                    "/*//*/",
                    //"Grand Total in " + selectedVendorCurrency,
                    "Grand Total",
                    vendorBidPrice);
            }


            requirementItems.Add(new KeyValuePair<string, DataTable>("ITEMS_TABLE", itemTable));

            #endregion Item Table

            #region Item Table Total

            DataTable itemTableTotal = new DataTable();

            itemTableTotal.Columns.Add(" khskaj ");
            itemTableTotal.Columns.Add(" khsdksj ");

            itemTableTotal.Rows.Add("Total", price);
            itemTableTotal.Rows.Add("Freight Charges", Utilities.RoundValue(freightcharges));
            //itemTableTotal.Rows.Add("Grand Total in " + requirement.Currency, vendorBidPrice);
            itemTableTotal.Rows.Add("Grand Total ", Utilities.RoundValue(vendorBidPrice));

            requirementItems.Add(new KeyValuePair<string, DataTable>("ITEMS_TABLE_TOTAL", itemTableTotal));

            #endregion Item Table Total

            #region terms & condition

            DataTable termsTable = new DataTable();

            //Define columns
            termsTable.Columns.Add(" Warranty ");
            if (string.IsNullOrEmpty(warranty))
            {
                termsTable.Columns.Add(" N ");
            }
            else
            {
                if (warranty == "Warranty")
                {
                    termsTable.Columns.Add(ExceptionHandler(warranty) + ".");
                }
                else
                {
                    termsTable.Columns.Add(ExceptionHandler(warranty));
                }
            }

            if (!string.IsNullOrEmpty(otherProperties))
            {
                termsTable.Rows.Add(" Other Properties ", ExceptionHandler(otherProperties.ToString()));
            }
            if (!string.IsNullOrEmpty(payment))
            {
                termsTable.Rows.Add(GetLabel("PAYMENT_TERMS"), payment.ToString());
            }
            if (!string.IsNullOrEmpty(duration))
            {
                termsTable.Rows.Add(GetLabel("DELIVERY_TERMS"), duration.ToString());
            }

            requirementItems.Add(new KeyValuePair<string, DataTable>("TERMS_TABLE", termsTable));

            #endregion terms & condition

            return requirementItems;
        }

        public static List<KeyValuePair<string, DataTable>> MakeDataTableMarginQuotation(List<RequirementItems> quotationObject, int reqID, int customerID, int vendorID, string sessionID,
            string warranty, string payment, string duration, string validity, double price, double tax, double freightcharges,
            double vendorBidPrice, bool isTabular, int revised, string quotationTaxes, double discountAmount, string otherProperties,
            string deliveryTermsDays, string deliveryTermsPercent, string paymentTermsDays, string paymentTermsPercent,
            string paymentTermsType, string folderPath, string gstNumber, int isRegret, double packingCharges,
            string selectedVendorCurrency, double revinstallationChargesWithTax, double revpackingChargesWithTax, double revfreightChargesWithTax,
            string INCO_TERMS, int TaxFiledValidation)
        {
            GetTemplateDetails(reqID);
            List<KeyValuePair<string, DataTable>> requirementItems = new List<KeyValuePair<string, DataTable>>();
            PRMServices prmservices = new PRMServices();
            Requirement requirement = prmservices.GetRequirementDataOfflinePrivate(reqID, customerID, sessionID);
            UserDetails customer = prmservices.GetUserDetails(customerID, sessionID);
            UserDetails vendor = prmservices.GetUserDetails(vendorID, sessionID);

            #region Item Table
            //Define columns
            DataTable itemTable = new DataTable();

            itemTable.Columns.Add(GetLabel("PRODUCT_NAME"));
            itemTable.Columns.Add(GetLabel("PRODUCT_NUMBER"));
            //itemTable.Columns.Add(" DESCRIPTION ");
            itemTable.Columns.Add(GetLabel("PRODUCT_MAKE_MODEL"));
            itemTable.Columns.Add(GetLabel("PRODUCT_QUANTITY"));
            itemTable.Columns.Add(" COST PRICE ");
            itemTable.Columns.Add(" TAX (%)");
            itemTable.Columns.Add(" MRP ");
            itemTable.Columns.Add(" NET PRICE ");
            itemTable.Columns.Add(" Margin Amount ");
            itemTable.Columns.Add(" Margin % ");

            bool isUOMDiffertent = false;

            foreach (RequirementItems quotation in quotationObject)
            {
                double itemPrice = 0;
                double unitPrice = 0;

                if (revised == 0)
                {
                    itemPrice = quotation.ItemPrice;
                    unitPrice = quotation.UnitPrice;
                }
                else if (revised == 1)
                {
                    itemPrice = quotation.RevItemPrice;
                    unitPrice = quotation.RevUnitPrice;
                }



                double itemUnitDiscount = 0;
                double itemCostPrice = 0;

                if (revised == 1)
                {
                    quotation.RevItemPrice = quotation.RevisedItemPrice;
                    itemUnitDiscount = quotation.RevUnitDiscount;

                }
                else if (revised == 0)
                {
                    itemUnitDiscount = quotation.UnitDiscount;
                }

                itemCostPrice = (quotation.UnitMRP * 100 * 100) / ((itemUnitDiscount * 100) + 10000 + (itemUnitDiscount * (quotation.CGst + quotation.SGst + quotation.IGst)) + ((quotation.CGst + quotation.SGst + quotation.IGst) * 100));

                int decimalPoint = Convert.ToInt16(ConfigurationManager.AppSettings["ROUNDING_DECIMALS"]);

                itemTable.Rows.Add(quotation.ProductIDorName, 
                    quotation.ProductNo, 
                    quotation.ProductBrand,
                    quotation.ProductQuantity,
                    Math.Round(Convert.ToDouble(itemCostPrice), decimalPoint),
                    quotation.CGst + quotation.SGst + quotation.IGst,
                    Math.Round(Convert.ToDouble(quotation.UnitMRP), decimalPoint),
                    Math.Round(Convert.ToDouble(itemCostPrice * (1 + (quotation.CGst + quotation.SGst + quotation.IGst) / 100)), decimalPoint),
                    Math.Round(Convert.ToDouble(quotation.UnitMRP - (itemCostPrice * (1 + (quotation.CGst + quotation.SGst + quotation.IGst) / 100))), decimalPoint),
                    Math.Round(Convert.ToDouble(itemUnitDiscount), decimalPoint));

            }

            requirementItems.Add(new KeyValuePair<string, DataTable>("ITEMS_TABLE", itemTable));

            #endregion Item Table

            #region Item Table Total
            double priceWithoutDiscount = vendorBidPrice + discountAmount;
            DataTable itemTableTotal = new DataTable();

            itemTableTotal.Columns.Add(" khskaj ");
            itemTableTotal.Columns.Add(" khsdksj ");

            itemTableTotal.Rows.Add(" Total ", price);
            itemTableTotal.Rows.Add(" Freight Charges ", Utilities.RoundValue(freightcharges));
            itemTableTotal.Rows.Add(" Price Without Discount ", priceWithoutDiscount.ToString());
            itemTableTotal.Rows.Add(" Discount ", Utilities.RoundValue(discountAmount).ToString());
            itemTableTotal.Rows.Add(" Grand Total ", Utilities.RoundValue(vendorBidPrice));

            requirementItems.Add(new KeyValuePair<string, DataTable>("ITEMS_TABLE_TOTAL", itemTableTotal));

            #endregion Item Table Total

            #region terms & condition

            DataTable termsTable = new DataTable();

            //Define columns
            termsTable.Columns.Add(" Warranty ");
            if (string.IsNullOrEmpty(warranty))
            {
                termsTable.Columns.Add(" N ");
            }
            else
            {
                if (warranty == "Warranty")
                {
                    termsTable.Columns.Add(ExceptionHandler(warranty) + ".");
                }
                else
                {
                    termsTable.Columns.Add(ExceptionHandler(warranty));
                }
            }

            if (!string.IsNullOrEmpty(otherProperties))
            {
                termsTable.Rows.Add(" Other Properties ", otherProperties.ToString());
            }
            if (!string.IsNullOrEmpty(payment))
            {
                termsTable.Rows.Add(GetLabel("PAYMENT_TERMS"), payment.ToString());
            }
            if (!string.IsNullOrEmpty(duration))
            {
                termsTable.Rows.Add(GetLabel("DELIVERY_TERMS"), duration.ToString());
            }
            if (!string.IsNullOrEmpty(INCO_TERMS))
            {
                termsTable.Rows.Add(GetLabel("GENERAL_TERMS"), ExceptionHandler(INCO_TERMS.ToString()));
            }

            requirementItems.Add(new KeyValuePair<string, DataTable>("TERMS_TABLE", termsTable));

            #endregion terms & condition

            return requirementItems;
        }

        public static void ExportDataTableQuotationToPdf(List<KeyValuePair<string, DataTable>> dtblTable, String strPdfPath, int reqId, int customerID,
            int vendorID, string sessionID, string duration, string gstNumber, string validity, int revised, string title, int reqIDFromJs, Requirement requirementDetails = null, string selectedVendorCurrency = "")
        {

            PRMServices prmservices = new PRMServices();
            Requirement requirement = requirementDetails != null ? requirementDetails : prmservices.GetRequirementDataOfflinePrivate(reqIDFromJs, customerID, sessionID);
            UserDetails customer = prmservices.GetUserDetails(customerID, sessionID);
            UserDetails vendor = prmservices.GetUserDetails(vendorID, sessionID);           
            System.IO.FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            KeyValuePair<string, DataTable> items_data = dtblTable.Where(item => item.Key == "ITEMS_TABLE").FirstOrDefault();
            if (items_data.Value.Columns.Count <= 8)
            {
                document.SetPageSize(iTextSharp.text.PageSize.A4);
            }
            else
            {

                document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            }
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            PdfWriterEvents writerEvent = new PdfWriterEvents(ConfigurationManager.AppSettings["COMPANY_NAME"].ToString(), 350, 300);
            writer.PageEvent = writerEvent;
            writerEvent.OnOpenDocument(writer, document);

            BaseFont bfntHead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont btnColumnHeader = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
            BaseFont sideHeaders = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            var FontColour = new BaseColor(128, 128, 128);
            var tabbleHeadColor = new BaseColor(17, 89, 128);
            var tabbleEvenColor = new BaseColor(242, 242, 242);
            var tableOddColor = new BaseColor(255, 255, 255);
            var tabbleFontBodColor = new BaseColor(117, 125, 138);
            var LineColour = new BaseColor(169, 169, 169);
            var BlackColour = new BaseColor(0, 0, 0);
            Font fntHead = new Font(bfntHead, 18, iTextSharp.text.Font.NORMAL, FontColour);
            Font sideHead = new Font(sideHeaders, 12, iTextSharp.text.Font.NORMAL, BlackColour);
            Font tblHead = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, tableOddColor);
            Font tblBody = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, BlackColour);
            Font tblBodyBold = new Font(sideHeaders, 8, iTextSharp.text.Font.NORMAL, BlackColour);
            Font fntColumnHeader = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, tableOddColor);
            Font fntQutBody = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, BlackColour);
            Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, LineColour, Element.ALIGN_LEFT, 1)));
            p.SpacingBefore = -1f;
            var spacerParagraph = new Paragraph();
            spacerParagraph.SpacingBefore = 3f;
            spacerParagraph.SpacingAfter = 0f;

            //paragraph prgheading = new paragraph();
            //prgheading.alignment = element.align_left;
            //prgheading.add(new chunk("requirement for quotation (rfq)", fnthead));
            //document.add(prgheading);

            Chunk glue = new Chunk(new VerticalPositionMark());
            Paragraph mainHead = new Paragraph(vendor.CompanyName?? string.Empty, fntHead);
            mainHead.Add(new Chunk(glue));

            if (revised > 0)
            {
                mainHead.Add("Revised Price Quotation");
            }
            else
            {
                mainHead.Add("Price Quotation");
            }
            mainHead.SpacingAfter = 1f;
            document.Add(mainHead);

            document.Add(p);
            document.Add(spacerParagraph);

            Paragraph rfqTitle = new Paragraph();
            rfqTitle.Alignment = Element.ALIGN_LEFT;
            rfqTitle.Add(new Chunk("RFQ Title : " + title, sideHead));
            document.Add(rfqTitle);

            document.Add(spacerParagraph);
            document.Add(spacerParagraph);

            Paragraph QutHead = new Paragraph("Quotation For : " + requirement.RequirementNumber, sideHead);
            QutHead.Add(new Chunk(glue));
            QutHead.Add(" Dated On : " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            document.Add(QutHead);

            Paragraph QutName = new Paragraph("Name : " + (customer.FirstName + " " + customer.LastName).ToString(), fntQutBody);
            QutName.Add(new Chunk(glue));
            QutName.Add("Phone : " + vendor.PhoneNum.ToString());
            document.Add(QutName);

            Paragraph QutCmpny = new Paragraph("Company : " + customer.CompanyName.ToString(), fntQutBody);
            QutCmpny.Add(new Chunk(glue));
            QutCmpny.Add("Prepared by : " + (vendor.FirstName + " " + vendor.LastName).ToString());
            document.Add(QutCmpny);

            Paragraph QutAdd = new Paragraph("Address : " + customer.Address.ToString(), fntQutBody);
            QutAdd.Add(new Chunk(glue));
            QutAdd.Add("E-mail : " + vendor.Email.ToString());
            document.Add(QutAdd);

            Paragraph QutPhone = new Paragraph("Phone : " + customer.PhoneNum.ToString(), fntQutBody);
            QutPhone.Add(new Chunk(glue));
            QutPhone.Add("Gst Number : " + gstNumber.ToString());//gstNumber.ToString()
            document.Add(QutPhone);

            document.Add(p);
            document.Add(spacerParagraph);

            Paragraph prodheading = new Paragraph(" Product Details : ", sideHead);
            prodheading.Add(new Chunk(glue));
            prodheading.Add("Currency: " + selectedVendorCurrency);
            //prodheading.Add("Validity : " + validity.ToString());
            document.Add(prodheading);

            document.Add(spacerParagraph);

            //KeyValuePair<string, DataTable> items_data = dtblTable.Where(item => item.Key == "ITEMS_TABLE").FirstOrDefault();
            KeyValuePair<string, DataTable> items_total_data = dtblTable.Where(item => item.Key == "ITEMS_TABLE_TOTAL").FirstOrDefault();

            float[] widths = new float[] { 20, 20, 10, 10, 15, 10, 5, 10 };
            PdfPTable tableItems = new PdfPTable(items_data.Value.Columns.Count);
            //if (items_data.Value.Columns.Count == 9)
            //{
            //    widths = new float[] { 20, 10, 10, 5, 15, 15, 10, 5, 10 };
            //}
            //else if (items_data.Value.Columns.Count == 10)
            //{
            //    widths = new float[] { 20, 5, 20, 5, 5, 5, 5, 10, 10, 10 };
            //}
            //else if (items_data.Value.Columns.Count == 11)
            //{
            //    widths = new float[] { 20, 5, 20, 5, 5, 5, 5, 10, 10, 5, 10 };
            //}
            if (items_data.Value.Columns.Count == 7)
            {
                widths = new float[] { 12, 20, 8, 10, 13, 9, 9 };
            }
            if (items_data.Value.Columns.Count == 8)
            {
                widths = new float[] { 12, 20, 8, 10, 13, 9, 9, 9};
            }
            if (items_data.Value.Columns.Count == 9)
            {
                widths = new float[] { 12, 20, 8, 10, 13, 9, 9, 9, 10 };
            }
            else if (items_data.Value.Columns.Count == 10)
            {
                widths = new float[] { 11, 18, 7, 10, 10, 8, 8, 9, 10, 9 };
            }
            else if (items_data.Value.Columns.Count == 11)
            {
                widths = new float[] { 11, 19, 8, 8, 5, 8, 8, 4, 9, 10, 10 };
            }
            else if (items_data.Value.Columns.Count == 12)
            {
                widths = new float[] { 11, 16, 7, 8, 5, 7, 7, 7, 5, 9, 10, 8 };
            }
            tableItems.SetWidths(widths);
            tableItems.DefaultCell.Padding = 3;
            tableItems.WidthPercentage = 100;
            tableItems.HorizontalAlignment = Element.ALIGN_CENTER;
            tableItems.DefaultCell.BorderColor = tableOddColor;

            var columnCount = items_data.Value.Columns.Count - 1;
            for (int i = 0; i < items_data.Value.Columns.Count; i++)
            {
                PdfPCell cell = new PdfPCell();
                cell.BackgroundColor = tabbleHeadColor;
                cell.PaddingBottom = 8;
                cell.VerticalAlignment = Element.ALIGN_CENTER;
                cell.AddElement(new Chunk(items_data.Value.Columns[i].ColumnName.ToString(), tblHead));
                tableItems.AddCell(cell);

            }

            var oddEvenFive = 1;
            for (int i = 0; i < items_data.Value.Rows.Count; i++)
            {
                var incColspan = 1;
                if (oddEvenFive % 2 == 0)
                {
                    for (int j = 0; j < items_data.Value.Columns.Count; j++)
                    {
                        if (items_data.Value.Rows[i][j].ToString() != "/*//*/") { 
                            PdfPCell tblcell = new PdfPCell();
                            //tblcell.BackgroundColor = tabbleEvenColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.Colspan = incColspan;
                            //tblcell.Colspan = items_data.Value.Columns.Count;
                            tblcell.VerticalAlignment = Element.ALIGN_CENTER;
                            if (items_data.Value.Rows.Count == oddEvenFive)
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBodyBold));
                            }
                            else
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBody));
                            }
                            
                            tableItems.AddCell(tblcell);
                            incColspan = 1;
                        }
                        else
                        {
                            incColspan++;
                        }
                    }
                }
                else
                {
                    for (int j = 0; j < items_data.Value.Columns.Count; j++)
                    {
                        if (items_data.Value.Rows[i][j].ToString() != "/*//*/")
                        {
                            PdfPCell tblcell = new PdfPCell();
                            //tblcell.BackgroundColor = tableOddColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.Colspan = incColspan;
                            //tblcell.Colspan = items_data.Value.Columns.Count;
                            tblcell.VerticalAlignment = Element.ALIGN_CENTER;
                            if (items_data.Value.Rows.Count == oddEvenFive)
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBodyBold));
                            }
                            else
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBody));
                            }
                            tableItems.AddCell(tblcell);
                            incColspan = 1;
                        }
                        else
                        {
                            incColspan++;
                        }
                    }
                }

                oddEvenFive++;
            }

            //for (int i = 0; i < items_total_data.Value.Rows.Count; i++)
            //{
            //    if (oddEvenFive % 2 == 0)
            //    {
            //        for (int j = 0; j < items_total_data.Value.Columns.Count; j++)
            //        {
            //            PdfPCell tblcell = new PdfPCell();
            //            tblcell.BackgroundColor = tabbleEvenColor;
            //            tblcell.PaddingTop = 0;
            //            tblcell.PaddingBottom = 8;
            //            if (j == 0)
            //            {
            //                tblcell.VerticalAlignment = Element.ALIGN_CENTER;
            //                tblcell.Colspan = columnCount;
            //            }
            //            tblcell.AddElement(new Chunk(items_total_data.Value.Rows[i][j].ToString(), tblBody));
            //            tableItems.AddCell(tblcell);
            //        }
            //    }
            //    else
            //    {
            //        for (int j = 0; j < items_total_data.Value.Columns.Count; j++)
            //        {
            //            PdfPCell tblcell = new PdfPCell();
            //            tblcell.BackgroundColor = tableOddColor;
            //            tblcell.PaddingTop = 0;
            //            tblcell.PaddingBottom = 8;
            //            if (j == 0)
            //            {
            //                tblcell.VerticalAlignment = Element.ALIGN_CENTER;
            //                tblcell.Colspan = columnCount;
            //            }
            //            tblcell.AddElement(new Chunk(items_total_data.Value.Rows[i][j].ToString(), tblBody));
            //            tableItems.AddCell(tblcell);
            //        }
            //    }

            //    oddEvenFive++;
            //}


            document.Add(tableItems);

            document.Add(spacerParagraph);
            document.Add(spacerParagraph);

            Paragraph prgheading = new Paragraph();
            prgheading.Alignment = Element.ALIGN_LEFT;
            prgheading.Add(new Chunk(" TERMS AND CONDITIONS : ", sideHead));
            document.Add(prgheading);


            document.Add(spacerParagraph);

            KeyValuePair<string, DataTable> terms_data = dtblTable.Where(item => item.Key == "TERMS_TABLE").FirstOrDefault();

            //Table Terms & Condition

            PdfPTable tableTerms = new PdfPTable(terms_data.Value.Columns.Count);
            float[] widthsTerms = new float[] { 30, 70 };
            tableTerms.SetWidths(widthsTerms);
            tableTerms.DefaultCell.Padding = 3;
            tableTerms.WidthPercentage = 100;
            tableTerms.DefaultCell.Border = 0;
            tableTerms.HorizontalAlignment = Element.ALIGN_CENTER;
            tableTerms.DefaultCell.BorderColor = tableOddColor;

            for (int i = 0; i < terms_data.Value.Columns.Count; i++)
            {
                PdfPCell cell = new PdfPCell();
                //cell.BackgroundColor = tabbleEvenColor;
                cell.PaddingBottom = 8;
                cell.Border = 0;
                cell.VerticalAlignment = Element.ALIGN_CENTER;
                cell.AddElement(new Chunk(terms_data.Value.Columns[i].ColumnName.ToString(), tblBody));
                tableTerms.AddCell(cell);

            }

            var oddEvenThree = 1;
            for (int i = 0; i < terms_data.Value.Rows.Count; i++)
            {
                if (oddEvenThree % 2 == 0)
                {
                    for (int j = 0; j < terms_data.Value.Columns.Count; j++)
                    {
                        PdfPCell tblcell = new PdfPCell();
                        //tblcell.BackgroundColor = tabbleEvenColor;
                        tblcell.PaddingTop = 0;
                        tblcell.PaddingBottom = 8;
                        tblcell.Border = 0;
                        tblcell.AddElement(new Chunk(terms_data.Value.Rows[i][j].ToString(), tblBody));
                        tableTerms.AddCell(tblcell);
                    }
                }
                else
                {
                    for (int j = 0; j < terms_data.Value.Columns.Count; j++)
                    {
                        PdfPCell tblcell = new PdfPCell();
                        //tblcell.BackgroundColor = tableOddColor;
                        tblcell.PaddingTop = 0;
                        tblcell.PaddingBottom = 8;
                        tblcell.Border = 0;
                        tblcell.AddElement(new Chunk(terms_data.Value.Rows[i][j].ToString(), tblBody));
                        tableTerms.AddCell(tblcell);
                    }
                }

                oddEvenThree++;
            }

            document.Add(tableTerms);

            //Paragraph prgNote = new Paragraph();
            //prgNote.Alignment = Element.ALIGN_LEFT;
            //prgNote.Add(new Chunk("NOTE : This Document is Auto Generated from PRM360 System. ", fntQutBody));
            //document.Add(prgNote);

            document.Add(spacerParagraph);

            Paragraph itemPriceNote = new Paragraph();
            itemPriceNote.Alignment = Element.ALIGN_LEFT;
            itemPriceNote.Add(new Chunk("Note: The formula used to calculate the Itemprice is (UNIT PRICE X QTY) X (1+Tax%). This Document is Auto Generated from PRM360 System. ", fntQutBody));
            document.Add(itemPriceNote);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);

            Paragraph prgThanks = new Paragraph();
            prgThanks.Alignment = Element.ALIGN_CENTER;
            prgThanks.Add(new Chunk(" THANK YOU FOR YOUR BUSINESS! ", sideHead));
            document.Add(prgThanks);

            PdfWriterEvents writerEvent1 = new PdfWriterEvents(ConfigurationManager.AppSettings["COMPANY_NAME"].ToString(), 350, 300);
            writer.PageEvent = writerEvent1;
            writerEvent.OnStartPage(writer, document);
            writerEvent.OnCloseDocument(writer, document);

            document.Close();
            writer.Close();
            fs.Close();

        }


        public static List<KeyValuePair<string, DataTable>> MakeDataTablePo(VendorPO vendorpo, Requirement req, Requirement vendorreq, UserDetails customer, UserDetails vendor)
        {
            List<KeyValuePair<string, DataTable>> PoItemsPdf = new List<KeyValuePair<string, DataTable>>();

            PRMServices prm = new PRMServices();
            string itemRows = string.Empty;
            double tax = 0;
            double totalPriceRev = 0;

            List<POItems> poItems = vendorpo.ListPOItems.Where(i => i.VendorPOQuantity > 0).ToList();
            int[] itemsArray = poItems.Select(p => p.ItemID).ToArray();

            List<RequirementItems> items = vendorreq.ListRequirementItems.Where(i => itemsArray.Contains(i.ItemID)).ToList();

            string commonExpectedDeliveryDate = "";
            string commmonDeliveryAddress = "";

            #region Item Table
            //Define columns
            DataTable itemTable = new DataTable();

            itemTable.Columns.Add("Product ID/Name");
            itemTable.Columns.Add("Product No.");
            //itemTable.Columns.Add("Description");  ri.ProductDescription,
            itemTable.Columns.Add("Brand Provider");
            if (!vendorpo.Common)
            {
                itemTable.Columns.Add("Delivery Date");
                itemTable.Columns.Add("Delivery Address");
            }
            itemTable.Columns.Add("Unit Price");
            itemTable.Columns.Add("Qty");
            itemTable.Columns.Add("VAT");
            itemTable.Columns.Add("Price");

            foreach (RequirementItems ri in items)
            {
                List<POItems> poitems = poItems.Where(i => i.ItemID == ri.ItemID).ToList();

                if (poitems.Count > 0)
                {

                    POItems poitem = poitems[0];

                    var GST = Convert.ToDouble(poitem.CGst) + Convert.ToDouble(poitem.SGst) + Convert.ToDouble(poitem.IGst);
                    var priceQuantity = Convert.ToDouble(poitem.POPrice) * Convert.ToDouble(poitem.VendorPOQuantity);


                    if (!vendorpo.Common)
                    {
                        itemTable.Rows.Add(ri.ProductIDorName, ri.ProductNo, ri.ProductBrand, poitem.ExpectedDeliveryDate,
                        poitem.DeliveryAddress, poitem.POPrice, poitem.VendorPOQuantity, GST, Convert.ToDouble(priceQuantity + ((priceQuantity / 100) * (GST))));
                    }
                    else
                    {

                        itemTable.Rows.Add(ri.ProductIDorName, ri.ProductNo, ri.ProductBrand, poitem.POPrice, poitem.VendorPOQuantity,
                            GST, Convert.ToDouble(priceQuantity + ((priceQuantity / 100) * (GST))));

                        commonExpectedDeliveryDate = Convert.ToDateTime(poitem.ExpectedDeliveryDate).ToString("dd/MM/yyyy");
                        commmonDeliveryAddress = poitem.DeliveryAddress;
                    }

                    totalPriceRev += Convert.ToDouble(priceQuantity + ((priceQuantity / 100) * (GST)));

                }
            }

            string POID = string.Empty;
            string Comments = string.Empty;

            //Requirement reqVendor = prm.GetRequirementData(req.RequirementID, vendorObj.UserID, sessionID);
            string taxRows = string.Empty;
            double totalPrice = totalPriceRev;
            foreach (RequirementTaxes taxItem in vendorreq.ListRequirementTaxes)
            {
                if (!vendorpo.Common)
                {
                    itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", taxItem.TaxName, taxItem.TaxPercentage);
                }
                else
                {
                    itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", taxItem.TaxName, taxItem.TaxPercentage);
                }
                totalPrice += (totalPriceRev * taxItem.TaxPercentage) / 100;
            }

            totalPrice += 0;//vendorreq.AuctionVendors[0].RevVendorFreight;

            if (!vendorpo.Common)
            {
                itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "Freight Charges", 0);
                itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", " Grand Total ", totalPrice);
            }
            else
            {
                itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "Freight Charges", 0);
                itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", " Grand Total ", totalPrice);
            }

            Credentials tinCred = prm.GetUserCredentials(req.CustomerID, vendorpo.SessionID).FirstOrDefault(v => v.FileType == "TIN");

            PoItemsPdf.Add(new KeyValuePair<string, DataTable>("ITEMS_TABLE", itemTable));

            #endregion Item Table

            return PoItemsPdf;
        }

        public static void MakeDataTablePoToPdf(List<KeyValuePair<string, DataTable>> dtblTable, String strPdfPath, UserDetails customer, UserDetails vendor, VendorPO vendorpo)
        {
            System.IO.FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            KeyValuePair<string, DataTable> items_data = dtblTable.Where(item => item.Key == "ITEMS_TABLE").FirstOrDefault();
            if (items_data.Value.Columns.Count >= 8)
            {
                document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            }
            else
            {
                document.SetPageSize(iTextSharp.text.PageSize.A4);
            }
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            BaseFont bfntHead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont btnColumnHeader = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
            BaseFont sideHeaders = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            var FontColour = new BaseColor(128, 128, 128);
            var tabbleHeadColor = new BaseColor(17, 89, 128);
            var tabbleEvenColor = new BaseColor(242, 242, 242);
            var tableOddColor = new BaseColor(255, 255, 255);
            var tabbleFontBodColor = new BaseColor(117, 125, 138);
            var LineColour = new BaseColor(169, 169, 169);
            var BlackColour = new BaseColor(0, 0, 0);
            Font fntHead = new Font(bfntHead, 18, iTextSharp.text.Font.NORMAL, FontColour);
            Font sideHead = new Font(sideHeaders, 12, iTextSharp.text.Font.NORMAL, BlackColour);
            Font tblHead = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, tableOddColor);
            Font tblBody = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, BlackColour);
            Font tblBodyBold = new Font(sideHeaders, 8, iTextSharp.text.Font.NORMAL, BlackColour);
            Font fntColumnHeader = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, tableOddColor);
            Font fntQutBody = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, BlackColour);
            Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, LineColour, Element.ALIGN_LEFT, 1)));
            p.SpacingBefore = -1f;
            var spacerParagraph = new Paragraph();
            spacerParagraph.SpacingBefore = 3f;
            spacerParagraph.SpacingAfter = 0f;

            Chunk glue = new Chunk(new VerticalPositionMark());
            Paragraph mainHead = new Paragraph(customer.CompanyName.ToString(), fntHead);
            mainHead.Add(new Chunk(glue));

            mainHead.Add("PURCHASE ORDER");

            mainHead.SpacingAfter = 2f;
            document.Add(mainHead);

            document.Add(p);
            document.Add(spacerParagraph);

            Paragraph QutHead = new Paragraph("Address : " + customer.Address.ToString(), fntQutBody);
            QutHead.Add(new Chunk(glue));
            QutHead.Add(" Dated On : " + DateTime.Now.ToShortDateString());
            document.Add(QutHead);

            Paragraph QutName = new Paragraph("Phone : " + customer.PhoneNum.ToString(), fntQutBody);
            QutName.Add(new Chunk(glue));
            QutName.Add("PO : " + vendorpo.PurchaseOrderID.ToString());
            document.Add(QutName);

            Paragraph QutCmpny = new Paragraph("Email : " + customer.Email.ToString(), fntQutBody);
            QutCmpny.Add(new Chunk(glue));
            QutCmpny.Add("Indent No : " + vendorpo.ListPOItems[0].IndentID.ToString());
            document.Add(QutCmpny);

            document.Add(spacerParagraph);

            Paragraph rfqTitle = new Paragraph();
            rfqTitle.Alignment = Element.ALIGN_LEFT;
            rfqTitle.Add(new Chunk("Ship To : ", sideHead));
            document.Add(rfqTitle);

            Paragraph rfqTitleCmpny = new Paragraph();
            rfqTitleCmpny.Alignment = Element.ALIGN_LEFT;
            rfqTitleCmpny.Add(new Chunk("Company Name : " + vendor.CompanyName.ToString(), fntQutBody));
            document.Add(rfqTitleCmpny);

            Paragraph rfqTitleAdd = new Paragraph();
            rfqTitleAdd.Alignment = Element.ALIGN_LEFT;
            rfqTitleAdd.Add(new Chunk("Address : " + vendor.Address.ToString(), fntQutBody));
            document.Add(rfqTitleAdd);

            Paragraph rfqTitlePhn = new Paragraph();
            rfqTitlePhn.Alignment = Element.ALIGN_LEFT;
            rfqTitlePhn.Add(new Chunk("Phone No : " + vendor.PhoneNum.ToString(), fntQutBody));
            document.Add(rfqTitlePhn);

            Paragraph rfqTitleEmail = new Paragraph();
            rfqTitleEmail.Alignment = Element.ALIGN_LEFT;
            rfqTitleEmail.Add(new Chunk("Email : " + vendor.Email.ToString(), fntQutBody));
            document.Add(rfqTitleEmail);

            document.Add(p);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);



            float[] widths = new float[] { 25, 25, 10, 10, 10, 10, 10 };
            PdfPTable tableItems = new PdfPTable(items_data.Value.Columns.Count);
            if (items_data.Value.Columns.Count == 8)
            {
                widths = new float[] { 20, 20, 10, 10, 10, 10, 10, 10 };
            }
            else if (items_data.Value.Columns.Count == 9)
            {
                widths = new float[] { 20, 10, 10, 5, 15, 15, 10, 5, 10 };
            }
            else if (items_data.Value.Columns.Count == 10)
            {
                widths = new float[] { 20, 5, 25, 5, 5, 5, 10, 10, 5, 10 };
            }
            else if (items_data.Value.Columns.Count == 11)
            {
                widths = new float[] { 20, 5, 20, 5, 5, 5, 5, 10, 10, 5, 10 };
            }
            tableItems.SetWidths(widths);
            tableItems.DefaultCell.Padding = 3;
            tableItems.WidthPercentage = 100;
            tableItems.HorizontalAlignment = Element.ALIGN_CENTER;
            tableItems.DefaultCell.BorderColor = tableOddColor;

            var columnCount = items_data.Value.Columns.Count - 1;
            for (int i = 0; i < items_data.Value.Columns.Count; i++)
            {
                PdfPCell cell = new PdfPCell();
                cell.BackgroundColor = tabbleHeadColor;
                cell.PaddingBottom = 8;
                cell.VerticalAlignment = Element.ALIGN_CENTER;
                cell.AddElement(new Chunk(items_data.Value.Columns[i].ColumnName.ToString(), tblHead));
                tableItems.AddCell(cell);

            }

            var oddEvenFive = 1;
            for (int i = 0; i < items_data.Value.Rows.Count; i++)
            {
                var incColspan = 1;
                if (oddEvenFive % 2 == 0)
                {
                    for (int j = 0; j < items_data.Value.Columns.Count; j++)
                    {
                        if (items_data.Value.Rows[i][j].ToString() != "/*//*/")
                        {
                            PdfPCell tblcell = new PdfPCell();
                            tblcell.BackgroundColor = tabbleEvenColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.Colspan = incColspan;
                            tblcell.VerticalAlignment = Element.ALIGN_CENTER;
                            if (items_data.Value.Rows.Count == oddEvenFive)
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBodyBold));
                            }
                            else
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBody));
                            }

                            tableItems.AddCell(tblcell);
                            incColspan = 1;
                        }
                        else
                        {
                            incColspan++;
                        }
                    }
                }
                else
                {
                    for (int j = 0; j < items_data.Value.Columns.Count; j++)
                    {
                        if (items_data.Value.Rows[i][j].ToString() != "/*//*/")
                        {
                            PdfPCell tblcell = new PdfPCell();
                            tblcell.BackgroundColor = tableOddColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.Colspan = incColspan;
                            tblcell.VerticalAlignment = Element.ALIGN_CENTER;
                            if (items_data.Value.Rows.Count == oddEvenFive)
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBodyBold));
                            }
                            else
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBody));
                            }
                            tableItems.AddCell(tblcell);
                            incColspan = 1;
                        }
                        else
                        {
                            incColspan++;
                        }
                    }
                }

                oddEvenFive++;
            }

            document.Add(tableItems);

            Paragraph prgNote = new Paragraph();
            prgNote.Alignment = Element.ALIGN_LEFT;
            prgNote.Add(new Chunk("NOTE : This Document is Auto Generated from PRM360 System. ", fntQutBody));
            document.Add(prgNote);

            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);

            Paragraph prgThanks = new Paragraph();
            prgThanks.Alignment = Element.ALIGN_CENTER;
            prgThanks.Add(new Chunk(" THANK YOU FOR YOUR BUSINESS! ", sideHead));
            document.Add(prgThanks);

            document.Close();
            writer.Close();
            fs.Close();
        }


        public static List<KeyValuePair<string, DataTable>> MakeDataTableDispatch(DispatchTrack dispatchtrack, Requirement req, Requirement vendorreq, UserDetails customer, UserDetails vendor)
        {
            GetTemplateDetails(req.RequirementID);
            List<KeyValuePair<string, DataTable>> PoItemsPdf = new List<KeyValuePair<string, DataTable>>();

            PRMServices prm = new PRMServices();
            string itemRows = string.Empty;
            double tax = 0;
            double totalPriceRev = 0;

            List<POItems> poItems = dispatchtrack.POItemsEntity.Where(i => i.VendorPOQuantity > 0).ToList();
            int[] itemsArray = poItems.Select(p => p.ItemID).ToArray();

            List<RequirementItems> items = vendorreq.ListRequirementItems.Where(i => itemsArray.Contains(i.ItemID)).ToList();

            string commonExpectedDeliveryDate = "";
            string commmonDeliveryAddress = "";


          

            #region Item Table
            //Define columns
            DataTable itemTable = new DataTable();

            itemTable.Columns.Add(GetLabel("PRODUCT_NAME"));
            itemTable.Columns.Add("PO Qty");
            itemTable.Columns.Add("TOTAL DIS.QTY");
            itemTable.Columns.Add("Dispatch Qty");
            itemTable.Columns.Add("Unit Price");
           

            foreach (RequirementItems ri in items)
            {
                foreach (POItems po in dispatchtrack.POItemsEntity)
                {
                    //List<POItems> poitems = poItems.Where(i => i.ItemID == ri.ItemID).ToList();

                    if (dispatchtrack.POItemsEntity.Count > 0 && po.ItemID == ri.ItemID)
                    {

                        // POItems poitem = poitems[0];

                        var GST = Convert.ToDouble(po.CGst) + Convert.ToDouble(po.SGst) + Convert.ToDouble(po.IGst);
                        var priceQuantity = Convert.ToDouble(po.POPrice) * Convert.ToDouble(po.VendorPOQuantity);


                        if (!dispatchtrack.Common)
                        {
                            itemTable.Rows.Add(ri.ProductIDorName, po.VendorPOQuantity, po.DispatchQuantity, po.DispatchQuantity, po.POPrice);
                        }
                        else
                        {

                            itemTable.Rows.Add(ri.ProductIDorName, po.VendorPOQuantity, po.DispatchQuantity, po.DispatchQuantity, po.POPrice);

                            commonExpectedDeliveryDate = Convert.ToDateTime(po.ExpectedDeliveryDate).ToString("dd/MM/yyyy");
                            commmonDeliveryAddress = po.DeliveryAddress;
                        }

                        totalPriceRev += Convert.ToDouble(priceQuantity + ((priceQuantity / 100) * (GST)));

                    }
                }
              
            }

            string POID = string.Empty;
            string Comments = string.Empty;

            //Requirement reqVendor = prm.GetRequirementData(req.RequirementID, vendorObj.UserID, sessionID);
            string taxRows = string.Empty;
            double totalPrice = totalPriceRev;
            foreach (RequirementTaxes taxItem in vendorreq.ListRequirementTaxes)
            {
                if (!dispatchtrack.Common)
                {
                    itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", taxItem.TaxName, taxItem.TaxPercentage);
                }
                else
                {
                    itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", taxItem.TaxName, taxItem.TaxPercentage);
                }
                totalPrice += (totalPriceRev * taxItem.TaxPercentage) / 100;
            }

            totalPrice += 0;//vendorreq.AuctionVendors[0].RevVendorFreight;

            //if (!dispatchtrack.Common)
            //{
            //    itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "Freight Charges", vendorreq.AuctionVendors[0].RevVendorFreight);
            //    itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", " Grand Total ", totalPrice);
            //}
            //else
            //{
            //    itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "Freight Charges", vendorreq.AuctionVendors[0].RevVendorFreight);
            //    itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", " Grand Total ", totalPrice);
            //}

            Credentials tinCred = prm.GetUserCredentials(req.CustomerID, dispatchtrack.SessionID).FirstOrDefault(v => v.FileType == "TIN");

            PoItemsPdf.Add(new KeyValuePair<string, DataTable>("ITEMS_TABLE", itemTable));

            #endregion Item Table

            return PoItemsPdf;
        }


        public static void MakeDataTableDispatchToPdf(List<KeyValuePair<string, DataTable>> dtblTable, String strPdfPath, UserDetails customer, UserDetails vendor, DispatchTrack dispatchtrack)
        {
            System.IO.FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            KeyValuePair<string, DataTable> items_data = dtblTable.Where(item => item.Key == "ITEMS_TABLE").FirstOrDefault();
            if (items_data.Value.Columns.Count >= 8)
            {
                document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            }
            else
            {
                document.SetPageSize(iTextSharp.text.PageSize.A4);
            }
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            BaseFont bfntHead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont btnColumnHeader = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
            BaseFont sideHeaders = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            var FontColour = new BaseColor(128, 128, 128);
            var tabbleHeadColor = new BaseColor(17, 89, 128);
            var tabbleEvenColor = new BaseColor(242, 242, 242);
            var tableOddColor = new BaseColor(255, 255, 255);
            var tabbleFontBodColor = new BaseColor(117, 125, 138);
            var LineColour = new BaseColor(169, 169, 169);
            var BlackColour = new BaseColor(0, 0, 0);
            Font fntHead = new Font(bfntHead, 18, iTextSharp.text.Font.NORMAL, FontColour);
            Font sideHead = new Font(sideHeaders, 12, iTextSharp.text.Font.NORMAL, BlackColour);
            Font tblHead = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, tableOddColor);
            Font tblBody = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, BlackColour);
            Font tblBodyBold = new Font(sideHeaders, 8, iTextSharp.text.Font.NORMAL, BlackColour);
            Font fntColumnHeader = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, tableOddColor);
            Font fntQutBody = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, BlackColour);
            Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, LineColour, Element.ALIGN_LEFT, 1)));
            p.SpacingBefore = -1f;
            var spacerParagraph = new Paragraph();
            spacerParagraph.SpacingBefore = 3f;
            spacerParagraph.SpacingAfter = 0f;

            Chunk glue = new Chunk(new VerticalPositionMark());
            Paragraph mainHead = new Paragraph(customer.CompanyName.ToString(), fntHead);
            mainHead.Add(new Chunk(glue));

            mainHead.Add("MATERIAL DISPATCH REPORT");

            mainHead.SpacingAfter = 2f;
            document.Add(mainHead);

            document.Add(p);
            document.Add(spacerParagraph);

            Paragraph QutReq = new Paragraph("Requirement ID : " + dispatchtrack.POItemsEntity[0].ReqID.ToString(), fntQutBody);
            QutReq.Add(new Chunk(glue));
            //QutHead.Add(" Dated On : " + DateTime.Now.ToShortDateString());
            document.Add(QutReq);

            Paragraph QutPOno = new Paragraph("Purchase Order No : " + dispatchtrack.POID.ToString(), fntQutBody);
            QutPOno.Add(new Chunk(glue));
            //QutName.Add("PO : " + dispatchtrack.POItemsEntity[0].PurchaseID.ToString());
            document.Add(QutPOno);

            Paragraph QutIndent = new Paragraph("Indent No : " + dispatchtrack.POItemsEntity[0].IndentID.ToString(), fntQutBody);
            QutIndent.Add(new Chunk(glue));
            //QutCmpny.Add("Indent No : " + dispatchtrack.POItemsEntity[0].IndentID.ToString());
            document.Add(QutIndent);

            document.Add(spacerParagraph);

            Paragraph rfqTitle = new Paragraph();
            rfqTitle.Alignment = Element.ALIGN_LEFT;
            rfqTitle.Add(new Chunk("Dispatch Details : ", sideHead));
            document.Add(rfqTitle);

            Paragraph QutDispType = new Paragraph("Dispatch Type : " + dispatchtrack.DispatchType.ToString(), fntQutBody);
            QutDispType.Add(new Chunk(glue));
            //QutHead.Add(" Dated On : " + DateTime.Now.ToShortDateString());
            document.Add(QutDispType);

            Paragraph QutTrackID = new Paragraph("Delivery TrackID : " + dispatchtrack.DeliveryTrackID.ToString(), fntQutBody);
            QutTrackID.Add(new Chunk(glue));
            //QutHead.Add(" Dated On : " + DateTime.Now.ToShortDateString());
            document.Add(QutTrackID);

            Paragraph QutDispCode = new Paragraph("Dispatch Code : " + dispatchtrack.DispatchCode.ToString(), fntQutBody);
            QutDispCode.Add(new Chunk(glue));
            //QutHead.Add(" Dated On : " + DateTime.Now.ToShortDateString());
            document.Add(QutDispCode);

            Paragraph QutDispMode = new Paragraph("Dispatch Mode : " + dispatchtrack.DispatchMode.ToString(), fntQutBody);
            QutDispMode.Add(new Chunk(glue));
            //QutHead.Add(" Dated On : " + DateTime.Now.ToShortDateString());
            document.Add(QutDispMode);

            Paragraph QutDispDate = new Paragraph("Dispatch Date : " + Convert.ToDateTime(dispatchtrack.DispatchDate).ToString("dd/MM/yyyy"), fntQutBody);
            QutDispDate.Add(new Chunk(glue));
            //QutHead.Add(" Dated On : " + DateTime.Now.ToShortDateString());
            document.Add(QutDispDate);

           
            document.Add(p);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);



            float[] widths = new float[] { 25, 25, 10, 10, 10, 10, 10 };
            PdfPTable tableItems = new PdfPTable(items_data.Value.Columns.Count);
            if (items_data.Value.Columns.Count == 8)
            {
                widths = new float[] { 20, 20, 10, 10, 10, 10, 10, 10 };
            }
            else if (items_data.Value.Columns.Count == 9)
            {
                widths = new float[] { 20, 10, 10, 5, 15, 15, 10, 5, 10 };
            }
            else if (items_data.Value.Columns.Count == 10)
            {
                widths = new float[] { 20, 5, 25, 5, 5, 5, 10, 10, 5, 10 };
            }
            else if (items_data.Value.Columns.Count == 11)
            {
                widths = new float[] { 20, 5, 20, 5, 5, 5, 5, 10, 10, 5, 10 };
            }
            else if (items_data.Value.Columns.Count == 5)
            {
                widths = new float[] { 25, 25, 25, 25, 25 };
            }
            tableItems.SetWidths(widths);
            tableItems.DefaultCell.Padding = 3;
            tableItems.WidthPercentage = 100;
            tableItems.HorizontalAlignment = Element.ALIGN_CENTER;
            tableItems.DefaultCell.BorderColor = tableOddColor;

            var columnCount = items_data.Value.Columns.Count - 1;
            for (int i = 0; i < items_data.Value.Columns.Count; i++)
            {
                PdfPCell cell = new PdfPCell();
                cell.BackgroundColor = tabbleHeadColor;
                cell.PaddingBottom = 8;
                cell.VerticalAlignment = Element.ALIGN_CENTER;
                cell.AddElement(new Chunk(items_data.Value.Columns[i].ColumnName.ToString(), tblHead));
                tableItems.AddCell(cell);

            }

            var oddEvenFive = 1;
            for (int i = 0; i < items_data.Value.Rows.Count; i++)
            {
                var incColspan = 1;
                if (oddEvenFive % 2 == 0)
                {
                    for (int j = 0; j < items_data.Value.Columns.Count; j++)
                    {
                        if (items_data.Value.Rows[i][j].ToString() != "/*//*/")
                        {
                            PdfPCell tblcell = new PdfPCell();
                            tblcell.BackgroundColor = tabbleEvenColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.Colspan = incColspan;
                            tblcell.VerticalAlignment = Element.ALIGN_CENTER;
                            if (items_data.Value.Rows.Count == oddEvenFive)
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBodyBold));
                            }
                            else
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBody));
                            }

                            tableItems.AddCell(tblcell);
                            incColspan = 1;
                        }
                        else
                        {
                            incColspan++;
                        }
                    }
                }
                else
                {
                    for (int j = 0; j < items_data.Value.Columns.Count; j++)
                    {
                        if (items_data.Value.Rows[i][j].ToString() != "/*//*/")
                        {
                            PdfPCell tblcell = new PdfPCell();
                            tblcell.BackgroundColor = tableOddColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.Colspan = incColspan;
                            tblcell.VerticalAlignment = Element.ALIGN_CENTER;
                            if (items_data.Value.Rows.Count == oddEvenFive)
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBodyBold));
                            }
                            else
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBody));
                            }
                            tableItems.AddCell(tblcell);
                            incColspan = 1;
                        }
                        else
                        {
                            incColspan++;
                        }
                    }
                }

                oddEvenFive++;
            }

            document.Add(tableItems);

            Paragraph prgNote = new Paragraph();
            prgNote.Alignment = Element.ALIGN_LEFT;
            prgNote.Add(new Chunk("NOTE : This Document is Auto Generated from PRM360 System. ", fntQutBody));
            document.Add(prgNote);

            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);

            Paragraph prgThanks = new Paragraph();
            prgThanks.Alignment = Element.ALIGN_CENTER;
            prgThanks.Add(new Chunk(" THANK YOU FOR YOUR BUSINESS! ", sideHead));
            document.Add(prgThanks);

            document.Close();
            writer.Close();
            fs.Close();
        }


        public static List<KeyValuePair<string, DataTable>> MakeDataTableMRR(DispatchTrack dispatchtrack, Requirement req, Requirement vendorreq, UserDetails customer, UserDetails vendor)
        {
            GetTemplateDetails(req.RequirementID);
            List<KeyValuePair<string, DataTable>> PoItemsPdf = new List<KeyValuePair<string, DataTable>>();

            PRMServices prm = new PRMServices();
            string itemRows = string.Empty;
            double tax = 0;
            double totalPriceRev = 0;

            List<POItems> poItems = dispatchtrack.POItemsEntity.Where(i => i.VendorPOQuantity > 0).ToList();
            int[] itemsArray = poItems.Select(p => p.ItemID).ToArray();

            List<RequirementItems> items = vendorreq.ListRequirementItems.Where(i => itemsArray.Contains(i.ItemID)).ToList();

            string commonExpectedDeliveryDate = "";
            string commmonDeliveryAddress = "";




            #region Item Table
            //Define columns
            DataTable itemTable = new DataTable();

            itemTable.Columns.Add(GetLabel("PRODUCT_NAME"));
            itemTable.Columns.Add("PO Qty");
            itemTable.Columns.Add("TOTAL Asn QTY");
            itemTable.Columns.Add("TOTAL REC.QTY");
            itemTable.Columns.Add("Asn Qty");
            itemTable.Columns.Add("Unit Price");
            itemTable.Columns.Add("Accepted Qty");
            itemTable.Columns.Add("Return Qty");


            foreach (RequirementItems ri in items)
            {
                foreach (POItems po in dispatchtrack.POItemsEntity)
                {
                    //List<POItems> poitems = poItems.Where(i => i.ItemID == ri.ItemID).ToList();

                    if (dispatchtrack.POItemsEntity.Count > 0 && po.ItemID == ri.ItemID)
                    {

                        // POItems poitem = poitems[0];

                        var GST = Convert.ToDouble(po.CGst) + Convert.ToDouble(po.SGst) + Convert.ToDouble(po.IGst);
                        var priceQuantity = Convert.ToDouble(po.POPrice) * Convert.ToDouble(po.RecivedQuantity);


                        if (!dispatchtrack.Common)
                        {
                            itemTable.Rows.Add(ri.ProductIDorName, po.VendorPOQuantity, po.DispatchQuantity, po.RecivedQuantity, po.DispatchQuantity, po.POPrice, po.SumRecivedQuantity,po.SumReturnQuantity);
                        }
                        else
                        {

                            itemTable.Rows.Add(ri.ProductIDorName, po.VendorPOQuantity, po.DispatchQuantity, po.RecivedQuantity, po.DispatchQuantity, po.POPrice, po.SumRecivedQuantity, po.SumReturnQuantity);

                            commonExpectedDeliveryDate = Convert.ToDateTime(po.ExpectedDeliveryDate).ToString("dd/MM/yyyy");
                            commmonDeliveryAddress = po.DeliveryAddress;
                        }

                        totalPriceRev += Convert.ToDouble(priceQuantity);

                    }
                }

            }

            string POID = string.Empty;
            string Comments = string.Empty;

            //Requirement reqVendor = prm.GetRequirementData(req.RequirementID, vendorObj.UserID, sessionID);
            string taxRows = string.Empty;
            double totalPrice = totalPriceRev;
            foreach (RequirementTaxes taxItem in vendorreq.ListRequirementTaxes)
            {
                if (!dispatchtrack.Common)
                {
                    itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", taxItem.TaxName, taxItem.TaxPercentage);
                }
                else
                {
                    itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", taxItem.TaxName, taxItem.TaxPercentage);
                }
                totalPrice += (totalPriceRev * taxItem.TaxPercentage) / 100;
            }

            totalPrice += 0;//vendorreq.AuctionVendors[0].RevVendorFreight;

            if (!dispatchtrack.Common)
            {
               // itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "Freight Charges", vendorreq.AuctionVendors[0].RevVendorFreight);
                itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", " Grand Total ", totalPriceRev);
            }
            else
            {
               // itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "Freight Charges", vendorreq.AuctionVendors[0].RevVendorFreight);
                itemTable.Rows.Add("/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", "/*//*/", " Grand Total ", totalPriceRev);
            }

            Credentials tinCred = prm.GetUserCredentials(req.CustomerID, dispatchtrack.SessionID).FirstOrDefault(v => v.FileType == "TIN");

            PoItemsPdf.Add(new KeyValuePair<string, DataTable>("ITEMS_TABLE", itemTable));

            #endregion Item Table

            #region mrr table
            DataTable mrrTable = new DataTable();
            
            

         
            

            //mrrTable.Rows.Add("Invoice No", ExceptionHandler(dispatchtrack.POItemsEntity[0].IndentID.ToString()));
            //mrrTable.Rows.Add("Date", dateFormatChange(DateTime.UtcNow));
            //mrrTable.Rows.Add("Delivery Challan No", 11);

            PoItemsPdf.Add(new KeyValuePair<string, DataTable>("MRR_TABLE", mrrTable));
            #endregion mrr table



            return PoItemsPdf;
        }


        public static void MakeDataTableMRRToPdf(List<KeyValuePair<string, DataTable>> dtblTable, String strPdfPath, UserDetails customer, UserDetails vendor, DispatchTrack dispatchtrack)
        {
            System.IO.FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            KeyValuePair<string, DataTable> items_data = dtblTable.Where(item => item.Key == "ITEMS_TABLE").FirstOrDefault();
            // KeyValuePair<string, DataTable> mrr_data = dtblTable.Where(item => item.Key == "MRR_TABLE").FirstOrDefault();
            if (items_data.Value.Columns.Count >= 8)
            {
                document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            }
            else
            {
                document.SetPageSize(iTextSharp.text.PageSize.A4);
            }
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            BaseFont bfntHead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont btnColumnHeader = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
            BaseFont sideHeaders = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            var FontColour = new BaseColor(128, 128, 128);
            var tabbleHeadColor = new BaseColor(17, 89, 128);
            var tabbleEvenColor = new BaseColor(242, 242, 242);
            var tableOddColor = new BaseColor(255, 255, 255);
            var tabbleFontBodColor = new BaseColor(117, 125, 138);
            var LineColour = new BaseColor(169, 169, 169);
            var BlackColour = new BaseColor(0, 0, 0);
            iTextSharp.text.Font myFont = new iTextSharp.text.Font(bfntHead, 10, iTextSharp.text.Font.NORMAL);
            Font fntHead = new Font(bfntHead, 18, iTextSharp.text.Font.NORMAL, FontColour);
            Font sideHead = new Font(sideHeaders, 12, iTextSharp.text.Font.NORMAL, BlackColour);
            Font tblHead = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, tableOddColor);
            Font tblBody = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, BlackColour);
            Font tblBodyBold = new Font(sideHeaders, 8, iTextSharp.text.Font.NORMAL, BlackColour);
            Font fntColumnHeader = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, tableOddColor);
            Font fntQutBody = new Font(btnColumnHeader, 8, iTextSharp.text.Font.NORMAL, BlackColour);
            Font commentHead = new Font(sideHeaders, 10, iTextSharp.text.Font.NORMAL, BlackColour);
            Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, LineColour, Element.ALIGN_LEFT, 1)));
            p.SpacingBefore = -1f;
            var spacerParagraph = new Paragraph();
            spacerParagraph.SpacingBefore = 3f;
            spacerParagraph.SpacingAfter = 0f;

            Chunk glue = new Chunk(new VerticalPositionMark());
            Paragraph mainHead = new Paragraph(customer.CompanyName.ToString(), fntHead);
            mainHead.Add(new Chunk(glue));

            mainHead.Add("ASN DOCUMENT");

            mainHead.SpacingAfter = 2f;
            document.Add(mainHead);

            document.Add(p);
            document.Add(spacerParagraph);

            Paragraph QutReq = new Paragraph("Supplier Name : " + dispatchtrack.POItemsEntity[0].VendorName.ToString(), sideHead);
            QutReq.Add(new Chunk(glue));
            //QutHead.Add(" Dated On : " + DateTime.Now.ToShortDateString());
            document.Add(QutReq);

            document.Add(p);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);

            Paragraph ponum = new Paragraph("PO No : " + dispatchtrack.PurchaseID.ToString(), tblBody);
            ponum.Add(new Chunk(glue));
            ponum.Add(" Date : " + dateFormatChange(DateTime.UtcNow));
            document.Add(ponum);

            Paragraph po = new Paragraph("ASN Code : " + dispatchtrack.DispatchCode.ToString(),tblBody);
            po.Add(new Chunk(glue));
            po.Add(" Delivery Date(ETA) : " + dispatchtrack.DispatchDate.ToString());
            document.Add(po);

            Paragraph po1 = new Paragraph("Shiment Date : " + dispatchtrack.ShipmentDate.ToString(), tblBody);
            po1.Add(new Chunk(glue));
            po1.Add(" Document Date : " + dispatchtrack.DocumentDate.ToString());
            document.Add(po1);

            Paragraph po2 = new Paragraph("Customer Batch : " + dispatchtrack.CustomerBatch.ToString(), tblBody);
            po2.Add(new Chunk(glue));
            po2.Add(" Customer Location : " + dispatchtrack.CustomerLocation.ToString());
            document.Add(po2);

            Paragraph po3 = new Paragraph("Ship From Location : " + dispatchtrack.ShipmentFromLocation.ToString(), tblBody);
            po3.Add(new Chunk(glue));
            po3.Add(" Ship To Location : " + dispatchtrack.ShipToLocation.ToString());
            document.Add(po3);

            Paragraph po4 = new Paragraph("Unloading Point : " + dispatchtrack.UnloadingPoint.ToString(), tblBody);
            po4.Add(new Chunk(glue));
            po4.Add(" Shipped Through : " + dispatchtrack.ShippedThrough.ToString());
            document.Add(po4);

            Paragraph po5 = new Paragraph("Invoice No : " + dispatchtrack.POItemsEntity[0].IndentID.ToString(), tblBody);
            po5.Add(new Chunk(glue));
            po5.Add(" Delivery Challan No : " + 11);
            document.Add(po5);

            //mrrTable.Rows.Add("Shiment Date", ExceptionHandler(dispatchtrack.ShipmentDate.ToString()));

            //mrrTable.Rows.Add("Document Date", ExceptionHandler(dispatchtrack.DocumentDate.ToString()));

            document.Add(spacerParagraph);
            document.Add(spacerParagraph);


            //PdfPTable tableMRR = new PdfPTable(mrr_data.Value.Columns.Count);
            //tableMRR.DefaultCell.Padding = 3;
            //tableMRR.WidthPercentage = 100;
            //tableMRR.DefaultCell.Border = 0;
            //tableMRR.HorizontalAlignment = Element.ALIGN_CENTER;
            //tableMRR.DefaultCell.BorderColor = tableOddColor;

            //for (int i = 0; i < mrr_data.Value.Columns.Count; i++)
            //{
            //    PdfPCell cell = new PdfPCell();
            //    cell.BackgroundColor = tabbleEvenColor;
            //    cell.PaddingBottom = 8;
            //    cell.Border = 0;
            //    cell.VerticalAlignment = Element.ALIGN_CENTER;
            //    cell.AddElement(new Chunk(mrr_data.Value.Columns[i].ColumnName.ToString(), myFont));
            //    tableMRR.AddCell(cell);

            //}

            //var oddEvenOne = 1;
            //for (int i = 0; i < mrr_data.Value.Rows.Count; i++)
            //{
            //    if (oddEvenOne % 2 == 0)
            //    {
            //        for (int j = 0; j < mrr_data.Value.Columns.Count; j++)
            //        {
            //            PdfPCell tblcell = new PdfPCell();
            //            tblcell.BackgroundColor = tabbleEvenColor;
            //            tblcell.PaddingTop = 0;
            //            tblcell.PaddingBottom = 8;
            //            tblcell.Border = 0;
            //            tblcell.AddElement(new Chunk(mrr_data.Value.Rows[i][j].ToString(), myFont));
            //            tableMRR.AddCell(tblcell);
            //        }
            //    }
            //    else
            //    {
            //        for (int j = 0; j < mrr_data.Value.Columns.Count; j++)
            //        {
            //            PdfPCell tblcell = new PdfPCell();
            //            tblcell.BackgroundColor = tableOddColor;
            //            tblcell.PaddingTop = 0;
            //            tblcell.PaddingBottom = 8;
            //            tblcell.Border = 0;
            //            tblcell.AddElement(new Chunk(mrr_data.Value.Rows[i][j].ToString(), myFont));
            //            tableMRR.AddCell(tblcell);
            //        }
            //    }

            //    oddEvenOne++;
            //}

            //document.Add(tableMRR);
            
            //Paragraph QutPOno = new Paragraph("Purchase No : " + dispatchtrack.POID.ToString(), fntQutBody);
            //QutPOno.Add(new Chunk(glue));
            ////QutName.Add("PO : " + dispatchtrack.POItemsEntity[0].PurchaseID.ToString());
            //document.Add(QutPOno);

            //Paragraph QutIndent = new Paragraph("Indent No : " + dispatchtrack.POItemsEntity[0].IndentID.ToString(), fntQutBody);
            //QutIndent.Add(new Chunk(glue));
            ////QutCmpny.Add("Indent No : " + dispatchtrack.POItemsEntity[0].IndentID.ToString());
            //document.Add(QutIndent);


            //Paragraph date = new Paragraph("Date : " + DateTime.Now.ToShortDateString());
            //date.Add(new Chunk(glue));
            //document.Add(date);
            
            document.Add(p);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);



            float[] widths = new float[] { 25, 25, 10, 10, 10, 10, 10 };
            PdfPTable tableItems = new PdfPTable(items_data.Value.Columns.Count);
            if (items_data.Value.Columns.Count == 8)
            {
                widths = new float[] { 20, 20, 10, 10, 10, 10, 10, 10 };
            }
            else if (items_data.Value.Columns.Count == 9)
            {
                widths = new float[] { 20, 10, 10, 5, 15, 15, 10, 5, 10 };
            }
            else if (items_data.Value.Columns.Count == 10)
            {
                widths = new float[] { 20, 5, 25, 5, 5, 5, 10, 10, 5, 10 };
            }
            else if (items_data.Value.Columns.Count == 11)
            {
                widths = new float[] { 20, 5, 20, 5, 5, 5, 5, 10, 10, 5, 10 };
            }
            else if (items_data.Value.Columns.Count == 5)
            {
                widths = new float[] { 25, 25, 25, 25, 25 };
            }
            tableItems.SetWidths(widths);
            tableItems.DefaultCell.Padding = 3;
            tableItems.WidthPercentage = 100;
            tableItems.HorizontalAlignment = Element.ALIGN_CENTER;
            tableItems.DefaultCell.BorderColor = tableOddColor;

            var columnCount = items_data.Value.Columns.Count - 1;
            for (int i = 0; i < items_data.Value.Columns.Count; i++)
            {
                PdfPCell cell = new PdfPCell();
                cell.BackgroundColor = tabbleHeadColor;
                cell.PaddingBottom = 8;
                cell.VerticalAlignment = Element.ALIGN_CENTER;
                cell.AddElement(new Chunk(items_data.Value.Columns[i].ColumnName.ToString(), tblHead));
                tableItems.AddCell(cell);

            }

            var oddEvenFive = 1;
            for (int i = 0; i < items_data.Value.Rows.Count; i++)
            {
                var incColspan = 1;
                if (oddEvenFive % 2 == 0)
                {
                    for (int j = 0; j < items_data.Value.Columns.Count; j++)
                    {
                        if (items_data.Value.Rows[i][j].ToString() != "/*//*/")
                        {
                            PdfPCell tblcell = new PdfPCell();
                            tblcell.BackgroundColor = tabbleEvenColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.Colspan = incColspan;
                            tblcell.VerticalAlignment = Element.ALIGN_CENTER;
                            if (items_data.Value.Rows.Count == oddEvenFive)
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBodyBold));
                            }
                            else
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBody));
                            }

                            tableItems.AddCell(tblcell);
                            incColspan = 1;
                        }
                        else
                        {
                            incColspan++;
                        }
                    }
                }
                else
                {
                    for (int j = 0; j < items_data.Value.Columns.Count; j++)
                    {
                        if (items_data.Value.Rows[i][j].ToString() != "/*//*/")
                        {
                            PdfPCell tblcell = new PdfPCell();
                            tblcell.BackgroundColor = tableOddColor;
                            tblcell.PaddingTop = 0;
                            tblcell.PaddingBottom = 8;
                            tblcell.Colspan = incColspan;
                            tblcell.VerticalAlignment = Element.ALIGN_CENTER;
                            if (items_data.Value.Rows.Count == oddEvenFive)
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBodyBold));
                            }
                            else
                            {
                                tblcell.AddElement(new Chunk(items_data.Value.Rows[i][j].ToString(), tblBody));
                            }
                            tableItems.AddCell(tblcell);
                            incColspan = 1;
                        }
                        else
                        {
                            incColspan++;
                        }
                    }
                }

                oddEvenFive++;
            }

            document.Add(tableItems);

            document.Add(spacerParagraph);
            document.Add(spacerParagraph);

            Paragraph mainComments = new Paragraph();
            mainComments.Alignment = Element.ALIGN_LEFT;
            mainComments.Add(new Chunk("Inspection Comments and advice : " + dispatchtrack.RecivedComments.ToString(), commentHead));
            document.Add(mainComments);

            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);

            Paragraph QutHead = new Paragraph("Store In-charge");
            QutHead.Add(new Chunk(glue));
            QutHead.Add("Inspected by");
            QutHead.Add(new Chunk(glue));
            QutHead.Add("Certified by");
            QutHead.Add(new Chunk(glue));
            QutHead.Add("Authourized Signatory");
            document.Add(QutHead);
            

            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);

            Paragraph prgNote = new Paragraph();
            prgNote.Alignment = Element.ALIGN_LEFT;
            prgNote.Add(new Chunk("NOTE : This Document is Auto Generated from PRM360 System. ", fntQutBody));
            document.Add(prgNote);

            document.Add(spacerParagraph);
            document.Add(spacerParagraph);
            document.Add(spacerParagraph);

            Paragraph prgThanks = new Paragraph();
            prgThanks.Alignment = Element.ALIGN_CENTER;
            prgThanks.Add(new Chunk(" THANK YOU FOR YOUR BUSINESS! ", sideHead));
            document.Add(prgThanks);

            document.Close();
            writer.Close();
            fs.Close();
        }


        public static string ExceptionHandler(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                value = " ";
            }
            return value;
        }

        public static string dateFormatChange(DateTime? value)
        {

            //DateTime s1 = System.Convert.ToDateTime(value);
            //DateTime date = (s1);
            //String frmdt = date.ToString("dd/MM/yyyy  hh:mm:ss tt");
            //return frmdt + " (UTC +0)";

            PRMServices prm = new PRMServices();
            DateTime s1 = System.Convert.ToDateTime(value);
            DateTime date = prm.toLocal((s1));
            String frmdt = date.ToString("dd/MM/yyyy  hh:mm:ss tt");
            return frmdt;

        }

        //private void InitializeComponent()
        //{
        //    this.SuspendLayout();
        //    // 
        //    // PdfUtilities
        //    // 
        //    this.ClientSize = new System.Drawing.Size(284, 261);
        //    this.Name = "PdfUtilities";
        //    this.Load += new System.EventHandler(this.PdfUtilities_Load);
        //    this.ResumeLayout(false);

        //}

        private void PdfUtilities_Load(object sender, EventArgs e)
        {

        }


        //private byte[] AddWatermark(byte[] bytes, BaseFont bf)
        //{
        //    using (var ms = new MemoryStream(10 * 1024))
        //    {
        //        using (var reader = new PdfReader(bytes))
        //        using (var stamper = new PdfStamper(reader, ms))
        //        {
        //            int times = reader.NumberOfPages;
        //            for (int i = 1; i <= times; i++)
        //            {
        //                var dc = stamper.GetOverContent(i);
        //                AddWaterMark(dc, ConfigurationManager.AppSettings["COMPANY_NAME"].ToString(), bf, 48, 35, new BaseColor(70, 70, 255), reader.GetPageSizeWithRotation(i));
        //            }
        //            stamper.Close();
        //        }
        //        return ms.ToArray();
        //    }
        //}

        //public static void AddWaterMark(PdfContentByte dc, string text, BaseFont font, float fontSize, float angle, BaseColor color, Rectangle realPageSize, Rectangle rect = null)
        //{
        //    var gstate = new PdfGState { FillOpacity = 0.1f, StrokeOpacity = 0.3f };
        //    dc.SaveState();
        //    dc.SetGState(gstate);
        //    dc.SetColorFill(color);
        //    dc.BeginText();
        //    dc.SetFontAndSize(font, fontSize);
        //    var ps = rect ?? realPageSize; /*dc.PdfDocument.PageSize is not always correct*/
        //    var x = (ps.Right + ps.Left) / 2;
        //    var y = (ps.Bottom + ps.Top) / 2;
        //    dc.ShowTextAligned(Element.ALIGN_CENTER, text, x, y, angle);
        //    dc.EndText();
        //    dc.RestoreState();
        //}

        private static void GetTemplateDetails(int reqId, int templateId = 0)
        {
            PRMServices service = new PRMServices();
            PRMCustomFieldService fieldService = new PRMCustomFieldService();
            if (templateId > 0)
            {
                templateFields = fieldService.GetTemplateFieldsTemp(templateId, string.Empty);
            }
            else
            {
                List<RequirementSetting> settings = service.GetReqSettings(reqId);
                if (settings != null && settings.Count > 0)
                {
                    var template = settings.First(s => s.REQ_SETTING == "TEMPLATE_ID");
                    if (template != null && !string.IsNullOrEmpty(template.REQ_SETTING_VALUE))
                    {
                        templateId = Convert.ToInt32(template.REQ_SETTING_VALUE);

                        if (templateId > 0)
                        {
                            templateFields = fieldService.GetTemplateFieldsTemp(templateId, string.Empty);
                        }
                    }
                }
            }
            

            if (templateFields == null || templateFields.Count <= 0)
            {
                templateFields = fieldService.GetTemplateFieldsTemp(0, "PRM-DEFAULT");
            }
        }

        private static string GetLabel(string fieldName)
        {
            string lableName = string.Empty;
            try
            {
                if (templateFields != null && templateFields.Count > 0 && !string.IsNullOrEmpty(fieldName))
                {
                    var template = templateFields.First(t => t.FIELD_NAME == fieldName.ToUpper());

                    if (template != null)
                    {
                        lableName = template.FIELD_LABEL;
                    }
                }
            }
            catch (Exception ex)
            {
                
            }

            return lableName;
        }
    }
}