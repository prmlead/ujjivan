﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using System.Net;
using System.Configuration;

using PRMServices.Models;
using PRMServices.Models.Catalog;
using PRMServices.SQLHelper;
using MAT = PRMServices.Syngene.Material.Service;
using PO = PRMServices.Syngene.PO.Service2;
using Newtonsoft.Json;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMSAPIntegrationService : IPRMSAPIntegrationService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();

        public Response SaveERPPRStageData(ERPPRStage[] data)
        {
            Response response = new Response();
            logger.Debug("Service started:" + data.Length);
            try
            {
                int count = 0;
                int readingCount = 1;
                if (data != null && data.Length > 0)
                {
                    logger.Debug("Total records came:" + data.Length);
                    string failedRecords = "";
                    string queryValues = string.Empty;
                    string query = @"INSERT INTO erp_pr_stage (PLANT, PLANT_CODE, PURCHASE_GROUP_CODE, PURCHASE_GROUP_NAME, REQUESITION_DATE, 
                                    PR_RELEASE_DATE, PR_CHANGE_DATE, PR_NUMBER, REQUISITIONER_NAME, REQUISITIONER_EMAIL, REQUESITION_ITEM, 
                                    MATERIAL_GROUP_CODE, MATERIAL_GROUP_DESC, MATERIAL_CODE, MATERIAL_TYPE, MATERIAL_HSN_CODE, MATERIAL_DESCRIPTION, SHORT_TEXT, ITEM_TEXT, UOM, 
                                    QTY_REQUIRED, MATERIAL_DELIVERY_DATE, LEAD_TIME, PR_TYPE, PR_TYPE_DESC, FILE_NAME, DATE_CREATED, DATE_MODIFIED, CREATED_BY, MODIFIED_BY)  Values";
                    string queryAudit = @"INSERT INTO erp_pr_stage_audit (PLANT, PLANT_CODE, PURCHASE_GROUP_CODE, PURCHASE_GROUP_NAME, REQUESITION_DATE, 
                                    PR_RELEASE_DATE, PR_CHANGE_DATE, PR_NUMBER, REQUISITIONER_NAME, REQUISITIONER_EMAIL, REQUESITION_ITEM, 
                                    MATERIAL_GROUP_CODE, MATERIAL_GROUP_DESC, MATERIAL_CODE, MATERIAL_TYPE, MATERIAL_HSN_CODE, MATERIAL_DESCRIPTION, SHORT_TEXT, ITEM_TEXT, UOM, 
                                    QTY_REQUIRED, MATERIAL_DELIVERY_DATE, LEAD_TIME, PR_TYPE, PR_TYPE_DESC, FILE_NAME, DATE_CREATED, DATE_MODIFIED, CREATED_BY, MODIFIED_BY)  Values";
                    foreach (var info in data)
                    {
                        try
                        {
                            DateTime REQUESITION_DATE;
                            DateTime.TryParse(trimValues(info.REQUESITION_DATE), out REQUESITION_DATE);

                            DateTime PR_RELEASED_DATE;
                            DateTime.TryParse(trimValues(info.PR_RELEASE_DATE),out PR_RELEASED_DATE);
                            DateTime PR_CHANGE_DATE;
                            DateTime.TryParse(trimValues(info.PR_CHANGE_DATE),out PR_CHANGE_DATE);
                            DateTime MATERIAL_DELIVERY_DATE;
                            DateTime.TryParse(trimValues(info.MATERIAL_DELIVERY_DATE),out MATERIAL_DELIVERY_DATE);
                            string columns = "(~$^{0}~$^,~$^{1}~$^,~$^{2}~$^,~$^{3}~$^,~$^{4}~$^," +
                                             "~$^{5}~$^,~$^{6}~$^,~$^{7}~$^,~$^{8}~$^,~$^{9}~$^," +
                                             "~$^{10}~$^,~$^{11}~$^,~$^{12}~$^,~$^{13}~$^,~$^{14}~$^," +
                                             "~$^{15}~$^,~$^{16}~$^,~$^{17}~$^,~$^{18}~$^,~$^{19}~$^," +
                                             "~$^{20}~$^,~$^{21}~$^,~$^{22}~$^,~$^{23}~$^,~$^{24}~$^, " +
                                             "~$^{25}~$^,utc_timestamp(), utc_timestamp(), 0, 0),";
                            queryValues = queryValues + 
                                string.Format(columns, trimValues(info.PLANT), trimValues(info.PLANT_CODE), trimValues(info.PURCHASE_GROUP_CODE), trimValues(info.PURCHASE_GROUP_NAME),REQUESITION_DATE.ToString("yyyy-MM-dd HH:mm:ss"),
                                PR_RELEASED_DATE.ToString("yyyy-MM-dd HH:mm:ss"),PR_CHANGE_DATE.ToString("yyyy-MM-dd HH:mm:ss"),trimValues(info.PR_NUMBER), trimValues(info.REQUISITIONER_NAME), trimValues(info.REQUISITIONER_EMAIL), trimValues(info.REQUISITION_ITEM), 
                                trimValues(info.MATERIAL_GROUP_CODE), trimValues(info.MATERIAL_GROUP_DESC),trimValues(info.MATERIAL_CODE), trimValues(info.MATERIAL_TYPE), trimValues(info.MATERIAL_HSN_CODE), trimValues(info.MATERIAL_DESCRIPTION),trimValues(info.SHORT_TEXT), trimValues(info.ITEM_TEXT),trimValues(info.UOM), 
                                info.QTY_REQUIRED.Replace(",",""),MATERIAL_DELIVERY_DATE.ToString("yyyy-MM-dd HH:mm:ss"),trimValues(info.LEAD_TIME), trimValues(info.PR_TYPE), trimValues(info.PR_TYPE_DESC), trimValues(info.FILE_NAME));

                            logger.Debug("Record Number:" + readingCount);
                            count++;
                            readingCount++;
                        }
                        catch (Exception ex)
                        {
                            failedRecords = failedRecords + info.PR_NUMBER + ";";
                            logger.Error("Error occured YPRMPR:" + ex.Message);
                        }
                    }

                    if (!string.IsNullOrEmpty(queryValues))
                    {
                        queryValues = queryValues.Replace(@"'", "\\'");
                        queryValues = queryValues.Replace("~$^", "'");
                        queryValues = queryValues.Substring(0, queryValues.Length - 1);
                        queryValues = queryValues + ";";
                        query = query + queryValues;
                        queryAudit = queryAudit + queryValues;
                        sqlHelper.ExecuteQuery("delete from erp_pr_stage");
                        var dataset = sqlHelper.ExecuteQuery(query);
                        sqlHelper.ExecuteQuery(queryAudit);
                    }

                    var dattable = sqlHelper.SelectQuery("SELECT COUNT(PR_NUMBER) AS TOTAL_RECORDS FROM ERP_PR_STAGE");
                    int totalRecords = 0;
                    if (dattable != null && dattable.Rows.Count > 0)
                    {
                        totalRecords = Convert.ToInt32(dattable.Rows[0]["TOTAL_RECORDS"]);
                    }

                    if (count > 0)
                    {
                        var sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_DATE", DateTime.Now);
                        ProcessPostIntegration("erp_ProcessPR", sd);
                    }
                    response.Message = "Records recieved:" + data.Length + ", processed:" + count + ", Current Total Records:" + totalRecords + ", Failed Records:" + failedRecords;
                    response.ObjectID = count;
                }
                else
                {
                    logger.Debug("No data found...");
                    throw new Exception("Empty data.");
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error occured YPRMPR:" + ex.Message);
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public string genLineItemNum(int num) 
        {
            var maxNum = 5;
            num = num * 10;
            return num.ToString().PadLeft(maxNum, '0');
        }
        
        public Response SendPostPoData(SAPOEntity[] data, string rawJSON, string qcsVendorAssignmentJSON, string qcsRequirementJSON, bool isDraft)
        {
            Response response = new Response();
            string sapResponse = string.Empty;
            string sapErrorResponse = string.Empty;
            Random generator = new Random();
            List<PO.ZWEB_PO_DET> poData = new List<PO.ZWEB_PO_DET>();
            string quotId = "360-" + generator.Next(0, 999999).ToString("D6");
            string poStatus = isDraft ? "DRAFT" : "SUBMITTING";
            try
            {
                if (data != null && data.Length > 0)
                {
                    if (!string.IsNullOrEmpty(data[0].QUOT_NO))
                    {
                        quotId = data[0].QUOT_NO;
                    }

                    // Generate PRM PO Number

                    Dictionary<int, string> distinctList = new Dictionary<int, string>();
                    var distinctIds = data.GroupBy(elem => elem.VENDOR_ID).Select(group => group.First());
                    foreach (SAPOEntity po in distinctIds)
                    {
                        string prmPONumber = string.Empty; //GeneratePRMPONumber(po.VENDOR_ID);
                        foreach (var de in data)
                        {
                            if (de.VENDOR_ID == po.VENDOR_ID)
                            {
                                de.PRM_PO_NUM = GeneratePRMPONumber(de.BRANCH_LOCATION, "PO", de.COMP_ID);
                            }

                        }
                        //distinctList.Add(po.VENDOR_ID, po.VENDOR_PRIMARY_EMAIL);
                    }

                    // Generate PRM PO Number


                    //if (isDraft && !string.IsNullOrWhiteSpace(data[0].QUOT_NO))
                    //{
                    //    sqlHelper.ExecuteNonQuery_IUD($"DELETE FROM POGenerateDetails WHERE QUOT_NO = '{quotId}'");
                    //}
                    string query = @" INSERT INTO [dbo].[POScheduleDetails] ( [COMP_ID],[PLANT],[PRODUCT_ID],[DESCRIPTION],[CATEGORY_ID],[VENDOR_CODE],
                                                                              [VENDOR_ID],[PO_NUMBER],[PO_LINE_ITEM],[PO_DATE],[PO_CREATOR],[PAYMENT_TERMS],
                                                                              [TAX_CODE],[TAX_CODE_DESC],[PO_RELEASE_DATE],[DELIVERY_DATE],[MAT_TYPE],[CITY],
                                                                              [REGION],[HSN_CODE],[ORDER_QTY],[UOM],[ALTERNATIVE_UOM],[ALTERNATIVE_UOM_QTY],
                                                                              [NET_PRICE],[VALUE],[CURRENCY],[FREIGHT],[PENDING_QTY],[PR_ITEM_ID],
                                                                              [ITEM_TEXT_PO],[REL_IND],[PR_REQUISITIONER],[DOC_TYPE],[SERVICE_CODE],[SERVICE_DESC],
                                                                              [MISC_CHARGES],[PACKING_CHARGES],[PO_CONTRACT],[VALID_FROM],[VALID_TO],[DELIVERY_COMPLETED],
                                                                              [DELETED_INDICATOR],[DATE_CREATED],[DATE_MODIFIED],[CREATED_BY],[MODIFIED_BY],[VENDOR_COMPANY],
                                                                              [PO_STATUS],[DELIVERY_STATUS],[LAST_RECEIPT_DATE],[ROW_ID],[VENDOR_ACK_STATUS],[VENDOR_ACK_COMMENTS],
                                                                              [VENDOR_EXPECTED_DELIVERY_DATE],[CGST],[SGST],[IGST],[CESS],[TCS],
                                                                              [PO_SENT_STATUS],[BRANCH_LOCATION],[MATERIAL],[MATERIAL_GROUP],[VENDOR_PHONE] ,[VENDOR_EMAIL], [REQ_ID], [PR_NUMBER], [QCS_ID], [QCS_TYPE],[PRM_PO_NUMBER],[ITEM_DISCOUNT_VALUE]) OUTPUT inserted.PO_SCHEDULE_ID ";

                    foreach (var dataEntry in data)
                    {

                        query += $@"SELECT {dataEntry.COMP_ID},'{RemoveAllSpecialCharacters(dataEntry.WERKS)}',{dataEntry.PRODUCT_ID},'{RemoveAllSpecialCharacters(dataEntry.SHORT_TEXT)}','{dataEntry.CATEGORY_ID.Split(',')[0]}','{dataEntry.LIFNR}',
                                            {dataEntry.VENDOR_ID},'{dataEntry.PRM_PO_NUM}','{dataEntry.BNFPO}',GETUTCDATE(),'{dataEntry.ERNAM}','{RemoveAllSpecialCharacters(dataEntry.ZTERM)}',
                                            '{dataEntry.MWSKZ}','',GETUTCDATE(),'{dataEntry.EINDT}','{dataEntry.MTART}','',
                                            '','',{dataEntry.MENGE},'{dataEntry.MEINS}','',NULL,
                                            {dataEntry.NETPR},NULL,'{dataEntry.WAERS}',NULL,NULL,'{dataEntry.BNFPO}',
                                            '{RemoveAllSpecialCharacters(dataEntry.SHORT_TEXT)}',NULL,NULL,'{dataEntry.BSART}',NULL,NULL,
                                            NULL,NULL,NULL,NULL,NULL,NULL,
                                             NULL,GETUTCDATE(),GETUTCDATE(),{dataEntry.USER},{dataEntry.USER},'{dataEntry.VENDOR_COMPANY}',
                                              NULL,NULL,NULL,0,NULL,NULL,
                                               NULL,NULL,NULL,NULL,NULL,NULL,
                                               0,'{dataEntry.BRANCH_LOCATION}','{dataEntry.MATNR}',NULL,'{dataEntry.VENDOR_PHONE}','{dataEntry.VENDOR_EMAIL}',{dataEntry.REQ_ID},'{dataEntry.BANFN}',{dataEntry.QCS_ID},(select QCS_TYPE from qcs_details where QCS_ID = {dataEntry.QCS_ID}),'{dataEntry.PRM_PO_NUM}',{dataEntry.RB00} UNION ";
                    }

                    if (!string.IsNullOrEmpty(query) && query.EndsWith(" UNION "))
                    {
                        query = query.Substring(0, query.Length - (" UNION ").Length);
                        query = query + ";";
                    }
                    DataSet ds = sqlHelper.ExecuteQuery(query);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        //string updatePOQuery = string.Empty;
                        //foreach (DataRow row in ds.Tables[0].Rows)
                        //{
                        //    int poID = row["PO_SCHEDULE_ID"] != DBNull.Value ? Convert.ToInt32(row["PO_SCHEDULE_ID"]) : 0;
                        //    updatePOQuery += $@"UPDATE POScheduleDetails SET PRM_PO_NUMBER = (select concat(PO_SCHEDULE_ID,REPLICATE('0',10-LEN(PO_SCHEDULE_ID))) from POScheduleDetails where PO_SCHEDULE_ID = {poID}), PO_STATUS = '{poStatus}' WHERE PO_SCHEDULE_ID = {poID};";
                        //}
                        //sqlHelper.ExecuteNonQuery_IUD(updatePOQuery);
                    }
                }
            }
            catch (Exception ex)
            {
                poStatus = "ERROR_OCCURED";
                logger.Error("Error occured:" + ex.Message + ", FOR QUOT ID:" + quotId);
                logger.Error("Error occured:" + ex.StackTrace + ", FOR QUOT ID:" + quotId);
                response.ErrorMessage = ex.Message + " - SAP RESPONSE:" + sapErrorResponse;
                string message = ex.Message.Length < 510 ? ex.Message : ex.Message.Substring(0, 510);
                message = message.Replace("'", "''");
                //  sqlHelper.ExecuteNonQuery_IUD($@"UPDATE POGenerateDetails SET SAP_RESPONSE ='{message}', PO_STATUS = '{poStatus}' WHERE QUOT_NO = '{quotId}';");
            }
            return response;
        }

        public string GeneratePRMPONumber(string branchLocation,string type,int compID)
        {
            string poNumber = string.Empty;
            var tempFactor = sqlHelper.ExecuteQuery($@"select dbo.GenerateSeries('{branchLocation}','PO',{compID})");
            if (tempFactor != null && tempFactor.Tables.Count > 0 && tempFactor.Tables[0].Rows.Count > 0)
            {
                poNumber =  (tempFactor.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(tempFactor.Tables[0].Rows[0][0]) : string.Empty);
            }
            return poNumber;
        }

        public string RemoveAllSpecialCharacters(string text)
        {
            if (string.IsNullOrEmpty(text))
                return text;

            string result = Regex.Replace(text, "[:!@#$%^&*()}{|\":?><\\[\\]\\;'/.,~\n]", " ");
            return result;
        }

        public Response PostPOData(SAPOEntity[] data, string rawJSON, string qcsVendorAssignmentJSON, string qcsRequirementJSON, bool isDraft)
        {
            Response response = new Response();
            string sapResponse = string.Empty;
            string sapErrorResponse = string.Empty;
            //logger.Debug("Service started:" + inputJSON);
            Random generator = new Random();
            List<PO.ZWEB_PO_DET> poData = new List<PO.ZWEB_PO_DET>();
            string quotId = "360-" + generator.Next(0, 999999).ToString("D6");
            string poStatus = isDraft ? "DRAFT" : "SUBMITTING";
            try
            {
                if (data != null && data.Length > 0)
                {
                    if (!string.IsNullOrEmpty(data[0].QUOT_NO))
                    {
                        quotId = data[0].QUOT_NO;
                    }

                    logger.Debug("TOTAL PO ENTRIES:" + data.Length.ToString());
                    logger.Debug("UI DATA:" + JsonConvert.SerializeObject(data));
                    if (isDraft && !string.IsNullOrWhiteSpace(data[0].QUOT_NO))
                    {
                        sqlHelper.ExecuteNonQuery_IUD($"DELETE FROM POGenerateDetails WHERE QUOT_NO = '{quotId}'");
                    }

                    string query = @"INSERT INTO [dbo].[POGenerateDetails] ([COMP_ID], [REQ_ID], [PO_NO], [PO_TEMPLATE], [QUOT_NO], [LIFNR], [BUKRS], [EKORG], [ZTERM], [ERNAM], [INCO1], [INCO2], [EKGRP], 
                                [WAERS], [MEMORYTYPE], [DELIVERY_MODE], [ZZQANO], [ZZMAP_NON_GMP], [EINDT], [ITM_SP_INST], [WERKS], [MTART], [MATNR], [EMATN], [BANFN], [BNFPO], [MEINS], 
                                [BPRME], [PEINH], [PEINHSpecified], [IDNLF], [KONNR], [KTPNR], [MWSKZ], [PBXX], [PBXXSpecified], [RB00], [RB00Specified], [P_26F1], [P_26F1Specified], [P_26F2], 
                                [P_26F2Specified], [FRA2], [FRA2Specified], [FRA1], [FRA1Specified], [FRB1], [FRB1Specified], [FRB2], [FRB2Specified], [ZMS1], [ZMS1Specified], [ZMS2], 
                                [ZMS2Specified], [ZMS3], [ZMS3Specified], [ZMS4], [ZMS4Specified], [MENGE], [KDATB], [KDATE], [NETPR], [SRVPOS], [EXTROW], [STEUC], [VENDOR_ID], [BSART],
                                [INSURANCE_NUMBER], [PENALITY_PER_WEEK], [NUMBER_WEEKS], [MAX_PENALITY], [MAX_WEEKS], [PO_STATUS], [QCS_ID],
                                [PO_RAW_JSON], [QCS_VENDOR_ASSIGNMENT_JSON], [QCS_REQUIREMENT_JSON],
                                [DATE_CREATED], [DATE_MODIFIED], [CREATED_BY], [MODIFIED_BY]) OUTPUT inserted.PO_GENERATE_ID ";

                    foreach (var dataEntry in data)
                    {
                        logger.Debug("ZTERM:" + dataEntry.ZTERM);
                        dataEntry.BUKRS = "5100";
                        dataEntry.EKORG = "5100";
                        dataEntry.QUOT_NO = quotId;
                        PO.ZWEB_PO_DET poEntry = new PO.ZWEB_PO_DET();
                        poEntry.QUOT_NO = quotId;
                        poEntry.LIFNR = dataEntry.LIFNR;
                        poEntry.BUKRS = dataEntry.BUKRS;
                        poEntry.EKORG = dataEntry.EKORG;
                        poEntry.ZTERM = dataEntry.ZTERM;
                        poEntry.ERNAM = dataEntry.ERNAM;
                        poEntry.INCO1 = dataEntry.INCO1;
                        poEntry.INCO2 = dataEntry.INCO2;
                        poEntry.EKGRP = dataEntry.EKGRP;
                        poEntry.WAERS = dataEntry.WAERS;
                        poEntry.MEMORYTYPE = dataEntry.MEMORYTYPE;
                        poEntry.DELIVERY_MODE = dataEntry.DELIVERY_MODE;
                        poEntry.ZZQANO = dataEntry.ZZQANO;
                        poEntry.ZZMAP_NON_GMP = dataEntry.ZZMAP_NON_GMP;
                        poEntry.EINDT = dataEntry.EINDT; //yyyy-MM-YY
                        poEntry.ITM_SP_INST = dataEntry.ITM_SP_INST;
                        poEntry.WERKS = dataEntry.WERKS;
                        poEntry.MTART = dataEntry.MTART;
                        poEntry.MATNR = dataEntry.MATNR;
                        poEntry.EMATN = dataEntry.EMATN;
                        poEntry.BANFN = dataEntry.BANFN;
                        poEntry.BNFPO = dataEntry.BNFPO;
                        poEntry.MEINS = dataEntry.MEINS;
                        poEntry.BPRME = dataEntry.BPRME;
                        poEntry.PEINH = dataEntry.PEINH;
                        poEntry.PEINHSpecified = true;// dataEntry.PEINHFieldSpecified;
                        poEntry.IDNLF = dataEntry.IDNLF;
                        poEntry.KONNR = dataEntry.KONNR;
                        poEntry.KTPNR = string.IsNullOrEmpty(dataEntry.KTPNR)? "0" : dataEntry.KTPNR;
                        poEntry.MWSKZ = dataEntry.MWSKZ;
                        poEntry.PBXX = dataEntry.PBXX;
                        poEntry.PBXXSpecified = true;//dataEntry.PBXXFieldSpecified;
                        poEntry.RB00 = dataEntry.RB00 > 0 ? dataEntry.RB00 * -1 : 0;
                        poEntry.RB00Specified = true;//dataEntry.RB00FieldSpecified;
                        poEntry.P_26F1 = dataEntry.P_26F1;
                        poEntry.P_26F1Specified = true;//dataEntry.P_26F1FieldSpecified;
                        poEntry.P_26F2 = dataEntry.P_26F2;
                        poEntry.P_26F2Specified = true;//dataEntry.P_26F2FieldSpecified;
                        poEntry.FRA2 = dataEntry.FRA2;
                        poEntry.FRA2Specified = true;//dataEntry.FRA2FieldSpecified;
                        poEntry.FRA1 = dataEntry.FRA1;
                        poEntry.FRA1Specified = true;//dataEntry.FRA1FieldSpecified;
                        poEntry.FRB1 = dataEntry.FRB1;
                        poEntry.FRB1Specified = true;//dataEntry.FRB1FieldSpecified;
                        poEntry.FRB2 = dataEntry.FRB2;
                        poEntry.FRB2Specified = true;//dataEntry.FRB2FieldSpecified;
                        poEntry.ZMS1 = dataEntry.ZMS1;
                        poEntry.ZMS1Specified = true;//dataEntry.ZMS1FieldSpecified;
                        poEntry.ZMS2 = dataEntry.ZMS2;
                        poEntry.ZMS2Specified = true;//dataEntry.ZMS2FieldSpecified;
                        poEntry.ZMS3 = dataEntry.ZMS3;
                        poEntry.ZMS3Specified = true;//dataEntry.ZMS3FieldSpecified;
                        poEntry.ZMS4 = dataEntry.ZMS4;
                        poEntry.ZMS4Specified = true;//dataEntry.ZMS4FieldSpecified;
                        poEntry.MENGE = dataEntry.MENGE;
                        poEntry.MENGESpecified = true;//dataEntry.MENGEFieldSpecified;
                        poEntry.KDATB = dataEntry.KDATB;
                        poEntry.KDATE = dataEntry.KDATE;
                        poEntry.BSART = dataEntry.BSART;
                        poEntry.NETPR = dataEntry.NETPR;
                        poEntry.NETPRSpecified = true;
                        poEntry.SRVPOS = dataEntry.SRVPOS;
                        poEntry.EXTROW = dataEntry.EXTROW;
                        poEntry.STEUC = dataEntry.STEUC;
                        //poEntry.JIMS = dataEntry.JINS;
                        poEntry.ZMIS = dataEntry.ZMIS;
                        poEntry.ZMISSpecified = true;
                        poEntry.JOFP = dataEntry.JOFP;
                        poEntry.JOFPSpecified = true;
                        poEntry.JOFV = dataEntry.JOFV;
                        poEntry.JOFVSpecified = true;
                        poEntry.EBTYP = dataEntry.EBTYP;
                        poEntry.ZZBONGING = dataEntry.ZZBONGING;

                        poData.Add(poEntry);

                        query += $@" SELECT {dataEntry.COMP_ID}, '{dataEntry.REQ_ID}', '', '{dataEntry.PO_TEMPLATE}', '{dataEntry.QUOT_NO}', '{dataEntry.LIFNR}', '{dataEntry.BUKRS}', '{dataEntry.EKORG}', '{dataEntry.ZTERM}', '{dataEntry.ERNAM}', 
                                            '{dataEntry.INCO1}', '{dataEntry.INCO2}', '{dataEntry.EKGRP}', '{dataEntry.WAERS}', '{dataEntry.MEMORYTYPE}', '{dataEntry.DELIVERY_MODE}', '{dataEntry.ZZQANO}', 
                                            '{dataEntry.ZZMAP_NON_GMP}', '{dataEntry.EINDT}', '{dataEntry.ITM_SP_INST}', '{dataEntry.WERKS}', '{dataEntry.MTART}', '{dataEntry.MATNR}', '{dataEntry.EMATN}', '{dataEntry.BANFN}', 
                                            '{dataEntry.BNFPO}', '{dataEntry.MEINS}', '{dataEntry.BPRME}', '{dataEntry.PEINH}', '{dataEntry.PEINHFieldSpecified}', '{dataEntry.IDNLF}', '{dataEntry.KONNR}', '{dataEntry.KTPNR}', 
                                            '{dataEntry.MWSKZ}', '{dataEntry.PBXX}', '{dataEntry.PBXXFieldSpecified}', '{dataEntry.RB00}', 
                                            '{dataEntry.RB00FieldSpecified}', '{dataEntry.P_26F1}', '{dataEntry.P_26F1FieldSpecified}', '{dataEntry.P_26F2}', 
                                            '{dataEntry.P_26F2FieldSpecified}', '{dataEntry.FRA2}', '{dataEntry.FRA2FieldSpecified}', '{dataEntry.FRA1}', '{dataEntry.FRA1FieldSpecified}', '{dataEntry.FRB1}', 
                                            '{dataEntry.FRB1FieldSpecified}', '{dataEntry.FRB2}', '{dataEntry.FRB2FieldSpecified}', '{dataEntry.ZMS1}', '{dataEntry.ZMS1FieldSpecified}', '{dataEntry.ZMS2}', 
                                            '{dataEntry.ZMS2FieldSpecified}', '{dataEntry.ZMS3}', '{dataEntry.ZMS3FieldSpecified}', '{dataEntry.ZMS4}', '{dataEntry.ZMS4FieldSpecified}', '{dataEntry.MENGE}', 
                                            '{dataEntry.KDATB}', '{dataEntry.KDATE}', '{dataEntry.NETPR}', '{dataEntry.SRVPOS}', '{dataEntry.EXTROW}', '{dataEntry.STEUC}', {dataEntry.VENDOR_ID}, '{dataEntry.BSART}',
                                            '{dataEntry.INSURANCE_NUMBER}', {dataEntry.PENALITY_PER_WEEK}, {dataEntry.NUMBER_WEEKS}, {dataEntry.MAX_PENALITY}, {dataEntry.MAX_WEEKS}, '{poStatus}', {dataEntry.QCS_ID},
                                            '{rawJSON}', '{qcsVendorAssignmentJSON}', '{qcsRequirementJSON}',
                                            GETUTCDATE(), GETUTCDATE(), '{dataEntry.USER}', '{dataEntry.USER}' UNION ";
                    }

                    if(!string.IsNullOrEmpty(query) && query.EndsWith(" UNION "))
                    {
                        query = query.Substring(0, query.Length - (" UNION ").Length);
                        query = query + ";";
                    }
                    DataSet ds = sqlHelper.ExecuteQuery(query);

                    if (ds!=null && ds.Tables.Count > 0) {
                        string updatePOQuery = string.Empty;
                        foreach (DataRow row in ds.Tables[0].Rows) {
                            int poID = row["PO_GENERATE_ID"] != DBNull.Value ? Convert.ToInt32(row["PO_GENERATE_ID"]) : 0;
                            updatePOQuery += $@"UPDATE POGenerateDetails SET PO_NO = (select concat(PO_GENERATE_ID,REPLICATE('0',10-LEN(PO_GENERATE_ID))) from POGenerateDetails where po_generate_id = {poID}),  SAP_RESPONSE = '{sapResponse}', PO_STATUS = '{poStatus}' WHERE PO_GENERATE_ID = {poID};";
                        }
                        sqlHelper.ExecuteNonQuery_IUD(updatePOQuery);
                    }

                    if (!isDraft && false)
                    {
                        //https://biopiqas.biocon.com/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_PRM360&receiverParty=&receiverService=&interface=SI_POCreate_Sync_Out&interfaceNamespace=http://biocon.com/PRM360/SAP/POCreate
                        string newURL = ConfigurationManager.AppSettings["Integration.PODomain"].ToString() + "/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_PRM360&receiverParty=&receiverService=&interface=SI_POCreate_Sync_Out&interfaceNamespace=http://biocon.com/PRM360/SAP/POCreate";
                        logger.Debug("SAP PO URL:" + newURL);
                        NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["Integration.UserId"].ToString(), ConfigurationManager.AppSettings["Integration.UserPwd"].ToString());
                        Uri uri = new Uri(newURL);
                        ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                        PO.SI_POCreate_Sync_OutService poService = new PO.SI_POCreate_Sync_OutService();
                        poService.Url = newURL;
                        poService.Credentials = credentials;
                        poService.PreAuthenticate = true;
                        poStatus = "SUBMITTED";
                        logger.Debug("SAP DATA:" + JsonConvert.SerializeObject(poData));
                        var poOutput = poService.SI_POCreate_Sync_Out(poData.ToArray(), out var poNumber);
                        sapResponse = poOutput != null ? JsonConvert.SerializeObject(poOutput) : string.Empty;
                        if (poOutput != null)
                        {
                            foreach (var item in poOutput)
                            {
                                if (!string.IsNullOrEmpty(item.MESSAGE))
                                {
                                    sapErrorResponse += item.MESSAGE + ",";
                                }

                                if (!string.IsNullOrEmpty(item.MESSAGE_V1))
                                {
                                    sapErrorResponse += item.MESSAGE_V1 + ",";
                                }

                                if (!string.IsNullOrEmpty(item.MESSAGE_V2))
                                {
                                    sapErrorResponse += item.MESSAGE_V2 + ",";
                                }

                                if (!string.IsNullOrEmpty(item.MESSAGE_V3))
                                {
                                    sapErrorResponse += item.MESSAGE_V3 + ",";
                                }

                                if (!string.IsNullOrEmpty(item.MESSAGE_V4))
                                {
                                    sapErrorResponse += item.MESSAGE_V4 + ",";
                                }
                            }
                        }

                        logger.Debug("PO NUMBER:" + poNumber);
                        logger.Debug("SAP RESPONSE:" + sapResponse);
                        response.Message = $"PO NUMBER: {poNumber} for QUOT ID: {quotId}" + " - SAP RESPONSE:" + sapErrorResponse;
                        if (string.IsNullOrEmpty(poNumber))
                        {
                            poStatus = "ERROR_RESPONSE";
                            poNumber = "";
                            response.ErrorMessage = $"FAILED TO GENERATED PO FOR QUOT ID: {quotId} - SAP RESPONSE: {sapErrorResponse}";
                        }

                        query = $@"UPDATE POGenerateDetails SET PO_NO ='{poNumber}',  SAP_RESPONSE = '{sapResponse}', PO_STATUS = '{poStatus}' WHERE QUOT_NO = '{quotId}';";
                        sqlHelper.ExecuteNonQuery_IUD(query);
                    }
                }
            }
            catch (Exception ex)
            {
                poStatus = "ERROR_OCCURED";
                logger.Error("Error occured:" + ex.Message + ", FOR QUOT ID:" +  quotId);
                logger.Error("Error occured:" + ex.StackTrace + ", FOR QUOT ID:" + quotId);
                response.ErrorMessage = ex.Message + " - SAP RESPONSE:" + sapErrorResponse;
                string message = ex.Message.Length < 510 ? ex.Message : ex.Message.Substring(0, 510);
                message = message.Replace("'", "''");
                sqlHelper.ExecuteNonQuery_IUD($@"UPDATE POGenerateDetails SET SAP_RESPONSE ='{message}', PO_STATUS = '{poStatus}' WHERE QUOT_NO = '{quotId}';");
            }

            return response;
        }

        public Product GetMaterialFromSAP(string materialcode, string sessionid)
        {
            Product product = new Product();
            try
            {
                logger.Debug("START GetMaterailDetails: " + DateTime.Now.ToString());
                string newURL = ConfigurationManager.AppSettings["Integration.Domain"].ToString() + "/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_PRM360&receiverParty=&receiverService=&interface=SI_MaterialMaster_Sync_Out&interfaceNamespace=http%3A%2F%2Fbiocon.com%2FPRM360%2FSAP%2FMaterialMaster";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["Integration.UserId"].ToString(), ConfigurationManager.AppSettings["Integration.UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                MAT.SI_MaterialMaster_Sync_OutService test = new MAT.SI_MaterialMaster_Sync_OutService();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;

                List<MAT.ZSASNUM> S_ASNUM = new List<MAT.ZSASNUM>();
                S_ASNUM.Add(new MAT.ZSASNUM()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<MAT.ZSASTYP> S_ASTYP = new List<MAT.ZSASTYP>();
                S_ASTYP.Add(new MAT.ZSASTYP()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<MAT.ZSMATNR> S_MATNR = new List<MAT.ZSMATNR>();
                S_MATNR.Add(new MAT.ZSMATNR()
                {

                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = materialcode,
                    HIGH = ""
                });

                List<MAT.ZSMTART> S_MTART = new List<MAT.ZSMTART>();
                S_MTART.Add(new MAT.ZSMTART()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });               

                List<MAT.ZSWERKS> S_WERKS = new List<MAT.ZSWERKS>();
                S_WERKS.Add(new MAT.ZSWERKS()
                {
                    SIGN = "I",
                    OPTION = "BT",
                    LOW = "5100",
                    HIGH = "5199"
                });

                var materails = test.SI_MaterialMaster_Sync_Out("", S_ASNUM.ToArray(), S_ASTYP.ToArray(), S_MATNR.ToArray(), S_MTART.ToArray(), S_WERKS.ToArray());

                if (materails != null && materails.Length > 0)
                {
                    product.ProductCode = !string.IsNullOrWhiteSpace(materails[0].MATNR) ? materails[0].MATNR : materails[0].ASNUM;
                    product.ProductName = materails[0].MAKTX;
                    product.ProductHSNCode = materails[0].STEUC;
                    product.ProductName = materails[0].MAKTX;
                    product.ProductNo = !string.IsNullOrWhiteSpace(materails[0].MATNR) ? materails[0].MATNR : materails[0].ASNUM;
                    product.ProdQty = !string.IsNullOrWhiteSpace(materails[0].MEINS) ? materails[0].MEINS : materails[0].MEINS1;
                    product.ProductDesc = !string.IsNullOrWhiteSpace(materails[0].MTART) ? materails[0].PO_TEXT : materails[0].ASKTX;
                    product.ProdAlternativeUnits = materails[0].MEINH;
                    product.UnitConversion = Convert.ToString(materails[0].UMREZ);
                    product.IsValid = 1;
                    product.CasNumber = materails[0].CASNR;
                    product.MfcdCode = materails[0].GPNUM;

                    product.ProdAlternativeUnits1 = materails[0].MEINH1;
                    product.ProdAlternativeUnits2 = materails[0].MEINH2;
                    product.ProdAlternativeUnits3 = materails[0].MEINH3;
                    product.ProdAlternativeUnits4 = materails[0].MEINH4;
                    product.ProdAlternativeUnits5 = materails[0].MEINH5;
                    product.ProdAlternativeUnits6 = materails[0].MEINH6;
                    product.ProdAlternativeUnits7 = materails[0].MEINH7;

                    product.ProdAlternativeUnits_con1 = materails[0].UMREZ1;
                    product.ProdAlternativeUnits_con2 = materails[0].UMREZ1;
                    product.ProdAlternativeUnits_con3 = materails[0].UMREZ1;
                    product.ProdAlternativeUnits_con4 = materails[0].UMREZ1;
                    product.ProdAlternativeUnits_con5 = materails[0].UMREZ1;
                    product.ProdAlternativeUnits_con6 = materails[0].UMREZ1;
                    product.ProdAlternativeUnits_con7 = materails[0].UMREZ1;
                    product.DateCreated = DateTime.UtcNow;
                    product.DateModified = DateTime.UtcNow;
                }
            }
            catch(Exception ex)
            {
                product.ErrorMessage = ex.Message;
            }

            return product;
        }

        public MAT.ZSMATFINAL[] GetMaterialFromSAPTemp(string materialcode, string sessionid)
        {
            Product product = new Product();
            try
            {
                logger.Debug("START GetMaterailDetails: " + DateTime.Now.ToString());
                string newURL = ConfigurationManager.AppSettings["Integration.Domain"].ToString() + "/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_PRM360&receiverParty=&receiverService=&interface=SI_MaterialMaster_Sync_Out&interfaceNamespace=http%3A%2F%2Fbiocon.com%2FPRM360%2FSAP%2FMaterialMaster";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["Integration.UserId"].ToString(), ConfigurationManager.AppSettings["Integration.UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                MAT.SI_MaterialMaster_Sync_OutService test = new MAT.SI_MaterialMaster_Sync_OutService();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;

                List<MAT.ZSASNUM> S_ASNUM = new List<MAT.ZSASNUM>();
                S_ASNUM.Add(new MAT.ZSASNUM()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<MAT.ZSASTYP> S_ASTYP = new List<MAT.ZSASTYP>();
                S_ASTYP.Add(new MAT.ZSASTYP()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<MAT.ZSMATNR> S_MATNR = new List<MAT.ZSMATNR>();
                S_MATNR.Add(new MAT.ZSMATNR()
                {

                    SIGN = "I",
                    OPTION = "EQ",
                    LOW = materialcode,
                    HIGH = ""
                });

                List<MAT.ZSMTART> S_MTART = new List<MAT.ZSMTART>();
                S_MTART.Add(new MAT.ZSMTART()
                {
                    SIGN = "",
                    OPTION = "",
                    LOW = "",
                    HIGH = ""
                });

                List<MAT.ZSWERKS> S_WERKS = new List<MAT.ZSWERKS>();
                S_WERKS.Add(new MAT.ZSWERKS()
                {
                    SIGN = "I",
                    OPTION = "BT",
                    LOW = "5100",
                    HIGH = "5199"
                });

                logger.Debug($"MATERIALCODE:{materialcode}");
                var materails = test.SI_MaterialMaster_Sync_Out("", S_ASNUM.ToArray(), S_ASTYP.ToArray(), S_MATNR.ToArray(), S_MTART.ToArray(), S_WERKS.ToArray());

                return materails;
            }
            catch (Exception ex)
            {
                logger.Debug($"ERROR:{ex.Message}");
                product.ErrorMessage = ex.Message;
            }

            return null;
        }


        public string trimValues(string value) {

            if (string.IsNullOrEmpty(value))
            {
                return value;
            }
            else {
                return value.Trim();
            }
        }

        private void ProcessPostIntegration(string spName, SortedDictionary<object, object> sd = null)
        {
            sd = sd ?? new SortedDictionary<object, object>() { };
            var dataset = sqlHelper.SelectList(spName, sd);
        }
    }
}